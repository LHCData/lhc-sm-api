import pandas as pd
import pytest
from nxcals.spark_session_builder import get_or_create
import pandas.testing as pdt
from lhcsmapi.api import query


def test_query_winccoa_by_variables_no_data(caplog: pytest.LogCaptureFixture):
    # arrange
    spark = get_or_create()
    t_start = 0
    t_end = 100
    variable = "RQ5.L8:CMD_ABORT_PIC"

    # act
    cmd_abort_pic = query.query_winccoa_by_variables(spark, t_start, t_end - t_start, variable)

    # assert
    assert cmd_abort_pic.empty
    assert caplog.messages == [
        "Querying NXCALS returned no results for the variable RQ5.L8:CMD_ABORT_PIC: "
        "system: WINCCOA, start_time: 0, duration: 100, include_latest_data_point_prior_to_start: False, "
        "max_latest_data_point_search_period: 31,536,000,000,000,000"
    ]


def test_query_winccoa_by_variables_one_variable(caplog: pytest.LogCaptureFixture):
    # arrange
    spark = get_or_create()
    t_start = 1692941470073000000
    t_end = 1692943413250000000
    variable = "RQ5.L8:CMD_ABORT_PIC"

    # act
    cmd_abort_pic = query.query_winccoa_by_variables(spark, t_start, t_end - t_start, variable)

    # assert
    pdt.assert_series_equal(
        cmd_abort_pic,
        pd.Series(
            {1692941590778000000: 0.0, 1692941591097000000: 1.0, 1692941591193000000: 0.0, 1692942918481000000: 1.0},
            name="RQ5.L8:CMD_ABORT_PIC",
        ).rename_axis("timestamp", axis=0),
    )
    assert caplog.messages == []


def test_query_winccoa_by_variables_with_previous_extra_point_one_variable(caplog: pytest.LogCaptureFixture):
    # arrange
    spark = get_or_create()
    t_start = 1692941470073000000
    t_end = 1692943413250000000
    variable = "RQ5.L8:CMD_ABORT_PIC"

    # act
    cmd_abort_pic = query.query_winccoa_by_variables(spark, t_start, t_end - t_start, variable, True)

    # assert
    pdt.assert_series_equal(
        cmd_abort_pic,
        pd.Series(
            {
                1692885418633000000: 1.0,
                1692941590778000000: 0.0,
                1692941591097000000: 1.0,
                1692941591193000000: 0.0,
                1692942918481000000: 1.0,
            },
            name="RQ5.L8:CMD_ABORT_PIC",
        ).rename_axis("timestamp", axis=0),
    )
    assert caplog.messages == []


def test_query_winccoa_by_variables_with_previous_extra_point_only_preceding_data(caplog: pytest.LogCaptureFixture):
    # arrange
    spark = get_or_create()
    t_start = 1692942918481000000 + 1
    t_end = 1692942918481000000 + 2
    latest_preceding_duration_threshold = 1
    variable = "RQ5.L8:CMD_ABORT_PIC"

    # act
    cmd_abort_pic = query.query_winccoa_by_variables(
        spark, t_start, t_end - t_start, variable, True, latest_preceding_duration_threshold
    )

    # assert
    pdt.assert_series_equal(
        cmd_abort_pic,
        pd.Series({1692942918481000000: 1.0}, name="RQ5.L8:CMD_ABORT_PIC").rename_axis("timestamp", axis=0),
    )
    assert caplog.messages == []


def test_query_winccoa_by_variables_with_previous_extra_point_one_variable_no_preceding_data(
    caplog: pytest.LogCaptureFixture,
):
    # arrange
    spark = get_or_create()
    t_start = 1692941470073000000
    t_end = 1692943413250000000
    variable = "RQ5.L8:CMD_ABORT_PIC"

    # act
    cmd_abort_pic = query.query_winccoa_by_variables(spark, t_start, t_end - t_start, variable, True, 1_000_000)

    # assert
    pdt.assert_series_equal(
        cmd_abort_pic,
        pd.Series(
            {1692941590778000000: 0.0, 1692941591097000000: 1.0, 1692941591193000000: 0.0, 1692942918481000000: 1.0},
            name="RQ5.L8:CMD_ABORT_PIC",
        ).rename_axis("timestamp", axis=0),
    )
    assert caplog.messages == [
        "Querying NXCALS returned no results while trying to query the latest data point prior to start: "
        "system: WINCCOA, variable: RQ5.L8:CMD_ABORT_PIC, start_time: 1692941470073000000, "
        "max_latest_data_point_search_period: 1,000,000"
    ]


def test_query_winccoa_by_variables_multiple_variables(caplog: pytest.LogCaptureFixture):
    # arrange
    spark = get_or_create()
    t_start = 1692941470073000000
    t_end = 1692943413250000000
    variable = ["RQ5.L8:CMD_ABORT_PIC", "RQ5.L8:ST_ABORT_PIC"]

    # act
    cmd_abort_pic, st_abort_pic = query.query_winccoa_by_variables(spark, t_start, t_end - t_start, variable)

    # assert
    pdt.assert_series_equal(
        cmd_abort_pic,
        pd.Series(
            {1692941590778000000: 0.0, 1692941591097000000: 1.0, 1692941591193000000: 0.0, 1692942918481000000: 1.0},
            name="RQ5.L8:CMD_ABORT_PIC",
        ).rename_axis("timestamp", axis=0),
    )
    pdt.assert_series_equal(
        st_abort_pic,
        pd.Series(
            {1692941590777000000: 0.0, 1692942866929000000: 1.0, 1692942866931000000: 0.0, 1692942918482000000: 1.0},
            name="RQ5.L8:ST_ABORT_PIC",
        ).rename_axis("timestamp", axis=0),
    )
    assert caplog.messages == []


def test_query_winccoa_by_variables_with_previous_extra_point_multiple_variables(caplog: pytest.LogCaptureFixture):
    # arrange
    spark = get_or_create()
    t_start = 1692941470073000000
    t_end = 1692943413250000000
    variable = ["RQ5.L8:CMD_ABORT_PIC", "RQ5.L8:ST_ABORT_PIC"]

    # act
    cmd_abort_pic, st_abort_pic = query.query_winccoa_by_variables(spark, t_start, t_end - t_start, variable, True)

    # assert
    pdt.assert_series_equal(
        cmd_abort_pic,
        pd.Series(
            {
                1692885418633000000: 1.0,
                1692941590778000000: 0.0,
                1692941591097000000: 1.0,
                1692941591193000000: 0.0,
                1692942918481000000: 1.0,
            },
            name="RQ5.L8:CMD_ABORT_PIC",
        ).rename_axis("timestamp", axis=0),
    )
    pdt.assert_series_equal(
        st_abort_pic,
        pd.Series(
            {
                1692885418633000000: 1.0,
                1692941590777000000: 0.0,
                1692942866929000000: 1.0,
                1692942866931000000: 0.0,
                1692942918482000000: 1.0,
            },
            name="RQ5.L8:ST_ABORT_PIC",
        ).rename_axis("timestamp", axis=0),
    )
    assert caplog.messages == []


@pytest.mark.parametrize(
    "start_time, duration",
    [
        (1678971236639000000, 1678973093361000000 - 1678971236639000000),
        (1678971236639000000, (1678973093361000000 - 1678971236639000000, "ns")),
    ],
)
def test_query_hwc_powering_test_parameters_valid_case(start_time, duration):
    # arrange
    circuit_name = "RD4.R4"

    # act
    result = query.query_hwc_powering_test_parameters(circuit_name, start_time, duration)

    # assert
    assert result == {
        "RPHF.UA47.RD4.R4": {
            "I_DELTA": {"value": 50.0, "unit": "A"},
            "I_MEAS_MAX": {"value": 5.0, "unit": "A"},
            "I_5TEV": {"value": 3200.0, "unit": "A"},
            "I_INTERM_1": {"value": 1000.0, "unit": "A"},
            "I_INTERM_2": {"value": 2000.0, "unit": "A"},
            "I_INTERM_3": {"value": 3500.0, "unit": "A"},
            "I_PHASE_1": {"value": 1000.0, "unit": "A"},
            "I_PCC": {"value": 250.0, "unit": "A"},
            "ACC_PNO": {"value": 2.0, "unit": "A/s²"},
            "I_ERR_PCC_MAX": {"value": 0.04, "unit": "A"},
            "TIME_ZERO": {"value": 60.0, "unit": "s"},
            "TIME_PCC": {"value": 30.0, "unit": "s"},
            "L_TOT": {"value": 0.052, "unit": "H"},
            "I_ERR_MAX": {"value": 0.07, "unit": "A"},
            "I_HARDWARE": {"value": 6850.0, "unit": "A"},
            "I_PNO": {"value": 6000.0, "unit": "A"},
            "DIDT_PNO": {"value": 18.147, "unit": "A/s"},
            "R_TOT_MEASURED": {"value": 0.000581, "unit": "Ω"},
            "I_MIN_OP": {"value": 200.0, "unit": "A"},
            "I_EARTH_MAX": {"value": 0.0075, "unit": "A"},
            "TIME_PNO": {"value": 300.0, "unit": "s"},
            "TIME_PNO_LEADS": {"value": 3600.0, "unit": "s"},
            "I_INJECTION": {"value": 350.0, "unit": "A"},
            "TIME_PLI": {"value": 240.0, "unit": "s"},
            "I_EARTH_PCC_MAX": {"value": 0.01, "unit": "A"},
            "I_PNO_TRAINING": {"value": 6050.0, "unit": "A"},
        }
    }


def test_query_hwc_powering_test_parameters_renamed_fgc():
    # arrange
    circuit_name = "RQ6.R1"
    start_time = 1492706598921000000
    end_time = 1492707382516000000

    # act
    result = query.query_hwc_powering_test_parameters(circuit_name, start_time, end_time - start_time)

    # assert
    assert result == {
        "RPHGB.RR17.RQ6.R1B1": {
            "ACC_PNO": {"unit": "A/s²", "value": 2.0},
            "DIDT_ASYN": {"unit": "A/s", "value": 4.0},
            "DIDT_PNO": {"unit": "A/s", "value": 12.931},
            "I_5TEV": {"unit": "A", "value": 2300.0},
            "I_DELTA": {"unit": "A", "value": 50.0},
            "I_EARTH_MAX": {"unit": "A", "value": 0.01},
            "I_EARTH_PCC_MAX": {"unit": "A", "value": 0.01},
            "I_ERR_MAX": {"unit": "A", "value": 0.05},
            "I_ERR_PCC_MAX": {"unit": "A", "value": 0.04},
            "I_HARDWARE": {"unit": "A", "value": 4650.0},
            "I_INJECTION": {"unit": "A", "value": 206.0},
            "I_INTERM_1": {"unit": "A", "value": 1300.0},
            "I_INTERM_1_MID": {"unit": "A", "value": 750.0},
            "I_INTERM_2": {"unit": "A", "value": 2300.0},
            "I_INTERM_3": {"unit": "A", "value": 1900.0},
            "I_INTERM_4": {"unit": "A", "value": 2300.0},
            "I_PCC": {"unit": "A", "value": 150.0},
            "I_PHASE_1": {"unit": "A", "value": 700.0},
            "I_PNO": {"unit": "A", "value": 4050.0},
            "I_PNO_MID": {"unit": "A", "value": 1250.0},
            "I_PNO_TRAINING": {"unit": "A", "value": 4100.0},
            "I_SM_INT_1": {"unit": "A", "value": 1200.0},
            "I_SM_INT_2": {"unit": "A", "value": 1700.0},
            "L_TOT": {"unit": "H", "value": 0.021},
            "R_TOT_MEASURED": {"unit": "Ω", "value": 0.000264},
            "TIME_PCC": {"unit": "s", "value": 30.0},
            "TIME_PNO_LEADS": {"unit": "s", "value": 3600.0},
            "TIME_TOP": {"unit": "s", "value": 300.0},
            "TIME_ZERO": {"unit": "s", "value": 60.0},
            "U_IND": {"unit": "V", "value": 0.271},
        },
        "RPHGB.RR17.RQ6.R1B2": {
            "ACC_PNO": {"unit": "A/s²", "value": 2.0},
            "DIDT_ASYN": {"unit": "A/s", "value": 4.0},
            "DIDT_PNO": {"unit": "A/s", "value": 12.931},
            "I_5TEV": {"unit": "A", "value": 2300.0},
            "I_DELTA": {"unit": "A", "value": 50.0},
            "I_EARTH_MAX": {"unit": "A", "value": 0.01},
            "I_EARTH_PCC_MAX": {"unit": "A", "value": 0.01},
            "I_ERR_MAX": {"unit": "A", "value": 0.05},
            "I_ERR_PCC_MAX": {"unit": "A", "value": 0.04},
            "I_HARDWARE": {"unit": "A", "value": 4650.0},
            "I_INJECTION": {"unit": "A", "value": 206.0},
            "I_INTERM_1": {"unit": "A", "value": 1300.0},
            "I_INTERM_1_MID": {"unit": "A", "value": 750.0},
            "I_INTERM_2": {"unit": "A", "value": 2300.0},
            "I_INTERM_3": {"unit": "A", "value": 1900.0},
            "I_INTERM_4": {"unit": "A", "value": 2300.0},
            "I_PCC": {"unit": "A", "value": 150.0},
            "I_PHASE_1": {"unit": "A", "value": 700.0},
            "I_PNO": {"unit": "A", "value": 4050.0},
            "I_PNO_MID": {"unit": "A", "value": 1250.0},
            "I_PNO_TRAINING": {"unit": "A", "value": 4100.0},
            "I_SM_INT_1": {"unit": "A", "value": 1200.0},
            "I_SM_INT_2": {"unit": "A", "value": 1700.0},
            "L_TOT": {"unit": "H", "value": 0.021},
            "R_TOT_MEASURED": {"unit": "Ω", "value": 0.000237},
            "TIME_PCC": {"unit": "s", "value": 30.0},
            "TIME_PNO_LEADS": {"unit": "s", "value": 3600.0},
            "TIME_TOP": {"unit": "s", "value": 300.0},
            "TIME_ZERO": {"unit": "s", "value": 60.0},
            "U_IND": {"unit": "V", "value": 0.271},
        },
    }, result


def test_query_hwc_powering_test_parameters_invalid_query(caplog: pytest.LogCaptureFixture):
    # arrange
    circuit_name = "RD4.R4"
    start_time = 100
    duration = 100

    res = query.query_hwc_powering_test_parameters(circuit_name, start_time, duration)

    assert res == {}
    assert caplog.messages == [
        "Querying HWC powering test parameters returned no data for the the pc RPHF.UA47.RD4.R4 "
        "(query: http://mpe-systems-pro:60245/v1/systemInformation/powerConverters/RPHF.UA47.RD4.R4/params?startTimeInNanos=100&endTimeInNanos=200)"
    ]


def test_query_hwc_powering_test_parameters_invalid_circuit_name():
    # arrange
    circuit_name = "qwerty"
    start_time = 1678971236639000000
    duration = 1678973093361000000 - 1678971236639000000

    # act
    with pytest.raises(ValueError):
        query.query_hwc_powering_test_parameters(circuit_name, start_time, duration)


def test_query_hwc_powering_test_parameters_invalid_duration():
    # arrange
    circuit_name = "RD4.R4"
    start_time = 1678971236639000000
    duration = -1
    # act
    with pytest.raises(ValueError):
        query.query_hwc_powering_test_parameters(circuit_name, start_time, duration)
