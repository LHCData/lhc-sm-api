import random
import warnings
from typing import List

import numpy as np
import pandas as pd
import pytest

from lhcsmapi.api import processing
from test.test_pyedsl.test_QueryBuilder import read_csv


def test_sum_empty_dataframe():
    df1 = pd.DataFrame()
    df2 = pd.DataFrame(index=[1, 2, 3], data=[100, 200, 300], columns=["col2"])

    with pytest.raises(ValueError):
        processing.sum_dataframes(df1, df2, "result")


def test_sum_no_data_dataframe():
    df1 = pd.DataFrame(columns=["col1"], dtype=np.float64, index=pd.Index([], dtype=int))
    df2 = pd.DataFrame(index=[1, 2, 3], data=[100, 200, 300], columns=["col2"])

    result = processing.sum_dataframes(df1, df2, "result")

    pd.testing.assert_frame_equal(
        pd.DataFrame(data=df2.values, index=df2.index, columns=["result"], dtype=np.float64), result
    )


def test_sum_dataframes():
    df1 = pd.DataFrame(index=[3, 4, 5], data=[100, 200, 300], columns=["col1"])
    df2 = pd.DataFrame(index=[1, 2, 3], data=[100, 200, 300], columns=["col2"])

    result = processing.sum_dataframes(df1, df2, "result")
    expected_result = pd.DataFrame(
        index=[1, 2, 3, 4, 5], data=[100, 200, 400, 500, 600], columns=["result"], dtype=np.float64
    )

    pd.testing.assert_frame_equal(expected_result, result)


def test_pm_signal_query_sync_time():
    # arrange
    i_meas_df_input = pd.DataFrame(
        {
            "STATUS.I_MEAS": {
                1426220409500000000: 9571.501,
                1426220409520000000: 9571.699,
                1426220409540000000: 9571.899,
                1426220409560000000: 9572.1,
                1426220409580000000: 9572.3,
            }
        }
    )
    timestamp = 1426220469520000000
    i_meas_df_ref = pd.DataFrame(
        {
            "STATUS.I_MEAS": {
                -60020000000: 9571.501,
                -60000000000: 9571.699,
                -59980000000: 9571.899,
                -59960000000: 9572.1,
                -59940000000: 9572.3,
            }
        }
    )
    # act
    i_meas_df = processing.SignalProcessing(i_meas_df_input).synchronize_time(timestamp).get_dataframes()
    # assert
    pd.testing.assert_frame_equal(i_meas_df_ref, i_meas_df.head())


def test_pm_signal_query_sync_time_convert_to_sec():
    # arrange
    timestamp = 1426220509500000000
    i_meas_df_input = pd.DataFrame(
        {
            "STATUS.I_MEAS": {
                1426220509480000000: 6853.1220,
                1426220509500000000: 6851.7130,
                1426220509520000000: 6850.3050,
                1426220509540000000: 6848.8970,
                1426220509560000000: 6847.4860,
                1426220509580000000: 6846.0780,
                1426220509600000000: 6844.6690,
            }
        }
    )
    i_meas_df_ref = pd.DataFrame(
        {"STATUS.I_MEAS": {-0.02: 6853.1220, 0.00: 6851.7130, 0.02: 6850.3050, 0.04: 6848.8970, 0.06: 6847.4860}}
    )
    # act
    i_meas_df = (
        processing.SignalProcessing(i_meas_df_input).synchronize_time(timestamp).convert_index_to_sec().get_dataframes()
    )
    # assert
    pd.testing.assert_frame_equal(i_meas_df_ref, i_meas_df.head())


def test_signal_processing_rename_columns():
    # arrange
    i_meas_b1_df = pd.DataFrame(
        {
            "STATUS.I_MEAS": {
                1426220509480000000: 6853.1220,
                1426220509500000000: 6851.7130,
                1426220509520000000: 6850.3050,
            }
        }
    )
    i_meas_b2_df = pd.DataFrame(
        {
            "STATUS.I_MEAS": {
                1426220509480000000: 6953.1220,
                1426220509500000000: 6951.7130,
                1426220509520000000: 6950.3050,
            }
        }
    )

    # act
    i_meas_b1_df, i_meas_b2_df = (
        processing.SignalProcessing([i_meas_b1_df, i_meas_b2_df])
        .rename_columns(["I_MEAS_B1", "I_MEAS_B2"])
        .get_dataframes()
    )

    # assert
    assert i_meas_b1_df.columns == ["I_MEAS_B1"]
    assert i_meas_b2_df.columns == ["I_MEAS_B2"]


def test_signal_processing_rename_columns_invalid_number_of_fields():
    # arrange
    i_meas_b1_df = pd.DataFrame({"STATUS.I_MEAS": {}})

    # act
    with pytest.raises(ValueError) as e:
        processing.SignalProcessing(i_meas_b1_df).rename_columns(["I_MEAS_B1", "I_MEAS_B2"]).get_dataframes()

    assert str(e.value) == "Number of column names should be equal to the number of dataframes"


def test_pm_signal_query_remove_values_for_time():
    # arrange
    i_meas_df_input = pd.DataFrame(
        {
            "STATUS.I_MEAS": {
                1426220509480000000: 6853.1220,
                1426220509500000000: 6851.7130,
                1426220509520000000: 6850.3050,
                1426220509540000000: 6848.8970,
                1426220509560000000: 6847.4860,
                1426220509580000000: 6846.0780,
                1426220509600000000: 6844.6690,
            }
        }
    )
    i_meas_df_ref = pd.DataFrame(
        {
            "STATUS.I_MEAS": {
                1426220509560000000: 6847.4860,
                1426220509580000000: 6846.0780,
                1426220509600000000: 6844.6690,
            }
        }
    )
    # act
    i_meas_df = (
        processing.SignalProcessing(i_meas_df_input)
        .remove_values_for_time_less_than_or_equal_to(1426220509540000000)
        .get_dataframes()
    )
    # assert
    pd.testing.assert_frame_equal(i_meas_df, i_meas_df_ref.head())


def test_pm_filter_median():
    # arrange
    i_meas_df_input = pd.DataFrame(
        {
            "STATUS.I_MEAS": {
                1426220409500000000: 9571.501,
                1426220409520000000: 9571.699,
                1426220409540000000: 9571.899,
                1426220409560000000: 9572.100,
                1426220409580000000: 9572.300,
            }
        }
    )
    i_meas_df_ref = pd.DataFrame(
        {
            "STATUS.I_MEAS": {
                1426220409500000000: 9571.501,
                1426220409520000000: 9571.6,
                1426220409540000000: 9571.699,
                1426220409560000000: 9571.899,
                1426220409580000000: 9572.1,
            }
        }
    )
    # act
    i_meas_df = processing.SignalProcessing(i_meas_df_input).filter_median().get_dataframes()
    # assert
    pd.testing.assert_frame_equal(i_meas_df_ref, i_meas_df.head())


def test_pm_signal_query_remove_initial_offset():
    # arrange
    i_hds_df_input = pd.DataFrame(
        {
            "I_HDS_4": {
                1426220469491020832: 0.0896478,
                1426220469491026040: 0.0915552,
                1426220469491031248: 0.0915552,
                1426220469491036456: 0.0934626,
                1426220469491041664: 0.0915552,
                1426220469491046872: 0.0915552,
                1426220469491052080: 0.0896478,
                1426220469491057288: 0.0934626,
                1426220469491062496: 0.0934626,
                1426220469491067704: 0.0934626,
            }
        }
    )
    timestamp = 1426220469491041664
    i_hds_df_ref = pd.DataFrame(
        {
            "I_HDS_4": {
                1426220469491020832: -0.0019074000000000035,
                1426220469491026040: 0.0,
                1426220469491031248: 0.0,
                1426220469491036456: 0.0019074000000000035,
                1426220469491041664: 0.0,
            }
        }
    )
    # act
    i_hds_df = processing.SignalProcessing(i_hds_df_input).remove_initial_offset(timestamp).get_dataframes()
    # assert
    pd.testing.assert_frame_equal(i_hds_df_ref, i_hds_df.head())


def test_query_builder_pm_duration_event_query_filter_source():
    # arrange
    source_timestamp_input_df = pd.DataFrame(
        {
            "source": {
                0: "B20L5",
                1: "C20L5",
                2: "A20L5",
                3: "A21L5",
                4: "B21L5",
                5: "C23L4",
                6: "B23L4",
                7: "A23L4",
                8: "C22L4",
                9: "C15R4",
                10: "B15R4",
                11: "A15R4",
                12: "B34L8",
                13: "C34L8",
                14: "A34L8",
                15: "C33L8",
                16: "C34R7",
                17: "A34R7",
                18: "A20R3",
                19: "B20R3",
                20: "C20R3",
                21: "B18L5",
                22: "A18L5",
                23: "C18L5",
                24: "A19L5",
            },
            "timestamp": {
                0: 1426220469491000000,
                1: 1426220517100000000,
                2: 1426220518112000000,
                3: 1426220625990000000,
                4: 1426220866112000000,
                5: 1426236802332000000,
                6: 1426236839404000000,
                7: 1426236839832000000,
                8: 1426236949841000000,
                9: 1426251285711000000,
                10: 1426251337747000000,
                11: 1426251388741000000,
                12: 1426258716281000000,
                13: 1426258747672000000,
                14: 1426258747370000000,
                15: 1426258835955000000,
                16: 1426258853947000000,
                17: 1426258854113000000,
                18: 1426267931956000000,
                19: 1426267983579000000,
                20: 1426268004144000000,
                21: 1426277626360000000,
                22: 1426277679838000000,
                23: 1426277680496000000,
                24: 1426277903449000000,
            },
        }
    )
    source_timestamp_ref_df = pd.DataFrame(
        {
            "source": {
                0: "B20L5",
                1: "C20L5",
                2: "A20L5",
                3: "A21L5",
                4: "B21L5",
                5: "C15R4",
                6: "B15R4",
                7: "A15R4",
                8: "B18L5",
                9: "A18L5",
                10: "C18L5",
                11: "A19L5",
            },
            "timestamp": {
                0: 1426220469491000000,
                1: 1426220517100000000,
                2: 1426220518112000000,
                3: 1426220625990000000,
                4: 1426220866112000000,
                5: 1426251285711000000,
                6: 1426251337747000000,
                7: 1426251388741000000,
                8: 1426277626360000000,
                9: 1426277679838000000,
                10: 1426277680496000000,
                11: 1426277903449000000,
            },
        }
    )
    # act
    source_timestamp_act_df = (
        processing.EventProcessing(source_timestamp_input_df)
        .filter_source(circuit_type="RB", circuit_name="RB.A45", system="QH")
        .get_dataframe()
    )
    # assert
    pd.testing.assert_frame_equal(source_timestamp_ref_df, source_timestamp_act_df)


def test_pm_event_query_filter_source_sort_values():
    # arrange
    source_timestamp_input_df = pd.DataFrame(
        {
            "source": {
                0: "C20L5",
                1: "B20L5",
                2: "A20L5",
                3: "A21L5",
                4: "B21L5",
                5: "C23L4",
                6: "B23L4",
                7: "A23L4",
                8: "C22L4",
                9: "C15R4",
                10: "B15R4",
                11: "A15R4",
                12: "B34L8",
                13: "C34L8",
                14: "A34L8",
                15: "C33L8",
                16: "C34R7",
                17: "A34R7",
                18: "A20R3",
                19: "B20R3",
                20: "C20R3",
                21: "B18L5",
                22: "A18L5",
                23: "C18L5",
                24: "A19L5",
            },
            "timestamp": {
                0: 1426220517100000000,
                1: 1426220469491000000,
                2: 1426220518112000000,
                3: 1426220625990000000,
                4: 1426220866112000000,
                5: 1426236802332000000,
                6: 1426236839404000000,
                7: 1426236839832000000,
                8: 1426236949841000000,
                9: 1426251285711000000,
                10: 1426251337747000000,
                11: 1426251388741000000,
                12: 1426258716281000000,
                13: 1426258747672000000,
                14: 1426258747370000000,
                15: 1426258835955000000,
                16: 1426258853947000000,
                17: 1426258854113000000,
                18: 1426267931956000000,
                19: 1426267983579000000,
                20: 1426268004144000000,
                21: 1426277626360000000,
                22: 1426277679838000000,
                23: 1426277680496000000,
                24: 1426277903449000000,
            },
        }
    )
    source_timestamp_ref_df = pd.DataFrame(
        {
            "source": {
                0: "B20L5",
                1: "C20L5",
                2: "A20L5",
                3: "A21L5",
                4: "B21L5",
                5: "C15R4",
                6: "B15R4",
                7: "A15R4",
                8: "B18L5",
                9: "A18L5",
                10: "C18L5",
                11: "A19L5",
            },
            "timestamp": {
                0: 1426220469491000000,
                1: 1426220517100000000,
                2: 1426220518112000000,
                3: 1426220625990000000,
                4: 1426220866112000000,
                5: 1426251285711000000,
                6: 1426251337747000000,
                7: 1426251388741000000,
                8: 1426277626360000000,
                9: 1426277679838000000,
                10: 1426277680496000000,
                11: 1426277903449000000,
            },
        }
    )
    # act
    source_timestamp_act_df = (
        processing.EventProcessing(source_timestamp_input_df)
        .filter_source(circuit_type="RB", circuit_name="RB.A45", system="QH")
        .sort_values(by="timestamp")
        .get_dataframe()
    )
    # assert
    pd.testing.assert_frame_equal(source_timestamp_ref_df, source_timestamp_act_df)


def test_signal_processing_overwrite_sampling_time():
    # arrange
    bm_df = pd.DataFrame(columns=["BEAMMODE"], data=[1, 2, 1, 3])
    bm_new_index_ref_df = pd.DataFrame({"BEAMMODE": {0.0: 1, 0.25: 2, 0.5: 1, 0.75: 3}})
    # act
    bm_new_index_df = processing.SignalProcessing(bm_df).overwrite_sampling_time(0.25, 1).get_dataframes()
    # assert
    pd.testing.assert_frame_equal(bm_new_index_ref_df, bm_new_index_df)


def test_pm_signal_query_append_suffix_to_column():
    # arrange
    i_meas_df_input = pd.DataFrame(
        {
            "STATUS.I_MEAS": {
                1426220409500000000: 9571.501,
                1426220409520000000: 9571.699,
                1426220409540000000: 9571.899,
                1426220409560000000: 9572.1,
                1426220409580000000: 9572.3,
            }
        }
    )
    i_meas_df_ref = pd.DataFrame(
        {
            "STATUS.I_MEAS_REF": {
                1426220409500000000: 9571.501,
                1426220409520000000: 9571.699,
                1426220409540000000: 9571.899,
                1426220409560000000: 9572.1,
                1426220409580000000: 9572.3,
            }
        }
    )
    # act
    i_meas_df = processing.SignalProcessing(i_meas_df_input).append_suffix_to_columns("_REF").get_dataframes()
    # assert
    pd.testing.assert_frame_equal(i_meas_df_ref, i_meas_df.head())


def test_signal_processing_map_values():
    # arrange
    bm_df = pd.DataFrame(columns=["BEAMMODE"], data=[1, 2, 1, 3])
    # act
    bm_mapped_df = (
        processing.SignalProcessing(bm_df).map_values({1: "INJECTION", 2: "RAMPUP", 3: "STABLEBEAMS"}).get_dataframes()
    )
    # assert
    bm_mapped_ref_df = pd.DataFrame(columns=["BEAMMODE"], data=["INJECTION", "RAMPUP", "INJECTION", "STABLEBEAMS"])
    pd.testing.assert_frame_equal(bm_mapped_ref_df, bm_mapped_df)


def test_event_processing_drop_duplicates_source_duplicated():
    # arrange
    source_timestamp_df = pd.DataFrame(
        {
            "source": {0: "A8R7", 1: "A8R7", 2: "A8R7"},
            "timestamp": {0: 1612261794564000000, 1: 1612262047581000000, 2: 1612262265122000000},
        }
    )
    event_processing = processing.EventProcessing(source_timestamp_df)
    # act
    source_timestamp_dropped_df = event_processing.drop_duplicates(column=["source", "timestamp"]).get_dataframe()
    # assert
    pd.testing.assert_frame_equal(source_timestamp_df, source_timestamp_dropped_df)


def test_event_processing_drop_duplicate_empty_df():
    # arrange
    event_processing = processing.EventProcessing(pd.DataFrame())
    # act
    ep_dropped = event_processing.drop_duplicate_source()
    # assert
    assert event_processing == ep_dropped


def test_signal_processing_filter_values():
    # arrange
    bm_df = pd.DataFrame(columns=["BEAMMODE"], data=[1, 2, 1, 3])
    # act
    bm_mapped_df = processing.SignalProcessing(bm_df).filter_values([1, 2]).get_dataframes()
    # assert
    bm_mapped_ref_df = pd.DataFrame(columns=["BEAMMODE"], data=[1, 2, 1])
    pd.testing.assert_frame_equal(bm_mapped_ref_df, bm_mapped_df)


def test_signal_processing_drop_first_n_points():
    # arrange
    df = pd.DataFrame(columns=["BEAMMODE"], data=[1, 2, 1, 3, 5, 4, 7, 6, 9, 8])
    # act
    dropped_df = processing.SignalProcessing(df).drop_first_n_points(5).get_dataframes()
    # assert
    pd.testing.assert_frame_equal(pd.DataFrame({"BEAMMODE": {5: 4, 6: 7, 7: 6, 8: 9, 9: 8}}), dropped_df)


def test_signal_processing_drop_last_n_points():
    # arrange
    df = pd.DataFrame(columns=["BEAMMODE"], data=[1, 2, 1, 3, 5, 4, 7, 6, 9, 8])
    # act
    dropped_df = processing.SignalProcessing(df).drop_last_n_points(5).get_dataframes()
    # assert
    pd.testing.assert_frame_equal(pd.DataFrame({"BEAMMODE": {0: 1, 1: 2, 2: 1, 3: 3, 4: 5}}), dropped_df)


def test_signal_processing_calculate_char_time_profile():
    df = pd.DataFrame(columns=["I_MEAS"], data=range(105), index=range(-5, 100))

    char_profile_df = processing.SignalProcessing(df).calculate_char_time_profile().get_dataframes()

    expected_out = pd.DataFrame(
        data=[np.nan] * 5 + list(range(-5, -105, -1)), index=range(-5, 100), columns=["I_MEAS/dI_MEAS"]
    )

    pd.testing.assert_frame_equal(expected_out, char_profile_df)


def test_signal_processing_calculate_time_derivative():
    df = pd.DataFrame(columns=["I_MEAS"], data=range(100), index=range(100))

    derivative = processing.SignalProcessing(df).calculate_time_derivative().get_dataframes()

    expected_out = pd.DataFrame(data=[1] * 100, index=range(100), columns=["dI_MEAS"], dtype="float64")

    pd.testing.assert_frame_equal(expected_out, derivative)


def test_signal_processing_calculate_last_const_derivative():
    df = pd.DataFrame(
        data=list(range(10)) + [5] * 5 + [random.randrange(10) for _ in range(50)],
        index=list(range(-16, 1)) + list(range(48)),
        columns=["a"],
    )

    value, duration = processing.SignalProcessing(df).calculate_last_const_derivative(1)

    assert (value, duration) == (0, 15)


def test_signal_processing_resample():
    df = pd.DataFrame(data=range(10), index=list(range(5)) + list(range(5, 15, 2)))

    expected_out = pd.DataFrame(
        data=[
            0.000000,
            1.083333,
            2.166667,
            3.250000,
            4.333333,
            5.208333,
            5.750000,
            6.291667,
            6.833333,
            7.375000,
            7.916667,
            8.458333,
            9.000000,
        ],
        index=[
            0.000000,
            1.083333,
            2.166667,
            3.250000,
            4.333333,
            5.416667,
            6.500000,
            7.583333,
            8.666667,
            9.750000,
            10.833333,
            11.916667,
            13.000000,
        ],
    )

    resampled = processing.SignalProcessing(df).resample().get_dataframes()

    pd.testing.assert_frame_equal(expected_out, resampled)


UNLATCHED_STATES = [
    "DIM_WRN|LOW_CURRENT|NOMINAL_LOAD|PC_PERMIT|VS_POWER_ON (10377)",
    "DIM_WRN|LOW_CURRENT|NOMINAL_LOAD|PC_PERMIT|VS_POWER_ON (10377)",
    "DIM_WRN|LOW_CURRENT|NOMINAL_LOAD|PC_PERMIT|VS_POWER_ON (10377)",
    "DIM_WRN|LOG_PLEASE|LOW_CURRENT|NOMINAL_LOAD|PC_PERMIT|POST_MORTEM|PWR_FAILURE|VS_POWER_ON (11177)",
    "DIM_WRN|LOG_PLEASE|LOW_CURRENT|NOMINAL_LOAD|PC_PERMIT|POST_MORTEM|VS_POWER_ON (11145)",
    "DIM_WRN|LOG_PLEASE|LOW_CURRENT|NOMINAL_LOAD|PC_PERMIT|POST_MORTEM|VS_POWER_ON (11145)",
    "DIM_WRN|LOG_PLEASE|LOW_CURRENT|NOMINAL_LOAD|PC_PERMIT|POST_MORTEM|VS_POWER_ON (11145)",
    "DIM_WRN|LOG_PLEASE|LOW_CURRENT|NOMINAL_LOAD|PC_PERMIT|POST_MORTEM|VS_POWER_ON (11145)",
    "DIM_WRN|LOW_CURRENT|NOMINAL_LOAD|PC_PERMIT|POST_MORTEM|VS_POWER_ON (10633)",
    "DIM_WRN|LOW_CURRENT|NOMINAL_LOAD|PC_PERMIT|POST_MORTEM|VS_POWER_ON (10633)",
    "DIM_WRN|LOW_CURRENT|NOMINAL_LOAD|PC_PERMIT|POST_MORTEM|VS_POWER_ON (10633)",
]
STATE_PC = [
    "IDLE (8)",
    "IDLE (8)",
    "ARMED (10)",
    "ARMED (10)",
    "ARMED (10)",
    "RUNNING (11)",
    "RUNNING (11)",
    "IDLE (8)",
    "FLT_STOPPING (2)",
    "FLT_STOPPING (2)",
    "FLT_STOPPING (2)",
]


def test_signal_processing_decode_states_unlatched():
    df = pd.DataFrame({"UNLATCHED_STATES": UNLATCHED_STATES})
    states = [
        "DIM_WRN",
        "LOW_CURRENT",
        "NOMINAL_LOAD",
        "PC_PERMIT",
        "VS_POWER_ON",
        "LOG_PLEASE",
        "POST_MORTEM",
        "PWR_FAILURE",
    ]
    decoded: pd.DataFrame = processing.SignalProcessing(df).decode_states("UNLATCHED_STATES", states).get_dataframes()

    assert list(decoded["DIM_WRN"]) == [1] * 11
    assert list(decoded["LOW_CURRENT"]) == [1] * 11
    assert list(decoded["NOMINAL_LOAD"]) == [1] * 11
    assert list(decoded["PC_PERMIT"]) == [1] * 11
    assert list(decoded["VS_POWER_ON"]) == [1] * 11
    assert list(decoded["LOG_PLEASE"]) == [0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0]
    assert list(decoded["POST_MORTEM"]) == [0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1]
    assert list(decoded["PWR_FAILURE"]) == [0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0]


def test_signal_processing_decode_one_state_from_unlatched():
    df = pd.DataFrame({"UNLATCHED_STATES": UNLATCHED_STATES})
    states = ["PWR_FAILURE"]
    decoded: pd.DataFrame = processing.SignalProcessing(df).decode_states("UNLATCHED_STATES", states).get_dataframes()
    assert len(decoded.columns) == 2
    assert list(decoded["PWR_FAILURE"]) == [0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0]


def test_signal_processing_decode_states_state_pc():
    df = pd.DataFrame({"STATE_PC": STATE_PC})
    states = ["IDLE", "ARMED", "RUNNING", "FLT_STOPPING"]
    decoded: pd.DataFrame = processing.SignalProcessing(df).decode_states("STATE_PC", states).get_dataframes()

    assert list(decoded["IDLE"]) == [1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0]
    assert list(decoded["ARMED"]) == [0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0]
    assert list(decoded["RUNNING"]) == [0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0]
    assert list(decoded["FLT_STOPPING"]) == [0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1]


_EMPTY_DF_DECODE_STATES_PARAMS = [["IDLE", "ARMED", "RUNNING", "FLT_STOPPING"], [], ["DIM_WRN"]]


@pytest.mark.parametrize("states", _EMPTY_DF_DECODE_STATES_PARAMS)
def test_signal_processing_decode_states_empty(states):
    df = pd.DataFrame({"STATE": []})
    decoded = processing.SignalProcessing(df).decode_states("STATE_PC", states).get_dataframes()
    assert decoded.empty
    assert list(decoded.columns) == ["STATE"] + states


def _get_events(sources: List, timestamps: List) -> pd.DataFrame:
    return pd.DataFrame(
        {"source": pd.Series(sources, dtype=object), "timestamp": pd.Series(timestamps, dtype="int64")},
        index=pd.RangeIndex(start=0, stop=len(sources), step=1),
    )


ANY_EVENTS = _get_events(["any1", "any2", "any3"], [1, 2, 3])

FILTER_SOURCE_NOT_SUPPORTED_SYSTEMS_PARAMS = [
    ("RB", "any_circuit", "PC", ANY_EVENTS, ANY_EVENTS),
    ("RQ", "any_circuit", "EE", ANY_EVENTS, ANY_EVENTS),
    ("IT", "any_circuit", "PIC", ANY_EVENTS, ANY_EVENTS),
    ("IPQ", "any_circuit", "BUSBAR", ANY_EVENTS, ANY_EVENTS),
    ("IPD", "any_circuit", "DIODE", ANY_EVENTS, ANY_EVENTS),
    ("any", "any_circuit", "any", ANY_EVENTS, ANY_EVENTS),
]

FILTER_SOURCE_IT_IPQ_IPD_PARAMS = [
    ("IT", "any_circuit", "QH", _get_events(["any_circuit", "other"], [1, 2]), _get_events(["any_circuit"], [1])),
    ("IPD", "any_circuit", "QH", _get_events(["other", "any_circuit"], [1, 2]), _get_events(["any_circuit"], [2])),
    ("IPQ", "any_circuit", "QH", _get_events(["other1", "other2"], [1, 2]), _get_events([], [])),
    ("IT", "any_circuit", "QDS", _get_events(["other", "any_circuit"], [1, 2]), _get_events(["any_circuit"], [2])),
    ("IPD", "any_circuit", "QDS", _get_events(["other1", "other2"], [1, 2]), _get_events([], [])),
    ("IPQ", "any_circuit", "QDS", _get_events(["any_circuit", "other"], [1, 2]), _get_events(["any_circuit"], [1])),
]

FILTER_SOURCE_RB_RQ_PARAMS = [
    ("RB", "RB.A34", "QH", pd.DataFrame(), pd.DataFrame(index=pd.RangeIndex(start=0, stop=0, step=1))),
    ("RQ", "RQD.A78", "QH", _get_events(["12L8", "14R8", "16R7"], [1, 2, 3]), _get_events(["12L8", "16R7"], [1, 3])),
    (
        "RB",
        "RB.A45",
        "QDS",
        _get_events(["B13R4", "B21R4", "B29R4", "B32L5"], [1, 2, 3, 4]),
        _get_events(["B13R4", "B21R4", "B29R4", "B32L5"], [1, 2, 3, 4]),
    ),
    (
        "RQ",
        "RQF.A56",
        "QDS",
        _get_events(["14L6", "34R5", "16L7", "14R5"], [1, 2, 3, 4]),
        _get_events(["14L6", "34R5", "14R5"], [1, 2, 4]),
    ),
    (
        "RB",
        "RB.A81",
        "DIODE_RB",
        _get_events(["B31R8", "B13L8", "B31L1"], [1, 2, 3]),
        _get_events(["B31R8", "B31L1"], [1, 3]),
    ),
    (
        "RQ",
        "RQD.A12",
        "DIODE_RQD",
        _get_events(["B31L2", "B25L2", "B11L2"], [1, 2, 3]),
        _get_events(["B31L2", "B25L2", "B11L2"], [1, 2, 3]),
    ),
    (
        "RQ",
        "RQF.A12",
        "DIODE_RQF",
        _get_events(["B18R2", "B28R2", "B27R3", "272.B25L2"], [1, 2, 3, 4]),
        _get_events([], []),
    ),
]


FILTER_SOURCE_PARAMS = (
    FILTER_SOURCE_NOT_SUPPORTED_SYSTEMS_PARAMS + FILTER_SOURCE_IT_IPQ_IPD_PARAMS + FILTER_SOURCE_RB_RQ_PARAMS
)


@pytest.mark.parametrize("circuit_type,circuit_name,system,given,expected", FILTER_SOURCE_PARAMS)
def test_event_processing_filter_source(circuit_type, circuit_name, system, given, expected):
    # arrange
    event_processing = processing.EventProcessing(given)

    # act
    result = event_processing.filter_source(
        circuit_type=circuit_type, circuit_name=circuit_name, system=system
    ).get_dataframe()

    # assert
    pd.testing.assert_frame_equal(result, expected)


def test_event_processing_drop_duplicate_source():
    # arrange
    event_processing = processing.EventProcessing(pd.DataFrame())

    # act
    ep_dropped = event_processing.drop_duplicate_source()

    # assert
    assert event_processing == ep_dropped


def test_event_processing_sort_values():
    # arrange
    event_processing = processing.EventProcessing(pd.DataFrame())

    # act
    ep_sorted = event_processing.sort_values(by="")

    # assert
    assert event_processing == ep_sorted


def test_event_processing_drop_duplicates_all_unique():
    # arrange
    source_timestamp_df = pd.DataFrame(
        {
            "source": {0: "A8R7", 1: "B8R7", 2: "A9R7"},
            "timestamp": {0: 1612261794564000000, 1: 1612262047581000000, 2: 1612262265122000000},
        }
    )
    ep = processing.EventProcessing(source_timestamp_df)

    # act
    source_timestamp_dropped_df = ep.drop_duplicates(column=["source", "timestamp"]).get_dataframe()

    # assert
    pd.testing.assert_frame_equal(source_timestamp_df, source_timestamp_dropped_df)


def test_event_processing_drop_duplicates_timestamp_duplicate():
    # arrange
    source_timestamp_df = pd.DataFrame(
        {
            "source": {0: "A8R7", 1: "B8R7", 2: "A9R7"},
            "timestamp": {0: 1612261794564000000, 1: 1612261794564000000, 2: 1612261794564000000},
        }
    )
    ep = processing.EventProcessing(source_timestamp_df)

    # act
    source_timestamp_dropped_df = ep.drop_duplicates(column=["source", "timestamp"]).get_dataframe()

    # assert
    pd.testing.assert_frame_equal(source_timestamp_df, source_timestamp_dropped_df)


def test_event_processing_drop_duplicates_source_timestamp_duplicate():
    # arrange
    source_timestamp_df = pd.DataFrame(
        {
            "source": {0: "A8R7", 1: "A8R7", 2: "A8R7"},
            "timestamp": {0: 1612261794564000000, 1: 1612261794564000000, 2: 1612261794564000000},
        }
    )
    ep = processing.EventProcessing(source_timestamp_df)

    # act
    source_timestamp_dropped_df = ep.drop_duplicates(column=["source", "timestamp"]).get_dataframe()

    # assert
    source_timestamp_dropped_ref_df = pd.DataFrame({"source": {0: "A8R7"}, "timestamp": {0: 1612261794564000000}})
    pd.testing.assert_frame_equal(source_timestamp_dropped_ref_df, source_timestamp_dropped_df)


def test_feature_processing_sort_busbar_location_warning():
    # arrange
    busbar_df = pd.DataFrame(columns=["R_MAG"])

    # act
    # assert
    with warnings.catch_warnings(record=True) as w:
        processing.FeatureProcessing(busbar_df).sort_busbar_location(circuit_type="RB", circuit_name="RB.A12")
        if w:
            assert (
                "This operation is only supported to feature DataFrame with nxcals_variable_name "
                "column, got Index(['R_MAG'], dtype='object')" == str(w[0].message)
            )


def test_feature_processing_sort_values():
    # arrange
    busbar_df = pd.DataFrame(list(zip([4, 0, 1, 2], [0, 0, 1, 2])), columns=["min", "max"])

    # act
    max_abs_act_df = processing.FeatureProcessing(busbar_df).sort_values(by="min").get_dataframe()

    # assert
    max_abs_ref_df = pd.DataFrame(list(zip([0, 1, 2, 4], [0, 1, 2, 0])), columns=["min", "max"])
    pd.testing.assert_frame_equal(max_abs_act_df, max_abs_ref_df)


def test_feature_processing_correct_voltage_sign():
    # arrange
    busbar_df = read_csv("query_builder/nxcals_fq/RB_U_MAG_RAW_Voltage_Sign_RB12_RB45.csv")
    expected_result = read_csv("query_builder/nxcals_fq/RB_U_MAG_Corrected_Voltage_Sign_RB12_RB45.csv")
    # act
    actual_result = processing.FeatureProcessing(busbar_df).correct_voltage_sign().get_dataframe()
    # assert
    pd.testing.assert_frame_equal(expected_result, actual_result)
