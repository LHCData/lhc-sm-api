import pkgutil
from io import StringIO
from unittest.mock import patch

import pandas as pd
import pytest

from lhcsmapi.api import resolver
from lhcsmapi.metadata import signal_metadata

_ANY_DURATION = 1234
_ANY_TIMESTAMP = 1544622149620000000
_ANOTHER_TIMESTAMP = 1544622149620000001
_ANOTHER_DURATION = _ANY_DURATION + 1

_PC_FOR_RQD = [
    "RPHE.UA23.RQD.A12",
    "RPHE.UA27.RQD.A23",
    "RPHE.UA43.RQD.A34",
    "RPHE.UA47.RQD.A45",
    "RPHE.UA63.RQD.A56",
    "RPHE.UA67.RQD.A67",
    "RPHE.UA83.RQD.A78",
    "RPHE.UA87.RQD.A81",
]

_PC_FOR_RB = [
    "RPTE.UA23.RB.A12",
    "RPTE.UA27.RB.A23",
    "RPTE.UA43.RB.A34",
    "RPTE.UA47.RB.A45",
    "RPTE.UA63.RB.A56",
    "RPTE.UA67.RB.A67",
    "RPTE.UA83.RB.A78",
    "RPTE.UA87.RB.A81",
]

_PC_FOR_IT = ["RPHFC.UL14.RQX.L1", "RPMBC.UL14.RTQX1.L1", "RPHGC.UL14.RTQX2.L1"]


def _expected_signals(dir_, file, circuit_name):
    data = pkgutil.get_data(f"test.resources.pyedsl.query_builder.{dir_}", file).decode("utf-8")
    df = pd.read_csv(StringIO(data))
    return df[df["circuit_name"] == circuit_name]["signal"].tolist()


_NXCALS = [
    (
        "RQ",
        "RQD.A12",
        "PC",
        "2018-12-12 00:00:00+01:00",
        _ANY_DURATION,
        ["I_MEAS"],
        None,
        resolver.VariableQueryParams("CMW", 1544569200000000000, _ANY_DURATION, ["RPHE.UA23.RQD.A12:I_MEAS"]),
    ),
    (
        "RQ",
        [cn for cn in signal_metadata.get_circuit_names("RQ") if "D" in cn],
        "PC",
        "2018-12-12 00:00:00+01:00",
        _ANY_DURATION,
        ["I_MEAS"],
        None,
        resolver.VariableQueryParams("CMW", 1544569200000000000, _ANY_DURATION, [f"{pc}:I_MEAS" for pc in _PC_FOR_RQD]),
    ),
    (
        "IT",
        "RQX.L1",
        "PC",
        "2017-04-11 20:38:19.627",
        _ANY_DURATION,
        ["I_MEAS"],
        None,
        resolver.VariableQueryParams("CMW", 1491935899627000000, _ANY_DURATION, [f"{pc}:I_MEAS" for pc in _PC_FOR_IT]),
    ),
    (
        "RB",
        "*",
        "PC",
        "2018-12-12 00:00:00+01:00",
        _ANY_DURATION,
        ["I_MEAS"],
        None,
        resolver.VariableQueryParams("CMW", 1544569200000000000, _ANY_DURATION, [f"{pc}:I_MEAS" for pc in _PC_FOR_RB]),
    ),
    (
        "RB",
        "RB.A12",
        "BUSBAR",
        "2018-12-12 00:00:00+01:00",
        _ANY_DURATION,
        ["U_RES"],
        {"BUSBAR": "*"},
        resolver.VariableQueryParams(
            "CMW", 1544569200000000000, _ANY_DURATION, _expected_signals("nxcals_fq", "RB.A12_U_RES.csv", "RB.A12")
        ),
    ),
    (
        "RQ",
        ["RQD.A12", "RQF.A12"],
        "BUSBAR",
        "2018-12-12 00:00:00+01:00",
        _ANY_DURATION,
        ["U_RES"],
        {"BUSBAR": "*"},
        resolver.VariableQueryParams(
            "CMW",
            1544569200000000000,
            _ANY_DURATION,
            _expected_signals("nxcals_fq", "RQD.A12_RQF.A12_U_RES.csv", "RQD.A12")
            + _expected_signals("nxcals_fq", "RQD.A12_RQF.A12_U_RES.csv", "RQF.A12"),
        ),
    ),
    (
        "RB",
        "RB.A23",
        "DIODE_RB",
        1634542889913000000,
        (10, "s"),
        ["U_DIODE_RB", "U_REF_N1"],
        {"MAGNET": "magnet", "QPS_CRATE": "crate"},
        resolver.VariableQueryParams(
            "CMW", 1634542889913000000, 10000000000, ["magnet:U_DIODE_RB", "DQQDS.crate.RB.A23:U_REF_N1"]
        ),
    ),
]


@pytest.mark.parametrize("circuit_type,circuit_name,system,timestamp,duration,signals,wildcard,expected", _NXCALS)
def test_get_params_for_nxcals(circuit_type, circuit_name, system, timestamp, duration, signals, wildcard, expected):
    result = resolver.get_params_for_nxcals(
        circuit_type, circuit_name, system, timestamp, duration=duration, signals=signals, wildcard=wildcard
    )
    assert result == expected


@patch.multiple(resolver.NxcalsQueryParams, __abstractmethods__=set())
@patch.multiple(resolver.PmQueryParams, __abstractmethods__=set())
def test_assert_compatible_nxcalsQueryParams():
    with pytest.raises(resolver.ParamsNotCompatibleException) as exception:
        nxcals_qp = resolver.NxcalsQueryParams("CMW", _ANY_TIMESTAMP, _ANY_DURATION, ["I_MEAS"])
        pm_qp = resolver.PmQueryParams(_ANY_TIMESTAMP)
        nxcals_qp._assert_compatible(pm_qp)
    assert str(exception.value) == "Incompatible types: expected NxcalsQueryParams, got PmQueryParams"


@patch.multiple(resolver.NxcalsQueryParams, __abstractmethods__=set())
@patch.multiple(resolver.PmQueryParams, __abstractmethods__=set())
def test_assert_compatible_pmQueryParams():
    with pytest.raises(resolver.ParamsNotCompatibleException) as exception:
        nxcals_qp = resolver.NxcalsQueryParams("CMW", _ANY_TIMESTAMP, _ANY_DURATION, ["I_MEAS"])
        pm_qp = resolver.PmQueryParams(_ANY_TIMESTAMP)
        pm_qp._assert_compatible(nxcals_qp)
    assert str(exception.value) == "Incompatible types: expected PmQueryParams, got NxcalsQueryParams"


@patch.multiple(resolver.NxcalsQueryParams, __abstractmethods__=set())
def test_assert_compatible_pmEventQueryParams():
    with pytest.raises(resolver.ParamsNotCompatibleException) as exception:
        nxcals_qp = resolver.NxcalsQueryParams("CMW", _ANY_TIMESTAMP, _ANY_DURATION, ["I_MEAS"])
        pm_event_qp = resolver.PmEventQueryParams(_ANY_TIMESTAMP, _ANY_DURATION, [])
        pm_event_qp._assert_compatible(nxcals_qp)
    assert str(exception.value) == "Incompatible types: expected PmEventQueryParams, got NxcalsQueryParams"


_PM_SIGNALS = [
    (
        "RQ",
        "RQD.A12",
        "QH",
        1544622149620000000,
        ["U_HDS"],
        {"CELL": "16L2"},
        resolver.PmSignalQueryParams(
            1544622149620000000,
            [resolver.PmSignals(resolver.PmTriplet("QPS", "DQAMCNMQ_PMHSU", "16L2"), ["16L2:U_HDS_1", "16L2:U_HDS_2"])],
        ),
    ),
    (
        "RQ",
        "RQD.A12",
        "QH",
        1675732149620000000,
        ["I_HDS"],
        {"CELL": "16L2"},
        resolver.PmSignalQueryParams(
            1675732149620000000,
            [resolver.PmSignals(resolver.PmTriplet("QPS", "DQAMCNMQ_PMHSU", "16L2"), ["16L2:I_HDS_1", "16L2:I_HDS_2"])],
        ),
    ),
    (
        "RQ",
        ["RQD.A12", "RQF.A12"],
        "PC",
        1544622149620000000,
        ["I_MEAS"],
        None,
        resolver.PmSignalQueryParams(
            1544622149620000000,
            [
                resolver.PmSignals(resolver.PmTriplet("FGC", "51_self_pmd", "RPHE.UA23.RQD.A12"), ["STATUS.I_MEAS"]),
                resolver.PmSignals(resolver.PmTriplet("FGC", "51_self_pmd", "RPHE.UA23.RQF.A12"), ["STATUS.I_MEAS"]),
            ],
        ),
    ),
    (
        "RQ",
        "RQD.A12",
        "QDS",
        1544622149598000000,
        ["U_1_EXT", "U_2_EXT"],
        {"CELL": "16L2"},
        resolver.PmSignalQueryParams(
            1544622149598000000,
            [resolver.PmSignals(resolver.PmTriplet("QPS", "DQAMCNMQ_PMSTD", "16L2"), ["16L2:U_1_EXT", "16L2:U_2_EXT"])],
        ),
    ),
    (
        "RB",
        ["RB.A12", "RB.A23"],
        "LEADS_EVEN",
        1544622149598000000,
        ["U_HTS", "U_RES"],
        {},
        resolver.PmSignalQueryParams(
            1544622149598000000,
            [
                resolver.PmSignals(
                    resolver.PmTriplet("QPS", "DQAMGNDRBEVEN", "RB.A12"),
                    [
                        "DFLAS.7L2.RB.A12.LD1:U_HTS",
                        "DFLAS.7L2.RB.A12.LD2:U_HTS",
                        "DFLAS.7L2.RB.A12.LD1:U_RES",
                        "DFLAS.7L2.RB.A12.LD2:U_RES",
                    ],
                ),
                resolver.PmSignals(
                    resolver.PmTriplet("QPS", "DQAMGNDRBEVEN", "RB.A23"),
                    [
                        "DFLAS.7R2.RB.A23.LD1:U_HTS",
                        "DFLAS.7R2.RB.A23.LD2:U_HTS",
                        "DFLAS.7R2.RB.A23.LD1:U_RES",
                        "DFLAS.7R2.RB.A23.LD2:U_RES",
                    ],
                ),
            ],
        ),
    ),
]


@pytest.mark.parametrize("circuit_type,circuit_name,system,timestamp,signals,wildcard,expected", _PM_SIGNALS)
def test_resolve_for_pm_signals(circuit_type, circuit_name, system, timestamp, signals, wildcard, expected):
    result = resolver.get_params_for_pm_signals(
        circuit_type, circuit_name, system, timestamp, signals=signals, wildcard=wildcard
    )
    assert result == expected


_PM_EVENTS = [
    (
        "RQ",
        "RQD.A12",
        "PC",
        1544622149620000000,
        _ANY_DURATION,
        {},
        resolver.PmEventQueryParams(
            1544622149620000000, _ANY_DURATION, [resolver.PmTriplet("FGC", "51_self_pmd", "RPHE.UA23.RQD.A12")]
        ),
    ),
    (
        "IPQ2",
        "RQ10.R4",
        "PC",
        "2021-01-21 09:19:25.335000000",
        _ANY_DURATION,
        {},
        resolver.PmEventQueryParams(
            1611217165335000000,
            _ANY_DURATION,
            [
                resolver.PmTriplet("FGC", "lhc_self_pmd", "RPHGA.UA47.RQ10.R4B1"),
                resolver.PmTriplet("FGC", "lhc_self_pmd", "RPHGA.UA47.RQ10.R4B2"),
            ],
        ),
    ),
    (
        "RQ",
        "RQD.A12",
        "QH",
        1544622149620000000,
        _ANY_DURATION,
        {"CELL": "16L2"},
        resolver.PmEventQueryParams(
            1544622149620000000, _ANY_DURATION, [resolver.PmTriplet("QPS", "DQAMCNMQ_PMHSU", "16L2")]
        ),
    ),
    (
        "RQ",
        ["RQD.A12", "RQF.A12"],
        "PC",
        1544622149620000000,
        _ANY_DURATION,
        {},
        resolver.PmEventQueryParams(
            1544622149620000000,
            _ANY_DURATION,
            [
                resolver.PmTriplet("FGC", "51_self_pmd", "RPHE.UA23.RQD.A12"),
                resolver.PmTriplet("FGC", "51_self_pmd", "RPHE.UA23.RQF.A12"),
            ],
        ),
    ),
    (
        "RQ",
        ["RQD.A12", "RQF.A12"],
        "QH",
        1544622149620000000,
        (10, "ns"),
        {"CELL": "16L2"},
        resolver.PmEventQueryParams(1544622149620000000, 10, [resolver.PmTriplet("QPS", "DQAMCNMQ_PMHSU", "16L2")]),
    ),
]


@pytest.mark.parametrize("circuit_type,circuit_name,system,timestamp,duration,wildcard,expected", _PM_EVENTS)
def test_resolve_for_pm_events(circuit_type, circuit_name, system, timestamp, duration, wildcard, expected):
    result = resolver.get_params_for_pm_events(
        circuit_type, circuit_name, system, timestamp, duration=duration, wildcard=wildcard
    )
    assert result == expected


def test_pm_signal_query_params_merge():
    params = resolver.PmSignalQueryParams(
        _ANY_TIMESTAMP, [resolver.PmSignals(resolver.PmTriplet("system1", "class1", "source1"), ["signal1", "signal2"])]
    )
    other_params = resolver.PmSignalQueryParams(
        _ANY_TIMESTAMP,
        [
            resolver.PmSignals(resolver.PmTriplet("system2", "class2", "source2"), ["signal1", "signal2"]),
            resolver.PmSignals(resolver.PmTriplet("system1", "class1", "source1"), ["signal1", "signal3"]),
        ],
    )
    expected = resolver.PmSignalQueryParams(
        _ANY_TIMESTAMP,
        [
            resolver.PmSignals(resolver.PmTriplet("system1", "class1", "source1"), ["signal1", "signal2", "signal3"]),
            resolver.PmSignals(resolver.PmTriplet("system2", "class2", "source2"), ["signal1", "signal2"]),
        ],
    )

    assert expected == params.merge(other_params)


def test_pm_signal_query_params_merge_exception():
    params = resolver.PmSignalQueryParams(
        _ANY_TIMESTAMP, [resolver.PmSignals(resolver.PmTriplet("system", "class", "source"), ["I_MEAS"])]
    )
    other_params = resolver.PmSignalQueryParams(
        _ANOTHER_TIMESTAMP, [resolver.PmSignals(resolver.PmTriplet("system", "class", "source"), ["I_MEAS"])]
    )
    with pytest.raises(resolver.ParamsNotCompatibleException) as exception:
        params.merge(other_params)
    assert str(exception.value) == "PmSignalQueryParams objects must have equal timestamp to be merge-able"


def test_pm_event_query_params_merge():
    params = resolver.PmEventQueryParams(
        _ANY_TIMESTAMP,
        _ANY_DURATION,
        [resolver.PmTriplet("system1", "class1", "source1"), resolver.PmTriplet("system3", "class3", "source3")],
    )
    other_params = resolver.PmEventQueryParams(
        _ANY_TIMESTAMP,
        _ANY_DURATION,
        [resolver.PmTriplet("system1", "class1", "source1"), resolver.PmTriplet("system2", "class2", "source2")],
    )
    expected = resolver.PmEventQueryParams(
        _ANY_TIMESTAMP,
        _ANY_DURATION,
        [
            resolver.PmTriplet("system1", "class1", "source1"),
            resolver.PmTriplet("system3", "class3", "source3"),
            resolver.PmTriplet("system2", "class2", "source2"),
        ],
    )

    assert expected == params.merge(other_params)


_PM_EVENT_PARAMS_EXCEPTION = [
    (
        resolver.PmEventQueryParams(
            _ANOTHER_TIMESTAMP, _ANY_DURATION, [resolver.PmTriplet("system", "class", "source")]
        ),
        "timestamp",
    ),
    (
        resolver.PmEventQueryParams(
            _ANY_TIMESTAMP, _ANOTHER_DURATION, [resolver.PmTriplet("system", "class", "source")]
        ),
        "duration",
    ),
]


@pytest.mark.parametrize("other_params,message", _PM_EVENT_PARAMS_EXCEPTION)
def test_pm_event_query_params_merge_exception(other_params, message):
    params = resolver.PmEventQueryParams(
        _ANY_TIMESTAMP, _ANY_DURATION, [resolver.PmTriplet("system", "class", "source")]
    )
    with pytest.raises(resolver.ParamsNotCompatibleException) as exception:
        params.merge(other_params)
    assert str(exception.value) == f"PmEventQueryParams objects must have equal {message} to be merge-able"


def test_variable_query_params_merge():
    params = resolver.VariableQueryParams("CMW", _ANY_TIMESTAMP, _ANY_DURATION, ["variable1", "variable2"])
    other_params = resolver.VariableQueryParams("CMW", _ANY_TIMESTAMP, _ANY_DURATION, ["variable2", "variable3"])
    expected = resolver.VariableQueryParams(
        "CMW", _ANY_TIMESTAMP, _ANY_DURATION, ["variable1", "variable2", "variable3"]
    )

    assert expected == params.merge(other_params)


_VARIABLE_PARAMS_EXCEPTION = [
    (resolver.VariableQueryParams("WINCCOA", _ANY_TIMESTAMP, _ANY_DURATION, ["I_MEAS"])),
    (resolver.VariableQueryParams("CMW", _ANOTHER_TIMESTAMP, _ANY_DURATION, ["I_MEAS"])),
    (resolver.VariableQueryParams("CMW", _ANY_TIMESTAMP, _ANOTHER_DURATION, ["I_MEAS"])),
]


@pytest.mark.parametrize("other_params", _VARIABLE_PARAMS_EXCEPTION)
def test_variable_query_params_merge_exception(other_params):
    params = resolver.VariableQueryParams("CMW", _ANY_TIMESTAMP, _ANY_DURATION, ["ANY"])
    with pytest.raises(resolver.ParamsNotCompatibleException) as exception:
        params.merge(other_params)
    assert (
        str(exception.value) == "VariableQueryParams objects must have equal system, timestamp and duration"
        " to be merge-able"
    )
