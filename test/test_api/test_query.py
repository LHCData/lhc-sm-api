from __future__ import annotations

import json
import logging
import pathlib
from unittest.mock import MagicMock, Mock, call, patch

import pandas as pd
import pandas.testing
import pytest
from pyspark.sql.session import SparkSession
from pyspark.sql.types import StructType

from lhcsmapi.api import query, resolver
from lhcsmapi.api.resolver import PmEventQueryParams, PmSignalQueryParams, PmSignals, PmTriplet, VariableQueryParams
from lhcsmapi.Time import Time


logger = logging.getLogger(__name__)


class BuilderMock:
    def __init__(self):
        self.parameters = {}
        self.rdd = MagicMock(return_value=True)

    def system(self, system):
        self.parameters["system"] = system
        return self

    def byVariables(self):
        return self

    def byEntities(self):
        return self

    def startTime(self, start_time):
        self.parameters["start_time"] = start_time
        return self

    def duration(self, duration):
        self.parameters["duration"] = duration
        return self

    def variables(self, variables):
        self.parameters["variables"] = variables
        return self

    def entity(self):
        return self

    def device(self, device):
        self.parameters["device"] = device
        return self

    def property(self, property):
        self.parameters["property"] = property
        return self

    def build(self):
        return self

    def count(self):
        return 0


def test_query_winccoa_with_utc_timestamps(caplog: pytest.LogCaptureFixture):
    # arrange
    t_start = "2017-04-11 20:38:19.627+00:00"
    duration = 7 * 1e12
    variables = ["v1", "v2"]
    spark_mock = Mock()

    with (
        patch("lhcsmapi.api.query.transform_variables_dataset_to_pandas") as mocked_method,
        patch("nxcals.api.extraction.data.builders.DataQuery.builder") as mocked_builder,
    ):
        mocked_builder.return_value = BuilderMock()
        mocked_method.return_value = []
        # act
        query.query_winccoa_by_variables(spark_mock, t_start, duration, variables)

    resolved_parameters = mocked_builder.return_value.parameters
    # assert
    mocked_builder.assert_called_with(spark_mock)
    assert "Querying NXCALS returned no results for the" in caplog.text
    assert "WINCCOA" == resolved_parameters["system"]
    assert Time.to_unix_timestamp(t_start) == resolved_parameters["start_time"]
    assert duration == resolved_parameters["duration"]
    assert variables == resolved_parameters["variables"]


def test_query_cmw(caplog: pytest.LogCaptureFixture):
    # arrange
    t_start = "2017-04-11 20:38:19.627+02:00"
    duration = 7 * 1e12
    variables = ["v1", "v2"]
    spark_mock = Mock()

    with (
        patch("lhcsmapi.api.query.transform_variables_dataset_to_pandas") as mocked_method,
        patch("nxcals.api.extraction.data.builders.DataQuery.builder") as mocked_builder,
    ):
        mocked_builder.return_value = BuilderMock()
        mocked_method.return_value = []
        # act
        query.query_cmw_by_variables(spark_mock, t_start, duration, variables)

    resolved_parameters = mocked_builder.return_value.parameters
    # assert
    assert "Querying NXCALS returned no results for the" in caplog.text
    mocked_builder.assert_called_with(spark_mock)
    assert "CMW" == resolved_parameters["system"]
    assert Time.to_unix_timestamp(t_start) == resolved_parameters["start_time"]
    assert duration == resolved_parameters["duration"]
    assert variables == resolved_parameters["variables"]


def test_query_cmw_with_devices_and_properties(caplog: pytest.LogCaptureFixture):
    # arrange
    t_start = "2017-04-11 20:38:19.627+00:00"
    duration = 7 * 1e12
    key_values = ("device1", "property1")
    signal = "signal"
    spark_mock = Mock()

    with (
        patch("lhcsmapi.api.query.transform_signal_dataset_to_pandas") as mocked_method,
        patch("nxcals.api.extraction.data.builders.DevicePropertyDataQuery.builder") as mocked_builder,
    ):
        mocked_builder.return_value = BuilderMock()
        mocked_method.return_value = []
        # act
        query.query_cmw_by_entities(spark_mock, t_start, duration, key_values, signal)

    resolved_parameters = mocked_builder.return_value.parameters
    # assert
    assert "Querying NXCALS returned no results for the" in caplog.text
    mocked_builder.assert_called_with(spark_mock)
    assert "CMW" == resolved_parameters["system"]
    assert Time.to_unix_timestamp(t_start) == resolved_parameters["start_time"]
    assert duration == resolved_parameters["duration"]
    assert key_values[0] == resolved_parameters["device"]
    assert key_values[1] == resolved_parameters["property"]


class TestDataQuery:
    @classmethod
    def setup_class(cls):
        cls.spark = SparkSession.Builder().master("local[2]").appName("testing").getOrCreate()

    @classmethod
    def teardown_class(cls):
        cls.spark.stop()

    def test_nxcals_variable_dataframe_creation(self):
        # arrange
        variable = "v1"
        input_dataframe = self.spark.createDataFrame(
            data=[[1, 1, 1641686966042231000, variable], [2, 2, 1661686966042231000, variable]],
            schema=["nxcals_value", "nxcals_entity_id", "nxcals_timestamp", "nxcals_variable_name"],
        )

        expected_result = pd.DataFrame({variable: {1641686966042231000: 1, 1661686966042231000: 2}}).rename_axis(
            "timestamp", axis=0
        )
        # act
        actual_result = query.transform_variables_dataset_to_pandas(input_dataframe, variable)
        # assert
        pandas.testing.assert_frame_equal(expected_result, actual_result)

    def test_nxcals_signal_dataframe_creation(self):
        # arrange
        signal = "signal1"
        key_values = [("device1", "property1")]
        input_dataframe = self.spark.createDataFrame(
            data=[
                [1524960260134000000, 1524960260134000000, "class1", key_values[0][0], 1, key_values[0][1]],
                [1554960260134000000, 1554960260134000000, "class1", key_values[0][0], 2, key_values[0][1]],
            ],
            schema=["__record_timestamp__", "acqStamp", "class", "device", signal, "property"],
        )

        expected_result = pd.DataFrame(data={signal: {1524960260134000000: 1, 1554960260134000000: 2}}).rename_axis(
            "timestamp", axis=0
        )
        # act
        actual_result = query.transform_signal_dataset_to_pandas(input_dataframe, key_values, signal)
        # assert
        pandas.testing.assert_frame_equal(expected_result, actual_result)

    def test_nxcals_returns_empty_dataset(self, caplog: pytest.LogCaptureFixture):
        # arrange
        signal = "signal1"
        key_values = [("device1", "property1")]
        input_dataframe = self.spark.createDataFrame([], StructType([]))
        expected_result = pd.DataFrame({signal: pd.Series(dtype="str")})
        # act
        actual_result = query.transform_signal_dataset_to_pandas(input_dataframe, key_values, signal)
        # assert
        assert "The dataset is empty" in caplog.text
        pandas.testing.assert_frame_equal(expected_result, actual_result)

    def test_nxcals_returns_empty_variable_dataset(self, caplog: pytest.LogCaptureFixture):
        # arrange
        variable = "v1"
        input_dataframe = self.spark.createDataFrame([], StructType([]))

        expected_result = pd.DataFrame(pd.DataFrame({variable: pd.Series(dtype="str")}))
        # act
        actual_result = query.transform_variables_dataset_to_pandas(input_dataframe, variable)
        # assert
        assert "The dataset is empty" in caplog.text
        pandas.testing.assert_frame_equal(expected_result, actual_result)

    def test_transform_dataset_for_multiple_variables(self):
        # arrange
        variables = ["v1", "v2"]
        input_dataframe = self.spark.createDataFrame(
            data=[[1, 1, 1641686966042231000, variables[0]], [2, 2, 1661686966042231000, variables[1]]],
            schema=["nxcals_value", "nxcals_entity_id", "nxcals_timestamp", "nxcals_variable_name"],
        )

        expected_result = [
            pd.DataFrame({variables[0]: {1641686966042231000: 1}}).rename_axis("timestamp", axis=0),
            pd.DataFrame({variables[1]: {1661686966042231000: 2}}).rename_axis("timestamp", axis=0),
        ]
        # act
        actual_result = query.transform_variables_dataset_to_pandas(input_dataframe, variables)
        # assert
        pandas.testing.assert_frame_equal(expected_result[0], actual_result[0])
        pandas.testing.assert_frame_equal(expected_result[1], actual_result[1])


def test_query_with_variable_query_params(caplog: pytest.LogCaptureFixture):
    system = "CMW"
    timestamp = "2017-04-11 20:38:19.627+00:00"
    duration = 7 * 1e12
    variables = ["v1"]
    spark_mock = Mock()
    with (
        patch("lhcsmapi.api.query.transform_variables_dataset_to_pandas") as mocked_method,
        patch("nxcals.api.extraction.data.builders.DataQuery.builder") as mocked_builder,
    ):
        mocked_builder.return_value = BuilderMock()
        mocked_method.return_value = []
        query_params = VariableQueryParams(system, timestamp, duration, variables)
        query.query_nxcals_by_variables(spark_mock, query_params)
    resolved_parameters = mocked_builder.return_value.parameters
    mocked_builder.assert_called_with(spark_mock)

    assert "Querying NXCALS returned no results for " in caplog.text
    assert system == resolved_parameters["system"]
    assert Time.to_unix_timestamp(timestamp) == resolved_parameters["start_time"]
    assert duration == resolved_parameters["duration"]
    assert variables == resolved_parameters["variables"]


def test_query_pm_data_signals_indexed_by_timestamp():
    # act
    with open(
        pathlib.Path(__file__).parent.parent
        / "resources"
        / "dbsignal"
        / "post_mortem"
        / "1426220469520000000_FGC_51_self_pmd_RPTE.UA47.RB.A45_STATUS.I_MEAS.json"
    ) as file:
        with patch("requests.get", return_value=Mock(status_code=200, text=file.read())):
            result = query.query_pm_data_signals(
                "FGC", "51_self_pmd", "RPTE.UA47.RB.A45", "STATUS.I_MEAS", 1426220469520000000
            )

    # assert
    assert result.head().equals(
        pd.Series(
            {
                1426220409500000000: 9571.501,
                1426220409520000000: 9571.699,
                1426220409540000000: 9571.899,
                1426220409560000000: 9572.100,
                1426220409580000000: 9572.300,
            },
            name="STATUS.I_MEAS",
        ).rename_axis(index="timestamp")
    )


def test_query_pm_data_signals_correction():
    # act
    with open(
        pathlib.Path(__file__).parent.parent
        / "resources"
        / "dbsignal"
        / "post_mortem"
        / "1612795307510000000_QPS_DQAMSN600_UA83.RSD1.A78B2.json"
    ) as file:
        with patch("requests.get", return_value=Mock(status_code=200, text=file.read())):
            result = query.query_pm_data_signals(
                "QPS", "DQAMSN600", "UA83.RSD1.A78B2", "ST_FPA_REC_0", 1612795307510000000
            )

    # assert
    assert 1612795307000000000 not in result.index
    assert len(result) == 1499


def test_query_pm_data_signals_no_correction():
    # act
    with open(
        pathlib.Path(__file__).parent.parent
        / "resources"
        / "dbsignal"
        / "post_mortem"
        / "1612795307510000000_QPS_DQAMSN600_UA83.RSD1.A78B2.json"
    ) as file:
        with patch("requests.get", return_value=Mock(status_code=200, text=file.read())):
            result = query.query_pm_data_signals(
                "QPS", "DQAMSN600", "UA83.RSD1.A78B2", "ST_FPA_REC_0", 1612795307510000000, False
            )

    # assert
    assert 1612795307000000000 in result.index
    assert len(result) == 1500


def test_query_pm_data_signals_remove_indices():
    # act
    data = {1: 2, 3: 4, 2: 5, 6: 6, 5: 9}
    series = pd.Series(data, name="signal")
    expected_series = pd.Series({1: 2, 3: 4, 6: 6}, name="ST_FPA_REC_0")
    response = {
        "content": [
            {
                "namesAndValues": [
                    {"name": "circ.AQNTIME", "value": series.index.tolist()},
                    {"name": "circ.DQEMC.UA83.RSD1.A78B2:ST_FPA_REC_0", "value": series.values.tolist()},
                ]
            }
        ]
    }
    with patch("requests.get", return_value=Mock(status_code=200, text=json.dumps(response))):
        result = query.query_pm_data_signals("QPS", "DQEMC", "UA83.RSD1.A78B2", "ST_FPA_REC_0", 1612795307510000000)
    assert result.equals(expected_series)


def test_query_pm_data_signals_remove_indices_disorder():
    # act
    data = {3: 2, 1: 4, 2: 5, 4: 6, 5: 9}
    series = pd.Series(data, name="signal")
    expected_series = pd.Series({3: 2, 4: 6, 5: 9}, name="ST_FPA_REC_0")
    response = {
        "content": [
            {
                "namesAndValues": [
                    {"name": "circ.AQNTIME", "value": series.index.tolist()},
                    {"name": "circ.DQEMC.UA83.RSD1.A78B2:ST_FPA_REC_0", "value": series.values.tolist()},
                ]
            }
        ]
    }
    with patch("requests.get", return_value=Mock(status_code=200, text=json.dumps(response))):
        result = query.query_pm_data_signals("QPS", "DQEMC", "UA83.RSD1.A78B2", "ST_FPA_REC_0", 1612795307510000000)
    assert result.equals(expected_series)


def test_query_pm_data_signals_indexed_by_aqntime():
    with open(
        pathlib.Path(__file__).parent.parent
        / "resources"
        / "dbsignal"
        / "post_mortem"
        / "1426220469491000000_QPS_DQAMCNMB_PMHSU_B20L5_I_HDS_4.json"
    ) as file:
        with patch("requests.get", return_value=Mock(status_code=200, text=file.read())):
            result = query.query_pm_data_signals("QPS", "DQAMCNMB_PMHSU", "B20L5", "I_HDS_4", 1426220469491000000)

    assert result.head().equals(
        pd.Series(
            {
                1426220469491020832: 0.0896478,
                1426220469491026040: 0.0915552,
                1426220469491031248: 0.0915552,
                1426220469491036456: 0.0934626,
                1426220469491041664: 0.0915552,
            },
            name="I_HDS_4",
        ).rename_axis(index="timestamp")
    )


def test_query_pm_data_signals_indexed_by_start_time_and_interval():
    with open(
        pathlib.Path(__file__).parent.parent
        / "resources"
        / "dbsignal"
        / "post_mortem"
        / "1426220469520000000_FGC_51_self_pmd_RPTE.UA47.RB.A45_IEARTH.IEARTH.json"
    ) as file:
        with patch("requests.get", return_value=Mock(status_code=200, text=file.read())):
            result = query.query_pm_data_signals(
                "FGC", "51_self_pmd", "RPTE.UA47.RB.A45", "IEARTH.IEARTH", 1426220469520000000
            )

    assert result.head().equals(
        pd.Series(
            {
                1426220459260000000: -0.009606788,
                1426220459280000000: -0.009633326,
                1426220459300000000: -0.009633326,
                1426220459320000000: -0.009633326,
                1426220459340000000: -0.00958025,
            },
            name="IEARTH.IEARTH",
        ).rename_axis(index="timestamp")
    )


def test_query_pm_data_signals_returning_no_data(caplog: pytest.LogCaptureFixture):
    # arrange
    with patch("requests.get", return_value=Mock(status_code=204, text="")):
        # act
        signal = query.query_pm_data_signals("QPS", "DQAMCNMB_PMHSU", "B20L5", "I_HDS_3", 426220469491000000)

    # assert
    assert signal.equals(pd.Series(name="I_HDS_3").rename_axis(index="timestamp"))
    assert "Post Mortem returned no QPS data for the following query: " in caplog.messages[0]


def test_query_pm_data_signals_no_timestamp(caplog: pytest.LogCaptureFixture):

    # act
    query.query_pm_data_signals("FGC", "51_self_pmd", "RPTE.UA47.RB.A45", "STATUS.I_MEAS", None)

    # assert
    assert "Timestamp is None, therefore no signals will be queried." in caplog.messages


def test_query_pm_data_signals_no_index_information(caplog: pytest.LogCaptureFixture):
    # arrange

    with patch(
        "requests.get",
        return_value=Mock(
            status_code=200,
            text=json.dumps({"content": [{"namesAndValues": [{"name": "STATUS.I_MEAS", "value": "value"}]}]}),
        ),
    ):
        # act
        signal = query.query_pm_data_signals(
            "FGC", "51_self_pmd", "RPTE.UA47.RB.A45", "STATUS.I_MEAS", 1426220469520000000
        )

    # assert
    assert signal.empty, signal
    assert "Time not calculated." in caplog.messages, caplog.messages


def test_query_pm_data_signals_signal_name_not_in_response(caplog: pytest.LogCaptureFixture):
    # arrange
    with patch(
        "requests.get",
        return_value=Mock(
            status_code=200,
            text=json.dumps({"content": [{"namesAndValues": [{"name": "STATUS.I_MEAS", "value": "value"}]}]}),
        ),
    ):
        # act
        query.query_pm_data_signals("FGC", "51_self_pmd", "RPTE.UA47.RB.A45", "not_to_be_found", 1426220469520000000)

    # assert
    assert "Signal not_to_be_found not found in the response." in caplog.messages, caplog.messages


@pytest.mark.parametrize("duration", [24 * 60 * 60 * 1e9, (24 * 60 * 60, "s")])
def test_query_pm_data_headers(duration: int | tuple[int, str]):
    # arrange
    with open(
        pathlib.Path(__file__).parent.parent
        / "resources"
        / "dbsignal"
        / "post_mortem"
        / "1426201200000000000_86400000000000_QPS_DQAMCNMB_PMHSU.json"
    ) as file:
        with patch("requests.get", return_value=Mock(status_code=200, text=file.read())):
            # act
            source_timestamp_act_df = query.query_pm_data_headers(
                "QPS", "DQAMCNMB_PMHSU", "*", 1426201200000000000, duration
            )

    # assert
    source_timestamp_ref_df = pd.DataFrame(
        {
            "source": {
                0: "B20L5",
                1: "C20L5",
                2: "A20L5",
                3: "A21L5",
                4: "B21L5",
                5: "C23L4",
                6: "B23L4",
                7: "A23L4",
                8: "C22L4",
                9: "C15R4",
                10: "B15R4",
                11: "A15R4",
                12: "B34L8",
                13: "A34L8",
                14: "C34L8",
                15: "C33L8",
                16: "C34R7",
                17: "A34R7",
                18: "A20R3",
                19: "B20R3",
                20: "C20R3",
                21: "B18L5",
                22: "A18L5",
                23: "C18L5",
                24: "A19L5",
            },
            "timestamp": {
                0: 1426220469491000000,
                1: 1426220517100000000,
                2: 1426220518112000000,
                3: 1426220625990000000,
                4: 1426220866112000000,
                5: 1426236802332000000,
                6: 1426236839404000000,
                7: 1426236839832000000,
                8: 1426236949841000000,
                9: 1426251285711000000,
                10: 1426251337747000000,
                11: 1426251388741000000,
                12: 1426258716281000000,
                13: 1426258747370000000,
                14: 1426258747672000000,
                15: 1426258835955000000,
                16: 1426258853947000000,
                17: 1426258854113000000,
                18: 1426267931956000000,
                19: 1426267983579000000,
                20: 1426268004144000000,
                21: 1426277626360000000,
                22: 1426277679838000000,
                23: 1426277680496000000,
                24: 1426277903449000000,
            },
        }
    )

    pd.testing.assert_frame_equal(source_timestamp_ref_df, source_timestamp_act_df)


def test_query_pm_data_header_more_than_one_day_duration():
    # arrange
    with open(
        pathlib.Path(__file__).parent.parent
        / "resources"
        / "dbsignal"
        / "post_mortem"
        / "1426201200000000000_86400000000000_QPS_DQAMCNMB_PMHSU.json"
    ) as file:
        with patch("requests.get", return_value=Mock(status_code=200, text=file.read())) as mock_get:
            # act
            query.query_pm_data_headers("QPS", "DQAMCNMB_PMHSU", "*", 1426201200000000000, (49 * 60 * 60, "s"))

    # assert
    assert len(mock_get.call_args_list) == 3


def test_query_pm_data_headers_no_data(caplog: pytest.LogCaptureFixture):
    # arrange
    with patch("requests.get", return_value=Mock(status_code=204, text="")):
        # act
        pm_data_headers = query.query_pm_data_headers("test", "test", "B250L5", 23232323, 234343223)

    # assert
    pd.testing.assert_frame_equal(
        pm_data_headers,
        pd.DataFrame(columns=["source", "timestamp"], index=range(0)).astype({"source": str, "timestamp": int}),
    )
    assert "Post Mortem returned no test data for the following query: " in caplog.messages[0]


def test_query_pm_signals_with_parameters():
    # arrange
    timestamp = 1426220469520000000
    pm_signals = PmSignals(PmTriplet("FGC", "51_self_pmd", "RPTE.UA47.RB.A45"), ["STATUS.I_MEAS"])
    pm_query_params = PmSignalQueryParams(timestamp, [pm_signals])

    with open(
        pathlib.Path(__file__).parent.parent
        / "resources"
        / "dbsignal"
        / "post_mortem"
        / "1426220469520000000_FGC_51_self_pmd_RPTE.UA47.RB.A45_STATUS.I_MEAS.json"
    ) as file:
        with patch("requests.get", return_value=Mock(status_code=200, text=file.read())):
            # act
            result = query.query_pm_signals_with_resolved_params(pm_query_params)

    # assert
    i_meas_df_ref = pd.DataFrame(
        {
            "STATUS.I_MEAS": {
                1426220409500000000: 9571.501,
                1426220409520000000: 9571.699,
                1426220409540000000: 9571.899,
                1426220409560000000: 9572.100,
                1426220409580000000: 9572.300,
            }
        }
    ).rename_axis("timestamp", axis=0)

    pd.testing.assert_frame_equal(i_meas_df_ref, result.head())


def test_query_pm_events_with_parameters():
    # arrange
    timestamp = 1426201200000000000
    query_parameters = PmEventQueryParams(timestamp, 23 * 60 * 60 * 1e9, [PmTriplet("QPS", "DQAMCNMB_PMHSU", "*")])
    with open(
        pathlib.Path(__file__).parent.parent
        / "resources"
        / "dbsignal"
        / "post_mortem"
        / "1426201200000000000_86400000000000_QPS_DQAMCNMB_PMHSU.json"
    ) as file:
        with patch("requests.get", return_value=Mock(status_code=200, text=file.read())):
            # act
            source_timestamp_act_df = query.query_pm_events_with_resolved_params(query_parameters)

    # assert
    source_timestamp_ref_df = pd.DataFrame(
        {
            "source": {
                0: "B20L5",
                1: "C20L5",
                2: "A20L5",
                3: "A21L5",
                4: "B21L5",
                5: "C23L4",
                6: "B23L4",
                7: "A23L4",
                8: "C22L4",
                9: "C15R4",
                10: "B15R4",
                11: "A15R4",
                12: "B34L8",
                13: "A34L8",
                14: "C34L8",
                15: "C33L8",
                16: "C34R7",
                17: "A34R7",
                18: "A20R3",
                19: "B20R3",
                20: "C20R3",
                21: "B18L5",
                22: "A18L5",
                23: "C18L5",
                24: "A19L5",
            },
            "timestamp": {
                0: 1426220469491000000,
                1: 1426220517100000000,
                2: 1426220518112000000,
                3: 1426220625990000000,
                4: 1426220866112000000,
                5: 1426236802332000000,
                6: 1426236839404000000,
                7: 1426236839832000000,
                8: 1426236949841000000,
                9: 1426251285711000000,
                10: 1426251337747000000,
                11: 1426251388741000000,
                12: 1426258716281000000,
                13: 1426258747370000000,
                14: 1426258747672000000,
                15: 1426258835955000000,
                16: 1426258853947000000,
                17: 1426258854113000000,
                18: 1426267931956000000,
                19: 1426267983579000000,
                20: 1426268004144000000,
                21: 1426277626360000000,
                22: 1426277679838000000,
                23: 1426277680496000000,
                24: 1426277903449000000,
            },
        }
    )

    pd.testing.assert_frame_equal(source_timestamp_ref_df, source_timestamp_act_df)


def test_query_raw_pm_data():
    system = "FGC"
    class_name = "51_self_pmd"
    source = "RPTE.UA47.RB.A45"
    timestamp = 1426220469520000000
    signal_name = ["STATUS.I_MEAS"]

    with open(
        pathlib.Path(__file__).parent.parent
        / "resources"
        / "dbsignal"
        / "post_mortem"
        / "1426220469520000000_FGC_51_self_pmd_RPTE.UA47.RB.A45_STATUS.I_MEAS.json"
    ) as file:
        with patch("requests.get", return_value=Mock(status_code=200, text=file.read())):
            response = query.query_raw_pm_data(system, class_name, source, timestamp, signal_name)

    assert signal_name[0] in response


def test_query_raw_pm_data_2():
    # arrange
    timestamp = 1430431203968406625

    with open(
        pathlib.Path(__file__).parent.parent / "resources" / "dbsignal" / "post_mortem" / "pm_data_mock.json"
    ) as file:
        with patch("requests.get", return_value=Mock(status_code=200, text=file.read())):
            # act
            response = query.query_raw_pm_data("LBDS", "TSU", "LBDS.865.MKCTS.B", timestamp)

    # assert
    assert response["acquisitionStamp"] == timestamp


def test_query_pm_data():
    # arrange
    system = "LBDS"
    class_name = "TSU"
    source = "LBDS.865.MKCTS.B"
    timestamp = 1430431203968406625

    # act
    response = query.query_pm_data(system, class_name, source, timestamp)

    # assert
    assert response == {
        "acqStamp": 0,
        "acquisitionStamp": 1430431203968406625,
        "armingWatchDogError": True,
        "asyncDumpDone": True,
        "bagkDelayStatus": False,
        "bagkTriggerDelay": 2.47,
        "beamLossDetected_Cpld1": True,
        "beamLossDetected_Cpld2": True,
        "becFastDetected_Cpld1": False,
        "becFastDetected_Cpld2": False,
        "becSlowDetected_Cpld1": True,
        "becSlowDetected_Cpld2": True,
        "bisFastDetected_Cpld1": True,
        "bisFastDetected_Cpld2": True,
        "bisFreq": 9.375,
        "bisFreq_Cpld1": 8.375,
        "bisFreq_Cpld2": 8.375,
        "bisSlowDetected_Cpld1": True,
        "bisSlowDetected_Cpld2": True,
        "brfCmp1": True,
        "brfCmp2": True,
        "brfDelayFault": False,
        "brfDetected": True,
        "brfSyncFault": False,
        "clientDumpRequested": False,
        "clientsOk_Cpld1": False,
        "clientsOk_Cpld2": False,
        "delayedPllFreqCmp1": True,
        "delayedPllFreqCmp2": True,
        "delayedPllOutputDetected": True,
        "dumpDone": True,
        "dumpRequestStamp": 1430431203968331975,
        "dumpRequestStamp2": 1430431203968331925,
        "dumpTriggerDelay": 36.0,
        "injectAndDumpDetected_Cpld1": True,
        "injectAndDumpDetected_Cpld2": True,
        "internalFault": False,
        "plcArming": True,
        "pllComputedCtrlWord": 282,
        "pllCtrlState": 2,
        "pllDphiDtTime": 0.0,
        "pllInCatchState": True,
        "pllInPullInRange": False,
        "pllInPullOutRange": False,
        "pllLockEnabled": True,
        "pllLocked": True,
        "pllLocked1": True,
        "pllLocked2": True,
        "pllNextNcoFrequency": 11.236081,
        "pllNextNcoPeriod": 88.999,
        "pllOutputDetected": True,
        "pllPdState": 1,
        "pllPdTime": 0.0,
        "pllPhiErrorTime": 13.08,
        "pllPresentCtrlWord": 282,
        "pllPresentNcoFrequency": 11.236081,
        "pllPresentNcoPeriod": 88.999,
        "pllPreviousCtrlWord": 282,
        "pllPreviousPhiErrorTime": 13.08,
        "remoteCtrl": True,
        "reset_Cpld1": False,
        "reset_Cpld2": False,
        "scssDetected_Cpld1": True,
        "scssDetected_Cpld2": True,
        "secondSyncFault": False,
        "secondTsuDelayedPllOutputDetected": True,
        "syncDumpEnabled": True,
        "syncDumpRequested": False,
        "syncFault": False,
        "triggerDelayStatus": True,
        "triggerWatchDogError": True,
        "tsuInternalDumpDetected_Cpld1": True,
        "tsuInternalDumpDetected_Cpld2": True,
        "tsuReady": True,
        "tsuState": 1024,
    }


def test_query_pm_data_no_response():
    # arrange
    system = "LBDS"
    class_name = "TSU"
    source = "LBDS.865.MKCTS.B"
    timestamp = 1

    # act
    response = query.query_pm_data(system, class_name, source, timestamp)

    # assert
    assert response == {}


def test_query_pm_data_no_timestamp(caplog: pytest.LogCaptureFixture):
    # arrange
    system = "LBDS"
    class_name = "TSU"
    source = "LBDS.865.MKCTS.B"
    timestamp = None

    # act
    response = query.query_pm_data(system, class_name, source, timestamp)

    # assert
    assert response == {}
    assert caplog.messages == ["Timestamp is None, therefore no query will be performed."]


@pytest.mark.parametrize(
    "query_params,old_column_name,new_column_name",
    [
        (resolver.PmSignalQueryParams(1677583560000000001, []), "RD1.L2:U_RES_B1", "RD1.L2:U_RES"),
        (resolver.VariableQueryParams("any", 1677583560000000001, 1, []), "RD1.L2:U_RES_B1", "RD1.L2:U_RES"),
        (resolver.PmSignalQueryParams(1677583550000000000, []), "RD1.L2:U_RES_B1", "RD1.L2:U_RES_B1"),
        (resolver.VariableQueryParams("any", 1677583550000000000, 1, []), "RD1.L2:U_RES_B1", "RD1.L2:U_RES_B1"),
        (resolver.PmSignalQueryParams(1677583560000000001, []), "RD1.L8:U_HDS_1_B1", "RD1.L8:U_HDS_1"),
        (resolver.VariableQueryParams("any", 1677583560000000001, 1, []), "RD1.L8:U_HDS_1_B2", "RD1.L8:U_HDS_2"),
        (resolver.PmSignalQueryParams(1677583550000000000, []), "RD1.L8:U_HDS_1_B2", "RD1.L8:U_HDS_1_B2"),
        (resolver.VariableQueryParams("any", 1677583550000000000, 1, []), "RD1.L8:U_HDS_1_B2", "RD1.L8:U_HDS_1_B2"),
        (resolver.VariableQueryParams("any", 1777583550000000000, 1, []), "RD2.L4:U_HDS_1_B2", "RD2.L4:U_HDS_1_B2"),
    ],
)
def test_rd1_very_dirty_fix(
    query_params: resolver.PmSignalQueryParams | resolver.VariableQueryParams,
    old_column_name: str,
    new_column_name: str,
):
    query_result = pd.DataFrame({old_column_name: []})

    @query._rd1_very_dirty_fix()
    def decorated(_):
        return query_result

    assert decorated(query_params).columns[0] == new_column_name


def test_events_not_duplicated():
    system = "QPS"
    class_name = "DQAMGNRQA"
    source = "RQ10.L5"
    timestamp = 1425696413600000000
    duration = 4000000000
    query_result = query.query_pm_data_headers(system, class_name, source, timestamp, duration)
    pd.testing.assert_frame_equal(
        query_result,
        pd.DataFrame({"source": ["RQ10.L5", "RQ10.L5"], "timestamp": [1425696415588000000, 1425696415589000000]}),
    )


@patch("lhcsmapi.api.query.builders.DataQuery.builder")
def test_query_nxcals_by_variables(mock_dataquery_builder, caplog: pytest.LogCaptureFixture):
    # arrange
    spark = MagicMock()
    system = "test_system"
    start_time = 0
    duration = 1000
    variables = ["var1", "var2"]
    max_latest_data_search_period = 4_000_000_000

    mock_dataquery_builder.return_value = mock_dataquery_builder
    mock_dataquery_builder.byVariables.return_value = mock_dataquery_builder
    mock_dataquery_builder.system.return_value = mock_dataquery_builder
    mock_dataquery_builder.startTime.return_value = mock_dataquery_builder
    mock_dataquery_builder.duration.return_value = mock_dataquery_builder
    mock_dataquery_builder.variables.return_value = mock_dataquery_builder
    mock_dataquery_builder.build.return_value.rdd.isEmpty.return_value = True

    caplog.set_level(logging.DEBUG)

    # act
    result = query._query_nxcals_by_variables(
        spark, system, start_time, duration, variables, True, max_latest_data_search_period
    )

    # assert
    assert isinstance(result, list)
    assert mock_dataquery_builder.method_calls == [
        call.byVariables(),
        call.system("test_system"),
        call.startTime(0),
        call.duration(1000),
        call.variables(["var1", "var2"]),
        call.build(),
        call.byVariables(),
        call.system("test_system"),
        call.startTime(-1000000000),
        call.duration(1000000000),
        call.variables(["var1"]),
        call.build(),
        call.byVariables(),
        call.system("test_system"),
        call.startTime(-3000000000),
        call.duration(2000000000),
        call.variables(["var1"]),
        call.build(),
        call.byVariables(),
        call.system("test_system"),
        call.startTime(-4000000000),
        call.duration(1000000000),
        call.variables(["var1"]),
        call.build(),
        call.byVariables(),
        call.system("test_system"),
        call.startTime(-1000000000),
        call.duration(1000000000),
        call.variables(["var2"]),
        call.build(),
        call.byVariables(),
        call.system("test_system"),
        call.startTime(-3000000000),
        call.duration(2000000000),
        call.variables(["var2"]),
        call.build(),
        call.byVariables(),
        call.system("test_system"),
        call.startTime(-4000000000),
        call.duration(1000000000),
        call.variables(["var2"]),
        call.build(),
    ]
    assert caplog.messages == [
        "Querying NXCALS for ['var1', 'var2'] with system test_system, duration 1000 and start time 0",
        "Querying NXCALS with p_start=-1000000000, p_end=0 for variable var1.",
        "No data found, doubling the time window.",
        "Querying NXCALS with p_start=-3000000000, p_end=-1000000000 for variable var1.",
        "No data found, doubling the time window.",
        "Querying NXCALS with p_start=-4000000000, p_end=-3000000000 for variable var1.",
        "No data found, doubling the time window.",
        "Querying NXCALS returned no results while trying to query the latest data point prior to start: system: test_system, variable: var1, start_time: 0, max_latest_data_point_search_period: 4,000,000,000",
        "Querying NXCALS with p_start=-1000000000, p_end=0 for variable var2.",
        "No data found, doubling the time window.",
        "Querying NXCALS with p_start=-3000000000, p_end=-1000000000 for variable var2.",
        "No data found, doubling the time window.",
        "Querying NXCALS with p_start=-4000000000, p_end=-3000000000 for variable var2.",
        "No data found, doubling the time window.",
        "Querying NXCALS returned no results while trying to query the latest data point prior to start: system: test_system, variable: var2, start_time: 0, max_latest_data_point_search_period: 4,000,000,000",
        "Querying NXCALS returned no results for the variable var1: system: test_system, start_time: 0, duration: 1000, include_latest_data_point_prior_to_start: True, max_latest_data_point_search_period: 4,000,000,000",
        "Querying NXCALS returned no results for the variable var2: system: test_system, start_time: 0, duration: 1000, include_latest_data_point_prior_to_start: True, max_latest_data_point_search_period: 4,000,000,000",
    ]
