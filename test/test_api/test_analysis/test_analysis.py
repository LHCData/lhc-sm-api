import unittest
from unittest.mock import patch

from pyspark.sql.session import SparkSession
import logging
from datetime import datetime

from lhcsmapi.api.analysis.analysis import Analysis, AnalysisManager


def create_log_record(msg, level, tags):
    record = logging.LogRecord(name="mock_logger", level=level, pathname="", lineno=0, msg=msg, args=(), exc_info=None)
    record.created = datetime.now().timestamp()
    record.tags = tags
    return record


class AnalysisTest(unittest.TestCase):
    @patch.multiple(Analysis, __abstractmethods__=set())
    def setUp(self):
        self.a = Analysis("id")
        self.a._logs = [
            create_log_record("Starting analysis", logging.INFO, {"init", "visible"}),
            create_log_record("User input received", logging.INFO, {"user_action", "invisible"}),
            create_log_record("Calculation performed", logging.INFO, {"calculation", "invisible"}),
            create_log_record("Error encountered", logging.ERROR, {"error", "visible", "user_action"}),
            create_log_record("Finalizing", logging.INFO, {"finalize", "invisible"}),
            create_log_record("Analysis complete", logging.INFO, {"complete", "visible"}),
        ]

    def test_get_filtered_logs_multiple_filters_multiple_results(self):
        # act
        filtered_logs = self.a.get_filtered_logs("visible", "user_action")
        # assert
        self.assertEqual(filtered_logs, "Starting analysis\nUser input received\nError encountered\nAnalysis complete")

    def test_get_filtered_logs_multiple_filters_one_result(self):
        # act
        filtered_logs = self.a.get_filtered_logs("init", "abc")
        # assert
        self.assertEqual(filtered_logs, "Starting analysis")

    def test_get_filtered_logs_multiple_filters_no_result(self):
        # act
        filtered_logs = self.a.get_filtered_logs("abc")
        # assert
        self.assertEqual(filtered_logs, "")


class AnalysisManagerTest(unittest.TestCase):
    @patch.multiple(Analysis, __abstractmethods__=set())
    def setUp(self):
        a1 = Analysis("id1")
        a1._logs = [
            create_log_record("Starting analysis", logging.INFO, {"init", "visible"}),
            create_log_record("User input received", logging.INFO, {"user_action", "invisible"}),
            create_log_record("Calculation performed", logging.INFO, {"calculation", "invisible"}),
            create_log_record("Error encountered", logging.ERROR, {"error", "visible", "user_action"}),
            create_log_record("Finalizing", logging.INFO, {"finalize", "invisible"}),
            create_log_record("Analysis complete", logging.INFO, {"complete", "visible"}),
        ]
        a2 = Analysis("id2")
        a2._logs = [
            create_log_record("Starting analysis", logging.INFO, {"init", "new_tag"}),
            create_log_record("Recalculating", logging.INFO, {"user_action", "visible"}),
        ]

        self.analysis_manager = AnalysisManager()
        self.analysis_manager.register_analysis(a1)
        self.analysis_manager.register_analysis(a2)

    def test_get_filtered_logs_multiple_filters_multiple_results_in_all_analyses(self):
        # act
        filtered_logs = self.analysis_manager.get_filtered_logs("visible", "user_action")
        # assert
        self.assertEqual(
            filtered_logs["id1"], "Starting analysis\nUser input received\nError encountered\nAnalysis complete"
        )
        self.assertEqual(filtered_logs["id2"], "Recalculating")
        self.assertEqual(self.analysis_manager.get_registered_analyses_identifiers(), ["id1", "id2"])

    def test_get_filtered_logs_multiple_filters_result_only_one_analysis(self):
        # act
        filtered_logs = self.analysis_manager.get_filtered_logs("new_tag", "abc")
        # assert
        self.assertEqual(filtered_logs["id2"], "Starting analysis")
        self.assertEqual(filtered_logs["id1"], "")
        self.assertEqual(self.analysis_manager.get_registered_analyses_identifiers(), ["id1", "id2"])

    def test_get_filtered_logs_multiple_filters_no_results(self):
        # act
        filtered_logs = self.analysis_manager.get_filtered_logs("def", "abc")
        # assert
        self.assertEqual(filtered_logs["id1"], "")
        self.assertEqual(filtered_logs["id2"], "")
        self.assertEqual(self.analysis_manager.get_registered_analyses_identifiers(), ["id1", "id2"])
