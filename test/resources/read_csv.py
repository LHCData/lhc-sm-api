import os
from pathlib import Path
import pandas as pd


def read_csv(circuit_type, signal_name):
    path = Path(os.path.dirname(__file__))
    full_path = os.path.join(path.parent, "{}/{}.csv".format(circuit_type, signal_name))
    return pd.read_csv(full_path, index_col=0)
