import unittest
import warnings
from datetime import UTC, date, datetime
from unittest.mock import Mock, patch

import numpy as np
import pandas as pd

from lhcsmapi.Time import Time as Time


class TestTime(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

        # region convertToUnixTime

    def test_convertToUnixTimestamp_realNumberInput_utcTimeZone_returnsCorrectTimestamp(self):
        """Tests that convertToUnixTimestamp returns the correct integer (ns unixTimestamp) when passed real number values"""

        # arrange:
        testCases = [
            (1, None, 1),
            (1, "ns", 1),
            (1.0, None, 1),
            (1.0, "ns", 1),
            (1.32, "ns", 1),
            (1363149019000000001, "ns", 1363149019000000001),
            (1.528374951891e18, "ns", 1528374951891000000),
            (1534598315.006001, "s", 1534598315006001000),
            (1, "us", int(1e3)),
            (1.0, "us", int(1e3)),
            (1, "ms", int(1e6)),
            (1.0, "ms", int(1e6)),
            (1.32, "ms", int(1.32e6)),
            (1, "s", int(1e9)),
            (100, "s", int(100e9)),
            (1.0, "s", int(1e9)),
            (1.2, "s", int(1.2e9)),
        ]

        for inputValue, unit, unixTimeComparison in testCases:
            with self.subTest(name=str(inputValue)):
                # act
                unixTime = Time.to_unix_timestamp(inputValue, unit=unit, tz="UTC")
                # assert
                self.assertEqual(unixTime, unixTimeComparison)

    def test_convertToUnixTimestamp_stringInput_utcTimeZone_returnsCorrectTimestamp(self):
        """Tests that convertToUnixTimestamp returns the correct integer (ns unixTimestamp) when passed strings"""

        # arrange:
        testCases = [
            # different precisions
            ("1970-01-01 00:00:00.000000001", None, 1),
            ("1970-01-01 00:00:00.000001", None, 1000),
            ("1970-01-01 00:00:00.001", None, int(1e6)),
            ("1970-01-01 00:00:01", None, int(1e9)),
            ("1970-01-01 00:01:00", None, int(60e9)),
            ("1970-01-01 01:00:00", None, int(3600e9)),
            ("2019-06-17 11:41:11.75", None, 1560771671750000000),
            ("2019-06-17 11:41:11.758001", None, 1560771671758001000),
            ("2019-06-17 11:41:11.758001000", None, 1560771671758001000),
            ("2019-06-17 13:41:11.758001000+02:00", None, 1560771671758001000),
            # different decades and utc offsets
            ("1981-06-20 06:59:02.000000004", None, 361868342000000004),
            ("1981-06-20 06:59:02.000000004+00:00", None, 361868342000000004),
            ("1981-06-20 08:59:02.000000004+02:00", None, 361868342000000004),
            ("2013-03-13 04:30:19.000000001", None, 1363149019000000001),
            ("2013-03-13 04:30:19.000000001+00:00", None, 1363149019000000001),
            ("2013-03-13 05:30:19.000000001+01:00", None, 1363149019000000001),
            ("2027-01-01 11:00:01.000010011", None, 1798801201000010011),
            ("2027-01-01 11:00:01.000010011+00:00", None, 1798801201000010011),
            ("2027-01-01 12:00:01.000010011+01:00", None, 1798801201000010011),
        ]

        for inputString, unit, unixTimeToCompare in testCases:
            with self.subTest(name=str(inputString)):
                # act

                unixTimestamp = Time.to_unix_timestamp(inputString, unit=unit, tz="UTC")

                # assert
                self.assertEqual(unixTimestamp, unixTimeToCompare)

    def test_convertToUnixTimestamp_datetimeInput_utcTimeZone_returnsCorrectTimestamp(self):
        """Tests that convertToUnixTimestamp returns the correct integer (ns unixTimestamp) when passed datetime"""

        # arrange:
        testCases = [
            # different precisions
            (datetime(1970, 1, 1, 0, 0, 0, 0), 0),
            (datetime(1970, 1, 1, 0, 0, 0, 1), 1000),
            (datetime(1970, 1, 1, 0, 0, 0, 1000), int(1e6)),
            (datetime(1970, 1, 1, 0, 0, 1, 0), int(1e9)),
            (datetime(1970, 1, 1, 0, 1, 0, 0), int(60e9)),
            (datetime(1970, 1, 1, 1, 0, 0, 0), int(3600e9)),
            (datetime(1981, 6, 20, 6, 59, 2, 0), 361868342000000000),
            (datetime(2013, 3, 13, 4, 30, 19, 0), 1363149019000000000),
            (datetime(2019, 6, 17, 11, 41, 11, 750000), 1560771671750000000),
            (datetime(2027, 1, 1, 11, 00, 1, 10), 1798801201000010000),
        ]

        for dtInput, unixTimeToCompare in testCases:
            with self.subTest(name=str(dtInput)):
                # act
                unixTimestamp = Time.to_unix_timestamp(dtInput, tz="UTC")

                # assert
                self.assertEqual(unixTimestamp, unixTimeToCompare)

    def test_convertToUnixTimestamp_ZurichTimeZone_returnsCorrectTimestamp(self):
        """Tests that convertToUnixTimestamp eturns the correct integer (ns unixTimestamp) when other time zone is used"""

        # arrange:
        testCases = [
            (1, "ns", 1),
            ("1970-01-01 01:00:00.000000001", None, 1),
            (datetime(1970, 1, 1, 1, 0, 1, 0), None, int(1e9)),
        ]

        for inputValue, unit, unixTimeToCompare in testCases:
            with self.subTest(name=str(inputValue)):
                # act
                unixTimestamp = Time.to_unix_timestamp(inputValue, unit=unit, tz="Europe/Zurich")

                # assert
                self.assertEqual(unixTimestamp, unixTimeToCompare)

        # endregion

    # region to_unix_timestamp_in_sec
    def test_to_unix_timestamp_in_sec_string_input_utc_time_zone_returns_correct_timestamp(self):
        """Tests that convertToUnixTimestamp returns the correct integer (ns unix_timestamp) when passed strings"""

        # arrange:
        test_cases = [
            # different precisions
            ("1970-01-01 00:00:00.000000001", None, 0.0),
            ("1970-01-01 00:00:00.000001", None, 1e-6),
            ("1970-01-01 00:00:00.001", None, 0.001),
            ("1970-01-01 00:00:01", None, 1.0),
            ("1970-01-01 00:01:00", None, 60.0),
            ("1970-01-01 01:00:00", None, 3600.0),
            ("2019-06-17 11:41:11.75", None, 1560771671.7500002),
            ("2019-06-17 11:41:11.758001", None, 1560771671.7580009),
            ("2019-06-17 11:41:11.758001000", None, 1560771671.7580009),
            ("2019-06-17 13:41:11.758001000+02:00", None, 1560771671.7580009),
            # different decades and utc offsets
            ("1981-06-20 06:59:02.000000004", None, 361868342.0),
            ("1981-06-20 06:59:02.000000004+00:00", None, 361868342.0),
            ("1981-06-20 08:59:02.000000004+02:00", None, 361868342.0),
            ("2013-03-13 04:30:19.000000001", None, 1363149019.0),
            ("2013-03-13 04:30:19.000000001+00:00", None, 1363149019.0),
            ("2013-03-13 05:30:19.000000001+01:00", None, 1363149019.0),
            ("2027-01-01 11:00:01.000010011", None, 1798801201.00001),
            ("2027-01-01 11:00:01.000010011+00:00", None, 1798801201.00001),
            ("2027-01-01 12:00:01.000010011+01:00", None, 1798801201.00001),
        ]

        for input_string, unit, unix_time_to_compare in test_cases:
            with self.subTest(name=str(input_string)):
                # act
                unix_timestamp = Time.to_unix_timestamp_in_sec(input_string, unit=unit, tz="UTC")

                # assert
                self.assertEqual(unix_timestamp, unix_time_to_compare)

    def test_to_unix_timestamp_in_sec_string_input_ns_timestamp_warning(self):
        # arrange:
        timestamp = 1363149019000000001

        # act
        with warnings.catch_warnings(record=True) as w:
            Time.to_unix_timestamp_in_sec(timestamp, warn=True)
            if w:
                self.assertEqual("Discarding nonzero 1 nanoseconds in conversion", str(w[0].message))

        # assert

    # end region

    # region to_pandas_timestamp

    def test_convertToPandasTimestamp_realNumberInput_utcTimeZone_returnsCorrectTimestamp(self):
        """Tests that to_pandas_timestamp returns the correct pandas Timestamp when passed real number values"""

        # arrange:
        test_cases = [
            (1, None, 1),
            (1, "ns", 1),
            (1.0, None, 1),
            (1.0, "ns", 1),
            (1.32, "ns", 1),
            (1363149019000000001, "ns", 1363149019000000001),
            (1.528374951891e18, "ns", 1528374951891000000),
            (1, "us", int(1e3)),
            (1.0, "us", int(1e3)),
            (1, "ms", int(1e6)),
            (1.0, "ms", int(1e6)),
            (1.32, "ms", int(1.32e6)),
            (1, "s", int(1e9)),
            (100, "s", int(100e9)),
            (1.0, "s", int(1e9)),
            (1.2, "s", int(1.2e9)),
            (1534597650.006001, "s", 1534597650006001000),
        ]

        for input_value, unit, unix_time in test_cases:
            with self.subTest(name=str(input_value)):
                # act
                pd_timestamp = Time.to_pandas_timestamp(input_value, unit=unit, tz="UTC")
                pd_timestamp_comparison = pd.Timestamp(unix_time, tz="UTC")

                # assert
                self.assertEqual(pd_timestamp, pd_timestamp_comparison)

    def test_convertToPandasTimestamp_stringInput_utcTimeZone_returnsCorrectTimestamp(self):
        """Tests that convertToPandasTimestamp returns the correct pandas Timestamp when passed strings"""

        # arrange:
        testCases = [
            # different precisions
            ("1970-01-01 00:00:00.000000001", 1),
            ("1970-01-01 00:00:00.000001", 1000),
            ("1970-01-01 00:00:00.001", int(1e6)),
            ("1970-01-01 00:00:01", int(1e9)),
            ("1970-01-01 00:01:00", int(60e9)),
            ("1970-01-01 01:00:00", int(3600e9)),
            ("2019-06-17 11:41:11.75", 1560771671750000000),
            ("2019-06-17 11:41:11.758001", 1560771671758001000),
            ("2019-06-17 11:41:11.758001000", 1560771671758001000),
            ("2019-06-17 13:41:11.758001000+02:00", 1560771671758001000),
            # different decades and utc offsets
            ("1981-06-20 06:59:02.000000004", 361868342000000004),
            ("1981-06-20 06:59:02.000000004+00:00", 361868342000000004),
            ("1981-06-20 08:59:02.000000004+02:00", 361868342000000004),
            ("2013-03-13 04:30:19.000000001", 1363149019000000001),
            ("2013-03-13 04:30:19.000000001+00:00", 1363149019000000001),
            ("2013-03-13 05:30:19.000000001+01:00", 1363149019000000001),
            ("2027-01-01 11:00:01.000010011", 1798801201000010011),
            ("2027-01-01 11:00:01.000010011+00:00", 1798801201000010011),
            ("2027-01-01 12:00:01.000010011+01:00", 1798801201000010011),
        ]

        for inputString, unixTime in testCases:
            with self.subTest(name=str(inputString)):
                # act

                pdTimestamp = Time.to_pandas_timestamp(inputString, tz="UTC")
                pdTimestampComparison = pd.Timestamp(unixTime, tz="UTC")

                # assert
                self.assertEqual(pdTimestamp, pdTimestampComparison)

    def test_convertToPandasTimestamp_datetimeInput_utcTimeZone_returnsCorrectTimestamp(self):
        """Tests that convertToUnixTimestamp returns the correct integer (ns unixTimestamp) when passed datetime"""

        # arrange:
        testCases = [
            # different precisions
            (datetime(1970, 1, 1, 0, 0, 0, 0), 0),
            (datetime(1970, 1, 1, 0, 0, 0, 1), 1000),
            (datetime(1970, 1, 1, 0, 0, 0, 1000), int(1e6)),
            (datetime(1970, 1, 1, 0, 0, 1, 0), int(1e9)),
            (datetime(1970, 1, 1, 0, 1, 0, 0), int(60e9)),
            (datetime(1970, 1, 1, 1, 0, 0, 0), int(3600e9)),
            (datetime(1981, 6, 20, 6, 59, 2, 0), 361868342000000000),
            (datetime(2013, 3, 13, 4, 30, 19, 0), 1363149019000000000),
            (datetime(2019, 6, 17, 11, 41, 11, 750000), 1560771671750000000),
            (datetime(2027, 1, 1, 11, 00, 1, 10), 1798801201000010000),
        ]

        for dtInput, unixTime in testCases:
            with self.subTest(name=str(dtInput)):
                # act

                pdTimestamp = Time.to_datetime(dtInput, tz="UTC")
                pdTimestampComparison = pd.Timestamp(unixTime, tz="UTC")

                # assert
                self.assertEqual(pdTimestamp, pdTimestampComparison)

    def test_convertToPandasTimestamp_ZurichTimeZone_returnsCorrectTimestamp(self):
        """Tests that convertToPandasTimestamp returns the correct pandas Timestamp when other time zone is used"""

        # arrange:
        # arrange:
        testCases = [
            (1, "ns", pd.Timestamp(1, tz="Europe/Zurich")),
            ("1970-01-01 01:00:00.000000001", None, pd.Timestamp(1, tz="Europe/Zurich")),
            (datetime(1970, 1, 1, 1, 0, 1, 0), None, pd.Timestamp(int(1e9), tz="Europe/Zurich")),
        ]

        for inputValue, unit, pdTimestampComparison in testCases:
            with self.subTest(name=str(inputValue)):
                # act

                pdTimestamp = Time.to_pandas_timestamp(inputValue, unit=unit, tz="Europe/Zurich")

                # assert
                self.assertEqual(pdTimestamp, pdTimestampComparison)

        # endregion

        # region convertString

    def test_convertToString_realNumberInput_utcTimeZone_returnsCorrectTimestamp(self):
        """Tests that convertToString returns the correct string when passed real number values"""

        # arrange:
        testCases = [
            (1, None, "1970-01-01 00:00:00.000000001+00:00"),
            (1, "ns", "1970-01-01 00:00:00.000000001+00:00"),
            (1.0, None, "1970-01-01 00:00:00.000000001+00:00"),
            (1.0, "ns", "1970-01-01 00:00:00.000000001+00:00"),
            (1.32, "ns", "1970-01-01 00:00:00.000000001+00:00"),
            (1363149019000000001, "ns", "2013-03-13 04:30:19.000000001+00:00"),
            (1.528374951891e18, "ns", "2018-06-07 12:35:51.891000+00:00"),
            (1, "us", "1970-01-01 00:00:00.000001+00:00"),
            (1.0, "us", "1970-01-01 00:00:00.000001+00:00"),
            (1, "ms", "1970-01-01 00:00:00.001000+00:00"),
            (1.0, "ms", "1970-01-01 00:00:00.001000+00:00"),
            (1.32, "ms", "1970-01-01 00:00:00.001320+00:00"),
            (1, "s", "1970-01-01 00:00:01+00:00"),
            (100, "s", "1970-01-01 00:01:40+00:00"),
            (100.0, "s", "1970-01-01 00:01:40+00:00"),
            (1.0, "s", "1970-01-01 00:00:01+00:00"),
            (1.2, "s", "1970-01-01 00:00:01.200000+00:00"),
        ]

        for inputValue, unit, comparisonString in testCases:
            with self.subTest(name=str(inputValue)):
                # act

                outputString = Time.to_string(inputValue, unit=unit, tz="UTC")

                # assert
                self.assertEqual(outputString, comparisonString)

    def test_convertToString_stringInput_utcTimeZone_returnsCorrectTimestamp(self):
        """Tests that convertToString returns a correctly formatted string when parsed another string"""
        # arrange:

        testCases = [
            # different precisions
            ("1970-01-01 00:00:00.000000001", "1970-01-01 00:00:00.000000001+00:00"),
            ("1970-01-01 00:00:00.000001", "1970-01-01 00:00:00.000001+00:00"),
            ("1970-01-01 00:00:00.001", "1970-01-01 00:00:00.001000+00:00"),
            ("1970-01-01 00:00:01", "1970-01-01 00:00:01+00:00"),
            ("1970-01-01 00:01:00", "1970-01-01 00:01:00+00:00"),
            ("1970-01-01 01:00:00", "1970-01-01 01:00:00+00:00"),
            ("2019-06-17 11:41:11.75", "2019-06-17 11:41:11.750000+00:00"),
            ("2019-06-17 11:41:11.758001", "2019-06-17 11:41:11.758001+00:00"),
            ("2019-06-17 11:41:11.758001000", "2019-06-17 11:41:11.758001+00:00"),
            ("2019-06-17 13:41:11.758001000+02:00", "2019-06-17 11:41:11.758001+00:00"),
            # different decades and utc offsets
            ("1981-06-20 06:59:02.000000004", "1981-06-20 06:59:02.000000004+00:00"),
            ("1981-06-20 06:59:02.000000004+00:00", "1981-06-20 06:59:02.000000004+00:00"),
            ("1981-06-20 08:59:02.000000004+02:00", "1981-06-20 06:59:02.000000004+00:00"),
            ("2013-03-13 04:30:19.000000001", "2013-03-13 04:30:19.000000001+00:00"),
            ("2013-03-13 04:30:19.000000001+00:00", "2013-03-13 04:30:19.000000001+00:00"),
            ("2013-03-13 05:30:19.000000001+01:00", "2013-03-13 04:30:19.000000001+00:00"),
            ("2027-01-01 11:00:01.000010011", "2027-01-01 11:00:01.000010011+00:00"),
            ("2027-01-01 11:00:01.000010011+00:00", "2027-01-01 11:00:01.000010011+00:00"),
            ("2027-01-01 12:00:01.000010011+01:00", "2027-01-01 11:00:01.000010011+00:00"),
        ]

        for inputString, comparisonString in testCases:
            with self.subTest(name=str(inputString)):
                # act

                outputString = Time.to_string(inputString, tz="UTC")

                # assert
                self.assertEqual(outputString, comparisonString)

    def test_convertToString_datetimeInput_utcTimeZone_returnsCorrectTimestamp(self):
        """Tests that convertToString returns the correct string when passed datetime"""

        # arrange:
        testCases = [
            # different precisions
            ("1970-01-01 00:00:00+00:00", datetime(1970, 1, 1, 0, 0, 0, 0)),
            ("1970-01-01 00:00:00.000001+00:00", datetime(1970, 1, 1, 0, 0, 0, 1)),
            ("1970-01-01 00:00:00.001000+00:00", datetime(1970, 1, 1, 0, 0, 0, 1000)),
            ("1970-01-01 00:00:01+00:00", datetime(1970, 1, 1, 0, 0, 1, 0)),
            ("1970-01-01 00:01:00+00:00", datetime(1970, 1, 1, 0, 1, 0, 0)),
            ("1970-01-01 01:00:00+00:00", datetime(1970, 1, 1, 1, 0, 0, 0)),
            ("2019-06-17 11:41:11.750000+00:00", datetime(2019, 6, 17, 11, 41, 11, 750000)),
            ("2019-06-17 11:41:11.758001+00:00", datetime(2019, 6, 17, 11, 41, 11, 758001)),
            # different decades and utc offsets
            ("1981-06-20 06:59:02+00:00", datetime(1981, 6, 20, 6, 59, 2, 0)),
            ("2013-03-13 04:30:19+00:00", datetime(2013, 3, 13, 4, 30, 19, 0)),
            ("2027-01-01 11:00:01.000010+00:00", datetime(2027, 1, 1, 11, 0, 1, 10)),
        ]

        for comparisonString, dt in testCases:
            with self.subTest(name=str(dt)):
                # act

                outputString = Time.to_string(dt, tz="UTC")

                # assert
                self.assertEqual(outputString, comparisonString)

    def test_convertToString_ZurichTimeZone_returnsCorrectTimestamp(self):
        """Tests that convertToString returns the correct timestamp string when time zone is used"""

        # arrange:
        # arrange:
        testCases = [
            (1, "ns", "1970-01-01 01:00:00.000000001+01:00"),
            ("1970-01-01 01:00:00.000000001", None, "1970-01-01 01:00:00.000000001+01:00"),
            (datetime(1970, 1, 1, 1, 0, 1, 0), None, "1970-01-01 01:00:01+01:00"),
        ]

        for inputValue, unit, timestampStringComparison in testCases:
            with self.subTest(name=str(inputValue)):
                # act
                timestampString = Time.to_string(inputValue, unit=unit, tz="Europe/Zurich")

                # assert
                self.assertEqual(timestampString, timestampStringComparison)

        # endregion

        # region convertToDateTime

    def test_convert_to_date_time_real_number_input_utc_time_zone_returns_correct_timestamp(self):
        """Tests that convertToDatetime returns the correct datetime when passed real number values"""

        # arrange:
        for inputValue, unit, dt_comparison in (
            (1, None, datetime(1970, 1, 1, 0, 0, 0, 0)),
            (1, "ns", datetime(1970, 1, 1, 0, 0, 0, 0)),
            (1.0, None, datetime(1970, 1, 1, 0, 0, 0, 0)),
            (1.0, "ns", datetime(1970, 1, 1, 0, 0, 0, 0)),
            (1.32, "ns", datetime(1970, 1, 1, 0, 0, 0, 0)),
            (1363149019000000001, "ns", datetime(2013, 3, 13, 4, 30, 19, 0)),
            (1.528374951891e18, "ns", datetime(2018, 6, 7, 12, 35, 51, 891000)),
            (1, "us", datetime(1970, 1, 1, 0, 0, 0, 1)),
            (1.0, "us", datetime(1970, 1, 1, 0, 0, 0, 1)),
            (1, "ms", datetime(1970, 1, 1, 0, 0, 0, 1000)),
            (1.0, "ms", datetime(1970, 1, 1, 0, 0, 0, 1000)),
            (1.32, "ms", datetime(1970, 1, 1, 0, 0, 0, 1320)),
            (1, "s", datetime(1970, 1, 1, 0, 0, 1, 0)),
            (100, "s", datetime(1970, 1, 1, 0, 1, 40, 0)),
            (1.0, "s", datetime(1970, 1, 1, 0, 0, 1, 0)),
            (1.2, "s", datetime(1970, 1, 1, 0, 0, 1, 200000)),
        ):
            with self.subTest(name=str(inputValue)):
                # act
                with warnings.catch_warnings(record=True) as w:
                    dt = Time.to_datetime(inputValue, unit=unit, tz="UTC")
                    dt_comparison = dt_comparison.replace(tzinfo=UTC)
                    if w:
                        self.assertEqual(len(w), 1)
                        self.assertTrue(str(w[0].message) == "Discarding nonzero nanoseconds in conversion.")

                # assert
                self.assertEqual(dt, dt_comparison)

    def test_convert_to_datetime_string_input_utc_time_zone_returns_correct_timestamp(self):
        """Tests that convertToDatetime returns the correct datetime when passed strings"""

        # arrange:
        for input_string, dt_comparison in (
            # different precisions
            ("1970-01-01 00:00:00.000000001", datetime(1970, 1, 1, 0, 0, 0, 0)),
            ("1970-01-01 00:00:00.000001", datetime(1970, 1, 1, 0, 0, 0, 1)),
            ("1970-01-01 00:00:00.001", datetime(1970, 1, 1, 0, 0, 0, 1000)),
            ("1970-01-01 00:00:01", datetime(1970, 1, 1, 0, 0, 1, 0)),
            ("1970-01-01 00:01:00", datetime(1970, 1, 1, 0, 1, 0, 0)),
            ("1970-01-01 01:00:00", datetime(1970, 1, 1, 1, 0, 0, 0)),
            ("2019-06-17 11:41:11.75", datetime(2019, 6, 17, 11, 41, 11, 750000)),
            ("2019-06-17 11:41:11.758001", datetime(2019, 6, 17, 11, 41, 11, 758001)),
            ("2019-06-17 11:41:11.758001000", datetime(2019, 6, 17, 11, 41, 11, 758001)),
            ("2019-06-17 13:41:11.758001000+02:00", datetime(2019, 6, 17, 11, 41, 11, 758001)),
            # different decades and utc offsets
            ("1981-06-20 06:59:02.000000004", datetime(1981, 6, 20, 6, 59, 2, 0)),
            ("1981-06-20 06:59:02.000000004+00:00", datetime(1981, 6, 20, 6, 59, 2, 0)),
            ("1981-06-20 08:59:02.000000004+02:00", datetime(1981, 6, 20, 6, 59, 2, 0)),
            ("2013-03-13 04:30:19.000000001", datetime(2013, 3, 13, 4, 30, 19, 0)),
            ("2013-03-13 04:30:19.000000001+00:00", datetime(2013, 3, 13, 4, 30, 19, 0)),
            ("2013-03-13 05:30:19.000000001+01:00", datetime(2013, 3, 13, 4, 30, 19, 0)),
            ("2027-01-01 11:00:01.000010011", datetime(2027, 1, 1, 11, 00, 1, 10)),
            ("2027-01-01 11:00:01.000010011+00:00", datetime(2027, 1, 1, 11, 00, 1, 10)),
            ("2027-01-01 12:00:01.000010011+01:00", datetime(2027, 1, 1, 11, 00, 1, 10)),
        ):
            with self.subTest(name=str(input_string)):
                # act
                with warnings.catch_warnings(record=True) as w:
                    dt = Time.to_datetime(input_string, tz="UTC")
                    dt_comparison = dt_comparison.replace(tzinfo=UTC)

                    if w:
                        self.assertEqual(len(w), 1)
                        self.assertTrue(str(w[0].message) == "Discarding nonzero nanoseconds in conversion.")

                # assert
                self.assertEqual(dt, dt_comparison)

    def test_convertToDatetime_datetimeInput_utcTimeZone_returnsCorrectTimestamp(self):
        """Tests that convertToDatetime returns the correct datetime when passed datetimes"""

        # arrange:
        for dtComparison in (
            # different precisions
            datetime(1970, 1, 1, 0, 0, 0, 0),
            datetime(1970, 1, 1, 0, 0, 0, 1),
            datetime(1970, 1, 1, 0, 0, 0, 1000),
            datetime(1970, 1, 1, 0, 0, 1, 0),
            datetime(1970, 1, 1, 0, 1, 0, 0),
            datetime(1970, 1, 1, 1, 0, 0, 0),
            datetime(2019, 6, 17, 11, 41, 11, 750000),
            datetime(2019, 6, 17, 11, 41, 11, 758001),
            # different decades and utc offsets
            datetime(1981, 6, 20, 6, 59, 2, 0),
            datetime(2013, 3, 13, 4, 30, 19, 0),
            datetime(2027, 1, 1, 11, 00, 1, 10),
        ):
            with self.subTest(name=str(dtComparison)):
                # act
                dt = Time.to_datetime(dtComparison, tz="UTC")
                dtComparison = dtComparison.replace(tzinfo=UTC)

                # assert
                self.assertEqual(dt, dtComparison)

        # endregion

        # region getQueryPeriodInUnixTime tests

    def test_getQueryPeriodInUnixTime_startTimeAndEndTimeProvided_tzSetToZurich_covertsToUnixTime(self):
        """Tests that getQueryPeriodInUnixTime returns the right start and t_end if they are both provided. Essentially
        just converts both to unix time"""

        # arrange:
        test_cases = [
            ("2018-08-18 15:07:30.006000", "2018-08-18 15:30:32.006000", 1534597650006000000, 1534599032006000000)
        ]

        for start_time_data, end_time_data, ref_start_time, ref_end_time in test_cases:
            with self.subTest():
                # act
                start_time, end_time = Time.get_query_period_in_unix_time(
                    start_time_data, end_time_data, None, tz="Europe/Zurich"
                )

                # assert
                self.assertEqual(start_time, ref_start_time)
                self.assertEqual(end_time, ref_end_time)

    def test_getQueryPeriodInUnixTime_singleDurationTupleProvided_tzSetToZurich_calculatesEndTime(self):
        """Tests that getQueryPeriodInUnixTime returns the right start and t_end if t_start and a single duration component is provided."""

        # arrange:
        testCases = [
            ("2018-08-18 15:07:30.006000", (2, "s"), 1534597650006000000, 1534597652006000000),
            ("2018-08-18 15:07:30.006000", (60 * 30, "s"), 1534597650006000000, 1534599450006000000),
        ]

        for startTimeData, durationData, refStartTime, refEndTime in testCases:
            with self.subTest():
                # act
                startTime, endTime = Time.get_query_period_in_unix_time(
                    startTimeData, None, durationData, tz="Europe/Zurich"
                )

                # assert
                self.assertEqual(startTime, refStartTime)
                self.assertEqual(endTime, refEndTime)

    def test_getQueryPeriodInUnixTime_singleDurationTupleProvidedInList_tzSetToZurich_calculatesEndTime(self):
        """Tests that getQueryPeriodInUnixTime returns the right start and t_end if t_start and a single duration component is provided
        in list format."""

        # arrange:
        testCases = [
            ("2018-08-18 15:07:30.006000", [(2, "s")], 1534597650006000000, 1534597652006000000),
            ("2018-08-18 15:07:30.006000", [(60 * 30, "s")], 1534597650006000000, 1534599450006000000),
        ]

        for startTimeData, durationData, refStartTime, refEndTime in testCases:
            with self.subTest():
                # act
                startTime, endTime = Time.get_query_period_in_unix_time(
                    startTimeData, None, durationData, tz="Europe/Zurich"
                )

                # assert
                self.assertEqual(startTime, refStartTime)
                self.assertEqual(endTime, refEndTime)

    def test_getQueryPeriodInUnixTime_TwoDurationTupleProvidedInList_tzSetToZurich_calculatesBothStartAndEndTime(self):
        """Tests that getQueryPeriodInUnixTime returns the right start and t_end if t_start and two duration components are provided
        in list format."""

        # arrange:
        test_cases = [
            ("2018-08-18 15:07:30.006000", [(2, "s"), (2, "s")], 1534597648006000000, 1534597652006000000),
            ("2018-08-18 15:07:30.006000", [(60 * 30, "s"), (60 * 30, "s")], 1534595850006000000, 1534599450006000000),
        ]

        for start_time_data, duration_data, ref_start_time, ref_end_time in test_cases:
            with self.subTest():
                # act
                start_time, end_time = Time.get_query_period_in_unix_time(
                    start_time_data, None, duration_data, tz="Europe/Zurich"
                )

                # assert
                self.assertEqual(ref_start_time, start_time)
                self.assertEqual(ref_end_time, end_time)

    def test_get_query_period_in_unix_time_error(self):
        # arrange
        t_start = None
        t_end = None
        duration = None

        # act
        # assert
        with self.assertRaises(ValueError):
            Time.get_query_period_in_unix_time(t_start, t_end, duration, tz="Europe/Zurich")

    def test_moduloDayInUnixTimestamp_error(self):
        # arrange
        start_time = "2018-12-13 00:00:00"
        end_time = "2018-12-12 00:00:00"

        # act
        # assert
        with self.assertRaises(AttributeError):
            Time.modulo_day_in_unix_timestamp(start_time, end_time)

    def test_moduloDayInUnixTimestamp_less_than_a_day(self):
        # arrange
        start_time = "2018-12-12 00:00:00"
        end_time = "2018-12-12 01:00:00"

        # act
        quotient_act, remainder_act = Time.modulo_day_in_unix_timestamp(start_time, end_time)

        # assert
        quotient_exp = 0
        remainder_exp = int(1 * 60 * 60 * 1e9)
        self.assertEqual(quotient_exp, quotient_act)
        self.assertEqual(remainder_act, remainder_exp)

    def test_moduloDayInUnixTimestamp_single_day(self):
        # arrange
        start_time = "2018-12-12 00:00:00"
        endTime = "2018-12-13 00:00:00"

        # act
        quotient_act, remainderAct = Time.modulo_day_in_unix_timestamp(start_time, endTime)

        # assert
        quotient_exp = 1
        remainder_exp = 0
        self.assertEqual(quotient_exp, quotient_act)
        self.assertEqual(remainderAct, remainder_exp)

    def test_moduloDayInUnixTimestamp_two_days(self):
        # arrange
        start_time = "2018-12-12 00:00:00"
        end_time = "2018-12-14 00:00:00"

        # act
        quotient_act, remainder_act = Time.modulo_day_in_unix_timestamp(start_time, end_time)

        # assert
        quotient_exp = 2
        remainder_exp = 0
        self.assertEqual(quotient_exp, quotient_act)
        self.assertEqual(remainder_act, remainder_exp)

    def test_moduloDayInUnixTimestamp_two_days_and_a_half(self):
        # arrange
        start_time = "2018-12-12 00:00:00"
        end_time = "2018-12-14 12:00:00"

        # act
        quotient_act, remainder_act = Time.modulo_day_in_unix_timestamp(start_time, end_time)

        # assert
        quotient_exp = 2
        remainder_exp = int(12 * 60 * 60 * 1e9)
        self.assertEqual(quotient_exp, quotient_act)
        self.assertEqual(remainder_act, remainder_exp)

    def test_timezone_conversion(self):
        # arrange
        date_exp = "2018-12-12 00:00:00+01:00"

        # act
        timestamp = Time.to_unix_timestamp(date_exp)
        date_act = Time.to_string(timestamp)

        # assert
        self.assertEqual(date_exp, date_act)

    def test_to_string_short_ns(self):
        # arrange
        date = 1544558893966000000

        # act
        date_str_short_act = Time.to_string_short(date)

        # assert
        date_str_short_exp = "2018-12-11 21:08:13.966"

        self.assertEqual(date_str_short_exp, date_str_short_act)

    def test_to_string_short_us(self):
        # arrange
        date = 1544558893966000

        # act
        date_str_short_act = Time.to_string_short(date, "us")

        # assert
        date_str_short_exp = "2018-12-11 21:08:13.966"

        self.assertEqual(date_str_short_exp, date_str_short_act)

    def test_to_string_short_ns_3_ms_digits(self):
        # arrange
        date = 1544558893900000000

        # act
        date_str_short_act = Time.to_string_short(date, "ns")

        # assert
        date_str_short_exp = "2018-12-11 21:08:13.900"

        self.assertEqual(date_str_short_exp, date_str_short_act)

    def test_to_string_short_ns_0_ms_digits(self):
        # arrange
        date = 0

        # act
        date_str_short_act = Time.to_string_short(date, "ns")

        # assert
        date_str_short_exp = "1970-01-01 01:00:00"

        self.assertEqual(date_str_short_exp, date_str_short_act)

    def test_to_string_short_ns_nan(self):
        # arrange
        date = float("nan")

        # act
        date_str_short_act = Time.to_string_short(date, "ns")

        # assert
        date_str_short_exp = ""

        self.assertEqual(date_str_short_exp, date_str_short_act)

    def test_to_string_short_ps_0s(self):
        # arrange
        date = 0

        # act
        # assert
        with self.assertRaises(ValueError) as context:
            Time.to_string_short(date, "ps")

        self.assertEqual("cannot cast unit ps", str(context.exception))

    def test_to_string_ns_nan(self):
        # arrange
        date = float("nan")

        # act
        date_str_short_act = Time.to_string(date, "ns")

        # assert
        date_str_short_exp = ""

        self.assertEqual(date_str_short_exp, date_str_short_act)

    def test_to_string_ns_np_array(self):
        # arrange
        date = np.array([1])

        # act
        # assert
        with self.assertRaises(TypeError) as context:
            Time.to_string(date, "ns")

        self.assertEqual(
            "Cannot convert input <class 'numpy.ndarray'> to string formatted timestamp", str(context.exception)
        )

    @patch("lhcsmapi.Time.datetime")
    def test_get_analysis_start_time(self, datetime_mock):
        # arrange
        datetime_mock.now = Mock(return_value=datetime(2020, 12, 16, 12, 38, 11, 508734))

        # act
        analysis_start_time_act = Time.get_analysis_start_time()

        # assert
        analysis_start_time_exp = "2020-12-16-12h38"
        self.assertEqual(analysis_start_time_exp, analysis_start_time_act)

    @patch("lhcsmapi.Time.datetime")
    def test_now(self, datetime_mock):
        # arrange
        datetime_mock.now = Mock(return_value=datetime(2020, 12, 16, 12, 38, 11, 508734))

        # act
        now_act = Time.now()

        # assert
        now_exp = datetime(2020, 12, 16, 12, 38, 11, 508734)
        self.assertEqual(now_exp, now_act)

    @patch("lhcsmapi.Time.time.sleep", return_value=None)
    def test_my_method(self, patched_time_sleep):
        Time.sleep(60)  # Should be instant

        # the mock should only be called once
        self.assertEqual(1, patched_time_sleep.call_count)

    # alternative version using a context manager
    def test_sleep(self):
        with patch("lhcsmapi.Time.time.sleep", return_value=None) as patched_time_sleep:
            Time.sleep(60)  # Should be instant

        # the mock should only be called once
        self.assertEqual(1, patched_time_sleep.call_count)

    def test_daterange_error(self):
        # arrange
        start_date = date(2014, 1, 5)
        end_date = date(2014, 1, 3)

        # act
        # assert
        with self.assertRaises(StopIteration):
            next(Time.daterange(start_date, end_date))

    def test_daterange_ok(self):
        # arrange
        start_date = date(2014, 1, 5)
        end_date = date(2014, 1, 6)

        # act
        daterange_act = next(Time.daterange(start_date, end_date))

        # assert
        daterange_exp = date(2014, 1, 5)
        self.assertEqual(daterange_exp, daterange_act)
