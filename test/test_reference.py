import re
from pathlib import Path
from unittest.mock import patch

import pandas as pd
import pytest

from lhcsmapi import reference
from lhcsmapi.metadata import signal_metadata

PARAMS_DISCHARGE = [
    ("RQ", "16L2", 1417079205509000000, 1417079205409000000),
    ("RB", "B24L5", 1424018504015000000, 1424018504014000000),
    ("RB", "C31R2", 1618509304343999990, 1422019689013000000),
    ("RB", "B24L5", 1611330989888000000, 1424018504014000000),
    ("RB", "B24L5", 1611330989888000010, 1611330989888000000),
    ("RB", "B24L5", 1424018504014000010, 1424018504014000000),
]


@pytest.mark.parametrize("circuit_type,cell,timestamp,timestamp_exp", PARAMS_DISCHARGE)
def test_get_quench_heater_reference_discharge(circuit_type, cell, timestamp, timestamp_exp):
    # act
    timestamp_act = reference.get_quench_heater_reference_discharge(circuit_type, cell, timestamp)
    # assert
    assert timestamp_exp == timestamp_act


def test_get_quench_heater_reference_discharge_before_reference():
    """This test checks the retrieval of a QH reference for input timestamp before the first reference.
    A warning should be displayed and the first timestamp returned"""
    # arrange
    circuit_type = "RB"
    magnet_name = "B24L5"
    timestamp = 1424018504014000000 - 10

    # act
    # assert
    timestamp_ref = 1424018504014000000
    with pytest.warns(UserWarning, match="The given is older than timestamp reference QH discharge!"):
        timestamp_act = reference.get_quench_heater_reference_discharge(circuit_type, magnet_name, timestamp)

    assert timestamp_ref == timestamp_act


def test_get_quench_heater_reference_discharge_error():
    # act
    with pytest.raises(ValueError) as exception:
        reference.get_quench_heater_reference_discharge("600A", "", 1)
    # assert
    assert "Circuit type 600A not supported!" == str(exception.value)


def test_get_energy_extraction_reference_features_rb():
    # arrange
    circuit_type = "RB"

    # act
    features_df_act = reference.get_energy_extraction_reference_features(circuit_type)

    # assert
    features_df_ref = pd.DataFrame(
        {
            "min": {
                "R_odd": 0.0675,
                "R_even": 0.0675,
                "t_delay_ee_odd": 0.05,
                "t_delay_ee_even": 0.55,
                "tau_u_dump_res_odd": 110.0,
                "tau_u_dump_res_even": 110.0,
            },
            "max": {
                "R_odd": 0.0825,
                "R_even": 0.0825,
                "t_delay_ee_odd": 0.15,
                "t_delay_ee_even": 0.65,
                "tau_u_dump_res_odd": 130.0,
                "tau_u_dump_res_even": 130.0,
            },
        }
    )
    pd.testing.assert_frame_equal(features_df_ref, features_df_act)


def test_get_energy_extraction_reference_features_error():
    # arrange
    circuit_type = "600A"

    # act
    with pytest.raises(KeyError) as exception:
        reference.get_energy_extraction_reference_features(circuit_type)

    # assert
    assert "'Circuit type 600A not supported!'" == str(exception.value)


def test_get_power_converter_reference_features_rb():
    # arrange
    circuit_type = "RB"

    # act
    features_df_act = reference.get_power_converter_reference_features(circuit_type)

    # assert
    features_df_ref = pd.DataFrame({"min": {"tau_i_meas": 90}, "max": {"tau_i_meas": 110}})
    pd.testing.assert_frame_equal(features_df_ref, features_df_act)


def test_get_power_converter_reference_features_error():
    # arrange
    circuit_type = "IT"

    # act
    with pytest.raises(ValueError) as exception:
        reference.get_power_converter_reference_features(circuit_type)

    # assert
    assert "Reference features for IT circuit type are not defined." == str(exception.value)


def test_get_power_converter_reference_fpa():
    # arrange
    circuit_type = "RB"
    circuit_name = "RB.A12"

    # act
    fgc_pm_act = reference.get_power_converter_reference_fpa(circuit_type, circuit_name, "fgcPm", 1521369783000000000)

    # assert
    assert fgc_pm_act == 1521298828680000000


def test_get_power_converter_reference_fpa_before_reference():
    # arrange
    circuit_type = "RQ"
    circuit_name = "RQD.A12"

    # act
    with pytest.warns(UserWarning, match=re.escape("The given is older than timestamp reference fgcPm!")):
        fgc_pm_act = reference.get_power_converter_reference_fpa(
            circuit_type, circuit_name, "fgcPm", 1521369783000000000
        )

    # assert
    assert fgc_pm_act == 1521370713600000000


def test_get_power_converter_reference_fpa_it_error():
    # arrange
    circuit_type = "IT"
    circuit_name = "RQX.L1"

    # act
    with pytest.raises(ValueError) as exception:
        reference.get_power_converter_reference_fpa(circuit_type, circuit_name, "fgcPm", 1635489452000000000)

    # assert
    assert "Circuit type IT not supported!" == str(exception.value)


PARAMS_PERIODS = [
    ("2018-01-01 00:00:00.000+01:00", "eYETS 2017/2018"),
    ("2021-01-01 00:00:00.000+01:00", "LS2"),
    ("2023-03-07 00:00:00.000+01:00", "HWC 2023"),
    ("2100-01-01 00:00:00.000+01:00", reference._UNKNOWN_MP3_PERIOD),
    ("1969-01-01 00:00:00.000+01:00", reference._UNKNOWN_MP3_PERIOD),
]


@pytest.mark.parametrize("date_time, expected_period", PARAMS_PERIODS)
def test_get_mp3_period(date_time, expected_period):
    actual_period = reference.get_mp3_period(date_time)
    assert expected_period == actual_period


# Nominal voltage-dependent QH discharge reference criteria - 900 V
def test_read_quench_heaters_reference_features_rb():
    # arrange
    circuit_type = "RB"

    # act
    features_act = reference.get_quench_heaters_reference_features(circuit_type)

    # assert
    features_ref = pd.DataFrame(
        {
            "min": {
                "%CELL%:U_HDS_1:first": 780,
                "%CELL%:U_HDS_2:first": 780,
                "%CELL%:U_HDS_3:first": 780,
                "%CELL%:U_HDS_4:first": 780,
                "%CELL%:U_HDS_1:last20mean": 15,
                "%CELL%:U_HDS_2:last20mean": 15,
                "%CELL%:U_HDS_3:last20mean": 15,
                "%CELL%:U_HDS_4:last20mean": 15,
            },
            "max": {
                "%CELL%:U_HDS_1:first": 980,
                "%CELL%:U_HDS_2:first": 980,
                "%CELL%:U_HDS_3:first": 980,
                "%CELL%:U_HDS_4:first": 980,
                "%CELL%:U_HDS_1:last20mean": 85,
                "%CELL%:U_HDS_2:last20mean": 85,
                "%CELL%:U_HDS_3:last20mean": 85,
                "%CELL%:U_HDS_4:last20mean": 85,
            },
        }
    )
    pd.testing.assert_frame_equal(features_ref, features_act)


def test_get_rq_circuit_parameters():
    # arrange
    # act
    rq_circuit_parameters = reference.get_rq_circuit_parameters()

    # assert
    rq_circuit_parameters_ref = pd.DataFrame(
        {
            "Circuit name": {
                0: "RQD/F.A12",
                1: "RQD/F.A23",
                2: "RQD/F.A34",
                3: "RQD/F.A45",
                4: "RQD/F.A56",
                5: "RQD/F.A67",
                6: "RQD/F.A78",
                7: "RQD/F.A81",
            },
            "Number of apertures": {0: 47, 1: 51, 2: 51, 3: 47, 4: 47, 5: 51, 6: 51, 7: 47},
            "Beam 1": {0: "Ext", 1: "Int", 2: "Int", 3: "Int", 4: "Ext", 5: "Ext", 6: "Ext", 7: "Int"},
            "Beam 2": {0: "Int", 1: "Ext", 2: "Ext", 3: "Ext", 4: "Int", 5: "Int", 6: "Int", 7: "Ext"},
            "RQF B1/B2": {
                0: "Odd/Even",
                1: "Even/Odd",
                2: "Odd/Even",
                3: "Even/Odd",
                4: "Odd/Even",
                5: "Even/Odd",
                6: "Odd/Even",
                7: "Even/Odd",
            },
            "RQD B1/B2": {
                0: "Even/Odd",
                1: "Odd/Even",
                2: "Even/Odd",
                3: "Odd/Even",
                4: "Even/Odd",
                5: "Odd/Even",
                6: "Even/Odd",
                7: "Odd/Even",
            },
            "Inductance [H]": {0: 0.2744, 1: 0.2856, 2: 0.2856, 3: 0.2744, 4: 0.2744, 5: 0.2856, 6: 0.2856, 7: 0.2744},
            "EE Resistance [ohm]": {
                0: 0.0066,
                1: 0.0077,
                2: 0.0077,
                3: 0.0066,
                4: 0.0066,
                5: 0.0077,
                6: 0.0077,
                7: 0.0066,
            },
        }
    )
    pd.testing.assert_frame_equal(rq_circuit_parameters_ref, rq_circuit_parameters)


_END_OF_PREVIOUS_LS_PARAMS = [
    ("2008-12-06", "2007-12-06 00:00:00"),
    ("2014-08-12", "2014-07-01 00:00:00"),
    ("2021-01-03", "2020-11-01 00:00:00"),
    ("2019-03-01T08:43:40.662+0000", "2014-07-01 00:00:00"),
    ("2021-01-03T00:00:00", "2020-11-01 00:00:00"),
    ("2021-01-03T00:00:01", "2020-11-01 00:00:00"),
    ("2022-01-04 10:00:01", "2020-11-01 00:00:00"),
]


@pytest.mark.parametrize("start_time,expected", _END_OF_PREVIOUS_LS_PARAMS)
def test_get_end_of_previous_ls(start_time, expected):
    result = reference._get_end_of_previous_ls(start_time)
    assert result == expected


@pytest.fixture(name="hwc_summary")
def fixture_hwc_summary():
    path = Path(__file__).parent / "resources" / "reference" / "test_hwc_summary.csv"
    return signal_metadata.get_hwc_summary(path)


@pytest.mark.parametrize(
    "t_start, strategy, expected",
    [
        (
            "2023-02-26 14:33:46",
            reference.ReferenceTestSelectionStrategy.LAST,
            ("2023-02-17 11:06:41.486000000", "2023-02-17 11:07:06.137000000"),
        ),
        (
            "2023-02-26 14:33:46",
            reference.ReferenceTestSelectionStrategy.FIRST_AFTER_LS,
            ("2022-02-18 09:14:21.915000000", "2022-02-18 09:15:46.109000000"),
        ),
        (
            "2016-03-01 17:44:25.246000000",
            reference.ReferenceTestSelectionStrategy.LAST,
            ("2008-02-12 14:40:31.474000000", "2008-02-12 15:00:38.630000000"),
        ),
        (
            "2008-02-26 14:16:31.306000000",
            reference.ReferenceTestSelectionStrategy.LAST,
            ("2008-02-12 14:40:31.474000000", "2008-02-12 15:00:38.630000000"),
        ),
        (
            "2023-02-26 14:33:46",
            reference.ReferenceTestSelectionStrategy.FIRST_AFTER_THERMOCYCLE,
            ("2022-02-18 09:14:21.915000000", "2022-02-18 09:15:46.109000000"),
        ),
    ],
)
def test_get_reference_test(
    t_start: str,
    strategy: reference.ReferenceTestSelectionStrategy,
    expected: tuple[str, str],
    hwc_summary: pd.DataFrame,
):
    with patch("lhcsmapi.reference.MappingMetadata.get_sector_name_for_circuit_name", return_value="S78"):
        result = reference.get_reference_test(hwc_summary, "dummy_circuit", "DUMMY_TEST_1", t_start, strategy)
    assert result == expected


@pytest.mark.parametrize(
    "start_time,strategy",
    [
        ("2016-03-01 17:44:25.246000000", reference.ReferenceTestSelectionStrategy.FIRST_AFTER_LS),
        ("2000-02-26 14:16:31.306000000", reference.ReferenceTestSelectionStrategy.LAST),
        ("2008-02-12 14:40:31.474000000", reference.ReferenceTestSelectionStrategy.LAST),
        ("2000-02-26 14:16:31.306000000", reference.ReferenceTestSelectionStrategy.FIRST_AFTER_LS),
        ("2021-03-26 14:16:31.306000000", reference.ReferenceTestSelectionStrategy.FIRST_AFTER_THERMOCYCLE),
    ],
)
def test_exception_get_reference_test(
    start_time: str, strategy: reference.ReferenceTestSelectionStrategy, hwc_summary: pd.DataFrame
):
    with (
        patch("lhcsmapi.reference.MappingMetadata.get_sector_name_for_circuit_name", return_value="S78"),
        pytest.raises(KeyError) as exception,
    ):
        reference.get_reference_test(hwc_summary, "dummy_circuit", "DUMMY_TEST_1", start_time, strategy)
    assert (
        f"'No reference test according to the strategy {strategy} for given circuit name dummy_circuit, "
        f"hwc test DUMMY_TEST_1 and start time {start_time[:23]}'" == str(exception.value)
    )
