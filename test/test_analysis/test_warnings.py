import unittest

from lhcsmapi.analysis.warnings import warning_on_one_line


class TestWarnings(unittest.TestCase):
    def test_warning_on_one_line(self):
        # arrange
        warning_message = "Missing file"

        # act
        formatted_warning_act = warning_on_one_line(warning_message, "", "", "")

        # assert
        self.assertEqual("Missing file\n", formatted_warning_act)
