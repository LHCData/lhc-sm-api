import unittest
from pathlib import Path
import os
import pandas as pd

import lhcsmapi.signal_analysis.features as signal_analysis
import lhcsmapi.signal_analysis.functions as signal_analysis_functions
import lhcsmapi.analysis.features_helper as utility_features


def read_csv(file_name):
    path = Path(os.path.dirname(__file__))
    return os.path.join(str(path.parent.parent), "resources/qh_rq/{}".format(file_name))


class TestQuenchHeaterAnalysisRQ(unittest.TestCase):
    def setUp(self):
        self.u_hds_1_df = pd.read_csv(read_csv("16L2_U_HDS_1.csv"), index_col=0)
        self.u_hds_2_df = pd.read_csv(read_csv("16L2_U_HDS_2.csv"), index_col=0)
        self.u_hds_1_ref_df = pd.read_csv(read_csv("16L2_U_HDS_1_REF.csv"), index_col=0)
        self.u_hds_2_ref_df = pd.read_csv(read_csv("16L2_U_HDS_2_REF.csv"), index_col=0)

    def test_calculate_initial_final_voltage(self):
        # arrange
        timestamp = 1544622149598000000

        row_feature_df = signal_analysis.calculate_features(
            [self.u_hds_1_df, self.u_hds_2_df], [utility_features.first, utility_features.last20mean], timestamp
        )
        # assert
        u_hds_1_initial = 880.46210
        u_hds_2_initial = 872.83540
        u_hds_1_final = 5.5750855263157915
        u_hds_2_final = 6.734703347368421

        self.assertAlmostEqual(u_hds_1_initial, row_feature_df.loc[timestamp, "16L2:U_HDS_1:first"], places=5)
        self.assertAlmostEqual(u_hds_2_initial, row_feature_df.loc[timestamp, "16L2:U_HDS_2:first"], places=5)
        self.assertAlmostEqual(u_hds_1_final, row_feature_df.loc[timestamp, "16L2:U_HDS_1:last20mean"], places=5)
        self.assertAlmostEqual(u_hds_2_final, row_feature_df.loc[timestamp, "16L2:U_HDS_2:last20mean"], places=5)

    def test_calculate_initial_final_voltage_ref(self):
        # arrange
        timestamp_ref = 1417079205409000000

        row_feature_ref_df = signal_analysis.calculate_features(
            [self.u_hds_1_ref_df, self.u_hds_2_ref_df],
            [utility_features.first, utility_features.last20mean],
            timestamp_ref,
        )
        # assert
        u_hds_1_initial_ref = 880.74457
        u_hds_2_initial_ref = 875.942569
        u_hds_1_final_ref = 6.541433684210526
        u_hds_2_final_ref = 8.384928710526315

        self.assertAlmostEqual(
            u_hds_1_initial_ref, row_feature_ref_df.loc[timestamp_ref, "16L2:U_HDS_1:first"], places=5
        )
        self.assertAlmostEqual(
            u_hds_2_initial_ref, row_feature_ref_df.loc[timestamp_ref, "16L2:U_HDS_2:first"], places=5
        )
        self.assertAlmostEqual(
            u_hds_1_final_ref, row_feature_ref_df.loc[timestamp_ref, "16L2:U_HDS_1:last20mean"], places=5
        )
        self.assertAlmostEqual(
            u_hds_2_final_ref, row_feature_ref_df.loc[timestamp_ref, "16L2:U_HDS_2:last20mean"], places=5
        )

    def test_calculate_characteristic_time(self):
        # arrange
        timestamp = 1544622149598000000

        # act
        row_feature_df = signal_analysis.calculate_features(
            [self.u_hds_1_df, self.u_hds_2_df], signal_analysis_functions.tau_charge, timestamp
        )

        # assert
        u_hds_1_tau_charge_exp = 0.07779030787449404
        u_hds_2_tau_charge_exp = 0.07670421101448886

        self.assertAlmostEqual(
            u_hds_1_tau_charge_exp, row_feature_df.loc[timestamp, "16L2:U_HDS_1:tau_charge"], places=5
        )
        self.assertAlmostEqual(
            u_hds_2_tau_charge_exp, row_feature_df.loc[timestamp, "16L2:U_HDS_2:tau_charge"], places=5
        )

    def test_calculate_characteristic_time_ref(self):
        # arrange
        timestamp_ref = 1417079205409000000

        # act
        row_feature_ref_df = signal_analysis.calculate_features(
            [self.u_hds_1_ref_df, self.u_hds_2_ref_df], signal_analysis_functions.tau_charge, timestamp_ref
        )

        # assert
        u_hds_1_tau_charge_ref_exp = 0.07703877303213426
        u_hds_2_tau_charge_ref_exp = 0.07755450719263507

        self.assertAlmostEqual(
            u_hds_1_tau_charge_ref_exp, row_feature_ref_df.loc[timestamp_ref, "16L2:U_HDS_1:tau_charge"], places=5
        )
        self.assertAlmostEqual(
            u_hds_2_tau_charge_ref_exp, row_feature_ref_df.loc[timestamp_ref, "16L2:U_HDS_2:tau_charge"], places=5
        )

    def test_calculate_features(self):
        # arrange
        timestamp = 1544622149598000000
        timestamp_ref = 1417079205409000000

        # act
        row_feature_df = signal_analysis.calculate_features(
            [self.u_hds_1_df, self.u_hds_2_df],
            [utility_features.first, utility_features.last20mean, signal_analysis_functions.tau_charge],
            timestamp,
        )

        row_feature_ref_df = signal_analysis.calculate_features(
            [self.u_hds_1_ref_df, self.u_hds_2_ref_df],
            [utility_features.first, utility_features.last20mean, signal_analysis_functions.tau_charge],
            timestamp_ref,
        )

        features_qh_concat = pd.concat([row_feature_df, row_feature_ref_df], axis=0)
        features_abs_diff_qh = features_qh_concat.diff().tail(1).abs()

        # DQAMCNMQ_PMHSU, 2, 810, 1020, 0, 10, 0.003, 20, 5, 0
        is_tau_ok = features_abs_diff_qh.filter(regex="tau").values < 0.003
        is_charge_initial_ok = (row_feature_df.filter(regex="first").values > 810) & (
            row_feature_df.filter(regex="first").values < 1020
        )
        is_charge_final_ok = (row_feature_df.filter(regex="last").values > 0) & (
            row_feature_df.filter(regex="last").values < 10
        )

        self.assertEqual(True, all(is_tau_ok[0]) & all(is_charge_initial_ok[0]) & all(is_charge_final_ok[0]))
