import unittest
from unittest.mock import patch
import warnings

import pandas as pd
import pytest

from lhcsmapi.analysis.IpdCircuitAnalysis import IpdCircuitAnalysis
from lhcsmapi.analysis.IpqCircuitAnalysis import IpqCircuitAnalysis
from lhcsmapi.analysis.ItCircuitAnalysis import ItCircuitAnalysis
from lhcsmapi.analysis.RqCircuitAnalysis import RqCircuitAnalysis
from lhcsmapi.analysis.qh.QuenchHeaterVoltageCurrentAnalysis import QuenchHeaterVoltageCurrentAnalysis
from lhcsmapi.pyedsl.dbsignal.SignalIndexConversion import SignalIndexConversion
from test.resources.read_csv import read_csv


class TestQuenchHeaterAnalysis(unittest.TestCase):
    # RB 900V
    # \\cernbox-smb\eos\project\m\mp3\RB\RB.A78\QHDA\RB.A78_QHDA-2021-02-04-09h46-2021-02-04-10h46_PASSED.html
    timestamp_rb_900v = 1612428379105000000
    source_rb_900v = "C17R7"

    u_hds_dfs_rb_900v = [
        read_csv("resources/hwc/rb/qhda_2021_02_04", "C17R7.U_HDS_1"),
        read_csv("resources/hwc/rb/qhda_2021_02_04", "C17R7.U_HDS_2"),
        read_csv("resources/hwc/rb/qhda_2021_02_04", "C17R7.U_HDS_3"),
        read_csv("resources/hwc/rb/qhda_2021_02_04", "C17R7.U_HDS_4"),
    ]

    i_hds_dfs_rb_900v = [
        read_csv("resources/hwc/rb/qhda_2021_02_04", "C17R7.I_HDS_1"),
        read_csv("resources/hwc/rb/qhda_2021_02_04", "C17R7.I_HDS_2"),
        read_csv("resources/hwc/rb/qhda_2021_02_04", "C17R7.I_HDS_3"),
        read_csv("resources/hwc/rb/qhda_2021_02_04", "C17R7.I_HDS_4"),
    ]

    u_hds_ref_dfs_rb_900v = [
        read_csv("resources/hwc/rb/qhda_2021_02_04", "C17R7.U_HDS_1_REF"),
        read_csv("resources/hwc/rb/qhda_2021_02_04", "C17R7.U_HDS_2_REF"),
        read_csv("resources/hwc/rb/qhda_2021_02_04", "C17R7.U_HDS_3_REF"),
        read_csv("resources/hwc/rb/qhda_2021_02_04", "C17R7.U_HDS_4_REF"),
    ]

    i_hds_ref_dfs_rb_900v = [
        read_csv("resources/hwc/rb/qhda_2021_02_04", "C17R7.I_HDS_1_REF"),
        read_csv("resources/hwc/rb/qhda_2021_02_04", "C17R7.I_HDS_2_REF"),
        read_csv("resources/hwc/rb/qhda_2021_02_04", "C17R7.I_HDS_3_REF"),
        read_csv("resources/hwc/rb/qhda_2021_02_04", "C17R7.I_HDS_4_REF"),
    ]

    # RQ 900V
    # https://swan001.cern.ch/user/mmacieje/view/mp3/RQ/RQ.A78/QHDA/RQ.A78_QHDA-2021-02-03-15h22-2021-02-04-10h38_PASSED.html
    timestamp_rq_900v = 1612362328969388564
    source_rq_900v = "15L8"

    u_hds_dfs_rq_900v = [
        read_csv("resources/hwc/rq/qhda_2021_02_03", "15L8.U_HDS_1"),
        read_csv("resources/hwc/rq/qhda_2021_02_03", "15L8.U_HDS_2"),
    ]

    i_hds_dfs_rq_900v = [
        read_csv("resources/hwc/rq/qhda_2021_02_03", "15L8.I_HDS_1"),
        read_csv("resources/hwc/rq/qhda_2021_02_03", "15L8.I_HDS_2"),
    ]

    # RQ 900V with reference
    # \\cernbox-smb\eos\project\m\mp3\RQ\RQ.A78\FPA\RQ.A78_FPA-2021-02-26-16h14-2021-02-26-17h10.html
    timestamp_rq_900v_2 = 1614352451073838422
    source_rq_900v_2 = "14L8"

    u_hds_dfs_rq_900v_2 = [
        read_csv("resources/hwc/rq/qhda_2021_02_26", "14L8.U_HDS_1"),
        read_csv("resources/hwc/rq/qhda_2021_02_26", "14L8.U_HDS_2"),
    ]

    i_hds_dfs_rq_900v_2 = [
        read_csv("resources/hwc/rq/qhda_2021_02_26", "14L8.I_HDS_1"),
        read_csv("resources/hwc/rq/qhda_2021_02_26", "14L8.I_HDS_2"),
    ]

    u_hds_ref_dfs_rq_900v_2 = [
        read_csv("resources/hwc/rq/qhda_2021_02_26", "14L8.U_HDS_1_REF"),
        read_csv("resources/hwc/rq/qhda_2021_02_26", "14L8.U_HDS_2_REF"),
    ]

    i_hds_ref_dfs_rq_900v_2 = [
        read_csv("resources/hwc/rq/qhda_2021_02_26", "14L8.I_HDS_1_REF"),
        read_csv("resources/hwc/rq/qhda_2021_02_26", "14L8.I_HDS_2_REF"),
    ]

    # IT
    # \\cernbox-smb\eos\project\m\mp3\IT\RQX.L8\QHDA\RQX.L8_QHDA-2021-02-04-10h29-2021-02-05-10h07_PASSED.html
    timestamp_it_900v = 1612430988829000000
    source_it_900v = "RQX.L8"

    u_hds_dfs_it_900v = [
        read_csv("resources/hwc/it/qhda", "heat.RQX.L8.U_HDS_1_Q1"),
        read_csv("resources/hwc/it/qhda", "heat.RQX.L8.U_HDS_2_Q1"),
        read_csv("resources/hwc/it/qhda", "heat.RQX.L8.U_HDS_1_Q2"),
        read_csv("resources/hwc/it/qhda", "heat.RQX.L8.U_HDS_2_Q2"),
        read_csv("resources/hwc/it/qhda", "heat.RQX.L8.U_HDS_3_Q2"),
        read_csv("resources/hwc/it/qhda", "heat.RQX.L8.U_HDS_4_Q2"),
        read_csv("resources/hwc/it/qhda", "heat.RQX.L8.U_HDS_1_Q3"),
        read_csv("resources/hwc/it/qhda", "heat.RQX.L8.U_HDS_2_Q3"),
    ]

    u_hds_ref_dfs_it_900v = [
        read_csv("resources/hwc/it/qhda", "heat.RQX.L8.U_HDS_1_Q1_REF"),
        read_csv("resources/hwc/it/qhda", "heat.RQX.L8.U_HDS_1_Q2_REF"),
        read_csv("resources/hwc/it/qhda", "heat.RQX.L8.U_HDS_2_Q1_REF"),
        read_csv("resources/hwc/it/qhda", "heat.RQX.L8.U_HDS_2_Q2_REF"),
        read_csv("resources/hwc/it/qhda", "heat.RQX.L8.U_HDS_3_Q2_REF"),
        read_csv("resources/hwc/it/qhda", "heat.RQX.L8.U_HDS_4_Q2_REF"),
        read_csv("resources/hwc/it/qhda", "heat.RQX.L8.U_HDS_1_Q3_REF"),
        read_csv("resources/hwc/it/qhda", "heat.RQX.L8.U_HDS_2_Q3_REF"),
    ]

    # IPD
    # \\cernbox-smb\eos\project\m\mp3\IPD\RD2.L8\QHDA\RD2.L8_QHDA-2021-02-01-16h57-2021-02-01-17h18_PASSED.html
    source_ipd_900v = "RD2.L8"
    timestamp_ipd_900v = 1612195066418000000

    u_hds_dfs_ipd_900v = [
        read_csv("resources/hwc/ipd/qhda", "heat.RD2.L8.U_HDS_1_B1"),
        read_csv("resources/hwc/ipd/qhda", "heat.RD2.L8.U_HDS_1_B2"),
    ]

    u_hds_ref_dfs_ipd_900v = [
        read_csv("resources/hwc/ipd/qhda", "heat.RD2.L8.U_HDS_1_B1_REF"),
        read_csv("resources/hwc/ipd/qhda", "heat.RD2.L8.U_HDS_1_B2_REF"),
    ]

    # IPQ
    # \\cernbox-smb\eos\project\m\mp3\IPQ\RQ10\RQ10.L8\QHDA\RQ10.L8_QHDA-2021-02-01-16h59-2021-02-01-17h28_PASSED.html
    source_ipq_900v = "RQ10.L8"
    timestamp_ipq_900v = 1612195171667000000

    u_hds_dfs_ipq_900v = [
        read_csv("resources/hwc/ipq/qhda", "heat.RQ10.L8.U_HDS_1_B1"),
        read_csv("resources/hwc/ipq/qhda", "heat.RQ10.L8.U_HDS_1_B2"),
    ]

    u_hds_ref_dfs_ipq_900v = [
        read_csv("resources/hwc/ipq/qhda", "heat.RQ10.L8.U_HDS_1_B1_REF"),
        read_csv("resources/hwc/ipq/qhda", "heat.RQ10.L8.U_HDS_1_B2_REF"),
    ]

    # RB
    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_analyze_single_qh_rb_900v(self, mock_show=None):
        # arrange
        nominal_voltage = 900
        mean_start_value = 15 if nominal_voltage < 450 else 50

        # act
        is_qh_correct = QuenchHeaterVoltageCurrentAnalysis("RB").analyze_single_qh_voltage_current_with_ref(
            self.source_rb_900v,
            self.timestamp_rb_900v,
            self.u_hds_dfs_rb_900v,
            self.i_hds_dfs_rb_900v,
            self.u_hds_ref_dfs_rb_900v,
            self.i_hds_ref_dfs_rb_900v,
            plot_qh_discharge=QuenchHeaterVoltageCurrentAnalysis.plot_voltage_current_resistance_with_ref_hwc,
            mean_start_value=mean_start_value,
            current_offset=0.085,
            nominal_voltage=nominal_voltage,
        )

        # assert
        self.assertEqual(True, is_qh_correct)

        if mock_show is not None:
            mock_show.assert_called()

    def test_calculate_rb_qh_feature_row_rb_900v(self):
        # arrange
        nominal_voltage = 900
        mean_start_value = 15 if nominal_voltage < 450 else 50

        # act
        feature_row_df_act = QuenchHeaterVoltageCurrentAnalysis("RB").calculate_qh_feature_row(
            self.u_hds_dfs_rb_900v,
            self.i_hds_dfs_rb_900v,
            self.timestamp_rb_900v,
            current_offset=0.085,
            mean_start_value=mean_start_value,
        )

        # assert
        feature_row_df_exp = pd.DataFrame(
            {
                "U_1_0": {1612428379105000000: 902.0},
                "U_1_end": {1612428379105000000: 33.0},
                "U_2_0": {1612428379105000000: 922.0},
                "U_2_end": {1612428379105000000: 40.0},
                "U_3_0": {1612428379105000000: 909.0},
                "U_3_end": {1612428379105000000: 32.0},
                "U_4_0": {1612428379105000000: 919.0},
                "U_4_end": {1612428379105000000: 42.0},
                "R_1_0": {1612428379105000000: 10.34},
                "R_2_0": {1612428379105000000: 10.49},
                "R_3_0": {1612428379105000000: 10.25},
                "R_4_0": {1612428379105000000: 10.68},
                "U_1_tau": {1612428379105000000: 0.087},
                "U_2_tau": {1612428379105000000: 0.093},
                "U_3_tau": {1612428379105000000: 0.087},
                "U_4_tau": {1612428379105000000: 0.095},
                "I_1_tau": {1612428379105000000: 0.076},
                "I_2_tau": {1612428379105000000: 0.081},
                "I_3_tau": {1612428379105000000: 0.076},
                "I_4_tau": {1612428379105000000: 0.083},
            }
        )

        pd.testing.assert_frame_equal(feature_row_df_exp, feature_row_df_act)

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_analyze_qh_rb_900v(self, mock_show=None):
        # arrange
        source_timestamp_qh_df = pd.DataFrame(
            {"source": {0: self.source_rb_900v}, "timestamp": {0: self.timestamp_rb_900v}}
        )

        # act
        qh_analysis = QuenchHeaterVoltageCurrentAnalysis("RB")
        qh_analysis.analyze_multi_qh_voltage_current_with_ref(
            source_timestamp_qh_df,
            [self.u_hds_dfs_rb_900v],
            [self.i_hds_dfs_rb_900v],
            [self.u_hds_ref_dfs_rb_900v],
            [self.i_hds_ref_dfs_rb_900v],
            current_offset=0.085,
        )

        # assert
        if mock_show is not None:
            mock_show.assert_called()

    # RQ
    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_analyze_single_qh_rq_900v(self, mock_show=None):
        # arrange
        nominal_voltage = 900
        mean_start_value = 15 if nominal_voltage < 450 else 50

        # act
        is_qh_correct = QuenchHeaterVoltageCurrentAnalysis("RQ").analyze_single_qh_voltage_current(
            self.source_rq_900v,
            self.timestamp_rq_900v,
            self.u_hds_dfs_rq_900v,
            self.i_hds_dfs_rq_900v,
            plot_qh_discharge=QuenchHeaterVoltageCurrentAnalysis.plot_voltage_current_resistance,
            mean_start_value=mean_start_value,
            current_offset=0.025,
            nominal_voltage=nominal_voltage,
        )

        # assert
        self.assertEqual(False, is_qh_correct)

        if mock_show is not None:
            mock_show.assert_called()

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_analyze_qh_voltage_current_900v(self, mock_show=None):
        # arrange
        rq_analysis = RqCircuitAnalysis("RQ", None, is_automatic=True)
        source_timestamp_qh_df = pd.DataFrame(
            {"source": {0: self.source_rq_900v_2}, "timestamp": {0: self.timestamp_rq_900v_2}}
        )

        # act
        rq_analysis.analyze_multi_qh_voltage_current_with_ref(
            source_timestamp_qh_df,
            [self.u_hds_dfs_rq_900v_2],
            [self.i_hds_dfs_rq_900v_2],
            [self.u_hds_ref_dfs_rq_900v_2],
            [self.i_hds_ref_dfs_rq_900v_2],
            current_offset=0.085,
        )

        # assert
        if mock_show is not None:
            mock_show.assert_called()

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_analyze_qh_rq(self, mock_show=None):
        # arrange
        rq_analysis = RqCircuitAnalysis("RQ", None, is_automatic=True)
        source_timestamp_qh_df = pd.DataFrame(
            {"source": {0: self.source_rq_900v_2}, "timestamp": {0: self.timestamp_rq_900v_2}}
        )

        # act
        rq_analysis.analyze_multi_qh_voltage_with_ref(
            source_timestamp_qh_df, [self.u_hds_dfs_rq_900v_2], [self.u_hds_ref_dfs_rq_900v_2]
        )

        # assert
        if mock_show is not None:
            mock_show.assert_called()

    # IT
    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_analyze_single_qh_voltage_with_ref_it_900v(self, mock_show=None):
        # arrange
        it_analysis = ItCircuitAnalysis("IT")
        circuit_name = "RQX.L8"
        discharge_level = 900

        # act
        it_analysis.analyze_single_qh_voltage_with_ref(
            circuit_name,
            self.timestamp_it_900v,
            self.u_hds_dfs_it_900v,
            self.u_hds_ref_dfs_it_900v,
            nominal_voltage=discharge_level,
        )

        # assert
        if mock_show is not None:
            mock_show.assert_called()

    # IPD
    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_analyze_single_qh_voltage_with_ref_ipd_900v(self, mock_show=None):
        # arrange
        it_analysis = IpdCircuitAnalysis("IPD2_B1B2")
        circuit_name = "RD2.L8"
        discharge_level = 900

        # act
        it_analysis.analyze_single_qh_voltage_with_ref(
            circuit_name,
            self.timestamp_ipd_900v,
            self.u_hds_dfs_ipd_900v,
            self.u_hds_ref_dfs_ipd_900v,
            nominal_voltage=discharge_level,
        )

        # assert
        if mock_show is not None:
            mock_show.assert_called()

    # IPQ
    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_analyze_single_qh_voltage_with_ref_ipq_900v(self, mock_show=None):
        # arrange
        analysis = IpqCircuitAnalysis("IPQ2", None, circuit_name="RQ10.L8")
        circuit_name = "RQ10.L8"
        discharge_level = 900

        # act
        analysis.analyze_single_qh_voltage_with_ref(
            circuit_name,
            self.timestamp_ipq_900v,
            self.u_hds_dfs_ipq_900v,
            self.u_hds_ref_dfs_ipq_900v,
            nominal_voltage=discharge_level,
        )

        # assert
        if mock_show is not None:
            mock_show.assert_called()

    def test_get_decay_only_c25r7(self):
        # arrange
        i_hds_dfs = [
            read_csv("resources/hwc/rb/qhda_2021_01_13", "C25R7.I_HDS_1"),
            read_csv("resources/hwc/rb/qhda_2021_01_13", "C25R7.I_HDS_2"),
            read_csv("resources/hwc/rb/qhda_2021_01_13", "C25R7.I_HDS_3"),
            read_csv("resources/hwc/rb/qhda_2021_01_13", "C25R7.I_HDS_4"),
        ]

        u_hds_dfs = [
            read_csv("resources/hwc/rb/qhda_2021_01_13", "C25R7.U_HDS_1"),
            read_csv("resources/hwc/rb/qhda_2021_01_13", "C25R7.U_HDS_2"),
            read_csv("resources/hwc/rb/qhda_2021_01_13", "C25R7.U_HDS_3"),
            read_csv("resources/hwc/rb/qhda_2021_01_13", "C25R7.U_HDS_4"),
        ]

        # act
        index_decay_start = QuenchHeaterVoltageCurrentAnalysis("RB").get_decay_start_index(
            i_hds_dfs, mean_start_value=50
        )
        hds_decay_dfs = QuenchHeaterVoltageCurrentAnalysis("RB").extract_decay(u_hds_dfs + i_hds_dfs, index_decay_start)

        hds_decay_dfs = SignalIndexConversion.synchronize_dfs(hds_decay_dfs)

        # assert
        for hds_decay_df in hds_decay_dfs:
            self.assertAlmostEqual(0.0, hds_decay_df.index[0], places=2)

    def test_get_decay_only_b25r7(self):
        # arrange
        i_hds_dfs = [
            read_csv("resources/hwc/rb/qhda_2021_01_13", "B25R7.I_HDS_1"),
            read_csv("resources/hwc/rb/qhda_2021_01_13", "B25R7.I_HDS_2"),
            read_csv("resources/hwc/rb/qhda_2021_01_13", "B25R7.I_HDS_3"),
            read_csv("resources/hwc/rb/qhda_2021_01_13", "B25R7.I_HDS_4"),
        ]

        u_hds_dfs = [
            read_csv("resources/hwc/rb/qhda_2021_01_13", "B25R7.U_HDS_1"),
            read_csv("resources/hwc/rb/qhda_2021_01_13", "B25R7.U_HDS_2"),
            read_csv("resources/hwc/rb/qhda_2021_01_13", "B25R7.U_HDS_3"),
            read_csv("resources/hwc/rb/qhda_2021_01_13", "B25R7.U_HDS_4"),
        ]

        # act
        # act
        index_decay_start = QuenchHeaterVoltageCurrentAnalysis("RB").get_decay_start_index(
            i_hds_dfs, mean_start_value=50
        )
        hds_decay_dfs = QuenchHeaterVoltageCurrentAnalysis("RB").extract_decay(u_hds_dfs + i_hds_dfs, index_decay_start)

        hds_decay_dfs = SignalIndexConversion.synchronize_dfs(hds_decay_dfs)

        # assert
        for hds_decay_df in hds_decay_dfs:
            self.assertAlmostEqual(0.0, hds_decay_df.index[0], places=2)


def _get_rb_missing_data_params(u_hds, i_hds, u_hds_ref, i_hds_ref, warning_message):
    u_hds_dfs = [
        [
            read_csv("resources/hwc/rb/qhda_2021_02_04", "C17R7.U_HDS_1").rename(
                columns={"C17R7:U_HDS_1": source + ":U_HDS_1"}
            )
        ]
        for source in u_hds
    ]
    i_hds_dfs = [
        [
            read_csv("resources/hwc/rb/qhda_2021_02_04", "C17R7.I_HDS_1").rename(
                columns={"C17R7:I_HDS_1": source + ":I_HDS_1"}
            )
        ]
        for source in i_hds
    ]
    u_hds_ref_dfs = [
        [
            read_csv("resources/hwc/rb/qhda_2021_02_04", "C17R7.U_HDS_1_REF").rename(
                columns={"C17R7:U_HDS_1": source + ":U_HDS_1"}
            )
        ]
        for source in u_hds_ref
    ]
    i_hds_ref_dfs = [
        [
            read_csv("resources/hwc/rb/qhda_2021_02_04", "C17R7.I_HDS_1_REF").rename(
                columns={"C17R7:I_HDS_1": source + ":I_HDS_1"}
            )
        ]
        for source in i_hds_ref
    ]
    return u_hds_dfs, i_hds_dfs, u_hds_ref_dfs, i_hds_ref_dfs, warning_message


_RB_MISSING_DATA = [
    _get_rb_missing_data_params(
        ["A25R6", "A8R8"],
        ["A11R6", "A25R6", "A8R8"],
        ["A11R6", "A25R6", "A8R8"],
        ["A11R6", "A25R6", "A8R8"],
        [
            "Voltages missing for A11R6.",
            "At least one DataFrame is empty, QH voltage and current analysis skipped!",
            "Voltages for A25R6 found at the wrong index.",
            "Voltages for A8R8 found at the wrong index.",
        ],
    ),
    _get_rb_missing_data_params(
        ["A11R6", "A25R6", "A8R8"],
        ["A11R6", "A8R8"],
        ["A11R6", "A25R6", "A8R8"],
        ["A11R6", "A25R6", "A8R8"],
        [
            "Currents missing for A25R6.",
            "At least one DataFrame is empty, QH voltage and current analysis skipped!",
            "Currents for A8R8 found at the wrong index.",
        ],
    ),
    _get_rb_missing_data_params(
        ["A11R6", "A25R6", "A8R8"],
        ["A11R6", "A25R6", "A8R8"],
        ["A11R6", "A25R6"],
        ["A11R6", "A25R6", "A8R8"],
        [
            "Reference voltages missing for A8R8.",
            "At least one DataFrame is empty, QH voltage and current analysis skipped!",
        ],
    ),
    _get_rb_missing_data_params(
        ["A11R6", "A8R8"],
        ["A11R6", "A8R8"],
        ["A11R6", "A8R8"],
        ["A11R6", "A8R8"],
        [
            "Voltages missing for A25R6.",
            "Currents missing for A25R6.",
            "Reference voltages missing for A25R6.",
            "Reference currents missing for A25R6.",
            "At least one DataFrame is empty, QH voltage and current analysis skipped!",
            "Voltages for A8R8 found at the wrong index.",
            "Currents for A8R8 found at the wrong index.",
            "Reference voltages for A8R8 found at the wrong index.",
            "Reference currents for A8R8 found at the wrong index.",
        ],
    ),
    _get_rb_missing_data_params(
        ["A25R6", "A8R8", "A11R6"],
        ["A11R6", "A25R6", "A8R8"],
        ["A11R6", "A25R6", "A8R8"],
        ["A11R6", "A25R6", "A8R8"],
        [
            "Voltages for A11R6 found at the wrong index.",
            "Voltages for A25R6 found at the wrong index.",
            "Voltages for A8R8 found at the wrong index.",
        ],
    ),
    _get_rb_missing_data_params(
        ["A25R6", "A11R6", "A8R8"],
        ["A11R6", "A25R6", "A8R8"],
        ["A11R6", "A25R6", "A8R8"],
        ["A11R6", "A25R6", "A8R8"],
        ["Voltages for A11R6 found at the wrong index.", "Voltages for A25R6 found at the wrong index."],
    ),
    _get_rb_missing_data_params(
        ["A11R6", "A8R8", "A25R6"],
        ["A11R6", "A25R6", "A8R8"],
        ["A11R6", "A25R6", "A8R8"],
        ["A11R6", "A25R6", "A8R8"],
        ["Voltages for A25R6 found at the wrong index.", "Voltages for A8R8 found at the wrong index."],
    ),
    _get_rb_missing_data_params(
        ["A11R6", "A8R8", "A25R6"],
        ["A11R6", "A8R8", "A25R6"],
        ["A11R6", "A8R8", "A25R6"],
        ["A11R6", "A8R8", "A25R6"],
        [
            "Voltages for A25R6 found at the wrong index.",
            "Currents for A25R6 found at the wrong index.",
            "Reference voltages for A25R6 found at the wrong index.",
            "Reference currents for A25R6 found at the wrong index.",
            "Voltages for A8R8 found at the wrong index.",
            "Currents for A8R8 found at the wrong index.",
            "Reference voltages for A8R8 found at the wrong index.",
            "Reference currents for A8R8 found at the wrong index.",
        ],
    ),
]


@patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
@pytest.mark.parametrize("u_hds,i_hds,u_hds_ref,i_hds_ref,warning_messages", _RB_MISSING_DATA)
def test_analyze_qh_rb_with_missing_data(mock_show, u_hds, i_hds, u_hds_ref, i_hds_ref, warning_messages):
    # arrange
    timestamp = 1612428379105000000
    source_timestamp_qh_df = pd.DataFrame(
        {"source": {0: "A11R6", 1: "A25R6", 2: "A8R8"}, "timestamp": {0: timestamp, 1: timestamp, 2: timestamp}}
    )

    # act
    with warnings.catch_warnings(record=True) as w:
        qh_analysis = QuenchHeaterVoltageCurrentAnalysis("RB")
        qh_analysis.analyze_multi_qh_voltage_current_with_ref(
            source_timestamp_qh_df, u_hds, i_hds, u_hds_ref, i_hds_ref, current_offset=0.085
        )

    # assert
    assert warning_messages == [
        str(warning.message)
        for warning in w
        if isinstance(warning.message, UserWarning)
        and not str(warning.message).startswith("\nThe is_first_col function was deprecated")
    ]
    if mock_show is not None:
        mock_show.assert_called()


_RB_NOTHING_TO_SHOW = [
    _get_rb_missing_data_params(
        ["A25R6", "A8R8"],
        ["A11R6", "A8R8"],
        ["A11R6", "A25R6"],
        ["A11R6", "A25R6", "A8R8"],
        [
            "Voltages missing for A11R6.",
            "At least one DataFrame is empty, QH voltage and current analysis skipped!",
            "Voltages for A25R6 found at the wrong index.",
            "Currents missing for A25R6.",
            "At least one DataFrame is empty, QH voltage and current analysis skipped!",
            "Voltages for A8R8 found at the wrong index.",
            "Currents for A8R8 found at the wrong index.",
            "Reference voltages missing for A8R8.",
            "At least one DataFrame is empty, QH voltage and current analysis skipped!",
        ],
    ),
    _get_rb_missing_data_params(
        ["A11R6", "A25R6", "A8R8"],
        ["A11R6", "A25R6", "A8R8"],
        ["A11R6", "A25R6", "A8R8"],
        [],
        [
            "Reference currents missing for A11R6.",
            "At least one DataFrame is empty, QH voltage and current analysis skipped!",
            "Reference currents missing for A25R6.",
            "At least one DataFrame is empty, QH voltage and current analysis skipped!",
            "Reference currents missing for A8R8.",
            "At least one DataFrame is empty, QH voltage and current analysis skipped!",
        ],
    ),
    _get_rb_missing_data_params(
        ["A11R6", "A25R6", "A8R8"],
        [],
        ["A11R6", "A25R6", "A8R8"],
        ["A11R6", "A25R6", "A8R8"],
        [
            "Currents missing for A11R6.",
            "At least one DataFrame is empty, QH voltage and current analysis skipped!",
            "Currents missing for A25R6.",
            "At least one DataFrame is empty, QH voltage and current analysis skipped!",
            "Currents missing for A8R8.",
            "At least one DataFrame is empty, QH voltage and current analysis skipped!",
        ],
    ),
    _get_rb_missing_data_params(
        ["A11R6", "A25R6", "A8R8"],
        ["A11R6", "A25R6", "A8R8"],
        [],
        ["A11R6", "A25R6", "A8R8"],
        [
            "Reference voltages missing for A11R6.",
            "At least one DataFrame is empty, QH voltage and current analysis skipped!",
            "Reference voltages missing for A25R6.",
            "At least one DataFrame is empty, QH voltage and current analysis skipped!",
            "Reference voltages missing for A8R8.",
            "At least one DataFrame is empty, QH voltage and current analysis skipped!",
        ],
    ),
    _get_rb_missing_data_params(
        [],
        ["A11R6", "A25R6", "A8R8"],
        ["A11R6", "A25R6", "A8R8"],
        ["A11R6", "A25R6", "A8R8"],
        [
            "Voltages missing for A11R6.",
            "At least one DataFrame is empty, QH voltage and current analysis skipped!",
            "Voltages missing for A25R6.",
            "At least one DataFrame is empty, QH voltage and current analysis skipped!",
            "Voltages missing for A8R8.",
            "At least one DataFrame is empty, QH voltage and current analysis skipped!",
        ],
    ),
    _get_rb_missing_data_params(
        [],
        [],
        [],
        [],
        [
            "Voltages missing for A11R6.",
            "Currents missing for A11R6.",
            "Reference voltages missing for A11R6.",
            "Reference currents missing for A11R6.",
            "At least one DataFrame is empty, QH voltage and current analysis skipped!",
            "Voltages missing for A25R6.",
            "Currents missing for A25R6.",
            "Reference voltages missing for A25R6.",
            "Reference currents missing for A25R6.",
            "At least one DataFrame is empty, QH voltage and current analysis skipped!",
            "Voltages missing for A8R8.",
            "Currents missing for A8R8.",
            "Reference voltages missing for A8R8.",
            "Reference currents missing for A8R8.",
            "At least one DataFrame is empty, QH voltage and current analysis skipped!",
        ],
    ),
]


@patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
@pytest.mark.parametrize("u_hds,i_hds,u_hds_ref,i_hds_ref,warning_messages", _RB_NOTHING_TO_SHOW)
def test_analyze_qh_rb_nothing_to_show(mock_show, u_hds, i_hds, u_hds_ref, i_hds_ref, warning_messages):
    # arrange
    timestamp = 1612428379105000000
    source_timestamp_qh_df = pd.DataFrame(
        {"source": {0: "A11R6", 1: "A25R6", 2: "A8R8"}, "timestamp": {0: timestamp, 1: timestamp, 2: timestamp}}
    )

    # act
    with warnings.catch_warnings(record=True) as w:
        warnings.filterwarnings("ignore", category=DeprecationWarning)
        qh_analysis = QuenchHeaterVoltageCurrentAnalysis("RB")
        qh_analysis.analyze_multi_qh_voltage_current_with_ref(
            source_timestamp_qh_df, u_hds, i_hds, u_hds_ref, i_hds_ref, current_offset=0.085
        )

    # assert
    assert [str(warning.message) for warning in w if isinstance(warning.message, UserWarning)] == warning_messages
    if mock_show is not None:
        mock_show.assert_not_called()
