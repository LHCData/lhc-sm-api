import unittest
from io import StringIO
from unittest.mock import patch

from lhcsmapi.analysis.expert_input import get_expert_comment, check_show_next, get_expert_decision


class TestUserInput(unittest.TestCase):
    # get_expert_comment
    def run_get_expert_comment(self, is_automatic, automatic_analysis, expected_out):
        with patch("sys.stdout", new=StringIO()) as fake_out:
            comment = get_expert_comment(
                system="QH", is_automatic=is_automatic, automatic_analysis_comment=automatic_analysis
            )
            self.assertEqual(fake_out.getvalue().strip(), expected_out)
            return comment

    @patch("lhcsmapi.analysis.expert_input.input", create=True)
    def test_get_expert_comment_with_automatic_with_empty_analysis_output(self, mocked_input):
        # arrange
        is_automatic = True
        automatic_comment = ""
        mocked_input.side_effect = [""]

        # act
        expected_out = "OUTPUT\n------\nQH Analysis comment is: Automatic analysis, no expert feedback.\n------"
        comment_act = self.run_get_expert_comment(is_automatic, automatic_comment, expected_out)

        # assert
        comment_ref = "Automatic analysis, no expert feedback."
        self.assertEqual(comment_ref, comment_act)

    @patch("lhcsmapi.analysis.expert_input.input", create=True)
    def test_get_expert_comment_with_automatic_with_analysis_output(self, mocked_input):
        # arrange
        is_automatic = True
        automatic_comment = "OK"
        mocked_input.side_effect = [""]

        # act
        expected_out = "OUTPUT\n------\nQH Analysis comment is: OK\n------"
        comment_act = self.run_get_expert_comment(is_automatic, automatic_comment, expected_out)

        # assert
        comment_ref = "OK"
        self.assertEqual(comment_ref, comment_act)

    @patch("lhcsmapi.analysis.expert_input.input", create=True)
    def test_get_expert_comment_without_automatic_with_ok_expert_comment(self, mocked_input):
        # arrange
        is_automatic = False
        automatic_comment = ""
        mocked_input.side_effect = ["OK"]

        # act
        expected_out = "OUTPUT\n------\nQH Analysis comment is: OK\n------"
        comment_act = self.run_get_expert_comment(is_automatic, automatic_comment, expected_out)

        # assert
        comment_ref = "OK"
        self.assertEqual(comment_ref, comment_act)

    @patch("lhcsmapi.analysis.expert_input.input", create=True)
    def test_get_expert_comment_without_automatic_with_not_ok_expert_comment(self, mocked_input):
        # arrange
        is_automatic = False
        automatic_comment = ""
        mocked_input.side_effect = ["NOT_OK", "OK"]

        # act
        expected_out = "OUTPUT\n------\nQH Analysis comment is: OK\n------"
        comment_act = self.run_get_expert_comment(is_automatic, automatic_comment, expected_out)

        # assert
        comment_ref = "OK"
        self.assertEqual(comment_ref, comment_act)

    # check_show_next
    def test_check_show_next_is_automatic_true(self):
        # arrange
        index = 0
        max_index = 1
        is_automatic = True

        # act
        output_act = check_show_next(index, max_index, is_automatic)

        # assert
        output_ref = False
        self.assertEqual(output_ref, output_act)

    def test_check_show_next_is_automatic_false_max_iter(self):
        # arrange
        index = 1
        max_index = 1
        is_automatic = False

        # act
        output_act = check_show_next(index, max_index, is_automatic)

        # assert
        output_ref = True
        self.assertEqual(output_ref, output_act)

    @patch("lhcsmapi.analysis.expert_input.input", create=True)
    def test_check_show_next_is_automatic_false_no(self, mocked_input):
        # arrange
        index = 0
        max_index = 2
        is_automatic = False
        mocked_input.side_effect = ["N"]

        # act
        output_act = check_show_next(index, max_index, is_automatic)

        # assert
        output_ref = True
        self.assertEqual(output_ref, output_act)

    @patch("lhcsmapi.analysis.expert_input.input", create=True)
    def test_check_show_next_is_automatic_false_yes(self, mocked_input):
        # arrange
        index = 0
        max_index = 2
        is_automatic = False
        mocked_input.side_effect = ["Y"]

        # act
        output_act = check_show_next(index, max_index, is_automatic)

        # assert
        output_ref = False
        self.assertEqual(output_ref, output_act)

    # get_expert_decision
    @patch("lhcsmapi.analysis.expert_input.input", create=True)
    def test_get_expert_decision_magnet(self, mocked_input):
        # arrange
        mocked_input.side_effect = ["magnet"]

        # act
        decision_act = get_expert_decision("Quench Origin", "magnet busbar other".split())

        # assert
        decision_ref = "magnet"
        self.assertEqual(decision_ref, decision_act)

    @patch("lhcsmapi.analysis.expert_input.input", create=True)
    def test_get_expert_decision_bus_bar_busbar(self, mocked_input):
        # arrange
        mocked_input.side_effect = ["bus bar", "busbar"]

        # act
        decision_act = get_expert_decision("Quench Origin", "magnet busbar other".split())

        # assert
        decision_ref = "busbar"
        self.assertEqual(decision_ref, decision_act)
