import unittest
import warnings

import numpy as np
import pandas as pd
import pytest

from lhcsmapi.analysis.decorators import check_dataframe_empty, check_nan_timestamp, check_arguments_not_none

_WARNING_TEXT_EMPTY_DF = "All DataFrames are empty, LEADS voltage plot skipped!"
_SUCCESS_RESULT_EMPTY_DF = "success"


class TestDataFrameEmptyDecorator(unittest.TestCase):
    @check_dataframe_empty(mode="all", warning=_WARNING_TEXT_EMPTY_DF)
    def analyze_leads_voltage(self, u_df, u_dfs):
        pass

    @check_dataframe_empty(mode="all", warning=_WARNING_TEXT_EMPTY_DF)
    def plot_leads_voltage(self, circuit_name, u_dfs):
        pass

    @check_dataframe_empty(mode="any", warning=_WARNING_TEXT_EMPTY_DF, default_return=pd.DataFrame())
    def method_to_test_any_mode(self, circuit_name, u_df, u_dfs):
        return _SUCCESS_RESULT_EMPTY_DF

    # analyze_leads_voltage
    def test_analyze_leads_voltage_all_empty(self):
        with warnings.catch_warnings(record=True) as w:
            self.analyze_leads_voltage(pd.DataFrame(), [pd.DataFrame(), pd.DataFrame()])
            self.assertEqual(str(w[0].message), _WARNING_TEXT_EMPTY_DF)

    def test_analyze_leads_voltage_scalar_empty(self):
        with warnings.catch_warnings(record=True) as w:
            self.analyze_leads_voltage(
                pd.DataFrame(),
                [pd.DataFrame(index=[0], data=[1], columns=["a"]), pd.DataFrame(index=[0], data=[1], columns=["a"])],
            )
            self.assertEqual(str(w[0].message), _WARNING_TEXT_EMPTY_DF)

    def test_analyze_leads_voltage_all_non_empty(self):
        with warnings.catch_warnings(record=True) as w:
            self.analyze_leads_voltage(
                pd.DataFrame(index=[0], data=[1], columns=["a"]),
                [pd.DataFrame(index=[0], data=[1], columns=["a"]), pd.DataFrame(index=[0], data=[1], columns=["a"])],
            )
            self.assertListEqual([], w)

    # plot_leads_voltage
    def test_plot_leads_voltage_all_empty(self):
        with warnings.catch_warnings(record=True) as w:
            self.plot_leads_voltage("RB.A12", [pd.DataFrame(), pd.DataFrame()])
            self.assertEqual(str(w[0].message), _WARNING_TEXT_EMPTY_DF)

    def test_plot_leads_voltage_all_non_empty(self):
        with warnings.catch_warnings(record=True) as w:
            self.plot_leads_voltage(
                "RB.A12",
                [pd.DataFrame(index=[0], data=[1], columns=["a"]), pd.DataFrame(index=[0], data=[1], columns=["a"])],
            )
            self.assertListEqual([], w)

    # method_to_test_any_mode

    def test_method_to_test_any_mode_all_empty(self):
        with warnings.catch_warnings(record=True) as w:
            result = self.method_to_test_any_mode("RB.A12", pd.DataFrame(), [pd.DataFrame(), pd.DataFrame()])
            self.assertEqual(str(w[0].message), _WARNING_TEXT_EMPTY_DF)
            self.assertTrue(isinstance(result, pd.DataFrame))
            self.assertTrue(result.empty)

    def test_method_to_test_any_mode_single_empty(self):
        with warnings.catch_warnings(record=True) as w:
            result = self.method_to_test_any_mode(
                "RB.A12",
                pd.DataFrame(),
                [pd.DataFrame(index=[0], data=[1], columns=["a"]), pd.DataFrame(index=[0], data=[1], columns=["a"])],
            )
            self.assertEqual(str(w[0].message), _WARNING_TEXT_EMPTY_DF)
            self.assertTrue(isinstance(result, pd.DataFrame))
            self.assertTrue(result.empty)

    def test_method_to_test_any_mode_in_list_empty(self):
        with warnings.catch_warnings(record=True) as w:
            result = self.method_to_test_any_mode(
                "RB.A12",
                pd.DataFrame(index=[0], data=[1], columns=["a"]),
                [pd.DataFrame(index=[0], data=[1], columns=["a"]), pd.DataFrame()],
            )
            self.assertEqual(str(w[0].message), _WARNING_TEXT_EMPTY_DF)
            self.assertTrue(isinstance(result, pd.DataFrame))
            self.assertTrue(result.empty)

    def test_method_to_test_any_mode_all_non_empty(self):
        with warnings.catch_warnings(record=True) as w:
            result = self.method_to_test_any_mode(
                "RB.A12",
                pd.DataFrame(index=[0], data=[1], columns=["a"]),
                [pd.DataFrame(index=[0], data=[1], columns=["a"]), pd.DataFrame(index=[0], data=[1], columns=["a"])],
            )
            self.assertListEqual([], w)
            self.assertEqual(result, _SUCCESS_RESULT_EMPTY_DF)

    def test_method_to_test_any_mode_all_non_empty_except_circuit(self):
        with warnings.catch_warnings(record=True) as w:
            result = self.method_to_test_any_mode(
                None,
                pd.DataFrame(index=[0], data=[1], columns=["a"]),
                [pd.DataFrame(index=[0], data=[1], columns=["a"]), pd.DataFrame(index=[0], data=[1], columns=["a"])],
            )
            self.assertListEqual([], w)
            self.assertEqual(result, _SUCCESS_RESULT_EMPTY_DF)

    def test_method_to_test_any_mode_empty_list(self):
        with warnings.catch_warnings(record=True) as w:
            result = self.method_to_test_any_mode("RB.A12", pd.DataFrame(index=[0], data=[1], columns=["a"]), [])
            self.assertListEqual([], w)
            self.assertEqual(result, _SUCCESS_RESULT_EMPTY_DF)

    def test_method_to_test_any_mode_all_none(self):
        with warnings.catch_warnings(record=True) as w:
            result = self.method_to_test_any_mode(None, None, None)
            self.assertListEqual([], w)
            self.assertEqual(result, _SUCCESS_RESULT_EMPTY_DF)

    def test_non_existing_mode(self):
        @check_dataframe_empty(
            mode="non_existing", arg_names=[], warning=_WARNING_TEXT_EMPTY_DF, default_return=pd.DataFrame()
        )
        def method_to_test(circuit_name, u_df, u_dfs):
            return _SUCCESS_RESULT_EMPTY_DF

        with self.assertRaises(ValueError) as context:
            method_to_test(None, None, None)

        self.assertEqual("Mode takes only values in ['all', 'any'], got non_existing", str(context.exception))

    def test_no_selected_args_is_success(self):
        @check_dataframe_empty(mode="all", arg_names=[], warning=_WARNING_TEXT_EMPTY_DF, default_return=pd.DataFrame())
        def method_to_test(circuit_name, u_df, u_dfs):
            return _SUCCESS_RESULT_EMPTY_DF

        with warnings.catch_warnings(record=True) as w:
            result = self.method_to_test_any_mode(None, pd.DataFrame(index=[0], data=[1], columns=["a"]), None)
            self.assertListEqual([], w)
            self.assertEqual(result, _SUCCESS_RESULT_EMPTY_DF)


_ARGS_EMPTY = [
    (
        None,
        (
            pd.DataFrame(),
            pd.DataFrame(index=[0], data=[1], columns=["a"]),
            pd.DataFrame(index=[0], data=[1], columns=["a"]),
            [],
        ),
    ),
    (
        ["u_df_first", "u_df_third"],
        (pd.DataFrame(), pd.DataFrame(), pd.DataFrame(index=[0], data=[1], columns=["a"]), [pd.DataFrame()]),
    ),
    (
        ["u_df_first", "u_dfs"],
        (
            pd.DataFrame(index=[0], data=[1], columns=["a"]),
            pd.DataFrame(index=[0], data=[1], columns=["a"]),
            pd.DataFrame(index=[0], data=[1], columns=["a"]),
            [pd.DataFrame(index=[0], data=[1], columns=["a"]), pd.DataFrame()],
        ),
    ),
    (
        ["u_df_third", "u_df_second", "u_dfs"],
        (
            pd.DataFrame(),
            pd.DataFrame(index=[0], data=[1], columns=["a"]),
            pd.DataFrame(index=[0], data=[1], columns=["a"]),
            [pd.DataFrame()],
        ),
    ),
    (
        ["u_dfs"],
        (
            None,
            None,
            None,
            [
                pd.DataFrame(index=[0], data=[1], columns=["a"]),
                pd.DataFrame(index=[0], data=[1], columns=["a"]),
                pd.DataFrame(),
            ],
        ),
    ),
]


@pytest.mark.parametrize("arg_names,parameters", _ARGS_EMPTY)
def test_selected_args_empty(arg_names, parameters):
    @check_dataframe_empty(
        mode="any", arg_names=arg_names, warning=_WARNING_TEXT_EMPTY_DF, default_return=pd.DataFrame()
    )
    def method_to_test(u_df_first, u_df_second, u_df_third, u_dfs):
        return _SUCCESS_RESULT_EMPTY_DF

    with warnings.catch_warnings(record=True) as w:
        method_to_test(*parameters)
        assert str(w[0].message) == _WARNING_TEXT_EMPTY_DF


@pytest.mark.parametrize("arg_names,parameters", _ARGS_EMPTY)
def test_selected_args_empty_for_class_method(arg_names, parameters):
    class UnderTest:
        @check_dataframe_empty(
            mode="any", arg_names=arg_names, warning=_WARNING_TEXT_EMPTY_DF, default_return=pd.DataFrame()
        )
        def method_to_test(self, u_df_first, u_df_second, u_df_third, u_dfs):
            return _SUCCESS_RESULT_EMPTY_DF

    with warnings.catch_warnings(record=True) as w:
        UnderTest().method_to_test(*parameters)
        assert str(w[0].message) == _WARNING_TEXT_EMPTY_DF


@pytest.mark.parametrize("arg_names,parameters", _ARGS_EMPTY)
def test_selected_args_empty_for_fun_with_defaults(arg_names, parameters):
    @check_dataframe_empty(
        mode="any", arg_names=arg_names, warning=_WARNING_TEXT_EMPTY_DF, default_return=pd.DataFrame()
    )
    def method_to_test(u_df_first, u_df_second, u_df_third=pd.DataFrame(index=[0], data=[1], columns=["a"]), u_dfs=[]):
        return _SUCCESS_RESULT_EMPTY_DF

    with warnings.catch_warnings(record=True) as w:
        method_to_test(*parameters)
        assert str(w[0].message) == _WARNING_TEXT_EMPTY_DF


_ARGS_NOT_EMPTY = [
    (
        None,
        (None, pd.DataFrame(index=[0], data=[1], columns=["a"]), pd.DataFrame(index=[0], data=[1], columns=["a"]), []),
    ),
    ([], (None, pd.DataFrame(), None, [])),
    (
        ["u_df_first", "u_df_third"],
        (None, pd.DataFrame(), pd.DataFrame(index=[0], data=[1], columns=["a"]), [pd.DataFrame()]),
    ),
    (
        ["u_df_first", "u_dfs"],
        (
            pd.DataFrame(index=[0], data=[1], columns=["a"]),
            pd.DataFrame(),
            pd.DataFrame(),
            [pd.DataFrame(index=[0], data=[1], columns=["a"])],
        ),
    ),
    (
        ["u_df_third", "u_df_second"],
        (
            pd.DataFrame(),
            pd.DataFrame(index=[0], data=[1], columns=["a"]),
            pd.DataFrame(index=[0], data=[1], columns=["a"]),
            [pd.DataFrame()],
        ),
    ),
    (
        ["u_dfs"],
        (
            pd.DataFrame(),
            None,
            None,
            [pd.DataFrame(index=[0], data=[1], columns=["a"]), pd.DataFrame(index=[0], data=[1], columns=["a"])],
        ),
    ),
]


@pytest.mark.parametrize("arg_names,parameters", _ARGS_NOT_EMPTY)
def test_selected_args_not_empty(arg_names, parameters):
    @check_dataframe_empty(
        mode="any", arg_names=arg_names, warning=_WARNING_TEXT_EMPTY_DF, default_return=pd.DataFrame()
    )
    def method_to_test(u_df_first, u_df_second, u_df_third, u_dfs):
        return _SUCCESS_RESULT_EMPTY_DF

    result = method_to_test(*parameters)
    assert result == _SUCCESS_RESULT_EMPTY_DF


@pytest.mark.parametrize("arg_names,parameters", _ARGS_NOT_EMPTY)
def test_selected_args_not_for_class_method(arg_names, parameters):
    class UnderTest:
        @check_dataframe_empty(
            mode="any", arg_names=arg_names, warning=_WARNING_TEXT_EMPTY_DF, default_return=pd.DataFrame()
        )
        def method_to_test(self, u_df_first, u_df_second, u_df_third, u_dfs):
            return _SUCCESS_RESULT_EMPTY_DF

    result = UnderTest().method_to_test(*parameters)
    assert result == _SUCCESS_RESULT_EMPTY_DF


@pytest.mark.parametrize("arg_names,parameters", _ARGS_NOT_EMPTY)
def test_selected_args_not_for_fun_with_defaults(arg_names, parameters):
    @check_dataframe_empty(
        mode="any", arg_names=arg_names, warning=_WARNING_TEXT_EMPTY_DF, default_return=pd.DataFrame()
    )
    def method_to_test(u_df_first, u_df_second, u_df_third=pd.DataFrame(index=[0], data=[1], columns=["a"]), u_dfs=[]):
        return _SUCCESS_RESULT_EMPTY_DF

    result = method_to_test(*parameters)
    assert result == _SUCCESS_RESULT_EMPTY_DF


class TestTimestampNanDecorator(unittest.TestCase):
    @check_nan_timestamp(
        return_type=False, warning="Input timestamp is NaN, analysis skipped returning default return_type."
    )
    def generic_analysis_method(self, df, ts):
        return True

    def test_method_to_test_nan_parameter(self):
        with warnings.catch_warnings(record=True) as w:
            result = self.generic_analysis_method(pd.DataFrame(), np.nan)

        self.assertFalse(result)
        self.assertEqual(
            str(w[0].message),
            "In function TestTimestampNanDecorator.generic_analysis_method: Input timestamp is NaN, "
            "analysis skipped returning default return_type.",
        )

    def test_method_to_test_float_parameter(self):
        with warnings.catch_warnings(record=True) as w:
            result = self.generic_analysis_method(pd.DataFrame(), 5)

        self.assertTrue(result)
        self.assertListEqual([], w)


@check_arguments_not_none()
def generic_method(arg1, arg2="value"):
    return True


def test_check_arguments_not_none_none_parameter():
    with pytest.raises(ValueError) as e:
        generic_method(None)
    assert str(e.value) == "The following positional arguments cannot be None: arg1"

    with pytest.raises(ValueError) as e:
        generic_method(None, "value")
    assert str(e.value) == "The following positional arguments cannot be None: arg1"

    with pytest.raises(ValueError) as e:
        generic_method("value", None)
    assert str(e.value) == "The following positional arguments cannot be None: arg2"

    with pytest.raises(ValueError) as e:
        generic_method(None, None)
    assert str(e.value) == "The following positional arguments cannot be None: arg1, arg2"

    with pytest.raises(ValueError) as e:
        generic_method(arg1=None)
    assert str(e.value) == "The following keyword arguments cannot be None: arg1"


def test_check_arguments_not_none_valid_parameters():
    generic_method("value")
    generic_method("value", "value")
    generic_method(arg1="value")
    generic_method(arg1="value", arg2="value")
