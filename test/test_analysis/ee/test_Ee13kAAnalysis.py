import unittest

from lhcsmapi.analysis.ee.Ee13kAAnalysis import Ee13kAAnalysis


class TestEe13kAAnalysis(unittest.TestCase):
    def test_check_pic_ee_timestamp_difference_rqd(self):
        # GIVEN
        timestamp_pic_rqd = 1614278190680000000
        timestamp_ee_rqd = 1614278190772000000

        # WHEN
        value_range_ee_rqd = (0.085, 0.115)  # cf: https://its.cern.ch/jira/browse/SIGMON-93
        result = Ee13kAAnalysis.check_pic_ee_timestamp_difference(
            timestamp_pic_rqd, timestamp_ee_rqd, value_range_in_seconds=value_range_ee_rqd
        )

        # THEN
        self.assertTrue(result)

    def test_check_pic_ee_timestamp_difference_rqf(self):
        # GIVEN
        timestamp_pic_rqf = 1614278190678000000
        timestamp_ee_rqf = 1614278190772000000

        # WHEN
        value_range_ee_rqf = (0.085, 0.115)
        result = Ee13kAAnalysis.check_pic_ee_timestamp_difference(
            timestamp_pic_rqf, timestamp_ee_rqf, value_range_in_seconds=value_range_ee_rqf
        )

        # THEN
        self.assertTrue(result)

    def test_check_pic_ee_timestamp_difference_odd(self):
        # GIVEN
        timestamp_pic = 1614282391330000000
        timestamp_ee_odd = 1614282391422000000

        # WHEN
        value_range_ee_odd = (0.05, 0.15)  # cf: https://its.cern.ch/jira/browse/SIGMON-93
        result = Ee13kAAnalysis.check_pic_ee_timestamp_difference(
            timestamp_pic, timestamp_ee_odd, value_range_in_seconds=value_range_ee_odd
        )

        # THEN
        self.assertTrue(result)

    def test_check_pic_ee_timestamp_difference_even(self):
        # GIVEN
        timestamp_pic = 1614282391330000000
        timestamp_ee_even = 1614282391920000000

        # WHEN
        value_range_ee_even = (0.55, 0.65)
        result = Ee13kAAnalysis.check_pic_ee_timestamp_difference(
            timestamp_pic, timestamp_ee_even, value_range_in_seconds=value_range_ee_even
        )

        # THEN
        self.assertTrue(result)

    def test_check_pic_ee_timestamp_difference_raise_typeerror(self):
        # WHEN
        with self.assertRaises(TypeError):
            Ee13kAAnalysis.check_pic_ee_timestamp_difference(1, 2, value_range_in_seconds="12")
            Ee13kAAnalysis.check_pic_ee_timestamp_difference(1, 2, value_range_in_seconds=(12))
            Ee13kAAnalysis.check_pic_ee_timestamp_difference(1, 2, value_range_in_seconds=(12, 15, 89))
