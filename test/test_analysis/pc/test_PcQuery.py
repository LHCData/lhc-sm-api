import warnings

import numpy as np
import pandas as pd
import pytest

from lhcsmapi.analysis.pc.PcQuery import PcItQuery
from test.resources.read_csv import read_csv

_I_MEAS = read_csv("resources/pc", "I_MEAS")
_I_REF = read_csv("resources/pc", "I_REF")
_I_A = read_csv("resources/pc", "I_A")
_SIGNAL_NAMES = ["I_MEAS", "I_REF", "I_A"]
_TIMESTAMP = 1612968102840000000
_SOURCE = "RPHGA.UA83.RQ10.L8B1"

_PARAMS = [(np.nan, _TIMESTAMP), (_TIMESTAMP, np.nan), (np.nan, np.nan)]


@pytest.mark.parametrize("timestamp_fgc,timestamp_sync", _PARAMS)
def test_query_pc_pm_warning(timestamp_fgc, timestamp_sync):
    pc_query = PcItQuery("IPQ2", "RQ10.L8")

    with warnings.catch_warnings(record=True) as w:
        i_meas, i_ref, i_a = pc_query.query_pc_pm_with_source(
            timestamp_fgc, timestamp_sync, _SOURCE, signal_names=_SIGNAL_NAMES
        )

    assert str(w[0].message) == "timestamp_fgc or timestamp_sync are nan."
    assert all(df.empty for df in [i_meas, i_ref, i_a])


def test_query_pc_pm_with_source():
    pc_query = PcItQuery("IPQ2", "RQ10.L8")

    i_meas, i_ref, i_a = pc_query.query_pc_pm_with_source(_TIMESTAMP, _TIMESTAMP, _SOURCE, signal_names=_SIGNAL_NAMES)
    pd.testing.assert_frame_equal(_I_MEAS, i_meas)
    pd.testing.assert_frame_equal(_I_REF, i_ref)
    pd.testing.assert_frame_equal(_I_A, i_a)
