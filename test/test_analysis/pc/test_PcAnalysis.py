import unittest
import warnings
from typing import List
from unittest.mock import patch, MagicMock, ANY

import numpy as np
import pandas as pd
import pytest

from lhcsmapi.analysis.pc.PcAnalysis import Pc13kAAnalysis, PcAnalysis
from test.resources.read_csv import read_csv


class TestPcAnalysis(unittest.TestCase):
    def setUp(self) -> None:
        self.i_meas_df = read_csv("resources/hwc/rb/fpa", "i_meas_df")
        self.i_meas_ref_df = read_csv("resources/hwc/rb/fpa", "i_meas_ref_df")
        self.i_earth_pcnt_df = read_csv("resources/hwc/rb/fpa", "i_earth_pcnt_df")
        self.i_earth_pcnt_ref_df = read_csv("resources/hwc/rb/fpa", "i_earth_pcnt_ref_df")

        self.empty_df_warning_message = "At least one DataFrame is empty, I_EARTH_PCNT analysis skipped!"
        self.outside_ref_warning_message = (
            "STATUS.I_EARTH_PCNT outside of the lower: "
            "STATUS.I_EARTH_PCNT*1.0321800397390961, "
            "upper: STATUS.I_EARTH_PCNT*1.0321800397390961 reference"
        )

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    @patch("lhcsmapi.pyedsl.AssertionBuilder.AssertionBuilderSignalPlot")
    def test_analyze_i_earth_pcnt_pc_success(
        self, mock_assertionbuilder: MagicMock = None, mock_show: MagicMock = None
    ):
        with warnings.catch_warnings(record=True):
            warnings.filterwarnings("error", category=UserWarning)
            # act
            Pc13kAAnalysis.analyze_i_earth_pcnt_pc(
                "RB",
                "RB.A12",
                1544631694840000000,
                self.i_meas_df,
                self.i_meas_ref_df,
                self.i_earth_pcnt_df,
                self.i_earth_pcnt_ref_df,
            )

        if mock_show is not None:
            mock_show.assert_called()

        if mock_assertionbuilder is not None:
            mock_assertionbuilder.assert_called_once_with(ANY, ANY, True)

    def test_analyze_i_earth_pcnt_pc_plot_warning_first_df_empty(self):
        with warnings.catch_warnings(record=True) as w:
            Pc13kAAnalysis.analyze_i_earth_pcnt_pc(
                "RB",
                "RB.A12",
                1544631694840000000,
                pd.DataFrame(),
                self.i_meas_ref_df,
                self.i_earth_pcnt_df,
                self.i_earth_pcnt_ref_df,
            )

        self.assertEqual([str(warning.message) for warning in w], [self.empty_df_warning_message])

    def test_analyze_i_earth_pcnt_pc_plot_warning_second_df_empty(self):
        with warnings.catch_warnings(record=True) as w:
            Pc13kAAnalysis.analyze_i_earth_pcnt_pc(
                "RB",
                "RB.A12",
                1544631694840000000,
                self.i_meas_df,
                pd.DataFrame(),
                self.i_earth_pcnt_df,
                self.i_earth_pcnt_ref_df,
            )

        self.assertEqual([str(warning.message) for warning in w], [self.empty_df_warning_message])

    def test_analyze_i_earth_pcnt_pc_plot_warning_third_df_empty(self):
        with warnings.catch_warnings(record=True) as w:
            Pc13kAAnalysis.analyze_i_earth_pcnt_pc(
                "RB",
                "RB.A12",
                1544631694840000000,
                self.i_meas_df,
                self.i_meas_ref_df,
                pd.DataFrame(),
                self.i_earth_pcnt_ref_df,
            )

        self.assertEqual([str(warning.message) for warning in w], [self.empty_df_warning_message])

    def test_analyze_i_earth_pcnt_pc_plot_warning_fourth_df_empty(self):
        with warnings.catch_warnings(record=True) as w:
            Pc13kAAnalysis.analyze_i_earth_pcnt_pc(
                "RB",
                "RB.A12",
                1544631694840000000,
                self.i_meas_df,
                self.i_meas_ref_df,
                self.i_earth_pcnt_df,
                pd.DataFrame(),
            )

        self.assertEqual([str(warning.message) for warning in w], [self.empty_df_warning_message])

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    @patch("lhcsmapi.pyedsl.AssertionBuilder.AssertionBuilderSignalPlot")
    def test_analyze_i_earth_pcnt_pc_assert_warning(
        self, mock_assertionbuilder: MagicMock = None, mock_show: MagicMock = None
    ):
        with warnings.catch_warnings(record=True) as w:
            warnings.filterwarnings("ignore", category=DeprecationWarning)
            Pc13kAAnalysis.analyze_i_earth_pcnt_pc(
                "RB",
                "RB.A12",
                1544631694840000000,
                self.i_meas_df,
                self.i_meas_ref_df,
                self.i_earth_pcnt_df,
                self.i_earth_pcnt_ref_df,
                offset=0,
            )

        self.assertEqual([str(warning.message) for warning in w], [self.outside_ref_warning_message])

        if mock_assertionbuilder is not None:
            mock_assertionbuilder.assert_called_once_with(ANY, ANY, False)


PARAMETERS = [
    ("RQ6.R2B2", False, -0.032),
    ("RQ6.R2B1", False, -0.02),
    ("RQ6.R2B2", True, -0.037),
    ("RQ6.R2B1", True, -0.025),
]


@pytest.mark.parametrize("source,moving_average,expected_quench_start", PARAMETERS)
def test_detect_quench_start_from_i_ref_i_a(source, moving_average, expected_quench_start):
    i_ref_df = read_csv("resources/pc", f"I_REF_{source}")
    i_a_df = read_csv("resources/pc", f"I_A_{source}")

    calculated_quench_time = PcAnalysis.detect_quench_start_from_i_ref_i_a(i_a_df, i_ref_df, "RQ6.R2B2", moving_average)
    assert expected_quench_start == calculated_quench_time


@pytest.mark.parametrize("moving_average", [True, False])
def test_detect_quench_start_from_i_ref_all_nans(moving_average):
    i_ref_df = pd.DataFrame({"IAB.I_A_RQ6": [np.NAN, np.NAN, np.NAN]}, dtype=np.float64)
    i_a_df = pd.DataFrame({"ILOOP.I_REF_RQ6": [np.NAN, np.NAN, np.NAN]}, dtype=np.float64)

    calculated_quench_time = PcAnalysis.detect_quench_start_from_i_ref_i_a(i_a_df, i_ref_df, "RQ6.R2B1", moving_average)
    assert calculated_quench_time is None
