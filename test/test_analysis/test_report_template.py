import unittest
from io import StringIO
from unittest.mock import patch

from lhcsmapi.analysis.expert_input import get_expert_comment, check_show_next, get_expert_decision
from lhcsmapi.analysis.report_template import provide_text_cell_render_html, provide_container_style_html


class TestReportTemplate(unittest.TestCase):
    def test_provide_container_style_html(self):
        # arrange

        # act
        container_style_html_act = provide_container_style_html()

        # assert
        container_style_html_ref = "<style>.container { width:95% !important; }</style>"

        self.assertEqual(container_style_html_ref, container_style_html_act)

    def test_provide_text_cell_render_html(self):
        # arrange

        # act
        text_cell_render_html_act = provide_text_cell_render_html()

        # assert
        text_cell_render_html_ref = (
            "<style>div.text_cell_render {      padding: 1pt;    }    div#notebook p,    "
            "div#notebook,    div#notebook li,    p {      font-size: 9pt;      "
            "line-height: 135%;      margin: 0;    }    .rendered_html h1,    "
            ".rendered_html h1:first-child {      font-size: 14pt;      margin: 7pt 0;    }"
            "                .rendered_html h2,    .rendered_html h2:first-child {      "
            "font-size: 12pt;      margin: 6pt 0;    }    .rendered_html h3,    "
            ".rendered_html h3:first-child {      font-size: 10pt;      margin: 6pt 0;    } "
            "</style>"
        )
        self.assertEqual(text_cell_render_html_ref, text_cell_render_html_act)
