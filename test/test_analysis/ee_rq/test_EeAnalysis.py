import unittest
from pathlib import Path
import os
from unittest.mock import patch

import pandas as pd
import matplotlib.pyplot as plt

from lhcsmapi.pyedsl.AssertionBuilder import AssertionBuilder


def read_csv(file_name):
    path = Path(os.path.dirname(__file__))
    return os.path.join(str(path.parent.parent), "resources/ee_rq/{}".format(file_name))


class TestEeAnalysis(unittest.TestCase):
    def setUp(self):
        self.t_res_0_rqd_df = pd.read_csv(read_csv("DQRQ.UA23.RQD.A12_T_RES_0.csv"), index_col=0)
        self.t_res_1_rqd_df = pd.read_csv(read_csv("DQRQ.UA23.RQD.A12_T_RES_1.csv"), index_col=0)
        self.t_res_0_rqf_df = pd.read_csv(read_csv("DQRQ.UA23.RQF.A12_T_RES_0.csv"), index_col=0)
        self.t_res_1_rqf_df = pd.read_csv(read_csv("DQRQ.UA23.RQF.A12_T_RES_1.csv"), index_col=0)

        self.t_res_0_rqd_ref_df = pd.read_csv(read_csv("DQRQ.UA23.RQD.A12_T_RES_0_REF.csv"), index_col=0)
        self.t_res_1_rqd_ref_df = pd.read_csv(read_csv("DQRQ.UA23.RQD.A12_T_RES_1_REF.csv"), index_col=0)
        self.t_res_0_rqf_ref_df = pd.read_csv(read_csv("DQRQ.UA23.RQF.A12_T_RES_0_REF.csv"), index_col=0)
        self.t_res_1_rqf_ref_df = pd.read_csv(read_csv("DQRQ.UA23.RQF.A12_T_RES_1_REF.csv"), index_col=0)

    @patch("matplotlib.pyplot.show")
    def test_similarity_with_reference(self, mock_show=None):
        # arrange

        # act
        abs_margin = 25
        col = self.t_res_0_rqd_ref_df.columns[0]
        col_ref = "{}_REF".format(col)
        col_min = "{}_MIN".format(col)
        col_max = "{}_MAX".format(col)
        self.t_res_0_rqd_ref_df = self.t_res_0_rqd_ref_df.rename(columns={col: col_ref})
        idx = self.t_res_0_rqd_ref_df.index.union(self.t_res_0_rqd_df.index)
        temp_ref_ee_analysis = self.t_res_0_rqd_ref_df.reindex(idx).interpolate("index")
        temp_ref_ee_analysis = temp_ref_ee_analysis.join(self.t_res_0_rqd_df).dropna()

        temp_ref_ee_analysis[col_min] = temp_ref_ee_analysis[col_ref] - abs_margin
        temp_ref_ee_analysis[col_max] = temp_ref_ee_analysis[col_ref] + abs_margin

        ax = self.t_res_0_rqd_df.plot(figsize=(15, 7))
        self.t_res_0_rqd_ref_df.plot(ax=ax, grid=True, style="--")

        ax.fill_between(
            temp_ref_ee_analysis.index,
            temp_ref_ee_analysis[col_min],
            temp_ref_ee_analysis[col_max],
            color="xkcd:yellowgreen",
            alpha=0.5,
        )

        # Compare two curves
        temp_ref_ee_analysis["Comparison"] = temp_ref_ee_analysis.apply(
            lambda row: (row[col] >= row[col_min]) and (row[col] <= row[col_max]), axis=1
        )
        if any(temp_ref_ee_analysis["Comparison"] == False):
            print("Signal {0} outside of range [-{1}, {1}]!".format(col, abs_margin))

        ax.tick_params(labelsize=15)
        plt.show()

        if mock_show is not None:
            mock_show.assert_called()

    def test_similarity_with_reference_assertion_builder(self):
        AssertionBuilder().with_signal([self.t_res_0_rqd_df]).compare_to_reference(
            signal_ref_dfs=[self.t_res_0_rqd_ref_df], abs_margin=25
        )

    def test_similarity_with_reference_assertion_builder_multiple_signals(self):
        signal_dfs = [self.t_res_0_rqd_df, self.t_res_1_rqd_df, self.t_res_0_rqf_df, self.t_res_1_rqf_df]
        signal_ref_dfs = [
            self.t_res_0_rqd_ref_df,
            self.t_res_1_rqd_ref_df,
            self.t_res_0_rqf_ref_df,
            self.t_res_1_rqf_ref_df,
        ]
        AssertionBuilder().with_signal(signal_dfs).compare_to_reference(
            signal_ref_dfs=signal_ref_dfs, abs_margin=25, scaling=1
        )
