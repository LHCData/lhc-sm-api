import pandas as pd
import pytest

from lhcsmapi.analysis import comparison


def test_compare_features_to_reference_error():
    # arrange
    features_df = pd.DataFrame()
    circuit_type = "RB"
    system = "VF"

    # act
    with pytest.raises(KeyError) as exception:
        comparison.compare_features_to_reference(features_df, circuit_type, system)

    # assert
    assert "'System VF is not supported!'" == str(exception.value)


def test_compare_difference_of_features_to_reference_error():
    # arrange
    features_df = pd.DataFrame()
    reference_df = pd.DataFrame()
    circuit_type = "RB"
    system = "EE"
    wildcard = ""

    # act
    with pytest.raises(KeyError) as exception:
        comparison.compare_difference_of_features_to_reference(
            features_df, reference_df, circuit_type, system, wildcard
        )

    # assert
    assert "'System EE is not supported!'" == str(exception.value)
