import unittest
import warnings

import pandas as pd

from lhcsmapi.analysis.diode.DiodeLeadResistanceQuery import DiodeLeadResistanceQuery, DiodeLeadResistanceRqQuery


class TestDiodeLeadResistanceQuery(unittest.TestCase):
    RB_A81_sources = [
        "B8R8",
        "B9R8",
        "B10R8",
        "B11R8",
        "B12R8",
        "B13R8",
        "B14R8",
        "B15R8",
        "B16R8",
        "B17R8",
        "B18R8",
        "B19R8",
        "B20R8",
        "B21R8",
        "B22R8",
        "B23R8",
        "B24R8",
        "B25R8",
        "B26R8",
        "B27R8",
        "B28R8",
        "B29R8",
        "B30R8",
        "B31R8",
        "B32R8",
        "B33R8",
        "B34R8",
        "B34L1",
        "B33L1",
        "B32L1",
        "B31L1",
        "B30L1",
        "B29L1",
        "B28L1",
        "B27L1",
        "B26L1",
        "B25L1",
        "B24L1",
        "B23L1",
        "B22L1",
        "B21L1",
        "B20L1",
        "B19L1",
        "B18L1",
        "B17L1",
        "B16L1",
        "B15L1",
        "B14L1",
        "B13L1",
        "B12L1",
        "B11L1",
        "B10L1",
        "B9L1",
        "B8L1",
    ]

    RQF_A56_sources = [
        "B11L6",
        "B13L6",
        "B15L6",
        "B17L6",
        "B19L6",
        "B21L6",
        "B23L6",
        "B25L6",
        "B27L6",
        "B29L6",
        "B31L6",
        "B33L6",
        "B33R5",
        "B31R5",
        "B29R5",
        "B27R5",
        "B25R5",
        "B23R5",
        "B21R5",
        "B19R5",
        "B17R5",
        "B15R5",
        "B13R5",
        "B11R5",
        "B12R5",
        "B14R5",
        "B16R5",
        "B18R5",
        "B20R5",
        "B22R5",
        "B24R5",
        "B26R5",
        "B28R5",
        "B30R5",
        "B32R5",
        "B34R5",
        "B32L6",
        "B30L6",
        "B28L6",
        "B26L6",
        "B24L6",
        "B22L6",
        "B20L6",
        "B18L6",
        "B16L6",
        "B14L6",
        "B12L6",
        "B10L6",
    ]

    def test_check_if_found_all_buffers_RB_if_found(self):
        # arrange
        circuit_type = "RB"
        circuit_name = "RB.A81"

        query = DiodeLeadResistanceQuery(circuit_type, circuit_name)
        query_result = pd.DataFrame(
            {"source": self.RB_A81_sources, "timestamp": [1619859877184000000] * len(self.RB_A81_sources)}
        )

        # act
        with warnings.catch_warnings(record=True):
            warnings.filterwarnings("error", category=UserWarning)
            # act
            query._warn_if_pm_buffers_are_missing(query_result, 1619859877184000000, [(10, "s"), (20, "s")])

    def test_check_if_found_all_buffers_RB_if_missing(self):
        # arrange
        circuit_type = "RB"
        circuit_name = "RB.A81"
        missing_sources = ["B30L1", "B28L1", "B8R8"]
        sources = [source for source in self.RB_A81_sources if source not in missing_sources]

        query = DiodeLeadResistanceQuery(circuit_type, circuit_name)
        query_result = pd.DataFrame({"source": sources, "timestamp": [1619859877184000000] * len(sources)})

        # act
        with warnings.catch_warnings(record=True) as w:
            warnings.filterwarnings("ignore", category=DeprecationWarning)
            # act
            query._warn_if_pm_buffers_are_missing(query_result, 1619859877184000000, [(10, "s"), (20, "s")])

            # assert
            self.assertEqual(
                "Some nQPS PM buffers missing. ['B8R8', 'B30L1', 'B28L1'] not found for RB.A81 within "
                "[(10, 's'), (20, 's')] around 1619859877184000000.",
                str(w[0].message),
            )

    def test_check_if_found_all_buffers_empty_input(self):
        # arrange
        circuit_type = "RB"
        circuit_name = "RB.A81"

        query = DiodeLeadResistanceQuery(circuit_type, circuit_name)
        query_result = pd.DataFrame()

        # act
        with warnings.catch_warnings(record=True) as w:
            warnings.filterwarnings("ignore", category=DeprecationWarning)

            # act
            query._warn_if_pm_buffers_are_missing(query_result, 1619859877184000000, [(10, "s"), (20, "s")])

            # assert
            self.assertEqual(
                f"Some nQPS PM buffers missing. {self.RB_A81_sources} not found for RB.A81 "
                f"within [(10, 's'), (20, 's')] around 1619859877184000000.",
                str(w[0].message),
            )

    def test_check_if_found_all_buffers_RQ_if_found(self):
        # arrange
        circuit_type = "RQ"
        circuit_name = "RQF.A56"

        query = DiodeLeadResistanceQuery(circuit_type, circuit_name)
        query_result = pd.DataFrame(
            {"source": self.RQF_A56_sources, "timestamp": [1619859877184000000] * len(self.RQF_A56_sources)}
        )

        # act
        with warnings.catch_warnings(record=True):
            warnings.filterwarnings("error", category=UserWarning)

            # act
            query._warn_if_pm_buffers_are_missing(query_result, 1619859877184000000, [(10, "s"), (20, "s")])

    def test_check_if_found_all_buffers_RQ_if_missing(self):
        # arrange
        circuit_type = "RQ"
        circuit_name = "RQF.A56"
        missing_sources = ["B31L6", "B22R5", "B10L6"]
        sources = [source for source in self.RQF_A56_sources if source not in missing_sources]

        query = DiodeLeadResistanceQuery(circuit_type, circuit_name)
        query_result = pd.DataFrame({"source": sources, "timestamp": [1619859877184000000] * len(sources)})

        # act
        with warnings.catch_warnings(record=True) as w:
            warnings.filterwarnings("ignore", category=DeprecationWarning)

            # act
            query._warn_if_pm_buffers_are_missing(query_result, 1619859877184000000, [(10, "s"), (20, "s")])

            # assert
            self.assertEqual(
                "Some nQPS PM buffers missing. ['B31L6', 'B22R5', 'B10L6'] not found for RQF.A56 "
                "within [(10, 's'), (20, 's')] around 1619859877184000000.",
                str(w[0].message),
            )

    def test_get_u_query_metadata_for_rq_u_diode(self):
        # arrange
        circuit_type = "RQ"
        circuit_name = "RQD.A12"
        source_qds = "33L2"

        query = DiodeLeadResistanceRqQuery(circuit_type, circuit_name)

        # act
        metadata = query._get_u_query_metadata(source_qds)

        # assert
        self.assertEqual(metadata["signal"], "U_DIODE_RQD")
        self.assertEqual(metadata["source"], "B34R1")
        self.assertEqual(metadata["wildcard"], {"MAGNET": "MQ.33L2"})

    def test_get_u_query_metadata_for_rq_u_ref_n1(self):
        # arrange
        circuit_type = "RQ"
        circuit_name = "RQD.A12"
        source_qds = "33R1"

        query = DiodeLeadResistanceRqQuery(circuit_type, circuit_name)

        # act
        metadata = query._get_u_query_metadata(source_qds)

        # assert
        self.assertEqual(metadata["signal"], "U_REF_N1")
        self.assertEqual(metadata["source"], "B34R1")
        self.assertEqual(metadata["wildcard"], {"QPS_CRATE": "B34R1"})
