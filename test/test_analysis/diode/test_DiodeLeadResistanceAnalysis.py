import math
import os
import unittest
from pathlib import Path

import pandas as pd

from lhcsmapi.Time import Time
from lhcsmapi.analysis.diode.DiodeLeadResistanceAnalysis import (
    preprocess_pm_qps_signal,
    preprocess_pm_pc_signal,
    resample_pm_signal,
    calculate_resistence,
    extract_res_max_time_current_pm,
)
from lhcsmapi.pyedsl.dbsignal.SignalIndexConversion import SignalIndexConversion
from lhcsmapi.api.processing import SignalProcessing


class TestDiodeLeadResistanceAnalysis(unittest.TestCase):
    @staticmethod
    def parse_cals_signal(signal_name, magnet_name):
        path = Path(os.path.dirname(__file__))

        if "I_MEAS" in signal_name:
            file_path = os.path.join(
                str(path.parent.parent),
                "../test/resources/diode_lead_resistance/cals/I_MEAS_{}.csv".format(magnet_name),
            )
        elif "U_DIODE" in signal_name:
            file_path = os.path.join(
                str(path.parent.parent),
                "../test/resources/diode_lead_resistance/cals/U_DIODE_{}.csv".format(magnet_name),
            )
        else:
            raise KeyError(
                "signal {} not supported. Suported signal are {}".format(signal_name, ["I_MEAS", "U_DIODE_%"])
            )

        return pd.read_csv(file_path, index_col=0)

    @staticmethod
    def parse_cals_sync_signal(signal_name, magnet_name):
        path = Path(os.path.dirname(__file__))

        if signal_name == "RPHE.UA47.RQD.A45:I_MEAS":
            filePath = os.path.join(
                str(path.parent.parent),
                "../test/resources/diode_lead_resistance/cals/I_MEAS_SYNC_{}.csv".format(magnet_name),
            )
        elif signal_name == "MQ.25R4:U_DIODE_RQD":
            filePath = os.path.join(
                str(path.parent.parent),
                "../test/resources/diode_lead_resistance/cals/U_DIODE_SYNC_{}.csv".format(magnet_name),
            )
        else:
            raise KeyError(
                f"signal {signal_name} not supported. Suported signal are 'RPHE.UA47.RQD.A45:I_MEAS', 'MQ.25R4:U_DIODE_RQD'"
            )

        return pd.read_csv(filePath, index_col=0)

    @staticmethod
    def parse_analysis_output(signal_name, magnet_name):
        path = Path(os.path.dirname(__file__))

        if signal_name == "RPHE.UA47.RQD.A45:I_MEAS":
            file_path = os.path.join(
                str(path.parent.parent),
                "../test/resources/diode_lead_resistance/cals/I_MEAS_OUTPUT_{}.csv".format(magnet_name),
            )
        elif signal_name == "MQ.25R4:U_DIODE_RQD":
            file_path = os.path.join(
                str(path.parent.parent),
                "../test/resources/diode_lead_resistance/cals/U_DIODE_OUTPUT_{}.csv".format(magnet_name),
            )
        else:
            raise KeyError(
                f"signal {signal_name} not supported. Suported signal are 'RPHE.UA47.RQD.A45:I_MEAS', 'MQ.25R4:U_DIODE_RQD'"
            )

        return pd.read_csv(file_path, index_col=0)

    @staticmethod
    def parseCalsSyncResampleSignal(signal_name, magnet_name):
        path = Path(os.path.dirname(__file__))

        if signal_name == "RPHE.UA47.RQD.A45:I_MEAS":
            file_path = os.path.join(
                str(path.parent.parent),
                f"../test/resources/diode_lead_resistance/cals/I_MEAS_SYNC_RESAMPLE_{magnet_name}.csv",
            )
        else:
            raise KeyError(f"signal {signal_name} not supported. Suported signal is 'RPHE.UA47.RQD.A45:I_MEAS'")

        return pd.read_csv(file_path, index_col=0)

    @staticmethod
    def parse_pm_signal(signal_name):
        path = Path(os.path.dirname(__file__))
        try:
            file_path = os.path.join(
                str(path.parent.parent), f"../test/resources/diode_lead_resistance/pm/{signal_name}.csv"
            )
            return pd.read_csv(file_path, index_col=0)
        except:
            raise KeyError("signal {} not supported".format(signal_name))

    def test_i_meas_sync_values(self):
        # arrange
        signalName = "RPHE.UA47.RQD.A45:I_MEAS"
        timestamp = "2018-12-09 09:55:47.668"
        timestampUnixSec = Time.to_unix_timestamp_in_sec(timestamp)
        magnetName = "MQ.25R4"

        # act
        i_meas = self.parse_cals_signal(signalName, magnetName)
        i_meas_sync_act = SignalIndexConversion.synchronize_df(i_meas, timestampUnixSec)

        # assert
        i_meas_sync_exp = self.parse_cals_sync_signal(signalName, magnetName)
        self.assertListEqual(list(i_meas_sync_exp[signalName].values), list(i_meas_sync_act[signalName].values))

    def test_i_meas_sync_index(self):
        # arrange
        signalName = "RPHE.UA47.RQD.A45:I_MEAS"
        timestamp = "2018-12-09 09:55:47.668"
        timestampUnixSec = Time.to_unix_timestamp_in_sec(timestamp)
        magnetName = "MQ.25R4"

        # act
        i_meas = self.parse_cals_signal(signalName, magnetName)
        i_meas_sync_act = SignalIndexConversion.synchronize_df(i_meas, timestampUnixSec)

        # assert
        i_meas_sync_exp = self.parse_cals_sync_signal(signalName, magnetName)
        for index_exp, index_act in zip(
            list(i_meas_sync_exp[signalName].index), list(i_meas_sync_act[signalName].index)
        ):
            self.assertAlmostEqual(index_exp, index_act, places=6)

    def test_u_diode_sync_values(self):
        # arrange
        signalName = "MQ.25R4:U_DIODE_RQD"
        timestamp = "2018-12-09 09:55:47.668"
        timestampUnixSec = Time.to_unix_timestamp_in_sec(timestamp)
        magnetName = "MQ.25R4"

        # act
        u_diode = self.parse_cals_signal(signalName, magnetName)
        u_diode_sync_act = SignalIndexConversion.synchronize_df(u_diode, timestampUnixSec)

        # assert
        u_diode_sync_exp = self.parse_cals_sync_signal(signalName, magnetName)
        self.assertListEqual(list(u_diode_sync_exp[signalName].values), list(u_diode_sync_act[signalName].values))

    def test_u_diode_sync_index(self):
        # arrange
        signalName = "MQ.25R4:U_DIODE_RQD"
        timestamp = "2018-12-09 09:55:47.668"
        timestampUnixSec = Time.to_unix_timestamp_in_sec(timestamp)
        magnetName = "MQ.25R4"

        # act
        i_meas = self.parse_cals_signal(signalName, magnetName)
        i_meas_sync_act = SignalIndexConversion.synchronize_df(i_meas, timestampUnixSec)

        # assert
        i_meas_sync_ext = self.parse_cals_sync_signal(signalName, magnetName)
        for index_exp, index_act in zip(
            list(i_meas_sync_ext[signalName].index), list(i_meas_sync_act[signalName].index)
        ):
            self.assertAlmostEqual(index_exp, index_act, places=6)

    def test_i_meas_sync_resample_index(self):
        # arrange
        signalName = "RPHE.UA47.RQD.A45:I_MEAS"
        timestamp = "2018-12-09 09:55:47.668"
        timestampUnixSec = Time.to_unix_timestamp_in_sec(timestamp)
        magnetName = "MQ.25R4"

        # act
        i_meas = self.parse_cals_signal(signalName, magnetName)
        i_meas_sync = SignalIndexConversion.synchronize_df(i_meas, timestampUnixSec)
        i_meas_sync_resample_act = SignalProcessing(i_meas_sync).resample(ts=0.1, t0=-50, t_end=150).get_dataframes()

        # assert
        i_meas_sync_resample_exp = self.parseCalsSyncResampleSignal(signalName, magnetName)
        for index_exp, index_act in zip(
            list(i_meas_sync_resample_exp[signalName].index), list(i_meas_sync_resample_act[signalName].index)
        ):
            self.assertAlmostEqual(index_exp, index_act, places=10)

    def test_i_meas_sync_resample_values(self):
        # arrange
        signalName = "RPHE.UA47.RQD.A45:I_MEAS"
        timestamp = "2018-12-09 09:55:47.668"
        timestampUnixSec = Time.to_unix_timestamp_in_sec(timestamp)
        magnetName = "MQ.25R4"

        # act
        i_meas = self.parse_cals_signal(signalName, magnetName)
        i_meas_sync = SignalIndexConversion.synchronize_df(i_meas, timestampUnixSec)
        i_meas_sync_resample_act = SignalProcessing(i_meas_sync).resample(ts=0.1, t0=-50, t_end=150).get_dataframes()

        # assert
        i_meas_sync_resample_exp = self.parseCalsSyncResampleSignal(signalName, magnetName)
        for index_exp, index_act in zip(
            list(i_meas_sync_resample_exp[signalName].values), list(i_meas_sync_resample_act[signalName].values)
        ):
            if math.isnan(index_exp):
                pass
            else:
                self.assertAlmostEqual(index_exp, index_act, places=3)

    def test_u_diode_sync_resample_index(self):
        """
        The goal of this test is to compare the resampled indices of circuit current and diode voltage.
        This is needed in order to concatenate resampled signals.
        """
        # arrange
        signalName = "MQ.25R4:U_DIODE_RQD"
        timestamp = "2018-12-09 09:55:47.668"
        timestampUnixSec = Time.to_unix_timestamp_in_sec(timestamp)
        magnetName = "MQ.25R4"

        # act
        i_meas = self.parse_cals_signal(signalName, magnetName)
        i_meas_sync = SignalIndexConversion.synchronize_df(i_meas, timestampUnixSec)
        i_meas_sync_resample_act = SignalProcessing(i_meas_sync).resample(ts=0.1, t0=-50, t_end=150).get_dataframes()

        # assert
        i_meas_sync_resample_exp = self.parseCalsSyncResampleSignal("RPHE.UA47.RQD.A45:I_MEAS", magnetName)
        for index_exp, index_act in zip(
            list(i_meas_sync_resample_exp["RPHE.UA47.RQD.A45:I_MEAS"].index),
            list(i_meas_sync_resample_act[signalName].index),
        ):
            self.assertAlmostEqual(index_exp, index_act, places=10)

    def test_u_res_calculation(self):
        # arrange
        U_DIODE = "MQ.25R4:U_DIODE_RQD"
        magnetName = "MQ.25R4"

        # act
        uDiode = self.parse_cals_sync_signal(U_DIODE, magnetName)
        uD = uDiode[U_DIODE]
        uRes = []
        # "5" - toggling period of nQPS U_DIODE signals
        # "50s x 10Hz + 20" delay to the diode full opening
        for i in range(0, uD.size):
            if i < 520 or i > uD.size - 6:
                uRes.append(0)
            else:
                uRes.append(abs(uD.iloc[i] - (uD.iloc[i - 5] + uD.iloc[i + 5]) / 2))

        # assert
        uD_exp = self.parse_analysis_output(U_DIODE, magnetName)
        uRes_exp = list(uD_exp["uRes"].values)
        for u_res_exp, u_res_act in zip(uRes_exp, uRes):
            self.assertAlmostEqual(u_res_exp, u_res_act, places=10)

    def test_u_res_calculation_rolling(self):
        # arrange
        U_DIODE = "MQ.25R4:U_DIODE_RQD"
        magnetName = "MQ.25R4"

        # act
        uDiode = self.parse_cals_sync_signal(U_DIODE, magnetName)
        calc_u_res = lambda x: abs(x[5] - (x[0] + x[-1]) / 2)
        uDiode["uRes2"] = uDiode["MQ.25R4:U_DIODE_RQD"].rolling(11).apply(calc_u_res, raw=True)
        uDiode.loc[uDiode.index < uDiode.index[520], "uRes2"] = 0
        uRes_act = list(uDiode["uRes2"].values)

        # assert
        uD_exp = self.parse_analysis_output(U_DIODE, magnetName)
        uRes_exp = list(uD_exp["uRes"].values)
        for u_res_exp, u_res_act in zip(uRes_exp, uRes_act):
            self.assertAlmostEqual(u_res_exp, u_res_act, places=0)

    def test_u_d_min_calculation(self):
        # arrange
        U_DIODE = "MQ.25R4:U_DIODE_RQD"
        magnetName = "MQ.25R4"

        # act
        uDiode = self.parse_cals_sync_signal(U_DIODE, magnetName)
        uD = uDiode[U_DIODE]
        uD_min = []
        # "5" - toggling period of nQPS U_DIODE signals
        # "50s x 10Hz + 20" delay to the diode full opening
        for i in range(0, uD.size):
            if i < 520 or i > uD.size - 6:
                uD_min.append(0)
            else:
                uD_min.append(min((uD.iloc[i - 5] + uD.iloc[i + 5]) / 2, uD.iloc[i]))

        # assert
        uD_exp = self.parse_analysis_output(U_DIODE, magnetName)
        uDmin_exp = list(uD_exp["uD_min"].values)
        for u_d_min_exp, u_d_min_act in zip(uDmin_exp, uD_min):
            self.assertAlmostEqual(u_d_min_exp, u_d_min_act, places=10)

    def test_u_d_min_calculation_rolling(self):
        # arrange
        U_DIODE = "MQ.25R4:U_DIODE_RQD"
        magnetName = "MQ.25R4"

        # act
        uDiode = self.parse_cals_sync_signal(U_DIODE, magnetName)
        calc_u_diode_min = lambda x: min((x[0] + x[-1]) / 2, x[5])
        uDiode["uD_min2"] = uDiode["MQ.25R4:U_DIODE_RQD"].rolling(11).apply(calc_u_diode_min, raw=True)
        uDiode.loc[uDiode.index < uDiode.index[520], "uD_min2"] = 0
        uDmin_act = list(uDiode["uD_min2"].values)

        # assert
        uD_exp = self.parse_analysis_output(U_DIODE, magnetName)
        uDmin_exp = list(uD_exp["uD_min"].values)
        for u_d_min_exp, u_d_min_act in zip(uDmin_exp, uDmin_act):
            self.assertAlmostEqual(u_d_min_exp, u_d_min_act, places=0)

    def test_U_DIODE_RQD_A(self):
        # arrange
        u_diode_rqd_a = self.parse_pm_signal("U_DIODE_RQD_A")
        u_diode_rqd_a_name = "MQ.25R4:U_DIODE_RQD"
        timestamp_iqps_qh = "2018-12-09 09:55:47.668"
        timestamp_unix = Time.to_unix_timestamp(timestamp_iqps_qh)
        time_corr = 0.0

        # act
        preprocess_pm_qps_signal(time_corr, timestamp_unix, u_diode_rqd_a, u_diode_rqd_a_name)

        # assert
        u_diode_rqd_a_processed = self.parse_pm_signal("U_DIODE_RQD_A_Processed")

        self.assertListEqual(
            list(u_diode_rqd_a_processed[u_diode_rqd_a_name].values), list(u_diode_rqd_a[u_diode_rqd_a_name].values)
        )

    def test_U_DIODE_RQD_B(self):
        # arrange
        u_diode_rqd_b = self.parse_pm_signal("U_DIODE_RQD_B")
        u_diode_rqd_b_name = "MQ.25R4:U_DIODE_RQD"
        timestamp_iqps_qh = "2018-12-09 09:55:47.668"
        timestamp_unix = Time.to_unix_timestamp(timestamp_iqps_qh)
        time_corr = 0.0

        # act
        preprocess_pm_qps_signal(time_corr, timestamp_unix, u_diode_rqd_b, u_diode_rqd_b_name)

        # assert
        u_deiode_rqd_b_processed = self.parse_pm_signal("U_DIODE_RQD_B_Processed")

        self.assertListEqual(
            list(u_deiode_rqd_b_processed[u_diode_rqd_b_name].values), list(u_diode_rqd_b[u_diode_rqd_b_name].values)
        )

    def test_U_REF_N1(self):
        # arrange
        u_ref_n1 = self.parse_pm_signal("U_REF_N1")
        u_ref_n1_name = "DQQDS.B26R4.RQD.A45:U_REF_N1"
        timestamp_iqps_qh = "2018-12-09 09:55:47.668"
        timestamp_unix = Time.to_unix_timestamp(timestamp_iqps_qh)
        time_corr = 0.0

        # act
        preprocess_pm_qps_signal(time_corr, timestamp_unix, u_ref_n1, u_ref_n1_name)

        # assert
        u_ref_n1_processed = self.parse_pm_signal("U_REF_N1_Processed")

        self.assertListEqual(list(u_ref_n1_processed[u_ref_n1_name].values), list(u_ref_n1[u_ref_n1_name].values))

    def test_IAB_I_A(self):
        # arrange
        u_diode_rqd_a = self.parse_pm_signal("U_DIODE_RQD_A")
        u_diode_rqd_a_name = "MQ.25R4:U_DIODE_RQD"

        iab_i_a = self.parse_pm_signal("IAB.I_A")
        iab_i_a_name = "IAB.I_A"
        timestamp_iqps_qh = "2018-12-09 09:55:47.668"
        timestamp_unix = Time.to_unix_timestamp(timestamp_iqps_qh)
        time_corr = 0.0

        # act
        u_diode_rqd_a = preprocess_pm_qps_signal(time_corr, timestamp_unix, u_diode_rqd_a, u_diode_rqd_a_name)

        index_start_diode = u_diode_rqd_a.index[0]
        index_end_diode = u_diode_rqd_a.index[-1]
        iab_i_a = preprocess_pm_pc_signal(iab_i_a, iab_i_a_name, index_end_diode, index_start_diode, timestamp_unix)

        # assert
        iab_i_a_processed = self.parse_pm_signal("IAB.I_A_Processed")

        self.assertListEqual(list(iab_i_a_processed[iab_i_a_name].values), list(iab_i_a[iab_i_a_name].values))

    def test_calculate_u_res_res_pm(self):
        # arrange
        u_diode_rqd_a = self.parse_pm_signal("U_DIODE_RQD_A")
        u_diode_rqd_a_name = "MQ.25R4:U_DIODE_RQD"

        u_diode_rqd_b = self.parse_pm_signal("U_DIODE_RQD_B")
        u_diode_rqd_b_name = "MQ.25R4:U_DIODE_RQD"

        iab_i_a = self.parse_pm_signal("IAB.I_A")
        iab_i_a_name = "IAB.I_A"

        timestamp_iqps_qh = "2018-12-09 09:55:47.668"
        timestamp_unix = Time.to_unix_timestamp(timestamp_iqps_qh)
        time_corr = 0.0

        # act
        u_diode_rqd_a = preprocess_pm_qps_signal(time_corr, timestamp_unix, u_diode_rqd_a, u_diode_rqd_a_name)
        u_diode_rqd_a_resampled = resample_pm_signal(u_diode_rqd_a)
        #
        u_diode_rqd_b = preprocess_pm_qps_signal(time_corr, timestamp_unix, u_diode_rqd_b, u_diode_rqd_b_name)
        u_diode_rqd_b_resampled = resample_pm_signal(u_diode_rqd_b)

        index_start_diode = u_diode_rqd_a.index[0]
        index_end_diode = u_diode_rqd_a.index[-1]
        iab_i_a = preprocess_pm_pc_signal(iab_i_a, iab_i_a_name, index_end_diode, index_start_diode, timestamp_unix)
        iab_i_a_resampled = resample_pm_signal(iab_i_a)

        #
        t_max = u_diode_rqd_a.idxmax(axis=0, skipna=True)[0]
        #
        # DataFrame operation
        uD = pd.DataFrame()
        uD["U_DIODE_A"] = u_diode_rqd_a_resampled[u_diode_rqd_a_name]
        uD["U_DIODE_B"] = u_diode_rqd_b_resampled[u_diode_rqd_b_name].values
        uD["IAB.I_A"] = iab_i_a_resampled[iab_i_a_name].values

        calculate_resistence(t_max, uD)

        # assert
        u_res = self.parse_pm_signal("U_RES")
        for u_d_min_exp, u_d_min_act in zip(list(u_res["U_RES"].values), list(uD["U_RES"].values)):
            self.assertAlmostEqual(u_d_min_exp, u_d_min_act, places=0)

        res = self.parse_pm_signal("RES")
        for u_d_min_exp, u_d_min_act in zip(list(res["RES"].values), list(uD["RES"].values)):
            self.assertAlmostEqual(u_d_min_exp, u_d_min_act / 1e6, places=5)

        res_max, t_res_max, i_res_max = extract_res_max_time_current_pm(uD)
        self.assertEqual(32, res_max)
        self.assertAlmostEqual(0.471735280772, t_res_max, places=6)
        self.assertAlmostEqual(11157.375496, i_res_max, places=6)
