import os
from pathlib import Path
import unittest
from unittest.mock import patch

import matplotlib.pyplot as plt
import pandas as pd
import pytest

from lhcsmapi.analysis.busbar.BusbarResistanceAnalysis import (
    BusbarResistanceAnalysis,
    MultipleBusbarResistanceAnalysis,
    find_start_end_of_the_longest_ramp_up,
    extract_intersecting_plateaus,
)
from test.resources.read_csv import read_csv


class TestBusbarResistanceAnalysis(unittest.TestCase):
    @patch("matplotlib.pyplot.show")
    def test_find_plateau_start_and_end_ipd(self, mock_show=None):
        # arrange
        i_meas_raw_df = read_csv("resources/hwc/ipd/pno.a8", "I_MEAS_RAW")

        # act
        plateau_start_act, plateau_end_act = BusbarResistanceAnalysis.find_plateau_start_and_end(
            i_meas_raw_df, i_meas_threshold=0, min_duration_in_sec=0
        )
        ax = i_meas_raw_df.plot()
        for ps, pe in zip(plateau_start_act, plateau_end_act):
            ax.axvspan(xmin=ps, xmax=pe, facecolor="xkcd:green")
        plt.show()

        # assert
        plateau_start_ref = [1424378585028884480, 1424378956398406400, 1424379410850597632, 1424379502942231040]
        plateau_end_ref = [1424378645088645376, 1424379015457171200, 1424379470910358528, 1424379561000000000]

        self.assertListEqual(plateau_start_ref, plateau_start_act)
        self.assertListEqual(plateau_end_ref, plateau_end_act)

        if mock_show is not None:
            mock_show.assert_called()

    @patch("matplotlib.pyplot.show")
    def test_find_plateau_start_and_end(self, mock_show=None):
        # arrange
        i_meas_raw_df = read_csv("resources/busbar", "I_MEAS")

        # act
        plateau_start_act, plateau_end_act = BusbarResistanceAnalysis.find_plateau_start_and_end(
            i_meas_raw_df, i_meas_threshold=50
        )

        ax = i_meas_raw_df.plot()
        for ps, pe in zip(plateau_start_act, plateau_end_act):
            ax.axvspan(xmin=ps, xmax=pe, facecolor="xkcd:green")
        plt.show()

        # assert
        plateau_start_ref = [1523190228160205568, 1523190720261294336, 1523193296790573312]
        plateau_end_ref = [1523190645245884416, 1523192207566821376, 1523195095160000000]

        self.assertListEqual(plateau_start_ref, plateau_start_act)
        self.assertListEqual(plateau_end_ref, plateau_end_act)

        if mock_show is not None:
            mock_show.assert_called()

    @patch("matplotlib.pyplot.show")
    def test_find_plateau_start_and_end_it_i_meas_rqx(self, mock_show=None):
        # arrange
        i_meas_raw_df = read_csv("resources/busbar", "I_MEAS_RQX")

        # act
        plateau_start_act, plateau_end_act = BusbarResistanceAnalysis.find_plateau_start_and_end(
            i_meas_raw_df, i_meas_threshold=0, min_duration_in_sec=60
        )

        ax = i_meas_raw_df.plot()
        for ps, pe in zip(plateau_start_act, plateau_end_act):
            ax.axvspan(xmin=ps, xmax=pe, facecolor="xkcd:green")
        plt.show()

        # assert
        plateau_start_ref = [1491935974257160192, 1491937723668302848]
        plateau_end_ref = [1491937185541833472, 1491941322514094848]

        self.assertListEqual(plateau_start_ref, plateau_start_act)
        self.assertListEqual(plateau_end_ref, plateau_end_act)

        if mock_show is not None:
            mock_show.assert_called()

    @patch("matplotlib.pyplot.show")
    def test_find_plateau_start_and_end_it_i_meas_rtqx1(self, mock_show=None):
        # arrange
        i_meas_raw_df = read_csv("resources/busbar", "I_MEAS_RTQX1")

        # act
        plateau_start_act, plateau_end_act = BusbarResistanceAnalysis.find_plateau_start_and_end(
            i_meas_raw_df, i_meas_threshold=0, min_duration_in_sec=60
        )

        ax = i_meas_raw_df.plot()
        for ps, pe in zip(plateau_start_act, plateau_end_act):
            ax.axvspan(xmin=ps, xmax=pe, facecolor="xkcd:green")
        plt.show()

        # assert
        plateau_start_ref = [1491935990250924544, 1491937381471159040, 1491941340097821440]
        plateau_end_ref = [1491937186440284928, 1491941329096079872, 1491942238240000000]

        self.assertListEqual(plateau_start_ref, plateau_start_act)
        self.assertListEqual(plateau_end_ref, plateau_end_act)

        if mock_show is not None:
            mock_show.assert_called()

    @patch("matplotlib.pyplot.show")
    def test_find_plateau_start_and_end_it_i_meas_rtqx2(self, mock_show=None):
        # arrange
        i_meas_raw_df = read_csv("resources/busbar", "I_MEAS_RTQX2")

        # act
        plateau_start_act, plateau_end_act = BusbarResistanceAnalysis.find_plateau_start_and_end(
            i_meas_raw_df, i_meas_threshold=0, min_duration_in_sec=60
        )

        ax = i_meas_raw_df.plot()
        for ps, pe in zip(plateau_start_act, plateau_end_act):
            ax.axvspan(xmin=ps, xmax=pe, facecolor="xkcd:green")
        plt.show()

        # assert
        plateau_start_ref = [1491935981753507072, 1491937726166777344, 1491941932163222784]
        plateau_end_ref = [1491937182037867264, 1491941326019620864, 1491942256240000000]

        self.assertListEqual(plateau_start_ref, plateau_start_act)
        self.assertListEqual(plateau_end_ref, plateau_end_act)

        if mock_show is not None:
            mock_show.assert_called()

    @patch("matplotlib.pyplot.show")
    def test_find_plateau_start_and_end_it(self, mock_show=None):
        # arrange
        i_meas_raw_dfs = [
            read_csv("resources/busbar", "I_MEAS_RQX"),
            read_csv("resources/busbar", "I_MEAS_RTQX1"),
            read_csv("resources/busbar", "I_MEAS_RTQX2"),
        ]

        # act
        plateau_starts = []
        plateau_ends = []
        for i_meas_raw_df in i_meas_raw_dfs:
            plateau_start, plateau_end = BusbarResistanceAnalysis.find_plateau_start_and_end(
                i_meas_raw_df, i_meas_threshold=0, min_duration_in_sec=10
            )
            plateau_starts.append(plateau_start)
            plateau_ends.append(plateau_end)

        plateau_start_act, plateau_end_act = extract_intersecting_plateaus(plateau_starts, plateau_ends)
        for i_meas_raw_df in i_meas_raw_dfs:
            ax = i_meas_raw_df.plot(figsize=(15, 7))
            for ps, pe in zip(plateau_start_act, plateau_end_act):
                ax.axvspan(xmin=ps, xmax=pe, facecolor="xkcd:green")

        plt.show()

        # assert
        plateau_start_ref = [1491935990250924544, 1491937726166777344]
        plateau_end_ref = [1491937182037867264, 1491941322514094848]

        self.assertListEqual(plateau_start_ref, plateau_start_act)
        self.assertListEqual(plateau_end_ref, plateau_end_act)

        if mock_show is not None:
            mock_show.assert_called()

    @patch("matplotlib.pyplot.show")
    def test_find_start_end_of_the_longest_ramp_up(self, mock_show=None):
        # arrange
        i_meas_raw_df = read_csv("resources/busbar", "I_MEAS")

        # act
        plateau_start, plateau_end = BusbarResistanceAnalysis.find_plateau_start_and_end(
            i_meas_raw_df, i_meas_threshold=50
        )

        ramp_up_start_act, ramp_up_end_act = find_start_end_of_the_longest_ramp_up(plateau_start, plateau_end)

        ax = i_meas_raw_df.plot()
        for ps, pe in zip(plateau_start, plateau_end):
            ax.axvspan(xmin=ps, xmax=pe, facecolor="xkcd:green")

        ax.axvspan(xmin=ramp_up_start_act, xmax=ramp_up_end_act, facecolor="xkcd:yellowgreen")
        plt.show()

        # assert
        ramp_up_start_ref = 1523192207566821376
        ramp_up_end_ref = 1523193296790573312

        self.assertEqual(ramp_up_start_ref, ramp_up_start_act)
        self.assertEqual(ramp_up_end_ref, ramp_up_end_act)

        if mock_show is not None:
            mock_show.assert_called()

    def test_extract_intersecting_plateaus(self):
        # arrange
        plateau_starts = [[8, 1399, 5358], [8, 1399, 5358], [0, 1744, 5950]]
        plateau_ends = [[1104, 5247, 6156], [1104, 5247, 6156], [1100, 5244, 6174]]

        # act
        intersect_plateaus_start_act, intersect_plateaus_end_act = extract_intersecting_plateaus(
            plateau_starts, plateau_ends
        )

        # assert
        intersect_plateaus_start_ref = [8, 1744, 5950]
        intersect_plateaus_end_ref = [1100, 5244, 6156]
        self.assertEqual(intersect_plateaus_start_ref, intersect_plateaus_start_act)
        self.assertEqual(intersect_plateaus_end_ref, intersect_plateaus_end_act)

    def test_extract_intersecting_plateaus2(self):
        # arrange
        plateau_starts = [
            [1491936034257160192, 1491937783668302848],
            [1491936050250924544, 1491937441471159040, 1491941400097821440],
            [1491936041753507072, 1491937786166777344, 1491941992163222784],
        ]

        plateau_ends = [
            [1491937145541833472, 1491941282514094848],
            [1491937146440284928, 1491941289096079872, 1491942198240000000],
            [1491937142037867264, 1491941286019620864, 1491942216240000000],
        ]

        intersect_plateaus_start_act, intersect_plateaus_end_act = extract_intersecting_plateaus(
            plateau_starts, plateau_ends
        )

        # assert
        intersect_plateaus_start_ref = [1491936050250924544, 1491937786166777344]
        intersect_plateaus_end_ref = [1491937142037867264, 1491941282514094848]
        self.assertEqual(intersect_plateaus_start_ref, intersect_plateaus_start_act)
        self.assertEqual(intersect_plateaus_end_ref, intersect_plateaus_end_act)

    def test_calculate_resistance(self):
        path = Path(os.path.dirname(__file__))

        i_meas_feature_df = read_csv("resources/busbar", "I_MEAS_FEATURE")
        u_res_feature_df = pd.read_csv(
            os.path.join(path.parent.parent, "resources/busbar/U_RES_FEATURE.csv"),
            index_col=[0, 1],
            names=["nxcals_variable_name", "class", "std", "mean"],
            header=0,
        )

        result = MultipleBusbarResistanceAnalysis("RB").calculate_resistance(
            i_meas_feature_df, u_res_feature_df, "U_RES", 1418312987759000000, "RB.A12"
        )
        expected = pd.read_csv(
            os.path.join(path.parent.parent, "resources/busbar/RESISTANCE.csv"), index_col=0, dtype="float64"
        )

        expected.index = pd.Index(expected.index, dtype="int")

        pd.testing.assert_frame_equal(expected, result, check_names=False, check_dtype=False)


PLATEAUS = [
    ([[1, 5], [2]], [[3, 7], [6]], [2, 5], [3, 6]),
    ([[1, 5], [1, 7]], [[3, 7], [5, 8]], [1], [3]),
    ([[1], [2], [1, 5]], [[8], [7], [4, 7]], [2, 5], [4, 7]),
    ([[3], [2], [1, 6], [3]], [[7], [9], [4, 8], [7]], [3, 6], [4, 7]),
]


@pytest.mark.parametrize("plateaus_starts,plateaus_ends,expected_plateau_starts,expected_plateau_ends", PLATEAUS)
def test_find_intersecting_plateaus(plateaus_starts, plateaus_ends, expected_plateau_starts, expected_plateau_ends):
    starts, ends = extract_intersecting_plateaus(plateaus_starts, plateaus_ends)
    assert starts == expected_plateau_starts
    assert ends == expected_plateau_ends
