import unittest
from pathlib import Path
import os
import pandas as pd

from lhcsmapi.metadata.MappingMetadata import MappingMetadata
from lhcsmapi.analysis.qds.QdsAnalysis import (
    find_circuit_name_with_first_quenched_magnet,
    choose_u_qs0,
    find_t_start_t_end_voltage_slope,
    calculate_voltage_slope,
)


def read_csv(file_name):
    path = Path(os.path.dirname(__file__))
    return os.path.join(str(path.parent.parent), "resources/iqps_rq/{}".format(file_name))


class TestIqpsAnalysis(unittest.TestCase):
    def setUp(self):
        self.u_diode_rqd_df = pd.read_csv(read_csv("MQ.16L2_U_DIODE_RQD.csv"), index_col=0)
        self.u_diode_rqf_df = pd.read_csv(read_csv("MQ.16L2_U_DIODE_RQF.csv"), index_col=0)
        self.u_qs0_ext_rq_df = pd.read_csv(read_csv("16L2_U_QS0_EXT.csv"), index_col=0)
        self.u_qs0_int_rq_df = pd.read_csv(read_csv("16L2_U_QS0_INT.csv"), index_col=0)

    def test_find_circuit_name_with_first_quenched_magnet(self):
        # arrange
        circuit_names = ["RQD.A12", "RQF.A12"]

        # act
        circuit_name_act = find_circuit_name_with_first_quenched_magnet(
            circuit_names, self.u_diode_rqd_df, self.u_diode_rqf_df
        )

        # assert
        circuit_name_exp = "RQF.A12"
        self.assertEqual(circuit_name_exp, circuit_name_act)

    def test_get_rq_aperture_from_circuit_name_and_magnet_name(self):
        # arrange
        circuit_names = ["RQD.A12", "RQF.A12"]
        cell = "16L2"
        magnet_name = "MQ.{}".format(cell)

        # act
        circuit_name = find_circuit_name_with_first_quenched_magnet(
            circuit_names, self.u_diode_rqd_df, self.u_diode_rqf_df
        )
        aperture_act = MappingMetadata.get_rq_aperture(circuit_name, magnet_name)

        # assert
        aperture_exp = "INT"
        self.assertEqual(aperture_exp, aperture_act)

    def test_find_t_start_t_end_voltage_slope(self):
        # arrange
        circuit_names = ["RQD.A12", "RQF.A12"]
        cell = "16L2"
        magnet_name = "MQ.{}".format(cell)

        # act
        circuit_name = find_circuit_name_with_first_quenched_magnet(
            circuit_names, self.u_diode_rqd_df, self.u_diode_rqf_df
        )
        aperture = MappingMetadata.get_rq_aperture(circuit_name, magnet_name)
        u_qs0_aperture_df = choose_u_qs0(aperture, self.u_qs0_int_rq_df, self.u_qs0_ext_rq_df)
        t_slope_start_act, t_slope_end_act = find_t_start_t_end_voltage_slope(u_qs0_aperture_df)

        # assert
        t_slope_start_exp = -0.034
        t_slope_end_exp = -0.024

        self.assertEqual(t_slope_start_exp, t_slope_start_act)
        self.assertEqual(t_slope_end_act, t_slope_end_exp)

    def test_calculate_voltage_slope(self):
        # arrange
        circuit_names = ["RQD.A12", "RQF.A12"]
        cell = "16L2"
        magnet_name = "MQ.{}".format(cell)

        # act
        circuit_name = find_circuit_name_with_first_quenched_magnet(
            circuit_names, self.u_diode_rqd_df, self.u_diode_rqf_df
        )
        aperture = MappingMetadata.get_rq_aperture(circuit_name, magnet_name)
        u_qs0_aperture_df = choose_u_qs0(aperture, self.u_qs0_int_rq_df, self.u_qs0_ext_rq_df)
        t_slope_start, t_slope_end = find_t_start_t_end_voltage_slope(u_qs0_aperture_df)
        du_dt_act = calculate_voltage_slope(u_qs0_aperture_df, t_slope_start, t_slope_end)

        # assert
        du_dt_exp = -7.7758588999999985
        self.assertAlmostEqual(du_dt_exp, du_dt_act, places=6)
