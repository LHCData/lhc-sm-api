import unittest
import warnings
from io import StringIO
from unittest.mock import patch

import numpy as np
import pandas as pd

from lhcsmapi.Time import Time
from lhcsmapi.analysis.ItCircuitAnalysis import ItCircuitAnalysis
from test.resources.read_csv import read_csv


class TestItCircuitAnalysis(unittest.TestCase):
    _results_table = pd.DataFrame(
        {
            "Circuit Name": {0: "RQX.R1"},
            "Circuit Family": {0: "RQX"},
            "Period": {0: "Operation 2018"},
            "Date (FGC_RQX)": {0: "2018-08-29"},
            "Time (FGC_RQX)": {0: "17:59:33.560"},
            "FPA Reason": {0: "Magnet quench"},
            "Timestamp_PIC": {0: np.nan},
            "Delta_t(FGC_RQX-PIC)": {0: np.nan},
            "Ramp rate RQX": {0: 0},
            "Ramp rate RTQX1": {0: 0},
            "Ramp rate RTQX2": {0: 0},
            "Plateau duration RQX": {0: 359.98},
            "Plateau duration RTQX1": {0: 359.98},
            "Plateau duration RTQX2": {0: 359.98},
            "I_Q_RQX": {0: 6271.2},
            "I_Q_RTQX1": {0: -48.7},
            "I_Q_RTQX2": {0: 4332.6},
            "MIITS_RQX": {0: 9.293},
            "MIITS_RTQX1": {0: 3e-05},
            "MIITS_RTQX2": {0: 2.883},
            "I_Earth_max": {0: 35.888672},
            "Delta_t(QPS-PIC)": {0: np.nan},
            "I_Q_Q1": {0: 6221.9},
            "I_Q_Q2": {0: 10603.7},
            "I_Q_Q3": {0: 6271.2},
            "MIITS_Q1": {0: 8.516},
            "MIITS_Q2": {0: 18.646},
            "MIITS_Q3": {0: 9.293},
            "Type of Quench": {0: "Training"},
            "Quench origin": {0: "Q2"},
            "Quench count": {0: np.nan},
            "QDS trigger origin": {0: "QPS"},
            "dU_QPS/dt": {0: np.nan},
            "QH analysis": {0: "Pass"},
            "Comment": {0: "-"},
            "Analysis performed by": {0: "mmacieje"},
            "lhcsmapi version": {0: "1.4.35"},
            "timestamp_fgc_rqx": {0: 1535558373560000000},
            "timestamp_fgc_rtqx1": {0: 1535558373560000000},
            "timestamp_fgc_rtqx2": {0: 1535558373560000000},
            "timestamp_pic": {0: 1535558373579000000},
            "timestamp_qds_a": {0: 1535558373578000000},
            "timestamp_qds_b": {0: 0},
            "dU_RES_dt_Q1": {0: -2.2735595963888886},
            "dU_RES_dt_Q2": {0: 3.1471253028125},
            "dU_RES_dt_Q3": {0: -2.373104992619048},
            "RQX.R1_LEADS_U_HTS": {0: "U_HTS < quench threshold"},
            "RQX.R1_LEADS_U_RES": {0: "U_RES < quench threshold"},
        }
    )

    _circuit_type = "IT"
    _circuit_name = "RQX.R1"
    _timestamp_fgc = 1535558373560000000
    _timestamp_qds = 1535558373578000000
    _is_automatic = True

    def test_create_timestamp_table_qds_missynchronized(self):
        # arrange
        timestamp_dct = {
            "FGC_RQX": 1435558373560000000,
            "FGC_RTQX1": 1535558373560000000,
            "FGC_RTQX2": 1535558373560000000,
            "PIC": 1535558373579000000,
            "QDS_A": 1535558373578000000,
            "QDS_B": 1535558373570000000,
        }

        # act
        with warnings.catch_warnings(record=True) as w:
            # Cause all warnings to always be triggered.
            warnings.simplefilter("always")
            # Trigger a warning.
            it_analysis = ItCircuitAnalysis(self._circuit_type, self._results_table, is_automatic=self._is_automatic)
            it_analysis.create_timestamp_table(timestamp_dct)

            self.assertEqual("FGC is the first timestamp, potentially a PC trip not a quench!", str(w[0].message))
            self.assertEqual("QDS PM events (board A and B) are not synchronized to 1 ms!", str(w[1].message))
            self.assertEqual("QDS_A and FGC PM events are not synchronized to +/- 40 ms!", str(w[2].message))

    def test_create_timestamp_table_fgc_first(self):
        # arrange
        timestamp_dct = {
            "FGC_RQX": 1435558373560000000,
            "FGC_RTQX1": 1535558373560000000,
            "FGC_RTQX2": 1535558373560000000,
            "PIC": 1535558373579000000,
            "QDS_A": 1535558373578000000,
            "QDS_B": np.nan,
        }

        # act
        with warnings.catch_warnings(record=True) as w:
            # Cause all warnings to always be triggered.
            warnings.simplefilter("always")
            # Trigger a warning.
            it_analysis = ItCircuitAnalysis(self._circuit_type, self._results_table, is_automatic=self._is_automatic)
            it_analysis.create_timestamp_table(timestamp_dct)

            self.assertEqual("FGC is the first timestamp, potentially a PC trip not a quench!", str(w[0].message))
            self.assertEqual("QDS Board B not present in PM buffer!", str(w[1].message))
            self.assertEqual("QDS_A and FGC PM events are not synchronized to +/- 40 ms!", str(w[2].message))

    def test_create_timestamp_table_fgc_missing(self):
        # arrange
        timestamp_dct = {"PIC": 1535558373579000000, "QDS_A": 1535558373578000000, "QDS_B": np.nan}

        # act
        with warnings.catch_warnings(record=True) as w:
            # Cause all warnings to always be triggered.
            warnings.simplefilter("always")
            # Trigger a warning.
            it_analysis = ItCircuitAnalysis(self._circuit_type, self._results_table, is_automatic=self._is_automatic)
            it_analysis.create_timestamp_table(timestamp_dct)

            self.assertEqual("QDS Board B not present in PM buffer!", str(w[0].message))
            self.assertEqual(
                "Either QDS_A or FGC PM event is missing. " "Can not check synchronization of QDS and FGC timestamps!",
                str(w[1].message),
            )

    def test_create_timestamp_table_both_qps_missing(self):
        # arrange
        timestamp_dct = {"PIC": 1535558373579000000, "FGC": 1535558373578000000, "QDS_A": np.nan, "QDS_B": np.nan}

        # act
        with warnings.catch_warnings(record=True) as w:
            # Cause all warnings to always be triggered.
            warnings.simplefilter("always")
            # Trigger a warning.
            it_analysis = ItCircuitAnalysis(self._circuit_type, self._results_table, is_automatic=self._is_automatic)
            it_analysis.create_timestamp_table(timestamp_dct)

            self.assertEqual("FGC is the first timestamp, potentially a PC trip not a quench!", str(w[0].message))
            self.assertEqual(
                "Neither QDS board A nor B are present in the PM buffer. " "No QH fired, it is not an FPA!",
                str(w[1].message),
            )
            self.assertEqual(
                "Either QDS_A or FGC PM event is missing. " "Can not check synchronization of QDS and FGC timestamps!",
                str(w[2].message),
            )

    def test_create_timestamp_table_qps_a_missing(self):
        # arrange
        timestamp_dct = {"PIC": 1535558373579000000, "FGC": np.nan, "QDS_A": np.nan, "QDS_B": 1535558373578000000}

        # act
        with warnings.catch_warnings(record=True) as w:
            # Cause all warnings to always be triggered.
            warnings.simplefilter("always")
            # Trigger a warning.
            it_analysis = ItCircuitAnalysis(self._circuit_type, self._results_table, is_automatic=self._is_automatic)
            it_analysis.create_timestamp_table(timestamp_dct)

            self.assertEqual("QDS Board A not present in PM buffer!", str(w[0].message))
            self.assertEqual(
                "Either QDS_A or FGC PM event is missing. " "Can not check synchronization of QDS and FGC timestamps!",
                str(w[1].message),
            )

    def test_create_timestamp_table(self):
        # arrange
        timestamp_dct = {
            "FGC_RQX": 1535558373560000000,
            "FGC_RTQX1": 1535558373560000000,
            "FGC_RTQX2": 1535558373560000000,
            "PIC": 1535558373579000000,
            "QDS_A": 1535558373578000000,
            "QDS_B": np.nan,
        }

        # act
        with warnings.catch_warnings(record=True) as w:
            # Cause all warnings to always be triggered.
            warnings.simplefilter("always")
            # Trigger a warning.
            it_analysis = ItCircuitAnalysis(self._circuit_type, self._results_table, is_automatic=self._is_automatic)
            it_analysis.create_timestamp_table(timestamp_dct)

            self.assertEqual("FGC is the first timestamp, potentially a PC trip not a quench!", str(w[0].message))
            self.assertEqual("QDS Board B not present in PM buffer!", str(w[1].message))

        # assert
        self.assertEqual(1535558373560000000, it_analysis.results_table.loc[0, "timestamp_fgc_rqx"])
        self.assertEqual(1535558373560000000, it_analysis.results_table.loc[0, "timestamp_fgc_rtqx1"])
        self.assertEqual(1535558373560000000, it_analysis.results_table.loc[0, "timestamp_fgc_rtqx2"])
        self.assertEqual(1535558373579000000, it_analysis.results_table.loc[0, "timestamp_pic"])
        self.assertEqual(1535558373578000000, it_analysis.results_table.loc[0, "timestamp_qds_a"])

    def test_plot_i_meas_pc(self):
        # arrange
        pass
        # act

        # assert

    def test_find_time_of_quench(self):
        # arrange
        i_ref_rqx_df = read_csv("resources/hwc/it/fpa", "I_REF_RQX")

        # act
        it_analysis = ItCircuitAnalysis(self._circuit_type, self._results_table, is_automatic=self._is_automatic)
        t_quench_act = it_analysis.estimate_quench_start_from_i_ref(i_ref_rqx_df)

        # assert
        t_quench_ref = -0.04

        self.assertEqual(t_quench_ref, t_quench_act)

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_plot_magnet_current(self, mock_show=None):
        # arrange
        i_meas_q1_df = read_csv("resources/hwc/it/fpa", "I_MEAS_Q1")
        i_meas_q2_df = read_csv("resources/hwc/it/fpa", "I_MEAS_Q2")
        i_meas_q3_df = read_csv("resources/hwc/it/fpa", "I_MEAS_Q3")

        # act
        it_analysis = ItCircuitAnalysis(self._circuit_type, self._results_table, is_automatic=self._is_automatic)
        it_analysis.plot_magnet_current(
            self._circuit_name, self._timestamp_fgc, i_meas_q1_df, i_meas_q2_df, i_meas_q3_df
        )

        if mock_show is not None:
            mock_show.assert_called()

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_plot_u_res_u_res_slope_u_1_u_2(self, mock_show=None):
        # arrange
        u_res_q1_df = read_csv("resources/hwc/it/fpa", "U_RES_Q1")
        u_1_q1_df = read_csv("resources/hwc/it/fpa", "U_1_Q1")
        u_2_q1_df = read_csv("resources/hwc/it/fpa", "U_2_Q1")

        # act
        it_analysis = ItCircuitAnalysis(self._circuit_type, self._results_table, is_automatic=self._is_automatic)
        u_res_q1_slope_df = it_analysis.calculate_u_res_slope(u_res_q1_df, col_name="dU_RES_dt_Q1")
        it_analysis.plot_u_res_u_res_slope_u_1_u_2(
            self._circuit_name, self._timestamp_qds, u_res_q1_df, u_res_q1_slope_df, u_1_q1_df, u_2_q1_df, suffix="_Q1"
        )

        # assert
        self.assertAlmostEqual(-2.2735595963888886, it_analysis.results_table.loc[0, "dU_RES_dt_Q1"], places=6)
        if mock_show is not None:
            mock_show.assert_called()

    def test_find_quench_origin(self):
        # arrange
        u_res_q1_df = read_csv("resources/hwc/it/fpa", "U_RES_Q1")
        u_res_q2_df = read_csv("resources/hwc/it/fpa", "U_RES_Q2")
        u_res_q3_df = read_csv("resources/hwc/it/fpa", "U_RES_Q3")

        # act
        it_analysis = ItCircuitAnalysis(self._circuit_type, self._results_table, is_automatic=self._is_automatic)
        it_analysis.find_quench_origin([u_res_q1_df, u_res_q2_df, u_res_q3_df])

        # assert
        self.assertEqual("Q2", it_analysis.results_table.loc[0, "Quench origin"])

    def test_find_quench_origin_u_res_q3_below_100_mv(self):
        # arrange
        u_res_q1_df = read_csv("resources/hwc/it/fpa", "U_RES_Q1")
        u_res_q2_df = read_csv("resources/hwc/it/fpa", "U_RES_Q2")
        u_res_q3_df = read_csv("resources/hwc/it/fpa", "U_RES_Q3")

        # act
        it_analysis = ItCircuitAnalysis(self._circuit_type, self._results_table, is_automatic=self._is_automatic)
        it_analysis.find_quench_origin([u_res_q1_df, u_res_q2_df, u_res_q3_df[abs(u_res_q3_df) < 0.1].dropna()])

        # assert
        self.assertEqual("Q2", it_analysis.results_table.loc[0, "Quench origin"])

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_plot_u_res_q1_q2_q3(self, mock_show=None):
        # arrange
        u_res_q1_df = read_csv("resources/hwc/it/fpa", "U_RES_Q1")
        u_res_q2_df = read_csv("resources/hwc/it/fpa", "U_RES_Q2")
        u_res_q3_df = read_csv("resources/hwc/it/fpa", "U_RES_Q3")

        # act
        it_analysis = ItCircuitAnalysis(self._circuit_type, self._results_table, is_automatic=self._is_automatic)
        it_analysis.plot_u_res_q1_q2_q3(self._circuit_name, self._timestamp_qds, u_res_q1_df, u_res_q2_df, u_res_q3_df)

        if mock_show is not None:
            mock_show.assert_called()

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_analyze_qh(self, mock_show=None):
        # arrange
        u_hds_dfs = [
            read_csv("resources/hwc/it/fpa", "U_HDS_1_Q1"),
            read_csv("resources/hwc/it/fpa", "U_HDS_2_Q1"),
            read_csv("resources/hwc/it/fpa", "U_HDS_1_Q2"),
            read_csv("resources/hwc/it/fpa", "U_HDS_2_Q2"),
            read_csv("resources/hwc/it/fpa", "U_HDS_3_Q2"),
            read_csv("resources/hwc/it/fpa", "U_HDS_4_Q2"),
            read_csv("resources/hwc/it/fpa", "U_HDS_1_Q3"),
            read_csv("resources/hwc/it/fpa", "U_HDS_2_Q3"),
        ]

        u_hds_ref_dfs = [
            read_csv("resources/hwc/it/fpa", "U_HDS_REF_1_Q1"),
            read_csv("resources/hwc/it/fpa", "U_HDS_REF_2_Q1"),
            read_csv("resources/hwc/it/fpa", "U_HDS_REF_1_Q2"),
            read_csv("resources/hwc/it/fpa", "U_HDS_REF_2_Q2"),
            read_csv("resources/hwc/it/fpa", "U_HDS_REF_3_Q2"),
            read_csv("resources/hwc/it/fpa", "U_HDS_REF_4_Q2"),
            read_csv("resources/hwc/it/fpa", "U_HDS_REF_1_Q3"),
            read_csv("resources/hwc/it/fpa", "U_HDS_REF_2_Q3"),
        ]

        # act
        it_analysis = ItCircuitAnalysis(self._circuit_type, self._results_table, is_automatic=self._is_automatic)
        it_analysis.analyze_single_qh_voltage_with_ref(
            self._circuit_name, self._timestamp_qds, u_hds_dfs, u_hds_ref_dfs
        )

        # assert
        self.assertEqual("Pass", it_analysis.results_table.loc[0, "QH analysis"])
        if mock_show is not None:
            mock_show.assert_called()

    def test_create_mp3_results_table(self):
        # arrange

        # act
        it_analysis = ItCircuitAnalysis(self._circuit_type, self._results_table, is_automatic=self._is_automatic)
        mp3_results_table_act = it_analysis.create_mp3_results_table()

        # assert
        mp3_results_table_ref = pd.DataFrame(
            {
                "Circuit Name": {0: "RQX.R1"},
                "Circuit Family": {0: "RQX"},
                "Period": {0: "Operation 2018"},
                "Date (FGC_RQX)": {0: "2018-08-29"},
                "Time (FGC_RQX)": {0: "17:59:33.560"},
                "FPA Reason": {0: "Magnet quench"},
                "Timestamp_PIC": {0: "2018-08-29 17:59:33.579"},
                "Delta_t(FGC_RQX-PIC)": {0: -19.0},
                "Ramp rate RQX": {0: 0},
                "Ramp rate RTQX1": {0: 0},
                "Ramp rate RTQX2": {0: 0},
                "Plateau duration RQX": {0: 359.98},
                "Plateau duration RTQX1": {0: 359.98},
                "Plateau duration RTQX2": {0: 359.98},
                "I_Q_RQX": {0: 6271.2},
                "I_Q_RTQX1": {0: -48.7},
                "I_Q_RTQX2": {0: 4332.6},
                "MIITS_RQX": {0: 9.293},
                "MIITS_RTQX1": {0: 3e-05},
                "MIITS_RTQX2": {0: 2.883},
                "I_Earth_max": {0: 35.888672},
                "Delta_t(QPS-PIC)": {0: -1.0},
                "I_Q_Q1": {0: 6221.9},
                "I_Q_Q2": {0: 10603.7},
                "I_Q_Q3": {0: 6271.2},
                "Type of Quench": {0: "Training"},
                "Quench origin": {0: "Q2"},
                "Quench count": {0: np.nan},
                "QDS trigger origin": {0: "QPS"},
                "dU_QPS/dt": {0: 3.1471253028125},
                "QH analysis": {0: "Pass"},
                "Comment": {0: "-"},
                "Analysis performed by": {0: "mmacieje"},
                "lhcsmapi version": {0: "1.4.35"},
            }
        )
        mp3_results_table_act.fillna(np.nan, inplace=True)
        mp3_results_table_ref.fillna(np.nan, inplace=True)
        pd.testing.assert_frame_equal(mp3_results_table_ref, mp3_results_table_act)

    def test_calculate_resistance_two_plateaus(self):
        # arrange
        u_mag_feature_df = pd.DataFrame(
            {
                "nxcals_variable_name": {
                    0: "RQX.L1:U_RES_Q3",
                    1: "RQX.L1:U_RES_Q2",
                    2: "RQX.L1:U_RES_Q1",
                    3: "RQX.L1:U_RES_Q1",
                    4: "RQX.L1:U_RES_Q3",
                    5: "RQX.L1:U_RES_Q2",
                    6: "RQX.L1:U_RES_Q2",
                    7: "RQX.L1:U_RES_Q3",
                    8: "RQX.L1:U_RES_Q1",
                },
                "class": {0: 0, 1: 0, 2: 0, 3: 1, 4: 1, 5: 1, 6: 2, 7: 2, 8: 2},
                "std": {
                    0: 0.0006590244283690554,
                    1: 0.0006003269209667091,
                    2: 0.0004803017287718265,
                    3: 0.0001780767894438353,
                    4: 0.0002847876326502599,
                    5: 0.00021343090121760577,
                    6: 0.00024841814499234444,
                    7: 0.00032373406166497315,
                    8: 0.00020742451816201134,
                },
                "mean": {
                    0: 0.001290283203125,
                    1: 0.0006379756221064815,
                    2: 0.001714183666087963,
                    3: 0.00042216071915938865,
                    4: 0.0001664420715065502,
                    5: -0.00016565314546943232,
                    6: -0.0001680449338041366,
                    7: 0.00016243281727165463,
                    8: 0.0004107689738339811,
                },
            }
        )

        i_meas_feature_df = pd.DataFrame(
            {
                "device": {
                    0: "RPHGC.UL14.RTQX2.L1",
                    1: "RPMBC.UL14.RTQX1.L1",
                    2: "RPHFC.UL14.RQX.L1",
                    3: "RPHGC.UL14.RTQX2.L1",
                    4: "RPHFC.UL14.RQX.L1",
                    5: "RPMBC.UL14.RTQX1.L1",
                    6: "RPMBC.UL14.RTQX1.L1",
                    7: "RPHFC.UL14.RQX.L1",
                    8: "RPHGC.UL14.RTQX2.L1",
                },
                "class": {0: 0, 1: 0, 2: 0, 3: 1, 4: 1, 5: 1, 6: 2, 7: 2, 8: 2},
                "std": {
                    0: 623.2160839069359,
                    1: 85.3789528047191,
                    2: 960.5071622546233,
                    3: 0.17811782108524057,
                    4: 0.17748943630537578,
                    5: 0.0,
                    6: 0.0,
                    7: 0.08718236908304788,
                    8: 0.08682924395786505,
                },
                "mean": {
                    0: 1227.847877664502,
                    1: 141.33127819548875,
                    2: 1854.4034016775408,
                    3: 150.03642857142856,
                    4: 200.03725,
                    5: 0.0,
                    6: 275.0,
                    7: 3499.9897540983607,
                    8: 2299.9898373983738,
                },
            }
        )

        # act
        r_res_df_act = ItCircuitAnalysis.calculate_resistance(i_meas_feature_df, u_mag_feature_df)

        # assert
        r_res_df_ref = pd.DataFrame(
            {
                "R_RES": {
                    "R_Q1": 3.1865445239756144e-09,
                    "R_Q2": 4.388678213750007e-10,
                    "R_Q3": 1.2149430120331455e-09,
                },
                "error": {"R_Q1": 7.647076659682919e-08, "R_Q2": 6.009504242125057e-08, "R_Q3": 1.306595158609503e-07},
            }
        )

        pd.testing.assert_frame_equal(r_res_df_ref, r_res_df_act)

    def test_calculate_resistance_three_plateaus(self):
        # arrange
        u_mag_feature_df = pd.DataFrame(
            {
                "nxcals_variable_name": {
                    0: "RQX.L1:U_RES_Q1",
                    1: "RQX.L1:U_RES_Q2",
                    2: "RQX.L1:U_RES_Q3",
                    3: "RQX.L1:U_RES_Q1",
                    4: "RQX.L1:U_RES_Q2",
                    5: "RQX.L1:U_RES_Q3",
                    6: "RQX.L1:U_RES_Q1",
                    7: "RQX.L1:U_RES_Q2",
                    8: "RQX.L1:U_RES_Q3",
                    9: "RQX.L1:U_RES_Q1",
                    10: "RQX.L1:U_RES_Q2",
                    11: "RQX.L1:U_RES_Q3",
                },
                "class": {0: 0, 1: 0, 2: 0, 3: 1, 4: 1, 5: 1, 6: 2, 7: 2, 8: 2, 9: 3, 10: 3, 11: 3},
                "std": {
                    0: 0.0004758769332995855,
                    1: 0.0004475119236661067,
                    2: 0.0007012181353060007,
                    3: 0.0001561319328316318,
                    4: 0.00017475681312702327,
                    5: 0.0003166274841875245,
                    6: 0.0002073698161029608,
                    7: 0.00022109258506310448,
                    8: 0.0003335089858722337,
                    9: 0.00010708753655745407,
                    10: 8.486743621549837e-05,
                    11: 9.055583745982993e-05,
                },
                "mean": {
                    0: 0.00015570215104630455,
                    1: 0.0004892921617597952,
                    2: 0.0003046648903606411,
                    3: -0.0003630760413955405,
                    4: 0.00019184293255465902,
                    5: -0.00012168904135216034,
                    6: -0.00036247300142169727,
                    7: 0.0001842268495734908,
                    8: -0.0001333054803696413,
                    9: -0.00035324323071216616,
                    10: 0.00017147799981454006,
                    11: -0.00013641447978486647,
                },
            }
        )

        i_meas_feature_df = pd.DataFrame(
            {
                "device": {
                    0: "RPHGC.UL14.RTQX2.L1",
                    1: "RPHFC.UL14.RQX.L1",
                    2: "RPMBC.UL14.RTQX1.L1",
                    3: "RPHGC.UL14.RTQX2.L1",
                    4: "RPHFC.UL14.RQX.L1",
                    5: "RPMBC.UL14.RTQX1.L1",
                    6: "RPMBC.UL14.RTQX1.L1",
                    7: "RPHFC.UL14.RQX.L1",
                    8: "RPHGC.UL14.RTQX2.L1",
                    9: "RPMBC.UL14.RTQX1.L1",
                    10: "RPHGC.UL14.RTQX2.L1",
                    11: "RPHFC.UL14.RQX.L1",
                },
                "class": {0: 0, 1: 0, 2: 0, 3: 1, 4: 1, 5: 1, 6: 2, 7: 2, 8: 2, 9: 3, 10: 3, 11: 3},
                "std": {
                    0: 1344.7743446836487,
                    1: 2142.264801712606,
                    2: 0.0011624763874381928,
                    3: 0.1781178210852372,
                    4: 0.1781178210852388,
                    5: 0.0,
                    6: 0.0,
                    7: 0.17271487988266526,
                    8: 0.1768992180122373,
                    9: 0.0,
                    10: 0.0,
                    11: 4.143658687328527,
                },
                "mean": {
                    0: 2268.286149231343,
                    1: 2703.1691916911227,
                    2: -0.00013513513513513514,
                    3: 150.03642857142856,
                    4: 200.03642857142853,
                    5: 0.0,
                    6: 0.0,
                    7: 6799.964761904763,
                    8: 4599.963,
                    9: 0.0,
                    10: 0.01,
                    11: 4.58,
                },
            }
        )

        # act
        r_res_df_act = ItCircuitAnalysis.calculate_resistance(i_meas_feature_df, u_mag_feature_df)

        # assert
        r_res_df_ref = pd.DataFrame(
            {
                "R_RES": {"R_Q1": 9.137068516298338e-11, "R_Q2": 6.892473292012265e-10, "R_Q3": 1.7600856298410749e-09},
                "error": {"R_Q1": 3.933004270057951e-08, "R_Q2": 2.5504304933691193e-08, "R_Q3": 6.96781595580523e-08},
            }
        )

        pd.testing.assert_frame_equal(r_res_df_ref, r_res_df_act)

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_assert_tt891a_min_max_value(self, mock_show=None):
        # arrange
        i_meas_dfs = [
            read_csv("resources/hwc/it/fpa", "I_MEAS_Q1"),
            read_csv("resources/hwc/it/fpa", "I_MEAS_Q2"),
            read_csv("resources/hwc/it/fpa", "I_MEAS_Q3"),
        ]
        tt891a_nxcals_dfs = [
            read_csv("resources/hwc/it/fpa", "DXCX01_03L2_TT891A.TEMPERATURECALC"),
            read_csv("resources/hwc/it/fpa", "DXCX02_03L2_TT891A.TEMPERATURECALC"),
            read_csv("resources/hwc/it/fpa", "DXCX03_03L2_TT891A.TEMPERATURECALC"),
            read_csv("resources/hwc/it/fpa", "DXCX04_03L2_TT891A.TEMPERATURECALC"),
        ]
        it_analysis = ItCircuitAnalysis(self._circuit_type, results_table=None, is_automatic=self._is_automatic)

        # act
        it_analysis.assert_tt891a_min_max_value(tt891a_nxcals_dfs, i_meas_dfs, value_range=(46, 54))

        if mock_show is not None:
            mock_show.assert_called()

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_assert_tt893_min_max_value(self, mock_show=None):
        # arrange
        i_meas_dfs = [
            read_csv("resources/hwc/it/fpa", "I_MEAS_Q1"),
            read_csv("resources/hwc/it/fpa", "I_MEAS_Q2"),
            read_csv("resources/hwc/it/fpa", "I_MEAS_Q3"),
        ]

        tt893_nxcals_dfs = [
            read_csv("resources/hwc/it/fpa", "DXCX01_03L2_TT893.TEMPERATURECALC"),
            read_csv("resources/hwc/it/fpa", "DXCX02_03L2_TT893.TEMPERATURECALC"),
            read_csv("resources/hwc/it/fpa", "DXCX03_03L2_TT893.TEMPERATURECALC"),
            read_csv("resources/hwc/it/fpa", "DXCX04_03L2_TT893.TEMPERATURECALC"),
        ]
        it_analysis = ItCircuitAnalysis(self._circuit_type, results_table=None, is_automatic=self._is_automatic)

        # act
        with warnings.catch_warnings(record=True) as w:
            # Cause all warnings to always be triggered.
            warnings.simplefilter("always")
            # Range taken to raise a warning
            it_analysis.assert_tt893_min_max_value(tt893_nxcals_dfs, i_meas_dfs, value_range=(285, 305))

            self.assertEqual("DXCX03_03L2_TT893.TEMPERATURECALC outside of the [285, 305] threshold", str(w[0].message))
            self.assertEqual("DXCX04_03L2_TT893.TEMPERATURECALC outside of the [285, 305] threshold", str(w[1].message))

        if mock_show is not None:
            mock_show.assert_called()

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_assert_u_res_min_max_slope(self, mock_show=None):
        # arrange
        u_res_nxcals_dfs = [
            read_csv("resources/hwc/it/fpa", "DFLX.3L2.RQX.L2.LD1_U_RES"),
            read_csv("resources/hwc/it/fpa", "DFLX.3L2.RQX.L2.LD2_U_RES"),
            read_csv("resources/hwc/it/fpa", "DFLX.3L2.RQX.L2.LD3_U_RES"),
            read_csv("resources/hwc/it/fpa", "DFLX.3L2.RQX.L2.LD4_U_RES"),
        ]
        plateau_start = [1492651370640000000, 1492653447140000000, 1492658100140000000]
        plateau_end = [1492652515140000000, 1492656989140000000, 1492658137140000000]
        t_start = "2017-04-20 03:20:36.676"
        it_analysis = ItCircuitAnalysis(self._circuit_type, results_table=None, is_automatic=self._is_automatic)

        # act
        with warnings.catch_warnings(record=True) as w:
            # Cause all warnings to always be triggered.
            warnings.simplefilter("always")
            it_analysis.assert_u_res_min_max_slope(
                u_res_nxcals_dfs, plateau_start, plateau_end, Time.to_unix_timestamp(t_start), slope_range=(-2, 2)
            )

            self.assertEqual(
                "The drift of DFLX.3L2.RQX.L2.LD1:U_RES is 17.663 mV/h for constant current "
                "from 6863.464 to 6900.464 s",
                str(w[0].message),
            )
            self.assertEqual(
                "The drift of DFLX.3L2.RQX.L2.LD4:U_RES is -18.179 mV/h for constant current "
                "from 6863.464 to 6900.464 s",
                str(w[1].message),
            )

        if mock_show is not None:
            mock_show.assert_called()

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_assert_u_res_min_max_value(self, mock_show=None):
        # arrange
        u_res_nxcals_dfs = [
            read_csv("resources/hwc/it/fpa", "DFLX.3L2.RQX.L2.LD1_U_RES"),
            read_csv("resources/hwc/it/fpa", "DFLX.3L2.RQX.L2.LD2_U_RES"),
            read_csv("resources/hwc/it/fpa", "DFLX.3L2.RQX.L2.LD3_U_RES"),
            read_csv("resources/hwc/it/fpa", "DFLX.3L2.RQX.L2.LD4_U_RES"),
        ]

        it_analysis = ItCircuitAnalysis(self._circuit_type, results_table=None, is_automatic=self._is_automatic)

        # act
        with warnings.catch_warnings(record=True) as w:
            # Cause all warnings to always be triggered.
            warnings.simplefilter("always")
            it_analysis.assert_u_res_min_max_value(u_res_nxcals_dfs, value_range=(-40e-3, 40e-3))

            self.assertEqual("DFLX.3L2.RQX.L2.LD1:U_RES outside of the [-0.04, 0.04] threshold", str(w[0].message))
            self.assertEqual("DFLX.3L2.RQX.L2.LD4:U_RES outside of the [-0.04, 0.04] threshold", str(w[1].message))

        if mock_show is not None:
            mock_show.assert_called()

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_assert_u_hts_min_max_value(self, mock_show=None):
        # arrange
        u_hts_nxcals_dfs = [
            read_csv("resources/hwc/it/fpa", "DFLX.3L2.RQX.L2.LD1_U_HTS"),
            read_csv("resources/hwc/it/fpa", "DFLX.3L2.RQX.L2.LD2_U_HTS"),
            read_csv("resources/hwc/it/fpa", "DFLX.3L2.RQX.L2.LD3_U_HTS"),
            read_csv("resources/hwc/it/fpa", "DFLX.3L2.RQX.L2.LD4_U_HTS"),
        ]

        it_analysis = ItCircuitAnalysis(self._circuit_type, results_table=None, is_automatic=self._is_automatic)

        # act
        it_analysis.assert_u_hts_min_max_value(u_hts_nxcals_dfs, value_range=(-0.5e-3, 0.5e-3))

        if mock_show is not None:
            mock_show.assert_called()

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_assert_cv891_min_max_variation(self, mock_show=None):
        # arrange
        cv891_nxcals_dfs = [
            read_csv("resources/hwc/it/fpa", "DXCX01_03L2_CV891.POSST"),
            read_csv("resources/hwc/it/fpa", "DXCX02_03L2_CV891.POSST"),
            read_csv("resources/hwc/it/fpa", "DXCX03_03L2_CV891.POSST"),
            read_csv("resources/hwc/it/fpa", "DXCX04_03L2_CV891.POSST"),
        ]

        it_analysis = ItCircuitAnalysis(self._circuit_type, results_table=None, is_automatic=self._is_automatic)

        # act
        plateau_start = [1492651370640000000, 1492653447140000000, 1492658100140000000]
        plateau_end = [1492652515140000000, 1492656989140000000, 1492658137140000000]
        t_start = "2017-04-20 03:20:36.676"
        it_analysis.assert_cv891_min_max_variation(
            cv891_nxcals_dfs, 8, plateau_start, plateau_end, Time.to_unix_timestamp(t_start)
        )

        if mock_show is not None:
            mock_show.assert_called()

    def test_assert_dataframe_to_string_resistance(self):
        # arrange
        resistance_df = read_csv("resources/hwc/it/pno_a9_5", "r_res")

        it_analysis = ItCircuitAnalysis(self._circuit_type)

        expected_out = "R_Q1 = 2.28 +/- 45.83 nOhm\n" + "R_Q2 = 0.86 +/- 27.68 nOhm\n" + "R_Q3 = 4.00 +/- 75.30 nOhm"

        with patch("sys.stdout", new=StringIO()) as fake_out:
            it_analysis.dataframe_to_string_resistance(resistance_df)
            self.assertEqual(fake_out.getvalue().strip(), expected_out)
