import unittest
from unittest.mock import patch

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

from lhcsmapi.Time import Time
from lhcsmapi.analysis.ItCircuitQuery import ItCircuitQuery
from lhcsmapi.pyedsl.PlotBuilder import PlotBuilder
from test.resources.read_csv import read_csv


class TestItCircuitQuery(unittest.TestCase):
    def test_create_report_analysis_template(self):
        # arrange
        query = ItCircuitQuery("IT", "RQX.R1")

        # act
        results_act = query.create_report_analysis_template(1535558373560000000, "")

        results_act["lhcsmapi version"] = "1.3.226"

        # assert
        results_ref = pd.DataFrame(
            {
                "Circuit Name": {0: "RQX.R1"},
                "Circuit Family": {0: "RQX"},
                "Period": {0: "Operation 2018"},
                "Date (FGC_RQX)": {0: "2018-08-29"},
                "Time (FGC_RQX)": {0: "17:59:33.560"},
                "FPA Reason": {0: np.nan},
                "Timestamp_PIC": {0: np.nan},
                "Delta_t(FGC_RQX-PIC)": {0: np.nan},
                "Ramp rate RQX": {0: np.nan},
                "Ramp rate RTQX1": {0: np.nan},
                "Ramp rate RTQX2": {0: np.nan},
                "Plateau duration RQX": {0: np.nan},
                "Plateau duration RTQX1": {0: np.nan},
                "Plateau duration RTQX2": {0: np.nan},
                "I_Q_RQX": {0: np.nan},
                "I_Q_RTQX1": {0: np.nan},
                "I_Q_RTQX2": {0: np.nan},
                "MIITS_RQX": {0: np.nan},
                "MIITS_RTQX1": {0: np.nan},
                "MIITS_RTQX2": {0: np.nan},
                "I_Earth_max": {0: np.nan},
                "Delta_t(QPS-PIC)": {0: np.nan},
                "I_Q_Q1": {0: np.nan},
                "I_Q_Q2": {0: np.nan},
                "I_Q_Q3": {0: np.nan},
                "Type of Quench": {0: np.nan},
                "Quench origin": {0: np.nan},
                "Quench count": {0: np.nan},
                "QDS trigger origin": {0: np.nan},
                "dU_QPS/dt": {0: np.nan},
                "QH analysis": {0: np.nan},
                "Comment": {0: np.nan},
                "Analysis performed by": {0: ""},
                "lhcsmapi version": {0: "1.3.226"},
            }
        )

        results_act.fillna(np.nan, inplace=True)
        pd.testing.assert_frame_equal(results_ref, results_act)

    def test_calculate_current_plateau_start_end_pno_a9(self):
        # arrange
        t_start = "2017-04-11 20:38:19.627000+02:00"
        t_end = "2017-04-11 22:24:50.702000+02:00"
        circuit_name = "RQX.R1"
        it_query = ItCircuitQuery("IT", circuit_name)
        hwc_test = "PNO.a9"
        spark = "spark"

        i_meas_raw_dfs = [
            read_csv("resources/hwc/it/pno_a9", "I_MEAS_RQX"),
            read_csv("resources/hwc/it/pno_a9", "I_MEAS_RTQX1"),
            read_csv("resources/hwc/it/pno_a9", "I_MEAS_RTQX2"),
        ]

        # act
        with patch("lhcsmapi.api.query.query_nxcals_by_variables", return_value=i_meas_raw_dfs):
            plateau_start_act, plateau_end_act = it_query.calculate_current_plateau_start_end(
                Time.to_unix_timestamp(t_start),
                Time.to_unix_timestamp(t_end),
                i_meas_threshold=0,
                min_duration_in_sec=240,
                time_shift_in_sec=(60, 40),
                spark=spark,
            )

        plateau_start_ref = [1491936050250924544, 1491937786166777344, 1491941992163222784]
        plateau_end_ref = [1491937142037867264, 1491941286019620864, 1491942198240000000]

        # assert
        self.assertListEqual(plateau_start_ref, plateau_start_act)
        self.assertListEqual(plateau_end_ref, plateau_end_act)

    def test_calculate_current_plateau_start_end_pno_a9_2(self):
        # arrange
        t_start = "2017-04-11 20:38:19.627000+02:00"
        t_end = "2017-04-11 22:24:50.702000+02:00"
        circuit_name = "RQX.R1"
        it_query = ItCircuitQuery("IT", circuit_name)
        hwc_test = "PNO.a9"
        spark = "spark"

        i_meas_raw_dfs = [
            read_csv("resources/hwc/it/pno_a9_2", "I_MEAS_RQX"),
            read_csv("resources/hwc/it/pno_a9_2", "I_MEAS_RTQX1"),
            read_csv("resources/hwc/it/pno_a9_2", "I_MEAS_RTQX2"),
        ]

        # act
        with patch("lhcsmapi.api.query.query_nxcals_by_variables", return_value=i_meas_raw_dfs):
            plateau_start_act, plateau_end_act = it_query.calculate_current_plateau_start_end(
                Time.to_unix_timestamp(t_start),
                Time.to_unix_timestamp(t_end),
                i_meas_threshold=0,
                min_duration_in_sec=100,
                time_shift_in_sec=(60, 40),
                spark=spark,
            )

        # assert
        plateau_start_ref = [1543848188149676288, 1543849490481762560, 1543853038369844736]
        plateau_end_ref = [1543848293183877120, 1543850589781963008, 1543854017640000000]

        self.assertListEqual(plateau_start_ref, plateau_start_act)
        self.assertListEqual(plateau_end_ref, plateau_end_act)

    def test_calculate_current_plateau_start_end_pno_a9_3(self):
        # arrange
        t_start = "2021-02-17 18:40:46.780000000"
        t_end = "2021-02-17 21:03:48.855000000"
        circuit_name = "RQX.L8"
        it_query = ItCircuitQuery("IT", circuit_name)
        hwc_test = "PNO.a9"
        spark = "spark"

        i_meas_raw_dfs = [
            read_csv("resources/hwc/it/pno_a9_3", "I_MEAS_RQX"),
            read_csv("resources/hwc/it/pno_a9_3", "I_MEAS_RTQX1"),
            read_csv("resources/hwc/it/pno_a9_3", "I_MEAS_RTQX2"),
        ]

        # act
        with patch("lhcsmapi.api.query.query_nxcals_by_variables", return_value=i_meas_raw_dfs):
            plateau_start_act, plateau_end_act = it_query.calculate_current_plateau_start_end(
                Time.to_unix_timestamp(t_start),
                Time.to_unix_timestamp(t_end),
                i_meas_threshold=5,
                min_duration_in_sec=100,
                time_shift_in_sec=(60, 40),
                spark=spark,
            )

        # assert
        plateau_start_ref = [1613583784093461504, 1613586098498007040]
        plateau_end_ref = [1613584885303426560, 1613587198707797248]

        self.assertListEqual(plateau_start_ref, plateau_start_act)
        self.assertListEqual(plateau_end_ref, plateau_end_act)

    def test_calculate_current_plateau_start_end2(self):
        # arrange
        t_start = "2017-04-11 20:38:19.627000+02:00"
        t_end = "2017-04-11 22:24:50.702000+02:00"
        circuit_name = "RQX.R1"
        it_query = ItCircuitQuery("IT", circuit_name)
        hwc_test = "PNO.a9"
        spark = "spark"

        i_meas_raw_dfs = [
            read_csv("resources/busbar", "I_MEAS_RQX"),
            read_csv("resources/busbar", "I_MEAS_RTQX1"),
            read_csv("resources/busbar", "I_MEAS_RTQX2"),
        ]

        # act
        with patch("lhcsmapi.api.query.query_nxcals_by_variables", return_value=i_meas_raw_dfs):
            plateau_start_act, plateau_end_act = it_query.calculate_current_plateau_start_end(
                Time.to_unix_timestamp(t_start),
                Time.to_unix_timestamp(t_end),
                i_meas_threshold=0,
                min_duration_in_sec=240,
                time_shift_in_sec=(60, 40),
                spark=spark,
            )

        # assert
        plateau_start_ref = [1491936050250924544, 1491937786166777344]
        plateau_end_ref = [1491937142037867264, 1491941282514094848]

        self.assertListEqual(plateau_start_ref, plateau_start_act)
        self.assertListEqual(plateau_end_ref, plateau_end_act)
