import re
import unittest
import warnings
from unittest.mock import patch

from lhcsmapi.analysis.ItCircuitAnalysis import ItCircuitAnalysis
import pytest
from test.resources.read_csv import read_csv


class TestItCircuitAnalysisPic2(unittest.TestCase):
    def test_find_start_end_quench_detection_u_res_q1(self):
        # arrange
        circuit_type = "IT"
        circuit_name = "RQX.L1"
        t_start = 1457201219228000000
        t_end = 1457202299296000000
        u_res_q1_df = read_csv("resources/hwc/it/pic2", "U_RES_Q1")
        it_analysis = ItCircuitAnalysis(circuit_type, None, is_automatic=False)

        # act
        t_start = it_analysis.find_start_end_quench_detection(u_res_q1_df)[0]

        # assert
        self.assertEqual(0.188, t_start)

    def test_find_start_end_quench_detection_u_res_q2(self):
        # arrange
        circuit_type = "IT"
        circuit_name = "RQX.L1"
        t_start = 1457201219228000000
        t_end = 1457202299296000000
        u_res_q2_df = read_csv("resources/hwc/it/pic2", "U_RES_Q2")
        it_analysis = ItCircuitAnalysis(circuit_type, None, is_automatic=False)

        # act
        with warnings.catch_warnings(record=True) as w:
            t_start = it_analysis.find_start_end_quench_detection(u_res_q2_df)[0]

        # assert
        self.assertEqual("U_RES signal circ.RQX.L1:U_RES_Q2 does not change - probably no quench!", str(w[0].message))
        self.assertEqual(1.996, t_start)

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_plot_u_res_u_res_slope_u_1_u_2(self, mock_show=None):
        # arrange
        circuit_type = "IT"
        circuit_name = "RQX.L1"
        t_start = 1457201219228000000
        t_end = 1457202299296000000
        timestamp_qds = 1457201338562000000
        u_1_q2_df = read_csv("resources/hwc/it/pic2", "U_1_Q2")
        u_2_q2_df = read_csv("resources/hwc/it/pic2", "U_2_Q2")
        u_res_q2_df = read_csv("resources/hwc/it/pic2", "U_RES_Q2")
        it_analysis = ItCircuitAnalysis(circuit_type, None, is_automatic=False)

        # act
        with pytest.warns(
            UserWarning, match=re.escape("U_RES signal circ.RQX.L1:U_RES_Q2 does not change - probably no quench")
        ):
            u_res_q2_slope_df = it_analysis.calculate_u_res_slope(u_res_q2_df, col_name="dU_RES_dt_Q2")
            it_analysis.plot_u_res_u_res_slope_u_1_u_2(
                circuit_name, timestamp_qds, u_res_q2_df, u_res_q2_slope_df, u_1_q2_df, u_2_q2_df, suffix="_Q2"
            )

        # assert
        if mock_show is not None:
            mock_show.assert_called()
