import warnings
import unittest
from unittest.mock import patch
from io import StringIO

import numpy as np
import pandas as pd

from lhcsmapi.analysis.IpdCircuitAnalysis import IpdCircuitAnalysis
from test.resources.read_csv import read_csv


class TestIpdCircuitAnalysis(unittest.TestCase):
    _circuit_type = "IPD2_B1B2"
    _is_automatic = True
    _circuit_name = "RD3.R4"
    _timestamp_pic = 1543842413329000000
    _timestamp_qds_a = 1543842413329000000
    _timestamp_qds_b = 1543842413330000000
    _timestamp_fgc = 1543842413340000000
    _author = "mmacieje"

    _results_table = pd.DataFrame(
        {
            "Circuit Name": {0: "RD3.R4"},
            "Circuit Family": {0: "RD3"},
            "Period": {0: "HWC 2018-2"},
            "Date (FGC)": {0: "2018-12-03"},
            "Time (FGC)": {0: "14:06:53.340"},
            "FPA Reason": {0: "Magnet quench"},
            "Timestamp_PIC": {0: np.nan},
            "Delta_t(FGC-PIC)": {0: np.nan},
            "Ramp rate": {0: 18.1},
            "Plateau duration": {0: 0},
            "I_Q_circ": {0: 5820.8},
            "MIITS_circ": {0: 4.995},
            "I_Earth_max": {0: -37.353516},
            "Delta_t(QPS-PIC)": {0: np.nan},
            "Type of Quench": {0: "Training"},
            "Quench count": {0: np.nan},
            "dU_QPS/dt": {0: 10.827636760000003},
            "QH analysis": {0: "Pass"},
            "Comment": {0: "-"},
            "Analysis performed by": {0: "mmacieje"},
            "lhcsmapi version": {0: "1.4.35"},
            "timestamp_fgc": {0: 1543842413340000000},
            "timestamp_pic": {0: 1543842413329000000},
            "timestamp_qds_a": {0: 1543842413329000000},
            "timestamp_qds_b": {0: 1543842413330000000},
            "RD3.R4_LEADS_U_HTS": {0: "U_HTS < quench threshold"},
            "RD3.R4_LEADS_U_RES": {0: "U_RES < quench threshold"},
            "QDS trigger origin": {0: "QPS"},
        }
    )

    analysis = IpdCircuitAnalysis(_circuit_type, _results_table, is_automatic=_is_automatic)

    def test_create_timestamp_table_qds_missynchronized(self):
        # arrange
        timestamp_dct = {
            "FGC": 1435558373560000000,
            "PIC": 1535558373579000000,
            "QDS_A": 1535558373578000000,
            "QDS_B": 1535558373570000000,
        }

        # act
        with warnings.catch_warnings(record=True) as w:
            # Cause all warnings to always be triggered.
            warnings.simplefilter("always")
            # Trigger a warning.
            self.analysis.create_timestamp_table(timestamp_dct)

            # assert
            self.assertEqual("FGC is the first timestamp, potentially a PC trip not a quench!", str(w[0].message))
            self.assertEqual("QDS PM events (board A and B) are not synchronized to 1 ms!", str(w[1].message))
            self.assertEqual("QDS_A and FGC PM events are not synchronized to +/- 40 ms!", str(w[2].message))

    def test_create_timestamp_table_fgc_missing(self):
        # arrange
        timestamp_dct = {"PIC": 1535558373579000000, "QDS_A": 1535558373578000000, "QDS_B": 1535558373570000000}

        # act
        with warnings.catch_warnings(record=True) as w:
            # Cause all warnings to always be triggered.
            warnings.simplefilter("always")
            # Trigger a warning.
            self.analysis.create_timestamp_table(timestamp_dct)

            self.assertEqual("QDS PM events (board A and B) are not synchronized to 1 ms!", str(w[0].message))
            self.assertEqual(
                "Either QDS_A or FGC PM event is missing. " "Can not check synchronization of QDS and FGC timestamps!",
                str(w[1].message),
            )

    def test_create_timestamp_table_qds_a_missing(self):
        # arrange
        timestamp_dct = {
            "FGC": 1435558373560000000,
            "PIC": 1535558373579000000,
            "QDS_A": np.nan,
            "QDS_B": 1535558373570000000,
        }

        # act
        with warnings.catch_warnings(record=True) as w:
            # Cause all warnings to always be triggered.
            warnings.simplefilter("always")
            # Trigger a warning.
            self.analysis.create_timestamp_table(timestamp_dct)

            self.assertEqual("FGC is the first timestamp, potentially a PC trip not a quench!", str(w[0].message))
            self.assertEqual("QDS Board A not present in PM buffer!", str(w[1].message))

    def test_create_timestamp_table_qds_b_missing(self):
        # arrange
        timestamp_dct = {
            "FGC": 1435558373560000000,
            "PIC": 1535558373579000000,
            "QDS_A": 1535558373578000000,
            "QDS_B": np.nan,
        }

        # act
        with warnings.catch_warnings(record=True) as w:
            # Cause all warnings to always be triggered.
            warnings.simplefilter("always")
            # Trigger a warning.
            self.analysis.create_timestamp_table(timestamp_dct)

            self.assertEqual("FGC is the first timestamp, potentially a PC trip not a quench!", str(w[0].message))
            self.assertEqual("QDS Board B not present in PM buffer!", str(w[1].message))

    def test_create_timestamp_table_both_qds_missing(self):
        # arrange
        timestamp_dct = {"FGC": 1435558373560000000, "PIC": 1535558373579000000, "QDS_A": np.nan, "QDS_B": np.nan}

        # act
        with warnings.catch_warnings(record=True) as w:
            # Cause all warnings to always be triggered.
            warnings.simplefilter("always")
            # Trigger a warning.
            self.analysis.create_timestamp_table(timestamp_dct)

            self.assertEqual("FGC is the first timestamp, potentially a PC trip not a quench!", str(w[0].message))
            self.assertEqual("Neither QDS board A nor B are present in the PM buffer.", str(w[1].message))

    def test_create_mp3_results_table(self):
        # arrange

        # act
        mp3_results_table_act = self.analysis.create_mp3_results_table()

        # assert
        mp3_results_table_ref = pd.DataFrame(
            {
                "Circuit Name": {0: "RD3.R4"},
                "Circuit Family": {0: "RD3"},
                "Period": {0: "HWC 2018-2"},
                "Date (FGC)": {0: "2018-12-03"},
                "Time (FGC)": {0: "14:06:53.340"},
                "FPA Reason": {0: "Magnet quench"},
                "Timestamp_PIC": {0: "2018-12-03 14:06:53.329"},
                "Delta_t(FGC-PIC)": {0: 11.0},
                "Ramp rate": {0: 18.1},
                "Plateau duration": {0: 0},
                "I_Q_circ": {0: 5820.8},
                "MIITS_circ": {0: 4.995},
                "I_Earth_max": {0: -37.353516},
                "Delta_t(QPS-PIC)": {0: 0.0},
                "Type of Quench": {0: "Training"},
                "Quench count": {0: np.nan},
                "QDS trigger origin": {0: "QPS"},
                "dU_QPS/dt": {0: 10.827636760000003},
                "QH analysis": {0: "Pass"},
                "Comment": {0: "-"},
                "Analysis performed by": {0: "mmacieje"},
                "lhcsmapi version": {0: "1.4.35"},
            }
        )

        mp3_results_table_act.fillna(np.nan, inplace=True)
        mp3_results_table_ref.fillna(np.nan, inplace=True)
        pd.testing.assert_frame_equal(mp3_results_table_ref, mp3_results_table_act)

    # Reproducing FPA notebook
    def test_create_timtestamp_table(self):
        # arrange
        timestamp_dct = {
            "FGC": self._timestamp_fgc,
            "PIC": self._timestamp_pic,
            "QDS_A": self._timestamp_qds_a,
            "QDS_B": self._timestamp_qds_b,
        }

        # act
        with warnings.catch_warnings(record=True) as w:
            # Cause all warnings to always be triggered.
            warnings.simplefilter("always")
            self.analysis.create_timestamp_table(timestamp_dct)

            # assert
            self.assertEqual(0, len(w))

    def test_calculate_i_meas_tau(self):
        # arrange
        i_meas_df = read_csv("resources/hwc/ipd/fpa", "STATUS.I_MEAS")

        # act
        tau_ipd_act = self.analysis.calculate_i_meas_tau([i_meas_df], duration_decay=(0, 2))

        # assert
        tau_ipd_ref = np.array([0.25506721])
        self.assertAlmostEqual(tau_ipd_ref[0], tau_ipd_act[0], places=6)

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_plot_i_meas_pc(self, mock_show=None):
        # arrange
        i_meas_df = read_csv("resources/hwc/ipd/fpa", "STATUS.I_MEAS")
        i_a_df = read_csv("resources/hwc/ipd/fpa", "IAB.I_A")
        i_ref_df = read_csv("resources/hwc/ipd/fpa", "STATUS.I_REF")

        # act
        tau_ipd = self.analysis.calculate_i_meas_tau([i_meas_df], duration_decay=(0, 2))
        self.analysis.plot_i_meas_pc(
            self._circuit_name, self._timestamp_fgc, [i_meas_df, i_a_df, i_ref_df], xlim=(-10, 3 * tau_ipd)
        )

        # assert
        if mock_show is not None:
            mock_show.assert_called()

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_plot_i_earth_pcnt_pc(self, mock_show=None):
        # arrange
        i_earth_pcnt_df = read_csv("resources/hwc/ipd/fpa", "STATUS.I_EARTH_PCNT")

        # act
        self.analysis.plot_i_earth_pcnt_pc(self._circuit_name, self._timestamp_fgc, i_earth_pcnt_df)

        # assert
        if mock_show is not None:
            mock_show.assert_called()

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_plot_i_earth_pc(self, mock_show=None):
        # arrange
        i_earth_df = read_csv("resources/hwc/ipd/fpa", "IEARTH.IEARTH")

        # act
        self.analysis.plot_i_earth_pc(self._circuit_name, self._timestamp_fgc, i_earth_df)

        # assert
        if mock_show is not None:
            mock_show.assert_called()

    def test_calculate_max_i_earth_pc(self):
        # arrange
        i_earth_df = read_csv("resources/hwc/ipd/fpa", "IEARTH.IEARTH")

        # act
        # assert
        with patch("sys.stdout", new=StringIO()) as fake_out:
            self.analysis.calculate_max_i_earth_pc(i_earth_df, col_name="I_Earth_max")
            self.assertEqual("The maximum unbiased abs(I_EARTH) is -37.35 mA.", fake_out.getvalue().strip())

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_plot_u_res_u_res_slope_u_1_u_2(self, mock_show=None):
        # arrange
        u_res_df = read_csv("resources/hwc/ipd/fpa", "circ.RD3.R4.U_RES_B1")
        u_1_df = read_csv("resources/hwc/ipd/fpa", "circ.RD3.R4.U_1_B1")
        u_2_df = read_csv("resources/hwc/ipd/fpa", "circ.RD3.R4.U_2_B1")

        # act
        with patch("sys.stdout", new=StringIO()) as fake_out:
            u_res_slope_df = self.analysis.calculate_u_res_slope(u_res_df, col_name="dU_QPS/dt")
            self.assertEqual("dU_QPS/dt = 10.828 V/s.", fake_out.getvalue().strip())

        self.analysis.plot_u_res_u_res_slope_u_1_u_2(
            self._circuit_name,
            self._timestamp_qds_a,
            u_res_df,
            u_res_slope_df,
            u_1_df,
            u_2_df,
            xlim=(u_res_slope_df.index[0] - 0.2, u_res_slope_df.index[1] + 0.1),
        )
        # assert
        if mock_show is not None:
            mock_show.assert_called()

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_analyze_leads_voltage_u_hts(self, mock_show=None):
        # arrange
        u_hts_dfs = [
            read_csv("resources/hwc/ipd/fpa", "stat.DFLCS.5R4.RD3.R4.LD1.U_HTS"),
            read_csv("resources/hwc/ipd/fpa", "stat.DFLCS.5R4.RD3.R4.LD2.U_HTS"),
        ]

        # act
        # assert
        self.analysis.analyze_leads_voltage(
            u_hts_dfs, self._circuit_name, self._timestamp_qds_a, signal="U_HTS", value_min=-0.003, value_max=0.003
        )

        if mock_show is not None:
            mock_show.assert_called()

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_analyze_leads_voltage_u_res(self, mock_show=None):
        # arrange
        u_res_dfs = [
            read_csv("resources/hwc/ipd/fpa", "stat.DFLCS.5R4.RD3.R4.LD1.U_RES"),
            read_csv("resources/hwc/ipd/fpa", "stat.DFLCS.5R4.RD3.R4.LD2.U_RES"),
        ]

        # act
        # assert
        self.analysis.analyze_leads_voltage(
            u_res_dfs, self._circuit_name, self._timestamp_qds_a, signal="U_RES", value_min=-0.1, value_max=0.1
        )

        if mock_show is not None:
            mock_show.assert_called()

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_analyze_qh(self, mock_show=None):
        # arrange
        u_hds_dfs = [read_csv("resources/hwc/ipd/fpa", "U_HDS_1_B1"), read_csv("resources/hwc/ipd/fpa", "U_HDS_1_B2")]

        u_hds_ref_dfs = [
            read_csv("resources/hwc/ipd/fpa", "U_HDS_REF_1_B1"),
            read_csv("resources/hwc/ipd/fpa", "U_HDS_REF_1_B2"),
        ]

        # act
        with patch("sys.stdout", new=StringIO()) as fake_out:
            self.analysis.analyze_single_qh_voltage_with_ref(
                self._circuit_name, self._timestamp_qds_a, u_hds_dfs, u_hds_ref_dfs
            )
            self.assertTrue("The QH discharges are labeled: Pass." in fake_out.getvalue().strip())

        # assert
        self.assertEqual("Pass", self.analysis.results_table.loc[0, "QH analysis"])
        if mock_show is not None:
            mock_show.assert_called()

    # RD1.R8
    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_analyze_qh_rd1r8(self, mock_show=None):
        # arrange
        circuit_name = "RD1.R8"
        circuit_type = "IPD2"
        timestamp_qds = 1425136770471000000
        u_hds_dfs = [
            read_csv("resources/hwc/ipd/fpa_rd1.r8", "U_HDS_1"),
            read_csv("resources/hwc/ipd/fpa_rd1.r8", "U_HDS_2"),
        ]

        u_hds_ref_dfs = [
            read_csv("resources/hwc/ipd/fpa_rd1.r8", "U_HDS_REF_1"),
            read_csv("resources/hwc/ipd/fpa_rd1.r8", "U_HDS_REF_2"),
        ]
        analysis = IpdCircuitAnalysis(circuit_type, pd.DataFrame(index=[0]), is_automatic=False)

        # act
        with patch("sys.stdout", new=StringIO()) as fake_out:
            analysis.analyze_single_qh_voltage_with_ref(circuit_name, timestamp_qds, u_hds_dfs, u_hds_ref_dfs)
            self.assertTrue("The QH discharges are labeled: Fail." in fake_out.getvalue().strip())

        # assert
        self.assertEqual("Fail", analysis.results_table.loc[0, "QH analysis"])
        if mock_show is not None:
            mock_show.assert_called()
