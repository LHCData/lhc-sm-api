import unittest
from unittest.mock import patch

import pandas as pd
import numpy as np

from lhcsmapi.analysis.IpdCircuitQuery import IpdCircuitQuery


class TestIpdCircuitQuery(unittest.TestCase):
    def test_constructor_ipd2(self):
        # arrange
        # act
        query = IpdCircuitQuery("IPD", "RD1.L2")

        # assert
        self.assertEqual("IPD2", query.circuit_type)

    def test_constructor_ipd2_b1b2(self):
        # arrange
        # act
        query = IpdCircuitQuery("IPD", "RD2.L1")

        # assert
        self.assertEqual("IPD2_B1B2", query.circuit_type)

    def test_create_report_analysis_template(self):
        # arrange
        query = IpdCircuitQuery("IPD", "RD3.R4")

        # act
        results_act = query.create_report_analysis_template(1543842413340000000, "")

        results_act["lhcsmapi version"] = "1.3.226"

        # assert
        results_ref = pd.DataFrame(
            {
                "Circuit Name": {0: "RD3.R4"},
                "Circuit Family": {0: "RD3"},
                "Period": {0: "HWC 2018-2"},
                "Date (FGC)": {0: "2018-12-03"},
                "Time (FGC)": {0: "14:06:53.340"},
                "FPA Reason": {0: np.nan},
                "Timestamp_PIC": {0: np.nan},
                "Delta_t(FGC-PIC)": {0: np.nan},
                "Ramp rate": {0: np.nan},
                "Plateau duration": {0: np.nan},
                "I_Q_circ": {0: np.nan},
                "MIITS_circ": {0: np.nan},
                "I_Earth_max": {0: np.nan},
                "Delta_t(QPS-PIC)": {0: np.nan},
                "Type of Quench": {0: np.nan},
                "Quench count": {0: np.nan},
                "QDS trigger origin": {0: np.nan},
                "dU_QPS/dt": {0: np.nan},
                "QH analysis": {0: np.nan},
                "Comment": {0: np.nan},
                "Analysis performed by": {0: ""},
                "lhcsmapi version": {0: "1.3.226"},
            }
        )

        results_act.fillna(np.nan, inplace=True)
        pd.testing.assert_frame_equal(results_ref, results_act)
