import unittest
from unittest.mock import patch

import numpy as np
import pandas as pd

from lhcsmapi.analysis.CircuitAnalysis import CircuitAnalysis
from lhcsmapi.analysis.RqCircuitAnalysis import RqCircuitAnalysis
from lhcsmapi.pyedsl.AssertionBuilder import AssertionBuilder
from test.resources.read_csv import read_csv


class RqCircuitAnalysisTest(unittest.TestCase):
    def setUp(self):
        self.circuit_type = "RQ"
        self.circuit_names = ["RQD.A12", "RQF.A12"]
        self.timestamp_fgc_rqd = 1544622149620000000
        self.timestamp_fgc_rqf = 1544622149620000000
        self.timestamp_fgc_ref_rqd = 1521298828680000000
        self.timestamp_fgc_ref_rqf = 1521298828680000000
        self.timestamp_pic_rqd = 1544622149611000000
        self.timestamp_pic_rqf = 1544622149611000000
        self.source_timestamp_ee_rqd_df = pd.DataFrame(
            {
                "source": {0: "UA23.RQD.A12", 1: "UA23.RQD.A12"},
                "timestamp": {0: 1544622149701000000, 1: 1544622569004000000},
            }
        )
        self.timestamp_ee_rqd = 1544622149701000000
        self.source_timestamp_ee_rqf_df = pd.DataFrame(
            {
                "source": {0: "UA23.RQF.A12", 1: "UA23.RQF.A12"},
                "timestamp": {0: 1544622149701000000, 1: 1544622571604000000},
            }
        )
        self.timestamp_ee_rqf = 1544622149701000000
        self.timestamp_ee_ref_rqd = 1521370713675000000
        self.timestamp_ee_ref_rqf = 1521370713675000000

        self.source_timestamp_ee_rqd_ref_df = pd.DataFrame(
            {
                "source": {0: "UA23.RQD.A12", 1: "UA23.RQD.A12"},
                "timestamp": {0: 1521370713675000000, 1: 1521371135404000000},
            }
        )
        self.source_timestamp_ee_rqf_ref_df = pd.DataFrame(
            {
                "source": {0: "UA23.RQF.A12", 1: "UA23.RQF.A12"},
                "timestamp": {0: 1521370713675000000, 1: 1521371141604000000},
            }
        )
        self.source_timestamp_qds_rq_df = pd.DataFrame({"source": {0: "16L2"}, "timestamp": {0: 1544622149598000000}})
        self.source_timestamp_nqps_rqd_df = pd.DataFrame(
            {"source": {0: "B17L2"}, "timestamp": {0: 1544622149613000000}}
        )

        self.source_timestamp_leads_rqd_df = pd.DataFrame()
        self.source_timestamp_leads_rqf_df = pd.DataFrame()

        report_analysis = pd.DataFrame(read_csv("resources/hwc/rq/fpa", "results_table"))
        self.rq_analysis = RqCircuitAnalysis(self.circuit_type, report_analysis, is_automatic=True)

    @patch("matplotlib.pyplot.show")
    def test_analyze_i_meas_rqd_pc(self, mock_show=None):
        i_meas_rqd_df = read_csv("resources/hwc/rq/fpa", "i_meas_rqd_df")
        i_meas_ref_rqd_df = read_csv("resources/hwc/rq/fpa", "i_meas_ref_rqd_df")

        self.rq_analysis.analyze_i_meas_pc(
            self.circuit_names[0],
            self.timestamp_fgc_rqd,
            self.timestamp_fgc_ref_rqd,
            self.timestamp_pic_rqd,
            i_meas_rqd_df,
            i_meas_ref_rqd_df,
        )

        # assert
        if mock_show is not None:
            mock_show.assert_called()

    @patch("matplotlib.pyplot.show")
    def test_analyze_i_meas_rqf_pc(self, mock_show=None):
        i_meas_rqf_df = read_csv("resources/hwc/rq/fpa", "i_meas_rqf_df")
        i_meas_ref_rqf_df = read_csv("resources/hwc/rq/fpa", "i_meas_ref_rqf_df")

        self.rq_analysis.analyze_i_meas_pc(
            self.circuit_names[1],
            self.timestamp_fgc_rqf,
            self.timestamp_fgc_ref_rqf,
            self.timestamp_pic_rqf,
            i_meas_rqf_df,
            i_meas_ref_rqf_df,
        )

        # assert
        if mock_show is not None:
            mock_show.assert_called()

    @patch("matplotlib.pyplot.show")
    def test_analyze_i_earth_pc_rqd(self, mock_show=None):
        i_a_rqd_df = read_csv("resources/hwc/rq/fpa", "i_a_rqd_df")
        i_earth_rqd_df = read_csv("resources/hwc/rq/fpa", "i_earth_rqd_df")
        i_earth_rqd_ref_df = read_csv("resources/hwc/rq/fpa", "i_earth_rqd_ref_df")

        self.rq_analysis.analyze_i_earth_pc(
            self.circuit_names[0], self.timestamp_fgc_rqd, i_a_rqd_df, i_earth_rqd_df, i_earth_rqd_ref_df
        )

        # assert
        if mock_show is not None:
            mock_show.assert_called()

    @patch("matplotlib.pyplot.show")
    def test_analyze_i_earth_pc_rqf(self, mock_show=None):
        i_a_rqf_df = read_csv("resources/hwc/rq/fpa", "i_a_rqf_df")
        i_earth_rqf_df = read_csv("resources/hwc/rq/fpa", "i_earth_rqf_df")
        i_earth_rqf_ref_df = read_csv("resources/hwc/rq/fpa", "i_earth_rqf_ref_df")

        self.rq_analysis.analyze_i_earth_pc(
            self.circuit_names[1], self.timestamp_fgc_rqd, i_a_rqf_df, i_earth_rqf_df, i_earth_rqf_ref_df
        )

        # assert
        if mock_show is not None:
            mock_show.assert_called()

    @patch("matplotlib.pyplot.show")
    def test_analyze_i_earth_pcnt_rqf(self, mock_show=None):
        i_meas_rqf_df = read_csv("resources/hwc/rq/fpa", "i_meas_rqf_df")
        i_meas_ref_rqf_df = read_csv("resources/hwc/rq/fpa", "i_meas_ref_rqf_df")
        i_earth_pcnt_rqf_df = read_csv("resources/hwc/rq/fpa", "i_earth_pcnt_rqf_df")
        i_earth_pcnt_rqf_ref_df = read_csv("resources/hwc/rq/fpa", "i_earth_pcnt_rqf_ref_df")

        self.rq_analysis.analyze_i_earth_pcnt_pc(
            self.circuit_names[1],
            self.timestamp_fgc_rqf,
            i_meas_rqf_df,
            i_meas_ref_rqf_df,
            i_earth_pcnt_rqf_df,
            i_earth_pcnt_rqf_ref_df,
        )

        # assert
        if mock_show is not None:
            mock_show.assert_called()

    @patch("matplotlib.pyplot.show")
    def test_analyze_char_time_u_dump_res_rqd(self, mock_show=None):
        u_dump_res_rqd_df = read_csv("resources/hwc/rq/fpa", "u_dump_res_rqd_df")
        i_meas_rqd_df = read_csv("resources/hwc/rq/fpa", "i_meas_rqd_df")

        self.rq_analysis.analyze_char_time_u_dump_res_ee(
            self.circuit_names[0], self.timestamp_fgc_rqd, u_dump_res_rqd_df, i_meas_rqd_df
        )

        # assert
        if mock_show is not None:
            mock_show.assert_called()

    @patch("matplotlib.pyplot.show")
    def test_analyze_char_time_u_dump_res_rqf(self, mock_show=None):
        u_dump_res_rqf_df = read_csv("resources/hwc/rq/fpa", "u_dump_res_rqf_df")
        i_meas_rqf_df = read_csv("resources/hwc/rq/fpa", "i_meas_rqf_df")

        self.rq_analysis.analyze_char_time_u_dump_res_ee(
            self.circuit_names[0], self.timestamp_fgc_rqd, u_dump_res_rqf_df, i_meas_rqf_df
        )

        # assert
        if mock_show is not None:
            mock_show.assert_called()

    @patch("matplotlib.pyplot.show")
    def test_analyze_delay_time_u_dump_res_ee_rqd(self, mock_show=None):
        i_a_rqd_df = read_csv("resources/hwc/rq/fpa", "i_a_rqd_df")
        i_ref_rqd_df = read_csv("resources/hwc/rq/fpa", "i_ref_rqd_df")
        u_dump_res_rqd_df = read_csv("resources/hwc/rq/fpa", "u_dump_res_rqd_df")

        self.rq_analysis.analyze_delay_time_u_dump_res_ee(
            self.circuit_names[0],
            self.timestamp_fgc_rqd,
            self.timestamp_pic_rqd,
            self.timestamp_ee_rqd,
            i_a_rqd_df,
            i_ref_rqd_df,
            u_dump_res_rqd_df,
        )

        # assert
        if mock_show is not None:
            mock_show.assert_called()

    @patch("matplotlib.pyplot.show")
    def test_analyze_delay_time_u_dump_res_ee_rqf(self, mock_show=None):
        i_a_rqf_df = read_csv("resources/hwc/rq/fpa", "i_a_rqf_df")
        i_ref_rqf_df = read_csv("resources/hwc/rq/fpa", "i_ref_rqf_df")
        u_dump_res_rqf_df = read_csv("resources/hwc/rq/fpa", "u_dump_res_rqf_df")

        self.rq_analysis.analyze_delay_time_u_dump_res_ee(
            self.circuit_names[0],
            self.timestamp_fgc_rqd,
            self.timestamp_pic_rqd,
            self.timestamp_ee_rqd,
            i_a_rqf_df,
            i_ref_rqf_df,
            u_dump_res_rqf_df,
        )

        # assert
        if mock_show is not None:
            mock_show.assert_called()

    def test_temperature_ee_rqd(self):
        t_res_0_rqd_df = read_csv("resources/hwc/rq/fpa", "t_res_0_rqd_df")
        t_res_1_rqd_df = read_csv("resources/hwc/rq/fpa", "t_res_1_rqd_df")

        t_res_0_rqd_ref_df = read_csv("resources/hwc/rq/fpa", "t_res_0_rqd_ref_df")
        t_res_1_rqd_ref_df = read_csv("resources/hwc/rq/fpa", "t_res_1_rqd_ref_df")

        AssertionBuilder().with_signal([t_res_0_rqd_df, t_res_1_rqd_df]).compare_to_reference(
            signal_ref_dfs=[t_res_0_rqd_ref_df, t_res_1_rqd_ref_df], abs_margin=25, scaling=1
        )

    def test_temperature_ee_rqf(self):
        t_res_0_rqf_df = read_csv("resources/hwc/rq/fpa", "t_res_0_rqf_df")
        t_res_1_rqf_df = read_csv("resources/hwc/rq/fpa", "t_res_1_rqf_df")

        t_res_0_rqf_ref_df = read_csv("resources/hwc/rq/fpa", "t_res_0_rqf_ref_df")
        t_res_1_rqf_ref_df = read_csv("resources/hwc/rq/fpa", "t_res_1_rqf_ref_df")

        AssertionBuilder().with_signal([t_res_0_rqf_df, t_res_1_rqf_df]).compare_to_reference(
            signal_ref_dfs=[t_res_0_rqf_ref_df, t_res_1_rqf_ref_df], abs_margin=25, scaling=1
        )

    @patch("matplotlib.pyplot.show")
    def test_analyze_qds(self, mock_show=None):
        iqps_analog_dfs = [
            [
                read_csv("resources/hwc/rq/fpa", "u_qs0_ext_rq_df"),
                read_csv("resources/hwc/rq/fpa", "u_qs0_int_rq_df"),
                read_csv("resources/hwc/rq/fpa", "u_1_ext_rq_df"),
                read_csv("resources/hwc/rq/fpa", "u_2_ext_rq_df"),
                read_csv("resources/hwc/rq/fpa", "u_1_int_rq_df"),
                read_csv("resources/hwc/rq/fpa", "u_2_int_rq_df"),
            ]
        ]

        iqps_digital_dfs = [
            [
                read_csv("resources/hwc/rq/fpa", "st_magnet_ok_ext_rq_df"),
                read_csv("resources/hwc/rq/fpa", "st_magnet_ok_int_rq_df"),
                read_csv("resources/hwc/rq/fpa", "st_nqd0_ext_rq_df"),
                read_csv("resources/hwc/rq/fpa", "st_nqd0_int_rq_df"),
            ]
        ]

        # due to a conflict with u_diode_rqd_df used for DIODE analysis, the signal is referred to as u_diode_rqd_pm_df
        u_nqps_rqd_dfs = [
            [read_csv("resources/hwc/rq/fpa", "u_diode_rqd_pm_df"), read_csv("resources/hwc/rq/fpa", "u_ref_rqd_df")]
        ]
        # due to a conflict with u_diode_rqf_df used for DIODE analysis, the signal is referred to as u_diode_rqf_pm_df
        u_nqps_rqf_dfs = [
            [read_csv("resources/hwc/rq/fpa", "u_diode_rqf_pm_df"), read_csv("resources/hwc/rq/fpa", "u_ref_rqf_df")]
        ]

        self.rq_analysis.analyze_qds(
            self.source_timestamp_qds_rq_df,
            self.circuit_names,
            iqps_analog_dfs,
            iqps_digital_dfs,
            u_nqps_rqd_dfs,
            u_nqps_rqf_dfs,
        )

        # assert
        if mock_show is not None:
            mock_show.assert_called()

    @patch("matplotlib.pyplot.show")
    def test_analyze_qh(self, mock_show=None):
        u_hds_rq_dfs = [
            [read_csv("resources/hwc/rq/fpa", "u_hds_1_rq_df"), read_csv("resources/hwc/rq/fpa", "u_hds_2_rq_df")]
        ]
        u_hds_rq_ref_dfs = [
            [
                read_csv("resources/hwc/rq/fpa", "u_hds_1_rq_ref_df"),
                read_csv("resources/hwc/rq/fpa", "u_hds_2_rq_ref_df"),
            ]
        ]

        self.rq_analysis.analyze_multi_qh_voltage_with_ref(
            self.source_timestamp_qds_rq_df, u_hds_rq_dfs, u_hds_rq_ref_dfs
        )

        # assert
        if mock_show is not None:
            mock_show.assert_called()

    @patch("matplotlib.pyplot.show")
    def test_analyze_diode_leads_rqd(self, mock_show=None):
        # due to a conflict with i_a_rqd_df used for PC analysis, the signal is referred to as i_a_rqd_raw_df
        i_meas_u_diode_u_ref_rqd_pm_dfs = [
            [
                read_csv("resources/hwc/rq/fpa", "i_a_rqd_raw_df"),
                read_csv("resources/hwc/rq/fpa", "u_diode_a_rqd_df"),
                read_csv("resources/hwc/rq/fpa", "u_diode_b_rqd_df"),
                read_csv("resources/hwc/rq/fpa", "u_ref_rqd_df"),
            ]
        ]

        i_meas_u_diode_rqd_nxcals_dfs = [
            [
                read_csv("resources/hwc/rq/fpa", "i_meas_diode_rqd_df"),
                read_csv("resources/hwc/rq/fpa", "u_diode_rqd_df"),
            ]
        ]

        self.rq_analysis.analyze_diode_leads(
            self.source_timestamp_qds_rq_df,
            self.timestamp_fgc_rqd,
            self.rq_analysis.results_table["I_Q_MQD"],
            self.circuit_names[0],
            i_meas_u_diode_u_ref_rqd_pm_dfs,
            i_meas_u_diode_rqd_nxcals_dfs,
            5,
        )

        # assert
        if mock_show is not None:
            mock_show.assert_called()

    @patch("matplotlib.pyplot.show")
    def test_analyze_diode_leads_rqf(self, mock_show=None):
        # due to a conflict with i_a_rqf_df used for PC analysis, the signal is referred to as i_a_rqf_raw_df
        i_meas_u_diode_u_ref_rqf_pm_dfs = [
            [
                read_csv("resources/hwc/rq/fpa", "i_a_rqf_raw_df"),
                read_csv("resources/hwc/rq/fpa", "u_diode_a_rqf_df"),
                read_csv("resources/hwc/rq/fpa", "u_diode_b_rqf_df"),
                read_csv("resources/hwc/rq/fpa", "u_ref_rqf_df"),
            ]
        ]

        i_meas_u_diode_rqf_nxcals_dfs = [
            [
                read_csv("resources/hwc/rq/fpa", "i_meas_diode_rqf_df"),
                read_csv("resources/hwc/rq/fpa", "u_diode_rqf_df"),
            ]
        ]

        self.rq_analysis.analyze_diode_leads(
            self.source_timestamp_qds_rq_df,
            self.timestamp_fgc_rqf,
            self.rq_analysis.results_table["I_Q_MQF"],
            self.circuit_names[1],
            i_meas_u_diode_u_ref_rqf_pm_dfs,
            i_meas_u_diode_rqf_nxcals_dfs,
            5,
        )

        # assert
        if mock_show is not None:
            mock_show.assert_called()

    def test_analyze_leads_u_res(self):
        u_res_rqd_dfs = [
            read_csv("resources/hwc/rq/fpa", "u_res_1_rqd_df"),
            read_csv("resources/hwc/rq/fpa", "u_res_2_rqd_df"),
        ]
        u_res_rqf_dfs = [
            read_csv("resources/hwc/rq/fpa", "u_res_1_rqf_df"),
            read_csv("resources/hwc/rq/fpa", "u_res_2_rqf_df"),
        ]
        AssertionBuilder().with_signal(u_res_rqd_dfs + u_res_rqf_dfs).has_min_max_value(value_min=-0.1, value_max=0.1)

    def test_analyze_leads_u_hts(self):
        u_hts_rqd_dfs = [
            read_csv("resources/hwc/rq/fpa", "u_hts_1_rqd_df"),
            read_csv("resources/hwc/rq/fpa", "u_hts_2_rqd_df"),
        ]
        u_hts_rqf_dfs = [
            read_csv("resources/hwc/rq/fpa", "u_hts_1_rqf_df"),
            read_csv("resources/hwc/rq/fpa", "u_hts_2_rqf_df"),
        ]
        AssertionBuilder().with_signal(u_hts_rqd_dfs + u_hts_rqf_dfs).has_min_max_value(
            value_min=-0.003, value_max=0.003
        )

    def test_create_mp3_results_table(self):
        # arrange
        circuit_type = "RQ"
        is_automatic = True
        results_table = read_csv("resources/hwc/rq/fpa", "results_table")
        rq_analysis = RqCircuitAnalysis(circuit_type, results_table, is_automatic=is_automatic)

        # act
        mp3_act = rq_analysis.create_mp3_results_table(
            min(self.timestamp_fgc_rqd, self.timestamp_fgc_rqd), min(self.timestamp_pic_rqd, self.timestamp_pic_rqd)
        )

        # assert
        mp3_ref = pd.DataFrame(
            {
                "Circuit Name": {0: "RQ.A12"},
                "Circuit Family": {0: "RQ"},
                "Period": {0: "HWC 2021"},
                "Date (FGC)": {0: "2018-12-12"},
                "Time (FGC)": {0: "14:42:29.620"},
                "FPA Reason": {0: np.nan},
                "Timestamp_PIC": {0: "2018-12-12 14:42:29.620"},
                "Delta_t(FGC-PIC)": {0: -9.0},
                "Delta_t(EE_RQD-PIC)": {0: 81.0},
                "Delta_t(EE_RQF-PIC)": {0: 81.0},
                "Ramp rate RQD": {0: 10.0},
                "Ramp rate RQF": {0: 10.0},
                "Plateau duration RQD": {0: 0},
                "Plateau duration RQF": {0: 0},
                "I_Q_RQD": {0: 11154.3},
                "I_Q_RQF": {0: 11154.2},
                "MIITS_RQD": {0: 1802.22},
                "MIITS_RQF": {0: 1872.826},
                "I_Earth_max_RQD": {0: 37.071286},
                "I_Earth_max_RQF": {0: 36.363277000000004},
                "EE analysis RQD": {0: np.nan},
                "EE analysis RQF": {0: np.nan},
                "V feeler analysis RQD": {0: np.nan},
                "V feeler analysis RQF": {0: np.nan},
                "U_EE_max_RQD": {0: 73.54279145708544},
                "U_EE_max_RQF": {0: 73.7999734734},
                "Position": {0: "16L2"},
                "I_Q_MQD": {0: 11154},
                "I_Q_MQF": {0: 11154},
                "Nr in Q event": {0: np.nan},
                "Delta_t(iQPS-PIC)": {0: -13.0},
                "nQPS RQD crate name": {0: "B17L2"},
                "nQPS RQF crate name": {0: "B17L2"},
                "Delta_t(nQPS_RQD-PIC)": {0: 2.0},
                "Delta_t(nQPS_RQF-PIC)": {0: 2.0},
                "Type of Quench": {0: np.nan},
                "Quench origin": {0: np.nan},
                "Quench count": {0: np.nan},
                "QDS trigger origin": {0: "RQF/INT"},
                "dU_iQPS/dt_RQD": {0: np.nan},
                "dU_iQPS/dt_RQF": {0: -7.78},
                "V_symm_max_RQD": {0: np.nan},
                "V_symm_max_RQF": {0: np.nan},
                "dV_symm_RQD/dt": {0: np.nan},
                "dV_symm_RQF/dt": {0: np.nan},
                "R_DL_max_RQD": {0: 45.89},
                "R_DL_max_RQF": {0: 40.4},
                "I_RQD at R_DL_max_RQD": {0: 5312.05},
                "I_RQF at R_DL_max_RQF": {0: 6530.05},
                "QH analysis": {0: "Pass"},
                "SSS ID": {0: np.nan},
                "I_Q_SM18": {0: np.nan},
                "dI_Q_Acc": {0: np.nan},
                "dI_Q_LHC": {0: np.nan},
                "Comment": {0: "-"},
                "Analysis performed by": {0: "mmacieje"},
                "lhcsmapi version": {0: "1.4.35"},
            }
        )
        mp3_act.fillna(np.nan, inplace=True)
        mp3_ref.fillna(np.nan, inplace=True)
        pd.testing.assert_frame_equal(mp3_act, mp3_ref)

    def test_get_rq_circuit_parameters_rqf_a12(self):
        # arrange
        circuit_type = "RQ"
        is_automatic = True
        results_table = read_csv("resources/hwc/rq/fpa", "results_table")
        rq_analysis = RqCircuitAnalysis(circuit_type, results_table, is_automatic=is_automatic)

        # act
        rq_circuit_parameters = rq_analysis.display_parameters_table("RQF.A12")
        # assert
        # assert
        rq_circuit_parameters_ref = pd.DataFrame(
            {
                "Circuit name": {0: "RQD/F.A12"},
                "Number of apertures": {0: 47},
                "Beam 1": {0: "Ext"},
                "Beam 2": {0: "Int"},
                "RQF B1/B2": {0: "Odd/Even"},
                "RQD B1/B2": {0: "Even/Odd"},
                "Inductance [H]": {0: 0.2744},
                "EE Resistance [ohm]": {0: 0.0066},
            }
        )
        pd.testing.assert_frame_equal(rq_circuit_parameters_ref, rq_circuit_parameters)

    def test_get_rq_circuit_parameters_rqd_a12(self):
        # arrange
        circuit_type = "RQ"
        is_automatic = True
        results_table = read_csv("resources/hwc/rq/fpa", "results_table")
        rq_analysis = RqCircuitAnalysis(circuit_type, results_table, is_automatic=is_automatic)

        # act
        rq_circuit_parameters = rq_analysis.display_parameters_table("RQD.A12")
        # assert
        # assert
        rq_circuit_parameters_ref = pd.DataFrame(
            {
                "Circuit name": {0: "RQD/F.A12"},
                "Number of apertures": {0: 47},
                "Beam 1": {0: "Ext"},
                "Beam 2": {0: "Int"},
                "RQF B1/B2": {0: "Odd/Even"},
                "RQD B1/B2": {0: "Even/Odd"},
                "Inductance [H]": {0: 0.2744},
                "EE Resistance [ohm]": {0: 0.0066},
            }
        )
        pd.testing.assert_frame_equal(rq_circuit_parameters_ref, rq_circuit_parameters)
