from importlib.metadata import version
import unittest
from unittest.mock import patch

import numpy as np
import pandas as pd

from lhcsmapi import reference
from lhcsmapi.analysis.RqCircuitQuery import RqCircuitQuery
from test.resources.read_csv import read_csv


class TestRqCircuitQuery(unittest.TestCase):
    def setUp(self):
        self.circuit_type = "RQ"
        self.circuit_name = "RQD.A12"
        self.timestamp_fgc_rqd = 1544622149620000000
        self.timestamp_fgc_rqf = 1544622149620000000
        self.timestamp_pic_rqd = 1544622149620000000
        self.timestamp_pic_rqf = 1544622149620000000
        self.author = "mmacieje"

    def test_create_report_analysis_template(self):
        # arrange
        source_timestamp_qds_rq_df = pd.DataFrame({"source": {0: "16L2"}, "timestamp": {0: 1544622149598000000}})
        source_timestamp_nqps_rqd_df = pd.DataFrame({"source": {0: "B17L2"}, "timestamp": {0: 1544622149613000000}})
        i_meas_rqd_df = read_csv("resources/hwc/rq/fpa", "i_meas_rqd_df")
        i_meas_rqf_df = read_csv("resources/hwc/rq/fpa", "i_meas_rqf_df")

        rqd_query = RqCircuitQuery(self.circuit_type, self.circuit_name)

        # act
        results_act = rqd_query.create_report_analysis_template(
            source_timestamp_qds_rq_df,
            source_timestamp_nqps_rqd_df,
            min(self.timestamp_fgc_rqd, self.timestamp_fgc_rqf),
            min(self.timestamp_pic_rqd, self.timestamp_pic_rqf),
            i_meas_rqd_df,
            i_meas_rqf_df,
            author=self.author,
        )
        results_act["lhcsmapi version"] = "1.3.226"

        # assert
        results_ref = pd.DataFrame(
            {
                "Circuit Name": {0: "RQ.A12"},
                "Circuit Family": {0: "RQ"},
                "Period": {0: "HWC 2018-2"},
                "Date (FGC)": {0: "2018-12-12"},
                "Time (FGC)": {0: "14:42:29.620"},
                "FPA Reason": {0: np.nan},
                "Timestamp_PIC": {0: np.nan},
                "Delta_t(FGC-PIC)": {0: np.nan},
                "Delta_t(EE_RQD-PIC)": {0: np.nan},
                "Delta_t(EE_RQF-PIC)": {0: np.nan},
                "Ramp rate RQD": {0: np.nan},
                "Ramp rate RQF": {0: np.nan},
                "Plateau duration RQD": {0: np.nan},
                "Plateau duration RQF": {0: np.nan},
                "I_Q_RQD": {0: np.nan},
                "I_Q_RQF": {0: np.nan},
                "MIITS_RQD": {0: np.nan},
                "MIITS_RQF": {0: np.nan},
                "I_Earth_max_RQD": {0: np.nan},
                "I_Earth_max_RQF": {0: np.nan},
                "EE analysis RQD": {0: np.nan},
                "EE analysis RQF": {0: np.nan},
                "V feeler analysis RQD": {0: np.nan},
                "V feeler analysis RQF": {0: np.nan},
                "U_EE_max_RQD": {0: np.nan},
                "U_EE_max_RQF": {0: np.nan},
                "Position": {0: "16L2"},
                "I_Q_MQD": {0: 11154},
                "I_Q_MQF": {0: 11154},
                "Nr in Q event": {0: np.nan},
                "Delta_t(iQPS-PIC)": {0: -22.0},
                "nQPS RQD crate name": {0: "B17L2"},
                "nQPS RQF crate name": {0: "B17L2"},
                "Delta_t(nQPS_RQD-PIC)": {0: -7.0},
                "Delta_t(nQPS_RQF-PIC)": {0: -7.0},
                "Type of Quench": {0: np.nan},
                "Quench origin": {0: np.nan},
                "Quench count": {0: np.nan},
                "QDS trigger origin": {0: np.nan},
                "dU_iQPS/dt_RQD": {0: np.nan},
                "dU_iQPS/dt_RQF": {0: np.nan},
                "V_symm_max_RQD": {0: np.nan},
                "V_symm_max_RQF": {0: np.nan},
                "dV_symm_RQD/dt": {0: np.nan},
                "dV_symm_RQF/dt": {0: np.nan},
                "R_DL_max_RQD": {0: np.nan},
                "R_DL_max_RQF": {0: np.nan},
                "I_RQD at R_DL_max_RQD": {0: np.nan},
                "I_RQF at R_DL_max_RQF": {0: np.nan},
                "QH analysis": {0: np.nan},
                "SSS ID": {0: np.nan},
                "I_Q_SM18": {0: np.nan},
                "dI_Q_Acc": {0: np.nan},
                "dI_Q_LHC": {0: np.nan},
                "Comment": {0: np.nan},
                "Analysis performed by": {0: "mmacieje"},
                "lhcsmapi version": {0: "1.3.226"},
                "timestamp_nqps": {0: 1544622149613000000},
            }
        )

        results_act.fillna(np.nan, inplace=True)
        pd.testing.assert_frame_equal(results_ref, results_act)

    def test_create_report_analysis_template_with_empty_dataframes(self):
        # arrange
        source_timestamp_qds_rq_df = pd.DataFrame()
        source_timestamp_nqps_rqd_df = pd.DataFrame()
        i_meas_rqd_df = pd.DataFrame()
        i_meas_rqf_df = pd.DataFrame()

        # act
        rqd_query = RqCircuitQuery(self.circuit_type, self.circuit_name)
        results_act = rqd_query.create_report_analysis_template(
            source_timestamp_qds_rq_df,
            source_timestamp_nqps_rqd_df,
            min(self.timestamp_fgc_rqd, self.timestamp_fgc_rqf),
            min(self.timestamp_pic_rqd, self.timestamp_pic_rqf),
            i_meas_rqd_df,
            i_meas_rqf_df,
            author=self.author,
        )

        # assert
        columns = reference.get_quench_database_columns(circuit_type="RQ", table_type="MP3")
        values = [
            ["RQ.A12", "RQ", "HWC 2018-2", "2018-12-12", "14:42:29.620"]
            + [np.nan] * 50
            + ["mmacieje", version("lhcsmapi")]
        ]
        results_ref = pd.DataFrame(values, columns=columns, dtype=object)
        pd.testing.assert_frame_equal(results_ref, results_act)
