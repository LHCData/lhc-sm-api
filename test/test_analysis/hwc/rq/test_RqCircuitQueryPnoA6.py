import unittest
from unittest.mock import patch

import matplotlib.pyplot as plt

from lhcsmapi.Time import Time
from lhcsmapi.analysis.RqCircuitQuery import RqCircuitQuery
from lhcsmapi.api.processing import SignalProcessing
from test.resources.read_csv import read_csv


class TestRqCircuitQueryPNOA6(unittest.TestCase):
    @patch("matplotlib.pyplot.show")
    def test_calculate_current_plateau_start_end(self, mock_show=None):
        # arrange
        circuit_type = "RQ"
        circuit_names = ["RQD.A12", "RQF.A12"]
        rq_query = RqCircuitQuery(circuit_type, circuit_names)

        t_start = Time.to_unix_timestamp("2015-01-22 13:49:10.168")
        t_end = Time.to_unix_timestamp("2015-01-22 18:45:22.697")

        i_meas_nxcals_dfs = [
            read_csv("resources/hwc/rq/pno_a6", "I_MEAS_RQD"),
            read_csv("resources/hwc/rq/pno_a6", "I_MEAS_RQF"),
        ]

        i_meas_raw_nxcals_dfs = [
            read_csv("resources/hwc/rq/pno_a6", "I_MEAS_RAW_RQD"),
            read_csv("resources/hwc/rq/pno_a6", "I_MEAS_RAW_RQF"),
        ]
        # act
        with patch("lhcsmapi.api.query.query_nxcals_by_variables", return_value=i_meas_raw_nxcals_dfs):
            plateau_start, plateau_end = rq_query.calculate_current_plateau_start_end(
                t_start,
                t_end,
                i_meas_threshold=500,
                min_duration_in_sec=600,
                time_shift_in_sec=(240, 60),
                spark="spark",
            )

        plateau_start_ref = [1421932321060988672, 1421935022213559808, 1421939083443909632]
        plateau_end_ref = [1421933880166026752, 1421938653435474688, 1421946010850796032]

        self.assertListEqual(plateau_start_ref, plateau_start)
        self.assertListEqual(plateau_end_ref, plateau_end)

        ax = i_meas_nxcals_dfs[0].plot(figsize=(30, 10))
        i_meas_nxcals_dfs[1].plot(ax=ax, grid=True)
        for ps, pe in zip(plateau_start, plateau_end):
            ax.axvspan(
                xmin=(ps - Time.to_unix_timestamp(i_meas_raw_nxcals_dfs[0].index[0])) / 1e9,
                xmax=(pe - Time.to_unix_timestamp(i_meas_raw_nxcals_dfs[0].index[0])) / 1e9,
                facecolor="xkcd:goldenrod",
            )
        ax.set_xlabel("time, [s]", fontsize=15)
        ax.set_ylabel("I_MEAS, [A]", fontsize=15)
        ax.tick_params(labelsize=15)
        plt.show()

        if mock_show is not None:
            mock_show.assert_called()
