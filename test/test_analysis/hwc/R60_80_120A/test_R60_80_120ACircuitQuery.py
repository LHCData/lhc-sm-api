import unittest
from unittest.mock import patch

import pandas as pd
import numpy as np

from lhcsmapi.analysis.R60_80_120ACircuitQuery import R60_80_120ACircuitQuery


class TestR60_80_120ACircuitQuery(unittest.TestCase):
    def test_create_report_analysis_template(self):
        # arrange
        query = R60_80_120ACircuitQuery("60A", "RCBV21.L2B2")

        # act
        results_act = query.create_report_analysis_template(1520859035920000000, "")

        results_act["lhcsmapi version"] = "1.3.226"

        # assert
        results_ref = pd.DataFrame(
            {
                "Circuit Name": {0: "RCBV21.L2B2"},
                "Circuit Family": {0: "RCBV"},
                "Period": {0: "HWC 2018-1"},
                "Date (FGC)": {0: "2018-03-12"},
                "Time (FGC)": {0: "13:50:35.920"},
                "FPA Reason": {0: np.nan},
                "Ramp rate": {0: np.nan},
                "Plateau duration": {0: np.nan},
                "I_Q_circ": {0: np.nan},
                "MIITS_circ": {0: np.nan},
                "I_Earth_max": {0: np.nan},
                "Type of Quench": {0: np.nan},
                "Quench count": {0: np.nan},
                "Comment": {0: np.nan},
                "Analysis performed by": {0: ""},
                "lhcsmapi version": {0: "1.3.226"},
            }
        )

        results_act.fillna(np.nan, inplace=True)
        pd.testing.assert_frame_equal(results_ref, results_act)
