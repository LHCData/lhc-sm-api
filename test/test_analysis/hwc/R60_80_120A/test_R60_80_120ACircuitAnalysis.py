import unittest
from unittest.mock import patch

import pandas as pd
import numpy as np

from lhcsmapi.analysis.R60_80_120ACircuitAnalysis import R60_80_120ACircuitAnalysis
from test.resources.read_csv import read_csv


class TestR60_80_120ACircuitAnalysis(unittest.TestCase):
    circuit_type = "60A"
    circuit_name = "RCBV21.L2B2"
    is_automatic = True
    timestamp_fgc = 1520859035920000000

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_plot_i_meas_pc(self, mock_show=None):
        # arrange
        i_meas_df = read_csv("resources/hwc/60A/fpa", "I_MEAS")
        i_a_df = read_csv("resources/hwc/60A/fpa", "I_A")
        i_ref_df = read_csv("resources/hwc/60A/fpa", "I_REF")
        analysis = R60_80_120ACircuitAnalysis(self.circuit_type, pd.DataFrame())

        # act
        analysis.plot_i_meas_pc(self.circuit_name, self.timestamp_fgc, [i_meas_df, i_a_df, i_ref_df])

        # assert
        if mock_show is not None:
            mock_show.assert_called()

    def test_find_time_of_quench(self):
        # arrange
        i_a_df = read_csv("resources/hwc/60A/fpa", "I_A")
        i_ref_df = read_csv("resources/hwc/60A/fpa", "I_REF")

        analysis = R60_80_120ACircuitAnalysis(self.circuit_type, pd.DataFrame())

        # act
        t_quench_act = analysis.estimate_quench_start_from_i_ref_i_a(i_ref_df, i_a_df)

        # assert
        t_quench_ref = -0.038
        self.assertEqual(t_quench_ref, t_quench_act)

    def test_create_mp3_results_table(self):
        # arrange
        results_table = pd.DataFrame(
            {
                "Circuit Name": {0: "RCBV21.L2B2"},
                "Circuit Family": {0: "RCBV21"},
                "Period": {0: "HWC 2018-1"},
                "Date (FGC)": {0: "2018-03-12"},
                "Time (FGC)": {0: "13:50:35.920"},
                "FPA Reason": {0: "Magnet quench"},
                "Timestamp_PIC": {0: np.nan},
                "Delta_t(FGC-PIC)": {0: np.nan},
                "Ramp rate": {0: 0.5},
                "Plateau duration": {0: 0},
                "I_Q_circ": {0: 55.0},
                "MIITS_circ": {0: 0.00035},
                "I_Earth_max": {0: np.nan},
                "Type of Quench": {0: "Training"},
                "Quench count": {0: np.nan},
                "Comment": {0: "-"},
                "Analysis performed by": {0: ""},
                "lhcsmapi version": {0: "1.4.35"},
                "timestamp_fgc": {0: 1520859035920000000},
            }
        )

        # act
        analysis = R60_80_120ACircuitAnalysis(self.circuit_type, results_table, is_automatic=self.is_automatic)
        mp3_results_table_act = analysis.create_mp3_results_table()

        # assert
        mp3_results_table_ref = pd.DataFrame(
            {
                "Circuit Name": {0: "RCBV21.L2B2"},
                "Circuit Family": {0: "RCBV21"},
                "Period": {0: "HWC 2018-1"},
                "Date (FGC)": {0: "2018-03-12"},
                "Time (FGC)": {0: "13:50:35.920"},
                "FPA Reason": {0: "Magnet quench"},
                "Ramp rate": {0: 0.5},
                "Plateau duration": {0: 0},
                "I_Q_circ": {0: 55.0},
                "MIITS_circ": {0: 0.00035},
                "I_Earth_max": {0: np.nan},
                "Type of Quench": {0: "Training"},
                "Quench count": {0: np.nan},
                "Comment": {0: "-"},
                "Analysis performed by": {0: ""},
                "lhcsmapi version": {0: "1.4.35"},
            }
        )

        mp3_results_table_act.fillna(np.nan, inplace=True)
        mp3_results_table_ref.fillna(np.nan, inplace=True)
        pd.testing.assert_frame_equal(mp3_results_table_ref, mp3_results_table_act)

    def test_find_time_of_quench_rcbh16(self):
        # arrange
        circuit_type = "60A"
        analysis = R60_80_120ACircuitAnalysis(self.circuit_type, None, is_automatic=self.is_automatic)
        i_a_df = read_csv("resources/hwc/60A/fpa_rcbh16", "I_A")
        i_ref_df = read_csv("resources/hwc/60A/fpa_rcbh16", "I_REF")

        # act
        t_quench_act = analysis.estimate_quench_start_from_i_ref_i_a(i_ref_df, i_a_df)

        # assert
        t_quench_ref = 0.0
        self.assertEqual(t_quench_ref, t_quench_act)
