import unittest
import warnings
from unittest.mock import patch
from io import StringIO

import pandas as pd
import numpy as np

from lhcsmapi.analysis.R600ACircuitAnalysis import R600ACircuitAnalysis, find_non_zero_min
from test.resources.read_csv import read_csv


class Test600ACircuitQuery(unittest.TestCase):
    # WITH/WITHOUT EE
    def test_create_timestamp_table_missynchronized_qps_ee(self):
        # arrange
        timestamp_dct = {
            "FGC": 1493246176000000000,
            "PIC": 1493246176022000000,
            "EE": 1493246176049000000,
            "QDS_A": 1493246176050000000,
            "QDS_B": 1493246176051000000,
        }
        analysis = R600ACircuitAnalysis("600A", pd.DataFrame(), is_automatic=True)

        # act
        with warnings.catch_warnings(record=True) as w:
            # Cause all warnings to always be triggered.
            warnings.simplefilter("always")
            # Trigger a warning.
            analysis.create_timestamp_table(timestamp_dct, "ROD.A67B1")
            self.assertEqual("FGC is the first timestamp, potentially a PC trip not a quench!", str(w[0].message))
            self.assertEqual("QDS_A and FGC PM events are not synchronized to +/- 40 ms!", str(w[1].message))

    def test_create_timestamp_table_missynchronized_qps_fgc_ee(self):
        # arrange
        timestamp_dct = {
            "FGC": 1493246176040000000,
            "PIC": 1493246176022000000,
            "EE": 1493246176019000000,
            "QDS_A": 1493246175924000000,
            "QDS_B": 1493246175925000000,
        }
        analysis = R600ACircuitAnalysis("600A", pd.DataFrame(), is_automatic=True)

        # act
        with warnings.catch_warnings(record=True) as w:
            # Cause all warnings to always be triggered.
            warnings.simplefilter("always")
            # Trigger a warning.
            analysis.create_timestamp_table(timestamp_dct, "ROD.A67B1")

            self.assertEqual("QDS_A and FGC PM events are not synchronized to +/- 40 ms!", str(w[0].message))
            self.assertEqual("QDS_A and EE PM events are not synchronized to +/- 40 ms!", str(w[1].message))

    def test_create_timestamp_table_missing_qds_b_missynchronized_qps_fgc_ee(self):
        # arrange
        timestamp_dct = {
            "FGC": 1493246176040000000,
            "PIC": 1493246176022000000,
            "EE": 1493246176019000000,
            "QDS_A": 1493246175924000000,
            "QDS_B": np.nan,
        }
        analysis = R600ACircuitAnalysis("600A", pd.DataFrame(), is_automatic=True)

        # act
        with warnings.catch_warnings(record=True) as w:
            # Cause all warnings to always be triggered.
            warnings.simplefilter("always")
            # Trigger a warning.
            analysis.create_timestamp_table(timestamp_dct, "ROD.A67B1")

            self.assertEqual("QDS Board B not present in PM buffer!", str(w[0].message))
            self.assertEqual("QDS_A and FGC PM events are not synchronized to +/- 40 ms!", str(w[1].message))
            self.assertEqual("QDS_A and EE PM events are not synchronized to +/- 40 ms!", str(w[3].message))

    def test_find_time_of_quench(self):
        # arrange
        i_ref_df = read_csv("resources/hwc/600A", "I_REF_ROD.A67B1")
        i_a_df = read_csv("resources/hwc/600A", "I_A_ROD.A67B1")

        # act
        analysis = R600ACircuitAnalysis("600A", pd.DataFrame(), is_automatic=True)
        t_quench_act = analysis.estimate_quench_start_from_i_ref_i_a(i_ref_df, i_a_df)

        # assert
        t_quench_ref = -0.079
        self.assertAlmostEqual(t_quench_ref, t_quench_act)

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_plot_i_meas_pc(self, mock_show=None):
        # arrange
        i_meas_df = read_csv("resources/hwc/600A", "I_MEAS_ROD.A67B1")
        i_ref_df = read_csv("resources/hwc/600A", "I_REF_ROD.A67B1")
        i_a_df = read_csv("resources/hwc/600A", "I_A_ROD.A67B1")
        circuit_name = "ROD.A67B1"
        timestamp_fgc = 1493246176040000000

        # act
        analysis = R600ACircuitAnalysis("600A", pd.DataFrame(), is_automatic=True)
        t_quench = analysis.estimate_quench_start_from_i_ref_i_a(i_ref_df, i_a_df)
        analysis.plot_i_meas_pc(
            circuit_name, timestamp_fgc, [i_meas_df, i_a_df, i_ref_df], xlim=(t_quench - 0.1, t_quench + 0.1)
        )

        # assert
        if mock_show is not None:
            mock_show.assert_called()

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_plot_i_meas_pc_zoom(self, mock_show=None):
        # arrange
        i_meas_df = read_csv("resources/hwc/600A", "I_MEAS_ROD.A67B1")
        i_ref_df = read_csv("resources/hwc/600A", "I_REF_ROD.A67B1")
        i_a_df = read_csv("resources/hwc/600A", "I_A_ROD.A67B1")
        circuit_name = "ROD.A67B1"
        timestamp_fgc = 1493246176040000000

        # act
        analysis = R600ACircuitAnalysis("600A", pd.DataFrame(), is_automatic=True)
        t_quench = analysis.estimate_quench_start_from_i_ref_i_a(i_ref_df, i_a_df)
        analysis.plot_i_meas_pc_zoom(
            circuit_name, timestamp_fgc, t_quench, [i_meas_df, i_a_df, i_ref_df], xlim=(t_quench - 0.1, t_quench + 0.1)
        )

        # assert
        if mock_show is not None:
            mock_show.assert_called()

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_plot_i_meas_pc_zoom_without_xlim(self, mock_show=None):
        # arrange
        i_meas_df = read_csv("resources/hwc/600A", "I_MEAS_ROD.A67B1")
        i_ref_df = read_csv("resources/hwc/600A", "I_REF_ROD.A67B1")
        i_a_df = read_csv("resources/hwc/600A", "I_A_ROD.A67B1")
        circuit_name = "ROD.A67B1"
        timestamp_fgc = 1493246176040000000

        # act
        analysis = R600ACircuitAnalysis("600A", pd.DataFrame(), is_automatic=True)
        t_quench = analysis.estimate_quench_start_from_i_ref_i_a(i_ref_df, i_a_df)
        analysis.plot_i_meas_pc_zoom(circuit_name, timestamp_fgc, t_quench, [i_meas_df, i_a_df, i_ref_df])

        # assert
        if mock_show is not None:
            mock_show.assert_called()

    def test_analyze_i_meas_pc_trigger(self):
        # arrange
        analysis = R600ACircuitAnalysis("600A", pd.DataFrame(index=[0]), is_automatic=True)
        timestamp_fgc = 1493246176040000000
        events_action_df = read_csv("resources/hwc/600A", "EVENTS.ACTION.ROD.A67B1")
        events_symbol_df = read_csv("resources/hwc/600A", "EVENTS.SYMBOL.ROD.A67B1")
        events_action_symbol_df = pd.concat([events_action_df, events_symbol_df], axis=1)

        # act
        analysis.analyze_i_meas_pc_trigger(timestamp_fgc, events_action_symbol_df)

    def test_calculate_current_miits(self):
        # arrange
        i_meas_df = read_csv("resources/hwc/600A", "I_MEAS_ROD.A67B1")
        i_ref_df = read_csv("resources/hwc/600A", "I_REF_ROD.A67B1")
        i_a_df = read_csv("resources/hwc/600A", "I_A_ROD.A67B1")

        # act
        analysis = R600ACircuitAnalysis("600A", pd.DataFrame(index=[0]), is_automatic=True)
        t_quench = analysis.estimate_quench_start_from_i_ref_i_a(i_ref_df, i_a_df)
        analysis.calculate_current_miits(i_meas_df, t_quench)

        # assert
        self.assertAlmostEqual(0.00228, analysis.results_table.loc[0, "I_MEAS MIITs"], places=4)

    def test_calculate_current_miits_i_meas_ia(self):
        # arrange
        i_meas_df = read_csv("resources/hwc/600A", "I_MEAS_ROD.A67B1")
        i_ref_df = read_csv("resources/hwc/600A", "I_REF_ROD.A67B1")
        i_a_df = read_csv("resources/hwc/600A", "I_A_ROD.A67B1")

        # act
        analysis = R600ACircuitAnalysis("600A", pd.DataFrame(index=[0]), is_automatic=True)
        t_quench = analysis.estimate_quench_start_from_i_ref_i_a(i_ref_df, i_a_df)
        analysis.calculate_current_miits_i_meas_i_a(i_meas_df, i_a_df, t_quench)

        # assert
        self.assertAlmostEqual(0.00265, analysis.results_table.loc[0, "I_MEAS MIITs"], places=4)

    def test_calculate_quench_current(self):
        # arrange
        i_meas_df = read_csv("resources/hwc/600A", "I_MEAS_ROD.A67B1")
        i_ref_df = read_csv("resources/hwc/600A", "I_REF_ROD.A67B1")
        i_a_df = read_csv("resources/hwc/600A", "I_A_ROD.A67B1")

        # act
        analysis = R600ACircuitAnalysis("600A", pd.DataFrame(index=[0]), is_automatic=True)
        t_quench = analysis.estimate_quench_start_from_i_ref_i_a(i_ref_df, i_a_df)
        analysis.calculate_quench_current(i_meas_df, t_quench)

        # assert
        self.assertAlmostEqual(186.2, analysis.results_table.loc[0, "Quench Current"], places=2)

    def test_calculate_current_slope(self):
        # arrange
        i_meas_df = read_csv("resources/hwc/600A", "I_MEAS_ROD.A67B1")

        # act
        analysis = R600ACircuitAnalysis("600A", pd.DataFrame(index=[0]), is_automatic=True)
        analysis.calculate_current_slope(i_meas_df)

        # assert
        self.assertAlmostEqual(-0.7, analysis.results_table.loc[0, "Ramp rate"], places=2)

    def test_calculate_current_slope_plateau(self):
        # timestamp_fgc_rco = 1521459746200000000
        # arrange
        i_meas_df = read_csv("resources/hwc/600A", "I_MEAS_RCO.A81B2")

        # act
        analysis = R600ACircuitAnalysis("600A", pd.DataFrame(index=[0]), is_automatic=True)
        analysis.calculate_current_slope(i_meas_df)

        # assert
        self.assertAlmostEqual(0.0, analysis.results_table.loc[0, "Ramp rate"], places=2)

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_plot_i_earth(self, mock_show=None):
        # arrange
        i_earth_df = read_csv("resources/hwc/600A", "IEARTH.IEARTH_ROD.A67B1")
        circuit_name = "ROD.A67B1"
        timestamp_fgc = 1493246176040000000

        # act
        analysis = R600ACircuitAnalysis("600A", pd.DataFrame(index=[0]), is_automatic=True)
        analysis.plot_i_earth_pc(circuit_name, timestamp_fgc, i_earth_df)

        # assert
        if mock_show is not None:
            mock_show.assert_called()

    def test_calculate_max_i_earth_pc(self):
        # arrange
        i_earth_df = read_csv("resources/hwc/600A", "IEARTH.IEARTH_ROD.A67B1")

        # act
        analysis = R600ACircuitAnalysis("600A", pd.DataFrame(index=[0]), is_automatic=True)
        analysis.calculate_max_i_earth_pc(i_earth_df)

        # assert
        self.assertAlmostEqual(13.824463, analysis.results_table.loc[0, "Earth Current"], places=3)

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_analyze_u_dump_res_ee(self, mock_show=None):
        # arrange
        i_meas_df = read_csv("resources/hwc/600A", "I_MEAS_ROD.A67B1")
        u_dump_res_df = read_csv("resources/hwc/600A", "U_DUMP_RES_ROD.A67B1")
        circuit_name = "ROD.A67B1"
        timestamp_fgc = 1493246176040000000

        # act
        analysis = R600ACircuitAnalysis("600A", pd.DataFrame(index=[0]), is_automatic=True)
        analysis.analyze_u_dump_res_ee(circuit_name, timestamp_fgc, i_meas_df, u_dump_res_df)

        # assert
        self.assertAlmostEqual(113.57, analysis.results_table.loc[0, "U_EE_max"], places=2)

        # assert
        if mock_show is not None:
            mock_show.assert_called()

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_plot_u_res(self, mock_show=None):
        # arrange
        u_res_nxcals_df = read_csv("resources/hwc/600A", "U_RES_NXCALS_ROD.A67B1")
        i_meas_df = read_csv("resources/hwc/600A", "I_MEAS_ROD.A67B1")
        circuit_name = "ROD.A67B1"
        timestamp_qds = 1493246175924000000

        # act
        analysis = R600ACircuitAnalysis("600A", pd.DataFrame(index=[0]), is_automatic=True)
        analysis.plot_u_res(circuit_name, timestamp_qds, u_res_nxcals_df, i_meas_df)

        # assert
        if mock_show is not None:
            mock_show.assert_called()

    def test_calculate_u_res_slope(self):
        # arrange
        u_res_df = read_csv("resources/hwc/600A", "U_RES_ROD.A67B1")

        # act
        analysis = R600ACircuitAnalysis("600A", pd.DataFrame(index=[0]), is_automatic=True)
        u_res_slope_act_df = analysis.calculate_u_res_slope(u_res_df)

        # assert
        u_res_slope_ref_df = pd.DataFrame({"U_RES_slope": {0.057999999999999996: 0.016139386, 0.096: 0.11316618}})

        pd.testing.assert_frame_equal(u_res_slope_ref_df, u_res_slope_act_df)
        self.assertAlmostEqual(2.553336684210526, analysis.results_table.loc[0, "dU_RES_dt"], places=2)

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_plot_u_res_slope(self, mock_show=None):
        # arrange
        u_res_df = read_csv("resources/hwc/600A", "U_RES_ROD.A67B1")
        circuit_name = "ROD.A67B1"
        timestamp_qds = 1493246175924000000

        # act
        analysis = R600ACircuitAnalysis("600A", pd.DataFrame(index=[0]), is_automatic=True)
        u_res_slope_df = analysis.calculate_u_res_slope(u_res_df)
        analysis.plot_u_res_slope(circuit_name, timestamp_qds, u_res_df, u_res_slope_df)

        # assert
        if mock_show is not None:
            mock_show.assert_called()

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_plot_qds(self, mock_show=None):
        # arrange
        u_res_df = read_csv("resources/hwc/600A", "U_RES_ROD.A67B1")
        u_diff_df = read_csv("resources/hwc/600A", "U_DIFF_ROD.A67B1")
        i_didt_df = read_csv("resources/hwc/600A", "I_DIDT_ROD.A67B1")
        i_dcct_df = read_csv("resources/hwc/600A", "I_DCCT_ROD.A67B1")
        circuit_name = "ROD.A67B1"
        timestamp_qds = 1493246175924000000

        # act
        analysis = R600ACircuitAnalysis("600A", pd.DataFrame(index=[0]), is_automatic=True)
        analysis.plot_qds(circuit_name, timestamp_qds, i_dcct_df, i_didt_df, u_diff_df, u_res_df)

        # assert
        if mock_show is not None:
            mock_show.assert_called()

    def test_create_mp3_results_table(self):
        # arrange
        circuit_type = "600A"
        is_automatic = True
        results_table = pd.DataFrame(
            {
                "Circuit Name": {0: "RQTL11.R6B1"},
                "Circuit Family": {0: "RQTL11"},
                "Period": {0: "HWC 2018-2"},
                "Date (FGC)": {0: "2018-12-06"},
                "Time (FGC)": {0: "11:50:46.900"},
                "FPA Reason": {0: "Magnet quench"},
                "Timestamp_PIC": {0: np.nan},
                "Delta_t(FGC-PIC)": {0: np.nan},
                "Delta_t(EE-PIC)": {0: np.nan},
                "Ramp rate": {0: 5.0},
                "Plateau duration": {0: 0},
                "I_Q_circ": {0: 316.9},
                "MIITS_circ": {0: 0.129},
                "I_Earth_max": {0: 15.075684},
                "EE analysis": {0: np.nan},
                "U_EE_max": {0: np.nan},
                "Delta_t(QPS-PIC)": {0: np.nan},
                "Type of Quench": {0: "Training"},
                "Quench count": {0: np.nan},
                "QDS trigger origin": {0: "QPS"},
                "dU_QPS/dt": {0: 64.1549985},
                "Comment": {0: "-"},
                "Analysis performed by": {0: "mmacieje"},
                "lhcsmapi version": {0: "1.4.35"},
                "timestamp_fgc": {0: 1544093446900000000},
                "timestamp_pic": {0: 1544093446888000000},
                "timestamp_qds_a": {0: 1544093446891000000},
                "timestamp_qds_b": {0: 0},
                "EE Analysis": {0: "No EE"},
                "RQTL11.R6B1_LEADS_U_HTS": {0: "U_HTS < quench threshold"},
                "RQTL11.R6B1_LEADS_U_RES": {0: "U_RES < quench threshold"},
            }
        )
        analysis_rcd = R600ACircuitAnalysis(circuit_type, results_table, is_automatic=is_automatic)

        # act
        mp3_act = analysis_rcd.create_mp3_results_table()

        # assert
        mp3_ref = pd.DataFrame(
            {
                "Circuit Name": {0: "RQTL11.R6B1"},
                "Circuit Family": {0: "RQTL11"},
                "Period": {0: "HWC 2018-2"},
                "Date (FGC)": {0: "2018-12-06"},
                "Time (FGC)": {0: "11:50:46.900"},
                "FPA Reason": {0: "Magnet quench"},
                "Timestamp_PIC": {0: "2018-12-06 11:50:46.888"},
                "Delta_t(FGC-PIC)": {0: 12.0},
                "Delta_t(EE-PIC)": {0: np.nan},
                "Ramp rate": {0: 5.0},
                "Plateau duration": {0: 0},
                "I_Q_circ": {0: 316.9},
                "MIITS_circ": {0: 0.129},
                "I_Earth_max": {0: 15.075684},
                "EE analysis": {0: np.nan},
                "U_EE_max": {0: np.nan},
                "Delta_t(QPS-PIC)": {0: 3.0},
                "Type of Quench": {0: "Training"},
                "Quench count": {0: np.nan},
                "QDS trigger origin": {0: "QPS"},
                "dU_QPS/dt": {0: 64.1549985},
                "Comment": {0: "-"},
                "Analysis performed by": {0: "mmacieje"},
                "lhcsmapi version": {0: "1.4.35"},
            }
        )

        mp3_act.fillna(np.nan, inplace=True)
        pd.testing.assert_frame_equal(mp3_ref, mp3_act)

    def test_create_mp3_results_table_ee(self):
        # arrange
        circuit_type = "600A"
        is_automatic = True
        results_table = pd.DataFrame(
            {
                "Circuit Name": {0: "RU.L4"},
                "Circuit Family": {0: "RU"},
                "Period": {0: "HWC 2018-2"},
                "Date (FGC)": {0: "2018-12-06"},
                "Time (FGC)": {0: "11:50:46.900"},
                "FPA Reason": {0: "Magnet quench"},
                "Timestamp_PIC": {0: np.nan},
                "Delta_t(FGC-PIC)": {0: np.nan},
                "Delta_t(EE-PIC)": {0: np.nan},
                "Ramp rate": {0: 5.0},
                "Plateau duration": {0: 0},
                "I_Q_circ": {0: 316.9},
                "MIITS_circ": {0: 0.129},
                "I_Earth_max": {0: 15.075684},
                "EE analysis": {0: "-"},
                "U_EE_max": {0: 50},
                "Delta_t(QPS-PIC)": {0: np.nan},
                "Type of Quench": {0: "Training"},
                "Quench count": {0: np.nan},
                "QDS trigger origin": {0: "QPS"},
                "dU_QPS/dt": {0: 64.1549985},
                "Comment": {0: "-"},
                "Analysis performed by": {0: "mmacieje"},
                "lhcsmapi version": {0: "1.4.35"},
                "timestamp_fgc": {0: 1544093446900000000},
                "timestamp_pic": {0: 1544093446888000000},
                "timestamp_ee": {0: 1544093446888000000},
                "timestamp_qds_a": {0: 1544093446891000000},
                "timestamp_qds_b": {0: 0},
                "EE Analysis": {0: "No EE"},
                "RQTL11.R6B1_LEADS_U_HTS": {0: "U_HTS < quench threshold"},
                "RQTL11.R6B1_LEADS_U_RES": {0: "U_RES < quench threshold"},
            }
        )
        analysis_rcd = R600ACircuitAnalysis(circuit_type, results_table, is_automatic=is_automatic)

        # act
        mp3_act = analysis_rcd.create_mp3_results_table()

        # assert
        mp3_ref = pd.DataFrame(
            {
                "Circuit Name": {0: "RU.L4"},
                "Circuit Family": {0: "RU"},
                "Period": {0: "HWC 2018-2"},
                "Date (FGC)": {0: "2018-12-06"},
                "Time (FGC)": {0: "11:50:46.900"},
                "FPA Reason": {0: "Magnet quench"},
                "Timestamp_PIC": {0: "2018-12-06 11:50:46.888"},
                "Delta_t(FGC-PIC)": {0: 12.0},
                "Delta_t(EE-PIC)": {0: 0.0},
                "Ramp rate": {0: 5.0},
                "Plateau duration": {0: 0},
                "I_Q_circ": {0: 316.9},
                "MIITS_circ": {0: 0.129},
                "I_Earth_max": {0: 15.075684},
                "EE analysis": {0: "-"},
                "U_EE_max": {0: 50},
                "Delta_t(QPS-PIC)": {0: 3.0},
                "Type of Quench": {0: "Training"},
                "Quench count": {0: np.nan},
                "QDS trigger origin": {0: "QPS"},
                "dU_QPS/dt": {0: 64.1549985},
                "Comment": {0: "-"},
                "Analysis performed by": {0: "mmacieje"},
                "lhcsmapi version": {0: "1.4.35"},
            }
        )

        mp3_act.fillna(np.nan, inplace=True)
        pd.testing.assert_frame_equal(mp3_ref, mp3_act)

    # RCD-RCO
    def test_create_timestamp_table_rcd(self):
        # arrange
        timestamp_dct = {
            "FGC_RCD": 1493246176040000000,
            "PIC_RCD": 1493246176022000000,
            "QDS_A_RCD": 1493246176020000000,
            "QDS_B_RCD": 1493246176021000000,
            "FGC_RCO": 1493246176040000000,
            "PIC_RCO": 1493246176022000000,
            "QDS_A_RCO": 1493246176020000000,
            "QDS_B_RCO": 1493246176021000000,
        }
        analysis = R600ACircuitAnalysis("600A", pd.DataFrame(), is_automatic=True)

        # act
        with warnings.catch_warnings(record=True) as w:
            # Cause all warnings to always be triggered.
            warnings.simplefilter("always")
            # Trigger a warning.
            analysis.create_timestamp_table(timestamp_dct, "RCD.A12B1")

        self.assertEqual(0, len(w))

    def test_create_timestamp_table_rcd_qds_a_missing(self):
        # arrange
        timestamp_dct = {
            "FGC_RCD": 1493246176040000000,
            "PIC_RCD": 1493246176022000000,
            "QDS_B_RCD": 1493246176021000000,
            "FGC_RCO": 1493246176040000000,
            "PIC_RCO": 1493246176022000000,
            "QDS_B_RCO": 1493246176021000000,
        }
        analysis = R600ACircuitAnalysis("600A", pd.DataFrame(), is_automatic=True)

        # act
        with warnings.catch_warnings(record=True) as w:
            # Cause all warnings to always be triggered.
            warnings.simplefilter("always")
            # Trigger a warning.
            analysis.create_timestamp_table(timestamp_dct, "RCD.A12B1")

        self.assertEqual("QDS Board A not present in PM buffer.", str(w[0].message))
        self.assertEqual(
            "Either QDS_A or FGC PM event is missing. " "Can not check synchronization of QDS and FGC timestamps!",
            str(w[1].message),
        )
        self.assertEqual("QDS Board A not present in PM buffer.", str(w[2].message))
        self.assertEqual(
            "Either QDS_A or FGC PM event is missing. " "Can not check synchronization of QDS and FGC timestamps!",
            str(w[3].message),
        )

    def test_create_timestamp_table_rcd_qds_a_b_missynchronised(self):
        # arrange
        timestamp_dct = {
            "FGC_RCD": 1493246176040000000,
            "PIC_RCD": 1493246176022000000,
            "QDS_A_RCD": 1493246176020000000,
            "QDS_B_RCD": 1493346176021000000,
            "FGC_RCO": 1493246176040000000,
            "PIC_RCO": 1493246176022000000,
            "QDS_A_RCO": 1493246176020000000,
            "QDS_B_RCO": 1493346176021000000,
        }
        analysis = R600ACircuitAnalysis("600A", pd.DataFrame(), is_automatic=True)

        # act
        with warnings.catch_warnings(record=True) as w:
            # Cause all warnings to always be triggered.
            warnings.simplefilter("always")
            # Trigger a warning.
            analysis.create_timestamp_table(timestamp_dct, "RCD.A12B1")

        self.assertEqual("QDS PM events (board A and B) are not synchronized to 1 ms!", str(w[0].message))
        self.assertEqual("QDS PM events (board A and B) are not synchronized to 1 ms!", str(w[1].message))

    def test_create_mp3_results_table_rcdo(self):
        # arrange
        circuit_type = "600A"
        is_automatic = True
        results_table = pd.DataFrame(
            {
                "Circuit Name": {0: "RCD-RCO.A81B2"},
                "Circuit Family": {0: "RCD"},
                "Period": {0: "HWC 2018-1"},
                "Date (FGC)": {0: "2018-03-19"},
                "Time (FGC)": {0: "12:42:26.200"},
                "FPA Reason": {0: "Magnet quench"},
                "Timestamp_PIC": {0: np.nan},
                "Delta_t(FGC-PIC)": {0: np.nan},
                "Delta_t(EE_RCD-PIC)": {0: np.nan},
                "Ramp rate RCO": {0: 0},
                "Ramp rate RCD": {0: -1.5},
                "Plateau duration RCO": {0: 272.46},
                "Plateau duration RCD": {0: 0},
                "I_Q_RCO": {0: 105.0},
                "I_Q_RCD": {0: -388.8},
                "MIITS_RCO": {0: 0.00033},
                "MIITS_RCD": {0: 0.00985},
                "I_Earth_max_RCO": {0: -6.396484},
                "I_Earth_max_RCD": {0: -11.932374},
                "EE_RCD analysis": {0: "-"},
                "U_EE_RCD_max": {0: -247.106},
                "Delta_t(QPS-PIC)": {0: np.nan},
                "Type of Quench": {0: "Training"},
                "Quench origin": {0: np.nan},
                "Quench count": {0: np.nan},
                "QDS trigger origin": {0: "QPS"},
                "dU_QPS/dt_RCO": {0: 78.75789899999992},
                "dU_QPS/dt_RCD": {0: -15.702699124999986},
                "Comment": {0: "-"},
                "Analysis performed by": {0: "mmacieje"},
                "lhcsmapi version": {0: "1.4.35"},
                "timestamp_fgc_rcd": {0: 1521459746260000000},
                "timestamp_fgc_rco": {0: 1521459746200000000},
                "timestamp_pic_rcd": {0: 1521459746248000000},
                "timestamp_pic_rco": {0: 1521459746193000000},
                "timestamp_ee_rcd": {0: 1521459746243000000},
                "timestamp_qds_a_rcd": {0: 1521459746250000000},
                "timestamp_qds_b_rcd": {0: 0},
                "timestamp_qds_a_rco": {0: 1521459746199000000},
                "timestamp_qds_b_rco": {0: 0},
                "RCD.A81B2_LEADS_U_HTS": {0: "U_HTS < quench threshold"},
                "RCD.A81B2_LEADS_U_RES": {0: "U_RES < quench threshold"},
                "RCO.A81B2_LEADS_U_HTS": {0: "U_HTS < quench threshold"},
                "RCO.A81B2_LEADS_U_RES": {0: "U_RES < quench threshold"},
            }
        )
        analysis_rcd = R600ACircuitAnalysis(circuit_type, results_table, is_automatic=is_automatic)

        # act
        mp3_act = analysis_rcd.create_mp3_results_table_rcdo()

        # assert
        mp3_ref = pd.DataFrame(
            {
                "Circuit Name": {0: "RCD-RCO.A81B2"},
                "Circuit Family": {0: "RCD"},
                "Period": {0: "HWC 2018-1"},
                "Date (FGC)": {0: "2018-03-19"},
                "Time (FGC)": {0: "12:42:26.200"},
                "FPA Reason": {0: "Magnet quench"},
                "Timestamp_PIC": {0: "2018-03-19 12:42:26.193"},
                "Delta_t(FGC-PIC)": {0: 7.0},
                "Delta_t(EE_RCD-PIC)": {0: 50.0},
                "Ramp rate RCO": {0: 0},
                "Ramp rate RCD": {0: -1.5},
                "Plateau duration RCO": {0: 272.46},
                "Plateau duration RCD": {0: 0},
                "I_Q_RCO": {0: 105.0},
                "I_Q_RCD": {0: -388.8},
                "MIITS_RCO": {0: 0.00033},
                "MIITS_RCD": {0: 0.00985},
                "I_Earth_max_RCO": {0: -6.396484},
                "I_Earth_max_RCD": {0: -11.932374},
                "EE_RCD analysis": {0: "-"},
                "U_EE_RCD_max": {0: -247.106},
                "Delta_t(QPS-PIC)": {0: 6.0},
                "Type of Quench": {0: "Training"},
                "Quench origin": {0: np.nan},
                "Quench count": {0: np.nan},
                "QDS trigger origin": {0: "QPS"},
                "dU_QPS/dt_RCO": {0: 78.75789899999992},
                "dU_QPS/dt_RCD": {0: -15.702699124999986},
                "Comment": {0: "-"},
                "Analysis performed by": {0: "mmacieje"},
                "lhcsmapi version": {0: "1.4.35"},
            }
        )

        mp3_act.fillna(np.nan, inplace=True)
        pd.testing.assert_frame_equal(mp3_ref, mp3_act)

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_plot_coupled_circuit_current_rcd_rco(self, mock_show=None):
        # arrange
        circuit_names = ["RCD.A81B2", "RCO.A81B2"]

        timestamp_rcd = 1521459746260000000
        i_meas_rcd_df = read_csv("resources/hwc/600A", "I_MEAS_RCD.A81B2")
        i_ref_rcd_df = read_csv("resources/hwc/600A", "I_REF_RCD.A81B2")

        timestamp_rco = 1521459746200000000
        i_meas_rco_df = read_csv("resources/hwc/600A", "I_MEAS_RCO.A81B2")
        i_ref_rco_df = read_csv("resources/hwc/600A", "I_REF_RCD.A81B2")

        analysis = R600ACircuitAnalysis("600A", pd.DataFrame(), is_automatic=True)

        # act
        analysis.plot_coupled_circuit_current(
            i_meas_rcd_df, i_ref_rcd_df, i_meas_rco_df, i_ref_rco_df, timestamp_rcd, timestamp_rco, circuit_names
        )

        # assert
        if mock_show is not None:
            mock_show.assert_called()

    def test_plot_coupled_circuit_current_missing_rcd(self):
        # arrange
        circuit_names = ["RCD.A81B2", "RCO.A81B2"]

        timestamp_rcd = float("nan")
        i_meas_rcd_df = pd.DataFrame(columns=["I_MEAS"])
        i_ref_rcd_df = pd.DataFrame(columns=["I_REF"])

        timestamp_rco = 1521459746200000000
        i_meas_rco_df = read_csv("resources/hwc/600A", "I_MEAS_RCO.A81B2")
        i_ref_rco_df = read_csv("resources/hwc/600A", "I_REF_RCD.A81B2")

        analysis = R600ACircuitAnalysis("600A", pd.DataFrame(), is_automatic=True)

        # act
        with warnings.catch_warnings(record=True) as w:
            # Cause all warnings to always be triggered.
            warnings.simplefilter("always")

            analysis.plot_coupled_circuit_current(
                i_meas_rcd_df, i_ref_rcd_df, i_meas_rco_df, i_ref_rco_df, timestamp_rcd, timestamp_rco, circuit_names
            )

            self.assertEqual("I_MEAS, I_REF are empty, plot of coupled circuit current skipped!", str(w[0].message))

    def test_plot_coupled_circuit_current_missing_rco(self):
        # arrange
        circuit_names = ["RCD.A81B2", "RCO.A81B2"]

        timestamp_rcd = 1521459746260000000
        i_meas_rcd_df = read_csv("resources/hwc/600A", "I_MEAS_RCD.A81B2")
        i_ref_rcd_df = read_csv("resources/hwc/600A", "I_REF_RCD.A81B2")

        timestamp_rco = float("nan")
        i_meas_rco_df = pd.DataFrame(columns=["I_MEAS"])
        i_ref_rco_df = pd.DataFrame(columns=["I_REF"])

        analysis = R600ACircuitAnalysis("600A", pd.DataFrame(), is_automatic=True)

        # act
        with warnings.catch_warnings(record=True) as w:
            # Cause all warnings to always be triggered.
            warnings.simplefilter("always")

            analysis.plot_coupled_circuit_current(
                i_meas_rcd_df, i_ref_rcd_df, i_meas_rco_df, i_ref_rco_df, timestamp_rcd, timestamp_rco, circuit_names
            )

            self.assertEqual("I_MEAS, I_REF are empty, plot of coupled circuit current skipped!", str(w[0].message))

    # RCBX
    def test_create_timestamp_table_rcbx(self):
        # arrange
        timestamp_dct = {
            "FGC_RCBXH": 1493246176040000000,
            "PIC_RCBXH": 1493246176022000000,
            "QDS_A_RCBXH": 1493246176020000000,
            "QDS_B_RCBXH": 1493246176021000000,
            "FGC_RCBXV": 1493246176040000000,
            "PIC_RCBXV": 1493246176022000000,
            "QDS_A_RCBXV": 1493246176020000000,
            "QDS_B_RCBXV": 1493246176021000000,
        }
        analysis = R600ACircuitAnalysis("600A", pd.DataFrame(), is_automatic=True)

        # act
        with warnings.catch_warnings(record=True) as w:
            # Cause all warnings to always be triggered.
            warnings.simplefilter("always")
            # Trigger a warning.
            analysis.create_timestamp_table(timestamp_dct, "RCBXH1.L1")

        self.assertEqual(0, len(w))

    def test_create_mp3_results_table_rcbxhv(self):
        # arrange
        circuit_type = "600A"
        is_automatic = True
        results_table = pd.DataFrame(
            {
                "Circuit Name": {0: "RCBX1.R1"},
                "Circuit Family": {0: "RCBX"},
                "Period": {0: "HWC 2017"},
                "Date (FGC)": {0: "2017-04-26"},
                "Time (FGC)": {0: "17:07:49.220"},
                "FPA Reason": {0: "RES current lead overvoltage"},
                "Timestamp_PIC": {0: np.nan},
                "Delta_t(FGC-PIC)": {0: np.nan},
                "Ramp rate H": {0: 2.2},
                "Ramp rate V": {0: -0.7},
                "Plateau duration H": {0: 0},
                "Plateau duration V": {0: 0},
                "I_Q_H": {0: 134.0},
                "I_Q_V": {0: 387.4},
                "I_radius": {0: np.nan},
                "phase": {0: 19.1},
                "MIITS_H": {0: 0.01828},
                "MIITS_V": {0: 0.05745},
                "I_Earth_max_H": {0: 6.134033},
                "I_Earth_max_V": {0: -4.608154},
                "Delta_t(QPS-PIC)": {0: np.nan},
                "Type of Quench": {0: "Unknown"},
                "Quench origin": {0: np.nan},
                "Quench count": {0: np.nan},
                "QDS trigger origin": {0: "RES current lead"},
                "dU_QPS/dt_H": {0: 87.70904875000001},
                "dU_QPS/dt_V": {0: np.nan},
                "Comment": {0: "-"},
                "Analysis performed by": {0: "mmacieje"},
                "lhcsmapi version": {0: "1.4.35"},
                "timestamp_fgc_rcbxh": {0: 1493219269240000000},
                "timestamp_fgc_rcbxv": {0: 1493219269220000000},
                "timestamp_pic_rcbxh": {0: 1493219269239000000},
                "timestamp_pic_rcbxv": {0: 1493219269212000000},
                "timestamp_qds_a_rcbxh": {0: 1493219269213000000},
                "timestamp_qds_b_rcbxh": {0: 0},
                "timestamp_qds_a_rcbxv": {0: 0},
                "timestamp_qds_b_rcbxv": {0: 0},
                "RCBXH1.R1_LEADS_U_HTS": {0: "U_HTS < quench threshold"},
                "RCBXH1.R1_LEADS_U_RES": {0: "U_RES < quench threshold"},
                "RCBXV1.R1_LEADS_U_HTS": {0: "U_HTS < quench threshold"},
                "RCBXV1.R1_LEADS_U_RES": {0: "U_RES > quench threshold"},
            }
        )
        analysis_rcbxh = R600ACircuitAnalysis(circuit_type, results_table, is_automatic=is_automatic)

        # act
        mp3_act = analysis_rcbxh.create_mp3_results_table_rcbxhv()
        analysis_rcbxh.compute_and_print_current_angle_for_combined_powering()

        # assert
        mp3_ref = pd.DataFrame(
            {
                "Circuit Name": {0: "RCBX1.R1"},
                "Circuit Family": {0: "RCBX"},
                "Period": {0: "HWC 2017"},
                "Date (FGC)": {0: "2017-04-26"},
                "Time (FGC)": {0: "17:07:49.220"},
                "FPA Reason": {0: "RES current lead overvoltage"},
                "Timestamp_PIC": {0: "2017-04-26 17:07:49.212"},
                "Delta_t(FGC-PIC)": {0: 8.0},
                "Ramp rate H": {0: 2.2},
                "Ramp rate V": {0: -0.7},
                "Plateau duration H": {0: 0},
                "Plateau duration V": {0: 0},
                "I_Q_H": {0: 134.0},
                "I_Q_V": {0: 387.4},
                "I_radius": {0: 409.92043130344206},
                "phase": {0: 19.1},
                "MIITS_H": {0: 0.01828},
                "MIITS_V": {0: 0.05745},
                "I_Earth_max_H": {0: 6.134033},
                "I_Earth_max_V": {0: -4.608154},
                "Delta_t(QPS-PIC)": {0: 1.0},
                "Type of Quench": {0: "Unknown"},
                "Quench origin": {0: np.nan},
                "Quench count": {0: np.nan},
                "QDS trigger origin": {0: "RES current lead"},
                "dU_QPS/dt_H": {0: 87.70904875000001},
                "dU_QPS/dt_V": {0: np.nan},
                "Comment": {0: "-"},
                "Analysis performed by": {0: "mmacieje"},
                "lhcsmapi version": {0: "1.4.35"},
            }
        )

        mp3_act.fillna(np.nan, inplace=True)
        pd.testing.assert_frame_equal(mp3_ref, mp3_act)

    def test_find_non_zero_min(self):
        # arrange
        timestamp_a = 2
        timestamp_b = 1

        # act
        non_zero_min_act = find_non_zero_min(timestamp_a, timestamp_b)

        # assert
        non_zero_min_ref = 1
        self.assertEqual(non_zero_min_ref, non_zero_min_act)

    def test_find_non_zero_min_a_0(self):
        # arrange
        timestamp_a = 0
        timestamp_b = 1

        # act
        non_zero_min_act = find_non_zero_min(timestamp_a, timestamp_b)

        # assert
        non_zero_min_ref = 1
        self.assertEqual(non_zero_min_ref, non_zero_min_act)

    def test_find_non_zero_min_b_0(self):
        # arrange
        timestamp_a = 1
        timestamp_b = 0

        # act
        non_zero_min_act = find_non_zero_min(timestamp_a, timestamp_b)

        # assert
        non_zero_min_ref = 1
        self.assertEqual(non_zero_min_ref, non_zero_min_act)

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_plot_coupled_circuit_current_rcbxh_rcbxv(self, mock_show=None):
        # arrange
        circuit_names = ["RCBXH1.R1", "RCBXV1.R1"]

        timestamp_rcbxh = 1521459746260000000
        i_meas_rcbxh_df = read_csv("resources/hwc/600A", "I_MEAS_RCBXH1.R1")
        i_ref_rcbxh_df = read_csv("resources/hwc/600A", "I_REF_RCBXH1.R1")

        timestamp_rcbxv = 1521459746200000000
        i_meas_rcbxv_df = read_csv("resources/hwc/600A", "I_MEAS_RCBXV1.R1")
        i_ref_rcbxv_df = read_csv("resources/hwc/600A", "I_REF_RCBXV1.R1")

        analysis = R600ACircuitAnalysis("600A", pd.DataFrame(), is_automatic=True)

        # act
        analysis.plot_coupled_circuit_current(
            i_meas_rcbxh_df,
            i_ref_rcbxh_df,
            i_meas_rcbxv_df,
            i_ref_rcbxv_df,
            timestamp_rcbxh,
            timestamp_rcbxv,
            circuit_names,
        )

        # assert
        if mock_show is not None:
            mock_show.assert_called()

    def test_plot_coupled_circuit_current_missing_rcbxh(self):
        # arrange
        circuit_names = ["RCBXH1.R1", "RCBXV1.R1"]

        timestamp_rcbxh = float("nan")
        i_meas_rcbxh_df = pd.DataFrame(columns=["I_MEAS"])
        i_ref_rcbxh_df = pd.DataFrame(columns=["I_REF"])

        timestamp_rcbxv = 1521459746200000000
        i_meas_rcbxv_df = read_csv("resources/hwc/600A", "I_MEAS_RCBXV1.R1")
        i_ref_rcbxv_df = read_csv("resources/hwc/600A", "I_REF_RCBXV1.R1")

        analysis = R600ACircuitAnalysis("600A", pd.DataFrame(), is_automatic=True)

        # act
        with warnings.catch_warnings(record=True) as w:
            # Cause all warnings to always be triggered.
            warnings.simplefilter("always")

            analysis.plot_coupled_circuit_current(
                i_meas_rcbxh_df,
                i_ref_rcbxh_df,
                i_meas_rcbxv_df,
                i_ref_rcbxv_df,
                timestamp_rcbxh,
                timestamp_rcbxv,
                circuit_names,
            )

            self.assertEqual("I_MEAS, I_REF are empty, plot of coupled circuit current skipped!", str(w[0].message))

    def test_plot_coupled_circuit_current_missing_rcbxv(self):
        # arrange
        circuit_names = ["RCBXH1.R1", "RCBXV1.R1"]

        timestamp_rcbxh = 1521459746260000000
        i_meas_rcbxh_df = read_csv("resources/hwc/600A", "I_MEAS_RCBXH1.R1")
        i_ref_rcbxh_df = read_csv("resources/hwc/600A", "I_REF_RCBXH1.R1")

        timestamp_rcbxv = float("nan")
        i_meas_rcbxv_df = pd.DataFrame(columns=["I_MEAS"])
        i_ref_rcbxv_df = pd.DataFrame(columns=["I_REF"])

        analysis = R600ACircuitAnalysis("600A", pd.DataFrame(), is_automatic=True)

        # act
        with warnings.catch_warnings(record=True) as w:
            # Cause all warnings to always be triggered.
            warnings.simplefilter("always")

            analysis.plot_coupled_circuit_current(
                i_meas_rcbxh_df,
                i_ref_rcbxh_df,
                i_meas_rcbxv_df,
                i_ref_rcbxv_df,
                timestamp_rcbxh,
                timestamp_rcbxv,
                circuit_names,
            )

            self.assertEqual("I_MEAS, I_REF are empty, plot of coupled circuit current skipped!", str(w[0].message))

    def test_compute_and_print_current_angle_for_combined_powering_pos_h_neg_v(self):
        # arrange
        results_table = pd.DataFrame({"I_Q_H": {0: 287.4}, "I_Q_V": {0: -292.1}})
        analysis = R600ACircuitAnalysis("600A", results_table, is_automatic=True)

        # act
        expected_angle_value = 135.0
        with patch("sys.stdout", new=StringIO()) as fake_out:
            analysis.compute_and_print_current_angle_for_combined_powering()
            self.assertEqual(
                f"The phase of currents in the combined powering is {expected_angle_value} deg.",
                fake_out.getvalue().strip(),
            )

        # assert
        self.assertEqual(expected_angle_value, analysis.results_table.loc[0, "phase"])

    def test_compute_and_print_current_angle_for_combined_powering_pos_h_pos_v(self):
        # arrange
        results_table = pd.DataFrame({"I_Q_H": {0: 305.7}, "I_Q_V": {0: 273.1}})
        analysis = R600ACircuitAnalysis("600A", results_table, is_automatic=True)

        # act
        expected_angle_value = 48.0
        with patch("sys.stdout", new=StringIO()) as fake_out:
            analysis.compute_and_print_current_angle_for_combined_powering()
            self.assertEqual(
                f"The phase of currents in the combined powering is {expected_angle_value} deg.",
                fake_out.getvalue().strip(),
            )

        # assert
        self.assertEqual(expected_angle_value, analysis.results_table.loc[0, "phase"])

    def test_compute_and_print_current_angle_for_combined_powering_neg_h_pos_v(self):
        # arrange
        results_table = pd.DataFrame({"I_Q_H": {0: -335}, "I_Q_V": {0: 242}})
        analysis = R600ACircuitAnalysis("600A", results_table, is_automatic=True)

        # act
        expected_angle_value = 306.0
        with patch("sys.stdout", new=StringIO()) as fake_out:
            analysis.compute_and_print_current_angle_for_combined_powering()
            self.assertEqual(
                f"The phase of currents in the combined powering is {expected_angle_value} deg.",
                fake_out.getvalue().strip(),
            )

        # assert
        self.assertEqual(expected_angle_value, analysis.results_table.loc[0, "phase"])

    def test_compute_and_print_current_angle_for_combined_powering_neg_h_neg_v(self):
        # arrange
        results_table = pd.DataFrame({"I_Q_H": {0: -60}, "I_Q_V": {0: -405}})
        analysis = R600ACircuitAnalysis("600A", results_table, is_automatic=True)

        # act
        expected_angle_value = 188.0
        with patch("sys.stdout", new=StringIO()) as fake_out:
            analysis.compute_and_print_current_angle_for_combined_powering()
            self.assertEqual(
                f"The phase of currents in the combined powering is {expected_angle_value} deg.",
                fake_out.getvalue().strip(),
            )

        # assert
        self.assertEqual(expected_angle_value, analysis.results_table.loc[0, "phase"])

    def test_compute_and_print_current_angle_for_combined_powering_missing_horizontal(self):
        # arrange
        results_table = pd.DataFrame({"I_Q_H": {0: float("nan")}, "I_Q_V": {0: 273.1}})
        analysis = R600ACircuitAnalysis("600A", results_table, is_automatic=True)

        # act
        # assert
        with warnings.catch_warnings(record=True) as w:
            analysis.compute_and_print_current_angle_for_combined_powering()
            self.assertEqual("Horizontal circuit not powered, analysis skipped", str(w[0].message))

    def test_compute_and_print_current_angle_for_combined_powering_missing_vertical(self):
        # arrange
        results_table = pd.DataFrame({"I_Q_H": {0: 305.7}, "I_Q_V": {0: float("nan")}})
        analysis = R600ACircuitAnalysis("600A", results_table, is_automatic=True)

        # act
        # assert
        with warnings.catch_warnings(record=True) as w:
            analysis.compute_and_print_current_angle_for_combined_powering()
            self.assertEqual("Vertical circuit not powered, analysis skipped", str(w[0].message))
