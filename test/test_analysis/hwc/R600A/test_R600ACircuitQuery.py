import unittest
import pandas as pd
import numpy as np
from unittest.mock import patch

from lhcsmapi.analysis.R600ACircuitQuery import R600ACircuitQuery


class Test600ACircuitQuery(unittest.TestCase):
    def test_create_report_analysis_template(self):
        # arrange
        query = R600ACircuitQuery("600A", "RU.L4")

        # act
        results_act = query.create_report_analysis_template(1493219269220000000, "")

        results_act["lhcsmapi version"] = "1.3.226"

        # assert
        results_ref = pd.DataFrame(
            {
                "Circuit Name": {0: "RU.L4"},
                "Circuit Family": {0: "RU"},
                "Period": {0: "HWC 2017"},
                "Date (FGC)": {0: "2017-04-26"},
                "Time (FGC)": {0: "17:07:49.220"},
                "FPA Reason": {0: np.nan},
                "Timestamp_PIC": {0: np.nan},
                "Delta_t(FGC-PIC)": {0: np.nan},
                "Delta_t(EE-PIC)": {0: np.nan},
                "Ramp rate": {0: np.nan},
                "Plateau duration": {0: np.nan},
                "I_Q_circ": {0: np.nan},
                "MIITS_circ": {0: np.nan},
                "I_Earth_max": {0: np.nan},
                "EE analysis": {0: np.nan},
                "U_EE_max": {0: np.nan},
                "Delta_t(QPS-PIC)": {0: np.nan},
                "Type of Quench": {0: np.nan},
                "Quench count": {0: np.nan},
                "QDS trigger origin": {0: np.nan},
                "dU_QPS/dt": {0: np.nan},
                "Comment": {0: np.nan},
                "Analysis performed by": {0: ""},
                "lhcsmapi version": {0: "1.3.226"},
            }
        )

        results_act.fillna(np.nan, inplace=True)
        pd.testing.assert_frame_equal(results_ref, results_act)

    def test_create_report_analysis_template_rcd(self):
        # arrange
        timestamp_fgc_rcd = 1493139303220000000
        timestamp_fgc_rco = 1493139303160000000
        circuit_names = ["RCD.A12B2", "RCO.A12B2"]
        query = R600ACircuitQuery("600A", circuit_names[0])

        # act
        results_act = query.create_report_analysis_template_rcd(timestamp_fgc_rcd, timestamp_fgc_rco, circuit_names)
        results_act["lhcsmapi version"] = "1.3.226"

        # assert
        results_ref = pd.DataFrame(
            {
                "Circuit Name": {0: "RCD-RCO.A12B2"},
                "Circuit Family": {0: "RCD"},
                "Period": {0: "HWC 2017"},
                "Date (FGC)": {0: "2017-04-25"},
                "Time (FGC)": {0: "18:55:03.160"},
                "FPA Reason": {0: np.nan},
                "Timestamp_PIC": {0: np.nan},
                "Delta_t(FGC-PIC)": {0: np.nan},
                "Delta_t(EE_RCD-PIC)": {0: np.nan},
                "Ramp rate RCO": {0: np.nan},
                "Ramp rate RCD": {0: np.nan},
                "Plateau duration RCO": {0: np.nan},
                "Plateau duration RCD": {0: np.nan},
                "I_Q_RCO": {0: np.nan},
                "I_Q_RCD": {0: np.nan},
                "MIITS_RCO": {0: np.nan},
                "MIITS_RCD": {0: np.nan},
                "I_Earth_max_RCO": {0: np.nan},
                "I_Earth_max_RCD": {0: np.nan},
                "EE_RCD analysis": {0: np.nan},
                "U_EE_RCD_max": {0: np.nan},
                "Delta_t(QPS-PIC)": {0: np.nan},
                "Type of Quench": {0: np.nan},
                "Quench origin": {0: np.nan},
                "Quench count": {0: np.nan},
                "QDS trigger origin": {0: np.nan},
                "dU_QPS/dt_RCO": {0: np.nan},
                "dU_QPS/dt_RCD": {0: np.nan},
                "Comment": {0: np.nan},
                "Analysis performed by": {0: ""},
                "lhcsmapi version": {0: "1.3.226"},
            }
        )

        results_act.fillna(np.nan, inplace=True)
        pd.testing.assert_frame_equal(results_ref, results_act)

    def test_create_report_analysis_template_rcbx(self):
        # arrange
        timestamp_fgc_rcd = 1493219269240000000
        timestamp_fgc_rco = 1493219269220000000
        circuit_names = ["RCBXH1.R1", "RCBXV1.R1"]
        query = R600ACircuitQuery("600A", circuit_names[0])

        # act
        results_act = query.create_report_analysis_template_rcbx(timestamp_fgc_rcd, timestamp_fgc_rco, circuit_names)
        results_act["lhcsmapi version"] = "1.3.226"

        # assert
        results_ref = pd.DataFrame(
            {
                "Circuit Name": {0: "RCBX1.R1"},
                "Circuit Family": {0: "RCBX"},
                "Period": {0: "HWC 2017"},
                "Date (FGC)": {0: "2017-04-26"},
                "Time (FGC)": {0: "17:07:49.220"},
                "FPA Reason": {0: np.nan},
                "Timestamp_PIC": {0: np.nan},
                "Delta_t(FGC-PIC)": {0: np.nan},
                "Ramp rate H": {0: np.nan},
                "Ramp rate V": {0: np.nan},
                "Plateau duration H": {0: np.nan},
                "Plateau duration V": {0: np.nan},
                "I_Q_H": {0: np.nan},
                "I_Q_V": {0: np.nan},
                "I_radius": {0: np.nan},
                "phase": {0: np.nan},
                "MIITS_H": {0: np.nan},
                "MIITS_V": {0: np.nan},
                "I_Earth_max_H": {0: np.nan},
                "I_Earth_max_V": {0: np.nan},
                "Delta_t(QPS-PIC)": {0: np.nan},
                "Type of Quench": {0: np.nan},
                "Quench origin": {0: np.nan},
                "Quench count": {0: np.nan},
                "QDS trigger origin": {0: np.nan},
                "dU_QPS/dt_H": {0: np.nan},
                "dU_QPS/dt_V": {0: np.nan},
                "Comment": {0: np.nan},
                "Analysis performed by": {0: ""},
                "lhcsmapi version": {0: "1.3.226"},
            }
        )

        results_act.fillna(np.nan, inplace=True)
        pd.testing.assert_frame_equal(results_ref, results_act)
