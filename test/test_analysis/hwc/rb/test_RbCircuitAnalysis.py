import re
import unittest
import warnings
from io import StringIO
from unittest.mock import patch

import matplotlib as mpl
import numpy as np
import pandas as pd
import pytest

from lhcsmapi.analysis.qds import QdsAnalysis
from lhcsmapi.analysis.RbCircuitAnalysis import RbCircuitAnalysis
from lhcsmapi.analysis.qds.QdsAnalysis import analyze_qds_trigger, plot_u_diode_nxcals
from lhcsmapi.analysis.qh.QuenchHeaterVoltageCurrentAnalysis import QuenchHeaterVoltageCurrentAnalysis
from lhcsmapi.pyedsl.AssertionBuilder import AssertionBuilder
from test.resources.read_csv import read_csv

mpl.rc("figure", max_open_warning=0)


def _get_u_qds_dfs_for_0_A31R1():
    return [
        read_csv("resources/hwc/rb/fpa", "0_A31R1.U_QS0"),
        read_csv("resources/hwc/rb/fpa", "0_A31R1.U_1"),
        read_csv("resources/hwc/rb/fpa", "0_A31R1.U_2"),
        read_csv("resources/hwc/rb/fpa", "0_A31R1.ST_NQD0"),
        read_csv("resources/hwc/rb/fpa", "0_A31R1.ST_MAGNET_OK"),
    ]


class RbCircuitAnalysisTest(unittest.TestCase):
    def setUp(self):
        self.circuit_type = "RB"
        self.circuit_name = "RB.A12"
        self.timestamp_fgc = 1544631694840000000
        self.timestamp_fgc_ref = 1521298828680000000
        self.timestamp_pic = 1544631694800000000
        self.timestamp_ee_odd = 1544631694896000000
        self.timestamp_ee_even = 1544631695395000000
        results_table = read_csv("resources/hwc/rb/fpa", "results_table")
        results_table = results_table[results_table.index == 0]
        self.rb_analysis = RbCircuitAnalysis(self.circuit_type, results_table, True)

    def test_create_ref_timestamp_table(self):
        # arrange
        timestamp_ref_dct = {
            "FGC": 1521298828680000000,
            "EE_ODD_first": 1521298828749000000,
            "EE_ODD_second": 1521299279004000000,
            "EE_EVEN_first": 1521298829246000000,
            "EE_EVEN_second": 1521299250804000000,
        }
        # act
        df = self.rb_analysis.create_ref_timestamp_table(timestamp_ref_dct)

        # asssert
        df_ref = pd.DataFrame(
            {
                "Source": {0: "FGC", 1: "EE_ODD_first", 2: "EE_ODD_second", 3: "EE_EVEN_first", 4: "EE_EVEN_second"},
                "Timestamp": {
                    0: 1521298828680000000,
                    1: 1521298828749000000,
                    2: 1521299279004000000,
                    3: 1521298829246000000,
                    4: 1521299250804000000,
                },
                "Date and time": {
                    0: "2018-03-17 16:00:28.680",
                    1: "2018-03-17 16:00:28.749",
                    2: "2018-03-17 16:07:59.004",
                    3: "2018-03-17 16:00:29.246",
                    4: "2018-03-17 16:07:30.804",
                },
            }
        )
        pd.testing.assert_frame_equal(df_ref, df)

    def test_create_timestamp_table(self):
        # arrange
        timestamp_dct = {
            "FGC": 1544631694840000000,
            "PIC": 1544631694800000000,
            "EE_EVEN": 1544631695395000000,
            "EE_ODD": 1544631694896000000,
            "iQPS": 1544631694792000000,
            "nQPS": 1544631694812000000,
            "LEADS_ODD": 1544631695407000000,
            "LEADS_EVEN": 1544631696004000000,
        }

        # act
        # assert
        with warnings.catch_warnings(record=True) as w:
            self.rb_analysis.create_timestamp_table(timestamp_dct)
            self.assertEqual("iQPS and FGC PM events are not synchronized to +/- 40 ms!", str(w[0].message))

    def test_create_timestamp_table_nqps_before_iqps(self):
        # arrange
        timestamp_dct = {
            "FGC": 1544631694840000000,
            "PIC": 1544631694800000000,
            "EE_EVEN": 1544631695395000000,
            "EE_ODD": 1544631694896000000,
            "iQPS": 1544631694812000000,
            "nQPS": 1544631694792000000,
            "LEADS_ODD": 1544631695407000000,
            "LEADS_EVEN": 1544631696004000000,
        }

        # act
        # assert
        with warnings.catch_warnings(record=True) as w:
            self.rb_analysis.create_timestamp_table(timestamp_dct)
            self.assertEqual(
                "nQPS PM timestamp (1544631694792000000) was created before iQPS one (1544631694812000000)",
                str(w[0].message),
            )

    def test_analyze_pic_equal(self):
        # arrange
        timestamp_pic = [1544631694800000000, 1544631694800000000]

        # act
        # assert
        with pytest.warns(
            UserWarning,
            match=re.escape(
                "PIC timestamps (2018-12-12 17:21:34.800) and (2018-12-12 17:21:34.800) differ by less than 1 ms (0 ns)"
            ),
        ):
            self.rb_analysis.analyze_pic(timestamp_pic)

    def test_analyze_pic_non_equal(self):
        # arrange
        timestamp_pic = [1544631694800000000, 1544631694807000000]

        # act
        # assert
        warning_ref = (
            "PIC timestamps (2018-12-12 17:21:34.800) and (2018-12-12 17:21:34.807) differ by more than "
            "5 ms (7000000 ns)"
        )

        with warnings.catch_warnings(record=True) as w:
            self.rb_analysis.analyze_pic(timestamp_pic)
            self.assertEqual(warning_ref, str(w[0].message))

    def test_analyze_pic_too_close(self):
        # arrange
        timestamp_pic = [1544631694800000000, 1544631694800100000]

        # act
        # assert
        warning_ref = (
            "PIC timestamps (2018-12-12 17:21:34.800) and (2018-12-12 17:21:34.8001) differ by less than "
            "1 ms (100000 ns)"
        )

        with warnings.catch_warnings(record=True) as w:
            self.rb_analysis.analyze_pic(timestamp_pic)
            self.assertEqual(warning_ref, str(w[0].message))

    def test_analyze_pic_middle(self):
        # arrange
        timestamp_pic = [1544631694800000000, 1544631694803000000]

        # act
        # assert
        output_ref = (
            "EVEN and ODD PIC timestamps (2018-12-12 17:21:34.800) and (2018-12-12 17:21:34.803) are "
            "within 1-5 ms away.\n"
        )

        with patch("sys.stdout", new=StringIO()) as stdout:
            self.rb_analysis.analyze_pic(timestamp_pic)
            self.assertEqual(output_ref, stdout.getvalue())

    def test_analyze_pic_nan(self):
        # arrange
        timestamp_pic = [1544631694800000000, np.nan]

        # act
        # assert
        warning_ref = "At least one timestamp is NaN, check of timestamp PIC difference is skipped."

        with warnings.catch_warnings(record=True) as w:
            self.rb_analysis.analyze_pic(timestamp_pic)
            self.assertEqual(warning_ref, str(w[0].message))

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_analyze_i_meas_pc(self, mock_show=None):
        # arrange
        i_meas_df = read_csv("resources/hwc/rb/fpa", "i_meas_df")
        i_meas_ref_df = read_csv("resources/hwc/rb/fpa", "i_meas_ref_df")

        # act
        self.rb_analysis.analyze_i_meas_pc(
            self.circuit_name, self.timestamp_fgc, self.timestamp_fgc_ref, self.timestamp_pic, i_meas_df, i_meas_ref_df
        )

        # assert
        if mock_show is not None:
            mock_show.assert_called()

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_analyze_i_earth_pc(self, mock_show=None):
        # arrange
        i_a_df = read_csv("resources/hwc/rb/fpa", "i_a_df")
        i_earth_df = read_csv("resources/hwc/rb/fpa", "i_earth_df")
        i_earth_ref_df = read_csv("resources/hwc/rb/fpa", "i_earth_ref_df")

        # act
        self.rb_analysis.analyze_i_earth_pc(self.circuit_name, self.timestamp_fgc, i_a_df, i_earth_df, i_earth_ref_df)

        # assert
        if mock_show is not None:
            mock_show.assert_called()

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_analyze_i_earth_pcnt_pc(self, mock_show=None):
        # arrange
        i_meas_df = read_csv("resources/hwc/rb/fpa", "i_meas_df")
        i_meas_ref_df = read_csv("resources/hwc/rb/fpa", "i_meas_ref_df")
        i_earth_pcnt_df = read_csv("resources/hwc/rb/fpa", "i_earth_pcnt_df")
        i_earth_pcnt_ref_df = read_csv("resources/hwc/rb/fpa", "i_earth_pcnt_ref_df")

        # act
        self.rb_analysis.analyze_i_earth_pcnt_pc(
            self.circuit_type,
            self.circuit_name,
            self.timestamp_fgc,
            i_meas_df,
            i_meas_ref_df,
            i_earth_pcnt_df,
            i_earth_pcnt_ref_df,
        )

        # assert
        if mock_show is not None:
            mock_show.assert_called()

    def test_analyze_i_earth_pcnt_pc_assertion_builder(self):
        # arrange
        i_meas_df = read_csv("resources/hwc/rb/fpa", "i_meas_df")
        i_meas_ref_df = read_csv("resources/hwc/rb/fpa", "i_meas_ref_df")
        i_earth_pcnt_df = read_csv("resources/hwc/rb/fpa", "i_earth_pcnt_df")
        i_earth_pcnt_ref_df = read_csv("resources/hwc/rb/fpa", "i_earth_pcnt_ref_df")
        scaling = (i_meas_df.max() / i_meas_ref_df.max()).values[0]

        # act
        is_i_earth_pcnt_ok = (
            AssertionBuilder()
            .with_signal(i_earth_pcnt_df[i_earth_pcnt_ref_df.index > 3])
            .is_between_two_references(
                first_ref=i_earth_pcnt_ref_df[i_earth_pcnt_ref_df.index > 3],
                first_scaling=scaling,
                first_offset=3,
                second_ref=i_earth_pcnt_ref_df[i_earth_pcnt_ref_df.index > 3],
                second_scaling=scaling,
                second_offset=3,
                is_plot=True,
            )
            .get_assertion_result()
        )

        # assert
        self.assertEqual(True, is_i_earth_pcnt_ok)

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_analyze_char_time_u_dump_res_ee_odd(self, mock_show=None):
        # arrange
        u_dump_res_odd_df = read_csv("resources/hwc/rb/fpa", "u_dump_res_odd_df")
        i_meas_df = read_csv("resources/hwc/rb/fpa", "i_meas_df")

        # act
        self.rb_analysis.analyze_char_time_u_dump_res_ee(
            self.circuit_name, self.timestamp_fgc, u_dump_res_odd_df, i_meas_df
        )

        # assert
        if mock_show is not None:
            mock_show.assert_called()

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_analyze_char_time_u_dump_res_ee_odd_even(self, mock_show=None):
        # arrange
        u_dump_res_odd_df = read_csv("resources/hwc/rb/fpa", "u_dump_res_odd_df")
        u_dump_res_even_df = read_csv("resources/hwc/rb/fpa", "u_dump_res_even_df")
        i_meas_df = read_csv("resources/hwc/rb/fpa", "i_meas_df")

        # act
        self.rb_analysis.analyze_char_time_u_dump_res_ee(
            self.circuit_name, self.timestamp_fgc, [u_dump_res_odd_df, u_dump_res_even_df], i_meas_df
        )

        # assert
        if mock_show is not None:
            mock_show.assert_called()

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_analyze_delay_time_u_dump_res_ee(self, mock_show=None):
        # arrange
        i_a_df = read_csv("resources/hwc/rb/fpa", "i_a_df")
        i_ref_df = read_csv("resources/hwc/rb/fpa", "i_ref_df")
        u_dump_res_odd_df = read_csv("resources/hwc/rb/fpa", "u_dump_res_odd_df")
        u_dump_res_even_df = read_csv("resources/hwc/rb/fpa", "u_dump_res_even_df")

        # act
        self.rb_analysis.analyze_delay_time_u_dump_res_ee(
            self.circuit_name,
            self.timestamp_fgc,
            self.timestamp_pic,
            [self.timestamp_ee_odd, self.timestamp_ee_even],
            i_a_df,
            i_ref_df,
            [u_dump_res_odd_df, u_dump_res_even_df],
        )

        # assert
        if mock_show is not None:
            mock_show.assert_called()

    def test_temperature_ee_odd(self):
        # arrange
        t_res_body_odd_dfs = [
            read_csv("resources/hwc/rb/fpa", "t_res_body_1_odd_0_df"),
            read_csv("resources/hwc/rb/fpa", "t_res_body_2_odd_0_df"),
            read_csv("resources/hwc/rb/fpa", "t_res_body_3_odd_0_df"),
            read_csv("resources/hwc/rb/fpa", "t_res_body_1_odd_1_df"),
            read_csv("resources/hwc/rb/fpa", "t_res_body_2_odd_1_df"),
            read_csv("resources/hwc/rb/fpa", "t_res_body_3_odd_1_df"),
        ]

        t_res_body_odd_ref_dfs = [
            read_csv("resources/hwc/rb/fpa", "t_res_body_1_odd_0_ref_df"),
            read_csv("resources/hwc/rb/fpa", "t_res_body_2_odd_0_ref_df"),
            read_csv("resources/hwc/rb/fpa", "t_res_body_3_odd_0_ref_df"),
            read_csv("resources/hwc/rb/fpa", "t_res_body_1_odd_1_ref_df"),
            read_csv("resources/hwc/rb/fpa", "t_res_body_2_odd_1_ref_df"),
            read_csv("resources/hwc/rb/fpa", "t_res_body_3_odd_1_ref_df"),
        ]
        # act
        AssertionBuilder().with_signal(t_res_body_odd_dfs).compare_to_reference(
            signal_ref_dfs=t_res_body_odd_ref_dfs, abs_margin=25, scaling=1
        )

    def test_temperature_ee_even(self):
        # arrange
        t_res_body_even_dfs = [
            read_csv("resources/hwc/rb/fpa", "t_res_body_1_even_0_df"),
            read_csv("resources/hwc/rb/fpa", "t_res_body_2_even_0_df"),
            read_csv("resources/hwc/rb/fpa", "t_res_body_3_even_0_df"),
            read_csv("resources/hwc/rb/fpa", "t_res_body_1_even_1_df"),
            read_csv("resources/hwc/rb/fpa", "t_res_body_2_even_1_df"),
            read_csv("resources/hwc/rb/fpa", "t_res_body_3_even_1_df"),
        ]

        t_res_body_even_ref_dfs = [
            read_csv("resources/hwc/rb/fpa", "t_res_body_1_even_0_ref_df"),
            read_csv("resources/hwc/rb/fpa", "t_res_body_2_even_0_ref_df"),
            read_csv("resources/hwc/rb/fpa", "t_res_body_3_even_0_ref_df"),
            read_csv("resources/hwc/rb/fpa", "t_res_body_1_even_1_ref_df"),
            read_csv("resources/hwc/rb/fpa", "t_res_body_2_even_1_ref_df"),
            read_csv("resources/hwc/rb/fpa", "t_res_body_3_even_1_ref_df"),
        ]
        # act
        AssertionBuilder().with_signal(t_res_body_even_dfs).compare_to_reference(
            signal_ref_dfs=t_res_body_even_ref_dfs, abs_margin=25, scaling=1
        )

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_analyze_qh(self, mock_show=None):
        # arrange
        source_timestamp_qh_df = pd.DataFrame({"source": {0: "A31R1"}, "timestamp": {0: 1544631694793000000}})
        u_hds_dfs = [
            [
                read_csv("resources/hwc/rb/fpa", "u_hds_1_df"),
                read_csv("resources/hwc/rb/fpa", "u_hds_2_df"),
                read_csv("resources/hwc/rb/fpa", "u_hds_3_df"),
                read_csv("resources/hwc/rb/fpa", "u_hds_4_df"),
            ]
        ]
        i_hds_dfs = [
            [
                read_csv("resources/hwc/rb/fpa", "i_hds_1_df"),
                read_csv("resources/hwc/rb/fpa", "i_hds_2_df"),
                read_csv("resources/hwc/rb/fpa", "i_hds_3_df"),
                read_csv("resources/hwc/rb/fpa", "i_hds_4_df"),
            ]
        ]

        u_hds_ref_dfs = [
            [
                read_csv("resources/hwc/rb/fpa", "u_hds_1_ref_df"),
                read_csv("resources/hwc/rb/fpa", "u_hds_2_ref_df"),
                read_csv("resources/hwc/rb/fpa", "u_hds_3_ref_df"),
                read_csv("resources/hwc/rb/fpa", "u_hds_4_ref_df"),
            ]
        ]
        i_hds_ref_dfs = [
            [
                read_csv("resources/hwc/rb/fpa", "i_hds_1_ref_df"),
                read_csv("resources/hwc/rb/fpa", "i_hds_2_ref_df"),
                read_csv("resources/hwc/rb/fpa", "i_hds_3_ref_df"),
                read_csv("resources/hwc/rb/fpa", "i_hds_4_ref_df"),
            ]
        ]
        # act
        self.rb_analysis.analyze_multi_qh_voltage_current_with_ref(
            source_timestamp_qh_df, u_hds_dfs, i_hds_dfs, u_hds_ref_dfs, i_hds_ref_dfs, current_offset=0.085
        )

        # assert
        if mock_show is not None:
            mock_show.assert_called()

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_analyze_qh_for_missing_data(self, mock_show=None):
        # arrange
        source_timestamp_qh_df = read_csv("resources/hwc/rb/fpa_2021_08_16", "source_timestamp_qh_df")
        u_hds_dfs = [
            [
                read_csv("resources/hwc/rb/fpa_2021_08_16", "B11R6_u_hds_1_df"),
                read_csv("resources/hwc/rb/fpa_2021_08_16", "B11R6_u_hds_2_df"),
            ],
            [
                read_csv("resources/hwc/rb/fpa_2021_08_16", "A11R6_u_hds_1_df"),
                read_csv("resources/hwc/rb/fpa_2021_08_16", "A11R6_u_hds_2_df"),
            ],
            [
                read_csv("resources/hwc/rb/fpa_2021_08_16", "B10R6_u_hds_1_df"),
                read_csv("resources/hwc/rb/fpa_2021_08_16", "B10R6_u_hds_2_df"),
            ],
            [
                read_csv("resources/hwc/rb/fpa_2021_08_16", "A10R6_u_hds_1_df"),
                read_csv("resources/hwc/rb/fpa_2021_08_16", "A10R6_u_hds_2_df"),
            ],
        ]
        i_hds_dfs = [
            [
                read_csv("resources/hwc/rb/fpa_2021_08_16", "B11R6_i_hds_1_df"),
                read_csv("resources/hwc/rb/fpa_2021_08_16", "B11R6_i_hds_2_df"),
            ],
            [
                read_csv("resources/hwc/rb/fpa_2021_08_16", "A11R6_i_hds_1_df"),
                read_csv("resources/hwc/rb/fpa_2021_08_16", "A11R6_i_hds_2_df"),
            ],
            [
                read_csv("resources/hwc/rb/fpa_2021_08_16", "B10R6_i_hds_1_df"),
                read_csv("resources/hwc/rb/fpa_2021_08_16", "B10R6_i_hds_2_df"),
            ],
            [
                read_csv("resources/hwc/rb/fpa_2021_08_16", "A10R6_i_hds_1_df"),
                read_csv("resources/hwc/rb/fpa_2021_08_16", "A10R6_i_hds_2_df"),
            ],
        ]
        u_hds_ref_dfs = [
            [
                read_csv("resources/hwc/rb/fpa_2021_08_16", "B11R6_u_hds_1_ref_df"),
                read_csv("resources/hwc/rb/fpa_2021_08_16", "B11R6_u_hds_2_ref_df"),
            ],
            [
                read_csv("resources/hwc/rb/fpa_2021_08_16", "A11R6_u_hds_1_ref_df"),
                read_csv("resources/hwc/rb/fpa_2021_08_16", "A11R6_u_hds_2_ref_df"),
            ],
            [
                read_csv("resources/hwc/rb/fpa_2021_08_16", "B10R6_u_hds_1_ref_df"),
                read_csv("resources/hwc/rb/fpa_2021_08_16", "B10R6_u_hds_2_ref_df"),
            ],
        ]
        i_hds_ref_dfs = [
            [
                read_csv("resources/hwc/rb/fpa_2021_08_16", "B11R6_i_hds_1_ref_df"),
                read_csv("resources/hwc/rb/fpa_2021_08_16", "B11R6_i_hds_2_ref_df"),
            ],
            [
                read_csv("resources/hwc/rb/fpa_2021_08_16", "A11R6_i_hds_1_ref_df"),
                read_csv("resources/hwc/rb/fpa_2021_08_16", "A11R6_i_hds_2_ref_df"),
            ],
            [
                read_csv("resources/hwc/rb/fpa_2021_08_16", "B10R6_i_hds_1_ref_df"),
                read_csv("resources/hwc/rb/fpa_2021_08_16", "B10R6_i_hds_2_ref_df"),
            ],
        ]
        # act
        with warnings.catch_warnings(record=True) as w:
            self.rb_analysis.analyze_multi_qh_voltage_current_with_ref(
                source_timestamp_qh_df, u_hds_dfs, i_hds_dfs, u_hds_ref_dfs, i_hds_ref_dfs, current_offset=0.085
            )
        # assert
        mock_show.assert_called()
        self.assertEqual(
            "At least one DataFrame is empty, QH voltage and current analysis skipped!", str(w[-1].message)
        )
        self.assertEqual("Reference currents missing for A10R6.", str(w[-2].message))
        self.assertEqual("Reference voltages missing for A10R6.", str(w[-3].message))

    def test_analyze_qds_trigger(self):
        # arrange
        u_qs0_df, st_nqd0_df, st_magnet_ok_df = self.load_inputs_for_analyze_qds_trigger_tests()

        # act
        report_qds_trigger_act = analyze_qds_trigger(u_qs0_df, st_magnet_ok_df, st_nqd0_df)

        # assert
        report_qds_trigger_ref = pd.DataFrame(
            {
                "t_st_nqd0 [s]": {0: -0.026},
                "t_st_magnet_ok [s]": {0: -0.016},
                "u_st_nqd0 [V]": {0: 0.0019524792},
                "t_start_quench [s]": {0: -0.038},
                "du_dt [V/s]": {0: -7.914204933333333},
                "t_delay_qh_trigger [s]": {0: 0.016},
                "analysis_comment": {0: ""},
            }
        )

        pd.testing.assert_frame_equal(report_qds_trigger_ref, report_qds_trigger_act)

    @staticmethod
    def load_inputs_for_analyze_qds_trigger_tests():
        u_qs0_df = read_csv("resources/hwc/rb/fpa", "u_qs0_0_df")
        st_nqd0_df = read_csv("resources/hwc/rb/fpa", "st_nqd0_0_df")
        st_magnet_ok_df = read_csv("resources/hwc/rb/fpa", "st_magnet_ok_0_df")
        return u_qs0_df, st_nqd0_df, st_magnet_ok_df

    def test_analyze_qds_trigger_empty_result_nQPS(self):
        # arrange
        u_qs0_df, st_nqd0_df, st_magnet_ok_df = self.load_inputs_for_analyze_qds_trigger_tests()
        st_magnet_ok_df.values[:] = True

        # act
        report_qds_trigger_act = analyze_qds_trigger(u_qs0_df, st_magnet_ok_df, st_nqd0_df)

        # assert
        expected_comment = "Seems that the heaters were triggered by nQPS"
        report_qds_trigger_ref = self.ref_result_for_analyze_qds_trigger_tests(expected_comment)

        pd.testing.assert_frame_equal(report_qds_trigger_ref, report_qds_trigger_act)

    @staticmethod
    def ref_result_for_analyze_qds_trigger_tests(comment: str):
        return pd.DataFrame(
            {
                "t_st_nqd0 [s]": {0: 0},
                "t_st_magnet_ok [s]": {0: 0},
                "u_st_nqd0 [V]": {0: np.nan},
                "t_start_quench [s]": {0: 0},
                "du_dt [V/s]": {0: 0},
                "t_delay_qh_trigger [s]": {0: np.nan},
                "analysis_comment": {0: comment},
            }
        )

    def test_analyze_qds_trigger_empty_result_ST_NQD0_below_1(self):
        # arrange
        u_qs0_df, st_nqd0_df, st_magnet_ok_df = self.load_inputs_for_analyze_qds_trigger_tests()
        st_nqd0_df.values[:] = 0.0
        expected_comment = "ST_NQD0 signal never reaches 1. (QDS from the first board)"

        # act
        with pytest.warns(UserWarning, match=re.escape(expected_comment)):
            report_qds_trigger_act = analyze_qds_trigger(u_qs0_df, st_magnet_ok_df, st_nqd0_df)

        # assert
        report_qds_trigger_ref = self.ref_result_for_analyze_qds_trigger_tests(expected_comment)

        pd.testing.assert_frame_equal(report_qds_trigger_ref, report_qds_trigger_act)

    def test_analyze_qds_trigger_no_quench_data(self):
        # arrange
        u_qs0_df, st_nqd0_df, st_magnet_ok_df = self.load_inputs_for_analyze_qds_trigger_tests()
        st_nqd0_df = st_nqd0_df.tail(3500)
        expected_comment = "No quench developing part found in the buffer."

        # act
        with pytest.warns(UserWarning, match=re.escape(expected_comment)):
            report_qds_trigger_act = analyze_qds_trigger(u_qs0_df, st_magnet_ok_df, st_nqd0_df)

        # assert
        report_qds_trigger_ref = self.ref_result_for_analyze_qds_trigger_tests(expected_comment)

        pd.testing.assert_frame_equal(report_qds_trigger_ref, report_qds_trigger_act)

    def test_get_t_st_magnet_ok(self):
        # arrange
        st_magnet_ok_df = read_csv("resources/hwc/rb/fpa", "st_magnet_ok_0_df")

        # act
        result = QdsAnalysis._get_t_st_magnet_ok(st_magnet_ok_df)

        # assert
        expected_result = -0.016
        self.assertEqual(expected_result, result)

    def test_get_t_st_magnet_ok_none(self):
        # arrange
        st_magnet_ok_df = read_csv("resources/hwc/rb/fpa", "st_magnet_ok_0_df")
        st_magnet_ok_df["A31R1:ST_MAGNET_OK"] = True

        # act
        result = QdsAnalysis._get_t_st_magnet_ok(st_magnet_ok_df)

        # assert
        self.assertIsNone(result)

    def test_get_t_st_magnet_ok_no_data(self):
        # arrange
        st_magnet_ok_df = pd.DataFrame()

        # act
        result = QdsAnalysis._get_t_st_magnet_ok(st_magnet_ok_df)

        # assert
        self.assertIsNone(result)

    def test_get_t_st_nqd0(self):
        # arrange
        st_nqd0_df = read_csv("resources/hwc/rb/fpa", "st_nqd0_0_df")
        t_st_magnet_ok = -0.325

        # act
        result = QdsAnalysis._get_t_st_nqd0(st_nqd0_df, t_st_magnet_ok)

        # assert
        expected_result = -0.324
        self.assertEqual(expected_result, result)

    def test_get_t_st_nqd0_none(self):
        # arrange
        st_nqd0_df = read_csv("resources/hwc/rb/fpa", "st_nqd0_0_df")
        st_nqd0_df["A31R1:ST_NQD0"] = False
        t_st_magnet_ok = -0.16

        # act
        result = QdsAnalysis._get_t_st_nqd0(st_nqd0_df, t_st_magnet_ok)

        # assert
        self.assertIsNone(result)

    def test_contains_quench_developing_part(self):
        # assert
        st_nqd0_df = read_csv("resources/hwc/rb/fpa", "st_nqd0_0_df")
        st_magnet_ok_df = read_csv("resources/hwc/rb/fpa", "st_magnet_ok_0_df")

        # act
        result = QdsAnalysis._contains_quench_developing_part(st_magnet_ok_df, st_nqd0_df)

        # assert
        self.assertTrue(result)

    def test_not_contains_quench_developing_part(self):
        # assert
        st_nqd0_df = read_csv("resources/hwc/rb/fpa", "st_nqd0_0_df")
        st_nqd0_df["A31R1:ST_NQD0"] = False
        st_magnet_ok_df = read_csv("resources/hwc/rb/fpa", "st_magnet_ok_0_df")

        # act
        result = QdsAnalysis._contains_quench_developing_part(st_magnet_ok_df, st_nqd0_df)

        # assert
        self.assertFalse(result)

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_analyze_qds(self, mock_show=None):
        # arrange
        i_meas_df = read_csv("resources/hwc/rb/fpa", "i_meas_df")
        u_qds_dfs = [
            _get_u_qds_dfs_for_0_A31R1(),
            [
                read_csv("resources/hwc/rb/fpa", "1_C31R1.U_QS0"),
                read_csv("resources/hwc/rb/fpa", "1_C31R1.U_1"),
                read_csv("resources/hwc/rb/fpa", "1_C31R1.U_2"),
                read_csv("resources/hwc/rb/fpa", "1_C31R1.ST_NQD0"),
                read_csv("resources/hwc/rb/fpa", "1_C31R1.ST_MAGNET_OK"),
            ],
            [
                read_csv("resources/hwc/rb/fpa", "2_B9R1.U_QS0"),
                read_csv("resources/hwc/rb/fpa", "2_B9R1.U_1"),
                read_csv("resources/hwc/rb/fpa", "2_B9R1.U_2"),
                read_csv("resources/hwc/rb/fpa", "2_B9R1.ST_NQD0"),
                read_csv("resources/hwc/rb/fpa", "2_B9R1.ST_MAGNET_OK"),
            ],
            [
                read_csv("resources/hwc/rb/fpa", "3_B31R1.U_QS0"),
                read_csv("resources/hwc/rb/fpa", "3_B31R1.U_1"),
                read_csv("resources/hwc/rb/fpa", "3_B31R1.U_2"),
                read_csv("resources/hwc/rb/fpa", "3_B31R1.ST_NQD0"),
                read_csv("resources/hwc/rb/fpa", "3_B31R1.ST_MAGNET_OK"),
            ],
            [
                read_csv("resources/hwc/rb/fpa", "4_A9R1.U_QS0"),
                read_csv("resources/hwc/rb/fpa", "4_A9R1.U_1"),
                read_csv("resources/hwc/rb/fpa", "4_A9R1.U_2"),
                read_csv("resources/hwc/rb/fpa", "4_A9R1.ST_NQD0"),
                read_csv("resources/hwc/rb/fpa", "4_A9R1.ST_MAGNET_OK"),
            ],
        ]

        u_nqps_dfs = [
            [
                read_csv("resources/hwc/rb/fpa", "0_MB.B30R1.U_DIODE_RB"),
                read_csv("resources/hwc/rb/fpa", "0_MB.A31R1.U_DIODE_RB"),
                read_csv("resources/hwc/rb/fpa", "0_MB.C31R1.U_DIODE_RB"),
                read_csv("resources/hwc/rb/fpa", "0_DQQDS.B31R1.RB.A12.U_REF_N1"),
            ],
            [
                read_csv("resources/hwc/rb/fpa", "1_MB.B30R1.U_DIODE_RB"),
                read_csv("resources/hwc/rb/fpa", "1_MB.A31R1.U_DIODE_RB"),
                read_csv("resources/hwc/rb/fpa", "1_MB.C31R1.U_DIODE_RB"),
                read_csv("resources/hwc/rb/fpa", "1_DQQDS.B31R1.RB.A12.U_REF_N1"),
            ],
            [
                read_csv("resources/hwc/rb/fpa", "2_MB.B9R1.U_DIODE_RB"),
                read_csv("resources/hwc/rb/fpa", "2_MB.B10R1.U_DIODE_RB"),
                read_csv("resources/hwc/rb/fpa", "2_MB.B11R1.U_DIODE_RB"),
                read_csv("resources/hwc/rb/fpa", "2_DQQDS.B11R1.RB.A12.U_REF_N1"),
            ],
            [
                read_csv("resources/hwc/rb/fpa", "3_MB.A30R1.U_DIODE_RB"),
                read_csv("resources/hwc/rb/fpa", "3_MB.C30R1.U_DIODE_RB"),
                read_csv("resources/hwc/rb/fpa", "3_MB.B31R1.U_DIODE_RB"),
                read_csv("resources/hwc/rb/fpa", "3_DQQDS.B30R1.RB.A12.U_REF_N1"),
            ],
            [
                read_csv("resources/hwc/rb/fpa", "4_MB.A9R1.U_DIODE_RB"),
                read_csv("resources/hwc/rb/fpa", "4_MB.A10R1.U_DIODE_RB"),
                read_csv("resources/hwc/rb/fpa", "4_MB.A11R1.U_DIODE_RB"),
                read_csv("resources/hwc/rb/fpa", "4_DQQDS.B10R1.RB.A12.U_REF_N1"),
            ],
        ]

        results_table_final = read_csv("resources/hwc/rb/fpa", "results_table_final")

        rb_analysis = RbCircuitAnalysis(
            self.circuit_type, pd.DataFrame(results_table_final.iloc[0:4]), is_automatic=True
        )

        # act
        rb_analysis.analyze_qds(self.timestamp_fgc, self.timestamp_pic, u_qds_dfs, u_qds_dfs, u_nqps_dfs, i_meas_df)

        # assert
        if mock_show is not None:
            mock_show.assert_called()

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_analyze_qds_threshold(self, mock_show=None):
        # arrange
        i_meas_df = read_csv("resources/hwc/rb/fpa", "i_meas_df")
        u_qds_dfs = [_get_u_qds_dfs_for_0_A31R1()]

        u_nqps_dfs = [
            [
                read_csv("resources/hwc/rb/fpa", "0_MB.B30R1.U_DIODE_RB"),
                read_csv("resources/hwc/rb/fpa", "0_MB.A31R1.U_DIODE_RB"),
                read_csv("resources/hwc/rb/fpa", "0_MB.C31R1.U_DIODE_RB"),
                read_csv("resources/hwc/rb/fpa", "0_DQQDS.B31R1.RB.A12.U_REF_N1"),
            ]
        ]

        results_table_final = read_csv("resources/hwc/rb/fpa", "results_table_final")

        rb_analysis = RbCircuitAnalysis(
            self.circuit_type, pd.DataFrame(results_table_final.iloc[0:1]), is_automatic=True
        )

        # act
        threshold = 0.2
        rb_analysis.analyze_qds(
            self.timestamp_fgc, self.timestamp_pic, u_qds_dfs, u_qds_dfs, u_nqps_dfs, i_meas_df, threshold
        )

        # assert
        if mock_show is not None:
            mock_show.assert_called()

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_analyze_qds_no_timestamp_iqps(self, mock_show=None):
        # arrange
        i_meas_df = pd.DataFrame()
        u_qds_dfs = [_get_u_qds_dfs_for_0_A31R1()]
        u_nqps_dfs = [[]]

        results_table_final = read_csv("resources/hwc/rb/fpa", "results_table_final")
        results_table_final.drop(["timestamp_iqps"], axis=1)

        rb_analysis = RbCircuitAnalysis(
            self.circuit_type, pd.DataFrame(results_table_final.iloc[0:1]), is_automatic=True
        )

        # act
        threshold = 0.2
        rb_analysis.analyze_qds(
            self.timestamp_fgc, self.timestamp_pic, u_qds_dfs, u_qds_dfs, u_nqps_dfs, i_meas_df, threshold
        )

        # assert
        if mock_show is not None:
            mock_show.assert_called()

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    @patch("lhcsmapi.analysis.qds.QdsAnalysis._contains_quench_developing_part", return_value=False)
    def test_analyze_qds_1_corrupted_buffer(self, mock_show=None, mock_method=None):
        # arrange
        i_meas_df = pd.DataFrame()
        u_qds_dfs = [_get_u_qds_dfs_for_0_A31R1()]
        u_qds_dfs[0][3] = u_qds_dfs[0][3].tail(3500)
        u_qds_dfs2 = [_get_u_qds_dfs_for_0_A31R1()]
        u_nqps_dfs = [[]]

        results_table_final = read_csv("resources/hwc/rb/fpa", "results_table_final")
        results_table_final.drop(["timestamp_iqps"], axis=1)

        rb_analysis = RbCircuitAnalysis(
            self.circuit_type, pd.DataFrame(results_table_final.iloc[0:1]), is_automatic=True
        )

        # act
        with warnings.catch_warnings(record=True) as w:
            warnings.filterwarnings("ignore", category=DeprecationWarning)
            threshold = 0.2
            rb_analysis.analyze_qds(
                self.timestamp_fgc, self.timestamp_pic, u_qds_dfs, u_qds_dfs2, u_nqps_dfs, i_meas_df, threshold
            )

            # assert
            assert "No quench developing part found in the first buffer. Checking the second one." in {
                str(warning.message) for warning in w
            }
            if mock_show is not None:
                mock_show.assert_called()

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    @patch("lhcsmapi.analysis.qds.QdsAnalysis._contains_quench_developing_part", return_value=False)
    def test_analyze_qds_2_corrupted_buffers(self, mock_show=None, mock_method=None):
        # arrange
        i_meas_df = pd.DataFrame()
        u_qds_dfs = [_get_u_qds_dfs_for_0_A31R1()]
        u_qds_dfs[0][3] = u_qds_dfs[0][3].tail(3500)
        u_qds_dfs2 = [_get_u_qds_dfs_for_0_A31R1()]
        u_qds_dfs2[0][3] = u_qds_dfs2[0][3].tail(3500)
        u_nqps_dfs = [[]]

        results_table_final = read_csv("resources/hwc/rb/fpa", "results_table_final")
        results_table_final.drop(["timestamp_iqps"], axis=1)

        rb_analysis = RbCircuitAnalysis(
            self.circuit_type, pd.DataFrame(results_table_final.iloc[0:1]), is_automatic=True
        )

        # act
        with warnings.catch_warnings(record=True) as w:
            warnings.filterwarnings("ignore", category=DeprecationWarning)
            threshold = 0.2
            rb_analysis.analyze_qds(
                self.timestamp_fgc, self.timestamp_pic, u_qds_dfs, u_qds_dfs2, u_nqps_dfs, i_meas_df, threshold
            )

            # assert
            assert "No quench developing part found in the first buffer. Checking the second one." in {
                str(warning.message) for warning in w
            }
            assert "No quench developing part found in the buffer." in {str(warning.message) for warning in w}
            if mock_show is not None:
                mock_show.assert_called()

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_analyze_qds_threshold_flipped_axis(self, mock_show=None):
        # arrange
        i_meas_df = read_csv("resources/hwc/rb/fpa_2021_03_08", "STATUS.I_MEAS")
        u_qds_dfs = [
            [
                read_csv("resources/hwc/rb/fpa_2021_03_08", "A16L8.U_QS0"),
                read_csv("resources/hwc/rb/fpa_2021_03_08", "A16L8.U_1"),
                read_csv("resources/hwc/rb/fpa_2021_03_08", "A16L8.U_2"),
                read_csv("resources/hwc/rb/fpa_2021_03_08", "A16L8.ST_NQD0"),
                read_csv("resources/hwc/rb/fpa_2021_03_08", "A16L8.ST_MAGNET_OK"),
            ]
        ]

        u_nqps_dfs = [
            [
                read_csv("resources/hwc/rb/fpa_2021_03_08", "MB.A16L8.U_DIODE_RB"),
                read_csv("resources/hwc/rb/fpa_2021_03_08", "MB.B17L8.U_DIODE_RB"),
                read_csv("resources/hwc/rb/fpa_2021_03_08", "MB.C16L8.U_DIODE_RB"),
                read_csv("resources/hwc/rb/fpa_2021_03_08", "DQQDS.B16L8.RB.A78.U_REF_N1"),
            ]
        ]

        results_table_final = read_csv("resources/hwc/rb/fpa", "results_table_final")

        rb_analysis = RbCircuitAnalysis(
            self.circuit_type, pd.DataFrame(results_table_final.iloc[0:1]), is_automatic=True
        )

        # act
        threshold = 0.2
        rb_analysis.analyze_qds(
            self.timestamp_fgc, self.timestamp_pic, u_qds_dfs, u_qds_dfs, u_nqps_dfs, i_meas_df, threshold
        )

        # assert
        if mock_show is not None:
            mock_show.assert_called()

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_analyze_qds_with_corrupted_pm_buffer(self, mock_show=None):
        # arrange
        i_meas_df = read_csv("resources/hwc/rb/fpa_2021_08_05", "STATUS.I_MEAS")
        u_qds_dfs = [
            [
                read_csv("resources/hwc/rb/fpa_2021_08_05", "C17R6.U_QS0"),
                read_csv("resources/hwc/rb/fpa_2021_08_05", "C17R6.U_1"),
                read_csv("resources/hwc/rb/fpa_2021_08_05", "C17R6.U_2"),
                read_csv("resources/hwc/rb/fpa_2021_08_05", "C17R6.ST_NQD0"),
                read_csv("resources/hwc/rb/fpa_2021_08_05", "C17R6.ST_MAGNET_OK"),
            ]
        ]
        u_qds_dfs2 = [
            [
                read_csv("resources/hwc/rb/fpa_2021_08_05", "B17R6.U_QS0"),
                read_csv("resources/hwc/rb/fpa_2021_08_05", "B17R6.U_1"),
                read_csv("resources/hwc/rb/fpa_2021_08_05", "B17R6.U_2"),
                read_csv("resources/hwc/rb/fpa_2021_08_05", "B17R6.ST_NQD0"),
                read_csv("resources/hwc/rb/fpa_2021_08_05", "B17R6.ST_MAGNET_OK"),
            ]
        ]

        u_nqps_dfs = [
            [
                read_csv("resources/hwc/rb/fpa_2021_08_05", "MB.B16R6.U_DIODE_RB"),
                read_csv("resources/hwc/rb/fpa_2021_08_05", "MB.A17R6.U_DIODE_RB"),
                read_csv("resources/hwc/rb/fpa_2021_08_05", "MB.C17R6.U_DIODE_RB"),
                read_csv("resources/hwc/rb/fpa_2021_08_05", "DQQDS.B17R6.RB.A67.U_REF_N1"),
            ]
        ]

        results_table_final = read_csv("resources/hwc/rb/fpa", "results_table_final")

        rb_analysis = RbCircuitAnalysis(
            self.circuit_type, pd.DataFrame(results_table_final.iloc[0:1]), is_automatic=True
        )

        timestamp_fgc = 1628186188960000000
        timestamp_pic = 1628186188939000000

        # act
        threshold = 0.2
        with pytest.warns(UserWarning, match=re.escape("WARNING: Boards A/B correlation is less than 0.9!")):
            rb_analysis.analyze_qds(
                timestamp_fgc, timestamp_pic, u_qds_dfs, u_qds_dfs2, u_nqps_dfs, i_meas_df, threshold
            )

        # assert
        if mock_show is not None:
            mock_show.assert_called()

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_analyze_u_diode_nqps(self, mock_show=None):
        # arrange
        u_dfs = [
            read_csv("resources/hwc/rb/fpa", "4_MB.A9R1.U_DIODE_RB"),
            read_csv("resources/hwc/rb/fpa", "4_MB.A10R1.U_DIODE_RB"),
            read_csv("resources/hwc/rb/fpa", "4_MB.A11R1.U_DIODE_RB"),
            read_csv("resources/hwc/rb/fpa", "4_DQQDS.B10R1.RB.A12.U_REF_N1"),
        ]

        i_meas_df = read_csv("resources/hwc/rb/fpa", "i_meas_df")

        results_table_final = read_csv("resources/hwc/rb/fpa", "results_table_final")

        rb_analysis = RbCircuitAnalysis(
            self.circuit_type, pd.DataFrame(results_table_final.iloc[0:4]), is_automatic=True
        )

        # act
        rb_analysis.analyze_u_diode_nqps(
            self.circuit_name,
            self.timestamp_fgc,
            i_meas_df,
            u_dfs,
            signal="U_DIODE_RB",
            system="DIODE_RB",
            legend=False,
            xlim=(-5, 150),
        )

        # assert
        if mock_show is not None:
            mock_show.assert_called()

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_analyze_u_diode_nqps_filtered(self, mock_show=None):
        # arrange
        u_dfs = [
            read_csv("resources/hwc/rb/fpa", "4_MB.A9R1.U_DIODE_RB"),
            read_csv("resources/hwc/rb/fpa", "4_MB.A10R1.U_DIODE_RB"),
            read_csv("resources/hwc/rb/fpa", "4_MB.A11R1.U_DIODE_RB"),
            read_csv("resources/hwc/rb/fpa", "4_DQQDS.B10R1.RB.A12.U_REF_N1"),
        ]

        i_meas_df = read_csv("resources/hwc/rb/fpa", "i_meas_df")

        results_table_final = read_csv("resources/hwc/rb/fpa", "results_table_final")

        rb_analysis = RbCircuitAnalysis(
            self.circuit_type, pd.DataFrame(results_table_final.iloc[0:4]), is_automatic=True
        )

        # act
        u_filtered_dfs = rb_analysis.filter_quenched_magnets(u_dfs, results_table_final["Position"])
        rb_analysis.analyze_u_diode_nqps(
            self.circuit_name,
            self.timestamp_fgc,
            i_meas_df,
            u_filtered_dfs,
            signal="U_DIODE_RB",
            system="DIODE_RB",
            legend=False,
            xlim=(-5, 150),
        )

        # assert
        if mock_show is not None:
            mock_show.assert_called()

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_analyze_diode_leads(self, mock_show=None):
        # arrange
        source_timestamp_qds_df = pd.DataFrame({"source": {0: "B31R1"}, "timestamp": {0: 1544631727546000000}})
        results_table = pd.DataFrame({"I_Q_M": {0: 8235}})
        i_meas_u_diode_nxcals_dfs = [
            [
                read_csv("resources/hwc/rb/fpa", "I_MEAS_U_DIODE"),
                read_csv("resources/hwc/rb/fpa", "MB.B31R1.U_DIODE_RB_U_DIODE"),
            ]
        ]
        rb_analysis = RbCircuitAnalysis(self.circuit_type, pd.DataFrame(), is_automatic=False)

        # act
        with patch("sys.stdout", new=StringIO()) as fake_out:
            rb_analysis.analyze_diode_leads(
                source_timestamp_qds_df,
                self.timestamp_fgc,
                results_table["I_Q_M"],
                self.circuit_name,
                [],
                i_meas_u_diode_nxcals_dfs,
                5,
            )

        # assert
        if mock_show is not None:
            mock_show.assert_called()

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_plot_leads_voltage_zoom(self, mock_show=None):
        # arrange
        u_hts_dfs = [
            read_csv("resources/hwc/rb/fpa", "DFLAS.7L2.RB.A12.LD1.U_HTS"),
            read_csv("resources/hwc/rb/fpa", "DFLAS.7L2.RB.A12.LD2.U_HTS"),
            read_csv("resources/hwc/rb/fpa", "DFLAS.7R1.RB.A12.LD3.U_HTS"),
            read_csv("resources/hwc/rb/fpa", "DFLAS.7R1.RB.A12.LD4.U_HTS"),
        ]
        rb_analysis = RbCircuitAnalysis(self.circuit_type, pd.DataFrame, is_automatic=False)

        # act
        rb_analysis.plot_leads_voltage_zoom(
            u_hts_dfs,
            self.circuit_name,
            self.timestamp_fgc,
            1544631694792000000,
            self.timestamp_ee_odd,
            self.timestamp_ee_even,
            signal="U_HTS",
        )

        # assert
        if mock_show is not None:
            mock_show.assert_called()

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_analyze_voltage_feelers(self, mock_show=None):
        # arrange
        u_earth_rb_names = [
            "MB.A8R1_U_EARTH_RB",
            "MB.B8R1_U_EARTH_RB",
            "MB.A10R1_U_EARTH_RB",
            "MB.B11R1_U_EARTH_RB",
            "MB.C12R1_U_EARTH_RB",
            "MB.C13R1_U_EARTH_RB",
            "MB.C14R1_U_EARTH_RB",
            "MB.C15R1_U_EARTH_RB",
            "MB.C16R1_U_EARTH_RB",
            "MB.C17R1_U_EARTH_RB",
            "MB.C18R1_U_EARTH_RB",
            "MB.C19R1_U_EARTH_RB",
            "MB.C20R1_U_EARTH_RB",
            "MB.C21R1_U_EARTH_RB",
            "MB.C22R1_U_EARTH_RB",
            "MB.C23R1_U_EARTH_RB",
            "MB.C24R1_U_EARTH_RB",
            "MB.C25R1_U_EARTH_RB",
            "MB.C26R1_U_EARTH_RB",
            "MB.C27R1_U_EARTH_RB",
            "MB.C28R1_U_EARTH_RB",
            "MB.C29R1_U_EARTH_RB",
            "MB.C30R1_U_EARTH_RB",
            "MB.C31R1_U_EARTH_RB",
            "MB.C32R1_U_EARTH_RB",
            "MB.A33R1_U_EARTH_RB",
            "MB.C34R1_U_EARTH_RB",
            "MB.C34L2_U_EARTH_RB",
            "MB.C33L2_U_EARTH_RB",
            "MB.C32L2_U_EARTH_RB",
            "MB.C31L2_U_EARTH_RB",
            "MB.C30L2_U_EARTH_RB",
            "MB.C29L2_U_EARTH_RB",
            "MB.C28L2_U_EARTH_RB",
            "MB.C27L2_U_EARTH_RB",
            "MB.C26L2_U_EARTH_RB",
            "MB.C25L2_U_EARTH_RB",
            "MB.C24L2_U_EARTH_RB",
            "MB.C23L2_U_EARTH_RB",
            "MB.C22L2_U_EARTH_RB",
            "MB.C21L2_U_EARTH_RB",
            "MB.C20L2_U_EARTH_RB",
            "MB.C19L2_U_EARTH_RB",
            "MB.C18L2_U_EARTH_RB",
            "MB.C17L2_U_EARTH_RB",
            "MB.C16L2_U_EARTH_RB",
            "MB.C15L2_U_EARTH_RB",
            "MB.C14L2_U_EARTH_RB",
            "MB.C13L2_U_EARTH_RB",
            "MB.C12L2_U_EARTH_RB",
            "MB.B11L2_U_EARTH_RB",
            "MB.A10L2_U_EARTH_RB",
            "MB.B8L2_U_EARTH_RB",
            "MB.A8L2_U_EARTH_RB",
        ]

        u_earth_rb_dfs = []
        for u_earth_rb_name in u_earth_rb_names:
            u_earth_rb_dfs.append(read_csv("resources/hwc/rb/fpa", u_earth_rb_name))

        rb_analysis = RbCircuitAnalysis(self.circuit_type, pd.DataFrame(), is_automatic=False)
        i_meas_df = read_csv("resources/hwc/rb/fpa", "i_meas_df")

        # act
        # assert
        with patch("sys.stdout", new=StringIO()) as fake_out:
            rb_analysis.analyze_voltage_feelers(
                self.circuit_name, self.timestamp_fgc, i_meas_df, u_earth_rb_dfs, "U_EARTH_RB", "VF"
            )

        vf_analysis_ref = (
            "Report on Voltage Feeler cards:\n"
            "- there are 54 cards.\n"
            "- there are 49 functioning cards.\n"
            "- there are 0 not communicating cards: [].\n"
            "- there are 5 disabled cards: ['MB.B8R1:U_EARTH_RB', 'MB.A10R1:U_EARTH_RB', "
            "'MB.C12R1:U_EARTH_RB', 'MB.C25R1:U_EARTH_RB', 'MB.C12L2:U_EARTH_RB']."
        )

        self.assertEqual(vf_analysis_ref, fake_out.getvalue().strip())

        if mock_show is not None:
            mock_show.assert_called()

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_analyze_voltage_feelers_not_communicating(self, mock_show=None):
        # arrange
        u_earth_rb_names = [
            "MB.A8R1_U_EARTH_RB",
            "MB.B8R1_U_EARTH_RB",
            "MB.A10R1_U_EARTH_RB",
            "MB.B11R1_U_EARTH_RB",
            "MB.C12R1_U_EARTH_RB",
            "MB.C13R1_U_EARTH_RB",
            "MB.C14R1_U_EARTH_RB",
            "MB.C15R1_U_EARTH_RB",
            "MB.C16R1_U_EARTH_RB",
            "MB.C17R1_U_EARTH_RB",
            "MB.C18R1_U_EARTH_RB",
            "MB.C19R1_U_EARTH_RB",
            "MB.C20R1_U_EARTH_RB",
            "MB.C21R1_U_EARTH_RB",
            "MB.C22R1_U_EARTH_RB",
            "MB.C23R1_U_EARTH_RB",
            "MB.C24R1_U_EARTH_RB",
            "MB.C25R1_U_EARTH_RB",
            "MB.C26R1_U_EARTH_RB",
            "MB.C27R1_U_EARTH_RB",
            "MB.C28R1_U_EARTH_RB",
            "MB.C29R1_U_EARTH_RB",
            "MB.C30R1_U_EARTH_RB",
            "MB.C31R1_U_EARTH_RB",
            "MB.C32R1_U_EARTH_RB",
            "MB.A33R1_U_EARTH_RB",
            "MB.C34R1_U_EARTH_RB",
            "MB.C34L2_U_EARTH_RB",
            "MB.C33L2_U_EARTH_RB",
            "MB.C32L2_U_EARTH_RB",
            "MB.C31L2_U_EARTH_RB",
            "MB.C30L2_U_EARTH_RB",
            "MB.C29L2_U_EARTH_RB",
            "MB.C28L2_U_EARTH_RB",
            "MB.C27L2_U_EARTH_RB",
            "MB.C26L2_U_EARTH_RB",
            "MB.C25L2_U_EARTH_RB",
            "MB.C24L2_U_EARTH_RB",
            "MB.C23L2_U_EARTH_RB",
            "MB.C22L2_U_EARTH_RB",
            "MB.C21L2_U_EARTH_RB",
            "MB.C20L2_U_EARTH_RB",
            "MB.C19L2_U_EARTH_RB",
            "MB.C18L2_U_EARTH_RB",
            "MB.C17L2_U_EARTH_RB",
            "MB.C16L2_U_EARTH_RB",
            "MB.C15L2_U_EARTH_RB",
            "MB.C14L2_U_EARTH_RB",
            "MB.C13L2_U_EARTH_RB",
            "MB.C12L2_U_EARTH_RB",
            "MB.B11L2_U_EARTH_RB",
            "MB.A10L2_U_EARTH_RB",
            "MB.B8L2_U_EARTH_RB",
            "MB.A8L2_U_EARTH_RB",
        ]

        u_earth_rb_dfs = []
        for u_earth_rb_name in u_earth_rb_names:
            u_earth_rb_dfs.append(read_csv("resources/hwc/rb/fpa", u_earth_rb_name))

        # add a non-communicating card
        u_earth_rb_dfs[0] -= 20000

        rb_analysis = RbCircuitAnalysis(self.circuit_type, pd.DataFrame(), is_automatic=False)
        i_meas_df = read_csv("resources/hwc/rb/fpa", "i_meas_df")

        # act
        # assert
        with patch("sys.stdout", new=StringIO()) as fake_out:
            rb_analysis.analyze_voltage_feelers(
                self.circuit_name, self.timestamp_fgc, i_meas_df, u_earth_rb_dfs, "U_EARTH_RB", "VF"
            )

        vf_analysis_ref = (
            "Report on Voltage Feeler cards:\n"
            "- there are 54 cards.\n"
            "- there are 48 functioning cards.\n"
            "- there are 1 not communicating cards: ['MB.A8R1:U_EARTH_RB'].\n"
            "- there are 5 disabled cards: ['MB.B8R1:U_EARTH_RB', 'MB.A10R1:U_EARTH_RB', "
            "'MB.C12R1:U_EARTH_RB', 'MB.C25R1:U_EARTH_RB', 'MB.C12L2:U_EARTH_RB']."
        )

        self.assertEqual(vf_analysis_ref, fake_out.getvalue().strip())

        if mock_show is not None:
            mock_show.assert_called()

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_analyze_ee_temp_and_cooling(self, mock_show=None):
        # arrange
        t_res_body_long_dfs = [
            read_csv("resources/hwc/rb/fpa", "DQRB.RR17.RB.A12.T_RES_BODY_1_EVEN_LONG"),
            read_csv("resources/hwc/rb/fpa", "DQRB.RR17.RB.A12.T_RES_BODY_2_EVEN_LONG"),
            read_csv("resources/hwc/rb/fpa", "DQRB.RR17.RB.A12.T_RES_BODY_3_EVEN_LONG"),
            read_csv("resources/hwc/rb/fpa", "DQRB.RR17.RB.A12.T_RES_BODY_1_ODD_LONG"),
            read_csv("resources/hwc/rb/fpa", "DQRB.RR17.RB.A12.T_RES_BODY_2_ODD_LONG"),
            read_csv("resources/hwc/rb/fpa", "DQRB.RR17.RB.A12.T_RES_BODY_3_ODD_LONG"),
        ]  #
        st_res_overtemp_long_dfs = [
            read_csv("resources/hwc/rb/fpa", "DQRB.RR17.RB.A12.ST_RES_OVERTEMP_EVEN_LONG"),
            read_csv("resources/hwc/rb/fpa", "DQRB.RR17.RB.A12.ST_RES_OVERTEMP_ODD_LONG"),
        ]

        rb_analysis = RbCircuitAnalysis(self.circuit_type, pd.DataFrame(), is_automatic=True)

        # act
        rb_analysis.analyze_ee_temp_and_cooling(
            t_res_body_long_dfs, st_res_overtemp_long_dfs, t_greater=2 * 3600, value_max=70
        )

        if mock_show is not None:
            mock_show.assert_called()

    def test_create_mp3_results_table(self):
        # arrange
        is_automatic = True
        results_table_final = read_csv("resources/hwc/rb/fpa", "results_table_final")

        # act
        rb_analysis = RbCircuitAnalysis(self.circuit_type, results_table_final, is_automatic=is_automatic)
        mp3_results_table_act = rb_analysis.create_mp3_results_table(self.timestamp_pic, self.timestamp_fgc)
        mp3_results_table_act.fillna(np.nan, inplace=True)

        # assert
        mp3_results_table_ref = read_csv("resources/hwc/rb/fpa", "mp3_results_table")
        mp3_results_table_ref.fillna(np.nan, inplace=True)

        pd.testing.assert_frame_equal(mp3_results_table_ref, mp3_results_table_act)

    # PLI3.a2
    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_analyze_delay_time_u_dump_res_ee_hwc(self, mock_show=None):
        # arrange
        circuit_name = "RB.A12"
        timestamp_fgc = 1418473597620000000
        timestamp_ee_odd = 1418473749786000000
        timestamp_ee_even = 1418473750032000000

        i_a_df = read_csv("resources/hwc/rb/pli3_d2", "I_A")
        i_ref_df = read_csv("resources/hwc/rb/pli3_d2", "I_REF")
        u_dump_res_odd_df = read_csv("resources/hwc/rb/pli3_d2", "U_DUMP_RES_ODD")
        u_dump_res_even_df = read_csv("resources/hwc/rb/pli3_d2", "U_DUMP_RES_EVEN")

        is_automatic = True
        results_table = None
        rb_analysis = RbCircuitAnalysis(self.circuit_type, results_table, is_automatic=is_automatic)

        # act
        rb_analysis.analyze_delay_time_u_dump_res_ee(
            circuit_name,
            timestamp_fgc,
            timestamp_fgc,
            [timestamp_ee_odd, timestamp_ee_even],
            i_a_df,
            i_ref_df,
            [u_dump_res_odd_df, u_dump_res_even_df],
        )

        # assert
        if mock_show is not None:
            mock_show.assert_called()

    # QHDA
    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_analyze_single_qh(self, mock_show=None):
        # arrange
        timestamp = 1544235350692000000
        source = "A32R1"

        u_hds_dfs = [
            read_csv("resources/hwc/rb/qhda", "0_A32R1.U_HDS_1"),
            read_csv("resources/hwc/rb/qhda", "0_A32R1.U_HDS_2"),
            read_csv("resources/hwc/rb/qhda", "0_A32R1.U_HDS_3"),
            read_csv("resources/hwc/rb/qhda", "0_A32R1.U_HDS_4"),
        ]
        i_hds_dfs = [
            read_csv("resources/hwc/rb/qhda", "0_A32R1.I_HDS_1"),
            read_csv("resources/hwc/rb/qhda", "0_A32R1.I_HDS_2"),
            read_csv("resources/hwc/rb/qhda", "0_A32R1.I_HDS_3"),
            read_csv("resources/hwc/rb/qhda", "0_A32R1.I_HDS_4"),
        ]
        u_hds_ref_dfs = [
            read_csv("resources/hwc/rb/qhda", "0_A32R1.U_HDS_1_REF"),
            read_csv("resources/hwc/rb/qhda", "0_A32R1.U_HDS_2_REF"),
            read_csv("resources/hwc/rb/qhda", "0_A32R1.U_HDS_3_REF"),
            read_csv("resources/hwc/rb/qhda", "0_A32R1.U_HDS_4_REF"),
        ]
        i_hds_ref_dfs = [
            read_csv("resources/hwc/rb/qhda", "0_A32R1.I_HDS_1_REF"),
            read_csv("resources/hwc/rb/qhda", "0_A32R1.I_HDS_2_REF"),
            read_csv("resources/hwc/rb/qhda", "0_A32R1.I_HDS_3_REF"),
            read_csv("resources/hwc/rb/qhda", "0_A32R1.I_HDS_4_REF"),
        ]

        # act
        QuenchHeaterVoltageCurrentAnalysis(circuit_type="RB").analyze_single_qh_voltage_current_with_ref(
            source,
            timestamp,
            u_hds_dfs,
            i_hds_dfs,
            u_hds_ref_dfs,
            i_hds_ref_dfs,
            plot_qh_discharge=QuenchHeaterVoltageCurrentAnalysis.plot_voltage_current_resistance_with_ref_hwc,
            current_offset=0.085,
            mean_start_value=50,
        )

        # assert
        if mock_show is not None:
            mock_show.assert_called()

    def test_calculate_rb_qh_feature_row(self):
        # arrange
        timestamp = 1544235350692000000

        u_hds_dfs = [
            read_csv("resources/hwc/rb/qhda", "0_A32R1.U_HDS_1"),
            read_csv("resources/hwc/rb/qhda", "0_A32R1.U_HDS_2"),
            read_csv("resources/hwc/rb/qhda", "0_A32R1.U_HDS_3"),
            read_csv("resources/hwc/rb/qhda", "0_A32R1.U_HDS_4"),
        ]
        i_hds_dfs = [
            read_csv("resources/hwc/rb/qhda", "0_A32R1.I_HDS_1"),
            read_csv("resources/hwc/rb/qhda", "0_A32R1.I_HDS_2"),
            read_csv("resources/hwc/rb/qhda", "0_A32R1.I_HDS_3"),
            read_csv("resources/hwc/rb/qhda", "0_A32R1.I_HDS_4"),
        ]

        # act
        feature_row = QuenchHeaterVoltageCurrentAnalysis(circuit_type="RB").calculate_qh_feature_row(
            u_hds_dfs, i_hds_dfs, timestamp, current_offset=0.085, mean_start_value=50
        )

        # assert
        feature_row_ref = pd.DataFrame(
            {
                "U_1_0": {1544235350692000000: 922.0},
                "U_1_end": {1544235350692000000: 35.0},
                "U_2_0": {1544235350692000000: 905.0},
                "U_2_end": {1544235350692000000: 31.0},
                "U_3_0": {1544235350692000000: 925.0},
                "U_3_end": {1544235350692000000: 33.0},
                "U_4_0": {1544235350692000000: 902.0},
                "U_4_end": {1544235350692000000: 33.0},
                "R_1_0": {1544235350692000000: 10.8},
                "R_2_0": {1544235350692000000: 10.52},
                "R_3_0": {1544235350692000000: 10.58},
                "R_4_0": {1544235350692000000: 10.51},
                "U_1_tau": {1544235350692000000: 0.089},
                "U_2_tau": {1544235350692000000: 0.086},
                "U_3_tau": {1544235350692000000: 0.087},
                "U_4_tau": {1544235350692000000: 0.088},
                "I_1_tau": {1544235350692000000: 0.079},
                "I_2_tau": {1544235350692000000: 0.076},
                "I_3_tau": {1544235350692000000: 0.077},
                "I_4_tau": {1544235350692000000: 0.079},
            }
        )

        pd.testing.assert_frame_equal(feature_row_ref, feature_row)

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_calc_plot_nqps_sunglass(self, mock_show=None):
        # arrange
        source_timestamp_nqps_df = pd.DataFrame(
            list(zip(["B31R1", "B31R1", "B11R1"], [1544631694812000000, 1544631694812000000, 1544631695406000000])),
            columns=["nqps_crate", "timestamp"],
        )
        u_nqps_dfs = [
            [
                read_csv("resources/hwc/rb/fpa", "0_MB.B30R1.U_DIODE_RB"),
                read_csv("resources/hwc/rb/fpa", "0_MB.A31R1.U_DIODE_RB"),
                read_csv("resources/hwc/rb/fpa", "0_MB.C31R1.U_DIODE_RB"),
                read_csv("resources/hwc/rb/fpa", "0_DQQDS.B31R1.RB.A12.U_REF_N1"),
            ],
            [
                read_csv("resources/hwc/rb/fpa", "1_MB.B30R1.U_DIODE_RB"),
                read_csv("resources/hwc/rb/fpa", "1_MB.A31R1.U_DIODE_RB"),
                read_csv("resources/hwc/rb/fpa", "1_MB.C31R1.U_DIODE_RB"),
                read_csv("resources/hwc/rb/fpa", "1_DQQDS.B31R1.RB.A12.U_REF_N1"),
            ],
            [
                read_csv("resources/hwc/rb/fpa", "2_MB.B9R1.U_DIODE_RB"),
                read_csv("resources/hwc/rb/fpa", "2_MB.B10R1.U_DIODE_RB"),
                read_csv("resources/hwc/rb/fpa", "2_MB.B11R1.U_DIODE_RB"),
                read_csv("resources/hwc/rb/fpa", "2_DQQDS.B11R1.RB.A12.U_REF_N1"),
            ],
            [
                read_csv("resources/hwc/rb/fpa", "3_MB.A30R1.U_DIODE_RB"),
                read_csv("resources/hwc/rb/fpa", "3_MB.C30R1.U_DIODE_RB"),
                read_csv("resources/hwc/rb/fpa", "3_MB.B31R1.U_DIODE_RB"),
                read_csv("resources/hwc/rb/fpa", "3_DQQDS.B30R1.RB.A12.U_REF_N1"),
            ],
        ]

        # act
        self.rb_analysis.calc_plot_nqps_sunglass(source_timestamp_nqps_df, u_nqps_dfs, threshold=0.7, scaling=1)

        # assert
        if mock_show is not None:
            mock_show.assert_called()

    def test_calc_min_max_iqps_u_qs0(self, mock_show=None):
        # arrange
        source_timestamp_qds_df = pd.DataFrame(
            list(
                zip(
                    ["A31R1", "C31R1", "B9R1", "B31R1", "A9R1"],
                    [
                        1544631694812000000,
                        1544631694812000000,
                        1544631695406000000,
                        1544631694812000000,
                        1544631694812000000,
                    ],
                )
            ),
            columns=["magnet", "timestamp_iqps"],
        )
        u_qds_dfs = [
            [read_csv("resources/hwc/rb/fpa", "0_A31R1.U_QS0")],
            [read_csv("resources/hwc/rb/fpa", "1_C31R1.U_QS0")],
            [read_csv("resources/hwc/rb/fpa", "2_B9R1.U_QS0")],
            [read_csv("resources/hwc/rb/fpa", "3_B31R1.U_QS0")],
            [read_csv("resources/hwc/rb/fpa", "4_A9R1.U_QS0")],
        ]

        # act
        self.rb_analysis.calc_min_max_iqps_u_qs0(u_qds_dfs, source_timestamp_qds_df, self.timestamp_fgc)

        # assert
        if mock_show is not None:
            mock_show.assert_called()

    def test_plot_u_diode_nxcals(self):
        expected_warning = "U_DIODE signals from NXCALS not plotted because the list of nQPS voltages is empty."
        with warnings.catch_warnings(record=True) as w:
            plot_u_diode_nxcals([], ax=mpl.pyplot.gca())

            self.assertEqual(str(w[0].message), expected_warning)
