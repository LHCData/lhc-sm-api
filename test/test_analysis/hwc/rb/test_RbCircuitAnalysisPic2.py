import unittest
from unittest.mock import patch

from lhcsmapi.analysis.RbCircuitAnalysis import RbCircuitAnalysis
from test.resources.read_csv import read_csv


class TestRbCircuitAnalysisPli3A5(unittest.TestCase):
    circuit_type = "RB"
    circuit_name = "RB.A45"
    hwc_test = "PIC2"
    t_start = "2021-01-26 17:31:08.349000000"
    t_end = "2021-01-26 17:52:06.187000000"

    @patch("matplotlib.pyplot.show")
    def test_analyze_char_time_u_dump_res_ee(self, mock_show=None):
        # arrange
        i_meas_df = read_csv("resources/hwc/rb/pic2", "I_MEAS")
        u_dump_res_odd_df = read_csv("resources/hwc/rb/pic2", "U_DUMP_RES_ODD")
        u_dump_res_even_df = read_csv("resources/hwc/rb/pic2", "U_DUMP_RES_EVEN")
        timestamp_fgc = 1611679146780000000

        # act
        RbCircuitAnalysis(self.circuit_type).analyze_char_time_u_dump_res_ee(
            self.circuit_name, timestamp_fgc, [u_dump_res_odd_df, u_dump_res_even_df], i_meas_df
        )

        if mock_show is not None:
            mock_show.assert_called()
