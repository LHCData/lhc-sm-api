import unittest
import warnings
from unittest.mock import patch
from io import StringIO

import pandas as pd
import pytest

from lhcsmapi.analysis.RbCircuitAnalysis import RbCircuitAnalysis
from lhcsmapi.pyedsl.PlotBuilder import create_hwc_plot_title_with_circuit_name, PlotBuilder
from test.resources.read_csv import read_csv


class TestRqCircuitAnalysisPli1B2(unittest.TestCase):
    t_start = "2018-03-16 18:55:57.270"
    t_end = "2018-03-16 19:07:00.286"
    timestamp_fgc = 1521223337520000000
    timestamp_fgc_ref = 1492786164780000000
    timestamp_pic = [1521223337485000000, 1521223337488000000]
    timestamp_ee_even = 1521223338077000000
    timestamp_ee_odd = 1521223337581000000
    source_timestamp_leads_odd_df = pd.DataFrame({"source": {0: "RB.A12"}, "timestamp": {0: 1521223337615000000}})
    source_timestamp_leads_even_df = pd.DataFrame()

    def test_create_timestamp_table(self):
        # arrange
        circuit_type = "RB"
        rb_analysis = RbCircuitAnalysis(circuit_type, results_table=None, is_automatic=True)

        # act
        timestamp_dct = {
            "FGC": self.timestamp_fgc,
            "PIC": min(self.timestamp_pic),
            "EE_EVEN": self.timestamp_ee_even,
            "EE_ODD": self.timestamp_ee_odd,
            "LEADS_ODD": self.source_timestamp_leads_odd_df,
            "LEADS_EVEN": self.source_timestamp_leads_even_df,
        }

        rb_analysis.create_timestamp_table(timestamp_dct)

    @patch("matplotlib.pyplot.show")
    def test_analyze_i_meas_pc(self, mock_show=None):
        # arrange
        circuit_type = "RB"
        circuit_name = "RB.A12"
        rb_analysis = RbCircuitAnalysis(circuit_type, results_table=None, is_automatic=True)
        i_meas_df = read_csv("resources/hwc/rb/pli1_b2", "I_MEAS_PM")
        i_meas_ref_df = read_csv("resources/hwc/rb/pli1_b2", "I_MEAS_REF_PM")

        # act
        rb_analysis.analyze_i_meas_pc(
            circuit_name, self.timestamp_fgc, self.timestamp_fgc_ref, min(self.timestamp_pic), i_meas_df, i_meas_ref_df
        )

        # assert
        if mock_show is not None:
            mock_show.assert_called()

    @patch("matplotlib.pyplot.show")
    def test_plot_power_converter_current(self, mock_show=None):
        # arrange
        circuit_type = "RB"
        circuit_name = "RB.A12"
        hwc_test = "PLI1.b2"

        i_meas_df = read_csv("resources/hwc/rb/pli1_b2", "I_MEAS_NXCALS")
        rb_analysis = RbCircuitAnalysis(circuit_type, results_table=None, is_automatic=True)

        # act
        title = create_hwc_plot_title_with_circuit_name(
            circuit_name=circuit_name, hwc_test=hwc_test, t_start=self.t_start, t_end=self.t_end, signal="I_MEAS"
        )
        rb_analysis.plot_i_meas(i_meas_df, title=title)

        if mock_show is not None:
            mock_show.assert_called()

    @patch("matplotlib.pyplot.show")
    def test_assert_i_meas_smoothness(self, mock_show=None):
        # arrange
        circuit_type = "RB"
        circuit_name = "RB.A12"
        hwc_test = "PLI1.b2"
        i_meas_df = read_csv("resources/hwc/rb/pli1_b2", "I_MEAS_PM")
        rb_analysis = RbCircuitAnalysis(circuit_type, results_table=None, is_automatic=True)

        # act
        title = create_hwc_plot_title_with_circuit_name(
            circuit_name=circuit_name,
            hwc_test=hwc_test,
            t_start=self.t_start,
            t_end=self.t_end,
            signal="I_MEAS smoothness",
        )

        rb_analysis.plot_i_meas_smoothness(i_meas_df, title=title)

        if mock_show is not None:
            mock_show.assert_called()

    @patch("matplotlib.pyplot.show")
    def test_assert_v_meas_zoom(self, mock_show=None):
        # arrange
        circuit_type = "RB"
        circuit_name = "RB.A12"
        hwc_test = "PLI1.b2"
        v_meas_df = read_csv("resources/hwc/rb/pli1_b2", "V_MEAS_PM")

        # act
        title = create_hwc_plot_title_with_circuit_name(
            circuit_name=circuit_name, hwc_test=hwc_test, t_start=self.t_start, t_end=self.t_end, signal="V_MEAS"
        )
        rb_analysis = RbCircuitAnalysis(circuit_type, results_table=None, is_automatic=True)
        with patch("sys.stdout", new=StringIO()) as fake_out:
            with warnings.catch_warnings(record=True) as w:
                rb_analysis.assert_v_meas(
                    self.timestamp_ee_even,
                    min(self.timestamp_pic),
                    t_after_ee=1,
                    v_meas_df=v_meas_df,
                    value_min=-2,
                    value_max=-1,
                    title=title,
                    xmax=5,
                )
                self.assertEqual("Analysis result", fake_out.getvalue().strip())
                self.assertEqual(
                    "STATUS.V_MEAS signal value at t = 1.592000 s  (-0.9 V) "
                    "is outside of the acceptance range [-2.0, -1.0] V.",
                    str(w[0].message),
                )

        if mock_show is not None:
            mock_show.assert_called()

    @patch("matplotlib.pyplot.show")
    def test_earth_current_analysis(self, mock_show=None):
        # arrange
        circuit_type = "RB"
        circuit_name = "RB.A12"
        i_earth_df = read_csv("resources/hwc/rb/pli1_b2", "I_EARTH_PM")
        rb_analysis = RbCircuitAnalysis(circuit_type, results_table=None, is_automatic=True)

        # act
        rb_analysis.plot_i_earth_pc(circuit_name, self.timestamp_fgc, i_earth_df)
        with patch("sys.stdout", new=StringIO()) as fake_out:
            rb_analysis.calculate_max_i_earth_pc(i_earth_df, col_name="Earth Current")
            self.assertEqual("The maximum unbiased abs(I_EARTH) is 8.64 mA.", fake_out.getvalue().strip())

        if mock_show is not None:
            mock_show.assert_called()

    @patch("matplotlib.pyplot.show")
    def test_analyze_char_time_u_dump_res_ee(self, mock_show=None):
        # arrange
        circuit_type = "RB"
        circuit_name = "RB.A12"
        rb_analysis = RbCircuitAnalysis(circuit_type, results_table=None, is_automatic=True)
        u_dump_res_odd_df = read_csv("resources/hwc/rb/pli1_b2", "U_DUMP_RES_ODD_PM")
        u_dump_res_even_df = read_csv("resources/hwc/rb/pli1_b2", "U_DUMP_RES_EVEN_PM")
        i_meas_df = read_csv("resources/hwc/rb/pli1_b2", "I_MEAS_PM")

        # act
        rb_analysis.analyze_char_time_u_dump_res_ee(
            circuit_name, self.timestamp_fgc, [u_dump_res_odd_df, u_dump_res_even_df], i_meas_df
        )

        if mock_show is not None:
            mock_show.assert_called()

    @patch("matplotlib.pyplot.show")
    def test_analyze_delay_time_u_dump_res_ee(self, mock_show=None):
        # arrange
        circuit_type = "RB"
        circuit_name = "RB.A12"
        rb_analysis = RbCircuitAnalysis(circuit_type, results_table=None, is_automatic=True)
        u_dump_res_odd_df = read_csv("resources/hwc/rb/pli1_b2", "U_DUMP_RES_ODD_PM")
        u_dump_res_even_df = read_csv("resources/hwc/rb/pli1_b2", "U_DUMP_RES_EVEN_PM")
        i_a_df = read_csv("resources/hwc/rb/pli1_b2", "I_A_PM")
        i_ref_df = read_csv("resources/hwc/rb/pli1_b2", "I_REF_PM")

        # act
        rb_analysis.analyze_delay_time_u_dump_res_ee(
            circuit_name,
            self.timestamp_fgc,
            min(self.timestamp_pic),
            [self.timestamp_ee_odd, self.timestamp_ee_even],
            i_a_df,
            i_ref_df,
            [u_dump_res_odd_df, u_dump_res_even_df],
        )

        if mock_show is not None:
            mock_show.assert_called()

    def test_compare_max_u_res_dump_to_reference_odd(self):
        # arrange
        circuit_type = "RB"
        rb_analysis = RbCircuitAnalysis(circuit_type, results_table=None, is_automatic=True)
        u_dump_res_odd_df = read_csv("resources/hwc/rb/pli1_b2", "U_DUMP_RES_ODD_PM")
        u_dump_res_odd_ref_df = read_csv("resources/hwc/rb/pli1_b2", "U_DUMP_RES_ODD_REF_PM")

        # act
        # assert
        with patch("sys.stdout", new=StringIO()) as fake_out:
            rb_analysis.compare_max_u_res_dump_to_reference(u_dump_res_odd_df, u_dump_res_odd_ref_df, "U_DUMP_RES_ODD")
            self.assertEqual(
                "Maximum U_DUMP_RES_ODD (52.1 V) is within of the 10% reference voltage [47.3, 57.8] V.",
                fake_out.getvalue().strip(),
            )

    def test_compare_max_u_res_dump_to_reference_even(self):
        # arrange
        circuit_type = "RB"
        rb_analysis = RbCircuitAnalysis(circuit_type, results_table=None, is_automatic=True)
        u_dump_res_even_df = read_csv("resources/hwc/rb/pli1_b2", "U_DUMP_RES_EVEN_PM")
        u_dump_res_even_ref_df = read_csv("resources/hwc/rb/pli1_b2", "U_DUMP_RES_EVEN_REF_PM")

        # act
        # assert
        with patch("sys.stdout", new=StringIO()) as fake_out:
            rb_analysis.compare_max_u_res_dump_to_reference(
                u_dump_res_even_df, u_dump_res_even_ref_df, "U_DUMP_RES_EVEN"
            )
            self.assertEqual(
                "Maximum U_DUMP_RES_EVEN (52.3 V) is within of the 10% reference voltage [47.6, 58.2] V.",
                fake_out.getvalue().strip(),
            )

    def test_compare_max_u_res_dump_to_reference_even_warning(self):
        # arrange
        circuit_type = "RB"
        rb_analysis = RbCircuitAnalysis(circuit_type, results_table=None, is_automatic=True)
        u_dump_res_even_df = 2 * read_csv("resources/hwc/rb/pli1_b2", "U_DUMP_RES_EVEN_PM")
        u_dump_res_even_ref_df = read_csv("resources/hwc/rb/pli1_b2", "U_DUMP_RES_EVEN_REF_PM")

        # act
        # assert
        with warnings.catch_warnings(record=True) as w:
            rb_analysis.compare_max_u_res_dump_to_reference(
                u_dump_res_even_df, u_dump_res_even_ref_df, "U_DUMP_RES_EVEN"
            )
            assert "Maximum U_DUMP_RES_EVEN (104.6 V) is outside of the 10% reference voltage [47.6, 58.2] V." in {
                str(warning.message) for warning in w
            }

    @patch("matplotlib.pyplot.show")
    def test_analyze_ee_temp_odd(self, mock_show=None):
        # arrange
        circuit_type = "RB"
        circuit_name = "RB.A12"
        rb_analysis = RbCircuitAnalysis(circuit_type, results_table=pd.DataFrame(), is_automatic=True)
        t_res_odd_0_df = [
            read_csv("resources/hwc/rb/pli1_b2", "T_RES_BODY_0_1_ODD"),
            read_csv("resources/hwc/rb/pli1_b2", "T_RES_BODY_0_2_ODD"),
            read_csv("resources/hwc/rb/pli1_b2", "T_RES_BODY_0_3_ODD"),
        ]
        t_res_odd_1_df = [
            read_csv("resources/hwc/rb/pli1_b2", "T_RES_BODY_1_1_ODD"),
            read_csv("resources/hwc/rb/pli1_b2", "T_RES_BODY_1_2_ODD"),
            read_csv("resources/hwc/rb/pli1_b2", "T_RES_BODY_1_3_ODD"),
        ]
        t_res_odd_0_ref_df = [
            read_csv("resources/hwc/rb/pli1_b2", "T_RES_BODY_0_1_ODD_REF"),
            read_csv("resources/hwc/rb/pli1_b2", "T_RES_BODY_0_2_ODD_REF"),
            read_csv("resources/hwc/rb/pli1_b2", "T_RES_BODY_0_3_ODD_REF"),
        ]
        t_res_odd_1_ref_df = [
            read_csv("resources/hwc/rb/pli1_b2", "T_RES_BODY_1_1_ODD_REF"),
            read_csv("resources/hwc/rb/pli1_b2", "T_RES_BODY_1_2_ODD_REF"),
            read_csv("resources/hwc/rb/pli1_b2", "T_RES_BODY_1_3_ODD_REF"),
        ]
        # act
        # assert
        rb_analysis.analyze_ee_temp(
            circuit_name + "_EE_ODD",
            self.timestamp_ee_odd,
            t_res_odd_0_df + t_res_odd_1_df,
            t_res_odd_0_ref_df + t_res_odd_1_ref_df,
            abs_margin=25,
            scaling=1,
        )

        if mock_show is not None:
            mock_show.assert_called()

    @patch("matplotlib.pyplot.show")
    def test_analyze_ee_temp_even(self, mock_show=None):
        # arrange
        circuit_type = "RB"
        circuit_name = "RB.A12"
        rb_analysis = RbCircuitAnalysis(circuit_type, results_table=None, is_automatic=True)
        t_res_even_0_df = [
            read_csv("resources/hwc/rb/pli1_b2", "T_RES_BODY_0_1_EVEN"),
            read_csv("resources/hwc/rb/pli1_b2", "T_RES_BODY_0_2_EVEN"),
            read_csv("resources/hwc/rb/pli1_b2", "T_RES_BODY_0_3_EVEN"),
        ]
        t_res_even_1_df = [
            read_csv("resources/hwc/rb/pli1_b2", "T_RES_BODY_1_1_EVEN"),
            read_csv("resources/hwc/rb/pli1_b2", "T_RES_BODY_1_2_EVEN"),
            read_csv("resources/hwc/rb/pli1_b2", "T_RES_BODY_1_3_EVEN"),
        ]
        t_res_even_0_ref_df = [
            read_csv("resources/hwc/rb/pli1_b2", "T_RES_BODY_0_1_EVEN_REF"),
            read_csv("resources/hwc/rb/pli1_b2", "T_RES_BODY_0_2_EVEN_REF"),
            read_csv("resources/hwc/rb/pli1_b2", "T_RES_BODY_0_3_EVEN_REF"),
        ]
        t_res_even_1_ref_df = [
            read_csv("resources/hwc/rb/pli1_b2", "T_RES_BODY_1_1_EVEN_REF"),
            read_csv("resources/hwc/rb/pli1_b2", "T_RES_BODY_1_2_EVEN_REF"),
            read_csv("resources/hwc/rb/pli1_b2", "T_RES_BODY_1_3_EVEN_REF"),
        ]
        # act
        # assert
        rb_analysis.analyze_ee_temp(
            circuit_name + "_EE_ODD",
            self.timestamp_ee_even,
            t_res_even_0_df + t_res_even_1_df,
            t_res_even_0_ref_df + t_res_even_1_ref_df,
            abs_margin=25,
            scaling=1,
        )

        if mock_show is not None:
            mock_show.assert_called()

    @patch("matplotlib.pyplot.show")
    def test_analyze_leads_voltage_u_hts_odd(self, mock_show=None):
        # arrange
        circuit_type = "RB"
        circuit_name = "RB.A12"
        rb_analysis = RbCircuitAnalysis(circuit_type, results_table=None, is_automatic=True)
        u_hts_odd_dfs = [
            read_csv("resources/hwc/rb/pli1_b2", "DFLAS.7R1.RB.A12.LD3.U_HTS"),
            read_csv("resources/hwc/rb/pli1_b2", "DFLAS.7R1.RB.A12.LD4.U_HTS"),
        ]

        # act
        timestamp = (
            self.t_start
            if self.source_timestamp_leads_odd_df.empty
            else self.source_timestamp_leads_odd_df.loc[0, "timestamp"]
        )

        with warnings.catch_warnings(record=True) as w:
            rb_analysis.analyze_leads_voltage(
                u_hts_odd_dfs, circuit_name, timestamp, signal="U_HTS", value_min=-0.003, value_max=0.003
            )
            self.assertEqual("DFLAS.7R1.RB.A12.LD3:U_HTS outside of the [-0.003, 0.003] threshold", str(w[0].message))

        if mock_show is not None:
            mock_show.assert_called()

    @patch("matplotlib.pyplot.show")
    def test_analyze_leads_voltage_u_hts_even(self, mock_show=None):
        # arrange
        circuit_type = "RB"
        circuit_name = "RB.A12"
        rb_analysis = RbCircuitAnalysis(circuit_type, results_table=None, is_automatic=True)
        u_hts_even_dfs = [
            read_csv("resources/hwc/rb/pli1_b2", "DFLAS.7L2.RB.A12.LD1.U_HTS"),
            read_csv("resources/hwc/rb/pli1_b2", "DFLAS.7L2.RB.A12.LD2.U_HTS"),
        ]

        # act
        timestamp = (
            self.t_start
            if self.source_timestamp_leads_odd_df.empty
            else self.source_timestamp_leads_odd_df.loc[0, "timestamp"]
        )

        rb_analysis.analyze_leads_voltage(
            u_hts_even_dfs, circuit_name, timestamp, signal="U_HTS", value_min=-0.003, value_max=0.003
        )

        if mock_show is not None:
            mock_show.assert_called()

    @patch("matplotlib.pyplot.show")
    def test_analyze_leads_voltage_u_res_odd(self, mock_show=None):
        # arrange
        circuit_type = "RB"
        circuit_name = "RB.A12"
        rb_analysis = RbCircuitAnalysis(circuit_type, results_table=None, is_automatic=True)
        u_res_odd_dfs = [
            read_csv("resources/hwc/rb/pli1_b2", "DFLAS.7R1.RB.A12.LD3.U_RES"),
            read_csv("resources/hwc/rb/pli1_b2", "DFLAS.7R1.RB.A12.LD4.U_RES"),
        ]

        # act
        timestamp = (
            self.t_start
            if self.source_timestamp_leads_odd_df.empty
            else self.source_timestamp_leads_odd_df.loc[0, "timestamp"]
        )

        rb_analysis.analyze_leads_voltage(
            u_res_odd_dfs, circuit_name, timestamp, signal="U_HTS", value_min=-0.003, value_max=0.003
        )

        if mock_show is not None:
            mock_show.assert_called()

    @patch("matplotlib.pyplot.show")
    def test_analyze_leads_voltage_u_res_even(self, mock_show=None):
        # arrange
        circuit_type = "RB"
        circuit_name = "RB.A12"
        rb_analysis = RbCircuitAnalysis(circuit_type, results_table=None, is_automatic=True)
        u_res_even_dfs = [
            read_csv("resources/hwc/rb/pli1_b2", "DFLAS.7L2.RB.A12.LD1.U_RES"),
            read_csv("resources/hwc/rb/pli1_b2", "DFLAS.7L2.RB.A12.LD2.U_RES"),
        ]

        # act
        timestamp = (
            self.t_start
            if self.source_timestamp_leads_odd_df.empty
            else self.source_timestamp_leads_odd_df.loc[0, "timestamp"]
        )

        with pytest.warns(
            UserWarning, match=r"DFLAS\.7L2\.RB\.A12\.LD\d:U_RES outside of the \[-0.003, 0.003\] threshold"
        ):
            rb_analysis.analyze_leads_voltage(
                u_res_even_dfs, circuit_name, timestamp, signal="U_HTS", value_min=-0.003, value_max=0.003
            )

        if mock_show is not None:
            mock_show.assert_called()
