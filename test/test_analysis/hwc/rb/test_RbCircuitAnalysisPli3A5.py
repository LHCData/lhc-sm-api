import unittest
from unittest.mock import patch

from lhcsmapi.Time import Time
from lhcsmapi.analysis.RbCircuitAnalysis import RbCircuitAnalysis
from test.resources.read_csv import read_csv


class TestRbCircuitAnalysisPli3A5(unittest.TestCase):
    circuit_type = "RB"
    circuit_name = "RB.A12"
    hwc_test = "PLI3.a5"
    t_start = "2017-04-22 08:57:30.399"
    t_end = "2017-04-22 11:32:09.824"

    @patch("matplotlib.pyplot.show")
    def test_assert_cv891_min_max_variation(self, mock_show=None):
        # arrange
        cv891_nxcals_dfs = [
            read_csv("resources/hwc/rb/pli3_a5", "DABA01_07R1_CV891.POSST"),
            read_csv("resources/hwc/rb/pli3_a5", "DABA02_07R1_CV891.POSST"),
            read_csv("resources/hwc/rb/pli3_a5", "DACA05_07L2_CV891.POSST"),
            read_csv("resources/hwc/rb/pli3_a5", "DACA06_07L2_CV891.POSST"),
        ]
        plateau_start = []
        plateau_end = []

        # act
        RbCircuitAnalysis.assert_cv891_min_max_variation(
            cv891_nxcals_dfs, 8, plateau_start, plateau_end, Time.to_unix_timestamp(self.t_start)
        )

        if mock_show is not None:
            mock_show.assert_called()

    @patch("matplotlib.pyplot.show")
    def test_assert_cv891_min_max_value(self, mock_show=None):
        # arrange
        cv891_nxcals_dfs = [
            read_csv("resources/hwc/rb/pli3_a5", "DABA01_07R1_CV891.POSST"),
            read_csv("resources/hwc/rb/pli3_a5", "DABA02_07R1_CV891.POSST"),
            read_csv("resources/hwc/rb/pli3_a5", "DACA05_07L2_CV891.POSST"),
            read_csv("resources/hwc/rb/pli3_a5", "DACA06_07L2_CV891.POSST"),
        ]

        # act
        RbCircuitAnalysis.assert_cv891_min_max_value(cv891_nxcals_dfs, value_range=(2, 50))

        if mock_show is not None:
            mock_show.assert_called()
