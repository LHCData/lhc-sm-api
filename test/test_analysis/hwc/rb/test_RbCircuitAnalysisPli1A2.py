import unittest
import warnings
from unittest.mock import patch
from io import StringIO

from lhcsmapi.analysis.RbCircuitAnalysis import RbCircuitAnalysis
from lhcsmapi.pyedsl.PlotBuilder import create_hwc_plot_title_with_circuit_name
from test.resources.read_csv import read_csv


class TestRqCircuitAnalysisPli1A2(unittest.TestCase):
    @patch("matplotlib.pyplot.show")
    def test_plot_power_converter_current(self, mock_show=None):
        """
        t_start = Time.to_unix_timestamp('2017-04-21 15:42:31.569')
        t_end = Time.to_unix_timestamp('2017-04-21 16:01:35.843')
        """

        # arrange
        circuit_type = "RB"
        circuit_name = "RB.A12"
        hwc_test = "PLI1.a2"
        t_start = "2017-04-21 15:42:31.569"
        t_end = "2017-04-21 16:01:35.843"

        i_meas_df = read_csv("resources/hwc/rb/pli1_a2", "I_MEAS")
        rb_analysis = RbCircuitAnalysis(circuit_type, results_table=None, is_automatic=True)

        # act
        title = create_hwc_plot_title_with_circuit_name(
            circuit_name=circuit_name, hwc_test=hwc_test, t_start=t_start, t_end=t_end, signal="I_MEAS"
        )
        rb_analysis.plot_i_meas(i_meas_df, title=title)

        if mock_show is not None:
            mock_show.assert_called()

    @patch("matplotlib.pyplot.show")
    def test_plot_power_converter_current_with_current_plateau(self, mock_show=None):
        """
        t_start = Time.to_unix_timestamp('2017-04-21 15:42:31.569')
        t_end = Time.to_unix_timestamp('2017-04-21 16:01:35.843')
        """
        # arrange
        circuit_type = "RB"
        circuit_name = "RB.A12"
        hwc_test = "PLI1.a2"
        t_start = "2017-04-21 15:42:31.569"
        t_end = "2017-04-21 16:01:35.843"
        i_meas_df = read_csv("resources/hwc/rb/pli1_a2", "I_MEAS")
        i_meas_raw_df = read_csv("resources/hwc/rb/pli1_a2", "I_MEAS_RAW")
        rb_analysis = RbCircuitAnalysis(circuit_type, results_table=None, is_automatic=True)

        # act
        plateau_start, plateau_end = rb_analysis.find_plateau_start_and_end(
            i_meas_raw_df, i_meas_threshold=0, min_duration_in_sec=60, time_shift_in_sec=(20, 1)
        )

        plateau_start_ref = [1492782319733004544, 1492782457856771328]
        plateau_end_ref = [1492782358786816256, 1492783036393990912]

        self.assertListEqual(plateau_start_ref, plateau_start)
        self.assertListEqual(plateau_end_ref, plateau_end)

        title = create_hwc_plot_title_with_circuit_name(
            circuit_name=circuit_name, hwc_test=hwc_test, t_start=t_start, t_end=t_end, signal="I_MEAS"
        )

        rb_analysis.plot_i_meas_with_current_plateau(
            i_meas_df, plateau_start=plateau_start, plateau_end=plateau_end, t0=i_meas_raw_df.index[0], title=title
        )

        if mock_show is not None:
            mock_show.assert_called()

    @patch("matplotlib.pyplot.show")
    def test_analyze_busbar_magnet_voltage(self, mock_show=None):
        # arrange
        circuit_type = "RB"
        circuit_name = "RB.A12"
        hwc_test = "PLI1.a2"
        t_start = "2017-04-21 15:42:31.569"
        t_end = "2017-04-21 16:01:35.843"
        u_res_df = read_csv("resources/hwc/rb/pli1_a2", "U_RES")
        rb_analysis = RbCircuitAnalysis(circuit_type, results_table=None, is_automatic=True)

        # act
        title = create_hwc_plot_title_with_circuit_name(
            circuit_name=circuit_name, hwc_test=hwc_test, t_start=t_start, t_end=t_end, signal="U_RES_abs_max"
        )
        rb_analysis.analyze_busbar_magnet_voltage(
            u_res_df, value_max=5e-3, title=title, ylabel="U_RES_abs_max (Calculated), [V]"
        )

        if mock_show is not None:
            mock_show.assert_called()

    def test_count_busbar_signal(self):
        # arrange
        circuit_type = "RB"
        u_res_df = read_csv("resources/hwc/rb/pli1_a2", "U_RES")
        rb_analysis = RbCircuitAnalysis(circuit_type, results_table=None, is_automatic=True)

        expected_signal_count = 156
        # act
        # assert
        with patch("sys.stdout", new=StringIO()) as fake_out:
            rb_analysis.check_signal_count(u_res_df, expected_signal_count, signal="U_RES")
            self.assertEqual("All (156) U_RES signals were found.", fake_out.getvalue().strip())

    def test_count_magnet_signal_all_signals(self):
        # arrange
        circuit_type = "RB"
        u_mag_df = read_csv("resources/hwc/rb/pli1_a2", "U_MAG")
        rb_analysis = RbCircuitAnalysis(circuit_type, results_table=None, is_automatic=True)

        expected_signal_count = 156
        # act
        # assert
        with patch("sys.stdout", new=StringIO()) as fake_out:
            rb_analysis.check_signal_count(u_mag_df, expected_signal_count, signal="U_MAG")
            self.assertEqual("All (156) U_MAG signals were found.", fake_out.getvalue().strip())

    def test_count_magnet_signal_missing_signals(self):
        # arrange
        circuit_type = "RB"
        u_mag_df = read_csv("resources/hwc/rb/pli1_a2", "U_MAG").head(155)
        rb_analysis = RbCircuitAnalysis(circuit_type, results_table=None, is_automatic=True)

        expected_signal_count = 156
        # act
        # assert
        with warnings.catch_warnings(record=True) as w:
            rb_analysis.check_signal_count(u_mag_df, expected_signal_count, signal="U_MAG")
            self.assertEqual("There were missing U_MAG signals. Found 155 signals, expected 156.", str(w[0].message))
