from importlib.metadata import version
import unittest
from unittest.mock import patch

import numpy as np
import pandas as pd

from lhcsmapi import reference
from lhcsmapi.analysis.RbCircuitQuery import RbCircuitQuery
from test.resources.read_csv import read_csv


class RbCircuitAnalysisTest(unittest.TestCase):
    def setUp(self):
        self.circuit_type = "RB"
        self.circuit_name = "RB.A12"
        self.timestamp_fgc = 1544631694840000000
        self.timestamp_pic = [1544631694840000000, 1544631694840000000]
        self.author = "lhcsm"

    def test_create_report_analysis_template(self):
        # arrange
        i_meas_df = read_csv("resources/hwc/rb/fpa", "i_meas_df")

        qds_df = pd.DataFrame(
            {
                "source": {
                    0: "A31R1",
                    1: "C31R1",
                    2: "B9R1",
                    3: "B31R1",
                    4: "A9R1",
                    5: "C30R1",
                    6: "B30R1",
                    7: "A10R1",
                    8: "A8R1",
                },
                "timestamp": {
                    0: 1544631694792000000,
                    1: 1544631695256000000,
                    2: 1544631695370000000,
                    3: 1544631727546000000,
                    4: 1544631741521000000,
                    5: 1544631744629000000,
                    6: 1544631804111000000,
                    7: 1544632025612000000,
                    8: 1544632025711000000,
                },
            }
        )

        nqps_df = pd.DataFrame(
            {
                "source": {
                    0: "B31R1",
                    1: "B9R1",
                    2: "B11R1",
                    3: "B11L2",
                    4: "B12R1",
                    5: "B15R1",
                    6: "B30R1",
                    7: "B22L2",
                    8: "B16R1",
                    9: "B33L2",
                    10: "B27R1",
                    11: "B13R1",
                    12: "B10L2",
                    13: "B23R1",
                    14: "B19L2",
                    15: "B25R1",
                    16: "B32R1",
                    17: "B21L2",
                    18: "B33R1",
                    19: "B31L2",
                    20: "B12L2",
                    21: "B20R1",
                    22: "B16L2",
                    23: "B17R1",
                    24: "B18L2",
                    25: "B26L2",
                    26: "B20L2",
                    27: "B21R1",
                    28: "B24L2",
                    29: "B24R1",
                    30: "B32L2",
                    31: "B34L2",
                    32: "B14R1",
                    33: "B29L2",
                    34: "B14L2",
                    35: "B23L2",
                    36: "B18R1",
                    37: "B28R1",
                    38: "B17L2",
                    39: "B9L2",
                    40: "B10R1",
                    41: "B19R1",
                    42: "B22R1",
                    43: "B13L2",
                    44: "B29R1",
                    45: "B26R1",
                    46: "B28L2",
                    47: "B25L2",
                    48: "B34R1",
                    49: "B30L2",
                    50: "B15L2",
                    51: "B8L2",
                    52: "B27L2",
                    53: "B8R1",
                },
                "timestamp": {
                    0: 1544631694812000000,
                    1: 1544631695406000000,
                    2: 1544631695406000000,
                    3: 1544631698229000000,
                    4: 1544631698254000000,
                    5: 1544631698418000000,
                    6: 1544631698501000000,
                    7: 1544631698515000000,
                    8: 1544631698523000000,
                    9: 1544631698539000000,
                    10: 1544631698611000000,
                    11: 1544631698618000000,
                    12: 1544631698625000000,
                    13: 1544631698636000000,
                    14: 1544631698658000000,
                    15: 1544631698672000000,
                    16: 1544631698711000000,
                    17: 1544631698720000000,
                    18: 1544631698753000000,
                    19: 1544631698766000000,
                    20: 1544631698842000000,
                    21: 1544631698844000000,
                    22: 1544631698877000000,
                    23: 1544631698892000000,
                    24: 1544631698900000000,
                    25: 1544631698942000000,
                    26: 1544631698943000000,
                    27: 1544631698944000000,
                    28: 1544631698964000000,
                    29: 1544631698971000000,
                    30: 1544631698980000000,
                    31: 1544631698984000000,
                    32: 1544631698989000000,
                    33: 1544631698993000000,
                    34: 1544631698994000000,
                    35: 1544631698995000000,
                    36: 1544631699000000000,
                    37: 1544631699008000000,
                    38: 1544631699013000000,
                    39: 1544631699013000000,
                    40: 1544631699015000000,
                    41: 1544631699032000000,
                    42: 1544631699033000000,
                    43: 1544631699048000000,
                    44: 1544631699058000000,
                    45: 1544631699059000000,
                    46: 1544631699063000000,
                    47: 1544631699086000000,
                    48: 1544631699094000000,
                    49: 1544631699100000000,
                    50: 1544631699113000000,
                    51: 1544631699119000000,
                    52: 1544631699175000000,
                    53: 1544631699291000000,
                },
            }
        )

        # act
        rb_query = RbCircuitQuery(self.circuit_type, self.circuit_name)
        report_analysis_act = rb_query.create_report_analysis_template(
            qds_df, nqps_df, min(self.timestamp_pic), self.timestamp_fgc, i_meas_df, self.author
        )
        report_analysis_act["lhcsmapi version"] = "1.4.35"

        # assert
        report_analysis_act.fillna(np.nan, inplace=True)

        report_analysis_ref = read_csv("resources/hwc/rb/fpa", "report_analysis_ref")
        report_analysis_ref.fillna(np.nan, inplace=True)

        pd.testing.assert_frame_equal(report_analysis_ref, report_analysis_act)

    def test_create_report_analysis_template_with_single_row_dataframes(self):
        # arrange
        i_meas_df = pd.DataFrame()
        qds_df = pd.DataFrame()
        nqps_df = pd.DataFrame()

        # act
        rb_query = RbCircuitQuery(self.circuit_type, self.circuit_name)
        report_analysis_act = rb_query.create_report_analysis_template(
            qds_df, nqps_df, min(self.timestamp_pic), self.timestamp_fgc, i_meas_df, self.author
        )

        # assert
        columns = reference.get_quench_database_columns(circuit_type="RB", table_type="MP3")
        values = [
            ["RB.A12", "RB", "HWC 2018-2", "2018-12-12", "17:21:34.840"]
            + [np.nan] * 38
            + ["lhcsm", version("lhcsmapi")]
        ]
        report_analysis_ref = pd.DataFrame(values, columns=columns, dtype=object)
        pd.testing.assert_frame_equal(report_analysis_ref, report_analysis_act)
