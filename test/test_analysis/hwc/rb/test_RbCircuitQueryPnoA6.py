import unittest
from unittest.mock import patch
import pandas as pd

from lhcsmapi.Time import Time
from lhcsmapi.analysis.RbCircuitAnalysis import RbCircuitAnalysis
from test.resources.read_csv import read_csv
import matplotlib.pyplot as plt


class TestRqCircuitQueryPNOA6(unittest.TestCase):
    @patch("matplotlib.pyplot.show")
    def test_find_plateau_start_end(self, mock_show=None):
        """
            t_start = Time.to_unix_timestamp('2017-04-25 02:36:19.179000+02:00')
            t_end = Time.to_unix_timestamp('2017-04-25 06:53:07.889000+02:00')
        :param mock_show:
        :return:
        """
        # arrange
        circuit_type = "RB"

        i_meas_nxcals_df = read_csv("resources/hwc/rb/pno_a6", "I_MEAS")

        i_meas_raw_nxcals_df = read_csv("resources/hwc/rb/pno_a6", "I_MEAS_RAW")

        rb_analysis = RbCircuitAnalysis(circuit_type, pd.DataFrame(), is_automatic=True)
        # act
        plateau_start, plateau_end = rb_analysis.find_plateau_start_and_end(
            i_meas_raw_nxcals_df, i_meas_threshold=0, min_duration_in_sec=360, time_shift_in_sec=(240, 60)
        )

        plateau_start_ref = [1493081088635263232, 1493083724807202304, 1493087535055718400]
        plateau_end_ref = [1493082587752607232, 1493087024041955584, 1493094433525224704]

        self.assertListEqual(plateau_start_ref, plateau_start)
        self.assertListEqual(plateau_end_ref, plateau_end)

        ax = i_meas_nxcals_df.plot(figsize=(30, 10))
        for ps, pe in zip(plateau_start, plateau_end):
            ax.axvspan(
                xmin=(ps - Time.to_unix_timestamp(i_meas_raw_nxcals_df.index[0])) / 1e9,
                xmax=(pe - Time.to_unix_timestamp(i_meas_raw_nxcals_df.index[0])) / 1e9,
                facecolor="xkcd:goldenrod",
            )
        ax.set_xlabel("time, [s]", fontsize=15)
        ax.set_ylabel("I_MEAS, [A]", fontsize=15)
        ax.tick_params(labelsize=15)
        plt.show()

        if mock_show is not None:
            mock_show.assert_called()

    @patch("matplotlib.pyplot.show")
    def test_assert_u_hts_min_max_slope(self, mock_show=None):
        # arrange
        u_hts_nxcals_dfs = [
            read_csv("resources/hwc/rb/pno_a6", "DFLAS.7L2.RB.A12.LD1.U_HTS"),
            read_csv("resources/hwc/rb/pno_a6", "DFLAS.7L2.RB.A12.LD2.U_HTS"),
            read_csv("resources/hwc/rb/pno_a6", "DFLAS.7R1.RB.A12.LD3.U_HTS"),
            read_csv("resources/hwc/rb/pno_a6", "DFLAS.7R1.RB.A12.LD4.U_HTS"),
        ]
        plateau_start = [
            1418407939021545728,
            1418409926207681536,
            1418411888391475456,
            1418413801570679040,
            1418415763754472960,
        ]
        plateau_end = [
            1418409439190163968,
            1418411425376206080,
            1418413387560000000,
            1418415300739203840,
            1418417262922997760,
        ]
        t_start = "2014-12-12 19:04:12.478"

        # act
        # assert
        RbCircuitAnalysis.assert_u_hts_min_max_slope(
            u_hts_nxcals_dfs, plateau_start, plateau_end, Time.to_unix_timestamp(t_start), slope_range=(-0.5, 0.5)
        )

        if mock_show is not None:
            mock_show.assert_called()

    @patch("matplotlib.pyplot.show")
    def test_assert_u_res_between_two_references(self, mock_show=None):
        # arrange
        i_meas_nxcals_df = read_csv("resources/hwc/rb/pno_a6", "I_MEAS")
        u_res_nxcals_dfs = [
            read_csv("resources/hwc/rb/pno_a6", "DFLAS.7L2.RB.A12.LD1.U_RES"),
            read_csv("resources/hwc/rb/pno_a6", "DFLAS.7L2.RB.A12.LD2.U_RES"),
            read_csv("resources/hwc/rb/pno_a6", "DFLAS.7R1.RB.A12.LD3.U_RES"),
            read_csv("resources/hwc/rb/pno_a6", "DFLAS.7R1.RB.A12.LD4.U_RES"),
        ]

        # act
        # assert
        RbCircuitAnalysis.assert_u_res_between_two_references(
            u_res_nxcals_dfs,
            first_ref=i_meas_nxcals_df,
            first_scaling=0.065 / 11000,
            second_ref=i_meas_nxcals_df,
            second_scaling=-0.065 / 11000,
        )

        if mock_show is not None:
            mock_show.assert_called()

    @patch("matplotlib.pyplot.show")
    def test_assert_u_res_between_two_references_with_median_filter(self, mock_show=None):
        # arrange
        i_meas_nxcals_df = read_csv("resources/hwc/rb/pno_a6", "I_MEAS")
        u_res_nxcals_dfs = [
            read_csv("resources/hwc/rb/pno_a6", "DFLAS.7L2.RB.A12.LD1.U_RES"),
            read_csv("resources/hwc/rb/pno_a6", "DFLAS.7L2.RB.A12.LD2.U_RES"),
            read_csv("resources/hwc/rb/pno_a6", "DFLAS.7R1.RB.A12.LD3.U_RES"),
            read_csv("resources/hwc/rb/pno_a6", "DFLAS.7R1.RB.A12.LD4.U_RES"),
        ]

        # act
        # assert
        RbCircuitAnalysis.assert_u_res_between_two_references(
            u_res_nxcals_dfs,
            first_ref=i_meas_nxcals_df,
            first_scaling=0.065 / 11000,
            second_ref=i_meas_nxcals_df,
            second_scaling=-0.065 / 11000,
            median_filter=True,
        )

        if mock_show is not None:
            mock_show.assert_called()
