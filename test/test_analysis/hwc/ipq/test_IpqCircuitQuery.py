import unittest
from unittest.mock import patch

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

from lhcsmapi.analysis.IpqCircuitQuery import IpqCircuitQuery
from lhcsmapi.analysis.busbar.BusbarResistanceAnalysis import BusbarResistanceAnalysis

from test.resources.read_csv import read_csv


class TestIpqCircuitQuery(unittest.TestCase):
    def test_constructor_ipq2(self):
        # arrange
        # act
        query = IpqCircuitQuery("IPQ", "RQ5.L1")

        # assert
        self.assertEqual("IPQ2", query.circuit_type)

    def test_constructor_ipq4(self):
        # arrange
        # act
        query = IpqCircuitQuery("IPQ", "RQ4.L1")

        # assert
        self.assertEqual("IPQ4", query.circuit_type)

    def test_constructor_ipq8(self):
        # arrange
        # act
        query = IpqCircuitQuery("IPQ", "RQ4.L2")

        # assert
        self.assertEqual("IPQ8", query.circuit_type)

    def test_create_report_analysis_template(self):
        # arrange
        query = IpqCircuitQuery("IPQ", "RQ4.L2")

        # act
        results_act = query.create_report_analysis_template(1544455364980000000, "")

        results_act["lhcsmapi version"] = "1.3.226"

        # assert
        results_ref = pd.DataFrame(
            {
                "Circuit Name": {0: "RQ4.L2"},
                "Circuit Family": {0: "RQ4"},
                "Period": {0: "HWC 2018-2"},
                "Date (FGC_Bn)": {0: "2018-12-10"},
                "Time (FGC_Bn)": {0: "16:22:44.980"},
                "FPA Reason": {0: np.nan},
                "Timestamp_PIC": {0: np.nan},
                "Delta_t(FGC-PIC)": {0: np.nan},
                "Ramp rate B1": {0: np.nan},
                "Ramp rate B2": {0: np.nan},
                "Plateau duration B1": {0: np.nan},
                "Plateau duration B2": {0: np.nan},
                "I_Q_B1": {0: np.nan},
                "I_Q_B2": {0: np.nan},
                "MIITS_B1": {0: np.nan},
                "MIITS_B2": {0: np.nan},
                "I_Earth_max_B1": {0: np.nan},
                "I_Earth_max_B2": {0: np.nan},
                "Delta_t(QPS-PIC)": {0: np.nan},
                "Type of Quench": {0: np.nan},
                "Quench origin": {0: np.nan},
                "Quench count": {0: np.nan},
                "QDS trigger origin": {0: np.nan},
                "dU_QPS_B1/dt": {0: np.nan},
                "dU_QPS_B2/dt": {0: np.nan},
                "QH analysis": {0: np.nan},
                "Comment": {0: np.nan},
                "Analysis performed by": {0: ""},
                "lhcsmapi version": {0: "1.3.226"},
            }
        )

        results_act.fillna(np.nan, inplace=True)
        pd.testing.assert_frame_equal(results_ref, results_act)

    # HWC PLI2.e3
    # t_start = '2015-01-27 21:40:53.757'
    # t_end = '2015-01-27 22:06:45.121'
    @patch("matplotlib.pyplot.show")
    def test_calculate_current_plateau_start_end_pli2_e3_b1(self, mock_show=None):
        # arrange
        i_meas_raw_df = read_csv("resources/hwc/ipq/pli2_e3", "I_MEAS_B1_RAW")

        # act
        plateau_start, plateau_end = BusbarResistanceAnalysis.find_plateau_start_and_end(
            i_meas_raw_df, i_meas_threshold=0, min_duration_in_sec=5
        )

        ax = i_meas_raw_df.plot()

        for ps, pe in zip(plateau_start, plateau_end):
            ax.axvspan(xmin=ps, xmax=pe, facecolor="xkcd:orange")
        plt.show()

        # assert
        plateau_start_ref = [1422391296509427712, 1422391376563299584, 1422391850882491648]
        plateau_end_ref = [1422391356549831680, 1422391675764646400, 1422392536343771136]
        self.assertListEqual(plateau_start_ref, plateau_start)
        self.assertListEqual(plateau_end_ref, plateau_end)

        if mock_show is not None:
            mock_show.assert_called()

    @patch("matplotlib.pyplot.show")
    def test_calculate_current_plateau_start_end_pli2_e3_b2(self, mock_show=None):
        # arrange
        i_meas_raw_df = read_csv("resources/hwc/ipq/pli2_e3", "I_MEAS_B2_RAW")

        # act
        plateau_start, plateau_end = BusbarResistanceAnalysis.find_plateau_start_and_end(
            i_meas_raw_df, i_meas_threshold=0, min_duration_in_sec=5
        )

        ax = i_meas_raw_df.plot()

        for ps, pe in zip(plateau_start, plateau_end):
            ax.axvspan(xmin=ps, xmax=pe, facecolor="xkcd:orange")
        plt.show()

        # assert
        plateau_start_ref = [1422391292009383424, 1422391376065683712, 1422391850383377920, 1422392235641420800]
        plateau_end_ref = [1422391358053619200, 1422391676266756096, 1422392149583780096, 1422392534841822976]
        self.assertListEqual(plateau_start_ref, plateau_start)
        self.assertListEqual(plateau_end_ref, plateau_end)

        if mock_show is not None:
            mock_show.assert_called()
