import unittest
import warnings
from io import StringIO

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

from unittest.mock import patch

from lhcsmapi.Time import Time
from lhcsmapi.analysis.IpqCircuitAnalysis import IpqCircuitAnalysis
from lhcsmapi.analysis.IpqCircuitQuery import IpqCircuitQuery
from lhcsmapi.pyedsl.PlotBuilder import PlotBuilder

from test.resources.read_csv import read_csv


class TestIpqCircuitAnalysis(unittest.TestCase):
    circuit_name = "RQ4.L2"
    timestamp_fgc_b1 = 1544455362980000000
    timestamp_fgc_b2 = 1544455362980000000
    timestamp_pic = 1544455364958000000
    source_timestamp_qds_df = pd.DataFrame({"source": {0: "RQ4.L2"}, "timestamp": {0: 1544455364959000000}})
    timestamp_qds = 1544455364959000000

    results_table = IpqCircuitQuery("IPQ", "RQ4.L2").create_report_analysis_template(
        min(timestamp_fgc_b1, timestamp_fgc_b2), ""
    )

    results_table["timestamp_pic"] = timestamp_pic
    results_table["timestamp_fgc_b1"] = timestamp_fgc_b1
    results_table["timestamp_fgc_b2"] = timestamp_fgc_b2
    results_table["timestamp_qds_a"] = timestamp_qds

    def test_constructor_ipq2(self):
        # arrange
        # act
        query = IpqCircuitAnalysis("IPQ", None, circuit_name="RQ5.L1")

        # assert
        self.assertEqual("IPQ2", query.circuit_type)

    def test_constructor_ipq4(self):
        # arrange
        # act
        query = IpqCircuitAnalysis("IPQ", None, circuit_name="RQ4.L1")

        # assert
        self.assertEqual("IPQ4", query.circuit_type)

    def test_constructor_ipq8(self):
        # arrange
        # act
        query = IpqCircuitAnalysis("IPQ", None, circuit_name="RQ4.L2")

        # assert
        self.assertEqual("IPQ8", query.circuit_type)

    def test_create_timestamp_table(self):
        # arrange
        ipq_analysis = IpqCircuitAnalysis(
            circuit_type="IPQ", results_table=self.results_table, is_automatic=True, circuit_name=self.circuit_name
        )

        timestamp_dct = {
            "FGC_B1": self.timestamp_fgc_b1,
            "FGC_B2": self.timestamp_fgc_b2,
            "PIC": self.timestamp_pic,
            "QDS_A": (
                self.source_timestamp_qds_df.loc[0, "timestamp"] if len(self.source_timestamp_qds_df) > 0 else np.nan
            ),
            "QDS_B": (
                self.source_timestamp_qds_df.loc[1, "timestamp"] if len(self.source_timestamp_qds_df) > 1 else np.nan
            ),
        }

        # act
        # assert
        with warnings.catch_warnings(record=True) as w:
            ipq_analysis.create_timestamp_table(timestamp_dct)

            self.assertEqual("FGC is the first timestamp, potentially a PC trip not a quench!", str(w[0].message))
            self.assertEqual("QDS Board B not present in PM buffer!", str(w[1].message))
            self.assertEqual("QDS_A and FGC_B1 PM events are not synchronized to +/- 40 ms!", str(w[2].message))
            self.assertEqual("QDS_A and FGC_B2 PM events are not synchronized to +/- 40 ms!", str(w[3].message))

    def test_create_timestamp_table_fgc_b1_missing(self):
        # arrange
        ipq_analysis = IpqCircuitAnalysis(
            circuit_type="IPQ", results_table=self.results_table, is_automatic=True, circuit_name=self.circuit_name
        )

        timestamp_dct = {
            "FGC_B2": self.timestamp_fgc_b2,
            "PIC": self.timestamp_pic,
            "QDS_A": (
                self.source_timestamp_qds_df.loc[0, "timestamp"] if len(self.source_timestamp_qds_df) > 0 else np.nan
            ),
            "QDS_B": (
                self.source_timestamp_qds_df.loc[1, "timestamp"] if len(self.source_timestamp_qds_df) > 1 else np.nan
            ),
        }

        # act
        # assert
        with warnings.catch_warnings(record=True) as w:
            ipq_analysis.create_timestamp_table(timestamp_dct)
            self.assertEqual("FGC is the first timestamp, potentially a PC trip not a quench!", str(w[0].message))
            self.assertEqual("QDS Board B not present in PM buffer!", str(w[1].message))
            self.assertEqual(
                "Either QDS_A or FGC_B1 PM event is missing. "
                "Can not check synchronization of QDS and FGC_B1 timestamps!",
                str(w[2].message),
            )
            self.assertEqual("QDS_A and FGC_B2 PM events are not synchronized to +/- 40 ms!", str(w[3].message))

    def test_create_timestamp_table_fgc_b2_missing(self):
        # arrange
        ipq_analysis = IpqCircuitAnalysis(
            circuit_type="IPQ", results_table=self.results_table, is_automatic=True, circuit_name=self.circuit_name
        )

        timestamp_dct = {
            "FGC_B1": self.timestamp_fgc_b1,
            "PIC": self.timestamp_pic,
            "QDS_A": (
                self.source_timestamp_qds_df.loc[0, "timestamp"] if len(self.source_timestamp_qds_df) > 0 else np.nan
            ),
            "QDS_B": (
                self.source_timestamp_qds_df.loc[1, "timestamp"] if len(self.source_timestamp_qds_df) > 1 else np.nan
            ),
        }

        # act
        # assert
        with warnings.catch_warnings(record=True) as w:
            ipq_analysis.create_timestamp_table(timestamp_dct)

            self.assertEqual("FGC is the first timestamp, potentially a PC trip not a quench!", str(w[0].message))
            self.assertEqual("QDS Board B not present in PM buffer!", str(w[1].message))
            self.assertEqual("QDS_A and FGC_B1 PM events are not synchronized to +/- 40 ms!", str(w[2].message))
            self.assertEqual(
                "Either QDS_A or FGC_B2 PM event is missing. "
                "Can not check synchronization of QDS and FGC_B2 timestamps!",
                str(w[3].message),
            )

    def test_create_timestamp_table_qds_a_missing(self):
        # arrange
        ipq_analysis = IpqCircuitAnalysis(
            circuit_type="IPQ", results_table=self.results_table, is_automatic=True, circuit_name=self.circuit_name
        )

        timestamp_dct = {
            "FGC_B1": self.timestamp_fgc_b1,
            "FGC_B2": self.timestamp_fgc_b2,
            "PIC": self.timestamp_pic,
            "QDS_B": 1544455364959000000,
        }

        # act
        # assert
        with warnings.catch_warnings(record=True) as w:
            ipq_analysis.create_timestamp_table(timestamp_dct)

            self.assertEqual("FGC is the first timestamp, potentially a PC trip not a quench!", str(w[0].message))
            self.assertEqual("QDS Board A not present in PM buffer!", str(w[1].message))
            self.assertEqual(
                "Either QDS_A or FGC_B1 PM event is missing. "
                "Can not check synchronization of QDS and FGC_B1 timestamps!",
                str(w[2].message),
            )
            self.assertEqual(
                "Either QDS_A or FGC_B2 PM event is missing. "
                "Can not check synchronization of QDS and FGC_B2 timestamps!",
                str(w[3].message),
            )

    def test_create_timestamp_table_both_qds_missing(self):
        # arrange
        ipq_analysis = IpqCircuitAnalysis(
            circuit_type="IPQ", results_table=self.results_table, is_automatic=True, circuit_name=self.circuit_name
        )

        timestamp_dct = {"FGC_B1": self.timestamp_fgc_b1, "FGC_B2": self.timestamp_fgc_b2, "PIC": self.timestamp_pic}

        # act
        # assert
        with warnings.catch_warnings(record=True) as w:
            ipq_analysis.create_timestamp_table(timestamp_dct)

            self.assertEqual("FGC is the first timestamp, potentially a PC trip not a quench!", str(w[0].message))
            self.assertEqual("Neither QDS board A nor B are present in the PM buffer.", str(w[1].message))
            self.assertEqual(
                "Either QDS_A or FGC_B1 PM event is missing. "
                "Can not check synchronization of QDS and FGC_B1 timestamps!",
                str(w[2].message),
            )
            self.assertEqual(
                "Either QDS_A or FGC_B2 PM event is missing. "
                "Can not check synchronization of QDS and FGC_B2 timestamps!",
                str(w[3].message),
            )

    def test_create_timestamp_table_both_qds_missynchronized(self):
        # arrange
        ipq_analysis = IpqCircuitAnalysis(
            circuit_type="IPQ", results_table=self.results_table, is_automatic=True, circuit_name=self.circuit_name
        )

        timestamp_dct = {
            "FGC_B1": self.timestamp_fgc_b1,
            "FGC_B2": self.timestamp_fgc_b2,
            "PIC": self.timestamp_pic,
            "QDS_A": 1544455364959000000,
            "QDS_B": 1544555364959000000,
        }

        # act
        # assert
        with warnings.catch_warnings(record=True) as w:
            ipq_analysis.create_timestamp_table(timestamp_dct)

            self.assertEqual("FGC is the first timestamp, potentially a PC trip not a quench!", str(w[0].message))
            self.assertEqual("QDS PM events (board A and B) are not synchronized to 1 ms!", str(w[1].message))
            self.assertEqual("QDS_A and FGC_B1 PM events are not synchronized to +/- 40 ms!", str(w[2].message))
            self.assertEqual("QDS_A and FGC_B2 PM events are not synchronized to +/- 40 ms!", str(w[3].message))

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_plot_i_meas_pc(self, mock_show=None):
        # arrange
        ipq_analysis = IpqCircuitAnalysis(
            circuit_type="IPQ", results_table=self.results_table, is_automatic=True, circuit_name="RQ4.L2"
        )

        i_meas_b1_df = read_csv("resources/hwc/ipq/fpa", "STATUS.I_MEAS_RQ4_B1")
        i_meas_b2_df = read_csv("resources/hwc/ipq/fpa", "STATUS.I_MEAS_RQ4_B2")
        i_ref_b1_df = read_csv("resources/hwc/ipq/fpa", "STATUS.I_REF_RQ4_B1")
        i_ref_b2_df = read_csv("resources/hwc/ipq/fpa", "STATUS.I_REF_RQ4_B2")

        # act
        ipq_analysis.plot_i_meas_pc(
            self.circuit_name,
            self.timestamp_fgc_b1,
            [i_meas_b1_df, i_meas_b2_df, i_ref_b1_df, i_ref_b2_df],
            xlim=(-0.1, 0.5),
        )

        # assert
        if mock_show is not None:
            mock_show.assert_called()

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_plot_i_meas_tau_pc(self, mock_show=None):
        # arrange
        ipq_analysis = IpqCircuitAnalysis(
            circuit_type="IPQ", results_table=self.results_table, is_automatic=True, circuit_name="RQ4.L2"
        )

        i_meas_b1_df = read_csv("resources/hwc/ipq/fpa", "STATUS.I_MEAS_RQ4_B1")
        i_meas_b2_df = read_csv("resources/hwc/ipq/fpa", "STATUS.I_MEAS_RQ4_B2")
        i_ref_b1_df = read_csv("resources/hwc/ipq/fpa", "STATUS.I_REF_RQ4_B1")
        i_ref_b2_df = read_csv("resources/hwc/ipq/fpa", "STATUS.I_REF_RQ4_B2")

        # act
        ipq_analysis.plot_i_meas_tau_pc(
            self.circuit_name, self.timestamp_fgc_b1, [i_meas_b1_df, i_meas_b2_df, i_ref_b1_df, i_ref_b2_df]
        )

        # assert
        if mock_show is not None:
            mock_show.assert_called()

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_analyze_pc_b1(self, mock_show=None):
        # arrange
        u_res_b1_df = read_csv("resources/hwc/ipq/fpa", "U_RES_B1")
        i_meas_b1_df = read_csv("resources/hwc/ipq/fpa", "STATUS.I_MEAS_RQ4_B1")
        i_ref_b1_df = read_csv("resources/hwc/ipq/fpa", "STATUS.I_REF_RQ4_B1")
        i_a_b1_df = read_csv("resources/hwc/ipq/fpa", "IAB.I_A_RQ4_B1")

        ipq_analysis = IpqCircuitAnalysis(
            circuit_type="IPQ", results_table=self.results_table, is_automatic=True, circuit_name="RQ4.L2"
        )

        # act
        t_quench_act = ipq_analysis.find_start_end_quench_detection(u_res_b1_df)[0]

        ipq_analysis.plot_i_meas_pc(
            self.circuit_name,
            self.timestamp_fgc_b1,
            [i_meas_b1_df, i_a_b1_df, i_ref_b1_df],
            xlim=(-0.1, 0.2),
            ylim=(i_meas_b1_df.max().values[0] - 1500, i_meas_b1_df.max().values[0] + 500),
        )
        ipq_analysis.calculate_current_slope(
            i_meas_b1_df.rename(columns={i_meas_b1_df.columns[0]: "STATUS.I_MEAS"}),
            col_name=["Ramp Rate B1", "Plateau Duration B1"],
        )
        ipq_analysis.calculate_quench_current(i_meas_b1_df, t_quench_act, col_name="I_Q B1")
        ipq_analysis.calculate_current_miits(i_meas_b1_df, t_quench_act, col_name="DCCT MIITs B1")

        # assert
        t_quench_ref = -1.041
        self.assertEqual(t_quench_ref, t_quench_act)
        self.assertAlmostEqual(10.8, ipq_analysis.results_table.loc[0, "Ramp Rate B1"], places=2)
        self.assertAlmostEqual(3585.5, ipq_analysis.results_table.loc[0, "I_Q B1"], places=2)
        self.assertAlmostEqual(14.981, ipq_analysis.results_table.loc[0, "DCCT MIITs B1"], places=2)

        if mock_show is not None:
            mock_show.assert_called()

    def test_calculate_current_miits_i_meas_i_a(self):
        # arrange
        u_res_b1_df = read_csv("resources/hwc/ipq/fpa", "U_RES_B1")
        i_meas_b1_df = read_csv("resources/hwc/ipq/fpa", "STATUS.I_MEAS_RQ4_B1")
        i_a_b1_df = read_csv("resources/hwc/ipq/fpa", "IAB.I_A_RQ4_B1")
        ipq_analysis = IpqCircuitAnalysis(
            circuit_type="IPQ", results_table=self.results_table, is_automatic=True, circuit_name="RQ4.L2"
        )

        # act
        t_quench_act = ipq_analysis.find_start_end_quench_detection(u_res_b1_df)[0]
        ipq_analysis.calculate_current_miits_i_meas_i_a(i_meas_b1_df, i_a_b1_df, t_quench_act, col_name="DCCT MIITs B1")

        self.assertAlmostEqual(14.954, ipq_analysis.results_table.loc[0, "DCCT MIITs B1"], places=2)

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_analyze_pc_b2(self, mock_show=None):
        # arrange
        u_res_b2_df = read_csv("resources/hwc/ipq/fpa", "U_RES_B2")
        i_meas_b2_df = read_csv("resources/hwc/ipq/fpa", "STATUS.I_MEAS_RQ4_B2")
        i_ref_b2_df = read_csv("resources/hwc/ipq/fpa", "STATUS.I_REF_RQ4_B2")
        i_a_b2_df = read_csv("resources/hwc/ipq/fpa", "IAB.I_A_RQ4_B2")

        ipq_analysis = IpqCircuitAnalysis(
            circuit_type="IPQ", results_table=self.results_table, is_automatic=True, circuit_name="RQ4.L2"
        )

        # act
        t_quench_act = ipq_analysis.find_start_end_quench_detection(u_res_b2_df)[0]

        ipq_analysis.plot_i_meas_pc(
            self.circuit_name,
            self.timestamp_fgc_b2,
            [i_meas_b2_df, i_a_b2_df, i_ref_b2_df],
            xlim=(-0.1, 0.2),
            ylim=(i_meas_b2_df.max().values[0] - 1500, i_meas_b2_df.max().values[0] + 500),
        )
        ipq_analysis.calculate_current_slope(
            i_meas_b2_df.rename(columns={i_meas_b2_df.columns[0]: "STATUS.I_MEAS"}),
            col_name=["Ramp Rate B2", "Plateau Duration B2"],
        )
        ipq_analysis.calculate_quench_current(i_meas_b2_df, t_quench_act, col_name="I_Q B2")
        ipq_analysis.calculate_current_miits(i_meas_b2_df, t_quench_act, col_name="DCCT MIITs B2")

        # assert
        t_quench_ref = -1.001
        self.assertEqual(t_quench_ref, t_quench_act)
        self.assertAlmostEqual(10.8, ipq_analysis.results_table.loc[0, "Ramp Rate B2"], places=2)
        self.assertAlmostEqual(3586.0, ipq_analysis.results_table.loc[0, "I_Q B2"], places=2)
        self.assertAlmostEqual(14.482, ipq_analysis.results_table.loc[0, "DCCT MIITs B2"], places=2)

        if mock_show is not None:
            mock_show.assert_called()

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_plot_i_earth_pc_b1(self, mock_show=None):
        # arrange
        i_earth_b1_df = read_csv("resources/hwc/ipq/fpa", "IEARTH.IEARTH_RQ4_B1")

        ipq_analysis = IpqCircuitAnalysis(
            circuit_type="IPQ", results_table=self.results_table, is_automatic=True, circuit_name="RQ4.L2"
        )

        # act
        ipq_analysis.plot_i_earth_pc(self.circuit_name, self.timestamp_fgc_b1, i_earth_b1_df)
        ipq_analysis.calculate_max_i_earth_pc(i_earth_b1_df, col_name="max_I_EARTH_B1")

        # assert
        self.assertAlmostEqual(-0.549317, ipq_analysis.results_table.loc[0, "max_I_EARTH_B1"], places=2)

        if mock_show is not None:
            mock_show.assert_called()

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_plot_i_earth_pc_b2(self, mock_show=None):
        # arrange
        i_earth_b2_df = read_csv("resources/hwc/ipq/fpa", "IEARTH.IEARTH_RQ4_B2")

        ipq_analysis = IpqCircuitAnalysis(
            circuit_type="IPQ", results_table=self.results_table, is_automatic=True, circuit_name="RQ4.L2"
        )

        # act
        ipq_analysis.plot_i_earth_pc(self.circuit_name, self.timestamp_fgc_b1, i_earth_b2_df)
        ipq_analysis.calculate_max_i_earth_pc(i_earth_b2_df, col_name="max_I_EARTH_B2")

        # assert
        self.assertAlmostEqual(-2.502441, ipq_analysis.results_table.loc[0, "max_I_EARTH_B2"], places=2)

        if mock_show is not None:
            mock_show.assert_called()

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_plot_u_res_b1_b2(self, mock_show=None):
        # arrange
        u_res_b1_df = read_csv("resources/hwc/ipq/fpa", "U_RES_B1")
        u_res_b2_df = read_csv("resources/hwc/ipq/fpa", "U_RES_B2")
        ipq_analysis = IpqCircuitAnalysis(
            circuit_type="IPQ", results_table=self.results_table, is_automatic=True, circuit_name="RQ4.L2"
        )

        # act
        t_quench_b1 = ipq_analysis.find_start_end_quench_detection(u_res_b1_df)[0]
        t_quench_b2 = ipq_analysis.find_start_end_quench_detection(u_res_b2_df)[0]
        ipq_analysis.plot_u_res_b1_b2(
            self.circuit_name, self.timestamp_qds, u_res_b1_df, u_res_b2_df, t_quench_b1, t_quench_b2
        )

        # assert
        if mock_show is not None:
            mock_show.assert_called()

    def test_calculate_qh_trigger_delay_b1(self, mock_show=None):
        # arrange
        u_res_b1_df = read_csv("resources/hwc/ipq/fpa", "U_RES_B1")
        ipq_analysis = IpqCircuitAnalysis(
            circuit_type="IPQ", results_table=self.results_table, is_automatic=True, circuit_name="RQ4.L2"
        )

        # act
        with patch("sys.stdout", new=StringIO()) as fake_out:
            ipq_analysis.calculate_quench_detection_time(u_res_b1_df)
            self.assertEqual(fake_out.getvalue().strip(), "Quench heater trigger delay is -1.017 s.")

        # assert
        if mock_show is not None:
            mock_show.assert_called()

    def test_calculate_qh_trigger_delay_b2(self, mock_show=None):
        # arrange
        u_res_b2_df = read_csv("resources/hwc/ipq/fpa", "U_RES_B2")
        ipq_analysis = IpqCircuitAnalysis(
            circuit_type="IPQ", results_table=self.results_table, is_automatic=True, circuit_name="RQ4.L2"
        )

        # act
        with patch("sys.stdout", new=StringIO()) as fake_out:
            ipq_analysis.calculate_quench_detection_time(u_res_b2_df)
            self.assertEqual(fake_out.getvalue().strip(), "Quench heater trigger delay is -0.989 s.")

        # assert
        if mock_show is not None:
            mock_show.assert_called()

    def test_find_quench_origin(self):
        # arrange
        u_res_b1_df = read_csv("resources/hwc/ipq/fpa", "U_RES_B1")
        u_res_b2_df = read_csv("resources/hwc/ipq/fpa", "U_RES_B2")
        ipq_analysis = IpqCircuitAnalysis(
            circuit_type="IPQ", results_table=self.results_table, is_automatic=True, circuit_name="RQ4.L2"
        )

        # act
        with patch("sys.stdout", new=StringIO()) as fake_out:
            ipq_analysis.find_quench_origin([u_res_b1_df, u_res_b2_df])
            self.assertEqual(fake_out.getvalue().strip(), "Quench origin is B1.")

        # assert
        self.assertAlmostEqual("B1", ipq_analysis.results_table.loc[0, "Quench origin"], places=2)

    def test_calculate_u_res_slope_b1(self):
        # arrange
        u_res_b1_df = read_csv("resources/hwc/ipq/fpa", "U_RES_B1")
        ipq_analysis = IpqCircuitAnalysis(
            circuit_type="IPQ", results_table=self.results_table, is_automatic=True, circuit_name="RQ4.L2"
        )

        # act
        with patch("sys.stdout", new=StringIO()) as fake_out:
            u_res_b1_slope_df = ipq_analysis.calculate_u_res_slope(u_res_b1_df, col_name="dU_RES_dt_B1")
            self.assertEqual(fake_out.getvalue().strip(), "dU_RES_dt_B1 = 7.019 V/s.")

        # assert
        u_res_b1_slope_df_ref = pd.DataFrame({"U_RES_slope": {-1.041: 0.00390625, -1.027: 0.10217285}})

        pd.testing.assert_frame_equal(u_res_b1_slope_df_ref, u_res_b1_slope_df)

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_calculate_plot_u_res_slope_b1(self, mock_show=None):
        # arrange
        u_res_b1_df = read_csv("resources/hwc/ipq/fpa", "U_RES_B1")
        u_1_b1_df = read_csv("resources/hwc/ipq/fpa", "U_1_B1")
        u_2_b1_df = read_csv("resources/hwc/ipq/fpa", "U_1_B2")
        ipq_analysis = IpqCircuitAnalysis(
            circuit_type="IPQ", results_table=self.results_table, is_automatic=True, circuit_name="RQ4.L2"
        )

        # act
        u_res_b1_slope_df = ipq_analysis.calculate_u_res_slope(u_res_b1_df, col_name="dU_RES_dt_B1")
        ipq_analysis.plot_u_res_u_res_slope_u_1_u_2(
            self.circuit_name, self.timestamp_qds, u_res_b1_df, u_res_b1_slope_df, u_1_b1_df, u_2_b1_df, suffix="_B1"
        )

        # assert
        if mock_show is not None:
            mock_show.assert_called()

    def test_calculate_u_res_slope_b2(self):
        # arrange
        u_res_b2_df = read_csv("resources/hwc/ipq/fpa", "U_RES_B2")
        ipq_analysis = IpqCircuitAnalysis(
            circuit_type="IPQ", results_table=self.results_table, is_automatic=True, circuit_name="RQ4.L2"
        )

        # act
        with patch("sys.stdout", new=StringIO()) as fake_out:
            u_res_b2_slope_df = ipq_analysis.calculate_u_res_slope(u_res_b2_df, col_name="dU_RES_dt_B2")
            self.assertEqual(fake_out.getvalue().strip(), "dU_RES_dt_B2 = -147.827 V/s.")

        # assert
        u_res_b2_slope_df_ref = pd.DataFrame({"U_RES_slope": {-1.001: 0.045654297, -0.999: -0.25}})

        pd.testing.assert_frame_equal(u_res_b2_slope_df_ref, u_res_b2_slope_df)

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_calculate_plot_u_res_slope_b2(self, mock_show=None):
        # arrange
        u_res_b2_df = read_csv("resources/hwc/ipq/fpa", "U_RES_B1")
        u_1_b2_df = read_csv("resources/hwc/ipq/fpa", "U_1_B1")
        u_2_b2_df = read_csv("resources/hwc/ipq/fpa", "U_1_B2")
        ipq_analysis = IpqCircuitAnalysis(
            circuit_type="IPQ", results_table=self.results_table, is_automatic=True, circuit_name="RQ4.L2"
        )

        # act
        u_res_b1_slope_df = ipq_analysis.calculate_u_res_slope(u_res_b2_df, col_name="dU_RES_dt_B1")
        ipq_analysis.plot_u_res_u_res_slope_u_1_u_2(
            self.circuit_name, self.timestamp_qds, u_res_b2_df, u_res_b1_slope_df, u_1_b2_df, u_2_b2_df, suffix="_B2"
        )

        # assert
        if mock_show is not None:
            mock_show.assert_called()

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_analyze_leads_voltage_u_hts(self, mock_show=None):
        # arrange
        u_hts_b1_dfs = [
            read_csv("resources/hwc/ipq/fpa", "LD1_U_HTS_B1"),
            read_csv("resources/hwc/ipq/fpa", "LD2_U_HTS_B1"),
        ]
        u_hts_b2_dfs = [
            read_csv("resources/hwc/ipq/fpa", "LD2_U_HTS_B2"),
            read_csv("resources/hwc/ipq/fpa", "LD3_U_HTS_B2"),
        ]
        ipq_analysis = IpqCircuitAnalysis(
            circuit_type="IPQ", results_table=self.results_table, is_automatic=True, circuit_name="RQ4.L2"
        )

        # act
        ipq_analysis.analyze_leads_voltage(
            u_hts_b1_dfs + u_hts_b2_dfs,
            self.circuit_name,
            self.timestamp_fgc_b1,
            timestamp_qps=self.timestamp_qds,
            signal="U_HTS",
            value_min=-0.003,
            value_max=0.003,
        )

        # assert
        if mock_show is not None:
            mock_show.assert_called()

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_analyze_leads_voltage_u_res(self, mock_show=None):
        # arrange
        u_res_b1_dfs = [
            read_csv("resources/hwc/ipq/fpa", "LD1_U_RES_B1"),
            read_csv("resources/hwc/ipq/fpa", "LD2_U_RES_B1"),
        ]
        u_res_b2_dfs = [
            read_csv("resources/hwc/ipq/fpa", "LD2_U_RES_B2"),
            read_csv("resources/hwc/ipq/fpa", "LD3_U_RES_B2"),
        ]
        ipq_analysis = IpqCircuitAnalysis(
            circuit_type="IPQ", results_table=self.results_table, is_automatic=True, circuit_name="RQ4.L2"
        )

        # act
        ipq_analysis.analyze_leads_voltage(
            u_res_b1_dfs + u_res_b2_dfs,
            self.circuit_name,
            self.timestamp_fgc_b1,
            timestamp_qps=self.timestamp_qds,
            signal="U_RES",
            value_min=-0.1,
            value_max=0.1,
        )

        # assert
        if mock_show is not None:
            mock_show.assert_called()

    @patch("matplotlib.pyplot.show")
    def test_analyze_qh(self, mock_show=None):
        # arrange
        u_hds_dfs = [
            read_csv("resources/hwc/ipq/fpa", "U_HDS_1_B1"),
            read_csv("resources/hwc/ipq/fpa", "U_HDS_2_B1"),
            read_csv("resources/hwc/ipq/fpa", "U_HDS_3_B1"),
            read_csv("resources/hwc/ipq/fpa", "U_HDS_4_B1"),
            read_csv("resources/hwc/ipq/fpa", "U_HDS_1_B2"),
            read_csv("resources/hwc/ipq/fpa", "U_HDS_2_B2"),
            read_csv("resources/hwc/ipq/fpa", "U_HDS_3_B2"),
            read_csv("resources/hwc/ipq/fpa", "U_HDS_4_B2"),
        ]
        u_hds_ref_dfs = [
            read_csv("resources/hwc/ipq/fpa", "U_HDS_1_B1_REF"),
            read_csv("resources/hwc/ipq/fpa", "U_HDS_2_B1_REF"),
            read_csv("resources/hwc/ipq/fpa", "U_HDS_3_B1_REF"),
            read_csv("resources/hwc/ipq/fpa", "U_HDS_4_B1_REF"),
            read_csv("resources/hwc/ipq/fpa", "U_HDS_1_B2_REF"),
            read_csv("resources/hwc/ipq/fpa", "U_HDS_2_B2_REF"),
            read_csv("resources/hwc/ipq/fpa", "U_HDS_3_B2_REF"),
            read_csv("resources/hwc/ipq/fpa", "U_HDS_4_B2_REF"),
        ]
        ipq_analysis = IpqCircuitAnalysis(
            circuit_type="IPQ", results_table=self.results_table, is_automatic=True, circuit_name="RQ4.L2"
        )
        # act
        ipq_analysis.analyze_single_qh_voltage_with_ref(self.circuit_name, self.timestamp_qds, u_hds_dfs, u_hds_ref_dfs)

        # assert
        self.assertEqual("Pass", ipq_analysis.results_table.loc[0, "QH analysis"])

        if mock_show is not None:
            mock_show.assert_called()

    @patch("matplotlib.pyplot.show")
    def test_mp3_results_table(self, mock_show=None):
        # arrange

        # # PC B1
        i_meas_b1_df = read_csv("resources/hwc/ipq/fpa", "STATUS.I_MEAS_RQ4_B1")
        i_earth_b1_df = read_csv("resources/hwc/ipq/fpa", "IEARTH.IEARTH_RQ4_B1")

        # # PC B2
        i_meas_b2_df = read_csv("resources/hwc/ipq/fpa", "STATUS.I_MEAS_RQ4_B2")
        i_earth_b2_df = read_csv("resources/hwc/ipq/fpa", "IEARTH.IEARTH_RQ4_B2")

        # # QDS
        u_res_b1_df = read_csv("resources/hwc/ipq/fpa", "U_RES_B1")
        u_res_b2_df = read_csv("resources/hwc/ipq/fpa", "U_RES_B2")

        # # QH
        u_hds_dfs = [
            read_csv("resources/hwc/ipq/fpa", "U_HDS_1_B1"),
            read_csv("resources/hwc/ipq/fpa", "U_HDS_2_B1"),
            read_csv("resources/hwc/ipq/fpa", "U_HDS_3_B1"),
            read_csv("resources/hwc/ipq/fpa", "U_HDS_4_B1"),
            read_csv("resources/hwc/ipq/fpa", "U_HDS_1_B2"),
            read_csv("resources/hwc/ipq/fpa", "U_HDS_2_B2"),
            read_csv("resources/hwc/ipq/fpa", "U_HDS_3_B2"),
            read_csv("resources/hwc/ipq/fpa", "U_HDS_4_B2"),
        ]
        u_hds_ref_dfs = [
            read_csv("resources/hwc/ipq/fpa", "U_HDS_1_B1_REF"),
            read_csv("resources/hwc/ipq/fpa", "U_HDS_2_B1_REF"),
            read_csv("resources/hwc/ipq/fpa", "U_HDS_3_B1_REF"),
            read_csv("resources/hwc/ipq/fpa", "U_HDS_4_B1_REF"),
            read_csv("resources/hwc/ipq/fpa", "U_HDS_1_B2_REF"),
            read_csv("resources/hwc/ipq/fpa", "U_HDS_2_B2_REF"),
            read_csv("resources/hwc/ipq/fpa", "U_HDS_3_B2_REF"),
            read_csv("resources/hwc/ipq/fpa", "U_HDS_4_B2_REF"),
        ]

        # # LEADS
        u_hts_b1_dfs = [
            read_csv("resources/hwc/ipq/fpa", "LD1_U_HTS_B1"),
            read_csv("resources/hwc/ipq/fpa", "LD2_U_HTS_B1"),
        ]
        u_hts_b2_dfs = [
            read_csv("resources/hwc/ipq/fpa", "LD2_U_HTS_B2"),
            read_csv("resources/hwc/ipq/fpa", "LD3_U_HTS_B2"),
        ]

        u_res_b1_dfs = [
            read_csv("resources/hwc/ipq/fpa", "LD1_U_RES_B1"),
            read_csv("resources/hwc/ipq/fpa", "LD2_U_RES_B1"),
        ]
        u_res_b2_dfs = [
            read_csv("resources/hwc/ipq/fpa", "LD2_U_RES_B2"),
            read_csv("resources/hwc/ipq/fpa", "LD3_U_RES_B2"),
        ]

        ipq_analysis = IpqCircuitAnalysis(
            circuit_type="IPQ", results_table=self.results_table, is_automatic=True, circuit_name="RQ4.L2"
        )

        # act
        # # PC B1
        t_quench_b1 = ipq_analysis.find_start_end_quench_detection(u_res_b1_df)[0]
        ipq_analysis.calculate_current_slope(
            i_meas_b1_df.rename(columns={i_meas_b1_df.columns[0]: "STATUS.I_MEAS"}),
            col_name=["Ramp rate B1", "Plateau duration B1"],
        )
        ipq_analysis.calculate_quench_current(i_meas_b1_df, t_quench_b1, col_name="I_Q_B1")
        ipq_analysis.calculate_current_miits(i_meas_b1_df, t_quench_b1, col_name="MIITS_B1")
        # # PC B2
        t_quench_b2 = ipq_analysis.find_start_end_quench_detection(u_res_b2_df)[0]
        ipq_analysis.calculate_current_slope(
            i_meas_b2_df.rename(columns={i_meas_b2_df.columns[0]: "STATUS.I_MEAS"}),
            col_name=["Ramp rate B2", "Plateau duration B2"],
        )
        ipq_analysis.calculate_quench_current(i_meas_b2_df, t_quench_b2, col_name="I_Q_B2")
        ipq_analysis.calculate_current_miits(i_meas_b2_df, t_quench_b2, col_name="MIITS_B2")

        # # IEARTH B1
        ipq_analysis.calculate_max_i_earth_pc(i_earth_b1_df, col_name="I_Earth_max_B1")

        # # IEARTH B2
        ipq_analysis.calculate_max_i_earth_pc(i_earth_b2_df, col_name="I_Earth_max_B2")

        # # QDS
        ipq_analysis.find_quench_origin([u_res_b1_df, u_res_b2_df])

        ipq_analysis.calculate_u_res_slope(u_res_b1_df, col_name="dU_QPS_B1/dt")
        ipq_analysis.calculate_u_res_slope(u_res_b2_df, col_name="dU_QPS_B2/dt")

        # # LEADS
        ipq_analysis.analyze_leads_voltage(
            u_hts_b1_dfs + u_hts_b2_dfs,
            self.circuit_name,
            self.timestamp_fgc_b1,
            timestamp_qps=self.timestamp_qds,
            signal="U_HTS",
            value_min=-0.003,
            value_max=0.003,
        )

        ipq_analysis.analyze_leads_voltage(
            u_res_b1_dfs + u_res_b2_dfs,
            self.circuit_name,
            self.timestamp_fgc_b1,
            timestamp_qps=self.timestamp_qds,
            signal="U_RES",
            value_min=-0.1,
            value_max=0.1,
        )

        # # QH
        ipq_analysis.analyze_single_qh_voltage_with_ref(self.circuit_name, self.timestamp_qds, u_hds_dfs, u_hds_ref_dfs)
        mp3_results_table_act = ipq_analysis.create_mp3_results_table()

        # assert

        mp3_results_table_ref = pd.DataFrame(
            {
                "Circuit Name": {0: "RQ4.L2"},
                "Circuit Family": {0: "RQ4"},
                "Period": {0: "HWC 2018-2"},
                "Date (FGC_Bn)": {0: "2018-12-10"},
                "Time (FGC_Bn)": {0: "16:22:42.980"},
                "FPA Reason": {0: np.nan},
                "Timestamp_PIC": {0: "2018-12-10 16:22:44.958"},
                "Delta_t(FGC-PIC)": {0: -1978.0},
                "Ramp rate B1": {0: 10.8},
                "Ramp rate B2": {0: 10.8},
                "Plateau duration B1": {0: 0},
                "Plateau duration B2": {0: 0},
                "I_Q_B1": {0: 3585.5},
                "I_Q_B2": {0: 3586.0},
                "MIITS_B1": {0: 14.981},
                "MIITS_B2": {0: 14.482},
                "I_Earth_max_B1": {0: -0.549317},
                "I_Earth_max_B2": {0: -2.502441},
                "Delta_t(QPS-PIC)": {0: 1.0},
                "Type of Quench": {0: np.nan},
                "Quench origin": {0: "B1"},
                "Quench count": {0: np.nan},
                "QDS trigger origin": {0: np.nan},
                "dU_QPS_B1/dt": {0: 7.019042857142962},
                "dU_QPS_B2/dt": {0: -147.82714850001628},
                "QH analysis": {0: "Pass"},
                "Comment": {0: np.nan},
                "Analysis performed by": {0: ""},
                "lhcsmapi version": {0: "1.4.35"},
            }
        )
        mp3_results_table_act.fillna(np.nan, inplace=True)
        mp3_results_table_act["lhcsmapi version"] = "1.4.35"
        mp3_results_table_ref.fillna(np.nan, inplace=True)
        pd.testing.assert_frame_equal(mp3_results_table_ref, mp3_results_table_act)

        if mock_show is not None:
            mock_show.assert_called()

    # HWC PLI2.e3
    @patch("matplotlib.pyplot.show")
    def test_calculate_splice_resistance_linreg(self, mock_show=None):
        # arrange
        plateau_start = [1422391375000000000, 1422391909000000000, 1422392234000000000]
        plateau_end = [1422391678500000000, 1422392152500000000, 1422392536000000000]

        i_meas_b1_raw_df = read_csv("resources/hwc/ipq/pli2_e3", "I_MEAS_B1_RAW")
        i_meas_b2_raw_df = read_csv("resources/hwc/ipq/pli2_e3", "I_MEAS_B2_RAW")
        u_res_b1_raw_df = read_csv("resources/hwc/ipq/pli2_e3", "RQ10.R6_U_RES_B1_RAW")
        u_res_b2_raw_df = read_csv("resources/hwc/ipq/pli2_e3", "RQ10.R6_U_RES_B2_RAW")

        ipq_analysis = IpqCircuitAnalysis(
            circuit_type="IPQ", results_table=None, is_automatic=True, circuit_name="RQ4.L2"
        )

        # act
        res_b1 = ipq_analysis.calculate_splice_resistance_linreg(
            u_res_b1_raw_df, i_meas_b1_raw_df, plateau_start, plateau_end
        )
        res_b2 = ipq_analysis.calculate_splice_resistance_linreg(
            u_res_b2_raw_df, i_meas_b2_raw_df, plateau_start, plateau_end
        )
        res_df = pd.DataFrame({"R_RES": {"R_B1": res_b1, "R_B2": res_b2}})

        with warnings.catch_warnings(record=True) as w:
            ipq_analysis.analyze_busbar_magnet_resistance(res_df, "R_RES", 5e-9)

            self.assertEqual("The following features are outside of range.", str(w[0].message))

        if mock_show is not None:
            mock_show.assert_called()

    # HWC PNO.c4
    @patch("matplotlib.pyplot.show")
    def test_calculate_current_plateau_start_end_pno_c4_b1(self, mock_show=None):
        # arrange
        t_start = "2015-02-21 12:56:33.073"
        t_end = "2015-02-21 15:32:43.142"
        circuit_name = "RQ10.R6"
        hwc_test = "PNO.c4"

        i_meas_b1_raw_df = read_csv("resources/hwc/ipq/pno_c4", "I_MEAS_B1_RAW")

        ipq_analysis = IpqCircuitAnalysis(
            circuit_type="IPQ", results_table=None, is_automatic=True, circuit_name="RQ4.L2"
        )
        # act
        plateau_start_b1, plateau_end_b1 = ipq_analysis.find_plateau_start_and_end(
            i_meas_b1_raw_df, i_meas_threshold=0, min_duration_in_sec=5
        )
        title = "%s, %s: %s-%s" % (
            circuit_name,
            hwc_test,
            Time.to_string(t_start).split(".")[0],
            Time.to_string(t_end).split(".")[0],
        )
        ax = (
            PlotBuilder()
            .with_signal(i_meas_b1_raw_df, title=title, grid=True)
            .with_ylabel(ylabel="I_MEAS, [A]")
            .plot(show_plot=False)
            .get_axes()[0]
        )

        for ps, pe in zip(plateau_start_b1, plateau_end_b1):
            ax.axvspan(xmin=ps, xmax=pe, facecolor="xkcd:yellow")

        plt.show()

        if mock_show is not None:
            mock_show.assert_called()

    @patch("matplotlib.pyplot.show")
    def test_calculate_current_plateau_start_end_pno_c4_b2(self, mock_show=None):
        # arrange
        t_start = "2015-02-21 12:56:33.073"
        t_end = "2015-02-21 15:32:43.142"
        circuit_name = "RQ10.R6"
        hwc_test = "PNO.c4"

        i_meas_b2_raw_df = read_csv("resources/hwc/ipq/pno_c4", "I_MEAS_B2_RAW")

        ipq_analysis = IpqCircuitAnalysis(
            circuit_type="IPQ", results_table=None, is_automatic=True, circuit_name="RQ4.L2"
        )
        # act
        plateau_start_b2, plateau_end_b2 = ipq_analysis.find_plateau_start_and_end(
            i_meas_b2_raw_df, i_meas_threshold=0, min_duration_in_sec=5
        )
        title = "%s, %s: %s-%s" % (
            circuit_name,
            hwc_test,
            Time.to_string(t_start).split(".")[0],
            Time.to_string(t_end).split(".")[0],
        )
        ax = (
            PlotBuilder()
            .with_signal(i_meas_b2_raw_df, title=title, grid=True)
            .with_ylabel(ylabel="I_MEAS, [A]")
            .plot(show_plot=False)
            .get_axes()[0]
        )

        for ps, pe in zip(plateau_start_b2, plateau_end_b2):
            ax.axvspan(xmin=ps, xmax=pe, facecolor="xkcd:yellow")

        plt.show()

        if mock_show is not None:
            mock_show.assert_called()
