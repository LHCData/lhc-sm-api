import unittest
from io import StringIO
from unittest.mock import patch
from lhcsmapi.Timer import Timer


class TestTimer(unittest.TestCase):
    def test_timer_context_manager(self):
        with patch("sys.stdout", new=StringIO()) as fake_out:
            with patch("lhcsmapi.Timer.time.time", return_value=10):
                with Timer():
                    pass

        self.assertEqual("Elapsed: 0.000 s.", fake_out.getvalue().strip())
