import unittest

import pytest

from lhcsmapi.metadata.MappingMetadata import MappingMetadata
from lhcsmapi.metadata import signal_metadata


class TestMappingMetadata(unittest.TestCase):
    def test_read_circuit_name_to_sector(self):
        # arrange
        circuit_name_to_sector_df = MappingMetadata.read_circuit_name_to_sector()
        sector_suffix_keys = [
            ".A12",
            ".A23",
            ".A34",
            ".A45",
            ".A56",
            ".A67",
            ".A78",
            ".A81",
            ".L2",
            ".L3",
            ".L4",
            ".L5",
            ".L6",
            ".L7",
            ".L8",
            ".L1",
            ".R1",
            ".R2",
            ".R3",
            ".R4",
            ".R5",
            ".R6",
            ".R7",
            ".R8",
        ]
        sector_values = [
            "S12",
            "S23",
            "S34",
            "S45",
            "S56",
            "S67",
            "S78",
            "S81",
            "S12",
            "S23",
            "S34",
            "S45",
            "S56",
            "S67",
            "S78",
            "S81",
            "S12",
            "S23",
            "S34",
            "S45",
            "S56",
            "S67",
            "S78",
            "S81",
        ]
        sector_suffix_to_sector = dict(zip(sector_suffix_keys, sector_values))

        # act
        # assert
        for key, value in sector_suffix_to_sector.items():
            sector_acts = circuit_name_to_sector_df[
                circuit_name_to_sector_df["Circuit name"].str.contains(key, regex=False)
            ]["Sector name"].unique()
            sector_act = sector_acts[0]
            sector_ref = value
            self.assertEqual(sector_ref, sector_act)
            self.assertTrue(len(sector_acts) == 1)

    def test_read_magnet_to_cell_qps_crate(self):
        # arrange
        circuit_type = "RB"

        # act
        with self.assertRaises(KeyError) as context:
            MappingMetadata.read_magnet_to_cell_qps_crate(circuit_type)

        # assert
        self.assertTrue("Circuit type RB is not supported!" in context.exception.args[0])

    def test_read_crate_to_magnet_and_vf_mapping(self):
        # arrange
        circuit_type = "IT"

        # act
        with self.assertRaises(KeyError) as context:
            MappingMetadata.read_crate_to_magnet_and_vf_mapping(circuit_type)

        # assert
        self.assertTrue("Circuit type IT is not supported!" in context.exception.args[0])

    def test_get_magnet_to_crate_and_vf_mapping(self):
        # arrange
        circuit_type = "IT"

        # act
        with self.assertRaises(KeyError) as context:
            MappingMetadata.get_magnet_to_crate_and_vf_mapping(circuit_type)

        # assert
        self.assertTrue("Circuit type IT is not supported!" in context.exception.args[0])

    def test_read_layout_details(self):
        # arrange
        circuit_type = "IT"

        # act
        with self.assertRaises(KeyError) as context:
            MappingMetadata.read_layout_details(circuit_type)

        # assert
        self.assertTrue("Circuit type IT is not supported!" in context.exception.args[0])

    def test_read_magnet_name_to_magnet_id_mapping(self):
        # arrange
        circuit_type = "IT"

        # act
        with self.assertRaises(KeyError) as context:
            MappingMetadata.read_magnet_name_to_magnet_id_mapping(circuit_type)

        # assert
        self.assertTrue("Circuit type IT is not supported!" in context.exception.args[0])

    def test_get_magnets_for_circuit_names(self):
        # arrange
        circuit_type = "IT"
        circuit_names = ["RQX.R1", "RQX.L1"]

        # act
        with self.assertRaises(KeyError) as context:
            MappingMetadata.get_magnets_for_circuit_names(circuit_type, circuit_names)

        # assert
        self.assertTrue("Circuit type IT is not supported!" in context.exception.args[0])

    def test_get_cells_for_circuit_names(self):
        # arrange
        circuit_type = "IT"
        circuit_names = ["RQX.R1", "RQX.L1"]

        # act
        with self.assertRaises(KeyError) as context:
            MappingMetadata.get_cells_for_circuit_names(circuit_type, circuit_names)

        # assert
        self.assertTrue("Circuit type IT is not supported!" in context.exception.args[0])

    def test_get_vf_for_circuit_names(self):
        # arrange
        circuit_type = "IT"
        circuit_names = ["RQX.R1", "RQX.L1"]

        # act
        with self.assertRaises(KeyError) as context:
            MappingMetadata.get_vf_for_circuit_names(circuit_type, circuit_names)

        # assert
        self.assertTrue("Circuit type IT is not supported!" in context.exception.args[0])

    def test_get_crates_for_circuit_names(self):
        # arrange
        circuit_type = "IT"
        circuit_names = ["RQX.R1", "RQX.L1"]

        # act
        with self.assertRaises(KeyError) as context:
            MappingMetadata.get_crates_for_circuit_names(circuit_type, circuit_names)

        # assert
        self.assertTrue("Circuit type IT is not supported!" in context.exception.args[0])

    def test_getCellsForCircuitNames_RBA12(self):
        # arrange
        circuit_type = "RB"
        circuit_name = "RB.A12"

        # act
        cells_act = MappingMetadata.get_cells_for_circuit_names(circuit_type, circuit_name)

        # assert
        cells_exp = [
            "A8R1",
            "B8R1",
            "A9R1",
            "B9R1",
            "A10R1",
            "B10R1",
            "A11R1",
            "B11R1",
            "A12R1",
            "B12R1",
            "C12R1",
            "A13R1",
            "B13R1",
            "C13R1",
            "A14R1",
            "B14R1",
            "C14R1",
            "A15R1",
            "B15R1",
            "C15R1",
            "A16R1",
            "B16R1",
            "C16R1",
            "A17R1",
            "B17R1",
            "C17R1",
            "A18R1",
            "B18R1",
            "C18R1",
            "A19R1",
            "B19R1",
            "C19R1",
            "A20R1",
            "B20R1",
            "C20R1",
            "A21R1",
            "B21R1",
            "C21R1",
            "A22R1",
            "B22R1",
            "C22R1",
            "A23R1",
            "B23R1",
            "C23R1",
            "A24R1",
            "B24R1",
            "C24R1",
            "A25R1",
            "B25R1",
            "C25R1",
            "A26R1",
            "B26R1",
            "C26R1",
            "A27R1",
            "B27R1",
            "C27R1",
            "A28R1",
            "B28R1",
            "C28R1",
            "A29R1",
            "B29R1",
            "C29R1",
            "A30R1",
            "B30R1",
            "C30R1",
            "A31R1",
            "B31R1",
            "C31R1",
            "A32R1",
            "B32R1",
            "C32R1",
            "A33R1",
            "B33R1",
            "C33R1",
            "A34R1",
            "B34R1",
            "C34R1",
            "C34L2",
            "B34L2",
            "A34L2",
            "C33L2",
            "B33L2",
            "A33L2",
            "C32L2",
            "B32L2",
            "A32L2",
            "C31L2",
            "B31L2",
            "A31L2",
            "C30L2",
            "B30L2",
            "A30L2",
            "C29L2",
            "B29L2",
            "A29L2",
            "C28L2",
            "B28L2",
            "A28L2",
            "C27L2",
            "B27L2",
            "A27L2",
            "C26L2",
            "B26L2",
            "A26L2",
            "C25L2",
            "B25L2",
            "A25L2",
            "C24L2",
            "B24L2",
            "A24L2",
            "C23L2",
            "B23L2",
            "A23L2",
            "C22L2",
            "B22L2",
            "A22L2",
            "C21L2",
            "B21L2",
            "A21L2",
            "C20L2",
            "B20L2",
            "A20L2",
            "C19L2",
            "B19L2",
            "A19L2",
            "C18L2",
            "B18L2",
            "A18L2",
            "C17L2",
            "B17L2",
            "A17L2",
            "C16L2",
            "B16L2",
            "A16L2",
            "C15L2",
            "B15L2",
            "A15L2",
            "C14L2",
            "B14L2",
            "A14L2",
            "C13L2",
            "B13L2",
            "A13L2",
            "C12L2",
            "B12L2",
            "A12L2",
            "B11L2",
            "A11L2",
            "B10L2",
            "A10L2",
            "B9L2",
            "A9L2",
            "B8L2",
            "A8L2",
        ]

        cells_exp.sort()
        cells_act.sort()
        self.assertListEqual(cells_exp, cells_act)

    def test_getCellsForCircuitNames_RBA12_RBA34(self):
        # arrange
        circuit_type = "RB"
        circuit_names = ["RB.A12", "RB.A34"]

        # act
        cells_act = MappingMetadata.get_cells_for_circuit_names(circuit_type, circuit_names)

        # assert
        cells_exp = [
            "A8R1",
            "B8R1",
            "A9R1",
            "B9R1",
            "A10R1",
            "B10R1",
            "A11R1",
            "B11R1",
            "A12R1",
            "B12R1",
            "C12R1",
            "A13R1",
            "B13R1",
            "C13R1",
            "A14R1",
            "B14R1",
            "C14R1",
            "A15R1",
            "B15R1",
            "C15R1",
            "A16R1",
            "B16R1",
            "C16R1",
            "A17R1",
            "B17R1",
            "C17R1",
            "A18R1",
            "B18R1",
            "C18R1",
            "A19R1",
            "B19R1",
            "C19R1",
            "A20R1",
            "B20R1",
            "C20R1",
            "A21R1",
            "B21R1",
            "C21R1",
            "A22R1",
            "B22R1",
            "C22R1",
            "A23R1",
            "B23R1",
            "C23R1",
            "A24R1",
            "B24R1",
            "C24R1",
            "A25R1",
            "B25R1",
            "C25R1",
            "A26R1",
            "B26R1",
            "C26R1",
            "A27R1",
            "B27R1",
            "C27R1",
            "A28R1",
            "B28R1",
            "C28R1",
            "A29R1",
            "B29R1",
            "C29R1",
            "A30R1",
            "B30R1",
            "C30R1",
            "A31R1",
            "B31R1",
            "C31R1",
            "A32R1",
            "B32R1",
            "C32R1",
            "A33R1",
            "B33R1",
            "C33R1",
            "A34R1",
            "B34R1",
            "C34R1",
            "C34L2",
            "B34L2",
            "A34L2",
            "C33L2",
            "B33L2",
            "A33L2",
            "C32L2",
            "B32L2",
            "A32L2",
            "C31L2",
            "B31L2",
            "A31L2",
            "C30L2",
            "B30L2",
            "A30L2",
            "C29L2",
            "B29L2",
            "A29L2",
            "C28L2",
            "B28L2",
            "A28L2",
            "C27L2",
            "B27L2",
            "A27L2",
            "C26L2",
            "B26L2",
            "A26L2",
            "C25L2",
            "B25L2",
            "A25L2",
            "C24L2",
            "B24L2",
            "A24L2",
            "C23L2",
            "B23L2",
            "A23L2",
            "C22L2",
            "B22L2",
            "A22L2",
            "C21L2",
            "B21L2",
            "A21L2",
            "C20L2",
            "B20L2",
            "A20L2",
            "C19L2",
            "B19L2",
            "A19L2",
            "C18L2",
            "B18L2",
            "A18L2",
            "C17L2",
            "B17L2",
            "A17L2",
            "C16L2",
            "B16L2",
            "A16L2",
            "C15L2",
            "B15L2",
            "A15L2",
            "C14L2",
            "B14L2",
            "A14L2",
            "C13L2",
            "B13L2",
            "A13L2",
            "C12L2",
            "B12L2",
            "A12L2",
            "B11L2",
            "A11L2",
            "B10L2",
            "A10L2",
            "B9L2",
            "A9L2",
            "B8L2",
            "A8L2",
            "A8R3",
            "B8R3",
            "A9R3",
            "B9R3",
            "A10R3",
            "B10R3",
            "A11R3",
            "B11R3",
            "A12R3",
            "B12R3",
            "C12R3",
            "A13R3",
            "B13R3",
            "C13R3",
            "A14R3",
            "B14R3",
            "C14R3",
            "A15R3",
            "B15R3",
            "C15R3",
            "A16R3",
            "B16R3",
            "C16R3",
            "A17R3",
            "B17R3",
            "C17R3",
            "A18R3",
            "B18R3",
            "C18R3",
            "A19R3",
            "B19R3",
            "C19R3",
            "A20R3",
            "B20R3",
            "C20R3",
            "A21R3",
            "B21R3",
            "C21R3",
            "A22R3",
            "B22R3",
            "C22R3",
            "A23R3",
            "B23R3",
            "C23R3",
            "A24R3",
            "B24R3",
            "C24R3",
            "A25R3",
            "B25R3",
            "C25R3",
            "A26R3",
            "B26R3",
            "C26R3",
            "A27R3",
            "B27R3",
            "C27R3",
            "A28R3",
            "B28R3",
            "C28R3",
            "A29R3",
            "B29R3",
            "C29R3",
            "A30R3",
            "B30R3",
            "C30R3",
            "A31R3",
            "B31R3",
            "C31R3",
            "A32R3",
            "B32R3",
            "C32R3",
            "A33R3",
            "B33R3",
            "C33R3",
            "A34R3",
            "B34R3",
            "C34R3",
            "C34L4",
            "B34L4",
            "A34L4",
            "C33L4",
            "B33L4",
            "A33L4",
            "C32L4",
            "B32L4",
            "A32L4",
            "C31L4",
            "B31L4",
            "A31L4",
            "C30L4",
            "B30L4",
            "A30L4",
            "C29L4",
            "B29L4",
            "A29L4",
            "C28L4",
            "B28L4",
            "A28L4",
            "C27L4",
            "B27L4",
            "A27L4",
            "C26L4",
            "B26L4",
            "A26L4",
            "C25L4",
            "B25L4",
            "A25L4",
            "C24L4",
            "B24L4",
            "A24L4",
            "C23L4",
            "B23L4",
            "A23L4",
            "C22L4",
            "B22L4",
            "A22L4",
            "C21L4",
            "B21L4",
            "A21L4",
            "C20L4",
            "B20L4",
            "A20L4",
            "C19L4",
            "B19L4",
            "A19L4",
            "C18L4",
            "B18L4",
            "A18L4",
            "C17L4",
            "B17L4",
            "A17L4",
            "C16L4",
            "B16L4",
            "A16L4",
            "C15L4",
            "B15L4",
            "A15L4",
            "C14L4",
            "B14L4",
            "A14L4",
            "C13L4",
            "B13L4",
            "A13L4",
            "C12L4",
            "B12L4",
            "A12L4",
            "B11L4",
            "A11L4",
            "B10L4",
            "A10L4",
            "B9L4",
            "A9L4",
            "B8L4",
            "A8L4",
        ]

        cells_exp.sort()
        cells_act.sort()
        self.assertEqual(cells_exp, cells_act)

    def test_getMagnetsForCircuitNames_RBA12(self):
        # arrange
        circuit_type = "RB"
        circuit_name = "RB.A12"

        # act
        magnet_names_act = MappingMetadata.get_magnets_for_circuit_names(circuit_type, circuit_name)

        # assert
        magnet_names_exp = [
            "MB.A8R1",
            "MB.B8R1",
            "MB.A9R1",
            "MB.B9R1",
            "MB.A10R1",
            "MB.B10R1",
            "MB.A11R1",
            "MB.B11R1",
            "MB.A12R1",
            "MB.B12R1",
            "MB.C12R1",
            "MB.A13R1",
            "MB.B13R1",
            "MB.C13R1",
            "MB.A14R1",
            "MB.B14R1",
            "MB.C14R1",
            "MB.A15R1",
            "MB.B15R1",
            "MB.C15R1",
            "MB.A16R1",
            "MB.B16R1",
            "MB.C16R1",
            "MB.A17R1",
            "MB.B17R1",
            "MB.C17R1",
            "MB.A18R1",
            "MB.B18R1",
            "MB.C18R1",
            "MB.A19R1",
            "MB.B19R1",
            "MB.C19R1",
            "MB.A20R1",
            "MB.B20R1",
            "MB.C20R1",
            "MB.A21R1",
            "MB.B21R1",
            "MB.C21R1",
            "MB.A22R1",
            "MB.B22R1",
            "MB.C22R1",
            "MB.A23R1",
            "MB.B23R1",
            "MB.C23R1",
            "MB.A24R1",
            "MB.B24R1",
            "MB.C24R1",
            "MB.A25R1",
            "MB.B25R1",
            "MB.C25R1",
            "MB.A26R1",
            "MB.B26R1",
            "MB.C26R1",
            "MB.A27R1",
            "MB.B27R1",
            "MB.C27R1",
            "MB.A28R1",
            "MB.B28R1",
            "MB.C28R1",
            "MB.A29R1",
            "MB.B29R1",
            "MB.C29R1",
            "MB.A30R1",
            "MB.B30R1",
            "MB.C30R1",
            "MB.A31R1",
            "MB.B31R1",
            "MB.C31R1",
            "MB.A32R1",
            "MB.B32R1",
            "MB.C32R1",
            "MB.A33R1",
            "MB.B33R1",
            "MB.C33R1",
            "MB.A34R1",
            "MB.B34R1",
            "MB.C34R1",
            "MB.C34L2",
            "MB.B34L2",
            "MB.A34L2",
            "MB.C33L2",
            "MB.B33L2",
            "MB.A33L2",
            "MB.C32L2",
            "MB.B32L2",
            "MB.A32L2",
            "MB.C31L2",
            "MB.B31L2",
            "MB.A31L2",
            "MB.C30L2",
            "MB.B30L2",
            "MB.A30L2",
            "MB.C29L2",
            "MB.B29L2",
            "MB.A29L2",
            "MB.C28L2",
            "MB.B28L2",
            "MB.A28L2",
            "MB.C27L2",
            "MB.B27L2",
            "MB.A27L2",
            "MB.C26L2",
            "MB.B26L2",
            "MB.A26L2",
            "MB.C25L2",
            "MB.B25L2",
            "MB.A25L2",
            "MB.C24L2",
            "MB.B24L2",
            "MB.A24L2",
            "MB.C23L2",
            "MB.B23L2",
            "MB.A23L2",
            "MB.C22L2",
            "MB.B22L2",
            "MB.A22L2",
            "MB.C21L2",
            "MB.B21L2",
            "MB.A21L2",
            "MB.C20L2",
            "MB.B20L2",
            "MB.A20L2",
            "MB.C19L2",
            "MB.B19L2",
            "MB.A19L2",
            "MB.C18L2",
            "MB.B18L2",
            "MB.A18L2",
            "MB.C17L2",
            "MB.B17L2",
            "MB.A17L2",
            "MB.C16L2",
            "MB.B16L2",
            "MB.A16L2",
            "MB.C15L2",
            "MB.B15L2",
            "MB.A15L2",
            "MB.C14L2",
            "MB.B14L2",
            "MB.A14L2",
            "MB.C13L2",
            "MB.B13L2",
            "MB.A13L2",
            "MB.C12L2",
            "MB.B12L2",
            "MB.A12L2",
            "MB.B11L2",
            "MB.A11L2",
            "MB.B10L2",
            "MB.A10L2",
            "MB.B9L2",
            "MB.A9L2",
            "MB.B8L2",
            "MB.A8L2",
        ]

        magnet_names_exp.sort()
        magnet_names_act.sort()
        self.assertEqual(magnet_names_exp, magnet_names_act)

    def test_getMagnetsForCircuitNames_RBA12_RBA34(self):
        # arrange
        circuit_type = "RB"
        circuit_names = ["RB.A12", "RB.A34"]

        # act
        magnet_names_act = MappingMetadata.get_magnets_for_circuit_names(circuit_type, circuit_names)

        # assert
        magnet_names_exp = [
            "MB.A8R1",
            "MB.B8R1",
            "MB.A9R1",
            "MB.B9R1",
            "MB.A10R1",
            "MB.B10R1",
            "MB.A11R1",
            "MB.B11R1",
            "MB.A12R1",
            "MB.B12R1",
            "MB.C12R1",
            "MB.A13R1",
            "MB.B13R1",
            "MB.C13R1",
            "MB.A14R1",
            "MB.B14R1",
            "MB.C14R1",
            "MB.A15R1",
            "MB.B15R1",
            "MB.C15R1",
            "MB.A16R1",
            "MB.B16R1",
            "MB.C16R1",
            "MB.A17R1",
            "MB.B17R1",
            "MB.C17R1",
            "MB.A18R1",
            "MB.B18R1",
            "MB.C18R1",
            "MB.A19R1",
            "MB.B19R1",
            "MB.C19R1",
            "MB.A20R1",
            "MB.B20R1",
            "MB.C20R1",
            "MB.A21R1",
            "MB.B21R1",
            "MB.C21R1",
            "MB.A22R1",
            "MB.B22R1",
            "MB.C22R1",
            "MB.A23R1",
            "MB.B23R1",
            "MB.C23R1",
            "MB.A24R1",
            "MB.B24R1",
            "MB.C24R1",
            "MB.A25R1",
            "MB.B25R1",
            "MB.C25R1",
            "MB.A26R1",
            "MB.B26R1",
            "MB.C26R1",
            "MB.A27R1",
            "MB.B27R1",
            "MB.C27R1",
            "MB.A28R1",
            "MB.B28R1",
            "MB.C28R1",
            "MB.A29R1",
            "MB.B29R1",
            "MB.C29R1",
            "MB.A30R1",
            "MB.B30R1",
            "MB.C30R1",
            "MB.A31R1",
            "MB.B31R1",
            "MB.C31R1",
            "MB.A32R1",
            "MB.B32R1",
            "MB.C32R1",
            "MB.A33R1",
            "MB.B33R1",
            "MB.C33R1",
            "MB.A34R1",
            "MB.B34R1",
            "MB.C34R1",
            "MB.C34L2",
            "MB.B34L2",
            "MB.A34L2",
            "MB.C33L2",
            "MB.B33L2",
            "MB.A33L2",
            "MB.C32L2",
            "MB.B32L2",
            "MB.A32L2",
            "MB.C31L2",
            "MB.B31L2",
            "MB.A31L2",
            "MB.C30L2",
            "MB.B30L2",
            "MB.A30L2",
            "MB.C29L2",
            "MB.B29L2",
            "MB.A29L2",
            "MB.C28L2",
            "MB.B28L2",
            "MB.A28L2",
            "MB.C27L2",
            "MB.B27L2",
            "MB.A27L2",
            "MB.C26L2",
            "MB.B26L2",
            "MB.A26L2",
            "MB.C25L2",
            "MB.B25L2",
            "MB.A25L2",
            "MB.C24L2",
            "MB.B24L2",
            "MB.A24L2",
            "MB.C23L2",
            "MB.B23L2",
            "MB.A23L2",
            "MB.C22L2",
            "MB.B22L2",
            "MB.A22L2",
            "MB.C21L2",
            "MB.B21L2",
            "MB.A21L2",
            "MB.C20L2",
            "MB.B20L2",
            "MB.A20L2",
            "MB.C19L2",
            "MB.B19L2",
            "MB.A19L2",
            "MB.C18L2",
            "MB.B18L2",
            "MB.A18L2",
            "MB.C17L2",
            "MB.B17L2",
            "MB.A17L2",
            "MB.C16L2",
            "MB.B16L2",
            "MB.A16L2",
            "MB.C15L2",
            "MB.B15L2",
            "MB.A15L2",
            "MB.C14L2",
            "MB.B14L2",
            "MB.A14L2",
            "MB.C13L2",
            "MB.B13L2",
            "MB.A13L2",
            "MB.C12L2",
            "MB.B12L2",
            "MB.A12L2",
            "MB.B11L2",
            "MB.A11L2",
            "MB.B10L2",
            "MB.A10L2",
            "MB.B9L2",
            "MB.A9L2",
            "MB.B8L2",
            "MB.A8L2",
            "MB.A8R3",
            "MB.B8R3",
            "MB.A9R3",
            "MB.B9R3",
            "MB.A10R3",
            "MB.B10R3",
            "MB.A11R3",
            "MB.B11R3",
            "MB.A12R3",
            "MB.B12R3",
            "MB.C12R3",
            "MB.A13R3",
            "MB.B13R3",
            "MB.C13R3",
            "MB.A14R3",
            "MB.B14R3",
            "MB.C14R3",
            "MB.A15R3",
            "MB.B15R3",
            "MB.C15R3",
            "MB.A16R3",
            "MB.B16R3",
            "MB.C16R3",
            "MB.A17R3",
            "MB.B17R3",
            "MB.C17R3",
            "MB.A18R3",
            "MB.B18R3",
            "MB.C18R3",
            "MB.A19R3",
            "MB.B19R3",
            "MB.C19R3",
            "MB.A20R3",
            "MB.B20R3",
            "MB.C20R3",
            "MB.A21R3",
            "MB.B21R3",
            "MB.C21R3",
            "MB.A22R3",
            "MB.B22R3",
            "MB.C22R3",
            "MB.A23R3",
            "MB.B23R3",
            "MB.C23R3",
            "MB.A24R3",
            "MB.B24R3",
            "MB.C24R3",
            "MB.A25R3",
            "MB.B25R3",
            "MB.C25R3",
            "MB.A26R3",
            "MB.B26R3",
            "MB.C26R3",
            "MB.A27R3",
            "MB.B27R3",
            "MB.C27R3",
            "MB.A28R3",
            "MB.B28R3",
            "MB.C28R3",
            "MB.A29R3",
            "MB.B29R3",
            "MB.C29R3",
            "MB.A30R3",
            "MB.B30R3",
            "MB.C30R3",
            "MB.A31R3",
            "MB.B31R3",
            "MB.C31R3",
            "MB.A32R3",
            "MB.B32R3",
            "MB.C32R3",
            "MB.A33R3",
            "MB.B33R3",
            "MB.C33R3",
            "MB.A34R3",
            "MB.B34R3",
            "MB.C34R3",
            "MB.C34L4",
            "MB.B34L4",
            "MB.A34L4",
            "MB.C33L4",
            "MB.B33L4",
            "MB.A33L4",
            "MB.C32L4",
            "MB.B32L4",
            "MB.A32L4",
            "MB.C31L4",
            "MB.B31L4",
            "MB.A31L4",
            "MB.C30L4",
            "MB.B30L4",
            "MB.A30L4",
            "MB.C29L4",
            "MB.B29L4",
            "MB.A29L4",
            "MB.C28L4",
            "MB.B28L4",
            "MB.A28L4",
            "MB.C27L4",
            "MB.B27L4",
            "MB.A27L4",
            "MB.C26L4",
            "MB.B26L4",
            "MB.A26L4",
            "MB.C25L4",
            "MB.B25L4",
            "MB.A25L4",
            "MB.C24L4",
            "MB.B24L4",
            "MB.A24L4",
            "MB.C23L4",
            "MB.B23L4",
            "MB.A23L4",
            "MB.C22L4",
            "MB.B22L4",
            "MB.A22L4",
            "MB.C21L4",
            "MB.B21L4",
            "MB.A21L4",
            "MB.C20L4",
            "MB.B20L4",
            "MB.A20L4",
            "MB.C19L4",
            "MB.B19L4",
            "MB.A19L4",
            "MB.C18L4",
            "MB.B18L4",
            "MB.A18L4",
            "MB.C17L4",
            "MB.B17L4",
            "MB.A17L4",
            "MB.C16L4",
            "MB.B16L4",
            "MB.A16L4",
            "MB.C15L4",
            "MB.B15L4",
            "MB.A15L4",
            "MB.C14L4",
            "MB.B14L4",
            "MB.A14L4",
            "MB.C13L4",
            "MB.B13L4",
            "MB.A13L4",
            "MB.C12L4",
            "MB.B12L4",
            "MB.A12L4",
            "MB.B11L4",
            "MB.A11L4",
            "MB.B10L4",
            "MB.A10L4",
            "MB.B9L4",
            "MB.A9L4",
            "MB.B8L4",
            "MB.A8L4",
        ]

        magnet_names_exp.sort()
        magnet_names_act.sort()
        self.assertEqual(magnet_names_exp, magnet_names_act)

    def test_getCratesForCircuitName_RB_A81(self):
        # arrange
        circuit_type = "RB"
        circuit_names = "RB.A81"

        # act
        crates_for_circuit_names_act = MappingMetadata.get_crates_for_circuit_names(circuit_type, circuit_names)

        # assert
        crates_for_circuit_names_exp = [
            "B9R8",
            "B11R8",
            "B13R8",
            "B15R8",
            "B17R8",
            "B19R8",
            "B21R8",
            "B23R8",
            "B25R8",
            "B27R8",
            "B29R8",
            "B31R8",
            "B33R8",
            "B34L1",
            "B32L1",
            "B30L1",
            "B28L1",
            "B26L1",
            "B24L1",
            "B22L1",
            "B20L1",
            "B18L1",
            "B16L1",
            "B14L1",
            "B12L1",
            "B10L1",
            "B8L1",
            "B9L1",
            "B11L1",
            "B13L1",
            "B15L1",
            "B17L1",
            "B19L1",
            "B21L1",
            "B23L1",
            "B25L1",
            "B27L1",
            "B29L1",
            "B31L1",
            "B33L1",
            "B34R8",
            "B32R8",
            "B30R8",
            "B28R8",
            "B26R8",
            "B24R8",
            "B22R8",
            "B20R8",
            "B18R8",
            "B16R8",
            "B14R8",
            "B12R8",
            "B10R8",
            "B8R8",
        ]

        crates_for_circuit_names_exp.sort()
        crates_for_circuit_names_act.sort()
        self.assertEqual(crates_for_circuit_names_exp, crates_for_circuit_names_act)

    def test_getCratesForCircuitName_RB_A81_A12(self):
        # arrange
        circuit_type = "RB"
        circuit_names = ["RB.A81", "RB.A12"]

        # act
        crates_for_circuit_names_act = MappingMetadata.get_crates_for_circuit_names(circuit_type, circuit_names)

        # assert
        crates_for_circuit_names_exp = [
            "B9R8",
            "B11R8",
            "B13R8",
            "B15R8",
            "B17R8",
            "B19R8",
            "B21R8",
            "B23R8",
            "B25R8",
            "B27R8",
            "B29R8",
            "B31R8",
            "B33R8",
            "B34L1",
            "B32L1",
            "B30L1",
            "B28L1",
            "B26L1",
            "B24L1",
            "B22L1",
            "B20L1",
            "B18L1",
            "B16L1",
            "B14L1",
            "B12L1",
            "B10L1",
            "B8L1",
            "B9L1",
            "B11L1",
            "B13L1",
            "B15L1",
            "B17L1",
            "B19L1",
            "B21L1",
            "B23L1",
            "B25L1",
            "B27L1",
            "B29L1",
            "B31L1",
            "B33L1",
            "B34R8",
            "B32R8",
            "B30R8",
            "B28R8",
            "B26R8",
            "B24R8",
            "B22R8",
            "B20R8",
            "B18R8",
            "B16R8",
            "B14R8",
            "B12R8",
            "B10R8",
            "B8R8",
            "B8L2",
            "B10L2",
            "B12L2",
            "B14L2",
            "B16L2",
            "B18L2",
            "B20L2",
            "B22L2",
            "B24L2",
            "B26L2",
            "B28L2",
            "B30L2",
            "B32L2",
            "B34L2",
            "B33R1",
            "B31R1",
            "B29R1",
            "B27R1",
            "B25R1",
            "B23R1",
            "B21R1",
            "B19R1",
            "B17R1",
            "B15R1",
            "B13R1",
            "B11R1",
            "B9R1",
            "B8R1",
            "B10R1",
            "B12R1",
            "B14R1",
            "B16R1",
            "B18R1",
            "B20R1",
            "B22R1",
            "B24R1",
            "B26R1",
            "B28R1",
            "B30R1",
            "B32R1",
            "B34R1",
            "B33L2",
            "B31L2",
            "B29L2",
            "B27L2",
            "B25L2",
            "B23L2",
            "B21L2",
            "B19L2",
            "B17L2",
            "B15L2",
            "B13L2",
            "B11L2",
            "B9L2",
        ]

        crates_for_circuit_names_exp.sort()
        crates_for_circuit_names_act.sort()
        self.assertEqual(crates_for_circuit_names_exp, crates_for_circuit_names_act)

    def test_getVoltageFeelersForCircuitName_RB_A81(self):
        # arrange
        circuit_type = "RB"
        circuit_names = "RB.A81"

        # act
        vf_for_circuit_names_act = MappingMetadata.get_vf_for_circuit_names(circuit_type, circuit_names)

        # assert
        vf_for_circuit_names_exp = [
            "DFLAS.7R8.5",
            "MB.B9R8",
            "MB.B12R8",
            "MB.B14R8",
            "MB.B16R8",
            "MB.B18R8",
            "MB.B20R8",
            "MB.B22R8",
            "MB.B24R8",
            "MB.B26R8",
            "MB.B28R8",
            "MB.B30R8",
            "MB.B32R8",
            "MB.B34R8",
            "MB.A34L1",
            "MB.A32L1",
            "MB.A30L1",
            "MB.A28L1",
            "MB.A26L1",
            "MB.A24L1",
            "MB.A22L1",
            "MB.A20L1",
            "MB.A18L1",
            "MB.A16L1",
            "MB.A14L1",
            "MB.A12L1",
            "MB.A9L1",
            "DFLAS.7L1.2",
            "MB.B9L1",
            "MB.B12L1",
            "MB.B14L1",
            "MB.B16L1",
            "MB.B18L1",
            "MB.B20L1",
            "MB.B22L1",
            "MB.B24L1",
            "MB.B26L1",
            "MB.B28L1",
            "MB.B30L1",
            "MB.B32L1",
            "MB.B34L1",
            "MB.A34R8",
            "MB.A32R8",
            "MB.A30R8",
            "MB.A28R8",
            "MB.A26R8",
            "MB.A24R8",
            "MB.A22R8",
            "MB.A20R8",
            "MB.A18R8",
            "MB.A16R8",
            "MB.A14R8",
            "MB.A12R8",
            "MB.A9R8",
        ]

        vf_for_circuit_names_exp.sort()
        vf_for_circuit_names_act.sort()
        self.assertEqual(vf_for_circuit_names_exp, vf_for_circuit_names_act)

    def test_getVoltageFeelersForCircuitName_RB_A78(self):
        # arrange
        circuit_type = "RB"
        circuit_names = "RB.A78"

        # act
        vf_for_circuit_names_act = MappingMetadata.get_vf_for_circuit_names(circuit_type, circuit_names)

        # assert
        vf_for_circuit_names_exp = [
            "DFLAS.7L8.5",
            "MB.A9L8",
            "MB.A12L8",
            "MB.A14L8",
            "MB.A16L8",
            "MB.A18L8",
            "MB.A20L8",
            "MB.A22L8",
            "MB.A24L8",
            "MB.A26L8",
            "MB.A28L8",
            "MB.A30L8",
            "MB.A32L8",
            "MB.A34L8",
            "MB.C33R7",
            "MB.B32R7",
            "MB.B30R7",
            "MB.B28R7",
            "MB.B26R7",
            "MB.B24R7",
            "MB.B22R7",
            "MB.B20R7",
            "MB.B18R7",
            "MB.B16R7",
            "MB.B14R7",
            "MB.B12R7",
            "MB.B9R7",
            "DFLAS.7R7.2",
            "MB.A9R7",
            "MB.A12R7",
            "MB.A14R7",
            "MB.A16R7",
            "MB.A18R7",
            "MB.A20R7",
            "MB.A22R7",
            "MB.A24R7",
            "MB.A26R7",
            "MB.A28R7",
            "MB.A30R7",
            "MB.A32R7",
            "MB.A34R7",
            "MB.B34L8",
            "MB.B32L8",
            "MB.B30L8",
            "MB.B28L8",
            "MB.B26L8",
            "MB.B24L8",
            "MB.B22L8",
            "MB.B20L8",
            "MB.B18L8",
            "MB.B16L8",
            "MB.B14L8",
            "MB.B12L8",
            "MB.B9L8",
        ]

        vf_for_circuit_names_exp.sort()
        vf_for_circuit_names_act.sort()
        self.assertEqual(vf_for_circuit_names_exp, vf_for_circuit_names_act)

    def test_getVoltageFeelersForCircuitName_RB_A81_A12(self):
        # arrange
        circuit_type = "RB"
        circuit_names = ["RB.A81", "RB.A12"]

        # act
        vf_for_circuit_names_act = MappingMetadata.get_vf_for_circuit_names(circuit_type, circuit_names)

        # assert
        vf_for_circuit_names_exp = [
            "DFLAS.7R8.5",
            "MB.B9R8",
            "MB.B12R8",
            "MB.B14R8",
            "MB.B16R8",
            "MB.B18R8",
            "MB.B20R8",
            "MB.B22R8",
            "MB.B24R8",
            "MB.B26R8",
            "MB.B28R8",
            "MB.B30R8",
            "MB.B32R8",
            "MB.B34R8",
            "MB.A34L1",
            "MB.A32L1",
            "MB.A30L1",
            "MB.A28L1",
            "MB.A26L1",
            "MB.A24L1",
            "MB.A22L1",
            "MB.A20L1",
            "MB.A18L1",
            "MB.A16L1",
            "MB.A14L1",
            "MB.A12L1",
            "MB.A9L1",
            "DFLAS.7L1.2",
            "MB.B9L1",
            "MB.B12L1",
            "MB.B14L1",
            "MB.B16L1",
            "MB.B18L1",
            "MB.B20L1",
            "MB.B22L1",
            "MB.B24L1",
            "MB.B26L1",
            "MB.B28L1",
            "MB.B30L1",
            "MB.B32L1",
            "MB.B34L1",
            "MB.A34R8",
            "MB.A32R8",
            "MB.A30R8",
            "MB.A28R8",
            "MB.A26R8",
            "MB.A24R8",
            "MB.A22R8",
            "MB.A20R8",
            "MB.A18R8",
            "MB.A16R8",
            "MB.A14R8",
            "MB.A12R8",
            "MB.A9R8",
            "MB.A8L2",
            "MB.A10L2",
            "MB.C12L2",
            "MB.C14L2",
            "MB.C16L2",
            "MB.C18L2",
            "MB.C20L2",
            "MB.C22L2",
            "MB.C24L2",
            "MB.C26L2",
            "MB.C28L2",
            "MB.C30L2",
            "MB.C32L2",
            "MB.C34L2",
            "MB.A33R1",
            "MB.C31R1",
            "MB.C29R1",
            "MB.C27R1",
            "MB.C25R1",
            "MB.C23R1",
            "MB.C21R1",
            "MB.C19R1",
            "MB.C17R1",
            "MB.C15R1",
            "MB.C13R1",
            "MB.B11R1",
            "MB.B8R1",
            "MB.A8R1",
            "MB.A10R1",
            "MB.C12R1",
            "MB.C14R1",
            "MB.C16R1",
            "MB.C18R1",
            "MB.C20R1",
            "MB.C22R1",
            "MB.C24R1",
            "MB.C26R1",
            "MB.C28R1",
            "MB.C30R1",
            "MB.C32R1",
            "MB.C34R1",
            "MB.C33L2",
            "MB.C31L2",
            "MB.C29L2",
            "MB.C27L2",
            "MB.C25L2",
            "MB.C23L2",
            "MB.C21L2",
            "MB.C19L2",
            "MB.C17L2",
            "MB.C15L2",
            "MB.C13L2",
            "MB.B11L2",
            "MB.B8L2",
        ]

        vf_for_circuit_names_exp.sort()
        vf_for_circuit_names_act.sort()
        self.assertEqual(vf_for_circuit_names_exp, vf_for_circuit_names_act)

    def test_getBusbarsForCircuitName_RB_A12(self):
        # arrange
        circuit_type = "RB"
        circuit_names = "RB.A12"

        # act
        bb_for_circuit_names_act = MappingMetadata.get_busbars_for_circuit_name(circuit_type, circuit_names)

        # assert
        bb_for_circuit_names_exp = [
            "DCBB.8L2.R",
            "DCBB.9L2.R",
            "DCBB.10L2.R",
            "DCBB.11L2.R",
            "DCBB.A12L2.R",
            "DCBB.B12L2.R",
            "DCBB.13L2.R",
            "DCBB.A14L2.R",
            "DCBB.B14L2.R",
            "DCBB.15L2.R",
            "DCBB.A16L2.R",
            "DCBB.B16L2.R",
            "DCBB.17L2.R",
            "DCBB.A18L2.R",
            "DCBB.B18L2.R",
            "DCBB.19L2.R",
            "DCBB.A20L2.R",
            "DCBB.B20L2.R",
            "DCBB.21L2.R",
            "DCBB.A22L2.R",
            "DCBB.B22L2.R",
            "DCBB.23L2.R",
            "DCBB.A24L2.R",
            "DCBB.B24L2.R",
            "DCBB.25L2.R",
            "DCBB.A26L2.R",
            "DCBB.B26L2.R",
            "DCBB.27L2.R",
            "DCBB.A28L2.R",
            "DCBB.B28L2.R",
            "DCBB.29L2.R",
            "DCBB.A30L2.R",
            "DCBB.B30L2.R",
            "DCBB.31L2.R",
            "DCBB.A32L2.R",
            "DCBB.B32L2.R",
            "DCBB.33L2.R",
            "DCBB.A34L2.R",
            "DCBB.B34L2.R",
            "DCBB.34R1.R",
            "DCBB.B33R1.R",
            "DCBB.A33R1.R",
            "DCBB.32R1.R",
            "DCBB.B31R1.R",
            "DCBB.A31R1.R",
            "DCBB.30R1.R",
            "DCBB.B29R1.R",
            "DCBB.A29R1.R",
            "DCBB.28R1.R",
            "DCBB.B27R1.R",
            "DCBB.A27R1.R",
            "DCBB.26R1.R",
            "DCBB.B25R1.R",
            "DCBB.A25R1.R",
            "DCBB.24R1.R",
            "DCBB.B23R1.R",
            "DCBB.A23R1.R",
            "DCBB.22R1.R",
            "DCBB.B21R1.R",
            "DCBB.A21R1.R",
            "DCBB.20R1.R",
            "DCBB.B19R1.R",
            "DCBB.A19R1.R",
            "DCBB.18R1.R",
            "DCBB.B17R1.R",
            "DCBB.A17R1.R",
            "DCBB.16R1.R",
            "DCBB.B15R1.R",
            "DCBB.A15R1.R",
            "DCBB.14R1.R",
            "DCBB.B13R1.R",
            "DCBB.A13R1.R",
            "DCBB.12R1.R",
            "DCBB.11R1.R",
            "DCBB.10R1.R",
            "DCBB.9R1.R",
            "DCBB.8R1.R",
            "DCBD.7R1.R",
            "DCBQ.7R1.L",
            "DCBQ.8R1.L",
            "DCBQ.9R1.L",
            "DCBQ.10R1.L",
            "DCBQ.11R1.L",
            "DCBB.12R1.L",
            "DCBB.A13R1.L",
            "DCBQ.13R1.L",
            "DCBB.14R1.L",
            "DCBB.A15R1.L",
            "DCBQ.15R1.L",
            "DCBB.16R1.L",
            "DCBB.A17R1.L",
            "DCBQ.17R1.L",
            "DCBB.18R1.L",
            "DCBB.A19R1.L",
            "DCBQ.19R1.L",
            "DCBB.20R1.L",
            "DCBB.A21R1.L",
            "DCBQ.21R1.L",
            "DCBB.22R1.L",
            "DCBB.A23R1.L",
            "DCBQ.23R1.L",
            "DCBB.24R1.L",
            "DCBB.A25R1.L",
            "DCBQ.25R1.L",
            "DCBB.26R1.L",
            "DCBB.A27R1.L",
            "DCBQ.27R1.L",
            "DCBB.28R1.L",
            "DCBB.A29R1.L",
            "DCBQ.29R1.L",
            "DCBB.30R1.L",
            "DCBB.A31R1.L",
            "DCBQ.31R1.L",
            "DCBB.32R1.L",
            "DCBB.A33R1.L",
            "DCBQ.33R1.L",
            "DCBB.34R1.L",
            "DCBB.B34L2.L",
            "DCBQ.33L2.L",
            "DCBB.33L2.L",
            "DCBB.B32L2.L",
            "DCBQ.31L2.L",
            "DCBB.31L2.L",
            "DCBB.B30L2.L",
            "DCBQ.29L2.L",
            "DCBB.29L2.L",
            "DCBB.B28L2.L",
            "DCBQ.27L2.L",
            "DCBB.27L2.L",
            "DCBB.A26L2.L",
            "DCBQ.25L2.L",
            "DCBB.25L2.L",
            "DCBB.B24L2.L",
            "DCBQ.23L2.L",
            "DCBB.23L2.L",
            "DCBB.B22L2.L",
            "DCBQ.21L2.L",
            "DCBB.21L2.L",
            "DCBB.B20L2.L",
            "DCBQ.19L2.L",
            "DCBB.19L2.L",
            "DCBB.B18L2.L",
            "DCBQ.17L2.L",
            "DCBB.17L2.L",
            "DCBB.B16L2.L",
            "DCBQ.15L2.L",
            "DCBB.15L2.L",
            "DCBB.B14L2.L",
            "DCBQ.13L2.L",
            "DCBB.13L2.L",
            "DCBB.A12L2.L",
            "DCBQ.11L2.L",
            "DCBQ.10L2.L",
            "DCBQ.9L2.L",
            "DCBQ.8L2.L",
            "DCBD.7L2.L",
        ]

        bb_for_circuit_names_exp.sort()
        bb_for_circuit_names_act.sort()
        self.assertEqual(bb_for_circuit_names_exp, bb_for_circuit_names_act)

    def test_getCrateNameFromMagnetName_RB_A30L2(self):
        # arrange
        circuit_type = "RB"
        magnet_name = "A30L2"

        # act
        crate_act = MappingMetadata.get_crate_name_from_magnet_name(circuit_type, magnet_name)

        # assert
        crate_exp = "B30L2"
        self.assertEqual(crate_exp, crate_act)

    def test_getCrateNameFromMagnetName_RQ_25R4(self):
        # arrange
        circuit_type = "RQ"
        magnet_name = "25R4"

        # act
        crate_act = MappingMetadata.get_crate_name_from_magnet_name(circuit_type, magnet_name)

        # assert
        crate_exp = "B26R4"
        self.assertEqual(crate_exp, crate_act)

    def test_getCrateNameFromBusbarName_RB_DCBB8L2(self):
        # arrange
        circuit_type = "RB"
        busbar_name = "DCBB.8L2.R"

        # act
        crate_act = MappingMetadata.get_crate_name_from_busbar_names(circuit_type, busbar_name)

        # assert
        crate_exp = ["B8L2"]
        self.assertEqual(crate_exp, crate_act)

    def test_getCrateNameFromBusbarName_RB_DCBB8L2_DCBB15L2(self):
        # arrange
        circuit_type = "RB"
        busbar_names = ["DCBB.8L2.R", "DCBB.15L2.R"]

        # act
        crate_act = MappingMetadata.get_crate_name_from_busbar_names(circuit_type, busbar_names)

        # assert
        crate_exp = ["B8L2", "B14L2"]

        crate_exp.sort()
        crate_act.sort()
        self.assertEqual(crate_exp, crate_act)

    def test_get_u_res_busbar_rq(self):
        # arrange
        circuit_type = "RQ"
        u_suffix = "U_RES"

        # act
        circuit_names = signal_metadata.get_circuit_names(circuit_type)
        u_res_names = MappingMetadata.get_busbars_for_circuit_name(circuit_type, circuit_names)
        u_res = ["{}:{}".format(u_res_name, u_suffix) for u_res_name in u_res_names]

        # assert
        u_res_exp = [
            "DCQDD.7L2.L:U_RES",
            "DCQFD.7L2.R:U_RES",
            "DCQDB.A12L2.L:U_RES",
            "DCQFB.A12L2.R:U_RES",
            "DCQDB.A14L2.L:U_RES",
            "DCQFB.A14L2.R:U_RES",
            "DCQDB.A16L2.L:U_RES",
            "DCQFB.A16L2.R:U_RES",
            "DCQDB.A18L2.L:U_RES",
            "DCQFB.A18L2.R:U_RES",
        ]

        u_res_act = u_res[:10]

        u_res_exp.sort()
        u_res_act.sort()
        self.assertListEqual(u_res_exp, u_res_act)

    def test_getMagnetNamesFromCrateName_RB_B25R1(self):
        """Tests that Metadata.getCrateToMagnetAndVoltageFeelerMapping() returns correct signal"""

        # arrange
        crate_name = "B25R1"
        circuit_type = "RB"

        # act
        magnets_act = MappingMetadata.get_magnet_names_from_crate_name(circuit_type, crate_name)

        # assert
        magnets_exp = ["MB.B24R1", "MB.A25R1", "MB.C25R1"]
        magnets_act.sort()
        magnets_exp.sort()
        self.assertListEqual(magnets_exp, magnets_act)

    def test_getMagnetNamesFromCrateName_RB_B31R1(self):
        """Tests that Metadata.getCrateToMagnetAndVoltageFeelerMapping() returns correct signal"""

        # arrange
        crate_name = "B31R1"
        circuit_type = "RB"

        # act
        magnets_act = MappingMetadata.get_magnet_names_from_crate_name(circuit_type, crate_name)

        # assert
        magnets_exp = ["MB.A31R1", "MB.B30R1", "MB.C31R1"]
        magnets_act.sort()
        magnets_exp.sort()
        self.assertListEqual(magnets_exp, magnets_act)

    def test_getMagnetNamesFromCrateName_RB_B9R1(self):
        """Tests that Metadata.getCrateToMagnetAndVoltageFeelerMapping() returns correct signal"""

        # arrange
        crate_name = "B9R1"
        circuit_type = "RB"

        # act
        magnets_act = MappingMetadata.get_magnet_names_from_crate_name(circuit_type, crate_name)

        # assert
        magnets_exp = ["MB.B8R1"]
        magnets_act.sort()
        magnets_exp.sort()
        self.assertListEqual(magnets_exp, magnets_act)

    def test_get_magnet_id_from_magnet_name(self):
        # arrange
        magnet_name = "MB.A8R1"
        circuit_type = "RB"

        # act
        magnet_id_act = MappingMetadata.get_magnet_id_from_magnet_name(circuit_type, magnet_name)

        # assert
        magnet_id_exp = 3288

        self.assertEqual(magnet_id_exp, magnet_id_act)

    def test_get_rq_aperture(self):
        # arrange
        circuit_name = "RQF.A12"
        magnet_name = "MQ.16L2"

        # act
        aperture_act = MappingMetadata.get_rq_aperture(circuit_name, magnet_name)

        # assert
        aperture_exp = "INT"

        self.assertEqual(aperture_exp, aperture_act)

    def test_get_electrical_position(self):
        # arrange
        circuit_type = "RB"
        magnet = "MB.A31R1"

        # act
        el_index_act = MappingMetadata.get_electrical_position(circuit_type, magnet)

        # assert
        el_index_exp = 45

        self.assertEqual(el_index_exp, el_index_act)

    def test_get_qps_schematic_naming_ipq(self):
        # arrange
        circuit_name = "RQ4.L2"

        # act
        qps_schematic_name = MappingMetadata.get_qps_schematic_naming("IPQ", circuit_name)

        # assert
        qps_schematic_name_ref = "2x2MQY"
        self.assertEqual(qps_schematic_name_ref, qps_schematic_name)

    def test_get_qps_schematic_naming_ipd_rd1_l2(self):
        # arrange
        circuit_name = "RD1.L2"

        # act
        qps_schematic_name = MappingMetadata.get_qps_schematic_naming("IPD", circuit_name)

        # assert
        qps_schematic_name_ref = "MBX"
        self.assertEqual(qps_schematic_name_ref, qps_schematic_name)

    def test_get_qps_schematic_naming_ipd_rd3_r4(self):
        # arrange
        circuit_name = "RD3.R4"

        # act
        qps_schematic_name = MappingMetadata.get_qps_schematic_naming("IPD", circuit_name)

        # assert
        qps_schematic_name_ref = "MBRS"
        self.assertEqual(qps_schematic_name_ref, qps_schematic_name)

    def test_read_busbar_to_magnet_mapping_error(self):
        # arrange
        circuit_type = "IT"

        # act
        with self.assertRaises(KeyError) as context:
            MappingMetadata.read_busbar_to_magnet_mapping(circuit_type)

        # assert
        self.assertTrue("Circuit type IT is not supported!" in context.exception.args[0])

    def test_get_qps_schematic_naming_error(self):
        # arrange
        circuit_type = "IT"
        circuit_name = "RQX.L1"

        # act
        with self.assertRaises(KeyError) as context:
            MappingMetadata.get_qps_schematic_naming(circuit_type, circuit_name)

        # assert
        self.assertTrue("Circuit type IT is not supported!" in context.exception.args[0])


PARAMS_CRATE_SIGNAL = [
    ("RQD.A12", "MQ.32L2", "B33L2", "U_DIODE_RQD"),
    ("RQD.A12", "MQ.34R1", "B33L2", "U_REF_N1"),
    ("RQF.A23", "MQ.34R2", "B33L3", "U_REF_N1"),
    ("RQF.A23", "MQ.32L3", "B33L3", "U_DIODE_RQF"),
]


@pytest.mark.parametrize("circuit_name,magnet_name,expected_crate_name,expected_signal_name", PARAMS_CRATE_SIGNAL)
def test_get_crate_and_diode_signal_name_for_rq(circuit_name, magnet_name, expected_crate_name, expected_signal_name):
    # act
    crate_name, signal_name = MappingMetadata.get_crate_and_diode_signal_name_for_rq(circuit_name, magnet_name)

    # assert
    assert (crate_name, signal_name) == (expected_crate_name, expected_signal_name)
