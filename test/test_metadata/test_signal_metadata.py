import re
import warnings

import pytest

from lhcsmapi.metadata import signal_metadata
from lhcsmapi.metadata.signal_metadata import GenericCircuitType


def test_get_metadata():
    circuit_type = "ITT"

    with pytest.raises(KeyError) as exception:
        signal_metadata._get_metadata(circuit_type)

    assert "ITT is not mapped to a json file!" in str(exception.value)


def test_get_metadata_for_circuit_name():
    circuit_type = "RB"
    circuit_name = "RQF.A45"

    with pytest.raises(ValueError) as exception:
        signal_metadata._get_metadata_for_circuit_name(circuit_type, circuit_name)

    assert "Circuit name RQF.A45 not present in metadata for RB" in str(exception.value)


_COMMON_SYSTEMS = [
    "BUSBAR",
    "CIP",
    "CRYO",
    "DIODE_RB",
    "EE_EVEN",
    "EE_ODD",
    "LEADS_EVEN",
    "LEADS_ODD",
    "PC",
    "PIC",
    "QDS",
    "QH",
    "VF",
]
_SYSTEMS = [
    (
        "RB",
        "RB.A45",
        [
            "BUSBAR",
            "CIP",
            "CRYO",
            "DIODE_RB",
            "EE_EVEN",
            "EE_ODD",
            "LEADS_EVEN",
            "LEADS_ODD",
            "PC",
            "PIC",
            "QDS",
            "QH",
            "VF",
        ],
    ),
    (
        "RQ",
        "RQD.A12",
        ["BUSBAR", "BUSBAR_RQD", "CIP", "CRYO", "DIODE_RQD", "EE", "LEADS", "PC", "PIC", "QDS", "QH", "VF_RQD"],
    ),
    ("IT", "RQX.L8", ["CIP", "CRYO_1.9K", "LEADS", "PC", "PIC", "QDS", "QH"]),
    ("IPD2", "RD1.L2", ["CIP", "CRYO_1.9K", "LEADS", "PC", "PIC", "QDS", "QH"]),
    ("IPQ2", "RQ5.L5", ["CIP", "CRYO_4.5", "LEADS_B1", "LEADS_B2", "PC", "PIC", "QDS", "QH"]),
    ("60A", "RCBH11.L4B1", ["CRYO_1.9K", "PC"]),
    ("600A", "RSF2.A56B2", ["CIP", "CRYO_1.9K", "EE", "LEADS", "PC", "PIC", "QDS"]),
    ("600A", "RSS.A12B1", ["CIP", "CRYO_1.9K", "LEADS", "PC", "PIC", "QDS"]),
    ("80-120A", "RCBCH10.L1B1", ["CIP", "CRYO_1.9K", "PC", "PIC"]),
]


@pytest.mark.parametrize("circuit_type,circuit_name,expected_systems", _SYSTEMS)
def test_get_system_types_per_circuit_name(circuit_type, circuit_name, expected_systems):
    systems = signal_metadata.get_system_types_per_circuit_name(circuit_type, circuit_name)

    assert sorted(systems) == sorted(expected_systems)


def test_get_circuit_types():
    circuit_types_act = signal_metadata.get_circuit_types()
    circuit_types_exp = ["IPD2", "IPD2_B1B2", "IPQ2", "IPQ4", "IPQ8", "IT", "RB", "RQ", "60A", "80-120A", "600A"]

    assert sorted(circuit_types_exp) == sorted(circuit_types_act)


_CIRCUIT_NAMES = [
    ("RB", ["RB.A12", "RB.A23", "RB.A34", "RB.A45", "RB.A56", "RB.A67", "RB.A78", "RB.A81"]),
    (
        ["IPD2", "IPD2_B1B2"],
        [
            "RD1.L2",
            "RD1.L8",
            "RD1.R2",
            "RD1.R8",
            "RD2.L1",
            "RD2.L2",
            "RD2.L5",
            "RD2.L8",
            "RD2.R1",
            "RD2.R2",
            "RD2.R5",
            "RD2.R8",
            "RD3.L4",
            "RD3.R4",
            "RD4.L4",
            "RD4.R4",
        ],
    ),
    (
        "RQ",
        [
            "RQD.A12",
            "RQD.A23",
            "RQD.A34",
            "RQD.A45",
            "RQD.A56",
            "RQD.A67",
            "RQD.A78",
            "RQD.A81",
            "RQF.A12",
            "RQF.A23",
            "RQF.A34",
            "RQF.A45",
            "RQF.A56",
            "RQF.A67",
            "RQF.A78",
            "RQF.A81",
        ],
    ),
]


@pytest.mark.parametrize("circuit_type,expected_names", _CIRCUIT_NAMES)
def test_get_circuit_names(circuit_type, expected_names):
    result_names = signal_metadata.get_circuit_names(circuit_type)

    assert sorted(expected_names) == sorted(result_names)


_PCS = [
    (
        "IPQ8",
        None,
        [
            "RPHH.UA23.RQ4.L2B1",
            "RPHH.UA23.RQ4.L2B2",
            "RPHH.UA83.RQ4.L8B1",
            "RPHH.UA83.RQ4.L8B2",
            "RPHH.UA27.RQ4.R2B1",
            "RPHH.UA27.RQ4.R2B2",
            "RPHH.UA87.RQ4.R8B1",
            "RPHH.UA87.RQ4.R8B2",
            "RPHH.UA23.RQ5.L2B1",
            "RPHH.UA23.RQ5.L2B2",
            "RPHH.UA87.RQ5.R8B1",
            "RPHH.UA87.RQ5.R8B2",
        ],
    ),
    ("RB", "RB.A12", ["RPTE.UA23.RB.A12"]),
    ("RQ", "RQF.A45", ["RPHE.UA47.RQF.A45"]),
    (["RQ", "RB"], ["RQD.A56", "RB.A67"], ["RPHE.UA63.RQD.A56", "RPTE.UA67.RB.A67"]),
]


@pytest.mark.parametrize("circuit_type,circuit_name,expected_pcs", _PCS)
def test_get_pc_names(circuit_type, circuit_name, expected_pcs):
    pc_names = signal_metadata.get_pc_names(circuit_type, circuit_name)

    assert pc_names == expected_pcs


@pytest.mark.parametrize(
    "circuit_name, timestamp, expected_fgcs",
    [
        ("RCBH11.L1B2", signal_metadata._RUN_3_START - 1, ["RPLA.12L1.RCBH11.L1B2"]),  # 60A
        ("RCBH11.L1B2", signal_metadata._RUN_3_START, ["RPLA.12L1.RCBH11.L1B2"]),  # 60A
        ("RCBCH10.L1B1", signal_metadata._RUN_3_START - 1, ["RPLB.RR13.RCBCH10.L1B1"]),  # 80-120A
        ("RCBCH10.L1B1", signal_metadata._RUN_3_START, ["RPLBB.RR13.RCBCH10.L1B1"]),  # 80-120A
        ("RCBXH1.L1", signal_metadata._RUN_3_START - 1, ["RPMBB.UL14.RCBXH1.L1"]),  # 600A
        ("RCBXH1.L1", signal_metadata._RUN_3_START, ["RPMBB.UL14.RCBXH1.L1"]),  # 600A
        (
            "RQX.L1",
            signal_metadata._RUN_3_START - 1,
            ["RPHFC.UL14.RQX.L1", "RPMBC.UL14.RTQX1.L1", "RPHGC.UL14.RTQX2.L1"],
        ),  # IT
        (
            "RQX.L1",
            signal_metadata._RUN_3_START,
            ["RPHFC.UL14.RQX.L1", "RPMBC.UL14.RTQX1.L1", "RPHGC.UL14.RTQX2.L1"],
        ),  # IT
        ("RD1.L2", signal_metadata._RUN_3_START - 1, ["RPHF.UA23.RD1.L2"]),  # IPD
        ("RD1.L2", signal_metadata._RUN_3_START, ["RPHF.UA23.RD1.L2"]),  # IPD
        ("RQ5.L1", signal_metadata._RUN_3_START - 1, ["RPHGB.RR13.RQ5.L1B1", "RPHGB.RR13.RQ5.L1B2"]),  # IPQ
        ("RQ5.L1", signal_metadata._RUN_3_START, ["RPHSB.RR13.RQ5.L1B1", "RPHSB.RR13.RQ5.L1B2"]),  # IPQ
        ("RB.A12", signal_metadata._RUN_3_START - 1, ["RPTE.UA23.RB.A12"]),  # RB
        ("RB.A12", signal_metadata._RUN_3_START, ["RPTE.UA23.RB.A12"]),  # RB
        ("RQD.A12", signal_metadata._RUN_3_START - 1, ["RPHE.UA23.RQD.A12"]),  # RQ
        ("RQD.A12", signal_metadata._RUN_3_START, ["RPHE.UA23.RQD.A12"]),  # RQ
    ],
)
def test_get_fgc_names_valid_circuit(circuit_name: str, timestamp: int, expected_fgcs: list[str]):
    assert signal_metadata.get_fgc_names(circuit_name, timestamp) == expected_fgcs


def test_get_fgc_names_invalid_circuit():
    with pytest.raises(ValueError) as e:
        signal_metadata.get_fgc_names("ABC", 0)

    assert str(e.value) == "Circuit name ABC not present in the metadata."


@pytest.mark.parametrize(
    ("circuit_name", "timestamp", "expected_ees"),
    [
        ("RCBH11.L1B2", signal_metadata._RUN_3_START - 1, []),  # 60A
        ("RCBH11.L1B2", signal_metadata._RUN_3_START, []),  # 60A
        ("RCBCH10.L1B1", signal_metadata._RUN_3_START - 1, []),  # 80-120A
        ("RCBCH10.L1B1", signal_metadata._RUN_3_START, []),  # 80-120A
        ("RCBXH1.L1", signal_metadata._RUN_3_START - 1, []),  # 600A without EE
        ("RCBXH1.L1", signal_metadata._RUN_3_START, []),  # 600A without EE
        ("RCD.A12B1", signal_metadata._RUN_3_START - 1, ["UA23.RCD.A12B1"]),  # 600A with EE
        ("RCD.A12B1", signal_metadata._RUN_3_START, ["UA23.RCD.A12B1"]),  # 600A with EE
        ("RQX.L1", signal_metadata._RUN_3_START - 1, []),  # IT
        ("RQX.L1", signal_metadata._RUN_3_START, []),  # IT
        ("RD1.L2", signal_metadata._RUN_3_START - 1, []),  # IPD
        ("RD1.L2", signal_metadata._RUN_3_START, []),  # IPD
        ("RQ5.L1", signal_metadata._RUN_3_START - 1, []),  # IPQ
        ("RQ5.L1", signal_metadata._RUN_3_START, []),  # IPQ
        ("RB.A12", signal_metadata._RUN_3_START - 1, ["RR17.RB.A12", "UA23.RB.A12"]),  # RB
        ("RB.A12", signal_metadata._RUN_3_START, ["RR17.RB.A12", "UA23.RB.A12"]),  # RB
        ("RQD.A12", signal_metadata._RUN_3_START - 1, ["UA23.RQD.A12"]),  # RQ
        ("RQD.A12", signal_metadata._RUN_3_START, ["UA23.RQD.A12"]),  # RQ
    ],
)
def test_get_ee_names_valid_circuit(circuit_name: str, timestamp: int, expected_ees: list[str]):
    assert signal_metadata.get_ee_names(circuit_name, timestamp) == expected_ees


def test_get_ee_names_invalid_circuit():
    with pytest.raises(ValueError, match=re.escape("Circuit name ABC not present in the metadata.")):
        signal_metadata.get_ee_names("ABC", 0)


@pytest.mark.parametrize("circuit_name", ["RCBH11.L1B2", "RCBCH10.L1B1"])  # 60A  # 80-120A
def test_get_dfb_names_circuit_without_dfb(circuit_name: str):
    with pytest.raises(ValueError, match=re.escape(f"Circuit {circuit_name} does not have a DFB.")):
        signal_metadata.get_dfb_names(circuit_name, 0)


@pytest.mark.parametrize(
    ("circuit_name", "timestamp", "expected_dfbs"),
    [
        ("RCBXH1.L1", signal_metadata._RUN_3_START - 1, ["DFLBS.3L1.RCBXH1.L1"]),  # 600A
        ("RCBXH1.L1", signal_metadata._RUN_3_START, ["DFLBS.3L1.RCBXH1.L1"]),  # 600A
        ("RQX.L1", signal_metadata._RUN_3_START - 1, ["DFLX.3L1.RQX.L1"]),  # IT
        ("RQX.L1", signal_metadata._RUN_3_START, ["DFLX.3L1.RQX.L1"]),  # IT
        ("RD1.L2", signal_metadata._RUN_3_START - 1, ["DFLCS.3L2.RD1.L2"]),  # IPD
        ("RD1.L2", signal_metadata._RUN_3_START, ["DFLCS.3L2.RD1.L2"]),  # IPD
        ("RQ5.L1", signal_metadata._RUN_3_START - 1, ["DFLCS.RR13.RQ5.L1"]),  # IPQ
        ("RQ5.L1", signal_metadata._RUN_3_START, ["DFLCS.RR13.RQ5.L1"]),  # IPQ
        ("RB.A12", signal_metadata._RUN_3_START - 1, ["DFLAS.7R1.RB.A12", "DFLAS.7L2.RB.A12"]),  # RB
        ("RB.A12", signal_metadata._RUN_3_START, ["DFLAS.7R1.RB.A12", "DFLAS.7L2.RB.A12"]),  # RB
        ("RQD.A12", signal_metadata._RUN_3_START - 1, ["DFLAS.7L2.RQD.A12"]),  # RQ
        ("RQD.A12", signal_metadata._RUN_3_START, ["DFLAS.7L2.RQD.A12"]),  # RQ
    ],
)
def test_get_dfb_names_valid_circuit(circuit_name: str, timestamp: int, expected_dfbs: list[str]):
    assert signal_metadata.get_dfb_names(circuit_name, timestamp) == expected_dfbs


@pytest.mark.parametrize("timestamp", [signal_metadata._RUN_3_START - 1, signal_metadata._RUN_3_START])
@pytest.mark.parametrize(
    "circuit_name",
    [
        circuit_name
        for circuit_type in signal_metadata.get_circuit_types()
        for circuit_name in signal_metadata.get_circuit_names(circuit_type)
    ],
)
def test_get_dfb_names_has_expected_length_of_1(circuit_name: str, timestamp: int) -> None:
    """Check that no circuit name raises an exception in the edge cases"""
    if signal_metadata.has_dfb(circuit_name):
        dfbs = signal_metadata.get_dfb_names(circuit_name, timestamp)
        if signal_metadata.is_main_dipole(circuit_name):
            assert len(dfbs) == 2
        else:
            assert len(dfbs) == 1


def test_get_dfb_names_invalid_circuit():
    with pytest.raises(ValueError, match=re.escape("Circuit name ABC not present in the metadata.")):
        signal_metadata.get_dfb_names("ABC", 0)


@pytest.mark.parametrize("circuit_name", ["RCBH11.L1B2", "RCBCH10.L1B1"])  # 60A  # 80-120A
def test_get_dfb_prefixes_valid_circuit_without_dfb(circuit_name: str):
    with pytest.raises(ValueError, match=re.escape(f"Circuit {circuit_name} does not have a DFB.")):
        signal_metadata.get_dfb_prefixes(circuit_name, "DFLBS.3L1.RCBXH1.L1", signal_metadata._RUN_3_START)


@pytest.mark.parametrize(
    ("circuit_name", "timestamp", "expected_prefixes"),
    [
        ("RCBXH1.L1", signal_metadata._RUN_3_START - 1, ["DXAY11_03L1_", "DXAY12_03L1_"]),  # 600A
        ("RCBXH1.L1", signal_metadata._RUN_3_START, ["DXAY11_03L1_", "DXAY12_03L1_"]),  # 600A
        (
            "RQX.L1",
            signal_metadata._RUN_3_START - 1,
            ["DXAX01_03L1_", "DXAX02_03L1_", "DXAX03_03L1_", "DXAX04_03L1_"],
        ),  # IT
        (
            "RQX.L1",
            signal_metadata._RUN_3_START,
            ["DXAX01_03L1_", "DXAX02_03L1_", "DXAX03_03L1_", "DXAX04_03L1_"],
        ),  # IT
        ("RD1.L2", signal_metadata._RUN_3_START - 1, ["DXCX05_03L2_", "DXCX06_03L2_"]),  # IPD
        ("RD1.L2", signal_metadata._RUN_3_START, ["DXCX05_03L2_", "DXCX06_03L2_"]),  # IPD
        ("RQ5.L1", signal_metadata._RUN_3_START - 1, ["DLAC06_RR13_", "DLAC07_RR13_", "DLAC08_RR13_"]),  # IPQ
        ("RQ5.L1", signal_metadata._RUN_3_START, ["DLAC06_RR13_", "DLAC07_RR13_", "DLAC08_RR13_"]),  # IPQ
        ("RQD.A12", signal_metadata._RUN_3_START - 1, ["DACA01_07L2_", "DACA02_07L2_"]),  # RQ
        ("RQD.A12", signal_metadata._RUN_3_START, ["DACA01_07L2_", "DACA02_07L2_"]),  # RQ
    ],
)
def test_get_dfb_prefixes_valid_circuit_one_dfb(circuit_name: str, timestamp: int, expected_prefixes: list[str]):
    [dfb] = signal_metadata.get_dfb_names(circuit_name, timestamp)
    assert signal_metadata.get_dfb_prefixes(circuit_name, dfb, timestamp) == expected_prefixes


@pytest.mark.parametrize("timestamp", [signal_metadata._RUN_3_START - 1, signal_metadata._RUN_3_START])
def test_get_dfb_prefixes_multiple_dfbs(timestamp: int):
    circuit_name = "RB.A12"
    dfb_odd, dfb_even = signal_metadata.get_dfb_names(circuit_name, timestamp)

    assert signal_metadata.get_dfb_prefixes(circuit_name, dfb_odd, timestamp) == ["DABA01_07R1_", "DABA02_07R1_"]
    assert signal_metadata.get_dfb_prefixes(circuit_name, dfb_even, timestamp) == ["DACA05_07L2_", "DACA06_07L2_"]


@pytest.mark.parametrize("timestamp", [signal_metadata._RUN_3_START - 1, signal_metadata._RUN_3_START])
@pytest.mark.parametrize(
    "circuit_name",
    [
        circuit_name
        for circuit_type in signal_metadata.get_circuit_types()
        for circuit_name in signal_metadata.get_circuit_names(circuit_type)
    ],
)
def test_get_dfb_prefixes_has_expected_length(circuit_name: str, timestamp: int):
    """Check that no circuit name raises an exception in the edge cases"""
    if signal_metadata.has_dfb(circuit_name):
        dfbs = signal_metadata.get_dfb_names(circuit_name, timestamp)
        for dfb in dfbs:
            dfb_prefixes = signal_metadata.get_dfb_prefixes(circuit_name, dfb, timestamp)
            if signal_metadata.is_inner_triplet(circuit_name):
                assert len(dfb_prefixes) == 4
            elif signal_metadata.is_ipq(circuit_name) or circuit_name.startswith("RCS"):
                assert len(dfb_prefixes) == 3
            elif circuit_name.startswith("RCO") and timestamp >= signal_metadata._RUN_3_START:
                assert len(dfb_prefixes) == 1
            else:
                assert len(dfb_prefixes) == 2


def test_get_dfb_prefixes_invalid_circuit():
    with pytest.raises(ValueError, match=re.escape("Circuit name ABC not present in the metadata.")):
        signal_metadata.get_dfb_prefixes("ABC", "ABC", 0)


def test_get_dfb_prefixes_invalid_dfb():
    with pytest.raises(ValueError, match=re.escape("DFB ABC not present in metadata for RB.A12.")):
        signal_metadata.get_dfb_prefixes("RB.A12", "ABC", 0)


_SIGNAL_NAMES = [
    (
        "RB",
        "RB.A12",
        "EE_EVEN",
        "NXCALS",
        ["T_RES_BODY_1", "T_RES_BODY_2", "T_RES_BODY_3"],
        ["DQRB.UA23.RB.A12:T_RES_BODY_1", "DQRB.UA23.RB.A12:T_RES_BODY_2", "DQRB.UA23.RB.A12:T_RES_BODY_3"],
    ),
    ("RQ", ["RQD.A12", "RQF.A12"], "EE", "NXCALS", "T_RES", ["DQRQ.UA23.RQD.A12:T_RES", "DQRQ.UA23.RQF.A12:T_RES"]),
    ("RB", "RB.A12", "EE_ODD", "NXCALS", "ST_RES_OVERTEMP", "DQRB.RR17.RB.A12:ST_RES_OVERTEMP"),
    (
        "RQ",
        ["RQD.A12", "RQF.A12"],
        "EE",
        "NXCALS",
        "ST_RES_OVERTEMP",
        ["DQRQ.UA23.RQD.A12:ST_RES_OVERTEMP", "DQRQ.UA23.RQF.A12:ST_RES_OVERTEMP"],
    ),
    ("RB", "RB.A12", "PC", "NXCALS", "I_MEAS", "RPTE.UA23.RB.A12:I_MEAS"),
    ("RQ", "RQD.A12", "PC", "NXCALS", "I_MEAS", "RPHE.UA23.RQD.A12:I_MEAS"),
    (
        "RB",
        "RB.A12",
        "QH",
        "NXCALS",
        "I_HDS",
        ["%MAGNET%:I_HDS_1", "%MAGNET%:I_HDS_2", "%MAGNET%:I_HDS_3", "%MAGNET%:I_HDS_4"],
    ),
    ("RB", "RB.A12", "PIC", "NXCALS", "ST_ABORT_PIC", ["RB.A12.ODD:ST_ABORT_PIC", "RB.A12.EVEN:ST_ABORT_PIC"]),
    (
        "RB",
        "RB.A12",
        "PC",
        "PM",
        ["I_REF", "I_MEAS", "V_REF", "V_MEAS", "I_EARTH", "I_EARTH_PCNT", "I_A", "I_B"],
        [
            "STATUS.I_REF",
            "STATUS.I_MEAS",
            "STATUS.V_REF",
            "STATUS.V_MEAS",
            "IEARTH.I_EARTH",
            "STATUS.I_EARTH_PCNT",
            "IAB.I_A",
            "IAB.I_B",
        ],
    ),
    ("RB", "RB.A12", "QDS", "PM", ["U_QS0", "U_1", "U_2"], ["%CELL%:U_1", "%CELL%:U_2", "%CELL%:U_QS0"]),
    ("RB", "RB.A12", "LEADS_ODD", "NXCALS", "U_BB_2", "RB.A12:U_BB_2"),
    ("RB", ["RB.A12", "RB.A23"], "LEADS_ODD", "NXCALS", "U_BB_2", ["RB.A12:U_BB_2", "RB.A23:U_BB_2"]),
    (
        "RQ",
        "RQD.A12",
        "QDS",
        "PM",
        ["U_QS0_EXT_A", "U_1_EXT_B", "U_2_EXT_A"],
        ["%CELL%:U_QS0_EXT_A", "%CELL%:U_1_EXT_B", "%CELL%:U_2_EXT_A"],
    ),
]


@pytest.mark.parametrize("circuit_type,circuit_name,system_type,database_name,variable_name,expected", _SIGNAL_NAMES)
def test_get_signal_name(circuit_type, circuit_name, system_type, database_name, variable_name, expected):
    variable_name_act = signal_metadata.get_signal_name(
        circuit_type, circuit_name, system_type, database_name, variable_name
    )

    assert sorted(variable_name_act) == sorted(expected)


_SIGNAL_METADATA_PM = [
    ("RB", "RB.A12", "QH", "%CELL%", "DQAMCNMB_PMHSU", "QPS"),
    ("RB", "RB.A12", "PC", "RPTE.UA23.RB.A12", "lhc_self_pmd", "FGC"),
    ("RB", "RB.A34", "LEADS_EVEN", "RB.A34", "DQAMGNDRBEVEN", "QPS"),
    ("RB", "RB.A45", "LEADS_ODD", "RB.A45", "DQAMGNDRBODD", "QPS"),
    ("RB", "RB.A23", "EE_EVEN", "UA27.RB.A23", "DQAMSNRB", "QPS"),
]


@pytest.mark.parametrize("circuit_type,circuit_name,system,source_exp,class_name_exp,system_exp", _SIGNAL_METADATA_PM)
def test_get_signal_metadata_pm(circuit_type, circuit_name, system, source_exp, class_name_exp, system_exp):
    database = "PM"

    metadata = signal_metadata.get_signal_metadata(circuit_type, circuit_name, system, database)
    system_act, class_name_act, source_act = metadata["system"], metadata["className"], metadata["source"]

    assert source_exp == source_act
    assert class_name_exp == class_name_act
    assert system_exp == system_act


_RB_CELLS = [
    "A8R1",
    "B8R1",
    "A9R1",
    "B9R1",
    "A10R1",
    "B10R1",
    "A11R1",
    "B11R1",
    "A12R1",
    "B12R1",
    "C12R1",
    "A13R1",
    "B13R1",
    "C13R1",
    "A14R1",
    "B14R1",
    "C14R1",
    "A15R1",
    "B15R1",
    "C15R1",
    "A16R1",
    "B16R1",
    "C16R1",
    "A17R1",
    "B17R1",
    "C17R1",
    "A18R1",
    "B18R1",
    "C18R1",
    "A19R1",
    "B19R1",
    "C19R1",
    "A20R1",
    "B20R1",
    "C20R1",
    "A21R1",
    "B21R1",
    "C21R1",
    "A22R1",
    "B22R1",
    "C22R1",
    "A23R1",
    "B23R1",
    "C23R1",
    "A24R1",
    "B24R1",
    "C24R1",
    "A25R1",
    "B25R1",
    "C25R1",
    "A26R1",
    "B26R1",
    "C26R1",
    "A27R1",
    "B27R1",
    "C27R1",
    "A28R1",
    "B28R1",
    "C28R1",
    "A29R1",
    "B29R1",
    "C29R1",
    "A30R1",
    "B30R1",
    "C30R1",
    "A31R1",
    "B31R1",
    "C31R1",
    "A32R1",
    "B32R1",
    "C32R1",
    "A33R1",
    "B33R1",
    "C33R1",
    "A34R1",
    "B34R1",
    "C34R1",
    "C34L2",
    "B34L2",
    "A34L2",
    "C33L2",
    "B33L2",
    "A33L2",
    "C32L2",
    "B32L2",
    "A32L2",
    "C31L2",
    "B31L2",
    "A31L2",
    "C30L2",
    "B30L2",
    "A30L2",
    "C29L2",
    "B29L2",
    "A29L2",
    "C28L2",
    "B28L2",
    "A28L2",
    "C27L2",
    "B27L2",
    "A27L2",
    "C26L2",
    "B26L2",
    "A26L2",
    "C25L2",
    "B25L2",
    "A25L2",
    "C24L2",
    "B24L2",
    "A24L2",
    "C23L2",
    "B23L2",
    "A23L2",
    "C22L2",
    "B22L2",
    "A22L2",
    "C21L2",
    "B21L2",
    "A21L2",
    "C20L2",
    "B20L2",
    "A20L2",
    "C19L2",
    "B19L2",
    "A19L2",
    "C18L2",
    "B18L2",
    "A18L2",
    "C17L2",
    "B17L2",
    "A17L2",
    "C16L2",
    "B16L2",
    "A16L2",
    "C15L2",
    "B15L2",
    "A15L2",
    "C14L2",
    "B14L2",
    "A14L2",
    "C13L2",
    "B13L2",
    "A13L2",
    "C12L2",
    "B12L2",
    "A12L2",
    "B11L2",
    "A11L2",
    "B10L2",
    "A10L2",
    "B9L2",
    "A9L2",
    "B8L2",
    "A8L2",
]

_RB_MAGNETS = [
    "MB.A8R1",
    "MB.B8R1",
    "MB.A9R1",
    "MB.B9R1",
    "MB.A10R1",
    "MB.B10R1",
    "MB.A11R1",
    "MB.B11R1",
    "MB.A12R1",
    "MB.B12R1",
    "MB.C12R1",
    "MB.A13R1",
    "MB.B13R1",
    "MB.C13R1",
    "MB.A14R1",
    "MB.B14R1",
    "MB.C14R1",
    "MB.A15R1",
    "MB.B15R1",
    "MB.C15R1",
    "MB.A16R1",
    "MB.B16R1",
    "MB.C16R1",
    "MB.A17R1",
    "MB.B17R1",
    "MB.C17R1",
    "MB.A18R1",
    "MB.B18R1",
    "MB.C18R1",
    "MB.A19R1",
    "MB.B19R1",
    "MB.C19R1",
    "MB.A20R1",
    "MB.B20R1",
    "MB.C20R1",
    "MB.A21R1",
    "MB.B21R1",
    "MB.C21R1",
    "MB.A22R1",
    "MB.B22R1",
    "MB.C22R1",
    "MB.A23R1",
    "MB.B23R1",
    "MB.C23R1",
    "MB.A24R1",
    "MB.B24R1",
    "MB.C24R1",
    "MB.A25R1",
    "MB.B25R1",
    "MB.C25R1",
    "MB.A26R1",
    "MB.B26R1",
    "MB.C26R1",
    "MB.A27R1",
    "MB.B27R1",
    "MB.C27R1",
    "MB.A28R1",
    "MB.B28R1",
    "MB.C28R1",
    "MB.A29R1",
    "MB.B29R1",
    "MB.C29R1",
    "MB.A30R1",
    "MB.B30R1",
    "MB.C30R1",
    "MB.A31R1",
    "MB.B31R1",
    "MB.C31R1",
    "MB.A32R1",
    "MB.B32R1",
    "MB.C32R1",
    "MB.A33R1",
    "MB.B33R1",
    "MB.C33R1",
    "MB.A34R1",
    "MB.B34R1",
    "MB.C34R1",
    "MB.C34L2",
    "MB.B34L2",
    "MB.A34L2",
    "MB.C33L2",
    "MB.B33L2",
    "MB.A33L2",
    "MB.C32L2",
    "MB.B32L2",
    "MB.A32L2",
    "MB.C31L2",
    "MB.B31L2",
    "MB.A31L2",
    "MB.C30L2",
    "MB.B30L2",
    "MB.A30L2",
    "MB.C29L2",
    "MB.B29L2",
    "MB.A29L2",
    "MB.C28L2",
    "MB.B28L2",
    "MB.A28L2",
    "MB.C27L2",
    "MB.B27L2",
    "MB.A27L2",
    "MB.C26L2",
    "MB.B26L2",
    "MB.A26L2",
    "MB.C25L2",
    "MB.B25L2",
    "MB.A25L2",
    "MB.C24L2",
    "MB.B24L2",
    "MB.A24L2",
    "MB.C23L2",
    "MB.B23L2",
    "MB.A23L2",
    "MB.C22L2",
    "MB.B22L2",
    "MB.A22L2",
    "MB.C21L2",
    "MB.B21L2",
    "MB.A21L2",
    "MB.C20L2",
    "MB.B20L2",
    "MB.A20L2",
    "MB.C19L2",
    "MB.B19L2",
    "MB.A19L2",
    "MB.C18L2",
    "MB.B18L2",
    "MB.A18L2",
    "MB.C17L2",
    "MB.B17L2",
    "MB.A17L2",
    "MB.C16L2",
    "MB.B16L2",
    "MB.A16L2",
    "MB.C15L2",
    "MB.B15L2",
    "MB.A15L2",
    "MB.C14L2",
    "MB.B14L2",
    "MB.A14L2",
    "MB.C13L2",
    "MB.B13L2",
    "MB.A13L2",
    "MB.C12L2",
    "MB.B12L2",
    "MB.A12L2",
    "MB.B11L2",
    "MB.A11L2",
    "MB.B10L2",
    "MB.A10L2",
    "MB.B9L2",
    "MB.A9L2",
    "MB.B8L2",
    "MB.A8L2",
]

_RQ_MAGNETS = [
    "MQ.11L2",
    "MQ.11R1",
    "MQ.12L2",
    "MQ.12R1",
    "MQ.13L2",
    "MQ.13R1",
    "MQ.14L2",
    "MQ.14R1",
    "MQ.15L2",
    "MQ.15R1",
    "MQ.16L2",
    "MQ.16R1",
    "MQ.17L2",
    "MQ.17R1",
    "MQ.18L2",
    "MQ.18R1",
    "MQ.19L2",
    "MQ.19R1",
    "MQ.20L2",
    "MQ.20R1",
    "MQ.21L2",
    "MQ.21R1",
    "MQ.22L2",
    "MQ.22R1",
    "MQ.23L2",
    "MQ.23R1",
    "MQ.24L2",
    "MQ.24R1",
    "MQ.25L2",
    "MQ.25R1",
    "MQ.26L2",
    "MQ.26R1",
    "MQ.27L2",
    "MQ.27R1",
    "MQ.28L2",
    "MQ.28R1",
    "MQ.29L2",
    "MQ.29R1",
    "MQ.30L2",
    "MQ.30R1",
    "MQ.31L2",
    "MQ.31R1",
    "MQ.32L2",
    "MQ.32R1",
    "MQ.33L2",
]

_U_ERATH_MAGNETS_FOR_RB = [
    "MB.A8L2",
    "MB.A10L2",
    "MB.C12L2",
    "MB.C14L2",
    "MB.C16L2",
    "MB.C18L2",
    "MB.C20L2",
    "MB.C22L2",
    "MB.C24L2",
    "MB.C26L2",
    "MB.C28L2",
    "MB.C30L2",
    "MB.C32L2",
    "MB.C34L2",
    "MB.A33R1",
    "MB.C31R1",
    "MB.C29R1",
    "MB.C27R1",
    "MB.C25R1",
    "MB.C23R1",
    "MB.C21R1",
    "MB.C19R1",
    "MB.C17R1",
    "MB.C15R1",
    "MB.C13R1",
    "MB.B11R1",
    "MB.B8R1",
    "MB.A8R1",
    "MB.A10R1",
    "MB.C12R1",
    "MB.C14R1",
    "MB.C16R1",
    "MB.C18R1",
    "MB.C20R1",
    "MB.C22R1",
    "MB.C24R1",
    "MB.C26R1",
    "MB.C28R1",
    "MB.C30R1",
    "MB.C32R1",
    "MB.C34R1",
    "MB.C33L2",
    "MB.C31L2",
    "MB.C29L2",
    "MB.C27L2",
    "MB.C25L2",
    "MB.C23L2",
    "MB.C21L2",
    "MB.C19L2",
    "MB.C17L2",
    "MB.C15L2",
    "MB.C13L2",
    "MB.B11L2",
    "MB.B8L2",
]

_U_ERATH_MAGNETS_FOR_RQ = [
    "MQ.11L2",
    "MQ.11R1",
    "MQ.12L2",
    "MQ.12R1",
    "MQ.13L2",
    "MQ.13R1",
    "MQ.14L2",
    "MQ.14R1",
    "MQ.15L2",
    "MQ.15R1",
    "MQ.16L2",
    "MQ.16R1",
    "MQ.17L2",
    "MQ.17R1",
    "MQ.18L2",
    "MQ.18R1",
    "MQ.19L2",
    "MQ.19R1",
    "MQ.20L2",
    "MQ.20R1",
    "MQ.21L2",
    "MQ.21R1",
    "MQ.22L2",
    "MQ.22R1",
    "MQ.23L2",
    "MQ.23R1",
    "MQ.24L2",
    "MQ.24R1",
    "MQ.25L2",
    "MQ.25R1",
    "MQ.26L2",
    "MQ.26R1",
    "MQ.27L2",
    "MQ.27R1",
    "MQ.28L2",
    "MQ.28R1",
    "MQ.29L2",
    "MQ.29R1",
    "MQ.30L2",
    "MQ.30R1",
    "MQ.31L2",
    "MQ.31R1",
    "MQ.32L2",
    "MQ.32R1",
    "MQ.33L2",
    "MQ.33R1",
    "MQ.34R1",
]

_RQ_CRATES = [
    "B10L2",
    "B11L2",
    "B11R1",
    "B12L2",
    "B12R1",
    "B13L2",
    "B13R1",
    "B14L2",
    "B14R1",
    "B15L2",
    "B15R1",
    "B16L2",
    "B16R1",
    "B17L2",
    "B17R1",
    "B18L2",
    "B18R1",
    "B19L2",
    "B19R1",
    "B20L2",
    "B20R1",
    "B21L2",
    "B21R1",
    "B22L2",
    "B22R1",
    "B23L2",
    "B23R1",
    "B24L2",
    "B24R1",
    "B25L2",
    "B25R1",
    "B26L2",
    "B26R1",
    "B27L2",
    "B27R1",
    "B28L2",
    "B28R1",
    "B29L2",
    "B29R1",
    "B30L2",
    "B30R1",
    "B31L2",
    "B31R1",
    "B32L2",
    "B32R1",
    "B33L2",
    "B33R1",
    "B34R1",
]

_RB_CRATES = [
    "B8L2",
    "B10L2",
    "B12L2",
    "B14L2",
    "B16L2",
    "B18L2",
    "B20L2",
    "B22L2",
    "B24L2",
    "B26L2",
    "B28L2",
    "B30L2",
    "B32L2",
    "B34L2",
    "B33R1",
    "B31R1",
    "B29R1",
    "B27R1",
    "B25R1",
    "B23R1",
    "B21R1",
    "B19R1",
    "B17R1",
    "B15R1",
    "B13R1",
    "B11R1",
    "B9R1",
    "B8R1",
    "B10R1",
    "B12R1",
    "B14R1",
    "B16R1",
    "B18R1",
    "B20R1",
    "B22R1",
    "B24R1",
    "B26R1",
    "B28R1",
    "B30R1",
    "B32R1",
    "B34R1",
    "B33L2",
    "B31L2",
    "B29L2",
    "B27L2",
    "B25L2",
    "B23L2",
    "B21L2",
    "B19L2",
    "B17L2",
    "B15L2",
    "B13L2",
    "B11L2",
    "B9L2",
]

_BUSBARS = [
    "DCBB.8L2.R",
    "DCBB.9L2.R",
    "DCBB.10L2.R",
    "DCBB.11L2.R",
    "DCBB.A12L2.R",
    "DCBB.B12L2.R",
    "DCBB.13L2.R",
    "DCBB.A14L2.R",
    "DCBB.B14L2.R",
    "DCBB.15L2.R",
    "DCBB.A16L2.R",
    "DCBB.B16L2.R",
    "DCBB.17L2.R",
    "DCBB.A18L2.R",
    "DCBB.B18L2.R",
    "DCBB.19L2.R",
    "DCBB.A20L2.R",
    "DCBB.B20L2.R",
    "DCBB.21L2.R",
    "DCBB.A22L2.R",
    "DCBB.B22L2.R",
    "DCBB.23L2.R",
    "DCBB.A24L2.R",
    "DCBB.B24L2.R",
    "DCBB.25L2.R",
    "DCBB.A26L2.R",
    "DCBB.B26L2.R",
    "DCBB.27L2.R",
    "DCBB.A28L2.R",
    "DCBB.B28L2.R",
    "DCBB.29L2.R",
    "DCBB.A30L2.R",
    "DCBB.B30L2.R",
    "DCBB.31L2.R",
    "DCBB.A32L2.R",
    "DCBB.B32L2.R",
    "DCBB.33L2.R",
    "DCBB.A34L2.R",
    "DCBB.B34L2.R",
    "DCBB.34R1.R",
    "DCBB.B33R1.R",
    "DCBB.A33R1.R",
    "DCBB.32R1.R",
    "DCBB.B31R1.R",
    "DCBB.A31R1.R",
    "DCBB.30R1.R",
    "DCBB.B29R1.R",
    "DCBB.A29R1.R",
    "DCBB.28R1.R",
    "DCBB.B27R1.R",
    "DCBB.A27R1.R",
    "DCBB.26R1.R",
    "DCBB.B25R1.R",
    "DCBB.A25R1.R",
    "DCBB.24R1.R",
    "DCBB.B23R1.R",
    "DCBB.A23R1.R",
    "DCBB.22R1.R",
    "DCBB.B21R1.R",
    "DCBB.A21R1.R",
    "DCBB.20R1.R",
    "DCBB.B19R1.R",
    "DCBB.A19R1.R",
    "DCBB.18R1.R",
    "DCBB.B17R1.R",
    "DCBB.A17R1.R",
    "DCBB.16R1.R",
    "DCBB.B15R1.R",
    "DCBB.A15R1.R",
    "DCBB.14R1.R",
    "DCBB.B13R1.R",
    "DCBB.A13R1.R",
    "DCBB.12R1.R",
    "DCBB.11R1.R",
    "DCBB.10R1.R",
    "DCBB.9R1.R",
    "DCBB.8R1.R",
    "DCBD.7R1.R",
    "DCBQ.7R1.L",
    "DCBQ.8R1.L",
    "DCBQ.9R1.L",
    "DCBQ.10R1.L",
    "DCBQ.11R1.L",
    "DCBB.12R1.L",
    "DCBB.A13R1.L",
    "DCBQ.13R1.L",
    "DCBB.14R1.L",
    "DCBB.A15R1.L",
    "DCBQ.15R1.L",
    "DCBB.16R1.L",
    "DCBB.A17R1.L",
    "DCBQ.17R1.L",
    "DCBB.18R1.L",
    "DCBB.A19R1.L",
    "DCBQ.19R1.L",
    "DCBB.20R1.L",
    "DCBB.A21R1.L",
    "DCBQ.21R1.L",
    "DCBB.22R1.L",
    "DCBB.A23R1.L",
    "DCBQ.23R1.L",
    "DCBB.24R1.L",
    "DCBB.A25R1.L",
    "DCBQ.25R1.L",
    "DCBB.26R1.L",
    "DCBB.A27R1.L",
    "DCBQ.27R1.L",
    "DCBB.28R1.L",
    "DCBB.A29R1.L",
    "DCBQ.29R1.L",
    "DCBB.30R1.L",
    "DCBB.A31R1.L",
    "DCBQ.31R1.L",
    "DCBB.32R1.L",
    "DCBB.A33R1.L",
    "DCBQ.33R1.L",
    "DCBB.34R1.L",
    "DCBB.B34L2.L",
    "DCBQ.33L2.L",
    "DCBB.33L2.L",
    "DCBB.B32L2.L",
    "DCBQ.31L2.L",
    "DCBB.31L2.L",
    "DCBB.B30L2.L",
    "DCBQ.29L2.L",
    "DCBB.29L2.L",
    "DCBB.B28L2.L",
    "DCBQ.27L2.L",
    "DCBB.27L2.L",
    "DCBB.A26L2.L",
    "DCBQ.25L2.L",
    "DCBB.25L2.L",
    "DCBB.B24L2.L",
    "DCBQ.23L2.L",
    "DCBB.23L2.L",
    "DCBB.B22L2.L",
    "DCBQ.21L2.L",
    "DCBB.21L2.L",
    "DCBB.B20L2.L",
    "DCBQ.19L2.L",
    "DCBB.19L2.L",
    "DCBB.B18L2.L",
    "DCBQ.17L2.L",
    "DCBB.17L2.L",
    "DCBB.B16L2.L",
    "DCBQ.15L2.L",
    "DCBB.15L2.L",
    "DCBB.B14L2.L",
    "DCBQ.13L2.L",
    "DCBB.13L2.L",
    "DCBB.A12L2.L",
    "DCBQ.11L2.L",
    "DCBQ.10L2.L",
    "DCBQ.9L2.L",
    "DCBQ.8L2.L",
    "DCBD.7L2.L",
]

_RB_CRYOSTATS = [
    "LBALA_08L3",
    "LBALA_09L3",
    "LBALA_10L3",
    "LBALA_11L3",
    "LBALA_11R2",
    "LBALA_12L3",
    "LBALA_12R2",
    "LBALA_13L3",
    "LBALA_13R2",
    "LBALA_14L3",
    "LBALA_14R2",
    "LBALA_15L3",
    "LBALA_15R2",
    "LBALA_16L3",
    "LBALA_16R2",
    "LBALA_17L3",
    "LBALA_17R2",
    "LBALA_18L3",
    "LBALA_18R2",
    "LBALA_19L3",
    "LBALA_19R2",
    "LBALA_20L3",
    "LBALA_20R2",
    "LBALA_21L3",
    "LBALA_21R2",
    "LBALA_22L3",
    "LBALA_22R2",
    "LBALA_23L3",
    "LBALA_23R2",
    "LBALA_24L3",
    "LBALA_24R2",
    "LBALA_25L3",
    "LBALA_25R2",
    "LBALA_26L3",
    "LBALA_26R2",
    "LBALA_27L3",
    "LBALA_27R2",
    "LBALA_28L3",
    "LBALA_28R2",
    "LBALA_29L3",
    "LBALA_29R2",
    "LBALA_30L3",
    "LBALA_30R2",
    "LBALA_31L3",
    "LBALA_31R2",
    "LBALA_32L3",
    "LBALA_32R2",
    "LBALA_33L3",
    "LBALA_33R2",
    "LBALA_34L3",
    "LBALA_34R2",
    "LBALB_12R2",
    "LBALB_13L3",
    "LBALB_14R2",
    "LBALB_15L3",
    "LBALB_16R2",
    "LBALB_17L3",
    "LBALB_18R2",
    "LBALB_19L3",
    "LBALB_20R2",
    "LBALB_21L3",
    "LBALB_22R2",
    "LBALB_23L3",
    "LBALB_24R2",
    "LBALB_25L3",
    "LBALB_26R2",
    "LBALB_27L3",
    "LBALB_28R2",
    "LBALB_29L3",
    "LBALB_30R2",
    "LBALB_31L3",
    "LBALB_32R2",
    "LBALB_33L3",
    "LBALB_34R2",
    "LBALE_08R2",
    "LBALE_09R2",
    "LBALE_10R2",
    "LBBLA_11R2",
    "LBBLA_12L3",
    "LBBLA_12R2",
    "LBBLA_13L3",
    "LBBLA_13R2",
    "LBBLA_14L3",
    "LBBLA_14R2",
    "LBBLA_15L3",
    "LBBLA_15R2",
    "LBBLA_16L3",
    "LBBLA_16R2",
    "LBBLA_17L3",
    "LBBLA_17R2",
    "LBBLA_18L3",
    "LBBLA_18R2",
    "LBBLA_19L3",
    "LBBLA_19R2",
    "LBBLA_20L3",
    "LBBLA_20R2",
    "LBBLA_21L3",
    "LBBLA_21R2",
    "LBBLA_22L3",
    "LBBLA_22R2",
    "LBBLA_23L3",
    "LBBLA_23R2",
    "LBBLA_24L3",
    "LBBLA_24R2",
    "LBBLA_25L3",
    "LBBLA_25R2",
    "LBBLA_26L3",
    "LBBLA_26R2",
    "LBBLA_27L3",
    "LBBLA_27R2",
    "LBBLA_28L3",
    "LBBLA_28R2",
    "LBBLA_29L3",
    "LBBLA_29R2",
    "LBBLA_30L3",
    "LBBLA_30R2",
    "LBBLA_31L3",
    "LBBLA_31R2",
    "LBBLA_32L3",
    "LBBLA_32R2",
    "LBBLA_33L3",
    "LBBLA_33R2",
    "LBBLA_34L3",
    "LBBLA_34R2",
    "LBBLB_09L3",
    "LBBLB_11L3",
    "LBBLD_10L3",
    "LBBLD_12L3",
    "LBBLD_13R2",
    "LBBLD_14L3",
    "LBBLD_15R2",
    "LBBLD_16L3",
    "LBBLD_17R2",
    "LBBLD_18L3",
    "LBBLD_19R2",
    "LBBLD_20L3",
    "LBBLD_21R2",
    "LBBLD_22L3",
    "LBBLD_23R2",
    "LBBLD_24L3",
    "LBBLD_25R2",
    "LBBLD_26L3",
    "LBBLD_27R2",
    "LBBLD_28L3",
    "LBBLD_29R2",
    "LBBLD_30L3",
    "LBBLD_31R2",
    "LBBLD_32L3",
    "LBBLD_33R2",
    "LBBLD_34L3",
    "LBBLF_08R2",
    "LBBLF_10R2",
    "LBBLG_09R2",
    "LBBLR_08L3",
]

_WILDCARDS = [
    ("RB", "RB.A12", "QH", "PM", "U_HDS", "%CELL%", _RB_CELLS),
    ("RB", "RB.A12", "QDS", "PM", "U_HDS", "%CELL%", _RB_CELLS),
    ("RB", "RB.A12", "QDS", "NXCALS", "U_HDS", "%MAGNET%", _RB_MAGNETS),
    ("RQ", "RQD.A12", "QH", "NXCALS", "U_HDS", "", [""]),
    ("RB", "RB.A12", "VF", "PM", "U_EARTH_RB", "%MAGNET%", _U_ERATH_MAGNETS_FOR_RB),
    ("RQ", "RQD.A12", "VF_RQD", "PM", "U_EARTH_RQD", "%MAGNET%", _U_ERATH_MAGNETS_FOR_RQ + ["DFLAS.7L2.2"]),
    ("RQ", "RQF.A12", "VF_RQF", "PM", "U_EARTH_RQF", "%MAGNET%", _U_ERATH_MAGNETS_FOR_RQ + ["DFLAS.7L2.4"]),
    ("RB", "RB.A12", "VF", "NXCALS", "U_EARTH", "%MAGNET%", _U_ERATH_MAGNETS_FOR_RB),
    ("RQ", "RQD.A12", "DIODE_RQD", "NXCALS", "U_DIODE_RQD", "%MAGNET%", _RQ_MAGNETS),
    ("RQ", "RQF.A12", "DIODE_RQF", "NXCALS", "U_DIODE_RQF", "%MAGNET%", _RQ_MAGNETS),
    ("RQ", "RQF.A12", "DIODE_RQF", "NXCALS", "U_REF_N1", "%CRATE%", _RQ_CRATES),
    ("RB", "RB.A12", "BUSBAR", "NXCALS", "U_RES", "%BUSBAR%", _BUSBARS),
    ("RB", "RB.A12", "BUSBAR", "NXCALS", "U_RES", "%BUSBAR%", _BUSBARS),
    ("RB", "RB.A12", "DIODE_RB", "PM", "U_REF_N1", "%CRATE%", _RB_CRATES),
    ("RB", "RB.A12", "DIODE_RB", "PM", "U_DIODE_RB", "%MAGNET%", _RB_MAGNETS),
    ("RB", "RB.A12", "DIODE_RB", "NXCALS", "U_DIODE_RB", "%MAGNET%", _RB_MAGNETS),
    ("RB", "RB.A45", "PIC", "PM", "U_RES", "", [""]),
    ("RB", "RB.A23", "CRYO", "NXCALS", "", "%CRYOSTAT2%", _RB_CRYOSTATS),
]


@pytest.mark.parametrize("circuit_type,circuit_name,system,db,signal,expected_wildcard,expected_values", _WILDCARDS)
def test_get_wildcard_for_circuit_system_db_signal(
    circuit_type, circuit_name, system, db, signal, expected_wildcard, expected_values
):
    wildcard_act = signal_metadata.get_wildcard_for_circuit_system_db_signal(
        circuit_type, circuit_name, system, db, signal
    )
    wildcard_name_act = wildcard_act[0]
    wildcard_values_act = wildcard_act[1]

    assert expected_wildcard == wildcard_name_act
    assert sorted(expected_values) == sorted(wildcard_values_act)


def test_get_wildcard_for_circuit_system_db_signal_exception():
    circuit_type = "RB"
    circuit_name = "any"
    system = "QH"
    db = "ANY_DB"
    signal = "any"

    with pytest.raises(KeyError) as exception:
        signal_metadata.get_wildcard_for_circuit_system_db_signal(circuit_type, circuit_name, system, db, signal)

    assert "\"Database type ANY_DB not present in ['PM', 'NXCALS'].\"" == str(exception.value)


_SECTORS = [
    ("RB", "MAGNET", "B20L5", "RB.A45"),
    ("RB", "BUSBAR", "DCBA.10L1.L:U_MAG", "RB.A81"),
    ("RB", "PC", "RPTE.UA23.RB.A12:I_MEAS", "RB.A12"),
    ("RB", "PC", "RPTE.UA27.RB.A23:I_MEAS", "RB.A23"),
]


@pytest.mark.parametrize("circuit_type,system,component,expected_sector", _SECTORS)
def test_get_sector_names_from_component(circuit_type, system, component, expected_sector):
    # arrange
    # act
    sector_act = signal_metadata.get_sector_names_from_component(circuit_type, system, component)

    # assert
    assert sector_act == expected_sector


def test_get_sector_names_from_component_exception():
    circuit_type = "RB"
    component = "ANY"
    identifier = "ANY"
    with pytest.raises(KeyError) as exception:
        signal_metadata.get_sector_names_from_component(circuit_type, component, identifier)
    assert "'System ANY does not support this feature'" == str(exception.value)


_SECTORS_WARNINGS = [
    ("BUSBAR", "ANY", "Key ANY does not contain proper MB magnet signal"),
    ("BUSBAR", "MB.A10L903", "Key L9 does not exist"),
]


@pytest.mark.parametrize("component,identifier,message", _SECTORS_WARNINGS)
def test_get_sector_names_from_component_warning(component, identifier, message):
    circuit_type = "RB"
    with warnings.catch_warnings(record=True) as w:
        signal_metadata.get_sector_names_from_component(circuit_type, component, identifier)
    assert message == str(w[0].message)


_600A_FAMILIES = [
    ("RQTL11.L1B1", "RQTL11"),
    ("RQTL10.L3B1", "RQTL10"),
    ("RQTL8.L3B1", "RQTL8"),
    ("RQTL7.L3B1", "RQTL7"),
    ("RQT13.L1B1", "RQT13"),
    ("RQT12.L1B1", "RQT12"),
    ("RQS.L1B2", "RQS"),
    ("RQSX3.L1", "RQSX3"),
    ("RU.L4", "RU"),
    ("RSS.A12B1", "RSS"),
    ("RQTL9.L3B1", "RQTL9"),
    ("RQTD.A12B1", "RQTD"),
    ("RQTF.A12B1", "RQTF"),
    ("RQS.A12B2", "RQS"),
    ("RQ6.L3B1", "RQ6"),
    ("ROD.A12B1", "ROD"),
    ("ROF.A12B1", "ROF"),
    ("RCS.A12B1", "RCS"),
    ("RCO.A12B1", "RCD-RCO"),
    ("RCD.A12B1", "RCD-RCO"),
    ("RCBXH1.L1", "RCBX"),
    ("RSD1.A12B1", "RSD"),
    ("RSF1.A12B1", "RSF"),
]


@pytest.mark.parametrize("circuit_name,family_name_expected", _600A_FAMILIES)
def test_get_family_name_for_600A(circuit_name, family_name_expected):
    family_name_act = signal_metadata.get_family_name_for_600A(circuit_name)

    assert family_name_expected == family_name_act


@pytest.mark.parametrize("circuit_name,family_name_expected", [("RCOSX3.L1", "RCOSX3"), ("RCBCH10.L1B1", "RCBCH")])
def test_get_family_name_for_80_120A(circuit_name, family_name_expected):
    family_name_act = signal_metadata.get_family_name_for_80_120A(circuit_name)

    assert family_name_expected == family_name_act


_CIRCUIT_TYPES = [
    ("RB", "RB.A12"),
    ("RQ", "RQD.A12"),
    ("IT", "RQX.R1"),
    ("IPQ8", "RQ4.L2"),
    ("IPQ4", "RQ4.L1"),
    ("IPQ2", "RQ5.L1"),
    ("IPD2", "RD1.L2"),
    ("IPD2_B1B2", "RD2.L1"),
    ("600A", "RCBXH1.L1"),
    ("80-120A", "RCBCH10.L1B1"),
    ("60A", "RCBH11.L1B2"),
]


@pytest.mark.parametrize("circuit_type,circuit_name", _CIRCUIT_TYPES)
def test_get_circuit_type_for_circuit_name(circuit_type, circuit_name):
    circuit_type_act = signal_metadata.get_circuit_type_for_circuit_name(circuit_name)

    assert circuit_type == circuit_type_act


def test_get_circuit_type_for_circuit_name_error():
    circuit_name = "ABC"

    with pytest.raises(KeyError) as exception:
        signal_metadata.get_circuit_type_for_circuit_name(circuit_name)

    assert "'Circuit name ABC does not map to internal metadata.'" == str(exception.value)


@pytest.mark.parametrize(
    ("circuit_name", "expected_circuit_type"),
    [
        ("RCBH11.L1B2", GenericCircuitType.A60),
        ("RCBCH10.L1B1", GenericCircuitType.A80_120),
        ("RCBXH1.L1", GenericCircuitType.A600),
        ("RQX.R1", GenericCircuitType.IT),
        ("RD1.L2", GenericCircuitType.IPD),
        ("RD2.L1", GenericCircuitType.IPD),
        ("RQ4.L2", GenericCircuitType.IPQ),
        ("RQ4.L1", GenericCircuitType.IPQ),
        ("RQ5.L1", GenericCircuitType.IPQ),
        ("RB.A12", GenericCircuitType.RB),
        ("RQD.A12", GenericCircuitType.RQ),
    ],
)
def test_get_generic_circuit_type_for_circuit_name(circuit_name: str, expected_circuit_type: GenericCircuitType):
    assert signal_metadata.get_generic_circuit_type_for_circuit_name(circuit_name) == expected_circuit_type
    assert signal_metadata.get_generic_circuit_type_for_circuit_name(circuit_name) == expected_circuit_type.value


def test_get_generic_circuit_type_for_circuit_name_invalid_circuit():
    with pytest.raises(ValueError, match=re.escape("Circuit name ABC not present in the metadata.")):
        signal_metadata.get_generic_circuit_type_for_circuit_name("ABC")


_NXCALS_SYSTEMS = [("RB", "QDS", "U_QS0", "CMW"), ("RB", ["LEADS_EVEN", "LEADS_ODD"], ["U_RES", "U_HTS"], "CMW")]


@pytest.mark.parametrize("circuit,system,signal,expected_system", _NXCALS_SYSTEMS)
def test_get_nxcals_system_name(circuit, system, signal, expected_system):
    nxcals_system = signal_metadata.get_nxcals_system_name(circuit, system, signal)

    assert nxcals_system == expected_system


def test_error_when_no_signal_found():
    circuit = "RB"
    system = "QDS"
    signal = "U_HDS"

    with pytest.raises(KeyError) as exception:
        signal_metadata.get_nxcals_system_name(circuit, system, signal)

    assert "\"Signal 'U_HDS' not present in QDS.\"" == str(exception.value)


def test_get_nxcals_system_name_more_systems():
    circuit = "RB"
    system = "BUSBAR"
    signals = ["U_RES", "ST_BUSBAR_OK"]

    with pytest.raises(KeyError) as exception:
        signal_metadata.get_nxcals_system_name(circuit, system, signals)

    assert (
        "\"Can't query for all signals ['U_RES', 'ST_BUSBAR_OK'] because they come from different "
        'NXCALS systems"' == str(exception.value)
    )


_QPS_UPGRADE_AFFECTED_SYSTEMS = ["QDS", "QH", "LEADS"]


@pytest.mark.parametrize("system", _QPS_UPGRADE_AFFECTED_SYSTEMS)
def test_acceptance_after_rd1_qps_upgrade(system):
    circuit_type = "IPD2"
    circuit_name = "RD1.L8"
    time_after_upgrade = 1679052360000000000
    time_before_upgrade = 1675052360000000000

    metadata_after = signal_metadata.get_signal_metadata(circuit_type, circuit_name, system, "PM", time_after_upgrade)
    metadata_before = signal_metadata.get_signal_metadata(circuit_type, circuit_name, system, "PM", time_before_upgrade)

    assert metadata_after["className"] == "DQAMGNRQA"
    assert metadata_before["className"] == "DQAMGNRD"


def test_get_circuit_interlock_type_valid_circuit_name():
    circuit_name = "RB.A12"
    interlock_type = signal_metadata.get_circuit_interlock_type(circuit_name)

    assert interlock_type == "A1"


def test_get_circuit_interlock_type_invalid_circuit_name():
    with pytest.raises(ValueError):
        signal_metadata.get_circuit_interlock_type("ABC")


def test_get_circuit_location_valid():
    circuit_name = "RB.A12"
    location = signal_metadata.get_circuit_location(circuit_name)

    assert location == "UA23"


def test_get_circuit_location_invalid_circuit_name():
    with pytest.raises(ValueError) as e:
        signal_metadata.get_circuit_location("ABC")

    assert str(e.value) == "Circuit name ABC not present in the metadata."


def test_get_circuit_subsector_valid_circuit_name():
    circuit_name = "RB.A12"
    subsector = signal_metadata.get_circuit_subsector(circuit_name)

    assert subsector == "A12"


def test_get_circuit_subsector_invalid_circuit_name():
    with pytest.raises(ValueError):
        signal_metadata.get_circuit_subsector("ABC")


def test_has_ee_valid_circuit_name():
    assert signal_metadata.has_ee("RB.A12")
    assert signal_metadata.has_ee("RQD.A12")
    assert signal_metadata.has_ee("RCD.A12B1")
    assert not signal_metadata.has_ee("RCO.A12B1")


def test_has_ee_invalid_circuit_name():
    with pytest.raises(ValueError, match=re.escape("Circuit name ABC not present in the metadata.")):
        signal_metadata.has_ee("ABC")


def test_has_crowbar_valid_circuit_name():
    assert signal_metadata.has_crowbar("RQS.L1B2")
    assert not signal_metadata.has_crowbar("RCBXH1.L1")


def test_has_crowbar_invalid_circuit_name():
    with pytest.raises(ValueError) as e:
        signal_metadata.has_crowbar("ABC")

    assert str(e.value) == "Circuit name ABC not present in the metadata."


def test_has_crowbar_invalid_circuit_type():
    with pytest.raises(NotImplementedError) as e:
        signal_metadata.has_crowbar("RB.A12")

    assert str(e.value) == "The has_crowbar method currently only supports 600a circuits."


_PM_CLASS_FGC = [
    ("RCBH11.L1B2", "2016-07-01 00:00:00", "51"),
    ("RCBH11.L1B2", "2017-07-01 00:00:00", "lhc"),
    ("RB.A12", "2017-07-01 00:00:00", "51"),
    ("RB.A12", "2018-07-01 00:00:00", "lhc"),
]


@pytest.mark.parametrize("circuit_name,timestamp,expected", _PM_CLASS_FGC)
def test_get_fgc_pm_class_name(circuit_name: str, timestamp: str, expected: str):
    assert signal_metadata.get_fgc_pm_class_name(circuit_name, timestamp, "self") == f"{expected}_self_pmd"
    assert signal_metadata.get_fgc_pm_class_name(circuit_name, timestamp, "ext") == f"{expected}_ext_pmd"


def test_get_fgc_pm_class_name_invalid_origin():
    with pytest.raises(
        ValueError, match=re.escape("The origin of the FGC dump could be only 'self' or 'ext' - provided origin.")
    ):
        signal_metadata.get_fgc_pm_class_name("RCBH11.L1B2", "2016-07-01 00:00:00", "origin")


def test_get_fgc_pm_class_name_invalid_circuit_name():
    with pytest.raises(ValueError, match=re.escape("Circuit name ABC not present in the metadata.")):
        signal_metadata.get_fgc_pm_class_name("ABC", "2016-07-01 00:00:00", "self")


def test_is_ipq():
    assert signal_metadata.is_ipq("RQ5.L2")
    assert signal_metadata.is_ipq("RQ4.L1")
    assert not signal_metadata.is_ipq("RQD.A12")


def test_is_ipd():
    assert signal_metadata.is_ipd("RD1.L2")
    assert signal_metadata.is_ipd("RD2.L1")
    assert not signal_metadata.is_ipd("RQD.A12")


def test_is_main_dipole():
    assert signal_metadata.is_main_dipole("RB.A12")
    assert not signal_metadata.is_main_dipole("RQD.A12")


def test_is_main_quadrupole():
    assert signal_metadata.is_main_quadrupole("RQD.A12")
    assert not signal_metadata.is_main_quadrupole("RB.A12")


def test_is_inner_triplet():
    assert signal_metadata.is_inner_triplet("RQX.L1")
    assert not signal_metadata.is_inner_triplet("RB.A12")


def test_is_60a():
    assert signal_metadata.is_60a("RCBH11.L1B2")
    assert not signal_metadata.is_60a("RB.A12")


def test_is_80_120a():
    assert signal_metadata.is_80_120a("RCBCH10.L1B1")
    assert not signal_metadata.is_80_120a("RB.A12")


def test_is_600a():
    assert signal_metadata.is_600a("RCBXH1.L1")
    assert not signal_metadata.is_600a("RB.A12")


def test_is_undulator():
    assert signal_metadata.is_undulator("RU.L4")
    assert not signal_metadata.is_undulator("RB.A12")


def test_get_number_of_heaters():
    assert signal_metadata.get_number_of_heaters("RQ5.R1") == 2
    assert signal_metadata.get_number_of_heaters("RQ4.R1") == 4
    assert signal_metadata.get_number_of_heaters("RQ5.L2") == 8

    with pytest.raises(KeyError):
        signal_metadata.get_number_of_heaters("ABC")


def test_get_qps_pm_class_name_for_ipq():
    assert signal_metadata.get_qps_pm_class_name_for_ipq("RQ5.R1") == "DQAMGNRQA"
    assert signal_metadata.get_qps_pm_class_name_for_ipq("RQ4.R1") == "DQAMGNRQB"
    assert signal_metadata.get_qps_pm_class_name_for_ipq("RQ5.L2") == "DQAMGNRQC"

    with pytest.raises(ValueError):
        signal_metadata.get_qps_pm_class_name_for_ipq("RD1.L2")

    with pytest.raises(KeyError):
        signal_metadata.get_qps_pm_class_name_for_ipq("ABC")


def test_get_qps_pm_class_name_for_ipd():
    timestamp_before_qps_3_upgrade = 1675052360000000000
    timestamp_after_qps_3_upgrade = 1679052360000000000

    assert signal_metadata.get_qps_pm_class_name_for_ipd("RD1.L2", timestamp_before_qps_3_upgrade) == "DQAMGNRD"
    assert signal_metadata.get_qps_pm_class_name_for_ipd("RD1.L2", timestamp_after_qps_3_upgrade) == "DQAMGNRQA"
    assert signal_metadata.get_qps_pm_class_name_for_ipd("RD2.L1", timestamp_before_qps_3_upgrade) == "DQAMGNRQA"
    assert signal_metadata.get_qps_pm_class_name_for_ipd("RD2.L1", timestamp_after_qps_3_upgrade) == "DQAMGNRQA"

    with pytest.raises(ValueError):
        signal_metadata.get_qps_pm_class_name_for_ipd("RQ5.L2", 1674052360000000000)

    with pytest.raises(KeyError):
        signal_metadata.get_qps_pm_class_name_for_ipd("ABC", timestamp_before_qps_3_upgrade)


def test_get_fgc_with_earth_measurement():
    assert signal_metadata.get_fgc_with_earth_measurement("RQ5.L5", 1679052360000000000) == "RPHSB.RR53.RQ5.L5B1"

    with pytest.raises(ValueError):
        signal_metadata.get_fgc_with_earth_measurement("RB.A12", 1679052360000000000)


def test_get_hwc_summary():
    hwc_summary = signal_metadata.get_hwc_summary()

    assert list(hwc_summary.columns) == [
        "campaign",
        "testId",
        "systemName",
        "testName",
        "executionStartTime",
        "executionEndTime",
        "executedTestStatus",
        "PO_EXPERT",
        "QPS_EXPERT",
        "MI_EXPERT",
        "MPP_EXPERT",
        "CRYO_EXPERT",
        "EIC",
        "EE_IST_EXPERT",
        "QPS_IST_EXPERT",
        "ELQA_EXPERT",
        "BT_LBDS_EXPERT",
        "BLM_EXPERT",
        "MACHINE_CHECK_OUT_EXPERT",
        "EE_EXPERT",
        "HWC_EDSL_AUTOMATED",
        "MCS_COLLIMATION",
        "HWC_SIGMON_AUTOMATED",
        "automatedAnalysisResult",
    ]


@pytest.mark.parametrize(
    ("circuit_name", "expected"),
    [
        (
            "RB.A12",
            {
                "A8R1",
                "B8R1",
                "A9R1",
                "B9R1",
                "A10R1",
                "B10R1",
                "A11R1",
                "B11R1",
                "A12R1",
                "B12R1",
                "C12R1",
                "A13R1",
                "B13R1",
                "C13R1",
                "A14R1",
                "B14R1",
                "C14R1",
                "A15R1",
                "B15R1",
                "C15R1",
                "A16R1",
                "B16R1",
                "C16R1",
                "A17R1",
                "B17R1",
                "C17R1",
                "A18R1",
                "B18R1",
                "C18R1",
                "A19R1",
                "B19R1",
                "C19R1",
                "A20R1",
                "B20R1",
                "C20R1",
                "A21R1",
                "B21R1",
                "C21R1",
                "A22R1",
                "B22R1",
                "C22R1",
                "A23R1",
                "B23R1",
                "C23R1",
                "A24R1",
                "B24R1",
                "C24R1",
                "A25R1",
                "B25R1",
                "C25R1",
                "A26R1",
                "B26R1",
                "C26R1",
                "A27R1",
                "B27R1",
                "C27R1",
                "A28R1",
                "B28R1",
                "C28R1",
                "A29R1",
                "B29R1",
                "C29R1",
                "A30R1",
                "B30R1",
                "C30R1",
                "A31R1",
                "B31R1",
                "C31R1",
                "A32R1",
                "B32R1",
                "C32R1",
                "A33R1",
                "B33R1",
                "C33R1",
                "A34R1",
                "B34R1",
                "C34R1",
                "C34L2",
                "B34L2",
                "A34L2",
                "C33L2",
                "B33L2",
                "A33L2",
                "C32L2",
                "B32L2",
                "A32L2",
                "C31L2",
                "B31L2",
                "A31L2",
                "C30L2",
                "B30L2",
                "A30L2",
                "C29L2",
                "B29L2",
                "A29L2",
                "C28L2",
                "B28L2",
                "A28L2",
                "C27L2",
                "B27L2",
                "A27L2",
                "C26L2",
                "B26L2",
                "A26L2",
                "C25L2",
                "B25L2",
                "A25L2",
                "C24L2",
                "B24L2",
                "A24L2",
                "C23L2",
                "B23L2",
                "A23L2",
                "C22L2",
                "B22L2",
                "A22L2",
                "C21L2",
                "B21L2",
                "A21L2",
                "C20L2",
                "B20L2",
                "A20L2",
                "C19L2",
                "B19L2",
                "A19L2",
                "C18L2",
                "B18L2",
                "A18L2",
                "C17L2",
                "B17L2",
                "A17L2",
                "C16L2",
                "B16L2",
                "A16L2",
                "C15L2",
                "B15L2",
                "A15L2",
                "C14L2",
                "B14L2",
                "A14L2",
                "C13L2",
                "B13L2",
                "A13L2",
                "C12L2",
                "B12L2",
                "A12L2",
                "B11L2",
                "A11L2",
                "B10L2",
                "A10L2",
                "B9L2",
                "A9L2",
                "B8L2",
                "A8L2",
            },
        ),
        (
            "RQD.A12",
            {
                "22L2",
                "27L2",
                "13L2",
                "13R1",
                "25L2",
                "24R1",
                "19L2",
                "19R1",
                "29R1",
                "33L2",
                "16L2",
                "12L2",
                "14L2",
                "14R1",
                "20R1",
                "11R1",
                "28R1",
                "27R1",
                "11L2",
                "31L2",
                "15R1",
                "29L2",
                "30R1",
                "20L2",
                "16R1",
                "21R1",
                "23L2",
                "26L2",
                "12R1",
                "18R1",
                "31R1",
                "34R1",
                "18L2",
                "25R1",
                "30L2",
                "24L2",
                "21L2",
                "22R1",
                "26R1",
                "33R1",
                "15L2",
                "28L2",
                "32L2",
                "32R1",
                "17L2",
                "17R1",
                "23R1",
            },
        ),
    ],
)
def test_get_cells_for_circuit(circuit_name: str, expected: set[str]):
    assert signal_metadata.get_cells_for_circuit(circuit_name) == expected


def test_get_cells_for_circuit_invalid_circuit_type():
    with pytest.raises(ValueError):
        signal_metadata.get_cells_for_circuit("RD1.L2")
