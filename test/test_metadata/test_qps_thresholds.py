import os
from pathlib import Path
from unittest.mock import patch

import pandas as pd
import pytest

from lhcsmapi.metadata.thresholds.qps_thresholds import (
    get_thresholds_for_magnet,
    QpsThreshold,
    get_thresholds_for_lead,
    get_thresholds_for_busbar,
)


def read_csv():
    path = Path(os.path.dirname(__file__))
    full_path = os.path.join(path.parent, "resources/thresholds/test_qps_thresholds.csv")
    return pd.read_csv(full_path)


PARAMS_FOR_MAGNETS = [
    (
        "RQX.R8",
        "2020-06-07 16:55:11.672000000",
        [
            QpsThreshold("DQQDT", "differential", 100.0, 100.0, 10.0),
            QpsThreshold("DQQDT", "absolute", 8500.0, 8500.0, 10.0),
        ],
    ),
    (
        "RQX.R8",
        "2021-06-07 16:55:11.672000000",
        [
            QpsThreshold("DQQDT", "differential", 200.0, 200.0, 20.0),
            QpsThreshold("DQQDT", "absolute", 2500.0, 2500.0, 20.0),
        ],
    ),
    ("RU.L4", "2021-06-07 16:55:11.672000000", [QpsThreshold("DQQDT", "absolute", 2500.0, 2500.0, 20.0)]),
    ("test", "2021-06-07 16:55:11.672000000", []),
]

PARAMS_FOR_LEADS = [
    (
        "RQX.R8",
        "2020-06-07 16:55:11.672000000",
        [
            QpsThreshold("DQQDC", "resistive part (U_RES)", 100.0, 100.0, 100.0),
            QpsThreshold("DQQDC", "HTS part (U_HTS)", 3.0, 3.0, 100.0),
        ],
    ),
    ("RU.L4", "2021-06-07 16:55:11.672000000", [QpsThreshold("DQQDC", "HTS part (U_HTS)", 300.0, 300.0, 100.0)]),
    ("RU.L4", "2020-06-07 16:55:11.672000000", [QpsThreshold("DQQDC", "HTS part (U_HTS)", 100.0, 100.0, 100.0)]),
    ("test", "2021-06-07 16:55:11.672000000", []),
]

PARAMS_FOR_BUSBAR = [
    ("A31L5", "2020-06-07 16:55:11.672000000", [QpsThreshold("DQQBS", "L_COMP disabled", 4.0, 4.0, 10000.0)]),
    (
        "A31L5",
        "2021-06-07 16:55:11.672000000",
        [
            QpsThreshold("DQQDB", "DQQDB board", 250000.0, 200000.0, 100.0),
            QpsThreshold("DQQDB", "L_COMP disabled", 4.0, 4.0, 10000.0),
        ],
    ),
    ("test", "2021-06-07 16:55:11.672000000", []),
]


@patch("lhcsmapi.metadata.thresholds.qps_thresholds._read_qps_thresholds_from_csv")
@pytest.mark.parametrize("source,timestamp,ref_thresholds", PARAMS_FOR_MAGNETS)
def test_get_qps_thresholds_for_magnet(mocked_data, source, timestamp, ref_thresholds):
    mocked_data.return_value = read_csv()

    thresholds = get_thresholds_for_magnet(source, timestamp)
    assert thresholds == ref_thresholds


@patch("lhcsmapi.metadata.thresholds.qps_thresholds._read_qps_thresholds_from_csv")
@pytest.mark.parametrize("source,timestamp,ref_thresholds", PARAMS_FOR_LEADS)
def test_get_qps_thresholds_for_lead(mocked_data, source, timestamp, ref_thresholds):
    mocked_data.return_value = read_csv()

    thresholds = get_thresholds_for_lead(source, timestamp)
    assert thresholds == ref_thresholds


@patch("lhcsmapi.metadata.thresholds.qps_thresholds._read_qps_thresholds_from_csv")
@pytest.mark.parametrize("source,timestamp,ref_thresholds", PARAMS_FOR_BUSBAR)
def test_get_qps_thresholds_for_busbar(mocked_data, source, timestamp, ref_thresholds):
    mocked_data.return_value = read_csv()

    thresholds = get_thresholds_for_busbar(source, timestamp)
    assert thresholds == ref_thresholds
