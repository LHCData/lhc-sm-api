import unittest

from lhcsmapi.gui.DateTimeBaseModule import DateTimeBaseModule


class TestDateTimeBaseModule(unittest.TestCase):
    def test_validate_correct_dates(self):
        start_date = "2021-02-25 00:00:00+01:00"
        end_date = "2022-02-25 00:00:00+01:00"
        result = DateTimeBaseModule.check_date_time(start_date, end_date)
        self.assertTrue(result)

    def test_warn_about_invalid_dates(self):
        start_date = "2021-02-25 00:00:00+01:00"
        end_date = "202e2-02-25 00:00:0w0+01:00"
        expected_message = "Provided dates are invalid."
        with self.assertWarns(UserWarning) as context:
            result = DateTimeBaseModule.check_date_time(start_date, end_date)
        self.assertFalse(result)
        self.assertEqual(expected_message, context.warning.args[0])

    def test_warn_when_start_date_later_than_end(self):
        start_date = "2023-02-25 00:00:00+01:00"
        end_date = "2022-02-25 00:00:00+01:00"
        expected_message = "Start time is not later than the end time."
        with self.assertWarns(UserWarning) as context:
            result = DateTimeBaseModule.check_date_time(start_date, end_date)
        self.assertFalse(result)
        self.assertEqual(expected_message, context.warning.args[0])
