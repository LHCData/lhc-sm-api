import unittest

from lhcsmapi.gui import pc
from lhcsmapi.gui.pc import select
from lhcsmapi.gui.pc.FgcPmCircuitNameDropdownBaseModule import get_circuit_names_or_prefixes_to_display
from lhcsmapi.gui.pc.find.impl.R600AFgcPmFindButtonBaseModule import combine_coupled_fgc_pm_entries


class Test_FgcPmSearchBaseModule_for_600ARcbxhv(unittest.TestCase):
    def test_get_circuit_names_600A(self):
        # arrange
        circuit_type = "600A"

        # act
        circuit_names_act = get_circuit_names_or_prefixes_to_display(circuit_type)

        # assert
        circuit_names_ref = [
            "RCS",
            "ROD",
            "ROF",
            "RQ6",
            "RQS",
            "RQSX3",
            "RQT12",
            "RQT13",
            "RQTD",
            "RQTF",
            "RQTL10",
            "RQTL11",
            "RQTL7",
            "RQTL8",
            "RQTL9",
            "RSD1",
            "RSD2",
            "RSF1",
            "RSF2",
            "RSS",
            "RU",
        ]
        circuit_names_act.sort()
        circuit_names_ref.sort()
        self.assertEqual(circuit_names_ref, circuit_names_act)

    def test_get_circuit_names_600A_RCBXHV(self):
        # arrange
        circuit_type = "600A_RCBXHV"

        # act
        circuit_names_act = get_circuit_names_or_prefixes_to_display(circuit_type)

        # assert
        circuit_names_ref = [
            "RCBX1.L1",
            "RCBX1.L2",
            "RCBX1.L5",
            "RCBX1.L8",
            "RCBX1.R1",
            "RCBX1.R2",
            "RCBX1.R5",
            "RCBX1.R8",
            "RCBX2.L1",
            "RCBX2.L2",
            "RCBX2.L5",
            "RCBX2.L8",
            "RCBX2.R1",
            "RCBX2.R2",
            "RCBX2.R5",
            "RCBX2.R8",
            "RCBX3.L1",
            "RCBX3.L2",
            "RCBX3.L5",
            "RCBX3.L8",
            "RCBX3.R1",
            "RCBX3.R2",
            "RCBX3.R5",
            "RCBX3.R8",
        ]
        circuit_names_act.sort()
        circuit_names_ref.sort()
        self.assertEqual(circuit_names_ref, circuit_names_act)

    def test_get_circuit_names_600A_RCDO(self):
        # arrange
        circuit_type = "600A_RCDO"

        # act
        circuit_names_act = get_circuit_names_or_prefixes_to_display(circuit_type)

        # assert
        circuit_names_ref = [
            "RC.A12B1",
            "RC.A12B2",
            "RC.A23B1",
            "RC.A23B2",
            "RC.A34B1",
            "RC.A34B2",
            "RC.A45B1",
            "RC.A45B2",
            "RC.A56B1",
            "RC.A56B2",
            "RC.A67B1",
            "RC.A67B2",
            "RC.A78B1",
            "RC.A78B2",
            "RC.A81B1",
            "RC.A81B2",
        ]
        circuit_names_act.sort()
        circuit_names_ref.sort()
        self.assertEqual(circuit_names_ref, circuit_names_act)

    def test_find_coupled_fgc_pm_entries_rcbx(self):
        # arrange
        fgc_pm_entries = [
            pc.Fgc("RCBXV1.R1", 1493219269220000000),
            pc.Fgc("RCBXH1.R1", 1493219269240000000),
            pc.Fgc("RCBXV1.R1", 1493238314340000000),
            pc.Fgc("RCBXH1.R1", 1493238314400000000),
        ]

        # act
        compacted_fgc_pm_entries_act = combine_coupled_fgc_pm_entries(
            fgc_pm_entries, circuit_prefixes=["RCBXH", "RCBXV"]
        )

        # assert
        compacted_fgc_pm_entries_ref = [
            (pc.Fgc("RCBXV1.R1", 1493219269220000000), pc.Fgc("RCBXH1.R1", 1493219269240000000)),
            (pc.Fgc("RCBXV1.R1", 1493238314340000000), pc.Fgc("RCBXH1.R1", 1493238314400000000)),
        ]

        self.assertListEqual(compacted_fgc_pm_entries_ref, compacted_fgc_pm_entries_act)

    def test_find_coupled_fgc_pm_entries_rcbx_mixed(self):
        # arrange
        fgc_pm_entries = [
            pc.Fgc("RCBXV1.R1", 1493219269220000000),
            pc.Fgc("RCBXH1.R1", 1593219269240000000),
            pc.Fgc("RCBXV1.R1", 1493238314340000000),
            pc.Fgc("RCBXH1.R1", 1493238314400000000),
        ]

        # act
        compacted_fgc_pm_entries_act = combine_coupled_fgc_pm_entries(
            fgc_pm_entries, circuit_prefixes=["RCBXH", "RCBXV"]
        )

        # assert
        compacted_fgc_pm_entries_ref = [
            pc.Fgc("RCBXV1.R1", 1493219269220000000),
            pc.Fgc("RCBXH1.R1", 1593219269240000000),
            (pc.Fgc("RCBXV1.R1", 1493238314340000000), pc.Fgc("RCBXH1.R1", 1493238314400000000)),
        ]

        self.assertListEqual(compacted_fgc_pm_entries_ref, compacted_fgc_pm_entries_act)

    def test_find_coupled_fgc_pm_entries_rcbx_different_names(self):
        # arrange
        fgc_pm_entries = [
            pc.Fgc("RCBXV1.R1", 1493219269220000000),
            pc.Fgc("RCBXH2.R1", 1593219269240000000),
            pc.Fgc("RCBXV1.R1", 1493238314340000000),
            pc.Fgc("RCBXH2.R1", 1493238314400000000),
        ]

        # act
        compacted_fgc_pm_entries_act = combine_coupled_fgc_pm_entries(
            fgc_pm_entries, circuit_prefixes=["RCBXH", "RCBXV"]
        )

        # assert
        compacted_fgc_pm_entries_ref = [
            pc.Fgc("RCBXV1.R1", 1493219269220000000),
            pc.Fgc("RCBXH2.R1", 1593219269240000000),
            pc.Fgc("RCBXV1.R1", 1493238314340000000),
            pc.Fgc("RCBXH2.R1", 1493238314400000000),
        ]

        self.assertListEqual(compacted_fgc_pm_entries_ref, compacted_fgc_pm_entries_act)

    def test_convert_fgc_pm_entries_to_options(self):
        # arrange
        fgc_pm_entries = [
            pc.RcbxhvFgc(pc.Fgc("RCBXV1.R1", 1493219269220000000), pc.Fgc("RCBXH1.R1", 1493219269240000000)),
            pc.RcbxhvFgc(pc.Fgc("RCBXV1.R1", 1493238314340000000), pc.Fgc("RCBXH1.R1", 1493238314400000000)),
        ]

        # act
        options_act = select.get_module_for_circuit_type("600A_RCBXHV").convert_fgc_pm_events_to_options(fgc_pm_entries)

        # assert
        options_ref = [
            "RCBXV1.R1: 2017-04-26 17:07:49.220000+02:00; RCBXH1.R1: 2017-04-26 17:07:49.240000+02:00",
            "RCBXV1.R1: 2017-04-26 22:25:14.340000+02:00; RCBXH1.R1: 2017-04-26 22:25:14.400000+02:00",
        ]
        self.assertListEqual(options_ref, options_act)
