import unittest

from lhcsmapi.gui.pc.find.impl.ItFgcPmFindButtonBaseModule import query_pm_convert_pc_to_circuit_name


class TestItFgcPmFindButtonBaseModule(unittest.TestCase):
    def test_query_pm_convert_pc_to_circuit_name_no_data(self):
        result = query_pm_convert_pc_to_circuit_name(1611615600000000000, 1612047600000000000, "RQX.L1")

        self.assertListEqual(result, [])

    def test_query_pm_convert_pc_to_circuit_name(self):
        result = query_pm_convert_pc_to_circuit_name(1426423440000000000, 1426509840000000000, "RQX.L1")

        expected_result = [
            ("RQX.L1", 1426445029160000000),
            ("RQX.L1", 1426454775120000000),
            ("RQX.L1", 1426461378020000000),
        ]

        self.assertListEqual(result, expected_result)
