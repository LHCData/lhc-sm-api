import unittest

from lhcsmapi.gui.pc.find.impl.IpdFgcPmFindButtonBaseModule import query_pm_convert_pc_to_circuit_name


class TestIpdFgcPmFindButtonBaseModule(unittest.TestCase):
    def test_query_pm_convert_pc_to_circuit_name_no_data(self):
        result = query_pm_convert_pc_to_circuit_name(1612310400000000000, 1612396800000000000, "RD1.L2")

        self.assertListEqual(result, [])

    def test_query_pm_convert_pc_to_circuit_name(self):
        result = query_pm_convert_pc_to_circuit_name(1423601716380000000, 1423605316380000000, "RD1.R2")

        expected_result = [("RD1.R2", 1423601716380000000)]

        self.assertListEqual(result, expected_result)
