from io import StringIO
from unittest import mock
from unittest.mock import patch

import numpy as np
import pytest

from lhcsmapi.gui import pc
from lhcsmapi.gui.pc import select

PARAMS = [
    (
        "600A",
        [
            pc.Fgc("RCS.A45B1", 1611941460800000000),
            pc.Fgc("RCS.A45B1", 1611943957300000000),
            pc.Fgc("RCS.A45B2", 1611901122500000000),
            pc.Fgc("RCS.A45B2", 1611946760300000000),
            pc.Fgc("RCS.A45B2", 1611949258300000000),
        ],
        [
            "RCS.A45B1: 2021-01-29 18:31:00.800000+01:00",
            "RCS.A45B1: 2021-01-29 19:12:37.300000+01:00",
            "RCS.A45B2: 2021-01-29 07:18:42.500000+01:00",
            "RCS.A45B2: 2021-01-29 19:59:20.300000+01:00",
            "RCS.A45B2: 2021-01-29 20:40:58.300000+01:00",
        ],
    ),
    (
        "80-120A",
        [
            pc.Fgc("RCBCHS5.L8B1", 1611948186420000000),
            pc.Fgc("RCBCHS5.L8B1", 1611948486580000000),
            pc.Fgc("RCBCHS5.L8B1", 1611948635420000000),
            pc.Fgc("RCBCHS5.L8B2", 1611948814920000000),
            pc.Fgc("RCBCHS5.L8B2", 1611949111580000000),
            pc.Fgc("RCBCHS5.L8B2", 1611949260420000000),
        ],
        [
            "RCBCHS5.L8B1: 2021-01-29 20:23:06.420000+01:00",
            "RCBCHS5.L8B1: 2021-01-29 20:28:06.580000+01:00",
            "RCBCHS5.L8B1: 2021-01-29 20:30:35.420000+01:00",
            "RCBCHS5.L8B2: 2021-01-29 20:33:34.920000+01:00",
            "RCBCHS5.L8B2: 2021-01-29 20:38:31.580000+01:00",
            "RCBCHS5.L8B2: 2021-01-29 20:41:00.420000+01:00",
        ],
    ),
    (
        "60A",
        [
            pc.Fgc("RCBH11.L8B1", 1611944437100000000),
            pc.Fgc("RCBH11.L8B1", 1611945091600000000),
            pc.Fgc("RCBH11.R7B1", 1611944436620000000),
            pc.Fgc("RCBH11.R7B1", 1611945091620000000),
        ],
        [
            "RCBH11.L8B1: 2021-01-29 19:20:37.100000+01:00",
            "RCBH11.L8B1: 2021-01-29 19:31:31.600000+01:00",
            "RCBH11.R7B1: 2021-01-29 19:20:36.620000+01:00",
            "RCBH11.R7B1: 2021-01-29 19:31:31.620000+01:00",
        ],
    ),
    ("IPD", [pc.Fgc("RD1.L2", 1543841585220000000)], ["2018-12-03 13:53:05.220000+01:00"]),
    (
        "IPQ",
        [
            pc.Fgc("RQ4.L2", 1544455364980000000),
            pc.Fgc("RQ4.L2", 1544465633760000000),
            pc.Fgc("RQ4.L2", 1544476819860000000),
        ],
        [
            "RQ4.L2: 2018-12-10 16:22:44.980000+01:00",
            "RQ4.L2: 2018-12-10 19:13:53.760000+01:00",
            "RQ4.L2: 2018-12-10 22:20:19.860000+01:00",
        ],
    ),
    ("IT", [pc.Fgc("RQX.L1", 1535558373560000000)], ["2018-08-29 17:59:33.560000+02:00"]),
    (
        "RB",
        [
            pc.Fgc("RB.A12", 1544419882440000000),
            pc.Fgc("RB.A12", 1544477282220000000),
            pc.Fgc("RB.A12", 1544517631800000000),
            pc.Fgc("RB.A12", 1544558894000000000),
            pc.Fgc("RB.A12", 1544631694840000000),
        ],
        [
            "2018-12-10 06:31:22.440000+01:00",
            "2018-12-10 22:28:02.220000+01:00",
            "2018-12-11 09:40:31.800000+01:00",
            "2018-12-11 21:08:14+01:00",
            "2018-12-12 17:21:34.840000+01:00",
        ],
    ),
    (
        "RQ",
        [
            pc.RqFgc(pc.Fgc("RQD.A12", 1544456704520000000), pc.Fgc("RQF.A12", 1544456704520000000)),
            pc.RqFgc(pc.Fgc("RQD.A12", 1544466193140000000), pc.Fgc("RQF.A12", 1544466193140000000)),
            pc.RqFgc(pc.Fgc("RQD.A12", 1544550419780000000), pc.Fgc("RQF.A12", 1544550419780000000)),
            pc.RqFgc(pc.Fgc("RQD.A12", 1544622149620000000), pc.Fgc("RQF.A12", 1544622149620000000)),
        ],
        [
            "RQD.A12: 2018-12-10 16:45:04.520000+01:00; RQF.A12: 2018-12-10 16:45:04.520000+01:00",
            "RQD.A12: 2018-12-10 19:23:13.140000+01:00; RQF.A12: 2018-12-10 19:23:13.140000+01:00",
            "RQD.A12: 2018-12-11 18:46:59.780000+01:00; RQF.A12: 2018-12-11 18:46:59.780000+01:00",
            "RQD.A12: 2018-12-12 14:42:29.620000+01:00; RQF.A12: 2018-12-12 14:42:29.620000+01:00",
        ],
    ),
]


@pytest.mark.parametrize("circuit_type,fgc_pm_events,options_ref", PARAMS)
def test_convert_fgc_pm_events_to_options_600A(circuit_type, fgc_pm_events, options_ref):
    fgc_pm_module = select.get_module_for_circuit_type(circuit_type=circuit_type)
    options_act = fgc_pm_module.convert_fgc_pm_events_to_options(fgc_pm_events)

    assert options_ref == options_act


PARAMS_2 = [("IPD", "RD2.L1"), ("IPQ", "RQ4.L2")]


@pytest.mark.parametrize("circuit_type,circuit_name", PARAMS_2)
def test_display_qps_circuit_schematic(circuit_type, circuit_name):
    # arrange
    ipd_fgc_pm_module = select.get_module_for_circuit_type(circuit_type)

    # act
    with patch("sys.stdout", new=StringIO()) as fake_out:
        ipd_fgc_pm_module.display_qps_circuit_schematic(circuit_name)

    # assert
    expected_out = "<IPython.core.display.Image object>"
    assert fake_out.getvalue().strip() == expected_out


def test_update_event_selection_not_nan_timestamp_used():
    not_none_timestmap = 1646773189300000000
    selection_tool = select.get_module_for_circuit_type("600A_RCBXHV")
    selection_tool.fgc_pm_events = [
        pc.RcbxhvFgc(
            rcbxh=pc.Fgc(circuit_name="RCBXH3.R2", timestamp=np.nan),
            rcbxv=pc.Fgc(circuit_name="RCBXV3.R2", timestamp=not_none_timestmap),
        )
    ]

    with mock.patch.object(selection_tool, "query_self_fgc_pm_events") as mocked:
        selection_tool.update_event_selection()
        mocked.assert_called_once_with(not_none_timestmap, [(5, "s"), (5, "s")])
