import unittest

from lhcsmapi.gui.pc.FgcPmCircuitNameDropdownBaseModule import get_circuit_names_or_prefixes_to_display


class TestRbFgcPmSearchBaseModule(unittest.TestCase):
    def test_get_circuit_names_rb(self):
        # arrange
        circuit_type = "RB"

        # act
        circuit_names_act = get_circuit_names_or_prefixes_to_display(circuit_type)

        # assert
        circuit_names_ref = ["RB.A12", "RB.A23", "RB.A34", "RB.A45", "RB.A56", "RB.A67", "RB.A78", "RB.A81"]

        circuit_names_act.sort()
        circuit_names_ref.sort()
        self.assertEqual(circuit_names_ref, circuit_names_act)
