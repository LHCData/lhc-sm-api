import unittest

from lhcsmapi.gui.pc.FgcPmCircuitNameDropdownBaseModule import get_circuit_names_or_prefixes_to_display


class TestRqFgcPmSearchBaseModule(unittest.TestCase):
    def test_get_circuit_names_rq(self):
        # arrange
        circuit_type = "RQ"

        # act
        circuit_names_act = get_circuit_names_or_prefixes_to_display(circuit_type)

        # assert
        circuit_names_ref = ["RQ.A12", "RQ.A23", "RQ.A34", "RQ.A45", "RQ.A56", "RQ.A67", "RQ.A78", "RQ.A81"]

        circuit_names_act.sort()
        circuit_names_ref.sort()
        self.assertEqual(circuit_names_ref, circuit_names_act)
