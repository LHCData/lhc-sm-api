import numpy as np
import pandas as pd
from pandas.testing import assert_frame_equal

from lhcsmapi.utils import dataframe

_TEST_DF = pd.DataFrame({"foo": [1, 2, 3, 4]})


def test_get_columns():
    # arrange
    expected_df = pd.DataFrame({"foo": [1, 2, 3, 4], "bar": [np.nan] * 4})

    # act
    result = dataframe.get_columns(_TEST_DF, ["foo", "bar"])

    # assert
    assert_frame_equal(result, expected_df)


def test_get_columns_empty():
    # arrange
    column = "foo"
    expected_df = pd.DataFrame(columns=["foo"])

    # act
    result = dataframe.get_columns(None, [column])

    # assert
    assert_frame_equal(result, expected_df)


def test_get_columns_none():
    # act
    result = dataframe.get_columns(_TEST_DF, [])

    # assert
    assert result.empty
