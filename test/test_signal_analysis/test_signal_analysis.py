from typing import Any

import pandas as pd
import pytest

from lhcsmapi import signal_analysis


@pytest.mark.parametrize(
    "ser, threshold, min_duration, expected",
    [
        (pd.Series([]), 2, 1, []),  # empty series
        (pd.Series([1, 1, 1, 1, 1, 1, 1, 1, 1, 1]), 2, 9, [(0, 9)]),  # regular case
        (pd.Series([1, 1, 1, 1, 1, 1, 1, 1, 1, 1]), 0.5, 9, []),  # threshold too low
        (pd.Series([1, 1, 1, 1, 1, 1, 1, 1, 1, 1]), 2, 10, []),  # min_duration too high
        (pd.Series([1, 1, 1, 1, 1, 1, 1, 1, 1, 2]), 2, 1, [(0, 8)]),  # one plateau on the left
        (pd.Series([2, 1, 1, 1, 1, 1, 1, 1, 1, 2]), 2, 1, [(1, 8)]),  # one plateau in the middle
        (pd.Series([2, 1, 1, 1, 1, 1, 1, 1, 1, 1]), 2, 1, [(1, 9)]),  # one plateau on the right
        (pd.Series([1, 1, 2, 1, 1, 1, 1, 2, 1, 1]), 2, 1, [(0, 1), (3, 6), (8, 9)]),  # multiple plateaus
        (pd.Series([1, 1, 1], index=[0, 10, 20]), 2, 1, [(0, 20)]),  # irregular index, all values under threshold
        (pd.Series([1, 1, 2, 1, 1], index=[0, 10, 20, 30, 40]), 2, 10, [(0, 10), (30, 40)]),  # irregular index
    ],
)
def test_get_regions_under_threshold(
    ser: pd.Series, threshold: float, min_duration: float, expected: list[tuple[Any, Any]]
):
    assert signal_analysis.get_regions_under_threshold(ser, threshold, min_duration) == expected
