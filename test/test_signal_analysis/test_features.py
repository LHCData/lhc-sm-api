import numpy as np
import pandas as pd
import pytest

import lhcsmapi.signal_analysis.features as signal_analysis
import lhcsmapi.signal_analysis.functions as signal_analysis_functions


def test_calculate_features_single_df_multiple_features():
    x = np.arange(100) * 0.001
    tau = 0.8
    df = pd.DataFrame(data=np.exp(-x / tau), index=x, columns=["Voltage1"])

    expected_out = pd.DataFrame(
        columns=["Voltage1:tau_charge", "Voltage1:tau_energy", "Voltage1:tau_exp_fit", "Voltage1:tau_lin_reg"],
        data=[[tau] * 4],
        index=[0],
    )

    functions_to_apply = [
        signal_analysis_functions.tau_charge,
        signal_analysis_functions.tau_energy,
        signal_analysis_functions.tau_exp_fit,
        signal_analysis_functions.tau_lin_reg,
    ]

    out = signal_analysis.calculate_features(df, functions_to_apply, new_index=0)

    pd.testing.assert_frame_equal(out, expected_out, atol=1e-4)


def test_calculate_features_single_df_single_feature():
    x = np.arange(100)
    df = pd.DataFrame(data=x, index=x, columns=["Line"])

    expected_out = 99 * 100 / 2  # sum of the numbers up to 99

    out = signal_analysis.calculate_features(df, np.sum)

    assert out == expected_out


def test_calculate_features_multi_dfs_multi_features():
    x = np.arange(100) * 0.01
    tau1 = 0.8
    tau2 = 0.6

    df1 = pd.DataFrame(data=np.exp(-x / tau1), index=x, columns=["Voltage1"])
    df2 = pd.DataFrame(data=np.exp(-x / tau2), index=x, columns=["Voltage2"])

    features = [signal_analysis_functions.tau_charge, signal_analysis_functions.tau_energy]
    new_index = 1

    expected_out = pd.DataFrame(
        columns=["Voltage1:tau_charge", "Voltage1:tau_energy", "Voltage2:tau_charge", "Voltage2:tau_energy"],
        data=[[tau1, tau1, tau2, tau2]],
        index=[new_index],
    )

    out = signal_analysis.calculate_features([df1, df2], features, new_index)

    pd.testing.assert_frame_equal(out, expected_out, atol=1e-4)


def test_calculate_features_multi_dfs_multi_features_one_empty_df():
    x = np.arange(100) * 0.01
    tau1 = 0.8

    df1 = pd.DataFrame(data=np.exp(-x / tau1), index=x, columns=["Voltage1"])
    df2 = pd.DataFrame(index=x, columns=["Voltage2"])

    features = [signal_analysis_functions.tau_charge, signal_analysis_functions.tau_energy]
    new_index = 1

    expected_out = pd.DataFrame(
        columns=["Voltage1:tau_charge", "Voltage1:tau_energy", "Voltage2:tau_charge", "Voltage2:tau_energy"],
        data=[[tau1, tau1, np.nan, np.nan]],
        index=[new_index],
    )

    out = signal_analysis.calculate_features([df1, df2], features, new_index)

    pd.testing.assert_frame_equal(out, expected_out, atol=1e-4)


def test_calculate_features_single_df_single_feature_empty_df():
    df = pd.DataFrame(index=np.arange(100), columns=["Voltage1"])

    out = signal_analysis.calculate_features(df, signal_analysis_functions.tau_charge)

    assert np.isnan(out)


def test_calculate_features_multi_dfs_multi_features_no_concat():
    x = np.arange(100) * 0.01
    tau1 = 0.8
    tau2 = 0.6

    df1 = pd.DataFrame(data=np.exp(-x / tau1), index=x, columns=["Voltage1"])
    df2 = pd.DataFrame(data=np.exp(-x / tau2), index=x, columns=["Voltage2"])

    features = [signal_analysis_functions.tau_charge, signal_analysis_functions.tau_energy]
    new_index = 1

    expected_out1 = pd.DataFrame(
        columns=["Voltage1:tau_charge", "Voltage1:tau_energy"], data=[[tau1, tau1]], index=[new_index]
    )

    expected_out2 = pd.DataFrame(
        columns=["Voltage2:tau_charge", "Voltage2:tau_energy"], data=[[tau2, tau2]], index=[new_index]
    )

    out1, out2 = signal_analysis.calculate_features([df1, df2], features, new_index, False)

    pd.testing.assert_frame_equal(out1, expected_out1, atol=1e-4)
    pd.testing.assert_frame_equal(out2, expected_out2, atol=1e-4)


def test_take_window():
    t = np.arange(100)
    df = pd.DataFrame(data=t, index=t, columns=["Voltage1"])

    out = signal_analysis.take_window(df, 10, 20)
    expected_out = pd.DataFrame(data=t[11:20], index=t[11:20], columns=["Voltage1"])

    pd.testing.assert_frame_equal(out, expected_out)


def test_subtract_min_value():
    t = np.arange(100)
    y = np.arange(100, 200)

    df = pd.DataFrame(data=y, index=t, columns=["Voltage1"])

    out = signal_analysis.subtract_min_value(df)
    expected_out = pd.DataFrame(data=y - 100, index=t, columns=["Voltage1"])

    pd.testing.assert_frame_equal(out, expected_out)


def test_calculate_features_single_df_single_feature_exception():
    df = pd.DataFrame()

    with pytest.raises(signal_analysis.WrongColumnNumberException) as exception:
        signal_analysis.calculate_features(df, np.mean)

    assert str(exception.value) == "Input dataframe should have 1 column"


def test_calculate_features_multi_dfs_no_dataframe():
    # We need to specify the dtype for Series to suppress a deprecation warning
    dfs = [pd.DataFrame()] * 3 + [pd.Series(dtype="float64")]

    with pytest.raises(TypeError) as exception:
        signal_analysis.calculate_features(dfs, np.mean)

    assert str(exception.value) == "Input dfs is a list but it contains elements that are not pd.DataFrame"


def test_calculate_features_multi_dfs_wrong_column_number():
    df_single_column = pd.DataFrame(columns=["C1"])
    df_multi_column = pd.DataFrame(columns=["C1", "C2"])

    dfs = [df_single_column, df_multi_column]

    with pytest.raises(signal_analysis.WrongColumnNumberException) as exception:
        signal_analysis.calculate_features(dfs, [np.mean, np.sum])

    assert str(exception.value) == "Every dataframe should have 1 column"
