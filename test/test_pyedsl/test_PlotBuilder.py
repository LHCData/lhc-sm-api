import unittest
from unittest.mock import patch
import warnings
from pathlib import Path
import os
import pandas as pd
import numpy as np

from lhcsmapi.pyedsl.PlotBuilder import PlotBuilder, create_title


def read_csv(file_name):
    path = Path(os.path.dirname(__file__))
    full_path = os.path.join(str(path.parent), "resources/pyedsl/{}".format(file_name))
    return pd.read_csv(full_path, index_col=0)


class TestPlotBuilder(unittest.TestCase):
    def test_create_title(self):
        # arrange
        circuit_name = "ROD.A56B1"
        timestamp = 1524371479600000000
        signals = ["I_MEAS", "I_REF", "I_A"]

        # act
        title_act = create_title(circuit_name, timestamp, signals)

        # assert
        title_ref = "2018-04-22 06:31:19.600, ROD.A56B1: I_MEAS(t), I_REF(t), I_A(t)"
        self.assertEqual(title_ref, title_act)

    def test_create_title_nan_timestamp(self):
        # arrange
        circuit_name = "ROD.A56B1"
        timestamp = np.nan
        signals = ["I_MEAS", "I_REF", "I_A"]

        # act
        title_act = create_title(circuit_name, timestamp, signals)

        # assert
        title_ref = "ROD.A56B1: I_MEAS(t), I_REF(t), I_A(t)"
        self.assertEqual(title_ref, title_act)

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_single_plot_single_df(self, mock_show=None):
        # arrange
        i_meas_df = read_csv("plot_builder/I_MEAS.csv")

        # act
        PlotBuilder().with_signal(i_meas_df, title="title", grid=True).with_ylabel(ylabel="I_MEAS, [A]").plot()

        if mock_show is not None:
            mock_show.assert_called()

    def test_single_plot_empty_df(self):
        # act
        with warnings.catch_warnings(record=True) as w:
            # act
            PlotBuilder().with_signal(pd.DataFrame(), title="title", grid=True).with_ylabel(ylabel="I_MEAS, [A]").plot()

            # assert
            self.assertEqual("All DataFrames are empty, no plots generated", str(w[0].message))

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_single_plot_many_dfs(self, mock_show=None):
        # arrange
        i_meas_df = read_csv("plot_builder/I_MEAS.csv")

        # act
        PlotBuilder().with_signal([i_meas_df, 1.1 * i_meas_df], title="title", grid=True).with_ylabel(
            ylabel="I_MEAS, [A]"
        ).plot()

        # assert
        if mock_show is not None:
            mock_show.assert_called()

    def test_single_plot_empty_dfs(self):
        # act
        with warnings.catch_warnings(record=True) as w:
            # act
            PlotBuilder().with_signal([pd.DataFrame(), pd.DataFrame()], title="title", grid=True).with_ylabel(
                ylabel="I_MEAS, [A]"
            ).plot()
            # assert
            self.assertEqual("All DataFrames are empty, no plots generated", str(w[0].message))

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_double_plot_two_dfs(self, mock_show=None):
        # arrange
        i_meas_df = read_csv("plot_builder/I_MEAS.csv")
        u_dump_res_df = read_csv("plot_builder/U_DUMP_RES.csv")

        # act
        PlotBuilder().with_signal(i_meas_df, title="title", grid=True).with_ylabel(ylabel="I_MEAS, [A]").with_xlim(
            (-1, 2)
        ).with_signal(u_dump_res_df).with_ylabel(ylabel="U_DUMP_RES, [V]").plot()

        # assert
        if mock_show is not None:
            mock_show.assert_called()

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_double_plot_primary_df_empty(self, mock_show=None):
        # arrange
        u_dump_res_df = read_csv("plot_builder/U_DUMP_RES.csv")

        # act
        PlotBuilder().with_signal(pd.DataFrame(), title="title", grid=True).with_ylabel(ylabel="I_MEAS, [A]").with_xlim(
            (-1, 2)
        ).with_signal(u_dump_res_df).with_ylabel(ylabel="U_DUMP_RES, [V]").plot()

        # assert
        if mock_show is not None:
            mock_show.assert_called()

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_double_plot_secondary_df_empty(self, mock_show=None):
        # arrange
        i_meas_df = read_csv("plot_builder/I_MEAS.csv")

        # act
        PlotBuilder().with_signal(i_meas_df, title="title", grid=True).with_ylabel(ylabel="I_MEAS, [A]").with_xlim(
            (-1, 2)
        ).with_signal(pd.DataFrame()).with_ylabel(ylabel="U_DUMP_RES, [V]").plot()

        # assert
        if mock_show is not None:
            mock_show.assert_called()

    def test_double_plot_empty_dfs(self):
        # act
        with warnings.catch_warnings(record=True) as w:
            PlotBuilder().with_signal(pd.DataFrame(), title="title", grid=True).with_ylabel(
                ylabel="I_MEAS, [A]"
            ).with_xlim((-1, 2)).with_signal(pd.DataFrame()).with_ylabel(ylabel="U_DUMP_RES, [V]").plot()
            self.assertEqual("All DataFrames are empty, no plots generated", str(w[0].message))

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_quadruple_plot_dfs(self, mock_show=None):
        # arrange
        i_dcct_df = read_csv("plot_builder/I_DCCT.csv")
        i_didt_df = read_csv("plot_builder/I_DIDT.csv")
        u_diff_df = read_csv("plot_builder/U_DIFF.csv")
        u_res_df = read_csv("plot_builder/U_RES.csv")

        # act
        PlotBuilder().with_signal(u_diff_df, title="title", figsize=(20, 7), color=["C0"], grid=True).with_ylabel(
            ylabel="U_DIFF, [V]"
        ).with_ylim([-10, 10]).with_legend(loc="upper left").with_signal(i_dcct_df, color=["C2"]).with_ylabel(
            ylabel="I_DCCT, [A]"
        ).with_legend(
            loc="upper right"
        ).with_signal(
            i_didt_df, color=["C3"]
        ).with_ylabel(
            ylabel="I_DIDT, [A/s]"
        ).with_legend(
            loc="lower right"
        ).with_spines(
            axes="right", position=1.1
        ).with_signal(
            u_res_df, color=["C1"]
        ).with_ylabel(
            ylabel="U_RES, [V]"
        ).with_ylim(
            [-0.25, 0.25]
        ).with_xlim(
            [-0.1, 0.1]
        ).with_legend(
            loc="lower left"
        ).with_spines(
            axes="right", position=-0.11
        ).with_tick_params(
            axis="y", direction="in", pad=-50
        ).with_ylabel_coords(
            position=(-0.175, 0.5)
        ).plot()

        if mock_show is not None:
            mock_show.assert_called()

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_quadruple_plot_dfs_single_empty(self, mock_show=None):
        # arrange
        i_dcct_df = pd.DataFrame()
        i_didt_df = read_csv("plot_builder/I_DIDT.csv")
        u_diff_df = read_csv("plot_builder/U_DIFF.csv")
        u_res_df = read_csv("plot_builder/U_RES.csv")

        # act
        PlotBuilder().with_signal(u_diff_df, title="title", figsize=(20, 7), color=["C0"], grid=True).with_ylabel(
            ylabel="U_DIFF, [V]"
        ).with_ylim([-10, 10]).with_legend(loc="upper left").with_signal(i_dcct_df, color=["C2"]).with_ylabel(
            ylabel="I_DCCT, [A]"
        ).with_legend(
            loc="upper right"
        ).with_signal(
            i_didt_df, color=["C3"]
        ).with_ylabel(
            ylabel="I_DIDT, [A/s]"
        ).with_legend(
            loc="lower right"
        ).with_spines(
            axes="right", position=1.1
        ).with_signal(
            u_res_df, color=["C1"]
        ).with_ylabel(
            ylabel="U_RES, [V]"
        ).with_ylim(
            [-0.25, 0.25]
        ).with_xlim(
            [-0.1, 0.1]
        ).with_legend(
            loc="lower left"
        ).with_spines(
            axes="right", position=-0.11
        ).with_tick_params(
            axis="y", direction="in", pad=-50
        ).with_ylabel_coords(
            position=(-0.175, 0.5)
        ).plot()

        if mock_show is not None:
            mock_show.assert_called()

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_quadruple_plot_dfs_double_empty(self, mock_show=None):
        # arrange
        i_dcct_df = pd.DataFrame()
        i_didt_df = pd.DataFrame()
        u_diff_df = read_csv("plot_builder/U_DIFF.csv")
        u_res_df = read_csv("plot_builder/U_RES.csv")

        # act
        PlotBuilder().with_signal(u_diff_df, title="title", figsize=(20, 7), color=["C0"], grid=True).with_ylabel(
            ylabel="U_DIFF, [V]"
        ).with_ylim([-10, 10]).with_legend(loc="upper left").with_signal(i_dcct_df, color=["C2"]).with_ylabel(
            ylabel="I_DCCT, [A]"
        ).with_legend(
            loc="upper right"
        ).with_signal(
            i_didt_df, color=["C3"]
        ).with_ylabel(
            ylabel="I_DIDT, [A/s]"
        ).with_legend(
            loc="lower right"
        ).with_spines(
            axes="right", position=1.1
        ).with_signal(
            u_res_df, color=["C1"]
        ).with_ylabel(
            ylabel="U_RES, [V]"
        ).with_ylim(
            [-0.25, 0.25]
        ).with_xlim(
            [-0.1, 0.1]
        ).with_legend(
            loc="lower left"
        ).with_spines(
            axes="right", position=-0.11
        ).with_tick_params(
            axis="y", direction="in", pad=-50
        ).with_ylabel_coords(
            position=(-0.175, 0.5)
        ).plot()

        if mock_show is not None:
            mock_show.assert_called()

    @patch("lhcsmapi.pyedsl.PlotBuilder.plt.show")
    def test_quadruple_plot_dfs_triple_empty(self, mock_show=None):
        # arrange
        i_dcct_df = pd.DataFrame()
        i_didt_df = pd.DataFrame()
        u_diff_df = pd.DataFrame()
        u_res_df = read_csv("plot_builder/U_RES.csv")

        # act
        PlotBuilder().with_signal(u_diff_df, title="title", figsize=(20, 7), color=["C0"], grid=True).with_ylabel(
            ylabel="U_DIFF, [V]"
        ).with_ylim([-10, 10]).with_legend(loc="upper left").with_signal(i_dcct_df, color=["C2"]).with_ylabel(
            ylabel="I_DCCT, [A]"
        ).with_legend(
            loc="upper right"
        ).with_signal(
            i_didt_df, color=["C3"]
        ).with_ylabel(
            ylabel="I_DIDT, [A/s]"
        ).with_legend(
            loc="lower right"
        ).with_spines(
            axes="right", position=1.1
        ).with_signal(
            u_res_df, color=["C1"]
        ).with_ylabel(
            ylabel="U_RES, [V]"
        ).with_ylim(
            [-0.25, 0.25]
        ).with_xlim(
            [-0.1, 0.1]
        ).with_legend(
            loc="lower left"
        ).with_spines(
            axes="right", position=-0.11
        ).with_tick_params(
            axis="y", direction="in", pad=-50
        ).with_ylabel_coords(
            position=(-0.175, 0.5)
        ).plot()

        if mock_show is not None:
            mock_show.assert_called()

    def test_quadruple_plot_dfs_quadruple_empty(self):
        # arrange
        i_dcct_df = pd.DataFrame()
        i_didt_df = pd.DataFrame()
        u_diff_df = pd.DataFrame()
        u_res_df = pd.DataFrame()

        # act
        with warnings.catch_warnings(record=True) as w:
            PlotBuilder().with_signal(u_diff_df, title="title", figsize=(20, 7), color=["C0"], grid=True).with_ylabel(
                ylabel="U_DIFF, [V]"
            ).with_ylim([-10, 10]).with_legend(loc="upper left").with_signal(i_dcct_df, color=["C2"]).with_ylabel(
                label="I_DCCT, [A]"
            ).with_legend(
                loc="upper right"
            ).with_signal(
                i_didt_df, color=["C3"]
            ).with_ylabel(
                ylabel="I_DIDT, [A/s]"
            ).with_legend(
                loc="lower right"
            ).with_spines(
                axes="right", position=1.1
            ).with_signal(
                u_res_df, color=["C1"]
            ).with_ylabel(
                ylabel="U_RES, [V]"
            ).with_ylim(
                [-0.25, 0.25]
            ).with_xlim(
                [-0.1, 0.1]
            ).with_legend(
                loc="lower left"
            ).with_spines(
                axes="right", position=-0.11
            ).with_tick_params(
                axis="y", direction="in", pad=-50
            ).with_ylabel_coords(
                position=(-0.175, 0.5)
            ).plot()
            self.assertEqual("All DataFrames are empty, no plots generated", str(w[0].message))
