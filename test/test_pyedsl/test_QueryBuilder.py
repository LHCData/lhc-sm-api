import unittest
from pathlib import Path
from unittest.mock import patch

import pandas as pd

from lhcsmapi.api.query_builder import QueryBuilder
from lhcsmapi.api.resolver import VariableQueryParams
from lhcsmapi.metadata import signal_metadata

_EMPTY_SIGNAL = pd.Series(name="signal_name").rename_axis(index="timestamp")


def read_csv(file_name):
    path = Path(__file__).parent.parent / "resources" / "pyedsl" / file_name
    return pd.read_csv(path, index_col=0)


class TestQueryBuilder(unittest.TestCase):
    def test_query_builder_wrong_attr_error(self):
        # arrange
        # act

        # assert
        with self.assertRaises(AttributeError) as context:
            QueryBuilder().with_cals()

        self.assertEqual("'QueryBuilder' object has no attribute 'with_cals'", context.exception.args[0])

    def test_query_builder_context_query_pm_error(self):
        # arrange
        qb = QueryBuilder().with_pm()

        # act
        # assert
        with self.assertRaises(AttributeError) as context:
            qb.context_query(None)

        self.assertEqual("'_PmQueryBuilder' object has no attribute 'context_query'", context.exception.args[0])

    def test_query_builder_with_db_time_def_error(self):
        # arrange
        qb = QueryBuilder().with_pm().with_timestamp(1544631694792000000)

        # act
        # assert
        with self.assertRaises(AttributeError) as context:
            qb.signal_query()

        self.assertEqual(
            "'_PmSignalCircuitTypeAndQueryParameterQueryBuilder' object has no attribute 'signal_query'",
            context.exception.args[0],
        )

    def test_query_builder_with_db_time_def_repr_timestamp_with_circuit_type(self):
        # arrange
        qb = QueryBuilder().with_pm().with_timestamp(1544631694792000000)

        # act
        # assert
        with self.assertRaises(ValueError) as context:
            qb.with_circuit_type("ITT")

        self.assertTrue("Circuit type ITT not supported!" in context.exception.args[0])

    def test_query_builder_fault_query_pm_error(self):
        # arrange
        qb = QueryBuilder().with_pm().with_timestamp(1)

        # act
        # assert
        with self.assertRaises(AttributeError) as context:
            qb.fault_query()

        self.assertEqual(
            "'_PmSignalCircuitTypeAndQueryParameterQueryBuilder' object has no attribute 'fault_query'",
            context.exception.args[0],
        )

    def test_query_builder_with_db_time_def_circuit_type_get_attr_error(self):
        # arrange
        qb = QueryBuilder().with_pm().with_timestamp(1544631694792000000).with_circuit_type("RB")

        # act
        # assert
        with self.assertRaises(AttributeError) as context:
            qb.signal_query()

        self.assertEqual(
            "'_PmSignalMetadataQueryBuilder' object has no attribute 'signal_query'", context.exception.args[0]
        )

    def test_query_builder_with_db_time_def_metadata_pm_timestamp_event_query(self):
        # arrange
        qb = (
            QueryBuilder()
            .with_pm()
            .with_timestamp(1544631694792000000)
            .with_circuit_type("RB")
            .with_metadata(circuit_name="RB.A12", system="PC", signal="I_MEAS")
        )

        # act
        # assert
        with self.assertRaises(AttributeError) as context:
            qb.event_query()

        self.assertEqual("'_PmSignalQueryBuilder' object has no attribute 'event_query'", context.exception.args[0])

    def test_query_builder_with_db_time_def_metadata_pm_duration_feature_query(self):
        # arrange
        qb = (
            QueryBuilder()
            .with_pm()
            .with_duration(t_start=0, t_end=1)
            .with_circuit_type("RB")
            .with_metadata(circuit_name="RB.A12", system="PC", signal="I_MEAS")
        )

        # act
        # assert
        with self.assertRaises(AttributeError) as context:
            qb.feature_query(["max"])

        self.assertEqual("'_PmEventQueryBuilder' object has no attribute 'feature_query'", context.exception.args[0])

    def test_query_builder_with_db_time_def_metadata_nxcals_duration_event_query(self):
        # arrange
        qb = (
            QueryBuilder()
            .with_nxcals(None)
            .with_duration(t_start=0, t_end=1)
            .with_circuit_type("RB")
            .with_metadata(circuit_name="RB.A12", system="PC", signal="I_MEAS")
        )

        # act
        # assert
        with self.assertRaises(AttributeError) as context:
            qb.event_query()

        self.assertEqual("'_NxcalsSignalQueryBuilder' object has no attribute 'event_query'", context.exception.args[0])

    def test_query_builder_with_db_time_def_metadata_nxcals_duration_query_event(self):
        # arrange
        qb = (
            QueryBuilder()
            .with_nxcals(None)
            .with_duration(t_start=0, t_end=1)
            .with_circuit_type("RB")
            .with_metadata(circuit_name="RB.A12", system="PC", signal="I_MEAS")
        )

        # act
        # assert
        with self.assertRaises(AttributeError) as context:
            qb.query_event()

        self.assertEqual("'_NxcalsSignalQueryBuilder' object has no attribute 'query_event'", context.exception.args[0])

    def test_query_builder_pm_timestamp_signal_query(self):
        with patch("lhcsmapi.api.query.query_pm_data_signals", return_value=_EMPTY_SIGNAL) as query:
            QueryBuilder().with_pm().with_timestamp(1426220469520000000).with_circuit_type("RB").with_metadata(
                circuit_name="RB.A45", system="PC", signal="I_MEAS"
            ).signal_query()

        query.assert_called_once_with("FGC", "51_self_pmd", "RPTE.UA47.RB.A45", ["STATUS.I_MEAS"], 1426220469520000000)

    def test_query_builder_pm_timestamp_signal_query_sync_time_convert_to_sec_remove_initial_offset(self):
        with patch("lhcsmapi.api.query.query_pm_data_signals", return_value=_EMPTY_SIGNAL) as query:
            QueryBuilder().with_pm().with_timestamp(1426220469491000000).with_query_parameters(
                system="QPS", className="DQAMCNMB_PMHSU", source="B20L5", signal="I_HDS_4"
            ).signal_query()

        query.assert_called_once_with("QPS", "DQAMCNMB_PMHSU", "B20L5", "I_HDS_4", 1426220469491000000)

    def test_query_builder_pm_duration_event_query(self):
        with patch("lhcsmapi.api.query.query_pm_data_headers", return_value=pd.DataFrame()) as query:
            QueryBuilder().with_pm().with_duration(
                t_start=1426201200000000000, duration=(24 * 60 * 60, "s")
            ).with_circuit_type("RB").with_metadata(circuit_name="RB.A45", system="QH", source="*").event_query()

        query.assert_called_once_with("QPS", "DQAMCNMB_PMHSU", "*", 1426201200000000000, 86400000000000)

    def test_eq_query_parameters_qps_16l2_source(self):
        with patch("lhcsmapi.api.query.query_pm_data_headers", return_value=pd.DataFrame()) as query:
            QueryBuilder().with_pm().with_duration(
                t_start=1544622149620000000, duration=[(10, "s"), (400, "s")]
            ).with_query_parameters(system="QPS", source="16L2", className="DQAMCNMQ_PMHSU").event_query()

        query.assert_called_once_with("QPS", "DQAMCNMQ_PMHSU", "16L2", 1544622139620000000, 410000000000)

    def test_sq_query_parameters_rqda12_fgc_i_meas(self):
        with patch("lhcsmapi.api.query.query_pm_data_signals", return_value=_EMPTY_SIGNAL) as query:
            QueryBuilder().with_pm().with_timestamp(1426220469520000000).with_query_parameters(
                signal="STATUS.I_MEAS", system="FGC", source="RPTE.UA47.RB.A45", className="51_self_pmd"
            ).signal_query()

        query.assert_called_once_with("FGC", "51_self_pmd", "RPTE.UA47.RB.A45", "STATUS.I_MEAS", 1426220469520000000)

    def test_sq_query_parameters_rqda12_fgc_i_meas_i_ref(self):
        with patch("lhcsmapi.api.query.query_pm_data_signals", return_value=_EMPTY_SIGNAL) as query:
            QueryBuilder().with_pm().with_timestamp(1426220469520000000).with_query_parameters(
                signal=["STATUS.I_MEAS", "STATUS.I_REF"],
                system=["FGC", "FGC"],
                source=["RPTE.UA47.RB.A45", "whatever"],
                className=["51_self_pmd", "51_self_pmd"],
            ).signal_query()

        self.assertTrue(query.call_count == 2)
        query.assert_any_call(
            "FGC", "51_self_pmd", "RPTE.UA47.RB.A45", ["STATUS.I_MEAS", "STATUS.I_REF"], 1426220469520000000
        )
        query.assert_any_call("FGC", "51_self_pmd", "whatever", ["STATUS.I_MEAS", "STATUS.I_REF"], 1426220469520000000)

    def test_sq_query_parameters_rqda12_fgc_i_meas_i_ref_polymorphic(self):
        with patch("lhcsmapi.api.query.query_pm_data_signals", return_value=_EMPTY_SIGNAL) as query:
            query.columns = ["mocked_single_column"]
            QueryBuilder().with_pm().with_timestamp(1426220469520000000).with_query_parameters(
                signal=["STATUS.I_MEAS", "STATUS.I_REF"],
                system="FGC",
                source="RPTE.UA47.RB.A45",
                className="51_self_pmd",
            ).signal_query()

        query.assert_called_once_with(
            "FGC", "51_self_pmd", "RPTE.UA47.RB.A45", ["STATUS.I_MEAS", "STATUS.I_REF"], 1426220469520000000
        )

    def test_eq_metadata_rqda12_pc_fgc_1_source(self):
        with patch("lhcsmapi.api.query.query_pm_data_headers", return_value=pd.DataFrame()) as query:
            QueryBuilder().with_pm().with_duration(
                t_start=1544622149620000000, duration=[(10, "s"), (400, "s")]
            ).with_circuit_type("RQ").with_metadata(circuit_name="RQD.A12", system="PC").event_query()

        query.assert_called_once_with("FGC", "51_self_pmd", "RPHE.UA23.RQD.A12", 1544622139620000000, 410000000000)

    def test_eq_metadata_ipq_rq10_source(self):
        with patch("lhcsmapi.api.query.query_pm_data_headers", return_value=pd.DataFrame()) as query:
            t_start = "2021-01-21 09:19:25.335000000"
            t_end = "2021-01-21 09:51:58.466000000"

            QueryBuilder().with_pm().with_duration(t_start=t_start, t_end=t_end).with_circuit_type(
                "IPQ2"
            ).with_metadata(circuit_name="RQ10.R4", system="PC").event_query()

        self.assertTrue(query.call_count == 2)
        query.assert_any_call("FGC", "lhc_self_pmd", "RPHGA.UA47.RQ10.R4B1", 1611217165335000000, 1953131000000)
        query.assert_any_call("FGC", "lhc_self_pmd", "RPHGA.UA47.RQ10.R4B2", 1611217165335000000, 1953131000000)

    def test_eq_metadata_rba12_ee(self):
        with patch("lhcsmapi.api.query.query_pm_data_headers", return_value=pd.DataFrame()) as query:
            QueryBuilder().with_pm().with_duration(
                t_start=1544622149620000000, duration=[(10, "s"), (400, "s")]
            ).with_circuit_type("RB").with_metadata(circuit_name="RB.A12", system=["EE_ODD", "EE_EVEN"]).event_query()

        self.assertTrue(query.call_count == 2)
        query.assert_any_call("QPS", "DQAMSNRB", "RR17.RB.A12", 1544622139620000000, 410000000000)
        query.assert_any_call("QPS", "DQAMSNRB", "UA23.RB.A12", 1544622139620000000, 410000000000)

    def test_eq_metadata_rqda12_qps_qh_16l2_source(self):
        with patch("lhcsmapi.api.query.query_pm_data_headers", return_value=pd.DataFrame()) as query:
            QueryBuilder().with_pm().with_duration(
                t_start=1544622149620000000, duration=[(10, "s"), (400, "s")]
            ).with_circuit_type("RQ").with_metadata(circuit_name="RQD.A12", system="QH", source="16L2").event_query()

        query.assert_called_once_with("QPS", "DQAMCNMQ_PMHSU", "16L2", 1544622139620000000, 410000000000)

    def test_eq_metadata_rqda12_rqfa12_pc_fgc_1_source(self):
        with patch("lhcsmapi.api.query.query_pm_data_headers", return_value=pd.DataFrame()) as query:
            QueryBuilder().with_pm().with_duration(
                timestamp=1544622149620000000, duration=[(10, "s"), (400, "s")]
            ).with_circuit_type("RQ").with_metadata(circuit_name=["RQD.A12", "RQF.A12"], system="PC").event_query()

        self.assertTrue(query.call_count == 2)
        query.assert_any_call("FGC", "51_self_pmd", "RPHE.UA23.RQD.A12", 1544622139620000000, 410000000000)
        query.assert_any_call("FGC", "51_self_pmd", "RPHE.UA23.RQF.A12", 1544622139620000000, 410000000000)

    def test_eq_metadata_rqda12_rqfa12_qps_qh_wildcard_source(self):
        with patch("lhcsmapi.api.query.query_pm_data_headers", return_value=pd.DataFrame()) as query:
            QueryBuilder().with_pm().with_duration(
                t_start=1544622149620000000, duration=[(10, "s"), (400, "s")]
            ).with_circuit_type("RQ").with_metadata(
                circuit_name=["RQD.A12", "RQF.A12"], system="QH", wildcard={"CELL": "16L2"}
            ).event_query()

        query.assert_called_once_with("QPS", "DQAMCNMQ_PMHSU", "16L2", 1544622139620000000, 410000000000)

    def test_sq_metadata_rqda12_qps_qh_u_hds_16l2_source(self):
        with patch("lhcsmapi.api.query.query_pm_data_signals", return_value=_EMPTY_SIGNAL) as query:
            QueryBuilder().with_pm().with_timestamp(timestamp=1544622149620000000).with_circuit_type(
                "RQ"
            ).with_metadata(
                circuit_name="RQD.A12", system="QH", source="16L2", signal="U_HDS", wildcard={"CELL": "16L2"}
            ).signal_query()

        query.assert_called_once_with(
            "QPS", "DQAMCNMQ_PMHSU", "16L2", ["16L2:U_HDS_1", "16L2:U_HDS_2"], 1544622149620000000
        )

    def test_sq_metadata_rqda12_qps_qh_i_hds_16l2_source(self):
        with self.assertRaises(KeyError) as context:
            QueryBuilder().with_pm().with_timestamp(timestamp=1544622149620000000).with_circuit_type(
                "RQ"
            ).with_metadata(
                circuit_name="RQD.A12", system="QH", source="16L2", signal="I_HDS", wildcard={"CELL": "16L2"}
            ).signal_query()

        # test should fail for timestamp before 2020-01-01 00:00:00
        self.assertEqual("I_HDS", context.exception.args[0])

    def test_sq_metadata_rqda12_qps_qh_u_hds_16l2_source_run3(self):
        with patch("lhcsmapi.api.query.query_pm_data_signals", return_value=_EMPTY_SIGNAL) as query:
            QueryBuilder().with_pm().with_timestamp(timestamp=1577833300000000000).with_circuit_type(
                "RQ"
            ).with_metadata(
                circuit_name="RQD.A12", system="QH", source="16L2", signal="U_HDS", wildcard={"CELL": "16L2"}
            ).signal_query()

        query.assert_called_once_with(
            "QPS", "DQAMCNMQ_PMHSU", "16L2", ["16L2:U_HDS_1", "16L2:U_HDS_2"], 1577833300000000000
        )

    def test_sq_metadata_rqda12_pm_fgc_i_meas(self):
        with patch("lhcsmapi.api.query.query_pm_data_signals", return_value=_EMPTY_SIGNAL) as query:
            QueryBuilder().with_pm().with_timestamp(1544622149620000000).with_circuit_type("RQ").with_metadata(
                circuit_name="RQD.A12", system="PC", signal="I_MEAS"
            ).signal_query()

        query.assert_called_once_with("FGC", "51_self_pmd", "RPHE.UA23.RQD.A12", ["STATUS.I_MEAS"], 1544622149620000000)

    def test_sq_metadata_rqda12_rqfa12_pm_fgc_i_meas(self):
        with patch("lhcsmapi.api.query.query_pm_data_signals", return_value=_EMPTY_SIGNAL) as query:
            QueryBuilder().with_pm().with_timestamp(1544622149620000000).with_circuit_type("RQ").with_metadata(
                circuit_name=["RQD.A12", "RQF.A12"], system="PC", signal="I_MEAS"
            ).signal_query()

        self.assertTrue(query.call_count == 2)
        query.assert_any_call("FGC", "51_self_pmd", "RPHE.UA23.RQD.A12", ["STATUS.I_MEAS"], 1544622149620000000)
        query.assert_any_call("FGC", "51_self_pmd", "RPHE.UA23.RQF.A12", ["STATUS.I_MEAS"], 1544622149620000000)

    def test_sq_metadata_rqda12_pm_qds_u_1_ext_u_2_ext(self):
        with patch("lhcsmapi.api.query.query_pm_data_signals", return_value=_EMPTY_SIGNAL) as query:
            QueryBuilder().with_pm().with_timestamp(1544622149598000000).with_circuit_type("RQ").with_metadata(
                circuit_name="RQD.A12", system="QDS", signal=["U_1_EXT", "U_2_EXT"], wildcard={"CELL": "16L2"}
            ).signal_query()

        query.assert_called_once_with(
            "QPS", "DQAMCNMQ_PMSTD", "16L2", ["16L2:U_1_EXT", "16L2:U_2_EXT"], 1544622149598000000
        )

    def test_sq_metadata_rba12_pm_leads_even_leads_odd_u_hts(self):
        with patch("lhcsmapi.api.query.query_pm_data_signals", return_value=_EMPTY_SIGNAL) as query:
            QueryBuilder().with_pm().with_timestamp(1544622149598000000).with_circuit_type("RB").with_metadata(
                circuit_name="RB.A12", system=["LEADS_EVEN", "LEADS_ODD"], signal="U_HTS"
            ).signal_query()

        self.assertTrue(query.call_count == 2)
        query.assert_any_call(
            "QPS",
            "DQAMGNDRBEVEN",
            "RB.A12",
            ["DFLAS.7L2.RB.A12.LD1:U_HTS", "DFLAS.7L2.RB.A12.LD2:U_HTS"],
            1544622149598000000,
        )
        query.assert_any_call(
            "QPS",
            "DQAMGNDRBODD",
            "RB.A12",
            ["DFLAS.7R1.RB.A12.LD3:U_HTS", "DFLAS.7R1.RB.A12.LD4:U_HTS"],
            1544622149598000000,
        )

    def test_sq_metadata_rba12_pm_leads_even_leads_odd_u_hts_u_res(self):
        with patch("lhcsmapi.api.query.query_pm_data_signals", return_value=_EMPTY_SIGNAL) as query:
            QueryBuilder().with_pm().with_timestamp(1544622149598000000).with_circuit_type("RB").with_metadata(
                circuit_name="RB.A12", system=["LEADS_EVEN", "LEADS_ODD"], signal=["U_HTS", "U_RES"]
            ).signal_query()

        self.assertTrue(query.call_count == 2)
        query.assert_any_call(
            "QPS",
            "DQAMGNDRBEVEN",
            "RB.A12",
            [
                "DFLAS.7L2.RB.A12.LD1:U_HTS",
                "DFLAS.7L2.RB.A12.LD2:U_HTS",
                "DFLAS.7L2.RB.A12.LD1:U_RES",
                "DFLAS.7L2.RB.A12.LD2:U_RES",
            ],
            1544622149598000000,
        )
        query.assert_any_call(
            "QPS",
            "DQAMGNDRBODD",
            "RB.A12",
            [
                "DFLAS.7R1.RB.A12.LD3:U_HTS",
                "DFLAS.7R1.RB.A12.LD4:U_HTS",
                "DFLAS.7R1.RB.A12.LD3:U_RES",
                "DFLAS.7R1.RB.A12.LD4:U_RES",
            ],
            1544622149598000000,
        )

    def test_sq_metadata_rba12_rba23_pm_leads_even_leads_odd_u_hts_u_res(self):
        with patch("lhcsmapi.api.query.query_pm_data_signals", return_value=_EMPTY_SIGNAL) as query:
            QueryBuilder().with_pm().with_timestamp(1544622149598000000).with_circuit_type("RB").with_metadata(
                circuit_name=["RB.A12", "RB.A23"], system=["LEADS_EVEN", "LEADS_ODD"], signal=["U_HTS", "U_RES"]
            ).signal_query()

        self.assertTrue(query.call_count == 4)
        query.assert_any_call(
            "QPS",
            "DQAMGNDRBEVEN",
            "RB.A12",
            [
                "DFLAS.7L2.RB.A12.LD1:U_HTS",
                "DFLAS.7L2.RB.A12.LD2:U_HTS",
                "DFLAS.7L2.RB.A12.LD1:U_RES",
                "DFLAS.7L2.RB.A12.LD2:U_RES",
            ],
            1544622149598000000,
        )
        query.assert_any_call(
            "QPS",
            "DQAMGNDRBEVEN",
            "RB.A23",
            [
                "DFLAS.7R2.RB.A23.LD1:U_HTS",
                "DFLAS.7R2.RB.A23.LD2:U_HTS",
                "DFLAS.7R2.RB.A23.LD1:U_RES",
                "DFLAS.7R2.RB.A23.LD2:U_RES",
            ],
            1544622149598000000,
        )
        query.assert_any_call(
            "QPS",
            "DQAMGNDRBODD",
            "RB.A12",
            [
                "DFLAS.7R1.RB.A12.LD3:U_HTS",
                "DFLAS.7R1.RB.A12.LD4:U_HTS",
                "DFLAS.7R1.RB.A12.LD3:U_RES",
                "DFLAS.7R1.RB.A12.LD4:U_RES",
            ],
            1544622149598000000,
        )
        query.assert_any_call(
            "QPS",
            "DQAMGNDRBODD",
            "RB.A23",
            [
                "DFLAS.7L3.RB.A23.LD3:U_HTS",
                "DFLAS.7L3.RB.A23.LD4:U_HTS",
                "DFLAS.7L3.RB.A23.LD3:U_RES",
                "DFLAS.7L3.RB.A23.LD4:U_RES",
            ],
            1544622149598000000,
        )

    def test_metadata_nxcals_signal_query_i_meas_single_circuit(self):
        t_start = "2018-12-12 00:00:00+01:00"
        t_end = "2018-12-13 00:00:00+01:00"
        circuit_type = "RQ"
        circuit_name = "RQD.A12"
        signal = "I_MEAS"
        spark = "spark"

        with patch("lhcsmapi.api.query.query_nxcals_by_variables", return_value=pd.DataFrame(columns=["one"])) as query:
            QueryBuilder().with_nxcals(spark).with_duration(t_start=t_start, t_end=t_end).with_circuit_type(
                circuit_type
            ).with_metadata(circuit_name=circuit_name, system="PC", signal=signal).signal_query()

        query.assert_called_once_with(
            "spark",
            VariableQueryParams(
                system="CMW",
                timestamp=1544569200000000000,
                duration=86400000000000,
                signals=["RPHE.UA23.RQD.A12:I_MEAS"],
            ),
        )

    def test_metadata_nxcals_signal_query_rqd_i_meas(self):
        circuit_name = [cn for cn in signal_metadata.get_circuit_names("RQ") if "D" in cn]
        spark = "spark"

        with patch("lhcsmapi.api.query.query_nxcals_by_variables", return_value=pd.DataFrame(columns=["one"])) as query:
            QueryBuilder().with_nxcals(spark).with_duration(
                t_start="2018-12-12 00:00:00+01:00", t_end="2018-12-13 00:00:00+01:00"
            ).with_circuit_type("RQ").with_metadata(
                circuit_name=circuit_name, system="PC", signal="I_MEAS"
            ).signal_query()

        query.assert_called_once_with(
            "spark",
            VariableQueryParams(
                system="CMW",
                timestamp=1544569200000000000,
                duration=86400000000000,
                signals=[
                    "RPHE.UA23.RQD.A12:I_MEAS",
                    "RPHE.UA27.RQD.A23:I_MEAS",
                    "RPHE.UA43.RQD.A34:I_MEAS",
                    "RPHE.UA47.RQD.A45:I_MEAS",
                    "RPHE.UA63.RQD.A56:I_MEAS",
                    "RPHE.UA67.RQD.A67:I_MEAS",
                    "RPHE.UA83.RQD.A78:I_MEAS",
                    "RPHE.UA87.RQD.A81:I_MEAS",
                ],
            ),
        )

    def test_metadata_nxcals_signal_query_i_meas_single_circuit_many_pcs(self):
        t_start = "2017-04-11 20:38:19.627"
        t_end = "2017-04-11 22:24:50.702"
        circuit_type = "IT"
        circuit_name = "RQX.L1"
        signal = "I_MEAS"
        spark = "spark"

        with patch("lhcsmapi.api.query.query_nxcals_by_variables", return_value=pd.DataFrame(columns=["one"])) as query:
            QueryBuilder().with_nxcals(spark).with_duration(t_start=t_start, t_end=t_end).with_circuit_type(
                circuit_type
            ).with_metadata(circuit_name=circuit_name, system="PC", signal=signal).signal_query()

        query.assert_called_once_with(
            "spark",
            VariableQueryParams(
                system="CMW",
                timestamp=1491935899627000000,
                duration=6391075000000,
                signals=["RPHFC.UL14.RQX.L1:I_MEAS", "RPMBC.UL14.RTQX1.L1:I_MEAS", "RPHGC.UL14.RTQX2.L1:I_MEAS"],
            ),
        )

    def test_metadata_nxcals_signal_query_metadata_update(self):
        spark = "spark"
        circuit_type = "IPD2_B1B2"
        circuit_name = "RD2.L5"
        t_start = "2021-01-26 11:17:54.586000000"
        t_end = "2021-01-26 11:48:18.746000000"
        system = "PC"
        signal_names = "I_MEAS"

        with patch("lhcsmapi.api.query.query_nxcals_by_variables", return_value=pd.DataFrame(columns=["one"])) as query:
            QueryBuilder().with_nxcals(spark).with_duration(t_start=t_start, t_end=t_end).with_circuit_type(
                circuit_type
            ).with_metadata(circuit_name=circuit_name, system=system, signal=signal_names).signal_query()

        query.assert_called_once_with(
            "spark",
            VariableQueryParams(
                system="CMW",
                timestamp=1611656274586000000,
                duration=1824160000000,
                signals=["RPHSB.RR53.RD2.L5:I_MEAS"],
            ),
        )

    def test_metadata_nxcals_feature_query_rb_i_meas(self):
        with patch("lhcsmapi.api.query.query_nxcals_by_variables", return_value=pd.DataFrame(columns=["one"])) as query:
            QueryBuilder().with_nxcals("spark").with_duration(
                t_start="2018-12-12 00:00:00+01:00", t_end="2018-12-13 00:00:00+01:00"
            ).with_circuit_type("RB").with_metadata(circuit_name="*", system="PC", signal="I_MEAS").signal_query()

        query.assert_called_once_with(
            "spark",
            VariableQueryParams(
                system="CMW",
                timestamp=1544569200000000000,
                duration=86400000000000,
                signals=[
                    "RPTE.UA23.RB.A12:I_MEAS",
                    "RPTE.UA27.RB.A23:I_MEAS",
                    "RPTE.UA43.RB.A34:I_MEAS",
                    "RPTE.UA47.RB.A45:I_MEAS",
                    "RPTE.UA63.RB.A56:I_MEAS",
                    "RPTE.UA67.RB.A67:I_MEAS",
                    "RPTE.UA83.RB.A78:I_MEAS",
                    "RPTE.UA87.RB.A81:I_MEAS",
                ],
            ),
        )

    def test_metadata_nxcals_feature_query_rqda12_u_res(self):
        with patch(
            "lhcsmapi.api.query._query_nxcals_by_variables", return_value=pd.DataFrame(columns=["one"])
        ) as query:
            QueryBuilder().with_nxcals("spark").with_duration(
                t_start="2018-12-12 00:00:00+01:00", t_end="2018-12-13 00:00:00+01:00"
            ).with_circuit_type("RQ").with_metadata(
                circuit_name="RQD.A12", system="BUSBAR", signal="U_RES", wildcard={"BUSBAR": "*"}
            ).signal_query()

        expected = list(read_csv("query_builder/nxcals_fq/RQD.A12_U_RES.csv")["signal"].values)
        query.assert_called_once_with(
            "spark", "CMW", 1544569200000000000, 86400000000000, expected, False, 31536000000000000
        )

    def test_metadata_nxcals_feature_query_rqfa12_u_res(self):
        with patch(
            "lhcsmapi.api.query._query_nxcals_by_variables", return_value=pd.DataFrame(columns=["one"])
        ) as query:
            QueryBuilder().with_nxcals("spark").with_duration(
                t_start="2018-12-12 00:00:00+01:00", t_end="2018-12-13 00:00:00+01:00"
            ).with_circuit_type("RQ").with_metadata(
                circuit_name="RQF.A12", system="BUSBAR", signal="U_RES", wildcard={"BUSBAR": "*"}
            ).signal_query()

        expected = list(read_csv("query_builder/nxcals_fq/RQF.A12_U_RES.csv")["signal"].values)
        query.assert_called_once_with(
            "spark", "CMW", 1544569200000000000, 86400000000000, expected, False, 31536000000000000
        )

    def test_metadata_nxcals_feature_query_rqda12_rqfa12_u_res(self):
        with patch(
            "lhcsmapi.api.query._query_nxcals_by_variables", return_value=pd.DataFrame(columns=["one"])
        ) as query:
            QueryBuilder().with_nxcals("spark").with_duration(
                t_start="2018-12-12 00:00:00+01:00", t_end="2018-12-13 00:00:00+01:00"
            ).with_circuit_type("RQ").with_metadata(
                circuit_name=["RQD.A12", "RQF.A12"], system="BUSBAR", signal="U_RES", wildcard={"BUSBAR": "*"}
            ).signal_query()

        expected = list(read_csv("query_builder/nxcals_fq/RQD.A12_RQF.A12_U_RES.csv")["signal"].values)
        query.assert_called_once_with(
            "spark", "CMW", 1544569200000000000, 86400000000000, expected, False, 31536000000000000
        )

    def test_metadata_nxcals_feature_query_rb_u_res(self):
        with patch(
            "lhcsmapi.api.query._query_nxcals_by_variables", return_value=pd.DataFrame(columns=["one"])
        ) as query:
            QueryBuilder().with_nxcals("spark").with_duration(
                t_start="2018-12-12 00:00:00+01:00", t_end="2018-12-13 00:00:00+01:00"
            ).with_circuit_type("RB").with_metadata(
                circuit_name="*", system="BUSBAR", signal="U_RES", wildcard={"BUSBAR": "*"}
            ).signal_query()

        expected = list(read_csv("query_builder/nxcals_fq/RB_U_RES.csv")["signal"].values)
        query.assert_called_once_with(
            "spark", "CMW", 1544569200000000000, 86400000000000, expected, False, 31536000000000000
        )

    def test_metadata_nxcals_feature_query_rb_u_hds(self):
        with patch(
            "lhcsmapi.api.query._query_nxcals_by_variables", return_value=pd.DataFrame(columns=["one"])
        ) as query:
            QueryBuilder().with_nxcals("spark").with_duration(
                t_start=1634542889913000000, t_end=1634542949913000000
            ).with_circuit_type("RB").with_metadata(
                circuit_name="RB.A23", system="QH", signal="U_HDS", wildcard={"MAGNET": "A28L3"}
            ).signal_query()

        expected = list(read_csv("query_builder/nxcals_fq/RB_U_HDS.csv")["signal"].values)
        query.assert_called_once_with(
            "spark", "CMW", 1634542889913000000, 60000000000, expected, False, 31536000000000000
        )

    def test_metadata_nxcals_feature_query_rb_u_diode_rb(self):
        with patch(
            "lhcsmapi.api.query._query_nxcals_by_variables", return_value=pd.DataFrame(columns=["one"])
        ) as query:
            QueryBuilder().with_nxcals("spark").with_duration(
                t_start=1634542889913000000, t_end=1634542949913000000
            ).with_circuit_type("RB").with_metadata(
                circuit_name="RB.A23", system="DIODE_RB", signal="U_DIODE_RB", wildcard={"MAGNET": "A28L3"}
            ).signal_query()

        expected = list(read_csv("query_builder/nxcals_fq/RB_U_DIODE.csv")["signal"].values)
        query.assert_called_once_with(
            "spark", "CMW", 1634542889913000000, 60000000000, expected, False, 31536000000000000
        )

    def test_query_parameters_nxcals_feature_query_rqd_u_res(self):
        with patch(
            "lhcsmapi.api.query._query_nxcals_by_variables", return_value=pd.DataFrame(columns=["one"])
        ) as query:
            QueryBuilder().with_nxcals("spark").with_duration(
                t_start="2017-04-24 19:29:45.679", t_end="2017-04-24 19:51:14.809"
            ).with_query_parameters(
                nxcals_system="WINCCOA", signal=["DCQFD.7L2.R:U_RES", "DCQFB.C21R1.R:U_RES", "DCQFQ.12L2.L:U_RES"]
            ).signal_query()

        expected = list(read_csv("query_builder/nxcals_fq/RQ_U_RES.csv")["signal"].values)
        query.assert_called_once_with(
            "spark", "WINCCOA", 1493054985679000000, 1289130000000, expected, False, 31536000000000000
        )
