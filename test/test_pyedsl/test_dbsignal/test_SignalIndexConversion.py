import unittest

from lhcsmapi.pyedsl.dbsignal.SignalIndexConversion import SignalIndexConversion


class TestSignalIndexConversion(unittest.TestCase):
    def test_convert_indices(self):
        # arrange

        # act
        # assert
        with self.assertRaises(TypeError) as context:
            SignalIndexConversion.convert_indices([], SignalIndexConversion.convert_indices_to_unix_timestamp)

        self.assertTrue(
            "Cannot convert the indices of input <class 'list'>.\n"
            "The data structure must be a pandas DataFrame or Series." in context.exception.args[0]
        )
