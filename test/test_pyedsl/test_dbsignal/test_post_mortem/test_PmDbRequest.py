import unittest
import warnings
from pathlib import Path
import os
import json
from unittest import mock
import pandas as pd

from lhcsmapi.pyedsl.dbsignal.post_mortem.PmDbRequest import PmDbRequest


def mocked_pmdbrequest_requests_get(*args, **kwargs):
    path = Path(os.path.dirname(__file__))

    class MockedRequest(object):
        def __init__(self, text, status_code):
            self.text = text
            self.status_code = status_code

    if args and "BLM" in args[0]:
        json_path = os.path.join(
            path.parent.parent.parent,
            "resources/dbsignal/post_mortem/" "1448260132000000000_2000000000_BLM_BLMLHC_HC.BLM.SR6.C.json",
        )
    elif args and "BSRA" in args[0]:
        json_path = os.path.join(
            path.parent.parent.parent,
            "resources/dbsignal/post_mortem/" "1448260132000000000_5000000000_LBDS_BSRA_LHC.BSRA.US45.B1.json",
        )
    elif args and "LHC" in args[0]:
        json_path = os.path.join(
            path.parent.parent.parent,
            "resources/dbsignal/post_mortem/" "1448260132000000000_2000000000_LHC_CISX_CISX.CCR.LHC.A.json",
        )
    elif args and "QPS" in args[0]:
        json_path = os.path.join(
            path.parent.parent.parent,
            "resources/dbsignal/post_mortem/" "1426201200000000000_86400000000000_QPS_DQAMCNMB_PMHSU.json",
        )
    else:
        return MockedRequest("", 204)

    with open(json_path) as json_file:
        return MockedRequest(json.dumps(json.load(json_file)), 200)


class TestPmDbRequest(unittest.TestCase):
    def setUp(self):
        self.SYSTEM = "QPS"
        self.CLASS_NAME = "DQAMCNMB_PMHSU"
        self.SOURCE = "B20L5"
        self.SIGNAL_NAME = "I_HDS_4"
        self.TIMESTAMP = 1426220469491000000
        self.DURATION = 86400000000000

    def tearDown(self):
        pass

    # region generateQuery tests
    def test_generateSignalRequest_Qps_Parameters(self):
        query_expected = "http://pm-rest.cern.ch/v2/pmdata/signal?system=QPS&className=DQAMCNMB_PMHSU&source=B20L5&timestampInNanos=1426220469491000000&signal=I_HDS_4"
        query_actual = PmDbRequest.generate_request(
            "pmdata/signal",
            False,
            system=self.SYSTEM,
            className=self.CLASS_NAME,
            source=self.SOURCE,
            timestampInNanos=self.TIMESTAMP,
            signal=self.SIGNAL_NAME,
        )
        self.assertEqual(len(query_expected), len(query_actual))

    def test_generateEventRequest_Qps_Parameters_Non_Zero_Duration_Header_True(self):
        query_expected = "http://pm-rest.cern.ch/v2/pmevent/header/within/duration?system=QPS&className=DQAMCNMB_PMHSU&source=B20L5&fromTimestampInNanos=1426220469491000000&durationInNanos=86400000000000"
        query_actual = PmDbRequest.generate_request(
            "pmevent",
            True,
            system=self.SYSTEM,
            className=self.CLASS_NAME,
            source=self.SOURCE,
            fromTimestampInNanos=self.TIMESTAMP,
            durationInNanos=self.DURATION,
        )
        self.assertEqual(len(query_expected), len(query_actual))

    def test_generateEventRequest_Qps_Parameters_Non_Zero_Duration_Header_False(self):
        query_expected = "http://pm-rest.cern.ch/v2/pmevent/within/duration?system=QPS&className=DQAMCNMB_PMHSU&source=B20L5&fromTimestampInNanos=1426220469491000000&durationInNanos=86400000000000"
        query_actual = PmDbRequest.generate_request(
            "pmevent",
            False,
            system=self.SYSTEM,
            className=self.CLASS_NAME,
            source=self.SOURCE,
            fromTimestampInNanos=self.TIMESTAMP,
            durationInNanos=self.DURATION,
        )
        self.assertEqual(len(query_expected), len(query_actual))

    def test_generateEventRequest_Qps_Parameters_Zero_Duration_Header_True(self):
        query_expected = "http://pm-rest.cern.ch/v2/pmevent/header?system=QPS&className=DQAMCNMB_PMHSU&source=B20L5&timestampInNanos=1426220469491000000"
        query_actual = PmDbRequest.generate_request(
            "pmevent",
            True,
            system=self.SYSTEM,
            className=self.CLASS_NAME,
            source=self.SOURCE,
            timestampInNanos=self.TIMESTAMP,
        )
        self.assertEqual(len(query_expected), len(query_actual))

    def test_generateEventRequest_Qps_Parameters_Zero_Duration_Header_False(self):
        """Tests that the default CalsSignal is created if no arguments are passed to the createSignalCALS method"""
        query_expected = "http://pm-rest.cern.ch/v2/pmevent?system=QPS&className=DQAMCNMB_PMHSU&source=B20L5&timestampInNanos=1426220469491000000"
        query_actual = PmDbRequest.generate_request(
            "pmevent",
            False,
            system=self.SYSTEM,
            className=self.CLASS_NAME,
            source=self.SOURCE,
            timestampInNanos=self.TIMESTAMP,
        )
        self.assertEqual(len(query_expected), len(query_actual))

    def test_generateDataRequest_Qps_Parameters_Non_Zero_Duration_Header_True(self):
        query_expected = "http://pm-rest.cern.ch/v2/pmdata/header/within/duration?system=QPS&className=DQAMCNMB_PMHSU&source=B20L5&fromTimestampInNanos=1426220469491000000&durationInNanos=86400000000000"
        query_actual = PmDbRequest.generate_request(
            "pmdata",
            True,
            system=self.SYSTEM,
            className=self.CLASS_NAME,
            source=self.SOURCE,
            fromTimestampInNanos=self.TIMESTAMP,
            durationInNanos=self.DURATION,
        )
        self.assertEqual(len(query_expected), len(query_actual))

    def test_generateDataRequest_Qps_Parameters_Non_Zero_Duration_Header_False(self):
        query_expected = "http://pm-rest.cern.ch/v2/pmdata/within/duration?system=QPS&className=DQAMCNMB_PMHSU&source=B20L5&fromTimestampInNanos=1426220469491000000&durationInNanos=86400000000000"
        query_actual = PmDbRequest.generate_request(
            "pmdata",
            False,
            system=self.SYSTEM,
            className=self.CLASS_NAME,
            source=self.SOURCE,
            fromTimestampInNanos=self.TIMESTAMP,
            durationInNanos=self.DURATION,
        )
        self.assertEqual(len(query_expected), len(query_actual))

    def test_generateDataRequest_Qps_Parameters_Zero_Duration_Header_True(self):
        query_expected = "http://pm-rest.cern.ch/v2/pmdata/header?system=QPS&className=DQAMCNMB_PMHSU&source=B20L5&timestampInNanos=1426220469491000000"
        query_actual = PmDbRequest.generate_request(
            "pmdata",
            True,
            system=self.SYSTEM,
            className=self.CLASS_NAME,
            source=self.SOURCE,
            timestampInNanos=self.TIMESTAMP,
        )
        self.assertEqual(len(query_expected), len(query_actual))

    def test_generateDataRequest_Qps_Parameters_Zero_Duration_Header_False(self):
        query_expected = "http://pm-rest.cern.ch/v2/pmdata?system=QPS&className=DQAMCNMB_PMHSU&source=B20L5&timestampInNanos=1426220469491000000"
        query_actual = PmDbRequest.generate_request(
            "pmdata",
            False,
            system=self.SYSTEM,
            className=self.CLASS_NAME,
            source=self.SOURCE,
            timestampInNanos=self.TIMESTAMP,
        )
        self.assertEqual(len(query_expected), len(query_actual))

    def test_generateDataRequest_Qps_Parameters_Zero_Duration_Header_False_error_missing_keyword(self):
        # act
        with self.assertRaises(ValueError) as context:
            PmDbRequest.generate_request(
                "pmdata",
                False,
                system=self.SYSTEM,
                className=self.CLASS_NAME,
                source=self.SOURCE,
                timestamp=self.TIMESTAMP,
            )

        # assert
        self.assertTrue(
            "Some keywords: {'timestamp'} not in dict_keys(['system', 'className', 'source', "
            "'signal', 'fromTimestampInNanos', 'timestampInNanos', 'durationInNanos'])!" in context.exception.args[0]
        )

    def test_generateDataRequest_Qps_Parameters_Zero_Duration_Header_False_error_missing_timestamp(self):
        # act
        with self.assertRaises(ValueError) as context:
            PmDbRequest.generate_request(
                "pmdata",
                False,
                system=self.SYSTEM,
                className=self.CLASS_NAME,
                source=self.SOURCE,
                timestampInNanos=self.TIMESTAMP,
                durationInNanos=self.TIMESTAMP,
            )

        # assert
        self.assertTrue(
            "If timestampInNanos is present, then both fromTimestampInNanos and durationInNanos "
            "have to be absent and vice versa!" in context.exception.args[0]
        )

    def test_generateDataRequest_Qps_Parameters_Zero_Duration_Header_False_error_request_type(self):
        # act
        with self.assertRaises(ValueError) as context:
            PmDbRequest.generate_request(
                "data",
                False,
                system=self.SYSTEM,
                className=self.CLASS_NAME,
                source=self.SOURCE,
                timestampInNanos=self.TIMESTAMP,
            )

        # assert
        self.assertTrue("PM request type data not supported." in context.exception.args[0])

    @mock.patch("requests.get", side_effect=mocked_pmdbrequest_requests_get)
    def test_query_context_blm(self, mock_get):
        # arrange

        # act
        context_df_act = PmDbRequest.query_context(
            t_start=1, t_end=2, system="BLM", className="BLMLHC", source="*", parameters=["pmFillNum"]
        )

        # assert
        context_df_ref = pd.DataFrame({"pmFillNum": {1448260133517488525: 4647}})
        self.assertEqual(len(mock_get.call_args_list), 1)

        pd.testing.assert_frame_equal(context_df_ref, context_df_act)

    @mock.patch("requests.get", side_effect=mocked_pmdbrequest_requests_get)
    def test_query_context_lhc(self, mock_get):
        # arrange

        # act
        context_df_act = PmDbRequest.query_context(
            t_start=1,
            t_end=2,
            system="LHC",
            className="CISX",
            source="*",
            parameters=["OVERALL_ENERGY", "OVERALL_INTENSITY_1", "OVERALL_INTENSITY_2"],
        )

        # assert
        context_df_ref = pd.DataFrame(
            {
                "OVERALL_ENERGY": {1448260133517488525: 20915},
                "OVERALL_INTENSITY_1": {1448260133517488525: 16537},
                "OVERALL_INTENSITY_2": {1448260133517488525: 17110},
            }
        )
        self.assertEqual(len(mock_get.call_args_list), 1)

        pd.testing.assert_frame_equal(context_df_ref, context_df_act)

    @mock.patch("requests.get", side_effect=mocked_pmdbrequest_requests_get)
    def test_query_context_lbds(self, mock_get):
        # arrange

        # act
        context_df_act = PmDbRequest.query_context(
            t_start=1,
            t_end=2,
            system="LBDS",
            className="BSRA",
            source="LHC.BSRA.US45.B1",
            parameters=["aGXpocTotalIntensity", "aGXpocTotalMaxIntensity"],
        )

        # assert
        context_df_ref = pd.DataFrame(
            {
                "aGXpocTotalIntensity": {1448260134624467000: 24571929033.591232},
                "aGXpocTotalMaxIntensity": {1448260134624467000: 856089258.5021862},
            }
        )
        self.assertEqual(len(mock_get.call_args_list), 1)

        pd.testing.assert_frame_equal(context_df_ref, context_df_act)

    @mock.patch("requests.get", side_effect=mocked_pmdbrequest_requests_get)
    def test_query_context_error(self, mock_get):
        # arrange

        # act
        with warnings.catch_warnings(record=True) as w:
            PmDbRequest.query_context(t_start=1, t_end=2, system="", className="", source="", parameters=[])

        # assert
        self.assertEqual(
            "Post Mortem returned no data for the following query: "
            "http://pm-rest.cern.ch/v2/pmdata/within/duration?system=&className=&source=&fromTimestampInNanos=1"
            "&durationInNanos=1",
            str(w[0].message),
        )

    @mock.patch("requests.get", side_effect=mocked_pmdbrequest_requests_get)
    def test_find_events(self, mock_get):
        # arrange
        source = "*"
        system = "QPS"
        className = "DQAMCNMB_PMHSU"
        selectedDayTimeStamp = 1426201200000000000
        eventDurationSeconds = 24 * 60 * 60

        # act
        source_timestamp_act = PmDbRequest.find_events(
            source, system, className, t_start=selectedDayTimeStamp, duration=[(eventDurationSeconds, "s")]
        )

        # assert
        source_timestamp_ref = [
            ("B20L5", 1426220469491000000),
            ("C20L5", 1426220517100000000),
            ("A20L5", 1426220518112000000),
            ("A21L5", 1426220625990000000),
            ("B21L5", 1426220866112000000),
            ("C23L4", 1426236802332000000),
            ("B23L4", 1426236839404000000),
            ("A23L4", 1426236839832000000),
            ("C22L4", 1426236949841000000),
            ("C15R4", 1426251285711000000),
            ("B15R4", 1426251337747000000),
            ("A15R4", 1426251388741000000),
            ("B34L8", 1426258716281000000),
            ("C34L8", 1426258747672000000),
            ("A34L8", 1426258747370000000),
            ("C33L8", 1426258835955000000),
            ("C34R7", 1426258853947000000),
            ("A34R7", 1426258854113000000),
            ("A20R3", 1426267931956000000),
            ("B20R3", 1426267983579000000),
            ("C20R3", 1426268004144000000),
            ("B18L5", 1426277626360000000),
            ("A18L5", 1426277679838000000),
            ("C18L5", 1426277680496000000),
            ("A19L5", 1426277903449000000),
        ]
        self.assertListEqual(source_timestamp_ref, source_timestamp_act)

    def test_get_response(self):
        # arrange
        source = "*"
        system = "FGC"
        className = "lhc_self_pmd"
        selectedDayTimeStamp = 1617081252900000000
        eventDurationNanos = 1000000000

        # act
        path = Path(os.path.dirname(__file__))
        json_path = os.path.join(
            path.parent.parent.parent,
            "resources/dbsignal/post_mortem/" "1617081252900000000_1000000000_FGC_LHC_SELF_PMD.json",
        )
        with open(json_path, "r") as reference:
            expected_response = json.load(reference)

        actual_response = PmDbRequest.get_response(
            "pmdata",
            False,
            source=source,
            system=system,
            className=className,
            fromTimestampInNanos=selectedDayTimeStamp,
            durationInNanos=eventDurationNanos,
        )

        # assert
        self.assertEqual(expected_response, actual_response)

    # endregion
