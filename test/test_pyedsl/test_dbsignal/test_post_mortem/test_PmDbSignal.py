import json
import os
from pathlib import Path


def mocked_pmdbsignal_requests_get(*args, **kwargs):
    path = Path(os.path.dirname(__file__))

    class MockedRequest(object):
        def __init__(self, text, status_code=200):
            self.text = text
            self.status_code = status_code

    if args and "STATUS.I_MEAS" in args[0]:
        json_path = os.path.join(
            path.parent.parent.parent,
            "resources/dbsignal/post_mortem/" "1426220469520000000_FGC_51_self_pmd_RPTE.UA47.RB.A45_STATUS.I_MEAS.json",
        )
    elif args and "IEARTH.IEARTH" in args[0]:
        json_path = os.path.join(
            path.parent.parent.parent,
            "resources/dbsignal/post_mortem/" "1426220469520000000_FGC_51_self_pmd_RPTE.UA47.RB.A45_IEARTH.IEARTH.json",
        )
    elif args and "IEARTH.IEARTH_ERROR" in args[0]:
        json_path = os.path.join(
            path.parent.parent.parent,
            "resources/dbsignal/post_mortem/"
            "1426220469520000000_FGC_51_self_pmd_RPTE.UA47.RB.A45_IEARTH.IEARTH_ERROR.json",
        )
    elif args and "I_HDS_4" in args[0]:
        json_path = os.path.join(
            path.parent.parent.parent,
            "resources/dbsignal/post_mortem/" "1426220469491000000_QPS_DQAMCNMB_PMHSU_B20L5_I_HDS_4.json",
        )
    else:
        return MockedRequest("", 204)
    with open(json_path) as json_file:
        return MockedRequest(json.dumps(json.load(json_file)))
