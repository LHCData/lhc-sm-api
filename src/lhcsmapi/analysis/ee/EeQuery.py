from typing import Union, List, Tuple

import pandas as pd
from pyspark.sql.session import SparkSession

from lhcsmapi.analysis.CircuitQuery import CircuitQuery, execution_count
from lhcsmapi.analysis.decorators import check_nan_timestamp, check_nan_timestamp_signals
from lhcsmapi.api.query_builder import QueryBuilder


class EeQuery(CircuitQuery):
    """Base class with methods for EE query"""

    @execution_count()
    @check_nan_timestamp(return_type=pd.DataFrame())
    def find_source_timestamp_ee(
        self,
        t_start: Union[str, int, float],
        duration: List[Tuple[int, str]] = [(10, "s"), (1000, "s")],
        system: str = "EE",
    ) -> pd.DataFrame:
        """Method searching PM energy extraction events
        If either the start or end time is NaN, then an empty dataframe is returned.

        :param t_start: start time of a query
        :param duration: search duration before and after the start time
        :param system: name of an EE system ('EE' by default, however for RB it takes either 'EE_ODD' or 'EE_EVEN')
        return: pd.DataFrame with source and timestamp of the PM EE events
        """
        return (
            QueryBuilder()
            .with_pm()
            .with_duration(t_start=t_start, duration=duration)
            .with_circuit_type(self.circuit_type)
            .with_metadata(circuit_name=self.circuit_name, system=system)
            .event_query(verbose=self.verbose)
            .get_dataframe()
        )

    @execution_count()
    @check_nan_timestamp_signals(return_type=pd.DataFrame())
    def query_ee_u_dump_res_pm(
        self, timestamp_ee: int, timestamp_sync: int, system: str = "EE", *, signal_names: List[str]
    ) -> Union[List[pd.DataFrame], pd.DataFrame]:
        """Method querying PM for energy extraction voltage signals
        If the timestamp is NaN, then a list of empty dataframes of the size of the signal names is returned.
        The signals are synchronized in time and converted from ns to s.

        :param timestamp_ee: EE PM event timestamp (ns precision)
        :param timestamp_sync: timestamp to which the queried signals are synchronized (ns precision)
        :param system: name of EE system to query
        :param signal_names: list of signal names to query according to metadata description
        :return: list of pd.DataFrames (if a signal was not queried, an empty pd.DataFrame is returned and warning
            containing the raw query is returned)
        """
        return (
            QueryBuilder()
            .with_pm()
            .with_timestamp(timestamp_ee)
            .with_circuit_type(self.circuit_type)
            .with_metadata(circuit_name=self.circuit_name, system=system, signal=signal_names)
            .signal_query(verbose=self.verbose)
            .remove_initial_offset(timestamp_ee)
            .synchronize_time(timestamp_sync)
            .convert_index_to_sec()
            .get_dataframes()
        )

    @execution_count()
    @check_nan_timestamp_signals(return_type=pd.DataFrame())
    def query_ee_nxcals(
        self,
        timestamp_fgc: Union[int, str, float],
        system: str = "EE",
        *,
        signal_names: List[str],
        spark: SparkSession,
        duration: List[Tuple[int, str]] = [(3 * 3600, "s")],
        t_thr: int = 1000,
    ) -> Union[List[pd.DataFrame], pd.DataFrame]:
        """Method querying NxCALS for energy extraction signals
        If the timestamp is NaN, then a list of empty dataframes of the size of the signal names is returned.
        The signals are synchronized in time and converted from ns to s.

        :param timestamp_fgc: FGC PM event timestamp (ns precision) to which the query is synchronised
        :param system: name of EE system
        :param signal_names: list of signal names to query according to metadata description
        :param spark: NXCALS  connector
        :param duration: search duration before and after the start time
        :param t_thr: threshold of time for the query after which signals are taken for the output
        :return: list of pd.DataFrames (if a signal was not queried, an empty pd.DataFrame is returned and warning
            containing the raw query is returned)
        """
        return (
            QueryBuilder()
            .with_nxcals(spark)
            .with_duration(t_start=timestamp_fgc, duration=duration)
            .with_circuit_type(self.circuit_type)
            .with_metadata(circuit_name=self.circuit_name, system=system, signal=signal_names)
            .signal_query(verbose=self.verbose)
            .synchronize_time(timestamp_fgc)
            .convert_index_to_sec()
            .remove_values_for_time_less_than_or_equal_to(t_thr)
            .get_dataframes()
        )


class Ee13kAQuery(EeQuery):
    """Class with methods for 13 kA EE query"""

    @execution_count()
    @check_nan_timestamp_signals(return_type=pd.DataFrame())
    def query_ee_t_res_pm(
        self, timestamp_ee: int, timestamp_sync: int, system: str = "EE", *, signal_names: List[str]
    ) -> Union[List[pd.DataFrame], pd.DataFrame]:
        """Method querying PM for energy extraction temperature signals
        If the timestamp is NaN, then a list of empty dataframes of the size of the signal names is returned.
        The signals are synchronized in time and converted from ns to s.

        :param timestamp_ee: EE PM event timestamp (ns precision)
        :param timestamp_sync: timestamp to which the queried signals are synchronized (ns precision)
        :param system: name of EE system to query
        :param signal_names: list of signal names to query according to metadata description
        :return: list of pd.DataFrames (if a signal was not queried, an empty pd.DataFrame is returned and warning
            containing the raw query is returned)
        """
        return (
            QueryBuilder()
            .with_pm()
            .with_timestamp(timestamp_ee)
            .with_circuit_type(self.circuit_type)
            .with_metadata(circuit_name=self.circuit_name, system=system, signal=signal_names)
            .signal_query(verbose=self.verbose)
            .remove_values_for_time_less_than_or_equal_to(timestamp_ee)
            .synchronize_time(timestamp_sync)
            .convert_index_to_sec()
            .get_dataframes()
        )
