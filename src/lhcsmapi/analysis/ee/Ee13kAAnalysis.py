import warnings
from typing import List, Tuple

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from lhcsmapi.Time import Time
from lhcsmapi.analysis.CircuitAnalysis import CircuitAnalysis, append
from lhcsmapi.analysis.decorators import check_dataframe_empty
from lhcsmapi.pyedsl.AssertionBuilder import AssertionBuilder
from lhcsmapi.pyedsl.PlotBuilder import PlotBuilder, create_title
from lhcsmapi.analysis import comparison
import lhcsmapi.signal_analysis.features as signal_analysis
import lhcsmapi.signal_analysis.functions as signal_analysis_functions


class Ee13kAAnalysis(CircuitAnalysis):
    """Class with methods for 13 kA PC analysis in RB and RQ"""

    @staticmethod
    @check_dataframe_empty(mode="any", warning="At least one DataFrame is empty, max U_DUMP_RES analysis skipped!")
    def compare_max_u_res_dump_to_reference(
        u_dump_res_df: pd.DataFrame, u_dump_res_ref_df: pd.DataFrame, signal: str
    ) -> None:
        """Method comparing maximum EE voltage to reference and displays a text (if within 10 % margin) or a warning
        (otherwise)

        :param u_dump_res_df: actual EE voltage
        :param u_dump_res_ref_df: reference EE voltage
        :param signal: name of the EE signal
        :return: None
        """
        max_voltage = u_dump_res_df.max()[0]
        min_voltage_ref = u_dump_res_ref_df.max()[0] * 0.9
        max_voltage_ref = u_dump_res_ref_df.max()[0] * 1.1
        if min_voltage_ref < max_voltage < max_voltage_ref:
            print(
                "Maximum %s (%.1f V) is within of the 10%% reference voltage [%.1f, %.1f] V."
                % (signal, max_voltage, min_voltage_ref, max_voltage_ref)
            )
        else:
            warnings.warn(
                "Maximum %s (%.1f V) is outside of the 10%% reference voltage [%.1f, %.1f] V."
                % (signal, max_voltage, min_voltage_ref, max_voltage_ref)
            )

    @check_dataframe_empty(mode="any", warning="At least one DataFrame is empty, tau_U_DUMP_RES analysis skipped!")
    def analyze_char_time_u_dump_res_ee(
        self,
        circuit_name: str,
        timestamp_fgc: int,
        u_dump_res_dfs: List[pd.DataFrame],
        i_meas_df: pd.DataFrame,
        duration_decay=(1, 120),
    ) -> None:
        """Method analyzing the characteristic time of an EE voltage decay. In addition, it plots EE voltage with main
        power converter current.

        :param circuit_name: circuit name (for title)
        :param timestamp_fgc: FGC PM timestamp (for title)
        :param u_dump_res_dfs: list of EE voltages
        :param i_meas_df: main power converter current
        :param duration_decay: a tuple with start and end time for time constant calculation
        :return: None
        """
        # Suffixes
        suffixes = ("_odd", "_even") if self.circuit_type == "RB" else ("",)

        # Vectorize Input
        u_dump_res_dfs = u_dump_res_dfs if isinstance(u_dump_res_dfs, list) else [u_dump_res_dfs]

        subtracted_min = signal_analysis.subtract_min_value(u_dump_res_dfs)
        selected_duration = signal_analysis.take_window(subtracted_min, *duration_decay)

        tau_u_dump_res = signal_analysis.calculate_features(
            selected_duration, signal_analysis_functions.tau_energy
        ).values[0]

        # # resistance
        def calculate_ee_resistance(u_dump_res_df, i_meas_df):
            index_i_meas_u_dump_res_max = i_meas_df.index.get_indexer(
                [u_dump_res_df.idxmax().values[0]], method="nearest"
            )[0]
            i_meas_u_dump_res_max = i_meas_df.iloc[index_i_meas_u_dump_res_max].values[0]
            return u_dump_res_df.max().values[0] / i_meas_u_dump_res_max

        r_dump_res = [calculate_ee_resistance(u_dump_res_df, i_meas_df) for u_dump_res_df in u_dump_res_dfs]

        # Plot
        title = "{} {} EE: I_MEAS(t) and U_DUMP_RES(t)".format(Time.to_string_short(timestamp_fgc), circuit_name)

        PlotBuilder().with_signal(i_meas_df, title=title, grid=True).with_ylabel(ylabel="I_MEAS, [A]").with_signal(
            u_dump_res_dfs, color=["C2", "C3"]
        ).with_ylabel(ylabel="U_DUMP_RES, [V]").with_xlim((-50, np.nan)).plot()

        # Update results table
        # Compare features to the reference
        ee_act_df = pd.DataFrame(index=["act"])
        append(ee_act_df, "tau_u_dump_res", tau_u_dump_res, suffixes=suffixes)
        append(ee_act_df, "R", r_dump_res, suffixes=suffixes)

        res_tau_u_dump_res_comp_df = comparison.compare_features_to_reference(ee_act_df, self.circuit_type, "EE")
        # Display analysis results
        print("Analysis results")
        print("----------------")
        comparison.check_feature_comparison(res_tau_u_dump_res_comp_df)

    @check_dataframe_empty(mode="any", warning="At least one DataFrame is empty, t_delay_U_DUMP_RES analysis skipped!")
    def analyze_delay_time_u_dump_res_ee(
        self,
        circuit_name: str,
        timestamp_fgc: int,
        timestamp_pic: int,
        timestamp_ees: List[int],
        i_a_df: pd.DataFrame,
        i_ref_df: pd.DataFrame,
        u_dump_res_dfs: List[pd.DataFrame],
    ) -> None:
        """Method analyzing delay time of EE triggers based on timestamps. In addition it plots EE voltages and main
        power converter current along with indications of the trigger delays.

        :param circuit_name: circuit name
        :param timestamp_fgc: FGC PM timestamp
        :param timestamp_pic: PIC NXCALS timestamp
        :param timestamp_ees: list of EE PM timestamps
        :param i_a_df: actual power converter current (non-filtered, high sampling rate)
        :param i_ref_df: reference power converter current
        :param u_dump_res_dfs: list of EE voltages
        :return: None
        """
        # Suffixes
        suffixes = ("_odd", "_even") if self.circuit_type == "RB" else ("",)

        # Vectorize Input
        u_dump_res_dfs = u_dump_res_dfs if isinstance(u_dump_res_dfs, list) else [u_dump_res_dfs]
        timestamp_ees = timestamp_ees if isinstance(timestamp_ees, list) else [timestamp_ees]

        # Calculate Features
        t_delay_ees = [(timestamp_ee - timestamp_pic) * 1e-9 for timestamp_ee in timestamp_ees]

        ee_act_df = pd.DataFrame(index=["act"])
        append(ee_act_df, "t_delay_ee", t_delay_ees, suffixes=suffixes)

        # Plot
        title = "{} {} EE: I_A(t) and U_DUMP_RES(t)".format(Time.to_string_short(timestamp_fgc), circuit_name)
        if all(i_a_df.index < 1):
            ax1_ylim_min = int(i_a_df.values[-1])
            ax1_xlim_min = i_a_df.index[0]
        else:
            ax1_ylim_min = int(i_a_df.reindex([1], method="nearest", tolerance=0.1).values[0])
            ax1_xlim_min = -0.2

        if all(i_a_df.index < 1):
            ax1_ylim_max = i_a_df.values[0]
            ax1_xlim_max = i_a_df.index[-1]
        else:
            ax1_ylim_max = int(i_a_df.reindex([-0.2], method="nearest", tolerance=0.1).values[0]) + 5
            ax1_xlim_max = 1

        ax2 = (
            PlotBuilder()
            .with_signal([i_a_df, i_ref_df], title=title, grid=True, marker=[None, "o"])
            .with_ylabel(ylabel="I_A, [A]")
            .with_xlim((ax1_xlim_min, ax1_xlim_max))
            .with_ylim((ax1_ylim_min, ax1_ylim_max))
            .with_signal(u_dump_res_dfs, color=["C2", "C3"])
            .with_ylabel(ylabel="U_DUMP_RES, [V]")
            .with_xlim((ax1_xlim_min, ax1_xlim_max))
            .with_axvline(x=(timestamp_pic - timestamp_fgc) * 1e-9, color="green", linestyle="--", linewidth=1.5)
            .plot(show_plot=False)
            .get_axes()[1]
        )

        for index, timestamp_ee in enumerate(timestamp_ees):
            ax2.annotate(
                "",
                xy=((timestamp_ee - timestamp_fgc) * 1e-9, (0.2 - index * 0.1) * ax2.get_ylim()[1]),
                xytext=((timestamp_pic - timestamp_fgc) * 1e-9, (0.2 - index * 0.1) * ax2.get_ylim()[1]),
                arrowprops=dict(arrowstyle="<->"),
            )
        plt.show()

        # Compare features to the reference
        t_delay_ee_comp_df = comparison.compare_features_to_reference(
            ee_act_df, self.circuit_type, "EE", timestamp=timestamp_fgc
        )

        print("Analysis results")
        print("----------------")
        comparison.check_feature_comparison(t_delay_ee_comp_df)

    def plot_ee_temp(
        self, circuit_name: str, timestamp_ee: int, t_res_act_dfs: List[pd.DataFrame], t_res_ref_dfs: List[pd.DataFrame]
    ) -> None:
        """Method plotting EE temperature for an actual and reference case

        :param circuit_name: circuit name (for title)
        :param timestamp_ee: EE PM timestamp (for title)
        :param t_res_act_dfs: list of actual EE temperatures
        :param t_res_ref_dfs: list of reference EE temperatures
        :return: None
        """
        title = create_title(circuit_name, timestamp_ee, "T_RES_BODY")
        style = [".-"] * len(t_res_act_dfs) + ["--"] * len(t_res_ref_dfs)
        color = ["C%d" % (i % len(t_res_act_dfs)) for i in range(len(t_res_act_dfs) + len(t_res_ref_dfs))]
        PlotBuilder().with_signal(
            t_res_act_dfs + t_res_ref_dfs, title=title, grid=True, style=style, color=tuple(color)
        ).with_ylabel(ylabel="T_RES_BODY, [degC]").plot()

    def analyze_ee_temp(
        self,
        circuit_name: str,
        timestamp_ee: int,
        t_res_act_dfs: List[pd.DataFrame],
        t_res_ref_dfs: List[pd.DataFrame],
        *,
        abs_margin=25,
        scaling=1,
    ) -> None:
        """Method analyzing EE temperature by comparing it to the reference with a given scaling and margin. Prior to
        performing the comparison, both lists of temperatures (actual and reference) are compensated to the room
        temperature.

        :param circuit_name: circuit name
        :param timestamp_ee: EE PM timestamp
        :param t_res_act_dfs: list of actual EE temperatures
        :param t_res_ref_dfs: list of reference EE temperatures
        :param abs_margin: absolute margin for the EE temperature comparison
        :param scaling: scaling coefficient for the reference signals
        :return: None
        """
        t_res_act_dfs = [t_res_act_df - t_res_act_dfs[0].values[0] + 20 for t_res_act_df in t_res_act_dfs]
        t_res_ref_dfs = [t_res_ref_df - t_res_ref_dfs[0].values[0] * scaling + 20 for t_res_ref_df in t_res_ref_dfs]
        title = create_title(circuit_name, timestamp_ee, "T_RES_BODY [compensated to 20 degC]")
        AssertionBuilder().with_signal(t_res_act_dfs).compare_to_reference(
            signal_ref_dfs=t_res_ref_dfs, abs_margin=abs_margin
        ).show_plot(ylabel="T_RES_BODY, [degC]", title=title)

    def analyze_ee_temp_and_cooling(
        self,
        t_res_body_long_dfs: List[pd.DataFrame],
        st_res_overtemp_long_dfs: List[pd.DataFrame],
        *,
        t_greater: float,
        value_max: float,
    ) -> None:
        """Method analyzing EE temperature and cooling at a given time after an FPA in 13 kA circuit (typically done
        for RB).

        :param t_res_body_long_dfs: list of temperatures of EE resistors
        :param st_res_overtemp_long_dfs: flag indicating over-heating of EE resistors
        :param t_greater: threshold time above which temperature has to fall below the maximum value
        :param value_max: maximum allowed temperature value
        :return: None
        """
        AssertionBuilder().with_signal(t_res_body_long_dfs).for_time_greater_than(
            t_greater=t_greater
        ).has_min_max_value(value_max=value_max).show_plot(
            y_label="T_RES_BODY, [degC]",
            data_sec=st_res_overtemp_long_dfs,
            drawstyle_sec="steps-post",
            ylabel_sec="ST_RES_OVERTEMP, [-]",
        )

    @staticmethod
    def check_pic_ee_timestamp_difference(timestamp_pic: int, timestamp_ee: int, value_range_in_seconds: Tuple[float]):
        """
        Checks if the time difference between PIC FGC and EE are within the given range_value.
        Args:
            timestamp_pic: the pic timestamp in nanoseconds.
            timestamp_ee: the ee timestamp in nanoseconds.
            value_range_in_seconds: A tuple of `float` such as (min_value, max_value) expressed in seconds.

        Returns: True if the time difference between PIC FGC and EE are within the given range_value. False Otherwise.
        Raises: TypeError if the given `value_range` tuple has not 2 elements.
        """
        if isinstance(value_range_in_seconds, tuple) and len(value_range_in_seconds) != 2:
            raise TypeError(
                f"The attribute `value_range` should be a python tuple such as `(min_value, max_value) in seconds`."
                f"{value_range_in_seconds} is not correct"
            )
        return value_range_in_seconds[0] < abs(1e-9 * timestamp_pic - 1e-9 * timestamp_ee) < value_range_in_seconds[1]
