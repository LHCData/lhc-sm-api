import pyspark.sql.functions as F
import pandas as pd
from lhcsmapi.Time import Time
from datetime import datetime


def path_exists(path: str, sc) -> bool:
    """Method checking if a path exist at the hadoop drive

    :param path: a path to a file to the hadoop drive
    :param sc: spark connector to execute the check
    :return: True if a path exists, False otherwise
    """
    fs = sc._jvm.org.apache.hadoop.fs.FileSystem.get(sc._jsc.hadoopConfiguration())
    return fs.exists(sc._jvm.org.apache.hadoop.fs.Path(path))


def get_checkpoint_timestamp(path: str, spark, sc) -> int:
    """Method querying the last checkpoint

    :param path: a path to a file to the hadoop drive
    :param spark: spark variable
    :param sc: spark connector to execute the check
    :return: value of the last timestamp, False otherwise
    """
    if not path_exists(path, sc):
        return False

    df = spark.read.csv(path=path, header=True)
    return int(df.select(F.max(F.col("checkpoint_timestamp")).alias("LATEST")).limit(1).collect()[0].LATEST)


def is_checkpoint_existing(path: str, t_checkpoint: int, spark, sc) -> bool:
    """Method checking whether a checkpoint already exists

    :param path: a path to a file to the hadoop drive
    :param t_checkpoint: checkpoint to verify
    :param spark: spark variable
    :param sc: spark connector to execute the check
    :return: True if checkpoint already exists, otherwise False
    """
    output = False
    if path_exists(path, sc):
        df = spark.read.csv(path=path, header=True)
        checkpoint_exists = (
            df.select("checkpoint_timestamp").where(F.col("checkpoint_timestamp") == t_checkpoint).count() != 0
        )
        if checkpoint_exists:
            print("WARNING: Checkpoint at %d already exists, skipping" % t_checkpoint)
            output = True

    return output


def save_row_with_checkpoint(pandas_row: pd.DataFrame, path: str, t_checkpoint: int, spark, sc) -> bool:
    """Method saving an input row with a checkpoint

    :param pandas_row: pandas row
    :param path: a path to a file to the hadoop drive
    :param t_checkpoint: checkpoint to verify
    :param spark: spark variable
    :param sc: spark connector to execute the check
    :return: False if a checkpoint already exists, otherwise True
    """
    if is_checkpoint_existing(path, t_checkpoint, spark, sc):
        return False

    df = spark.createDataFrame(pandas_row)
    df = df.withColumn("checkpoint_creation", F.lit(Time.to_unix_timestamp(datetime.now())))
    df = df.withColumn("checkpoint_timestamp", F.lit(t_checkpoint))

    df.write.csv(path=path, mode="append", header=True)

    return True
