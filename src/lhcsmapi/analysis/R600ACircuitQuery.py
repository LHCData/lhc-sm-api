from importlib.metadata import version

from typing import List

import pandas as pd

import lhcsmapi
from lhcsmapi.Time import Time
from lhcsmapi.analysis.CircuitQuery import CircuitQuery, min_nan
from lhcsmapi.analysis.dfb.DfbQuery import DfbQuery
from lhcsmapi.analysis.ee.EeQuery import EeQuery
from lhcsmapi.analysis.pc.PcQuery import PcQuery
from lhcsmapi.analysis.pic.PicQuery import PicQuery
from lhcsmapi.analysis.qds.QdsQuery import QdsQuery
from lhcsmapi import reference


class R600ACircuitQuery(PicQuery, PcQuery, EeQuery, QdsQuery, DfbQuery, CircuitQuery):
    """Class extending the CircuitQuery class with methods for creation of templates of analysis tables
    in 600A circuits (with/without EE, RCBXH/V, RCD/O).

    """

    def create_report_analysis_template(self, timestamp_fgc: int, author: str = "") -> pd.DataFrame:
        """Method creating a report analysis template for 600A circuits with/without EE

        :param timestamp_fgc: timestamp of an FGC event
        :param author: NICE account name of the analysis author
        :return: pre-filled pd.DataFrame with a template to be filled further with data from an analysis of
            a powering event in 600A circuits with/without EE
        """
        columns = reference.get_quench_database_columns(circuit_type="600A", table_type="MP3")

        results_table = pd.DataFrame(columns=columns, index=[0])
        results_table["Circuit Name"] = self.circuit_name
        results_table["Circuit Family"] = self.circuit_name.split(".")[0]
        results_table["Period"] = reference.get_mp3_period(timestamp_fgc)
        results_table["Date (FGC)"] = Time.to_string_short(timestamp_fgc).split(" ")[0]
        results_table["Time (FGC)"] = Time.to_string_short(timestamp_fgc).split(" ")[1]
        results_table["Analysis performed by"] = author
        results_table["lhcsmapi version"] = version("lhcsmapi")

        return results_table

    def create_report_analysis_template_rcd(
        self, timestamp_fgc_rcd: int, timestamp_fgc_rco: int, circuit_names: List[str], author: str = ""
    ) -> pd.DataFrame:
        """Method creating a report analysis template for RCD/RCO 600A circuits

        :param timestamp_fgc_rcd: timestamp of an FGC event in RCD circuit
        :param timestamp_fgc_rco: timestamp of an FGC event in RCO circuit
        :param circuit_names: list of RCD/RCO circuit names [RCD.xxx, RCO.xxx]
        :param author: NICE account name of the analysis author
        :return: pre-filled pd.DataFrame with a template to be filled further with data from analysis of
            a powering event in RCD/RCO 600A circuits
        """
        columns = reference.get_quench_database_columns(circuit_type="600A_RCDO", table_type="MP3")

        results_table = pd.DataFrame(columns=columns, index=[0])
        # Assumes that circuit_names[0] is always RCD
        results_table["Circuit Name"] = circuit_names[0].replace("RCD", "RCD-RCO")
        results_table["Circuit Family"] = "RCD"

        timestamp_fgc = min_nan(timestamp_fgc_rcd, timestamp_fgc_rco)
        results_table["Period"] = reference.get_mp3_period(timestamp_fgc)
        results_table["Date (FGC)"] = Time.to_string_short(timestamp_fgc).split(" ")[0]
        results_table["Time (FGC)"] = Time.to_string_short(timestamp_fgc).split(" ")[1]
        results_table["Analysis performed by"] = author
        results_table["lhcsmapi version"] = version("lhcsmapi")

        return results_table

    def create_report_analysis_template_rcbx(
        self, timestamp_fgc_rcbxh: int, timestamp_fgc_rcbxv: int, circuit_names: List[str], author: str = ""
    ) -> pd.DataFrame:
        """Method creating a report analysis template for RCBXH/RCBXV 600A circuits

        :param timestamp_fgc_rcbxh: timestamp of an FGC event in RCBXH circuit
        :param timestamp_fgc_rcbxv: timestamp of an FGC event in RCBXV circuit
        :param circuit_names: list of RCBXH/RCBXV circuit names [RCBXH.xxx, RCBXV.xxx]
        :param author: NICE account name of the analysis author
        :return: pre-filled pd.DataFrame with a template to be filled further with data from analysis
            of a powering event in RCBXH/RCBXV 600A circuits
        """
        columns = reference.get_quench_database_columns(circuit_type="600A_RCBXHV", table_type="MP3")

        results_table = pd.DataFrame(columns=columns, index=[0])
        results_table["Circuit Name"] = circuit_names[0].replace("H", "").replace("V", "")
        results_table["Circuit Family"] = "RCBX"

        timestamp_fgc = min_nan(timestamp_fgc_rcbxh, timestamp_fgc_rcbxv)
        results_table["Period"] = reference.get_mp3_period(timestamp_fgc)
        results_table["Date (FGC)"] = Time.to_string_short(timestamp_fgc).split(" ")[0]
        results_table["Time (FGC)"] = Time.to_string_short(timestamp_fgc).split(" ")[1]
        results_table["Analysis performed by"] = author
        results_table["lhcsmapi version"] = version("lhcsmapi")

        return results_table
