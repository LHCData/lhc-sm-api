from typing import List

import pandas as pd
import numpy as np

from lhcsmapi.Time import Time
from lhcsmapi.pyedsl.dbsignal.post_mortem.PmDbRequest import PmDbRequest
from lhcsmapi.api.query_builder import QueryBuilder


def get_lhc_context(start_time_date, duration=[(1, "s"), (2, "s")]) -> pd.DataFrame:
    """Function querying and returning an LHC context with:
    - fill number
    - overall beam energy
    - overall intensity in beam 1
    - overall intensity in beam 2
    - beam mode
    - beam position monitors

    :param start_time_date: start time and date for LHC context query
    :param duration: tuple with query duration (first element for time before the start time and date) and (second
    element for time after the start time and date)
    :return: DataFrame with LHC context for a given start time and date
    """
    t_start, t_end = Time.get_query_period_in_unix_time(start_time_date=start_time_date, duration_date=duration)

    # Fill number
    pm_fill_num_df = PmDbRequest.query_context(
        t_start=t_start,
        t_end=t_start + int(2e9),
        system="BLM",
        className="BLMLHC",
        source="HC.BLM.SR6.C",
        parameters=["pmFillNum"],
    )

    # Beam energy and intensity
    lhc_parameters_df = PmDbRequest.query_context(
        t_start=t_start,
        t_end=t_start + int(2e9),
        system="LHC",
        className="CISX",
        source="CISX.CCR.LHC.A",
        parameters=["OVERALL_ENERGY", "OVERALL_INTENSITY_1", "OVERALL_INTENSITY_2"],
    )
    lhc_parameters_df["OVERALL_ENERGY"] *= 1.2  # magic number

    # Beam Mode

    lhc_bm_df = PmDbRequest.query_context(
        t_start=t_start,
        t_end=t_start + int(2e9),
        system="LHC",
        className="CISX",
        source="CISX.CCR.LHC.GA",
        parameters=["BEAM_MODE"],
    )

    # Abort gap
    abort_gap_b1_df = PmDbRequest.query_context(
        t_start=t_start,
        t_end=t_start + int(5e9),
        system="LBDS",
        className="BSRA",
        source="LHC.BSRA.US45.B1",
        parameters=["aGXpocTotalIntensity", "aGXpocTotalMaxIntensity"],
    )
    abort_gap_b1_df.columns = ["%sB1" % col for col in abort_gap_b1_df.columns]

    abort_gap_b2_df = PmDbRequest.query_context(
        t_start=t_start,
        t_end=t_start + int(5e9),
        system="LBDS",
        className="BSRA",
        source="LHC.BSRA.US45.B2",
        parameters=["aGXpocTotalIntensity", "aGXpocTotalMaxIntensity"],
    )
    abort_gap_b2_df.columns = ["%sB2" % col for col in abort_gap_b2_df.columns]

    # Summary
    lhc_data_df = pd.concat([pm_fill_num_df, lhc_parameters_df, lhc_bm_df], axis=1)
    lhc_data_df["timestamp_blm"] = lhc_data_df.index
    lhc_data_df.index = pm_fill_num_df["pmFillNum"]
    lhc_data_df.drop(columns=["pmFillNum"], inplace=True)

    if (abort_gap_b1_df.index == 0) and (abort_gap_b2_df.index == 0):
        abort_gap_df = pd.DataFrame()
    elif abort_gap_b1_df.index == 0:
        abort_gap_df = abort_gap_b2_df
        abort_gap_df.index = pm_fill_num_df["pmFillNum"]
    elif abort_gap_b2_df.index == 0:
        abort_gap_df = abort_gap_b1_df
        abort_gap_df.index = pm_fill_num_df["pmFillNum"]
    else:
        abort_gap_b1_df["timestamp_abort_gap_b1"] = abort_gap_b1_df.index
        abort_gap_b2_df["timestamp_abort_gap_b2"] = abort_gap_b2_df.index
        abort_gap_b1_df.index = pm_fill_num_df["pmFillNum"]
        abort_gap_b2_df.index = pm_fill_num_df["pmFillNum"]
        abort_gap_df = pd.concat([abort_gap_b1_df, abort_gap_b2_df], axis=1)

    return pd.concat([lhc_data_df, abort_gap_df], axis=1)


def get_unique_blm_timestamps(source_timestamp_df: pd.DataFrame) -> pd.DataFrame:
    """Method returning unique blm timestamps (difference below 100 s between the BLM timestamps)

    :param source_timestamp_df: PM source and timestamp for BLM
    :return: a DataFrame with unique BLM timestamps
    """
    if source_timestamp_df.empty:
        return pd.DataFrame()

    diff = source_timestamp_df.drop_duplicates("timestamp")["timestamp"].diff()
    # replace NaN with inf
    diff = diff.fillna(np.inf)
    # only take rows for which the the difference is more than 100 s
    diff = diff[diff > 100e9]
    # then one row corresponds to a timestamp for which the BLMs have timestamps +/- 1 s
    timestamps = source_timestamp_df.take(diff.index)

    return timestamps


def _calculate_multicolumn_features(df, features, prefix: str):
    features = features if isinstance(features, list) else [features]
    df_out = df.agg(features).T
    df_out.columns = ["%s_%s" % (prefix, col) for col in df_out.columns]

    return df_out


def _convert_table_df_into_row(df: pd.DataFrame, index: int) -> pd.DataFrame:
    """Function converting a table into a single row. The indices are joined with columns by an underscore

    :param df: input tabular DataFrame
    :param index: index for output row DataFrame
    :return: single row dataframe with an index
    """
    df_out = pd.DataFrame(df.stack())
    df_out.index = df_out.index.map("_".join)
    df_out = df_out.T
    df_out.index = [index]

    return df_out


def calculate_blm_feature_row(
    source_timestamp_df: pd.DataFrame, features: List[str], context_index: int, signal="pmTurnLoss"
) -> pd.DataFrame:
    """Function calculating a BLM feature row

    :param source_timestamp_df: a DataFrame with PM source and timestamp for BLM
    :param features: list of features to calculate
    :param context_index: index for the row with the context
    :param signal: signal for which features are calculated
    :return: a DataFrame with a row containing the requested features for the given signal
    """
    blm_turn_loss_features_row_dfs = []
    for index, row in source_timestamp_df.iterrows():
        print("\tAnalysing BLM source %s - %d/%d" % (row["source"], index, len(source_timestamp_df)))

        blm_turn_loss_df = (
            QueryBuilder()
            .with_pm()
            .with_duration(t_start=row["timestamp"], duration=[(2, "s")])
            .with_query_parameters(system="BLM", className="BLMLHC", source=row["source"], signal=signal)
            .signal_query()
            .overwrite_sampling_time(t_sampling=4e-05, t_end=1)
            .dfs[0]
        )

        blm_turn_loss_features_row_df = _convert_table_df_into_row(
            _calculate_multicolumn_features(blm_turn_loss_df, features, "turn_loss"), context_index
        )

        blm_turn_loss_features_row_dfs.append(blm_turn_loss_features_row_df)

    # concatenate list into a single row
    feature_row = pd.concat(blm_turn_loss_features_row_dfs, axis=1)

    # sort columns alphabetically
    feature_row = feature_row.reindex(sorted(feature_row.columns), axis=1)

    return feature_row
