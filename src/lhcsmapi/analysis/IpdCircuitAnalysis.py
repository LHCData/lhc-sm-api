import warnings
from typing import Dict

import numpy as np
import pandas as pd

from lhcsmapi.Time import Time
from lhcsmapi.analysis.CircuitAnalysis import CircuitAnalysis
from lhcsmapi.analysis.busbar.BusbarResistanceAnalysis import SingleBusbarResistanceAnalysis
from lhcsmapi.analysis.dfb.DfbAnalysis import DfbAnalysis
from lhcsmapi.analysis.pc.PcAnalysis import PcAnalysis
from lhcsmapi.analysis.qds.QdsAnalysis import QdsAnalysis
from lhcsmapi.analysis.qh.QuenchHeaterVoltageAnalysis import QuenchHeaterVoltageAnalysis
from lhcsmapi import reference


class IpdCircuitAnalysis(
    DfbAnalysis, PcAnalysis, QdsAnalysis, QuenchHeaterVoltageAnalysis, SingleBusbarResistanceAnalysis, CircuitAnalysis
):
    """Class for analysis of IPD (Individually Powered Dipole) circuits"""

    # Timestamp table
    def create_timestamp_table(
        self,
        timestamp_dct: Dict[str, int],
        circuit_name: str = "",
        is_qps_exp: bool = True,
        qps_fgc_max_delay: float = 0.04,
    ) -> None:
        """Method creating timestamp table as well as checking the timing between considered timestamps.

        :param timestamp_dct: a dictionary from timestamp name to its value (it is assumed that does not contain NaN)
        :param circuit_name: name of a circuit
        :param is_qps_exp: flag checking whether a QPS is expected or not
        :param qps_fgc_max_delay: maximum time delay between FGC and QPS timestamps in s
        """
        super().create_timestamp_table(timestamp_dct)

        if is_qps_exp and np.isnan(timestamp_dct.get("QDS_A", np.nan)) and np.isnan(timestamp_dct.get("QDS_B", np.nan)):
            warnings.warn("Neither QDS board A nor B are present in the PM buffer.", stacklevel=2)
        elif is_qps_exp and np.isnan(timestamp_dct.get("QDS_A", np.nan)):
            warnings.warn("QDS Board A not present in PM buffer!", stacklevel=2)
        elif is_qps_exp and np.isnan(timestamp_dct.get("QDS_B", np.nan)):
            warnings.warn("QDS Board B not present in PM buffer!", stacklevel=2)
        else:
            if is_qps_exp and (timestamp_dct["QDS_B"] - timestamp_dct["QDS_A"] != int(1e6)):
                warnings.warn("QDS PM events (board A and B) are not synchronized to 1 ms!")

        if ("QDS_A" in timestamp_dct) and ("FGC" in timestamp_dct):
            if abs(timestamp_dct["QDS_A"] - timestamp_dct["FGC"]) > int(1e9 * qps_fgc_max_delay):
                warnings.warn(
                    "QDS_A and FGC PM events are not synchronized to +/- %d ms!" % int(1e3 * qps_fgc_max_delay),
                    stacklevel=2,
                )
        else:
            warnings.warn(
                "Either QDS_A or FGC PM event is missing. " "Can not check synchronization of QDS and FGC timestamps!"
            )

    def create_mp3_results_table(self):
        """Method creating an MP3-compatible quench database row for an IPD circuit

        :return: pd.DataFrame with the results of analysis calculation
        """
        columns_mp3 = reference.get_quench_database_columns(circuit_type="IPD", table_type="MP3")
        mp3_table = pd.DataFrame(columns=columns_mp3, index=self.results_table.index)

        mp3_table[columns_mp3] = self.results_table[columns_mp3]

        # Fill non-matching column names
        mp3_table["Timestamp_PIC"] = self.results_table.apply(
            lambda col: Time.to_string_short(col["timestamp_pic"]), axis=1
        )
        mp3_table["Delta_t(FGC-PIC)"] = self.results_table.apply(
            lambda col: 1e-6 * (col["timestamp_fgc"] - col["timestamp_pic"]), axis=1
        )
        mp3_table["Delta_t(QPS-PIC)"] = self.results_table.apply(
            lambda col: 1e-6 * (col["timestamp_qds_a"] - col["timestamp_pic"]), axis=1
        )

        return mp3_table
