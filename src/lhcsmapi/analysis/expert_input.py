from typing import List


def get_expert_comment(system: str, is_automatic: bool = False, automatic_analysis_comment: str = "") -> str:
    """Function getting expert comment as typed input. It has several modes of operation depending on the input
    arguments:
    - if the analysis is automatic (is_automatic is True), then the expert comment is skipped
      - if the comment from automatic analysis is empty, then the system comment is set to default value
      - otherwise, the automatic system comment is returned
    - otherwise it takes expert input (by default it is set to NOT_OK, expecting a further justification)

    At the end, the function prints and returns the system comment.

    :param system: name of a system to get expert comment on
    :param is_automatic: flag indicating whether the analysis is automatic or not. If True expert comment is based
        on the automatic analysis (if done). If in the automatic mode (is_automatic=True), always return OK to avoid
        breaking the analysis pipeline
    :param automatic_analysis_comment: comment from automatic analysis (if done)
    :return: system comment
    """
    if is_automatic:
        if automatic_analysis_comment == "":
            system_comment = "Automatic analysis, no expert feedback."
        else:
            system_comment = automatic_analysis_comment
    else:
        system_check = input("Is {} OK? [NOT_OK]:".format(system))
        system_check = system_check.upper() or "NOT_OK"
        if system_check != "OK":
            system_check = "NOT_OK"
            system_comment = input("Please insert some comments: ")
            system_comment = system_comment or system_check
        else:
            system_comment = "OK"

    print("OUTPUT")
    print("------")
    print("{} Analysis comment is: {}".format(system, system_comment))
    print("------")
    return system_comment


def check_show_next(index: int, max_index: int, is_automatic: bool = False) -> False:
    """Function checking whether to stop the next iteration of a loop based on the current index and the
    maximum number of iterations. In addition, if the analysis is done in the automatic mode the next iteration is not
    stopped (False is returned), otherwise user decides.

    :param index: current index
    :param max_index: maximum index
    :param is_automatic: flag indicating whether the analysis is automatic or not. If True expert decides whether or not
        the next iteration should stopped. If in the automatic mode (is_automatic=True), always return False to avoid
        breaking the analysis pipeline
    :return: bool if
    """
    #
    if is_automatic:
        return False
    # if the list has been exhausted
    if index == max_index:
        return True
    else:
        show_next = input("Show next? [Y or N]:")
        show_next = show_next.upper() or "Y"
        if show_next == "N":
            return True
        else:
            return False


def get_expert_decision(user_prompt: str, answers: List[str]) -> str:
    """Function getting expert decision out of a predefined list. The function is executed until the user makes a
    selection out of the predefined list of answers.

    :param user_prompt: a user prompt indicating what choice is to be made (e.g., Quench origin)
    :param answers: a list of predefined answers from which a user decides on the output (e.g., magnet, busbar, other)

    :return: string with the selected answer
    """
    answers_upper = [answer.upper() for answer in answers]
    expert_decision = input("{} {} (entered by user):".format(user_prompt, answers))
    while expert_decision.upper() not in answers_upper:
        expert_decision = input("{} {} (entered by user):".format(user_prompt, answers))

    # find the index of matching the input
    index_decision = answers_upper.index(expert_decision.upper())
    return answers[index_decision]
