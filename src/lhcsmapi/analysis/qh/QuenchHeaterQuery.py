from typing import List, Union, Tuple

import pandas as pd
from tqdm.notebook import tqdm

from lhcsmapi.analysis.CircuitQuery import CircuitQuery, execution_count
from lhcsmapi.analysis.decorators import check_nan_timestamp
from lhcsmapi.api.query_builder import QueryBuilder
from lhcsmapi import reference


class QuenchHeaterQuery(CircuitQuery):
    """Class for query of quench heater events and signals (both voltage and current). Works for actual and reference."""

    @execution_count()
    @check_nan_timestamp(return_type=pd.DataFrame())
    def find_source_timestamp_qh_with_pattern(self, pattern, start_date_time, end_date_time):
        return (
            QueryBuilder()
            .with_pm()
            .with_duration(t_start=start_date_time, t_end=end_date_time)
            .with_circuit_type(self.circuit_type)
            .with_metadata(circuit_name=self.circuit_name, system="QH", source=pattern)
            .event_query()
            .filter_source(circuit_type=self.circuit_type, circuit_name=self.circuit_name, system="QH")
            .sort_values(by="timestamp")
            .drop_duplicates(column=["source", "timestamp"])
            .get_dataframe()
        )

    @execution_count()
    @check_nan_timestamp(return_type=pd.DataFrame())
    def find_source_timestamp_qh(
        self, t_start: Union[int, str, float], duration: List[Tuple[int, str]] = [(10, "s"), (400, "s")]
    ) -> pd.DataFrame:
        """Method searching PM QH events
        If the start is NaN, then an empty dataframe is returned.

        :param t_start: start time of a search
        :param duration: search duration before and after the start time
        :return: pd.DataFrame with source and timestamp of the PM QDS events
        """
        return (
            QueryBuilder()
            .with_pm()
            .with_duration(t_start=t_start, duration=duration)
            .with_circuit_type(self.circuit_type)
            .with_metadata(circuit_name=self.circuit_name, system="QH", source="*")
            .event_query(verbose=self.verbose)
            .filter_source(self.circuit_type, self.circuit_name, "QH")
            .sort_values(by="timestamp")
            .drop_duplicate_source()
            .get_dataframe()
        )

    @execution_count()
    def query_qh_pm(
        self, source_timestamp_qh_df: pd.DataFrame, *, signal_names: List[str], is_ref: bool = False
    ) -> List[List[pd.DataFrame]]:
        """Method querying PM for QH signals (either voltage or current or both)
        Signals are synchronized to the QH PM event and index is converted to seconds.

        :param source_timestamp_qh_df: dataframe with PM source and timestamp for QH events
        :param signal_names: list of signal names to query according to metadata description
        :param is_ref: boolean flag to determine whether a query of reference or actual discharge is necessary
        :return: List of list of QH signals. The outer list has as many elements as QH events and the length
            of the inner is equal to the number of QH signals.
        """
        hds_dfs = []
        for index, row in tqdm(
            source_timestamp_qh_df.iterrows(), total=source_timestamp_qh_df.shape[0], desc="Querying PM"
        ):
            source_qds = row["source"]
            if is_ref:
                timestamp_qh = reference.get_quench_heater_reference_discharge(
                    self.circuit_type, source_qds, row["timestamp"]
                )
            else:
                timestamp_qh = row["timestamp"]

            hds_df = (
                QueryBuilder()
                .with_pm()
                .with_timestamp(timestamp_qh)
                .with_circuit_type(self.circuit_type)
                .with_metadata(
                    circuit_name=self.circuit_name,
                    system="QH",
                    signal=signal_names,
                    source=source_qds,
                    wildcard={"CELL": source_qds},
                )
                .signal_query()
                .synchronize_time(timestamp_qh)
                .convert_index_to_sec()
                .drop_first_n_points(5)
                .drop_last_n_points(5)
                .get_dataframes()
            )

            hds_dfs.append(hds_df)

        return hds_dfs
