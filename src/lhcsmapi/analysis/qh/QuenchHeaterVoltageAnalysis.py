from typing import List

import numpy as np
import pandas as pd
from IPython.display import display_html

from lhcsmapi.Time import Time
from lhcsmapi.analysis.CircuitAnalysis import CircuitAnalysis
from lhcsmapi.analysis.decorators import check_dataframe_empty
from lhcsmapi.analysis.expert_input import check_show_next
from lhcsmapi.pyedsl.PlotBuilder import PlotBuilder
from lhcsmapi import reference
from lhcsmapi.analysis import comparison
import lhcsmapi.signal_analysis.features as signal_analysis
import lhcsmapi.signal_analysis.functions as signal_analysis_functions
import lhcsmapi.analysis.features_helper as utility_features


class QuenchHeaterVoltageAnalysis(CircuitAnalysis):
    """Base class for quench heater analysis in circuits for which only the voltage is stored in PM buffers:
    - RQ (multiple QH discharges)
    - IT (single QH discharge)
    - IPQ (single QH discharge)
    - IPD (single QH discharge)
    """

    def __init__(self, circuit_type, results_table=None, is_automatic=True, colormap="default"):
        super().__init__(circuit_type, results_table, is_automatic, colormap)

    def analyze_multi_qh_voltage_with_ref(
        self,
        source_timestamp_qh_df: pd.DataFrame,
        u_hds_dfss: List[List[pd.DataFrame]],
        u_hds_ref_dfss: List[List[pd.DataFrame]],
        wildcard_key: str = "CELL",
        nominal_voltage: int = 900,
    ) -> None:
        """Method analyzing QH voltage signals in RQ circuit. It is plotting all signals as obtained from
        PM. For the voltage decay part, it is calculating:
        - initial voltage
        - final voltage (as the mean value of the last 20 points)
        - time constant with the charge approach
        and comparing to reference values.

        :param source_timestamp_qh_df: dataframe with PM source and timestamp for QH events
        :param u_hds_dfss: list of lists of QH voltage signals. There are as many outer lists as quenched
            magnets and as many elements in the inner list as there are QH voltage signals to analyze
        :param u_hds_ref_dfss: list of lists of reference QH voltage signals. There are as many outer lists as
            quenched magnets and as many elements in the inner list as there are reference QH voltage signals to analyze
        :param wildcard_key: wildcard key for retrieval of reference QH features
        :param nominal_voltage: nominal QH voltage (300, 900); 900 by default
        """

        index_max = source_timestamp_qh_df.index[-1]
        for index, row in source_timestamp_qh_df.iterrows():
            source_qh, timestamp_qh = row[["source", "timestamp"]]
            timestamp_qh = Time.to_unix_timestamp(timestamp_qh)

            u_hds_dfs = u_hds_dfss[index]
            u_hds_ref_dfs = u_hds_ref_dfss[index]

            self.analyze_single_qh_voltage_with_ref(
                source_qh,
                timestamp_qh,
                u_hds_dfs,
                u_hds_ref_dfs,
                wildcard_key=wildcard_key,
                results_table_index=index,
                nominal_voltage=nominal_voltage,
            )

            # Show next
            if check_show_next(index, index_max, self.is_automatic):
                break

    @check_dataframe_empty(
        mode="all", warning="All discharge curves are empty, please check PM data, analysis skipped!"
    )
    def analyze_single_qh_voltage_with_ref(
        self,
        source_qh: str,
        timestamp_qh: int,
        u_hds_dfs: List[pd.DataFrame],
        u_hds_ref_dfs: List[pd.DataFrame],
        wildcard_key: str = "CIRCUIT",
        results_table_index: int = 0,
        nominal_voltage: int = 900,
    ) -> bool:
        """Method analyzing QH voltage signals in IT, IPD, IPQ circuits. It is plotting all signals as obtained from
        PM. For the voltage decay part, it is calculating:
        - initial voltage
        - final voltage (as the mean value of the last 20 points)
        - time constant with the charge approach
        and comparing to reference values.

        :param source_qh: QH PM source
        :param timestamp_qh: unix timestamp of the currently analyzed QH discharge
        :param u_hds_dfs: list of QH voltage signals.
        :param u_hds_ref_dfs: list of reference QH voltage signals.
        :param wildcard_key: wildcard key for retrieval of reference QH features
        :param nominal_voltage: nominal QH voltage (300, 900); 900 by default
        """

        # Plot signals
        timestamp_ref = reference.get_quench_heater_reference_discharge(self.circuit_type, source_qh, timestamp_qh)
        self.plot_voltage_with_ref(source_qh, timestamp_qh, timestamp_ref, u_hds_dfs, u_hds_ref_dfs)

        # Take decay only
        u_hds_decay_dfs = self.extract_voltage_decay(u_hds_dfs)
        u_hds_ref_decay_dfs = self.extract_voltage_decay(u_hds_ref_dfs)

        # Calculate features
        # - initial and final voltage
        is_qh_ok = self.calculate_and_display_feature_tables(
            source_qh, timestamp_qh, timestamp_ref, u_hds_decay_dfs, u_hds_ref_decay_dfs, wildcard_key, nominal_voltage
        )
        # Update results table
        final_label = "Pass" if is_qh_ok else "Fail"
        self.update_results_table("QH analysis", results_table_index, final_label)

        print("The QH discharges are labeled: %s." % final_label)
        self.analysis_result.add_qh_analysis(timestamp_qh, source_qh, timestamp_ref, is_qh_ok)
        return is_qh_ok

    def calculate_and_display_feature_tables(
        self,
        source_qh: str,
        timestamp_qh: int,
        timestamp_ref: int,
        u_hds_decay_dfs: List[pd.DataFrame],
        u_hds_ref_decay_dfs: List[pd.DataFrame],
        wildcard_key: str,
        nominal_voltage: int,
    ) -> bool:
        """Method calculating QH discharge features for a voltage decay:
        - initial and final voltage
        - characteristic decay time

        :param source_qh: QH PM source
        :param timestamp_qh: QH PM timestamp
        :param timestamp_ref: QH PM reference timestamp
        :param u_hds_decay_dfs: list of decay QH voltages
        :param u_hds_ref_decay_dfs: list of decay QH reference voltages
        :param wildcard_key: wildcard key for reference (either CIRCUIT - IPD, IPQ, IT or CELL - RQ)
        :param nominal_voltage: nominal voltage
        :return: True if QH features within reference, otherwise False
        """
        first_last_u_df = self.calculate_first_and_last_voltage(timestamp_qh, u_hds_decay_dfs)
        first_last_u_comp_df = self.compare_first_last_voltage_with_ref(
            self.circuit_type, source_qh, first_last_u_df, wildcard_key, nominal_voltage
        )
        # - characteristic decay time
        tau_u_df = self.calculate_char_time(timestamp_qh, u_hds_decay_dfs)
        tau_u_ref_df = self.calculate_char_time(timestamp_ref, u_hds_ref_decay_dfs)
        tau_u_comp_df = self.compare_char_time_with_ref(
            self.circuit_type, source_qh, tau_u_df, tau_u_ref_df, wildcard_key, nominal_voltage
        )

        # Display comparison tables
        self.display_comparison_tables(first_last_u_comp_df, tau_u_comp_df)

        # QH analysis result
        is_first_last_u_ok = first_last_u_comp_df["result"].all()
        is_tau_correct = tau_u_comp_df["result"].all()
        is_qh_ok = is_first_last_u_ok and is_tau_correct
        return is_qh_ok

    @staticmethod
    def extract_voltage_decay(u_hds_dfs: List[pd.DataFrame]) -> List[pd.DataFrame]:
        """Method extracting voltage decay

        :param u_hds_dfs: list of QH PM voltages
        :return: list of QH PM decay voltages
        """
        u_hds_decay_dfs = []
        for u_hds_df in u_hds_dfs:
            max_value = u_hds_df.max().values[0]
            min_value = u_hds_df.min().values[0]
            min_index = u_hds_df.idxmin().values[0]
            if max_value > min_value:
                u_hds_decay_dfs.append(u_hds_df[(u_hds_df.index >= 0) & (u_hds_df.index < min_index)])
            else:
                u_hds_decay_dfs.append(u_hds_df[(u_hds_df.index >= 0)])

        return u_hds_decay_dfs

    @staticmethod
    def calculate_first_and_last_voltage(timestamp_qh: int, u_hds_dfs: List[pd.DataFrame]) -> pd.DataFrame:
        """Method calculating initial and final voltage. Output features are rounded to 0 decimal digit.

        :param timestamp_qh: QH PM timestamp
        :param u_hds_dfs: list of QH decay voltages
        :return: Pandas DataFrame with index set to timestamp_qh and columns given by the names of voltages and
        selected features. E.g, assuming u_hds_dfs = [U_HDS_1, U_HDS_2]
        index     | U_HDS_1_first | U_HDS_1_last_20_mean | U_HDS_2_first | U_HDS_2_last_20_mean |
        timestamp |               |                      |               |                      |
        """

        first_last_u_df = signal_analysis.calculate_features(
            u_hds_dfs, [utility_features.first, utility_features.last20mean], timestamp_qh
        )
        first_last_u_df = first_last_u_df.round()
        return first_last_u_df

    @staticmethod
    def compare_first_last_voltage_with_ref(
        circuit_type, source_qh: str, first_last_u_df: pd.DataFrame, wildcard_key: str, nominal_voltage=900
    ) -> pd.DataFrame:
        """Method comparing initial and final voltage with reference

        :param circuit_type: circuit type.
        :param source_qh: QH PM source
        :param first_last_u_df: pd.DataFrame with initial and final voltage
        :param wildcard_key: wildcard key for reference (either CIRCUIT - IPD, IPQ, IT or CELL - RQ)
        :param nominal_voltage: nominal voltage (either 300 or 900 V)
        :return: pd.DataFrame with comparison of initial and final voltage to reference
        """
        return comparison.compare_features_to_reference(
            first_last_u_df,
            circuit_type,
            "QH",
            wildcard={wildcard_key: source_qh},
            nominal_voltage=nominal_voltage,
            precision=1,
        )

    @staticmethod
    def calculate_char_time(timestamp_qh, u_hds_dfs: List[pd.DataFrame]) -> pd.DataFrame:
        """Method calculating characteristic time of voltage pseudo-exponential decay. Calculated features are rounded
        to three decimal digits.

        :param timestamp_qh: QH PM timestamp
        :param u_hds_dfs: list of QH decay voltages
        :return: Pandas DataFrame with index set to timestamp_qh and columns given by the names of voltages and
        selected features. E.g, assuming u_hds_dfs = [U_HDS_1, U_HDS_2]
        index     | U_HDS_1_tau_charge | U_HDS_1_tau_charge |
        timestamp |                    |                    |
        """
        tau_df = signal_analysis.calculate_features(u_hds_dfs, signal_analysis_functions.tau_charge, timestamp_qh)
        tau_df = tau_df.round(3)
        return tau_df

    @staticmethod
    def compare_char_time_with_ref(
        circuit_type, source_qh, tau_df: pd.DataFrame, tau_ref_df: pd.DataFrame, wildcard_key, nominal_voltage=900
    ) -> pd.DataFrame:
        """Method comparing characteristic decay time with reference

        :param circuit_type: circuit type.
        :param source_qh: QH PM source
        :param tau_df: pd.DataFrame with voltage characteristic time
        :param tau_ref_df: DataFrame with reference voltage characteristic time
        :param wildcard_key: wildcard key for reference (either CIRCUIT - IPD, IPQ, IT or CELL - RQ)
        :param nominal_voltage: nominal voltage (either 300 or 900 V)
        :return: pd.DataFrame with comparison of characteristic decay time of voltage for actual and reference
        """

        return comparison.compare_difference_of_features_to_reference(
            tau_df,
            tau_ref_df,
            circuit_type,
            "QH",
            wildcard={wildcard_key: source_qh},
            nominal_voltage=nominal_voltage,
            precision=3,
        )

    @staticmethod
    def plot_voltage_with_ref(
        source_qh: str,
        timestamp_qh: int,
        timestamp_ref: int,
        u_hds_dfs: List[pd.DataFrame],
        u_hds_ref_dfs: List[pd.DataFrame],
    ) -> None:
        """Method plotting voltage for actual and reference timestamp

        :param source_qh: QH PM source
        :param timestamp_qh: QH PM timestamp
        :param timestamp_qh: reference QH PM timestamp
        :param u_hds_dfs: QH voltages of a magnet
        :param u_hds_ref_dfs: reference QH voltages of a magnet
        :param timestamp_ref: reference timestamp
        :return: None
        """
        title = "Magnet: {}, Time Stamp: {}, Reference {}, U_HDS(t)".format(
            source_qh, Time.to_string_short(timestamp_qh), Time.to_string_short(timestamp_ref)
        )
        PlotBuilder().with_signal(u_hds_dfs, title=title, figsize=(15, 7), grid=True).with_xlim(
            (-0.5, 1.5)
        ).with_signal(u_hds_ref_dfs, is_twinx=False, grid=True, style="--").with_ylabel(
            ylabel="U_HDS, [V]", color="C0"
        ).plot()

        PlotBuilder().with_signal(u_hds_dfs, title=title, figsize=(15, 7), grid=True).with_xlim(
            (-0.5, 1.5)
        ).with_signal(u_hds_ref_dfs, is_twinx=False, grid=True, style="--").with_ylabel(
            ylabel="U_HDS, [V]", color="C0"
        ).plot(
            yscale="log"
        )

    @staticmethod
    def display_comparison_tables(first_last_u_comp_df: pd.DataFrame, tau_comp_df: pd.DataFrame) -> None:
        """Method displaying comparison of initial and final voltage and characteristic decay time

        :param first_last_u_comp_df: pd.DataFrame with initial and final voltage compared to reference
        :param tau_comp_df: pd.DataFrame with characteristic decay time compared to reference
        :return: None
        """
        row_color_dct = {False: "background-color: red", True: ""}

        display_html(
            first_last_u_comp_df.style.set_table_attributes("style='display:inline'")
            .set_caption("Comparison of the inititial and final voltage")
            .apply(lambda s: first_last_u_comp_df["result"].map(row_color_dct))
            .format(precision=1)
            .to_html()
            + tau_comp_df.style.set_table_attributes("style='display:inline'")
            .set_caption("Comparison of the discharge characteristic time to the reference")
            .apply(lambda s: tau_comp_df["result"].map(row_color_dct))
            .format(precision=3)
            .to_html(),
            raw=True,
        )

    def get_analysis_result(self):
        print(self.analysis_result.toJSON())

    def get_status(self):
        print(self.analysis_result.get_status())
