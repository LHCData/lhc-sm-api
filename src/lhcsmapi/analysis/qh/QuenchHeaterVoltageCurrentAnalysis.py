import warnings
from typing import List, Tuple

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from IPython.display import display_html

from lhcsmapi.Time import Time
from lhcsmapi.analysis.decorators import check_dataframe_empty
from lhcsmapi.analysis.expert_input import check_show_next
from lhcsmapi.analysis.qh.QuenchHeaterVoltageAnalysis import QuenchHeaterVoltageAnalysis
from lhcsmapi.pyedsl.dbsignal.SignalIndexConversion import SignalIndexConversion
from lhcsmapi import reference
from lhcsmapi.analysis import comparison
import lhcsmapi.signal_analysis.features as signal_analysis
import lhcsmapi.analysis.features_helper as utility_features


class QuenchHeaterVoltageCurrentAnalysis(QuenchHeaterVoltageAnalysis):
    """Derivative class for quench heater analysis in circuits for which voltage and current are stored in PM buffers:
    - RQ (multiple QH discharges) - post HWC 2021
    - RB (multiple QH discharges)
    """

    @check_dataframe_empty("all", arg_names=["source_timestamp_qh_df"], warning="No QH events found, analysis skipped.")
    def analyze_multi_qh_voltage_current_with_ref(
        self,
        source_timestamp_qh_df: pd.DataFrame,
        u_hds_dfss: List[List[pd.DataFrame]],
        i_hds_dfss: List[List[pd.DataFrame]],
        u_hds_ref_dfss: List[List[pd.DataFrame]],
        i_hds_ref_dfss: List[List[pd.DataFrame]],
        current_offset: float,
        nominal_voltage=900,
    ) -> None:
        """Method analyzing quench heater discharge with voltage and current (for an actual and reference discharge)
        :param source_timestamp_qh_df: dataframe with PM source and timestamp for QH events
        :param u_hds_dfss: list of list of QH voltages (length of the outer list equal to the number of quenched magnets)
        :param i_hds_dfss: list of list of QH currents (length of the outer list equal to the number of quenched magnets)
        :param u_hds_ref_dfss: reference list of list of QH voltages (length of the outer list equal to the number of
        quenched magnets)
        :param i_hds_ref_dfss: reference list of list of QH voltages (length of the outer list equal to the number of
        quenched magnets)
        :param current_offset: value of the current offset to subtract from the input currents for feature and
        resistance calculation
        :param nominal_voltage: nominal QH voltage (300, 900); 900 by default
        """
        index_max = source_timestamp_qh_df.index[-1]
        for index, row in source_timestamp_qh_df.iterrows():
            source_qh, timestamp_qh = row[["source", "timestamp"]]
            print(
                "{}/{}: Analysing quench heater signals of {} on {}, {}".format(
                    index, index_max, source_qh, Time.to_string(timestamp_qh), timestamp_qh
                )
            )

            def is_from_source(dfs):
                return source_qh in dfs[0].columns[0]

            def get_dfs(dfss, data_description):
                if index < len(dfss) and is_from_source(dfss[index]):
                    return dfss[index]
                with_source = [dfs for dfs in dfss if is_from_source(dfs)]
                if len(with_source) > 0:
                    warnings.warn(f"{data_description} for {source_qh} found at the wrong index.")
                    return with_source[0]
                else:
                    warnings.warn(f"{data_description} missing for {source_qh}.")
                    return [pd.DataFrame()]

            u_hds_dfs = get_dfs(u_hds_dfss, "Voltages")
            i_hds_dfs = get_dfs(i_hds_dfss, "Currents")
            u_hds_ref_dfs = get_dfs(u_hds_ref_dfss, "Reference voltages")
            i_hds_ref_dfs = get_dfs(i_hds_ref_dfss, "Reference currents")
            self.analyze_single_qh_voltage_current_with_ref(
                source_qh,
                timestamp_qh,
                u_hds_dfs,
                i_hds_dfs,
                u_hds_ref_dfs,
                i_hds_ref_dfs,
                plot_qh_discharge=self.plot_voltage_current_resistance_with_ref,
                index_results_table=index,
                nominal_voltage=nominal_voltage,
                current_offset=current_offset,
            )

            # Show next
            if check_show_next(index, index_max, self.is_automatic):
                break

    @check_dataframe_empty(
        mode="any", warning="At least one DataFrame is empty, QH voltage and current analysis skipped!"
    )
    def analyze_single_qh_voltage_current_with_ref(
        self,
        source_qh: str,
        timestamp_qh: int,
        u_hds_dfs: List[pd.DataFrame],
        i_hds_dfs: List[pd.DataFrame],
        u_hds_ref_dfs: List[pd.DataFrame],
        i_hds_ref_dfs: List[pd.DataFrame],
        plot_qh_discharge,
        current_offset,
        nominal_voltage=900,
        index_results_table=0,
        mean_start_value=50,
    ) -> None:
        """Function analyzing a single QH discharge. It is finding the start of a pseudo-exponential decay of current
        and voltage and calculates resistance. All these signals are plotted against corresponding references. Plot
        function can be chosen (either for an FPA or HWC analysis).
        Initial and final voltage, time constant, initial resistance are calculated and compared to references.

        :param source_qh: magnet name
        :param timestamp_qh: QH PM timestamp
        :param u_hds_dfs: list of actual QH voltages
        :param i_hds_dfs: list of actual QH currents
        :param u_hds_ref_dfs: list of reference QH voltages
        :param i_hds_ref_dfs: list of reference QH currents
        :param plot_qh_discharge: function for plotting of voltages, currents, and resistances
        :param current_offset: value of the current offset to subtract from the input currents for feature and
        resistance calculation
        :param nominal_voltage: nominal QH voltage (300, 900); 900 by default
        :param index_results_table:
        :param mean_start_value: mean start value of current
        :return: None
        """

        # Extract decay
        u_hds_decay_dfs, i_hds_decay_dfs = self.preprocess_voltage_current(
            u_hds_dfs, i_hds_dfs, current_offset, mean_start_value
        )
        u_hds_decay_ref_dfs, i_hds_decay_ref_dfs = self.preprocess_voltage_current(
            u_hds_ref_dfs, i_hds_ref_dfs, current_offset, mean_start_value
        )

        # Synchronize time of the raw signal from PM to 0
        # # For plotting
        i_index_to_sync = i_hds_decay_dfs[0].index[0]
        i_index_to_sync_ref = i_hds_decay_ref_dfs[0].index[0]
        u_index_to_sync = u_hds_decay_dfs[0].index[0]
        u_index_to_sync_ref = u_hds_decay_ref_dfs[0].index[0]

        u_hds_sync_dfs = self.synchronize_raw_signal_to_decay(u_hds_dfs, u_index_to_sync)
        i_hds_sync_dfs = self.synchronize_raw_signal_to_decay(i_hds_dfs, i_index_to_sync)
        u_hds_ref_sync_dfs = self.synchronize_raw_signal_to_decay(u_hds_ref_dfs, u_index_to_sync_ref)
        i_hds_ref_sync_dfs = self.synchronize_raw_signal_to_decay(i_hds_ref_dfs, i_index_to_sync_ref)

        # # For feature engineering
        u_hds_decay_sync_dfs = SignalIndexConversion.synchronize_dfs(u_hds_decay_dfs)
        i_hds_decay_sync_dfs = SignalIndexConversion.synchronize_dfs(i_hds_decay_dfs)

        u_hds_decay_ref_sync_dfs = SignalIndexConversion.synchronize_dfs(u_hds_decay_ref_dfs)
        i_hds_decay_ref_sync_dfs = SignalIndexConversion.synchronize_dfs(i_hds_decay_ref_dfs)

        # Calculate resistance
        r_hds_dfs = self.calculate_resistance(u_hds_decay_sync_dfs, i_hds_decay_sync_dfs)
        r_hds_ref_dfs = self.calculate_resistance(u_hds_decay_ref_sync_dfs, i_hds_decay_ref_sync_dfs)

        # Plot
        plot_qh_discharge(
            source_qh,
            timestamp_qh,
            u_hds_sync_dfs,
            i_hds_sync_dfs,
            r_hds_dfs,
            u_hds_ref_sync_dfs,
            i_hds_ref_sync_dfs,
            r_hds_ref_dfs,
        )

        timestamp_ref = reference.get_quench_heater_reference_discharge(self.circuit_type, source_qh, timestamp_qh)

        # Calculate features
        first_last_u_df = self.calculate_first_and_last_voltage(timestamp_qh, u_hds_decay_sync_dfs)
        first_last_u_comp_df = self.compare_first_last_voltage_with_ref(
            self.circuit_type, source_qh, first_last_u_df, "CELL", nominal_voltage
        )

        first_r_df = self.calculate_initial_resistance(r_hds_dfs, timestamp_qh)
        first_r_ref_df = self.calculate_initial_resistance(r_hds_ref_dfs, timestamp_ref)
        first_r_comp_df = self.compare_initial_resistance_with_ref(
            self.circuit_type, first_r_df, first_r_ref_df, source_qh, nominal_voltage
        )

        tau_df = self.calculate_char_time(timestamp_qh, u_hds_decay_sync_dfs + i_hds_decay_sync_dfs)
        tau_ref_df = self.calculate_char_time(timestamp_qh, u_hds_decay_ref_sync_dfs + i_hds_decay_ref_sync_dfs)
        tau_comp_df = self.compare_char_time_with_ref(
            self.circuit_type, source_qh, tau_df, tau_ref_df, "CELL", nominal_voltage
        )

        capacitance_df = self.calculate_capacitance(tau_df, first_r_df)
        capacitance_ref_df = self.calculate_capacitance(tau_ref_df, first_r_ref_df)
        capacitance_comp_df = comparison.compare_difference_of_features_to_reference(
            capacitance_df, capacitance_ref_df, self.circuit_type, "QH", precision=3
        )

        # Display comparison
        self.display_features(first_last_u_comp_df, first_r_comp_df, tau_comp_df, capacitance_comp_df)

        # Assert comparison
        is_first_last_u_correct = first_last_u_comp_df["result"].all()
        is_first_r_correct = first_r_comp_df["result"].all()
        is_tau_correct = tau_comp_df["result"].all()
        is_qh_ok = is_first_last_u_correct and is_first_r_correct and is_tau_correct

        # Update results table
        final_label = "Pass" if is_qh_ok else "Fail"
        self.update_results_table("QH analysis", index_results_table, final_label)

        print("The QH discharges are labeled: %s." % final_label)
        print("Reference QH discharge timestamp is: %s" % Time.to_string_short(timestamp_ref))

        self.analysis_result.add_qh_analysis(timestamp_qh, source_qh, timestamp_ref, is_qh_ok)

        return is_qh_ok

    def analyze_single_qh_voltage_current(
        self,
        source_qh: str,
        timestamp_qh: int,
        u_hds_dfs: List[pd.DataFrame],
        i_hds_dfs: List[pd.DataFrame],
        plot_qh_discharge,
        current_offset,
        nominal_voltage=900,
        index_results_table=0,
        mean_start_value=50,
    ) -> None:
        """Function analyzing a single QH discharge. It is finding the start of a pseudo-exponential decay of current
        and voltage and calculates resistance. All these signals are plotted. Plot function can be chosen (either for an
        FPA or HWC analysis).
        Initial and final voltage, time constant, initial resistance are calculated and compared to references.

        :param source_qh: magnet name
        :param timestamp_qh: QH PM timestamp
        :param u_hds_dfs: list of actual QH voltages
        :param i_hds_dfs: list of actual QH currents
        :param plot_qh_discharge: function for plotting of voltages, currents, and resistances
        :param current_offset: value of the current offset to subtract from the input currents for feature and
        resistance calculation
        :param nominal_voltage: nominal QH voltage (300, 900); 900 by default
        :param index_results_table:
        :param mean_start_value: mean start value of current
        :return: None
        """

        # Extract decay
        u_hds_decay_dfs, i_hds_decay_dfs = self.preprocess_voltage_current(
            u_hds_dfs, i_hds_dfs, current_offset, mean_start_value
        )

        # Synchronize time of the raw signal from PM to 0
        # # For plotting
        index_to_sync = u_hds_decay_dfs[0].index[0]
        u_hds_sync_dfs = self.synchronize_raw_signal_to_decay(u_hds_dfs, index_to_sync)
        i_hds_sync_dfs = self.synchronize_raw_signal_to_decay(i_hds_dfs, index_to_sync)

        # # For feature engineering
        u_hds_decay_sync_dfs = SignalIndexConversion.synchronize_dfs(u_hds_decay_dfs)
        i_hds_decay_sync_dfs = SignalIndexConversion.synchronize_dfs(i_hds_decay_dfs)

        # Calculate resistance
        r_hds_dfs = self.calculate_resistance(u_hds_decay_sync_dfs, i_hds_decay_sync_dfs)

        # Plot
        plot_qh_discharge(source_qh, timestamp_qh, u_hds_sync_dfs, i_hds_sync_dfs, r_hds_dfs)

        timestamp_ref = reference.get_quench_heater_reference_discharge(self.circuit_type, source_qh, timestamp_qh)

        # Calculate features
        first_last_u_df = self.calculate_first_and_last_voltage(timestamp_qh, u_hds_decay_sync_dfs)
        first_last_u_comp_df = self.compare_first_last_voltage_with_ref(
            self.circuit_type, source_qh, first_last_u_df, "CELL", nominal_voltage
        )

        row_color_dct = {False: "background-color: red", True: ""}
        display_html(
            first_last_u_comp_df.style.set_table_attributes("style='display:inline'")
            .set_caption("Comparison of the inititial and final voltage")
            .apply(lambda s: first_last_u_comp_df["result"].map(row_color_dct))
            ._repr_html_(),
            raw=True,
        )

        # Assert comparison
        is_first_last_u_correct = first_last_u_comp_df["result"].all()
        is_qh_ok = is_first_last_u_correct

        # Update results table
        final_label = "Pass" if is_qh_ok else "Fail"
        self.update_results_table("QH analysis", index_results_table, final_label)

        print("The QH discharges are labeled: %s." % final_label)
        print("Reference QH discharge timestamp is: %s" % Time.to_string_short(timestamp_ref))

        self.analysis_result.add_qh_analysis(timestamp_qh, source_qh, timestamp_ref, is_qh_ok)

        return is_qh_ok

    @staticmethod
    def display_features(
        first_last_u_comp_df: pd.DataFrame,
        first_r_comp_df: pd.DataFrame,
        tau_comp_df: pd.DataFrame,
        capacitance_comp_df: pd.DataFrame,
    ) -> None:
        """Method displaying features computed for a QH decay and compared to references.
        In case a feature is outside of the range, the corresponding row(s) is(are) in red.

        :param first_last_u_comp_df: pd.DataFrame with initial and final voltage
        :param first_r_comp_df: pd.DataFrame with initial resistance
        :param tau_comp_df: pd.DataFrame with characteristic decay time
        :param capacitance_comp_df: pd.DataFrame with estimation of capacitance compared to reference
        :return: None
        """
        row_color_dct = {False: "background-color: red", True: "", np.nan: ""}
        display_html(
            first_last_u_comp_df.style.set_table_attributes("style='display:inline'")
            .set_caption("Comparison of the inititial and final voltage")
            .apply(lambda s: first_last_u_comp_df["result"].map(row_color_dct))
            .format(precision=1)
            .to_html()
            + first_r_comp_df.style.set_table_attributes("style='display:inline'")
            .set_caption("Comparison of the initial resistance to the reference")
            .apply(lambda s: first_r_comp_df["result"].map(row_color_dct))
            .format(precision=2)
            .to_html()
            + tau_comp_df.style.set_table_attributes("style='display:inline'")
            .set_caption("Comparison of the discharge characteristic time to the reference")
            .apply(lambda s: tau_comp_df["result"].map(row_color_dct))
            .format(precision=3)
            .to_html()
            + capacitance_comp_df.style.set_table_attributes("style='display:inline'")
            .set_caption("Comparison of the estimated capacitance to the reference")
            .apply(lambda s: capacitance_comp_df["result"].map(row_color_dct))
            .format(precision=3)
            .to_html(),
            raw=True,
        )

    @staticmethod
    def synchronize_raw_signal_to_decay(hds_dfs: List[pd.DataFrame], index_to_sync: float) -> List[pd.DataFrame]:
        """Function synchronizing raw signal to decay start index

        :param hds_dfs: list of signals to synchronize
        :param index_to_sync: index to which signals are synchronized (in seconds)
        :return: list of synchronized signals
        """
        return [SignalIndexConversion.synchronize_df(hds_df, index_to_sync) for hds_df in hds_dfs]

    def preprocess_voltage_current(
        self,
        u_hds_dfs: List[pd.DataFrame],
        i_hds_dfs: List[pd.DataFrame],
        current_offset: float,
        mean_start_value: float,
    ) -> Tuple[List[pd.DataFrame], List[pd.DataFrame]]:
        """Method preprocessing voltage and current signals by subtracting current offset, finding start of a decay
        based on current and filtering signals with median filter

        :param u_hds_dfs: list of actual QH voltages
        :param i_hds_dfs: list of actual QH currents
        :param current_offset: value of the current offset to subtract from the input currents for feature and
        resistance calculation
        :param mean_start_value: mean start value of current
        :return: tuple of lists of voltages and currents after pre-processing
        """
        # Subtract current offset
        i_hds_no_offset_dfs = [i_hds_df - current_offset for i_hds_df in i_hds_dfs if not i_hds_df.empty]

        # Find start of decay
        index_decay_start = self.get_decay_start_index(i_hds_dfs, mean_start_value)

        # Extract decay only
        u_hds_decay_dfs = self.extract_decay(u_hds_dfs, index_decay_start)
        i_hds_no_offset_decay_dfs = self.extract_decay(i_hds_no_offset_dfs, index_decay_start)

        # Median filter
        return self.filter_median(u_hds_decay_dfs), self.filter_median(i_hds_no_offset_decay_dfs)

    def get_decay_start_index(self, i_hds_dfs: List[pd.DataFrame], mean_start_value=50, index_increment=20) -> int:
        """Function calculating the start index of the current decay in a QH supply unit

        :param i_hds_dfs: list of QH current signals
        :param mean_start_value: mean value at the beginning
        :param index_increment: index increment for the rolling check of the mean and std values
        :return: start index of an exponential decay
        """
        decay_start_index = 0
        for i_hds_df in i_hds_dfs:
            i_decay_start_index = self.get_start_index_with_current_mean_std(
                i_hds_df, index_increment, mean_start_value
            )
            decay_start_index = max(decay_start_index, i_decay_start_index)

        return decay_start_index

    @staticmethod
    def get_start_index_with_current_mean_std(
        i_hds_df: pd.DataFrame, index_increment: int, mean_start_value: int
    ) -> int:
        """Function returning start index of an exponential current decay based on its mean and std value

        :param i_hds_df: QH current signal
        :param index_increment: index increment for the rolling check of the mean and std values
        :param mean_start_value: mean value at the beginning
        :return: 0 if index not found, otherwise the found index
        """

        def has_window_mean_std(x: np.ndarray, mean=50, std=0.1) -> bool:
            """Function checking if a window vector has certain mean and std values

            :param x: input vector
            :param mean: threshold mean value
            :param std: threshold standard deviation value
            :return: True if vector has a mean value greater or equal to the mean threshold and standard deviation
            smaller or equal to the std threshold
            """
            return (x.mean() >= mean) and (x.std() <= std)

        with_mean_std_df = (
            i_hds_df.rolling(index_increment)
            .apply(has_window_mean_std, args=(mean_start_value,), raw=True)
            .shift(-index_increment + 1)
        )

        with_mean_std_df.dropna(inplace=True)
        mask = with_mean_std_df[with_mean_std_df[with_mean_std_df.columns[0]] == 1.0]
        if len(mask) == 0:
            return 0
        else:
            return with_mean_std_df.index.get_loc(mask.index[0])

    @staticmethod
    def extract_decay(hds_dfs, index_decay_start):
        """Method returning signals with exponential decays only

        :param hds_dfs: list of QH signals
        :param index_decay_start: index of the decay start
        :return: list of QH signals with exponential decays only
        """
        if index_decay_start == 0:
            return hds_dfs
        else:
            return [hds_df.iloc[(index_decay_start + 1) : len(hds_df)] for hds_df in hds_dfs]

    @staticmethod
    def filter_median(hds_dfs, window=3) -> List[pd.DataFrame]:
        """Median filter with default window size of 3

        :param hds_dfs: list of signals to filter
        :param window: window size, 3 by default
        :return: list of signals after applying the median filter with a given window size
        """
        return [hds_df.rolling(window=window, min_periods=1).median() for hds_df in hds_dfs]

    @staticmethod
    def calculate_resistance(u_hds_dfs: List[pd.DataFrame], i_hds_dfs: List[pd.DataFrame]) -> List[pd.DataFrame]:
        """Function calculating a list of resistances of a signal from a list of QH voltages and currents

        :param u_hds_dfs: list of QH voltages
        :param i_hds_dfs: list of QH currents
        :return: list of QH resistances
        """
        r_hds_dfs = []
        for u_hds_df, i_hds_df in zip(u_hds_dfs, i_hds_dfs):
            r_value = u_hds_df.values / i_hds_df.values
            column = u_hds_df.columns[0].replace("U", "R")
            time = u_hds_df.index
            r_hds_dfs.append(pd.DataFrame(data=r_value, index=time, columns=[column]))

        return r_hds_dfs

    @staticmethod
    def calculate_initial_resistance(r_hds_dfs: List[pd.DataFrame], timestamp_qh) -> pd.DataFrame:
        """Method calculating initial resistance. The calculated feature is rounded to two decimal places.

        :param r_hds_dfs: list of initial QH resistance
        :param timestamp_qh: QH PM timestamp
        :return: Pandas DataFrame with index set to timestamp_qh and columns given by the names of voltages and
        selected features. E.g, assuming r_hds_dfs = [R_HDS_1, R_HDS_2]
        index     | R_HDS_1_first20mean | R_HDS_2_first20mean |
        timestamp |                     |                     |
        """

        first_r_df = signal_analysis.calculate_features(r_hds_dfs, utility_features.first20mean, timestamp_qh)
        first_r_df = first_r_df.round(2)
        return first_r_df

    @staticmethod
    def compare_initial_resistance_with_ref(
        circuit_type, first_r_df: pd.DataFrame, first_r_ref_df: pd.DataFrame, source_qh: str, nominal_voltage: int
    ) -> pd.DataFrame:
        """Method comparing initial resistance to the reference initial resistance.
        :param circuit_type: circuit type
        :param first_r_df: pd.DataFrame with actual initial resistance
        :param first_r_ref_df: pd.DataFrame with reference initial resistance
        :param source_qh: source
        :param nominal_voltage: nominal_voltage
        :return: pd.DataFrame with comparison
        """
        return comparison.compare_difference_of_features_to_reference(
            first_r_df,
            first_r_ref_df,
            circuit_type,
            "QH",
            wildcard={"CELL": source_qh},
            nominal_voltage=nominal_voltage,
            precision=2,
        )

    @staticmethod
    def calculate_capacitance(tau_df: pd.DataFrame, first_r_df: pd.DataFrame) -> pd.DataFrame:
        """Calculates the estimation of capacitance.
        :param tau_df: pd.DataFrame with discharge characteristic time
        :param first_r_df: pd.DataFrame with actual initial resistance
        :return: pd.DataFrame
        """

        def get_transposed_with_feature_name(df: pd.DataFrame):
            def get_feature_name(old_name: str) -> str:
                return old_name.split(":")[0] + "_C_HDS_" + old_name.split(":")[-2].split("_")[-1] + ":capacitance"

            transposed = df.T
            transposed.index = transposed.index.map(get_feature_name)
            return transposed

        tau_df_transposed = get_transposed_with_feature_name(tau_df.filter(regex="(.*):U_(.*)"))
        first_r_df_transposed = get_transposed_with_feature_name(first_r_df)

        tau_df_transposed.iloc[:, 0] = tau_df_transposed.iloc[:, 0] / first_r_df_transposed.iloc[:, 0]
        return tau_df_transposed.T

    @staticmethod
    def plot_voltage_current_resistance_with_ref(
        source_qh: int,
        timestamp_qh: int,
        u_hds_dfs: List[pd.DataFrame],
        i_hds_dfs: List[pd.DataFrame],
        r_hds_dfs: List[pd.DataFrame],
        u_hds_ref_dfs: List[pd.DataFrame],
        i_hds_ref_dfs: List[pd.DataFrame],
        r_hds_ref_dfs: List[pd.DataFrame],
    ) -> None:
        """Function plotting QH signals: voltage, current, resistance for actual and reference timestamp.
        The layout is suited for FPA analysis.

        :param source_qh: magnet name (QH PM source)
        :param timestamp_qh: actual QH PM timestamp
        :param u_hds_dfs: QH voltages of a magnet
        :param i_hds_dfs: QH currents of a magnet
        :param r_hds_dfs: QH resistances of a magnet
        :param u_hds_ref_dfs: reference QH voltages of a magnet
        :param i_hds_ref_dfs: reference QH currents of a magnet
        :param r_hds_ref_dfs: reference QH resistances of a magnet
        :return: None
        """
        title = "Magnet: {}, Time Stamp: {}, U_HDS(t), I_HDS(t), and R_HDS(t)".format(
            source_qh, Time.to_string_short(timestamp_qh)
        )

        fig, ax = plt.subplots(1, 3, figsize=(20, 7))
        for u_hds_df, u_hds_ref_df in zip(u_hds_dfs, u_hds_ref_dfs):
            u_hds_df.plot(ax=ax[0])
            u_hds_ref_df.plot(ax=ax[0], style="--")

        ax[0].grid(True)
        ax[0].tick_params(labelsize=15)
        ax[0].set_xlabel("time, [s]", fontsize=15)
        ax[0].set_ylabel("U_HDS, [V]", fontsize=15)

        for i_hds_df, i_hds_ref_df in zip(i_hds_dfs, i_hds_ref_dfs):
            i_hds_df.plot(ax=ax[1], title=title)
            i_hds_ref_df.plot(ax=ax[1], style="--")

        ax[1].title.set_size(20)
        ax[1].grid(True)
        ax[1].tick_params(labelsize=15)
        ax[1].set_xlabel("time, [s]", fontsize=15)
        ax[1].set_ylabel("I_HDS, [A]", fontsize=15)

        for r_hds_df, r_hds_ref_df in zip(r_hds_dfs, r_hds_ref_dfs):
            r_hds_df.plot(ax=ax[2])
            r_hds_ref_df.plot(ax=ax[2], style="--")
        ax[2].grid(True)
        ax[2].tick_params(labelsize=15)
        ax[2].set_xlabel("time, [s]", fontsize=15)
        ax[2].set_ylabel("R_HDS (Calculated), [Ohm]", fontsize=15)

        plt.show()

    @staticmethod
    def plot_voltage_current_resistance_with_ref_hwc(
        source_qh: str,
        timestamp_qh: int,
        u_hds_dfs: List[pd.DataFrame],
        i_hds_dfs: List[pd.DataFrame],
        r_hds_dfs: List[pd.DataFrame],
        u_hds_ref_dfs: List[pd.DataFrame],
        i_hds_ref_dfs: List[pd.DataFrame],
        r_hds_ref_dfs: List[pd.DataFrame],
    ) -> None:
        """Function plotting QH signals: voltage, current, resistance for actual and reference timestamp.
        The layout is suited for HWC analysis.

        :param source_qh: magnet name (QH PM source)
        :param timestamp_qh: actual QH PM timestamp
        :param u_hds_dfs: QH voltages of a magnet
        :param i_hds_dfs: QH currents of a magnet
        :param r_hds_dfs: QH resistances of a magnet
        :param u_hds_ref_dfs: reference QH voltages of a magnet
        :param i_hds_ref_dfs: reference QH currents of a magnet
        :param r_hds_ref_dfs: reference QH resistances of a magnet
        :return: None
        """

        title = "Magnet: {}, Time Stamp: {}, U_HDS(t), I_HDS(t), and R_HDS(t)".format(
            source_qh, Time.to_string_short(timestamp_qh)
        )

        fig, ax = plt.subplots(2, 2, figsize=(20, 14))
        fig.suptitle(title, fontsize=20)

        for u_hds_df, u_hds_ref_df in zip(u_hds_dfs, u_hds_ref_dfs):
            u_hds_df.plot(ax=ax[0][0])
            u_hds_ref_df.plot(ax=ax[0][0], style="--")

        ax[0][0].grid(True)
        ax[0][0].tick_params(labelsize=15)
        ax[0][0].set_xlabel("time, [s]", fontsize=15)
        ax[0][0].set_ylabel("U_HDS, [V]", fontsize=15)

        for u_hds_df, u_hds_ref_df in zip(u_hds_dfs, u_hds_ref_dfs):
            u_hds_df.plot(ax=ax[1][0])
            u_hds_ref_df.plot(ax=ax[1][0], logy=True, style="--")

        ax[1][0].grid(True)
        ax[1][0].tick_params(labelsize=15)
        ax[1][0].set_xlabel("time, [s]", fontsize=15)
        ax[1][0].set_ylabel("U_HDS (log-y), [V]", fontsize=15)

        for i_hds_df, i_hds_ref_df in zip(i_hds_dfs, i_hds_ref_dfs):
            i_hds_df.plot(ax=ax[0][1])
            i_hds_ref_df.plot(ax=ax[0][1], style="--")

        ax[0][1].grid(True)
        ax[0][1].tick_params(labelsize=15)
        ax[0][1].set_xlabel("time, [s]", fontsize=15)
        ax[0][1].set_ylabel("I_HDS, [A]", fontsize=15)

        for i_hds_df, i_hds_ref_df in zip(i_hds_dfs, i_hds_ref_dfs):
            i_hds_df.plot(ax=ax[1][1])
            i_hds_ref_df.plot(ax=ax[1][1], logy=True, style="--")

        ax[1][1].grid(True)
        ax[1][1].tick_params(labelsize=15)
        ax[1][1].set_xlabel("time, [s]", fontsize=15)
        ax[1][1].set_ylabel("I_HDS (log-y), [A]", fontsize=15)

        plt.show()

        fig, ax = plt.subplots(1, 1, figsize=(20, 7))

        for r_hds_df, r_hds_ref_df in zip(r_hds_dfs, r_hds_ref_dfs):
            r_hds_df.plot(ax=ax)
            r_hds_ref_df.plot(ax=ax, style="--")
        ax.grid(True)
        ax.tick_params(labelsize=15)
        ax.set_xlabel("time, [s]", fontsize=15)
        ax.set_ylabel("R_HDS (Calculated), [Ohm]", fontsize=15)
        ax.set_ylim(0, 25)

        plt.show()

    @staticmethod
    def plot_voltage_current_resistance(
        source_qh: str,
        timestamp_qh: int,
        u_hds_dfs: List[pd.DataFrame],
        i_hds_dfs: List[pd.DataFrame],
        r_hds_dfs: List[pd.DataFrame],
    ) -> None:
        """Function plotting QH signals: voltage, current, resistance for actual timestamp.
        The layout is suited for HWC analysis.

        :param source_qh: magnet name (QH PM source)
        :param timestamp_qh: actual QH PM timestamp
        :param u_hds_dfs: QH voltages of a magnet
        :param i_hds_dfs: QH currents of a magnet
        :param r_hds_dfs: QH resistances of a magnet
        :return: None
        """

        title = "Magnet: {}, Time Stamp: {}, U_HDS(t), I_HDS(t), and R_HDS(t)".format(
            source_qh, Time.to_string_short(timestamp_qh)
        )

        fig, ax = plt.subplots(2, 2, figsize=(20, 14))
        fig.suptitle(title, fontsize=20)

        for u_hds_df in u_hds_dfs:
            u_hds_df.plot(ax=ax[0][0])

        ax[0][0].grid(True)
        ax[0][0].tick_params(labelsize=15)
        ax[0][0].set_xlabel("time, [s]", fontsize=15)
        ax[0][0].set_ylabel("U_HDS, [V]", fontsize=15)

        for u_hds_df in u_hds_dfs:
            u_hds_df.plot(ax=ax[1][0], logy=True)
        ax[1][0].grid(True)
        ax[1][0].tick_params(labelsize=15)
        ax[1][0].set_xlabel("time, [s]", fontsize=15)
        ax[1][0].set_ylabel("U_HDS, [V]", fontsize=15)

        for i_hds_df in i_hds_dfs:
            i_hds_df.plot(ax=ax[0][1])
        ax[0][1].title.set_size(20)
        ax[0][1].grid(True)
        ax[0][1].tick_params(labelsize=15)
        ax[0][1].set_xlabel("time, [s]", fontsize=15)
        ax[0][1].set_ylabel("I_HDS, [A]", fontsize=15)

        for i_hds_df in i_hds_dfs:
            i_hds_df.plot(ax=ax[1][1], logy=True)
        ax[1][1].title.set_size(20)
        ax[1][1].grid(True)
        ax[1][1].tick_params(labelsize=15)
        ax[1][1].set_xlabel("time, [s]", fontsize=15)
        ax[1][1].set_ylabel("I_HDS, [A]", fontsize=15)

        plt.show()

        fig, ax = plt.subplots(1, 1, figsize=(20, 7))

        for r_hds_df in r_hds_dfs:
            r_hds_df.plot(ax=ax)
        ax.grid(True)
        ax.tick_params(labelsize=15)
        ax.set_xlabel("time, [s]", fontsize=15)
        ax.set_ylabel("R_HDS (Calculated), [Ohm]", fontsize=15)
        ax.set_ylim(0, 25)

        plt.show()

    def calculate_qh_feature_row(
        self,
        u_hds_dfs: List[pd.DataFrame],
        i_hds_dfs: List[pd.DataFrame],
        timestamp: int,
        current_offset: float,
        mean_start_value: float,
    ) -> pd.DataFrame:
        """Method calculating a qh feature row for voltage and current:
        - initial and final voltage
        - initial resistance
        - characteristic decay time for voltage and current

        :param u_hds_dfs: list of QH voltages
        :param i_hds_dfs: list of QH currents
        :param timestamp: QH PM timestamp
        :param current_offset: current offset in A to subtract
        :param mean_start_value: mean start value of current in A for decay start detection
        :return:
        """
        try:
            u_hds_decay_dfs, i_hds_decay_dfs = self.preprocess_voltage_current(
                u_hds_dfs, i_hds_dfs, current_offset=current_offset, mean_start_value=mean_start_value
            )
            u_hds_decay_sync_dfs = SignalIndexConversion.synchronize_dfs(u_hds_decay_dfs)
            i_hds_decay_sync_dfs = SignalIndexConversion.synchronize_dfs(i_hds_decay_dfs)
            r_hds_dfs = self.calculate_resistance(u_hds_decay_sync_dfs, i_hds_decay_sync_dfs)

            u_names = [u_hds_df.columns[0] for u_hds_df in u_hds_decay_sync_dfs]
            i_names = [i_hds_df.columns[0] for i_hds_df in i_hds_decay_sync_dfs]
            r_names = [u_name.replace("U_", "R_") for u_name in u_names]

            # calculate features
            first_last_u_df = self.calculate_first_and_last_voltage(timestamp, u_hds_decay_sync_dfs)
            # rename columns
            first_last_u_df.rename(
                inplace=True, columns={"%s:first" % u_name: "U_%d_0" % (i + 1) for i, u_name in enumerate(u_names)}
            )
            first_last_u_df.rename(
                inplace=True,
                columns={"%s:last20mean" % u_name: "U_%d_end" % (i + 1) for i, u_name in enumerate(u_names)},
            )

            # calculate features
            first_r_df = self.calculate_initial_resistance(r_hds_dfs, timestamp)
            # rename columns
            first_r_df.rename(
                inplace=True,
                columns={"%s:first20mean" % r_name: "R_%d_0" % (i + 1) for i, r_name in enumerate(r_names)},
            )

            # calculate features
            tau_df = self.calculate_char_time(timestamp, u_hds_decay_sync_dfs + i_hds_decay_sync_dfs)
            # rename columns
            tau_df.rename(
                inplace=True,
                columns={"%s:tau_charge" % u_name: "U_%d_tau" % (i + 1) for i, u_name in enumerate(u_names)},
            )
            tau_df.rename(
                inplace=True,
                columns={"%s:tau_charge" % i_name: "I_%d_tau" % (i + 1) for i, i_name in enumerate(i_names)},
            )

            return pd.concat([first_last_u_df, first_r_df, tau_df], axis=1)

        except Exception as e:
            warnings.warn(str(e))
            n_signals = len(u_hds_dfs)
            u_0_names = ["U_%d_0" % (i + 1) for i in range(n_signals)]
            u_end_names = ["U_%d_end" % (i + 1) for i in range(n_signals)]
            r_0_names = ["R_%d_0" % (i + 1) for i in range(n_signals)]
            u_tau_names = ["U_%d_tau" % (i + 1) for i in range(n_signals)]
            i_tau_names = ["I_%d_tau" % (i + 1) for i in range(n_signals)]
            return pd.DataFrame(
                columns=u_0_names + u_end_names + r_0_names + u_tau_names + i_tau_names, index=[timestamp]
            )
