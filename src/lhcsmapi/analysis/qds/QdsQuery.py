from typing import List, Union, Tuple
import requests

import pandas as pd
from pyspark.sql.session import SparkSession
from tqdm.notebook import tqdm

from lhcsmapi.Time import Time
from lhcsmapi.analysis.CircuitQuery import CircuitQuery, execution_count
from lhcsmapi.analysis.decorators import check_nan_timestamp_signals, check_nan_timestamp
from lhcsmapi.metadata import signal_metadata
from lhcsmapi.api.query_builder import QueryBuilder


class QdsQuery(CircuitQuery):
    """Base class for quench detection system (QDS) query"""

    @execution_count()
    @check_nan_timestamp_signals(return_type=pd.DataFrame())
    def query_iqps_nxcals(
        self,
        timestamp_fgc: int,
        *,
        signal_names: List[str],
        spark: SparkSession,
        duration: List[Tuple[int, str]] = [(300, "s"), (10, "s")],
    ) -> List[pd.DataFrame]:
        """Method querying NXCALS for iQPS signals

        :param timestamp_fgc: FGC PM event timestamp (ns precision) to which the query is synchronised
        :param signal_names: list of signal names to query according to metadata description
        :param spark: spark session variable needed to perform an NXCALS query
        :param duration: search duration before and after the start time
        :return: list of pd.DataFrames (if a signal was not queried, an empty pd.DataFrame is returned and warning
            containing the raw query is returned)
        """
        return (
            QueryBuilder()
            .with_nxcals(spark)
            .with_duration(t_start=timestamp_fgc, duration=duration)
            .with_circuit_type(self.circuit_type)
            .with_metadata(circuit_name=self.circuit_name, system="QDS", signal=signal_names)
            .signal_query(verbose=self.verbose)
            .synchronize_time(timestamp_fgc)
            .convert_index_to_sec()
            .get_dataframes()
        )

    @execution_count()
    @check_nan_timestamp(return_type=pd.DataFrame())
    def find_source_timestamp_qds(
        self, t_start: Union[int, str, float], duration: List[Tuple[int, str]] = [(10, "s"), (400, "s")]
    ) -> pd.DataFrame:
        """Method searching PM QDS events
        If the start is NaN, then an empty dataframe is returned.

        :param t_start: start time of a search
        :param duration: search duration before and after the start time
        :return: pd.DataFrame with source and timestamp of the PM QDS events
        """
        return (
            QueryBuilder()
            .with_pm()
            .with_duration(t_start=t_start, duration=duration)
            .with_circuit_type(self.circuit_type)
            .with_metadata(circuit_name=self.circuit_name, system="QDS", source="*")
            .event_query(verbose=self.verbose)
            .filter_source(self.circuit_type, self.circuit_name, "QDS")
            .sort_values(by="timestamp")
            .drop_duplicate_source()
            .get_dataframe()
        )

    @check_nan_timestamp(return_type=pd.DataFrame())
    def find_source_timestamp_qds_board_ab(
        self, t_start: Union[int, str, float], duration: List[Tuple[int, str]] = [(10, "s"), (400, "s")]
    ) -> pd.DataFrame:
        """Method searching PM QDS events
        If the start is NaN, then an empty dataframe is returned.

        :param t_start: start time of a search
        :param duration: search duration before and after the start time
        :return: pd.DataFrame with source and timestamp of the PM QDS events
        """
        return (
            QueryBuilder()
            .with_pm()
            .with_duration(t_start=t_start, duration=duration)
            .with_circuit_type(self.circuit_type)
            .with_metadata(circuit_name=self.circuit_name, system="QDS", source="*")
            .event_query(verbose=self.verbose)
            .filter_source(self.circuit_type, self.circuit_name, "QDS")
            .sort_values(by=["source", "timestamp"])
            .get_dataframe()
            .reset_index(drop=True)
        )

    @execution_count()
    @check_nan_timestamp_signals(return_type=pd.DataFrame())
    def query_qds_pm(
        self, timestamp_qds: int, timestamp_qds_sync: int, *, signal_names: List[str]
    ) -> List[pd.DataFrame]:
        """Method querying PM for QDS signals
        If the timestamp is NaN, then a list of empty dataframes of the size of the signal names is returned.
        The signals are synchronized in time and converted from ns to s.

        :param timestamp_qds: PM QDS timestamp in ns precision
        :param timestamp_qds: PM QDS timestamp in ns precision (for signal synchronization)
        :param signal_names: list of signal names to query according to metadata description
        :return: list of pd.DataFrames (if a signal was not queried, an empty pd.DataFrame is returned and warning
            containing the raw query is returned)
        """
        return (
            QueryBuilder()
            .with_pm()
            .with_timestamp(timestamp_qds)
            .with_circuit_type(self.circuit_type)
            .with_metadata(circuit_name=self.circuit_name, system="QDS", signal=signal_names)
            .signal_query(verbose=self.verbose)
            .synchronize_time(timestamp_qds_sync)
            .convert_index_to_sec()
            .filter_median()
            .get_dataframes()
        )

    @execution_count()
    def query_iqps_analog_pm(
        self, source_timestamp_qds_df: pd.DataFrame, signal_names: List[str]
    ) -> List[List[pd.DataFrame]]:
        """Method querying PM for iQPS analog signals
        Signals are synchronized to the iQPS PM event and index is converted to seconds.

        :param source_timestamp_qds_df: dataframe with PM source and timestamp for QDS events
        :param signal_names: list of signal names to query according to metadata description
        :return: List of list of iQPS signals. The outer list has as many elements as iQPS events and the lenght
            of the inner is equal to the number of iQPS signals.
        """
        iqps_analog_dfs = []
        for index, row in tqdm(
            source_timestamp_qds_df.iterrows(), total=source_timestamp_qds_df.shape[0], desc="Querying iQPS PM"
        ):
            timestamp_qds = row["timestamp"]
            source_qds = row["source"]

            iqps_analog_df = (
                QueryBuilder()
                .with_pm()
                .with_timestamp(timestamp_qds)
                .with_circuit_type(self.circuit_type)
                .with_metadata(
                    circuit_name=self.circuit_name,
                    system="QDS",
                    signal=signal_names,
                    source=source_qds,
                    wildcard={"CELL": source_qds},
                )
                .signal_query()
                .synchronize_time(timestamp_qds)
                .convert_index_to_sec()
                .filter_median()
                .get_dataframes()
            )

            iqps_analog_dfs.append(iqps_analog_df)

        return iqps_analog_dfs

    @execution_count()
    def query_iqps_digital_pm(
        self, source_timestamp_qds_df: pd.DataFrame, signal_names: List[str]
    ) -> List[List[pd.DataFrame]]:
        """Method querying PM for iQPS digital signals
        Signals are synchronized to the iQPS PM event and index is converted to seconds.

        :param source_timestamp_qds_df: dataframe with PM source and timestamp for QDS events
        :param signal_names: list of signal names to query according to metadata description
        :return: List of list of iQPS signals. The outer list has as many elements as iQPS events and the lenght
            of the inner is equal to the number of iQPS signals.
        """
        iqps_digital_dfs = []
        for index, row in tqdm(
            source_timestamp_qds_df.iterrows(), total=source_timestamp_qds_df.shape[0], desc="Querying IQPS PM"
        ):
            timestamp_qds = row["timestamp"]
            source_qds = row["source"]

            iqps_digital_df = (
                QueryBuilder()
                .with_pm()
                .with_timestamp(timestamp_qds)
                .with_circuit_type(self.circuit_type)
                .with_metadata(circuit_name=self.circuit_name, system="QDS", signal=signal_names, source=source_qds)
                .signal_query()
                .synchronize_time(timestamp_qds)
                .convert_index_to_sec()
                .get_dataframes()
            )
            iqps_digital_dfs.append(iqps_digital_df)

        return iqps_digital_dfs


class QdsIpdQuery(QdsQuery):
    """Class for IPD quench detection system (QDS) query"""

    @execution_count()
    @check_nan_timestamp(return_type=pd.DataFrame())
    def find_source_timestamp_qds(
        self, t_start: Union[int, str, float], duration: List[Tuple[int, str]] = [(10, "s"), (400, "s")]
    ) -> pd.DataFrame:
        """Method searching PM QDS events
        If the start is NaN, then an empty dataframe is returned.

        :param t_start: start time of a search
        :param duration: search duration before and after the start time
        :return: pd.DataFrame with source and timestamp of the PM QDS events
        """
        return (
            QueryBuilder()
            .with_pm()
            .with_duration(t_start=t_start, duration=duration)
            .with_circuit_type(self.circuit_type)
            .with_metadata(circuit_name=self.circuit_name, system="QDS", source="*")
            .event_query(verbose=self.verbose)
            .sort_values(by="timestamp")
            .get_dataframe()
        )


class QdsRbQuery(QdsQuery):
    """Class for RB quench detection system (QDS) query"""

    # iQPS
    @execution_count()
    def query_pm_iqps_board_type(self, source_timestamp_qds_df: pd.DataFrame) -> pd.DataFrame:
        """Method querying PM for qps board type

        :param source_timestamp_qds_df: table with QDS PM source and timestamp

        :return: table with iQPS board types (1, 0)
        """
        iqps_board_type_df = pd.DataFrame(columns=["iqps_board_type"])
        board_types = []
        for index, row in tqdm(
            source_timestamp_qds_df.iterrows(), total=source_timestamp_qds_df.shape[0], desc="Querying PM"
        ):
            source_qds, timestamp_qds = row[["source", "timestamp"]]

            metadata_qds = signal_metadata.get_signal_metadata(
                self.circuit_type, self.circuit_name, "QDS", "PM", timestamp_qds
            )

            # ToDo: It would be nice to add this query to the library
            pm_request = (
                "http://pm-rest.cern.ch/v2/"
                + "/pmdata?system=QPS&className="
                + metadata_qds["className"]
                + "&source="
                + source_qds
                + "&TimestampInNanos="
                + str(timestamp_qds)
                + "&pageSize=1&pageIndex=0"
            )
            resp = requests.get(pm_request)
            names_and_values = resp.json()["content"][0]["namesAndValues"]
            board_type = [entry for entry in names_and_values if entry["name"] == "BOARD"]
            board_types.append(board_type[0]["value"])

        iqps_board_type_df["iqps_board_type"] = board_types
        return iqps_board_type_df

    @execution_count()
    def query_voltage_logic_iqps(
        self, source_timestamp_qds_df: pd.DataFrame, *, signal_names: List[str], filter_window=1
    ):
        """Method querying voltage and logic signals of iQPS.
        The signals are synchronized in time and converted from ns to s.

        :param source_timestamp_qds_df: table with source and timestamp for iQPS
        :param signal_names: list of signal names to query
        :return: list of pd.DataFrames (if a signal was not queried, an empty pd.DataFrame is returned and warning
            containing the raw query is returned)
        """
        u_qds_dfs = []
        for index, row in tqdm(
            source_timestamp_qds_df.iterrows(), total=source_timestamp_qds_df.shape[0], desc="Querying iQPS logic PM"
        ):
            source_qds, timestamp_qds = row[["source", "timestamp"]]

            u_qds_df = (
                QueryBuilder()
                .with_pm()
                .with_timestamp(timestamp_qds)
                .with_circuit_type(self.circuit_type)
                .with_metadata(
                    circuit_name=self.circuit_name,
                    system="QDS",
                    source=source_qds,
                    wildcard={"CELL": source_qds},
                    signal=signal_names,
                )
                .signal_query()
                .synchronize_time(timestamp_qds)
                .convert_index_to_sec()
                .filter_median(filter_window)
                .get_dataframes()
            )

            u_qds_dfs.append(u_qds_df)

        return u_qds_dfs

    @execution_count()
    @check_nan_timestamp_signals(return_type=pd.DataFrame())
    def query_iqps_nxcals(
        self,
        timestamp_fgc: int,
        *,
        signal_names: List[str],
        spark: SparkSession,
        duration: List[Tuple[int, str]] = [(300, "s"), (10, "s")],
    ) -> List[pd.DataFrame]:
        """Method querying NXCALS for iQPS signals

        :param timestamp_fgc: FGC PM event timestamp (ns precision) to which the query is synchronised
        :param signal_names: list of signal names to query according to metadata description
        :param spark: spark session variable needed to perform an NXCALS query
        :param duration: search duration before and after the start time
        :return: list of pd.DataFrames (if a signal was not queried, an empty pd.DataFrame is returned and warning
            containing the raw query is returned)
        """
        return (
            QueryBuilder()
            .with_nxcals(spark)
            .with_duration(t_start=timestamp_fgc, duration=duration)
            .with_circuit_type(self.circuit_type)
            .with_metadata(circuit_name=self.circuit_name, system="QDS", signal=signal_names)
            .signal_query(verbose=self.verbose)
            .synchronize_time(timestamp_fgc)
            .convert_index_to_sec()
            .filter_median()
            .get_dataframes()
        )
