import math
import warnings
from copy import deepcopy
from typing import List, Optional, Tuple, Union

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from IPython.display import display, HTML

from lhcsmapi.Time import Time
from lhcsmapi.analysis.CircuitAnalysis import CircuitAnalysis
from lhcsmapi.analysis.decorators import check_dataframe_empty
from lhcsmapi.analysis.expert_input import get_expert_decision, check_show_next
from lhcsmapi.api.processing import SignalProcessing
from lhcsmapi.metadata.MappingMetadata import MappingMetadata
from lhcsmapi.pyedsl.PlotBuilder import create_title, PlotBuilder


class QdsAnalysis(CircuitAnalysis):
    """Base class for quench detection system (QDS) analysis"""

    @staticmethod
    def calculate_quench_detection_time(
        u_res_df: pd.DataFrame, threshold: float = 0.1, t_discrimination: float = 10e-3
    ) -> float:
        """Method calculating quench detection time based on U_RES quench detection signal. A quench is detected
        if the U_RES signal is larger than the threshold value over a team window greater or equal to the discrimination
        time. The quench detection time is equal to the last time stamp of a window for which the threshold was
        reached.

        :param u_res_df: U_RES quench detection signal
        :param threshold: quench detection voltage threshold
        :param t_discrimination: discrimination time
        :return: quench detection time if the threshold was reached, otherwise NaN
        """
        dt = u_res_df.index[1] - u_res_df.index[0]
        window = math.ceil(t_discrimination / dt) + 1
        quench_detection_df = u_res_df.rolling(window).apply(lambda x: all(abs(x) > threshold), raw=True)
        quench_detection_df.dropna(inplace=True)
        mask = quench_detection_df[quench_detection_df.columns[0]] == 1.0
        qh_trigger_delay = quench_detection_df[mask].index[0] if len(mask) > 0 else np.nan

        print("Quench heater trigger delay is %4.3f s." % (qh_trigger_delay))

        return qh_trigger_delay

    @staticmethod
    def find_start_end_quench_detection(
        u_res_df: pd.DataFrame, threshold: float = 0.1, u_res_min: Optional[float] = None
    ) -> Tuple[float, float, float, float]:
        """Method finding the start and end of the quench detection. After cutting off the first 10 points of the
        signal to avoid initial noise, the first value hitting the threshold is being looked for. That's the end of
        the quench detection. [If the threshold isn't hit at all, then the returned tuples will contain 0's. If all
        signals differ not that much from one another, then a warning is raised and the returned tuple will contain
        the last point in the dataframe.] The median is then calculated on the signal below the threshold. Points
        where the signal crosses the 5 * abs(median) or -5 * abs(median) are afterwards calculated and the last one
        before the end of the quench detection is assumed as a start of the quench detection.

        Note that 5 * abs(median) and -5 * abs(median) thresholds can be overwritten by u_res_min (and - u_res_min)

        Note that the method assumes that after reaching the threshold the signal stays above the threshold.
        In other words, the method does not check whether the quench recovered after reaching the threshold value.

        :param u_res_df: U_RES quench detection signal
        :param threshold: quench detection voltage threshold
        :param u_res_min: voltage threshold used to determine a start of the quench detection. If None (default), the
        5 * abs(signal) is used.
        :return: a four-element tuple indicating the time and voltage for the start of a quench followed by the time
        and voltage of the first point beyond or equal to the threshold. The tuple contains 0s if no quench was found.
        """
        if u_res_df.empty:
            warnings.warn(
                "U_RES signal is empty! Calculation of the start of a quench detection is skipped. "
                "t_start = 0 s returned."
            )
            return 0, 0, 0, 0

        column_name = u_res_df.columns[0]
        if all(u_res_df.std() < 1e-10):
            warnings.warn(f"U_RES signal {column_name} does not change - probably no quench!")
            return u_res_df.index[-1], u_res_df.iloc[-1, 0], u_res_df.index[-1], u_res_df.iloc[-1, 0]

        signal = u_res_df.iloc[10:][column_name]

        above_threshold = signal[abs(signal) >= abs(threshold)].dropna()
        if above_threshold.empty:
            return 0, 0, 0, 0

        t_u_max: float = above_threshold.index[0]
        v_u_max: float = above_threshold.iloc[0]

        below_threshold = signal.loc[:t_u_max]

        if u_res_min is None:
            u_res_min = abs(below_threshold.median()) * 5

        def exceeds_u_res_min(points: Tuple[float, float]):
            return (points[0] <= u_res_min < points[1]) or (points[0] >= -u_res_min > points[1])

        condition = below_threshold.rolling(window=2).apply(exceeds_u_res_min, raw=True).shift(-1)
        t_u_min: float = condition[condition == 1.0].index[-1]
        v_u_min: float = signal.loc[t_u_min]
        return t_u_min, v_u_min, t_u_max, v_u_max

    @check_dataframe_empty(mode="all", warning="U_RES signal is empty, analysis skipped!")
    def calculate_u_res_slope(
        self,
        u_res_df: pd.DataFrame,
        col_name: str = "dU_RES_dt",
        threshold: float = 0.1,
        u_res_min: Optional[float] = None,
    ) -> pd.DataFrame:
        """Method calculating and plotting the slope of the resistive voltage increase
        If any input signal is missing, then the analysis is skipped.
        The calculated voltage derivative is set in the results table.

        :param circuit_name: name of the analyzed circuit (for the title)
        :param timestamp_qps: timestamp of the QPS PM event (for the title)
        :param u_res_df: resistive voltage signal
        :param col_name: name of a column to set the output results table
        :param threshold: value of the quench detection threshold (in V)
        :param u_res_min: voltage threshold used to determine a start of the quench detection. If None, the default
        5 * median is used
        """

        t_u_min, v_u_min, t_u_max, v_u_max = QdsAnalysis.find_start_end_quench_detection(
            u_res_df, threshold=threshold, u_res_min=u_res_min
        )

        # Calculate dU_RES/dt
        if t_u_max > t_u_min:
            d_u_res_dt = (v_u_max - v_u_min) / (t_u_max - t_u_min)
        else:
            d_u_res_dt = float("nan")

        print("%s = %4.3f V/s." % (col_name, d_u_res_dt))

        # Store analysis result
        if self.results_table is not None:
            self.results_table[col_name] = d_u_res_dt

        return pd.DataFrame(data=[v_u_min, v_u_max], index=[t_u_min, t_u_max], columns=["U_RES_slope"])

    @staticmethod
    @check_dataframe_empty(mode="all", warning="Either U_RES or I_MEAS signal is empty, analysis skipped!")
    def plot_u_res(
        circuit_name: str, timestamp_qps: int, u_res_nxcals_df: pd.DataFrame, i_meas_nxcals_df: pd.DataFrame
    ) -> None:
        """Method plotting the resistive voltage of Board A and B prior to a quench event
        If the input signal is missing, then the analysis is skipped.

        :param circuit_name: name of the analyzed circuit (for the title)
        :param timestamp_qps: timestamp of the QPS PM event (for the title)
        :param u_res_nxcals_df: resistive voltage signal
        :param i_meas_nxcals_df: measured power converter current
        """
        title = create_title(circuit_name, timestamp_qps, "U_RES")

        PlotBuilder().with_signal(u_res_nxcals_df, title=title, grid=True).with_ylabel(ylabel="U_RES, [V]").with_ylim(
            [-0.25, 0.25]
        ).with_axhspan(ymin=-0.1, ymax=+0.1, facecolor="xkcd:yellowgreen").with_signal(i_meas_nxcals_df).with_ylabel(
            ylabel="I_MEAS, [A]"
        ).plot()

    @staticmethod
    @check_dataframe_empty(mode="any", warning="At least one DataFrame is empty, plot of U_RES, U_1, U2 skipped!")
    def plot_u_res_u_res_slope_u_1_u_2(
        circuit_name: str,
        timestamp_qds: int,
        u_res_df: pd.DataFrame,
        u_res_slope_df: pd.DataFrame,
        u_1_df: pd.DataFrame,
        u_2_df: pd.DataFrame,
        suffix: str = "",
        xlim: Tuple[float, float] = (np.nan, np.nan),
        ylim_left: Tuple[float, float] = (np.nan, np.nan),
        ylim_right: Tuple[float, float] = (np.nan, np.nan),
    ):
        """Method plotting U_RES, its slope along with U_1 and U_2 signals used for quench detection.

        :param circuit_name: name of the analyzed circuit (for the title)
        :param timestamp_qds: timestamp of the QDS PM event (for the title)
        :param u_res_df: quench detection voltage
        :param u_res_slope_df: initial slope of the voltage
        :param u_1_df: voltage of the first aperture used for comparison
        :param u_2_df: voltage of the second aperture used for comparison
        :param xlim: x axis limit
        :param suffix: magnet name, e.g., '_Q1'
        :param ylim_left: y axis limit (left)
        :param ylim_right: y axis limit (right)
        """
        title = create_title(
            circuit_name, timestamp_qds, ["U_RES" + suffix, "U_RES_slope" + suffix, "U_1" + suffix, "U_2" + suffix]
        )
        PlotBuilder().with_signal(
            [u_res_df, u_res_slope_df], title=title, figsize=(13, 6.5), grid=True, marker="."
        ).with_ylabel(ylabel="U_RES, [V]").with_xlim(xlim).with_ylim(ylim_left).with_signal(
            [u_1_df, u_2_df]
        ).with_ylabel(
            ylabel="U_1, U_2, [V]"
        ).with_ylim(
            ylim_right
        ).plot()

    @check_dataframe_empty(mode="all", warning="All DataFrames are empty, search for the quench origin is skipped!")
    def find_quench_origin(self, u_res_dfs: List[pd.DataFrame], n_points_to_skip=20, threshold: float = 0.1) -> None:
        """Method finding an origin of a quench in magnets. First it finds the moment when the detection voltage
        reaches the threshold of 0.1 V. If the threshold is not reached, then the last index of the signal is returned.
        The quench origin is selected as the first magnet to reach the threshold.
        The name of the quench origin is printed out and saved to the results table.

        :param u_res_dfs: list of quench detection voltages
        :param n_points_to_skip: number of initial points to skip
        :param threshold: threshold (default 0.1)
        """

        def find_time_quench_detection(u_res_df: pd.DataFrame) -> float:
            # Take absolute value
            u_res_abs_df = abs(u_res_df)

            # Find first index of argmax (threshold)
            if u_res_abs_df[u_res_abs_df >= threshold].dropna().empty:
                return u_res_abs_df.index[-1]
            else:
                return u_res_abs_df[u_res_abs_df >= threshold].dropna().index[0]

        # Skip first 20 points
        u_res_dfs = [
            u_res_df.drop(u_res_df.head(n_points_to_skip).index) for u_res_df in u_res_dfs if not u_res_df.empty
        ]

        t_u_maxs = [find_time_quench_detection(u_res_df) for u_res_df in u_res_dfs]

        # Put into a dict
        q_to_t_u_max = {u_res_df.columns[0].split(":")[-1]: t_u_max for u_res_df, t_u_max in zip(u_res_dfs, t_u_maxs)}

        # Sort the dictionary based on value
        q_to_t_u_max_sorted = {k: v for k, v in sorted(q_to_t_u_max.items(), key=lambda item: item[1])}

        # Quench origin is the first element in the dictionary sorted in the ascending order
        quench_origin = list(q_to_t_u_max_sorted.keys())[0].split("_")[-1]
        print("Quench origin is %s." % quench_origin)
        if self.results_table is not None:
            self.results_table["Quench origin"] = quench_origin

    @staticmethod
    @check_dataframe_empty(mode="all", warning="All DataFrames are empty, search for the quench origin is skipped!")
    def find_voltage_threshold_detection(u_dfs: List[pd.DataFrame], threshold: float) -> None:
        """Method finding and printing a timestamp for which the voltage detection threshold has been reached

        :param u_dfs: list of voltage signals
        :param threshold: voltage threshold

        """
        t_detect = []
        for u_df in u_dfs:
            if any(u_df.values > threshold):
                t_detect.append(u_df[u_df.values > threshold].index[0])

        if t_detect:
            print("U_HTS signal threshold detected at t = %4.3f s." % max(t_detect))


# RB
def get_max_diff_val_and_idx(
    u_nqps_diff_dfs: List[pd.DataFrame], t_st_magnet_ok: float, quenched_magnets: pd.Series
) -> Tuple[float, float]:
    """Function calculating maximum difference between nQPS detection signals (differences of U_DIODE voltages) and
    returning its value and index. This function is used to analyze nQPS detection.

    :param u_nqps_diff_dfs:
    :param t_st_magnet_ok:
    :param quenched_magnets:
    :return:
    """
    # take only certain period of time
    # # a fix in case after t_st_magnet_ok QH resulted in voltage increase beyond the threshold
    u_nqps_diff_slice_dfs = []
    for u_nqps_diff_df in u_nqps_diff_dfs:
        mask = (u_nqps_diff_df.index > t_st_magnet_ok - 0.2) & (u_nqps_diff_df.index < t_st_magnet_ok)
        u_nqps_diff_slice_dfs.append(u_nqps_diff_df[mask])

    if all([u_nqps_diff_slice_df.empty for u_nqps_diff_slice_df in u_nqps_diff_slice_dfs]):
        return -math.inf, 0

    u_nqps_diff_no_quench_dfs = []
    for u_nqps_diff_slice_df in u_nqps_diff_slice_dfs:
        col = u_nqps_diff_slice_df.columns[0]
        if "U_REF" in col or not all([quenched_magnet in col for quenched_magnet in quenched_magnets]):
            u_nqps_diff_no_quench_dfs.append(u_nqps_diff_slice_df)

    max_idx = [u_nqps_diff_no_quench_df.max(axis=1).idxmax() for u_nqps_diff_no_quench_df in u_nqps_diff_no_quench_dfs]
    max_val = [u_nqps_diff_no_quench_df.max(axis=0).max() for u_nqps_diff_no_quench_df in u_nqps_diff_no_quench_dfs]
    idx_max_val = max_val.index(max(max_val))
    return max_idx[idx_max_val], max_val[idx_max_val]


def analyze_qds_trigger(
    u_qs0_df: pd.DataFrame, st_magnet_ok_df: pd.DataFrame, st_nqd0_df: pd.DataFrame
) -> pd.DataFrame:
    """Function analyzing QDS trigger signal based on U_QS0, ST_MAGNET_OK, ST_NQD0.
    It returns a DataFrame with the following columns:
    - t_st_nqd0 - time when ST_NQD0 reaches 1
    - t_st_magnet_ok - time when ST_MAGNET_OK reaches 1
    - u_st_nqd0 - U_QS0 voltage when ST_NQD0 reaches 1
    - t_start_quench - moment when U_QS0 is greater than 15 mV
    - du_dt - slope of U_QS0 increase

    :param u_qs0_df: U_QS0 signal
    :param st_magnet_ok_df: ST_MAGNET_OK signal
    :param st_nqd0_df: ST_NQD0 signal
    :return: pd.DataFrame with QDS trigger parameters
    """

    def empty_result(comment):
        return format_result(0, 0, math.nan, 0, 0, math.nan, comment)

    def format_result(
        result_t_st_nqd0,
        result_t_st_magnet_ok,
        result_u_start_quench,
        result_t_start_quench,
        result_du_dt,
        result_t_delay_qh_trigger,
        result_analysis_comment="",
    ):
        COLUMN_NAMES = [
            "t_st_nqd0 [s]",
            "t_st_magnet_ok [s]",
            "u_st_nqd0 [V]",
            "t_start_quench [s]",
            "du_dt [V/s]",
            "t_delay_qh_trigger [s]",
            "analysis_comment",
        ]
        value = [
            result_t_st_nqd0,
            result_t_st_magnet_ok,
            result_u_start_quench,
            result_t_start_quench,
            result_du_dt,
            result_t_delay_qh_trigger,
            result_analysis_comment,
        ]
        return pd.DataFrame(columns=COLUMN_NAMES, data=[value])

    t_st_magnet_ok = _get_t_st_magnet_ok(st_magnet_ok_df)

    if not t_st_magnet_ok:
        analysis_comment = "Seems that the heaters were triggered by nQPS"
        return empty_result(analysis_comment)

    if (st_nqd0_df.values < 1.0).all():
        warning_comment = "ST_NQD0 signal never reaches 1. (QDS from the first board)"
        warnings.warn(warning_comment)
        return empty_result(warning_comment)

    t_st_nqd0 = _get_t_st_nqd0(st_nqd0_df, t_st_magnet_ok)

    if not t_st_nqd0:
        warning_comment = "No quench developing part found in the buffer."
        warnings.warn(warning_comment)
        return empty_result(warning_comment)

    min_t_start_quench = t_st_magnet_ok - 0.2

    if min(abs(u_qs0_df).values)[0] <= 0.01:
        u_qs0_before_quench_df = u_qs0_df[(u_qs0_df.index > st_nqd0_df.index[10]) & (u_qs0_df.index < t_st_nqd0)]

        mask = (abs(u_qs0_before_quench_df - u_qs0_before_quench_df.values[0]) <= 0.015).values.reshape(
            len(u_qs0_before_quench_df)
        )

        t_start_quench = u_qs0_before_quench_df[mask].index[-1]
        t_start_quench = u_qs0_df.index[u_qs0_df.index.get_loc(t_start_quench) - 1]

        if t_start_quench < min_t_start_quench:
            t_start_quench = min_t_start_quench
    else:
        t_start_quench = min_t_start_quench

    u_start_quench = u_qs0_df.loc[
        u_qs0_df.index[u_qs0_df.index.get_indexer([t_start_quench], method="nearest")[0]]
    ].values[0]

    u_st_nqd0 = u_qs0_df[u_qs0_df.index == t_st_nqd0].values[0][0]
    du_dt = (u_st_nqd0 - u_start_quench) / (t_st_nqd0 - t_start_quench)

    t_delay_qh_trigger = -t_st_magnet_ok

    return format_result(t_st_nqd0, t_st_magnet_ok, u_start_quench, t_start_quench, du_dt, t_delay_qh_trigger)


def _get_t_st_magnet_ok(st_magnet_ok_df: pd.DataFrame) -> Union[float, None]:
    """Returns time when ST_MAGNET_OK reaches 1 or None if not found.
    :param st_magnet_ok_df: ST_MAGNET_OK signal
    """
    if len(st_magnet_ok_df.index) <= 10:
        return None

    mask = (st_magnet_ok_df.index > st_magnet_ok_df.index[10]) & (st_magnet_ok_df.values == False).reshape(
        len(st_magnet_ok_df)
    )
    st_qds_sub_df = st_magnet_ok_df[mask]
    return st_qds_sub_df.index[0] if len(st_qds_sub_df.index) > 0 else None


def _get_t_st_nqd0(st_nqd0_df: pd.DataFrame, t_st_magnet_ok: float) -> Union[pd.DataFrame, None]:
    """Returns time when ST_NQD0 reaches 1 or None if not found.
    :param st_nqd0_df: ST_NQD0 signal
    :param t_st_magnet_ok: time when ST_MAGNET_OK reaches 1
    """
    mask = (
        (st_nqd0_df.index > st_nqd0_df.index[10])
        & (st_nqd0_df.index < t_st_magnet_ok)
        & (st_nqd0_df.values == True).reshape(len(st_nqd0_df))
    )
    t_st_nqd = st_nqd0_df[mask]
    if len(t_st_nqd.index) == 0:
        return None
    t_st_nqd0 = t_st_nqd.index[-1]
    return st_nqd0_df.index[st_nqd0_df.index.get_loc(t_st_nqd0) + 1]


def _contains_quench_developing_part(st_magnet_ok_df: pd.DataFrame, st_nqd0_df: pd.DataFrame) -> bool:
    """Checks if PM buffer data is not corrupted and contains quench developing part
    :param st_magnet_ok_df: ST_MAGNET_OK signal
    :param st_nqd0_df: ST_NQD0 signal
    """
    t_st_magnet_ok = _get_t_st_magnet_ok(st_magnet_ok_df)
    return t_st_magnet_ok is not None and _get_t_st_nqd0(st_nqd0_df, t_st_magnet_ok) is not None


def find_dfs_with_quenched_magnet(u_nqps_shift_diff_dfs: List[pd.DataFrame], source_qds: str) -> List[pd.DataFrame]:
    """Function finding a quenched magnet in list of nQPS detection signals (U_DIODE differences)

    :param u_nqps_shift_diff_dfs:
    :param source_qds: name of a QDS source
    :return: list of nQPS detection signals in which the QDS source is present
    """
    u_nqps_shift_diff_filt_dfs = []
    for u_nqps_shift_diff_df in u_nqps_shift_diff_dfs:
        if source_qds in u_nqps_shift_diff_df.columns.values[0]:
            u_nqps_shift_diff_filt_dfs.append(u_nqps_shift_diff_df)
    return u_nqps_shift_diff_filt_dfs


def plot_u_diode_nxcals(u_nqps_dfs: List[pd.DataFrame], i_meas_df=None, ax=None, xlim=None) -> None:
    """Function plotting U_DIODE signals from NXCALS with a main power converter current I_MEAS

    :param u_nqps_dfs: list of nQPS voltages
    :param i_meas_df: main power converter current
    :param ax: axis on which the plot is displayed
    :param xlim: limits of x-axis
    :return: None
    """

    if len(u_nqps_dfs) == 0:
        warnings.warn("U_DIODE signals from NXCALS not plotted because the list of nQPS voltages is empty.")

    for u_nqps_df in u_nqps_dfs:
        if not u_nqps_df.empty:
            u_nqps_df.plot(ax=ax, title="nQPS(NXCALS)", grid=True, marker="o", ms=1)

    ax.title.set_size(20)
    ax.legend(loc="lower left")
    ax.set_xlabel("time, [s]", fontsize=15)
    ax.set_ylabel("U_DIODE_RB, [V]", fontsize=15)
    ax.tick_params(labelsize=15)

    if xlim is not None:
        ax.set_xlim(xlim)

    if i_meas_df is not None:
        ax2 = ax.twinx()
        i_meas_df.plot(ax=ax2, color="C4")
        ax2.legend(loc="upper right")
        ax2.tick_params(labelsize=15)
        ax2.set_ylabel("I_MEAS, [A]", color="C4", fontsize=15)

    ax.grid(True)


def plot_u_diode_diff_pm(
    u_nqps_diff_dfs: List[pd.DataFrame], t_st_magnet_ok=None, t_shift_qds=0, ax=None, xlim=(0, 2)
) -> None:
    """Function plotting nQPS detection signals (differences of U_DIODE) from PM

    :param u_nqps_diff_dfs: list of nQPS U_DIODE differences
    :param t_st_magnet_ok: time when ST_MAGNET_OK reaches 1
    :param t_shift_qds: QDS time shift in s
    :param ax: axis on which the plot is displayed
    :param xlim: limits of x-axis
    :return: None
    """

    for u_nqps_diff_df in u_nqps_diff_dfs:
        u_nqps_diff_df.plot(ax=ax, grid=True, marker="o", ms=1)

    ax.axvspan(xmin=-0.2 + t_shift_qds, xmax=t_st_magnet_ok, ymin=0, ymax=0.5, facecolor="xkcd:yellowgreen")
    ax.set_ylabel("U (Calculated diode differences), [V]", fontsize=15)
    ax.legend(loc="lower left")
    ax.set_xlabel("time, [s]", fontsize=15)
    if t_st_magnet_ok is not None:
        ax.set_xlim(t_st_magnet_ok - 0.2, t_st_magnet_ok + 0.1)
    if xlim is not None:
        ax.set_ylim(xlim)
    ax.tick_params(labelsize=15)


def plot_u_diode_diff_pm_value_range(u_nqps_diff_dfs: List[pd.DataFrame], ax=None, value_range=()) -> None:
    """Function plotting nQPS detection signals (differences of U_DIODE) from PM with an optional value range

    :param u_nqps_diff_dfs: list of nQPS U_DIODE differences
    :param ax: axis on which the plot is displayed
    :param value_range: range of allowed values for a box
    :return: None
    """

    for u_nqps_diff_df in u_nqps_diff_dfs:
        u_nqps_diff_df.plot(ax=ax, title="nQPS (Calculated diode differences)", grid=True, marker="o", ms=1)

    ax.title.set_size(20)
    ax.legend(loc="lower left")
    ax.set_xlabel("time, [s]", fontsize=15)
    ax.set_ylabel("U (Calculated diode differences), [V]", fontsize=15)
    ax.tick_params(labelsize=15)

    if value_range:
        ax.axhspan(ymin=value_range[0], ymax=value_range[1], color="xkcd:yellowgreen")


def plot_u_diode_pm_nqds(u_nqps_dfs: List[pd.DataFrame], ax=None) -> None:
    """Function plotting nQPS signals U_DIODE from PM

    :param u_nqps_dfs:
    :param ax: axis on which the plot is displayed
    :return: None
    """

    for u_nqps_df in u_nqps_dfs:
        u_nqps_df.plot(ax=ax, title="nQPS(PM)", grid=True, marker="o", ms=1)

    ax.title.set_size(20)
    ax.legend(loc="lower left")
    ax.set_xlabel("time, [s]", fontsize=15)
    ax.set_ylabel("U_DIODE_RB, [V]", fontsize=15)
    ax.tick_params(labelsize=15)


def _get_magnet_name(signal_name: str) -> str:
    """Function returning magnet name from an nQPS signal name

    :param signal_name: nQPS signal name (either U_DIODE or U_REF)
    :return: magnet name from a signal name
    """
    magnet_name = signal_name.split(".")[1].split(":")[0]
    if "U_REF" in signal_name:
        return magnet_name + ":U_REF"
    else:
        return magnet_name


def calculate_col_diff(u_diode_ref_dfs: List[pd.DataFrame], is_abs=True, is_avg=False) -> List[pd.DataFrame]:
    """Function calculating all pair-wise difference for an input list of signals

    :param u_diode_ref_dfs: List of (typically nQPS) signals for which all pair-wise differences are calculated
    :param is_abs: flag denoting whether to take an absolute value of the difference (if flag is set to True)
    :param is_avg: flag denoting whether to apply a moving averate to the difference (if flag is set to True)
    :return:
    """
    diff_dfs = []
    for index, df_first in enumerate(u_diode_ref_dfs):
        magnet_first = _get_magnet_name(df_first.columns[0])
        for df_second in u_diode_ref_dfs[index + 1 :]:
            magnet_second = _get_magnet_name(df_second.columns[0])
            diff_name = "{}-{}".format(magnet_first, magnet_second)

            value = abs(df_first.values - df_second.values) if is_abs else df_first.values - df_second.values

            time = df_first.index
            diff_df = pd.DataFrame(index=time, data=value, columns=[diff_name])
            # take an absolute
            diff_df = diff_df.abs() if is_abs else diff_df
            # calculate 20-point moving average
            diff_df = diff_df.rolling(20).mean() if is_avg else diff_df
            diff_dfs.append(diff_df)

    return diff_dfs


def synchronize_nqps_to_iqps(
    u_diode_ref_dfs: List[pd.DataFrame], timestamp_nqps: int, timestamp_qds: int, t_shift_qds: float = 0
) -> List[pd.DataFrame]:
    """Function synchronizing nQPS signals to iQPS timestamps

    :param u_diode_ref_dfs: list of signals to synchronize
    :param timestamp_nqps: nQPS PM timestamp
    :param timestamp_qds: iQPS PM timestamp
    :param t_shift_qds: QDS time shift in s
    :return: synchronized list of signals
    """
    u_nqps_shift_dfs = deepcopy(u_diode_ref_dfs)
    for u_nqps_shift_df in u_nqps_shift_dfs:
        if not math.isnan(timestamp_nqps):
            u_nqps_shift_df.index = u_nqps_shift_df.index + (timestamp_nqps - timestamp_qds) * 1e-9 + t_shift_qds
        else:
            u_nqps_shift_df.index = u_nqps_shift_df.index + 10

    return u_nqps_shift_dfs


def plot_u_qds(
    u_qs0_df: pd.DataFrame,
    u_qs0_filtered_df: pd.DataFrame,
    u_1_df: pd.DataFrame,
    u_2_df: pd.DataFrame,
    timestamp_qds: int,
    timestamp_pic: int,
    *,
    xmin: float,
    xmax: float,
    xlim: float,
    ax=None,
    threshold: float = 0.1,
) -> None:
    """Method plotting analog QDS signals with acceptance ranges

    :param u_qs0_df: U_QS0 signal
    :param u_qs0_filtered_df: U_QS0 signal (filtered)
    :param u_1_df: U_1 signal
    :param u_2_df: U_2 signal
    :param timestamp_qds: QDS PM timestamp (for detection line)
    :param timestamp_pic: PIC NXCALS timestamp (for detection line)
    :param xmin: minimum value on x-axis for a box with quench signal increase
    :param xmax: maximum value on x-axis for a box with quench signal increase
    :param xlim: limits of x-axis
    :param ax: axis on which the plot is displayed
    :param threshold: y-scale threshold for the quench detection.
    :return: None
    """
    u_qs0_df.plot(
        ax=ax, title="iQPS PM (U_1, U_2, U_QS0, zoom)", marker="o", ms=2, color="C0", grid=True, xlim=xlim, legend=True
    )
    u_qs0_filtered_df.plot(ax=ax, color="C1", marker="o", ms=2, grid=True)

    ax.title.set_size(20)
    ax.axvline(x=timestamp_pic - timestamp_qds * 1e-9, color="green", linestyle="--", linewidth=1.5)
    ax.legend(["U_QS0", "U_QS0 (filtered)"], loc="lower left")
    ax.set_ylabel("U_QS0, [V]", color="C0", fontsize=15)
    ax.set_xlabel("time, [s]", fontsize=15)
    ax.tick_params(labelsize=15)
    ax.axhspan(ymin=-threshold, ymax=+threshold, facecolor="xkcd:yellowgreen")
    ax.axvspan(xmin=xmin, xmax=xmax, facecolor="xkcd:goldenrod")
    ax.set_ylim((-0.25 * threshold / 0.1, 0.25 * threshold / 0.1))

    ax2 = ax.twinx()
    u_1_df.plot(ax=ax2, color="C2", marker="o", ms=2, xlim=xlim)
    u_2_df.plot(ax=ax2, color="C3", marker="o", ms=2, xlim=xlim)
    ax2.legend(["U_1", "U_2"], loc="upper right")
    ax2.set_ylabel("U_1, U_2, [V]", fontsize=15)
    ax2.set_xlabel("time, [s]", fontsize=15)
    ax2.tick_params(labelsize=15)


def plot_st_qds(
    u_qs0_df: pd.DataFrame,
    st_magnet_ok_df: pd.DataFrame,
    st_nqd0_df: pd.DataFrame,
    *,
    xmin: float,
    xmax: float,
    xlim: Tuple[float, float],
    ax=None,
    threshold: float = 0.1,
) -> None:
    """Method plotting digital QDS signals with acceptance ranges

    :param u_qs0_df: U_QS0 signal
    :param st_magnet_ok_df: ST_MAGNET_OK signal
    :param st_nqd0_df:  ST_NQD0 signal
    :param xmin: minimum value on x-axis for a box with quench signal increase
    :param xmax: maximum value on x-axis for a box with quench signal increase
    :param xlim: limits of x-axis
    :param ax: axis on which the plot is displayed
    :param threshold: y-scale threshold for the quench detection.
    :return: None
    """
    u_qs0_df.plot(
        ax=ax, title="iQPS PM (U_QS0, ST_NQD0, ST_MAGNET_OK, zoom)", style="o-", ms=2, grid=True, xlim=xlim, legend=True
    )

    ax.title.set_size(20)
    ax.legend(["U_QS0"], loc="lower left")
    ax.set_xlabel("time, [s]", fontsize=15)
    ax.set_ylabel("U_QS0, [V]", color="C0", fontsize=15)
    ax.tick_params(labelsize=15)
    ax.axhspan(ymin=-threshold, ymax=+threshold, facecolor="xkcd:yellowgreen")  # beige
    ax.axvspan(xmin=xmin, xmax=xmax, facecolor="xkcd:goldenrod")
    ax.set_ylim((-0.25 * threshold / 0.1, 0.25 * threshold / 0.1))

    ax2 = ax.twinx()
    st_nqd0_df.astype(int).plot(
        ax=ax2, yticks=[0, 1], ylim=[-0.1, 1.1], style="bx-", ms=2, drawstyle="steps-post", xlim=xlim, grid=True
    )
    st_magnet_ok_df.astype(int).plot(
        ax=ax2, yticks=[0, 1], ylim=[-0.1, 1.1], drawstyle="steps-post", style="ro-", ms=2, xlim=xlim, grid=True
    )

    ax2.set_ylabel("ST_NDQ0, ST_MAGNET_OK, [-]", fontsize=15)
    ax2.legend(["ST_NDQ0", "ST_MAGNET_OK"], loc="upper right")
    ax2.tick_params(labelsize=15)


@check_dataframe_empty(
    mode="any", warning="u_qs0_df or u_qs0_df2 is empty. Plot analog U_QS0 signals for board A and B skipped"
)
def plot_iqps_u_qs0_AB(u_qs0_df: pd.DataFrame, u_qs0_df2: pd.DataFrame, title: str, t_shift_qds: float) -> None:
    """Method plotting analog U_QS0 signals for board A and B

    :param u_qs0_df: U_QS0 signal for 1st board
    :param u_qs0_df2: U_QS0 signal for 2nd board
    :param title: plot title
    :param t_shift_qds: QDS time shift in s
    :return: None
    """
    fig, ax = plt.subplots(1, figsize=(21, 7))
    xmin = -1.0 + t_shift_qds
    xmax = xmin + 2.0
    xlim = (xmin, xmax)
    u_qs0_df.iloc[:, 0].plot(ax=ax, title=title, marker="o", ms=3, color="C0", grid=True, legend=True, xlim=xlim)
    u_qs0_df2.iloc[:, 0].plot(ax=ax, color="C1", marker="o", ms=3, grid=True, legend=True, xlim=xlim)
    fig.suptitle(title, fontsize=20)
    plt.show()
    correlation = (
        pd.concat([u_qs0_df.iloc[:, 0], u_qs0_df2.iloc[:, 0]], axis=1, keys=["df", "df2"]).corr().loc["df2", "df"]
    )
    print("Time shift of QDS w.r.t. timestamp of PM =", t_shift_qds, "s")
    print("Boards A/B Correlation = %5.3f" % (correlation))
    print("By default, 1st in time PM buffer will be used for the analysis")
    if correlation < 0.9:
        warnings.warn("WARNING: Boards A/B correlation is less than 0.9!")


class QdsRbAnalysis(QdsAnalysis):
    """Class for quench detection system (QDS) analysis in RB circuit"""

    def analyze_qds(
        self,
        timestamp_fgc: int,
        timestamp_pic: int,
        u_qds_dfs: List[List[pd.DataFrame]],
        u_qds_dfs2: List[List[pd.DataFrame]],
        u_nqps_dfs: List[List[pd.DataFrame]],
        i_meas_df: pd.DataFrame,
        threshold: float = 0.1,
    ) -> None:
        """Method analyzing QDS signals in an RB circuit. It plots all relevant signals as well as calculates
        key features characterizing a quench detection process.

        :param timestamp_fgc: FGC PM timestamp (ns precision)
        :param timestamp_pic: PIC NXCALS timestamp (ns precision)
        :param u_qds_dfs: list of list of iQPS analog and digital signals (from PM)
        :param u_qds_dfs2: list of list of iQPS analog and digital signals (from PM) - from second board (A/B)
        :param u_nqps_dfs: list of list of nQPS analog signals (from PM and NXCALS)
        :param i_meas_df: main power converter current
        :param threshold: y-scale threshold for the quench detection.
        :return: None
        """

        def get_filtered_signals(
            u_qds_df: List[pd.DataFrame], rolling_window: int
        ) -> Tuple[pd.DataFrame, pd.DataFrame, pd.DataFrame, pd.DataFrame, pd.DataFrame]:
            u_qs0, u_1, u_2, st_nqd0, st_magnet_ok = u_qds_df

            u_qs0 = u_qs0.rolling(window=rolling_window, min_periods=1, center=True).median()
            u_1 = u_1.rolling(window=rolling_window, min_periods=1).median()
            u_2 = u_2.rolling(window=rolling_window, min_periods=1).median()
            st_nqd0 = st_nqd0.rolling(window=rolling_window, min_periods=1).median()
            st_magnet_ok = st_magnet_ok.rolling(window=rolling_window, min_periods=1).median()

            return u_qs0, u_1, u_2, st_nqd0, st_magnet_ok

        index_max = self.results_table.index[-1] if not self.results_table.empty else 0
        for index, row in self.results_table.iterrows():
            source_qds, source_nqps, timestamp_qds, timestamp_nqps, i_quench, circuit_name = row.reindex(
                ["Position", "nQPS crate name", "timestamp_iqps", "timestamp_nqps", "I_Q_M", "Circuit Name"]
            )

            print("-------------------------------------------------------------------------------")
            print(
                "{}/{}: Analysing quench detection signals of {} on {}, {}".format(
                    index, self.results_table.index[-1], source_qds, Time.to_string(timestamp_qds), timestamp_qds
                )
            )
            print("-------------------------------------------------------------------------------")

            u_qs0_df, u_1_df, u_2_df, st_nqd0_df, st_magnet_ok_df = u_qds_dfs[index]
            # filter u_qs0
            window = 3
            u_qs0_filtered_df, u_1_df, u_2_df, st_nqd0_filtered_df, st_magnet_ok_filtered_df = get_filtered_signals(
                u_qds_dfs[index], window
            )
            (u_qs0_filtered_df2, u_1_df2, u_2_df2, st_nqd0_filtered_df2, st_magnet_ok_filtered_df2) = (
                get_filtered_signals(u_qds_dfs2[index], window)
            )

            # Analysis
            if _contains_quench_developing_part(st_magnet_ok_filtered_df, st_nqd0_filtered_df):
                report_qds_trigger = analyze_qds_trigger(
                    u_qs0_filtered_df, st_magnet_ok_filtered_df, st_nqd0_filtered_df
                )
                t_shift_qds = u_qs0_filtered_df.index[0] + 1.198
            else:
                warnings.warn("No quench developing part found in the first buffer. Checking the second one.")
                report_qds_trigger = analyze_qds_trigger(
                    u_qs0_filtered_df2, st_magnet_ok_filtered_df2, st_nqd0_filtered_df2
                )
                t_shift_qds = u_qs0_filtered_df2.index[0] + 1.198

            t_start_quench = report_qds_trigger.at[0, "t_start_quench [s]"]
            t_st_nqd0 = report_qds_trigger.at[0, "t_st_nqd0 [s]"]
            t_st_magnet_ok = report_qds_trigger.at[0, "t_st_magnet_ok [s]"]

            # # iQPS
            dt_from_first_quench = (
                timestamp_qds - self.results_table.iloc[0].get("timestamp_iqps", default=np.nan)
            ) / 1e9

            title = "Magnet: {}, iQPS Time Stamp: {}, Current = {} A".format(
                source_qds, Time.to_string_short(timestamp_qds), i_quench
            )

            plot_iqps_u_qs0_AB(u_qs0_filtered_df, u_qs0_filtered_df2, title, t_shift_qds)

            title = (
                "Magnet: {}, iQPS Time Stamp: {}, nQPS Crate: {}, Current = {} A, Time from the 1st quench {} s".format(
                    source_qds, Time.to_string_short(timestamp_qds), source_nqps, i_quench, dt_from_first_quench
                )
            )
            fig, ax = plt.subplots(2, 2, figsize=(30, 15))
            fig.subplots_adjust(hspace=0.2)
            fig.suptitle(title, fontsize=20)

            if t_start_quench < t_st_magnet_ok:
                xlim = (t_st_magnet_ok - 0.25, t_st_magnet_ok + 0.25)
            else:
                xlim = (t_st_magnet_ok - 1.0, t_st_magnet_ok + 2.0)
            plot_u_qds(
                u_qs0_df,
                u_qs0_filtered_df,
                u_1_df,
                u_2_df,
                timestamp_qds,
                timestamp_pic,
                xmin=t_start_quench,
                xmax=t_st_nqd0,
                xlim=xlim,
                ax=ax[0, 0],
                threshold=threshold,
            )
            plot_st_qds(
                u_qs0_df,
                st_magnet_ok_df,
                st_nqd0_df,
                xmin=t_start_quench,
                xmax=t_st_nqd0,
                xlim=xlim,
                ax=ax[1, 0],
                threshold=threshold,
            )

            if 1e-9 * (timestamp_qds - timestamp_fgc) < 2:
                u_nqps_shift_df = synchronize_nqps_to_iqps(
                    u_nqps_dfs[index], timestamp_nqps, timestamp_qds, t_shift_qds
                )
                u_nqps_shift_df = [
                    u_nqps_shift_df_el for u_nqps_shift_df_el in u_nqps_shift_df if not u_nqps_shift_df_el.empty
                ]
                u_nqps_shift_diff_df = calculate_col_diff(u_nqps_shift_df)
                quenched_magnets = self.results_table.loc[: index - 1, "Position"].values
                u_nqps_shift_diff_filt_dfs = find_dfs_with_quenched_magnet(u_nqps_shift_diff_df, source_qds)
                t_nqps_diff_max, u_nqps_diff_max = get_max_diff_val_and_idx(
                    u_nqps_shift_diff_filt_dfs, t_st_magnet_ok, quenched_magnets
                )

                plot_u_diode_pm_nqds(u_nqps_shift_df, ax=ax[0, 1])
                # Plot only the differences with the quenched magnet
                if u_nqps_shift_diff_filt_dfs:
                    plot_u_diode_diff_pm(u_nqps_shift_diff_filt_dfs, t_st_magnet_ok, t_shift_qds, ax=ax[1, 1])
                else:
                    ax[1, 1].axis("off")
            else:
                u_nqps_shift_df = u_nqps_dfs[index] if len(u_nqps_dfs) > index else []

                i_meas_shift_df = deepcopy(i_meas_df)
                i_meas_shift_df.index = i_meas_shift_df.index + (timestamp_fgc - timestamp_qds) * 1e-9
                plot_u_diode_nxcals(u_nqps_shift_df, i_meas_df=i_meas_shift_df, ax=ax[0, 1])
                plot_u_diode_nxcals(u_nqps_shift_df, i_meas_df=None, ax=ax[1, 1], xlim=[-2, 2])

                t_nqps_diff_max = math.nan
                u_nqps_diff_max = math.nan

            ax[0, 1].xaxis.set_tick_params(which="both", labelbottom=True)
            plt.show()

            report_qds_trigger["t_nqps_diff_max [s]"] = t_nqps_diff_max
            report_qds_trigger["u_nqps_diff_max [V]"] = u_nqps_diff_max
            display(HTML(report_qds_trigger.to_html()))

            du_dt = report_qds_trigger.at[0, "du_dt [V/s]"]
            self.results_table.at[index, "dU_iQPS/dt"] = du_dt

            if circuit_name.split(".")[1] in ["A12", "A56", "A67", "A78"]:
                quench_origin = "EXT" if du_dt > 0 else "INT"
            else:
                quench_origin = "INT" if du_dt > 0 else "EXT"

            self.results_table["Quench origin"] = self.results_table["Quench origin"].astype(str)
            self.results_table.at[index, "Quench origin"] = quench_origin

            if not self.is_automatic:
                self.results_table.at[index, "Type of Quench"] = get_expert_decision(
                    "What is the quench type",
                    [
                        "Training",
                        "Heater-provoked",
                        "Beam-induced",
                        "GHe propagation",
                        "QPS crate reset",
                        "Single Event Upset",
                        "Short-to-ground",
                        "EM disturbance",
                        "nQPS lost reference",
                        "Unknown",
                    ],
                )
                self.results_table.at[index, "QDS trigger origin"] = get_expert_decision(
                    "What is the QDS trigger origin", ["iQPS", "nQPS"]
                )

            # Show next
            if check_show_next(index, index_max, self.is_automatic):
                break

    @staticmethod
    def calc_plot_nqps_sunglass(
        source_timestamp_nqps_df: pd.DataFrame, u_nqps_dfs: List[pd.DataFrame], threshold=0.7, scaling=1
    ) -> None:
        """Method plotting and calculating nQPS signal features in order to analyze so called sunglasses settings.
        Calculated features are updated in place for the 'source_timestamp_nqps_df'.

        :param source_timestamp_nqps_df: source timestamp DataFrame (note that source column is renamed to nqps_crate)
        :param u_nqps_dfs: List of list of nQPS signals
        :param threshold: sunglass detection threshold
        :param scaling: threshold scaling according to the current
        :return: None
        """
        source_timestamp_nqps_df["U_max"] = float("nan")
        source_timestamp_nqps_df["U_min"] = float("nan")
        source_timestamp_nqps_df["U_max_pcnt"] = float("nan")
        source_timestamp_nqps_df["U_min_pcnt"] = float("nan")
        scaled_threshold = threshold * scaling
        print("Scaled threshold is equal to: %.3f V." % scaled_threshold)
        for index, row in source_timestamp_nqps_df.iterrows():
            print("Analysing nQPS crate: %s" % row["nqps_crate"])

            u_nqps_df = u_nqps_dfs[index]
            u_nqps_non_empty_df = [u_nqps_df_el for u_nqps_df_el in u_nqps_df if not u_nqps_df_el.empty]
            u_nqps_diff_df = calculate_col_diff(u_nqps_non_empty_df, is_abs=False, is_avg=True)

            fig, ax = plt.subplots(1, 2, figsize=(30, 7))
            plot_u_diode_pm_nqds(u_nqps_non_empty_df, ax=ax[0])
            plot_u_diode_diff_pm_value_range(
                u_nqps_diff_df, ax=ax[1], value_range=(-scaled_threshold, scaled_threshold)
            )
            plt.show()

            u_max = max([u_nqps_diff_df_el.max().values[0] for u_nqps_diff_df_el in u_nqps_diff_df])
            u_min = min([u_nqps_diff_df_el.min().values[0] for u_nqps_diff_df_el in u_nqps_diff_df])
            source_timestamp_nqps_df.loc[index, "U_max"] = u_max
            source_timestamp_nqps_df.loc[index, "U_min"] = u_min
            source_timestamp_nqps_df.loc[index, "U_max_pcnt"] = u_max / scaled_threshold * 100
            source_timestamp_nqps_df.loc[index, "U_min_pcnt"] = u_min / scaled_threshold * 100

    @staticmethod
    def display_qps_signal_browser(
        u_qds_dfs: List[pd.DataFrame], source_timestamp_qds_df: pd.DataFrame, timestamp_fgc: int
    ):
        """Method displaying a QPS signal browser to go through all 154 U_QS0 signals per RB circuit

        :param u_qds_dfs: list of list with U_QS0 signal as the first element of the inner list
        :param source_timestamp_qds_df: DataFrame with source and timestamp for QDS (note that source column is magnet
        and timestamp column is timestamp_iqps). The number of columns has to match the size of the outer list in u_qds_dfs
        :param timestamp_fgc: FGC PM timestamp for synchronizing the U_QS0 signals
        :return QpsSignalBrowser browser:
        """
        from ipywidgets import Label, VBox, Select, Layout
        from IPython.display import clear_output, display, HTML
        from lhcsmapi.pyedsl.PlotBuilder import PlotBuilder
        import functools
        from copy import deepcopy

        class QpsSignalBrowser(object):
            def __init__(self, u_qds_dfs, source_timestamp_qds_df, timestamp_fgc):
                self.u_qds_dfs = u_qds_dfs
                self.source_timestamp_qds_df = source_timestamp_qds_df
                self.timestamp_fgc = timestamp_fgc
                self.label = Label(value="Select a magnet for plotting")
                self.magnet_sel = Select(
                    options=source_timestamp_qds_df["magnet"].values[::2],
                    rows=10,
                    disabled=False,
                    layout=Layout(width="100%"),
                )
                self.magnet_sel.observe(functools.partial(QpsSignalBrowser.plot_signals, self=self), names=["value"])

                self.widget = VBox(children=[self.label, self.magnet_sel])
                display(self.widget)
                QpsSignalBrowser.plot_signals(None, self)

            @staticmethod
            def plot_signals(value, self):
                index = self.magnet_sel.index

                clear_output()
                display(self.widget)

                u_qs0_dfs = []
                for i in range(2):
                    u_qs0_df = deepcopy(self.u_qds_dfs[2 * index + i][0])
                    u_qs0_df.index += (
                        self.source_timestamp_qds_df.loc[2 * index + i, "timestamp_iqps"] - self.timestamp_fgc
                    ) / 1e9
                    iqps_board_type = self.source_timestamp_qds_df.loc[2 * index + i, "iqps_board_type"]
                    signal_name = u_qs0_df.columns[0]
                    u_qs0_df.rename(columns={signal_name: "%s_%s" % (signal_name, iqps_board_type)}, inplace=True)
                    u_qs0_dfs.append(u_qs0_df.iloc[10:])

                magnet = self.source_timestamp_qds_df.loc[2 * index, "magnet"]
                title = "iQPS, magnet: %s" % (magnet)
                PlotBuilder().with_signal(u_qs0_dfs, title=title, grid=True).with_ylabel(ylabel="U_QS0, [V]").with_xlim(
                    [float("nan"), float("nan")]
                ).plot()

                display(
                    HTML(
                        self.source_timestamp_qds_df.loc[2 * index : 2 * index + 1]
                        .drop(columns="timestamp_iqps")
                        .to_html()
                    )
                )

        QpsSignalBrowser(u_qds_dfs, source_timestamp_qds_df, timestamp_fgc)

    def calc_min_max_iqps_u_qs0(
        self, u_qds_dfs: List[List[pd.DataFrame]], source_timestamp_qds_df: pd.DataFrame, timestamp_fgc: int
    ) -> None:
        """Method calculating minimum and maximum value of iQPS signal - uQSO. This method updates in-place an input
        DataFrame - source_timestamp_qds_df with four columns - min/max U_QS0 value and corresponding timestamp

        :param u_qds_dfs: list of list with U_QS0 signal as the first element of the inner list
        :param source_timestamp_qds_df: DataFrame with source and timestamp for QDS (note that source column is magnet
        and timestamp column is timestamp_iqps). The number of columns has to match the size of the outer list in u_qds_dfs
        :param timestamp_fgc: FGC PM timestamp for synchronizing the U_QS0 signals
        :return None:
        """
        source_timestamp_qds_df["elec_position"] = int()
        source_timestamp_qds_df["U_QS0_max"] = float("nan")
        source_timestamp_qds_df["t_U_QS0_max"] = float("nan")
        source_timestamp_qds_df["U_QS0_min"] = float("nan")
        source_timestamp_qds_df["t_U_QS0_min"] = float("nan")

        for index, row in source_timestamp_qds_df.iterrows():
            source_qds, timestamp_iqps = row[["magnet", "timestamp_iqps"]]
            # if index > len(source_timestamp_qds_df)-1: break
            u_qs0_df = u_qds_dfs[index][0]

            # max-value is calculated starting from position 10
            dt_qds = (timestamp_iqps - timestamp_fgc) / 1e9
            max_u_qs0 = u_qs0_df.iloc[10:].max().values[0]
            t_max_u_qs0 = u_qs0_df.iloc[10:].idxmax().values[0] + dt_qds
            min_u_qs0 = u_qs0_df.iloc[10:].min().values[0]
            t_min_u_qs0 = u_qs0_df.iloc[10:].idxmin().values[0] + dt_qds

            source_timestamp_qds_df.at[index, "elec_position"] = MappingMetadata.get_electrical_position(
                circuit_type=self.circuit_type, magnet="MB." + source_qds
            )
            source_timestamp_qds_df.at[index, "U_QS0_max"] = max_u_qs0
            source_timestamp_qds_df.at[index, "t_U_QS0_max"] = t_max_u_qs0
            source_timestamp_qds_df.at[index, "U_QS0_min"] = min_u_qs0
            source_timestamp_qds_df.at[index, "t_U_QS0_min"] = t_min_u_qs0


# RQ
def find_circuit_name_with_first_quenched_magnet(
    circuit_names: List[str], u_diode_rqd_df: pd.DataFrame, u_diode_rqf_df: pd.DataFrame, u_threshold=1
) -> str:
    """Function finding an RQ circuit with first quenched magnet. The circuit selection is based on the first diode
    voltage that goes above the threshold.

    :param circuit_names: names of RQ circuits
    :param u_diode_rqd_df: U_DIODE_RQD signal
    :param u_diode_rqf_df: U_DIODE_RQF signal
    :param u_threshold: voltage detection threshold
    :return: name of a circuit with a quadrupole magnet that quenched as the first one
    """
    u_diode_rqd_exceed_threshold = u_diode_rqd_df[u_diode_rqd_df.values > u_threshold]
    t_threshold_rqd = u_diode_rqd_exceed_threshold.index[0] if len(u_diode_rqd_exceed_threshold) > 0 else np.nan

    u_diode_rqf_exceed_threshold = u_diode_rqf_df[u_diode_rqf_df.values > u_threshold]
    t_threshold_rqf = u_diode_rqf_exceed_threshold.index[0] if len(u_diode_rqf_exceed_threshold) > 0 else np.nan

    if t_threshold_rqd == np.nan:
        return "" if t_threshold_rqf == np.nan else circuit_names[1]
    if t_threshold_rqf == np.nan:
        return circuit_names[0]
    return circuit_names[0] if t_threshold_rqd < t_threshold_rqf else circuit_names[1]


def find_t_start_t_end_voltage_slope(
    u_qs0_df: pd.DataFrame, u_slope_start=0.01, u_slope_end=0.1
) -> Tuple[float, float]:
    """Function finds start time and end time for voltage slope increase

    :param u_qs0_df:  U_QS0 signal
    :param u_slope_start: start slope voltage
    :param u_slope_end: end slope voltage
    :return: a tuple of slope start and end times
    """
    # Remove first 10 points in order to get rid of initial spikes
    u_qs0_df = u_qs0_df[u_qs0_df.index > u_qs0_df.index[10]]
    u_qs0_df = abs(u_qs0_df)
    if len(u_qs0_df) < 1:
        return 0.0, 0.0
    t_first_max = u_qs0_df.idxmax().values[0]
    u_qs0_short_df = u_qs0_df[u_qs0_df.index < t_first_max]
    if len(u_qs0_short_df) < 1:
        return 0.0, 0.0
    u_qs0_short2_df = u_qs0_short_df[u_qs0_short_df < u_slope_start].dropna()
    t_slope_start = u_qs0_short2_df.index[-1] if len(u_qs0_short2_df) > 0 else u_qs0_short_df.index[0]
    u_qs0_short2_df = u_qs0_short_df[u_qs0_short_df < u_slope_end].dropna()
    t_slope_end = u_qs0_short2_df.index[-1] if len(u_qs0_short2_df) > 0 else u_qs0_short_df.index[-1]
    return t_slope_start, t_slope_end


def calculate_voltage_slope(u_qs0_aperture_df: pd.DataFrame, t_slope_start: float, t_slope_end: float) -> float:
    """Function calculating U_QS0 voltage slope

    :param u_qs0_aperture_df: U_QS0 signal of the quenched aperture
    :param t_slope_start: start time for the voltage slope
    :param t_slope_end: end time for the voltage slope
    :return: value of the voltage slope (NaN in case it can't be calculated)
    """
    if t_slope_start == t_slope_end:
        return np.nan
    u_qs0_slope_df = u_qs0_aperture_df[
        (u_qs0_aperture_df.index >= t_slope_start) & (u_qs0_aperture_df.index <= t_slope_end)
    ]
    if len(u_qs0_slope_df) < 2:
        return np.nan
    du_dt = (u_qs0_slope_df.values[-1] - u_qs0_slope_df.values[0]) / (
        u_qs0_slope_df.index[-1] - u_qs0_slope_df.index[0]
    )
    return float(du_dt)


def choose_u_qs0(aperture: str, u_qs0_int_rq_df: pd.DataFrame, u_qs0_ext_rq_df: pd.DataFrame) -> pd.DataFrame:
    """Function choosing a U_QS0 corresponding to a quenched aperture

    :param aperture: name of an aperture that quenched
    :param u_qs0_int_rq_df: U_QS0 of the INT aperture
    :param u_qs0_ext_rq_df: U_QS0 of the EXT aperture
    :return: a U_QS0 corresponding to a quenched aperture
    """
    return u_qs0_int_rq_df if aperture == "INT" else u_qs0_ext_rq_df


def find_quench_detection(st_nolatch_br_df: pd.DataFrame, start_point: int) -> float:
    """Method finding a moment when a quench is detected in an MQ magnet

    :param st_nolatch_br_df: quench detection logic signal (0 - quench detected)
    :param start_point: start point for quench detection analysis
    :return: time when quench is detected, NaN otherwise
    """
    data = st_nolatch_br_df[start_point:]
    data = data[data == 0].dropna()

    if data.empty:
        return np.nan

    return data.index[0]


class QdsRqAnalysis(QdsAnalysis):
    """Class for quench detection system (QDS) analysis in RQ circuit"""

    @check_dataframe_empty(mode="any", warning="U_QS0, ST_MAGNET, U_DIODE signals are empty, analysis skipped!")
    def analyze_qds(
        self,
        source_timestamp_qds_df: pd.DataFrame,
        circuit_names: List[str],
        iqps_analog_dfs: List[pd.DataFrame],
        iqps_digital_dfs: List[pd.DataFrame],
        u_nqps_rqd_dfs: List[pd.DataFrame],
        u_nqps_rqf_dfs: List[pd.DataFrame],
    ) -> None:
        """Method analyzing QDS signals in RQ circuit. It is plotting all signals and calculating:
        - quench origin (RQD/RQF, INT/EXT)
        - time derivative of the U_QS0 signal

        :param source_timestamp_qds_df: dataframe with PM source and timestamp for QDS events
        :param circuit_names: names of RQ circuits to analyze ['RQD.XXX', 'RQF.XXX']
        :param iqps_analog_dfs: list of tuples of analog iQPS signals. There are as many outer lists as quenched magnets
            and as many elements in the inner tuple as there are analog iQPS signals to analyze
        :param iqps_analog_dfs: list of tuples of digital iQPS signals. There are as many outer lists as quenched
            magnets and as many elements in the inner tuple as there are digital iQPS signals to analyze
        :param u_nqps_rqd_dfs: list of tuples of nQPS RQD signals. There are as many outer lists as quenched
            magnets and as many elements in the inner tuple as there are nQPS RQD signals to analyze
        :param u_nqps_rqf_dfs: list of tuples of nQPS RQD signals. There are as many outer lists as quenched
        magnets and as many elements in the inner tuple as there are nQPS RQD signals to analyze
        """
        for index, row in source_timestamp_qds_df.iterrows():
            source_qds, timestamp_qds = row[["source", "timestamp"]]
            magnet_name = "MQ.{}".format(source_qds)

            (u_qs0_ext_rq_df, u_qs0_int_rq_df, u_1_ext_rq_df, u_2_ext_rq_df, u_1_int_rq_df, u_2_int_rq_df) = (
                iqps_analog_dfs[index]
            )
            st_magnet_ok_ext_rq_df, st_magnet_ok_int_rq_df, st_nqd0_ext_rq_df, st_nqd0_int_rq_df = iqps_digital_dfs[
                index
            ]

            u_diode_rqd_df, u_ref_rqd_df = u_nqps_rqd_dfs[index]
            u_diode_rqf_df, u_ref_rqf_df = u_nqps_rqf_dfs[index]

            # Analysis
            circuit_name = find_circuit_name_with_first_quenched_magnet(circuit_names, u_diode_rqd_df, u_diode_rqf_df)
            aperture = MappingMetadata.get_rq_aperture(circuit_name, magnet_name) if len(circuit_name) > 0 else ""
            u_qs0_aperture_df = (
                choose_u_qs0(aperture, u_qs0_int_rq_df, u_qs0_ext_rq_df) if len(aperture) > 0 else pd.DataFrame()
            )
            t_slope_start, t_slope_end = find_t_start_t_end_voltage_slope(u_qs0_aperture_df)
            du_dt = calculate_voltage_slope(u_qs0_aperture_df, t_slope_start, t_slope_end)
            if len(aperture) > 0:
                print("The first aperture to quench is {} of magnet {}".format(aperture, magnet_name))
                self.results_table.loc[index, "Quench origin"] = "%s/%s" % (circuit_name.split(".")[0], aperture)
                print("The voltage increase is {:.3f} V/s".format(du_dt))
            else:
                print("No aperture quenched in magnet {}".format(magnet_name))
                self.results_table.loc[index, "QDS trigger origin"] = "None"

            if "RQD" in circuit_name:
                self.results_table.loc[index, "dU_iQPS/dt_RQD"] = round(du_dt, 2)
            else:
                self.results_table.loc[index, "dU_iQPS/dt_RQF"] = round(du_dt, 2)

            fig, ax = plt.subplots(2, 2, figsize=(30, 15))
            title = "Magnet: {}, Aperture: {}/{}, Time Stamp: {}".format(
                source_qds, circuit_name.split(".")[0], aperture, Time.to_string_short(timestamp_qds)
            )
            fig.suptitle(title, fontsize=30)

            # iQPS analog
            u_qs0_ext_rq_df.plot(ax=ax[0][0])
            u_qs0_int_rq_df.plot(ax=ax[0][0], grid=True)
            ax[0][0].tick_params(labelsize=15)
            ax[0][0].set_xlim((-0.1, 0.1))
            ax[0][0].set_ylim((-0.3, 0.3))
            ax[0][0].axhspan(ymin=-0.1, ymax=+0.1, facecolor="xkcd:yellowgreen")
            ax[0][0].axvspan(xmin=t_slope_start, xmax=t_slope_end, facecolor="xkcd:goldenrod")

            # nQPS analog
            u_diode_rqd_df.plot(ax=ax[0][1])
            u_ref_rqd_df.plot(ax=ax[0][1])
            u_diode_rqf_df.plot(ax=ax[0][1])
            u_ref_rqf_df.plot(ax=ax[0][1], grid=True)
            ax[0][1].tick_params(labelsize=15)

            # iQPS digital
            st_magnet_ok_ext_rq_df.astype(int).plot(ax=ax[1][0])
            st_magnet_ok_int_rq_df.astype(int).plot(ax=ax[1][0], grid=True)
            ax[1][0].tick_params(labelsize=15)
            ax[1][0].set_xlim((-0.1, 0.1))
            st_nqd0_ext_rq_df.astype(int).plot(ax=ax[1][0])
            st_nqd0_int_rq_df.astype(int).plot(ax=ax[1][0], grid=True)
            ax[1][0].tick_params(labelsize=15)

            # nQPS analog - zoom
            u_diode_rqd_df.plot(ax=ax[1][1])
            u_ref_rqd_df.plot(ax=ax[1][1])
            u_diode_rqf_df.plot(ax=ax[1][1])
            u_ref_rqf_df.plot(ax=ax[1][1], grid=True)
            ax[1][1].tick_params(labelsize=15)
            ax[1][1].set_xlim((-0.1, 0.1))
            plt.show()

    def analyze_qds_run3(
        self,
        source_timestamp_qds_df: pd.DataFrame,
        circuit_names: List[str],
        iqps_analog_dfs: List[pd.DataFrame],
        iqps_digital_dfs: List[pd.DataFrame],
        u_nqps_rqd_dfs: List[pd.DataFrame],
        u_nqps_rqf_dfs: List[pd.DataFrame],
        start_point: int = 100,
    ) -> None:
        """Method analyzing QDS signals in RQ circuit. It is plotting all signals and calculating:
        - quench origin (RQD/RQF, INT/EXT)
        - time derivative of the U_QS0 signal

        :param source_timestamp_qds_df: dataframe with PM source and timestamp for QDS events
        :param circuit_names: names of RQ circuits to analyze ['RQD.XXX', 'RQF.XXX']
        :param iqps_analog_dfs: list of tuples of analog iQPS signals. There are as many outer lists as quenched magnets
            and as many elements in the inner tuple as there are analog iQPS signals to analyze
        :param iqps_analog_dfs: list of tuples of digital iQPS signals. There are as many outer lists as quenched
            magnets and as many elements in the inner tuple as there are digital iQPS signals to analyze
        :param u_nqps_rqd_dfs: list of tuples of nQPS RQD signals. There are as many outer lists as quenched
            magnets and as many elements in the inner tuple as there are nQPS RQD signals to analyze
        :param u_nqps_rqf_dfs: list of tuples of nQPS RQD signals. There are as many outer lists as quenched
        magnets and as many elements in the inner tuple as there are nQPS RQD signals to analyze
        :param start_point: start point for quench detection analysis, defaults to 100 (SIGMON-335)
        """
        for index, row in source_timestamp_qds_df.iterrows():
            source_qds, timestamp_qds = row[["source", "timestamp"]]
            magnet_name = "MQ.{}".format(source_qds)

            u_qs0_int_a_rq_df, u_qs0_ext_a_rq_df = iqps_analog_dfs[index]
            (st_nolatch_br_ext_a_df, st_nolatch_br_int_a_df, st_notrig_br_ext_a_df, st_notrig_br_int_a_df) = (
                iqps_digital_dfs[index]
            )

            u_diode_rqd_df, u_ref_rqd_df = u_nqps_rqd_dfs[index]
            u_diode_rqf_df, u_ref_rqf_df = u_nqps_rqf_dfs[index]

            # Analysis
            t_nolatch_ext_a = find_quench_detection(st_nolatch_br_ext_a_df, start_point)
            t_nolatch_int_a = find_quench_detection(st_nolatch_br_int_a_df, start_point)

            if np.isnan(t_nolatch_ext_a):
                aperture = "INT"
                u_qs0_aperture_df = u_qs0_int_a_rq_df
            elif np.isnan(t_nolatch_int_a):
                aperture = "EXT"
                u_qs0_aperture_df = u_qs0_ext_a_rq_df
            else:
                aperture = "INT" if t_nolatch_int_a < t_nolatch_ext_a else "EXT"
                u_qs0_aperture_df = u_qs0_int_a_rq_df if t_nolatch_int_a < t_nolatch_ext_a else u_qs0_ext_a_rq_df

            if not u_diode_rqd_df.empty and not u_diode_rqf_df.empty:
                circuit_name = find_circuit_name_with_first_quenched_magnet(
                    circuit_names, u_diode_rqd_df, u_diode_rqf_df
                )
            else:
                circuit_name = ""

            t_slope_start, t_slope_end = find_t_start_t_end_voltage_slope(u_qs0_aperture_df)
            du_dt = calculate_voltage_slope(u_qs0_aperture_df, t_slope_start, t_slope_end)
            if len(aperture) > 0:
                print("The first aperture to quench is {} of magnet {}".format(aperture, magnet_name))
                results_table_entry = "{}/{}".format(circuit_name.split(".")[0], aperture)
                print("The voltage increase is {:.3f} V/s".format(du_dt))
            else:
                print("No aperture quenched in magnet {}".format(magnet_name))
                results_table_entry = "None"

            if self.results_table is not None:
                self.results_table.loc[index, "Quench origin"] = results_table_entry

                if "RQD" in circuit_name:
                    self.results_table.loc[index, "dU_iQPS/dt_RQD"] = round(du_dt, 2)
                else:
                    self.results_table.loc[index, "dU_iQPS/dt_RQF"] = round(du_dt, 2)

            fig, ax = plt.subplots(2, 2, figsize=(30, 15))
            title = "Magnet: {}, Aperture: {}/{}, Time Stamp: {}".format(
                source_qds, circuit_name.split(".")[0], aperture, Time.to_string_short(timestamp_qds)
            )
            fig.suptitle(title, fontsize=30)

            # iQPS analog
            if not u_qs0_ext_a_rq_df.empty:
                u_qs0_ext_a_rq_df.plot(ax=ax[0][0])
            if not u_qs0_int_a_rq_df.empty:
                u_qs0_int_a_rq_df.plot(ax=ax[0][0], grid=True)
            ax[0][0].tick_params(labelsize=15)
            ax[0][0].set_xlim((-0.1, 0.1))
            ax[0][0].set_ylim((-0.3, 0.3))  #
            ax[0][0].axhspan(ymin=-0.1, ymax=+0.1, facecolor="xkcd:yellowgreen")
            ax[0][0].axvspan(xmin=t_slope_start, xmax=t_slope_end, facecolor="xkcd:goldenrod")

            # nQPS analog
            if not u_diode_rqd_df.empty:
                u_diode_rqd_df.plot(ax=ax[0][1])
            if not u_ref_rqd_df.empty:
                u_ref_rqd_df.plot(ax=ax[0][1])
            if not u_diode_rqf_df.empty:
                u_diode_rqf_df.plot(ax=ax[0][1])
            if not u_ref_rqf_df.empty:
                u_ref_rqf_df.plot(ax=ax[0][1], grid=True)
            ax[0][1].tick_params(labelsize=15)

            # iQPS digital
            if not st_nolatch_br_ext_a_df.empty:
                st_nolatch_br_ext_a_df.plot(ax=ax[1][0])
            if not st_nolatch_br_int_a_df.empty:
                st_nolatch_br_int_a_df.plot(ax=ax[1][0], grid=True)
            if not st_notrig_br_ext_a_df.empty:
                st_notrig_br_ext_a_df.plot(ax=ax[1][0])
            if not st_notrig_br_int_a_df.empty:
                st_notrig_br_int_a_df.plot(ax=ax[1][0], grid=True)
            ax[1][0].tick_params(labelsize=15)
            ax[1][0].set_xlim((-0.1, 0.1))
            ax[1][0].tick_params(labelsize=15)

            # nQPS analog - zoom
            if not u_diode_rqd_df.empty:
                u_diode_rqd_df.plot(ax=ax[1][1])
            if not u_ref_rqd_df.empty:
                u_ref_rqd_df.plot(ax=ax[1][1])
            if not u_diode_rqf_df.empty:
                u_diode_rqf_df.plot(ax=ax[1][1])
            if not u_ref_rqf_df.empty:
                u_ref_rqf_df.plot(ax=ax[1][1], grid=True)
            ax[1][1].tick_params(labelsize=15)
            ax[1][1].set_xlim((-0.1, 0.1))
            plt.show()
