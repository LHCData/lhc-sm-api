from typing import List, Tuple

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from lhcsmapi.Time import Time
from lhcsmapi.analysis.CircuitAnalysis import CircuitAnalysis
from lhcsmapi.analysis.decorators import check_dataframe_empty
from lhcsmapi.pyedsl.AssertionBuilder import AssertionBuilder
from lhcsmapi.pyedsl.PlotBuilder import PlotBuilder


class DfbAnalysis(CircuitAnalysis):
    """Class for analysis of distributed feedbox (DFB) system"""

    @staticmethod
    def assert_tt891a_min_max_value(tt891a_dfs: List[pd.DataFrame], i_meas_dfs=pd.DataFrame(), value_range=(46, 54)):
        """Method asserting whether the TT891A temperature is within specified [min, max] range for the entire
        signal duration.

        :param tt891a_dfs: list of TT891A temperature measurements
        :param i_meas_dfs: list of power converter current measurements (an empty dataframe by default)
        :param value_range: a tuple of min and max values
        :return: True if the signal is within the specified range
        """
        AssertionBuilder().with_signal(tt891a_dfs).has_min_max_value(
            value_min=value_range[0], value_max=value_range[1]
        ).show_plot(ylabel="TT891A.TEMPERATURECALC [K]", data_sec=i_meas_dfs, ylabel_sec="I_MEAS, [A]")

    @staticmethod
    def assert_tt893_min_max_value(
        tt893_dfs: List[pd.DataFrame], i_meas_dfs=pd.DataFrame(), value_range: Tuple[float, float] = (280, 320)
    ) -> None:
        """Method asserting whether the TT893 temperature is within specified [min, max] range for the entire
        signal duration.

        :param tt893_dfs: list of TT891A temperature measurements
        :param i_meas_dfs: list of power converter current measurements (an empty DataFrame by default)
        :param value_range: a tuple of min and max values
        :return: None
        """
        AssertionBuilder().with_signal(tt893_dfs).has_min_max_value(
            value_min=value_range[0], value_max=value_range[1]
        ).show_plot(ylabel="TT893.TEMPERATURECALC, [K]", data_sec=i_meas_dfs, ylabel_sec="I_MEAS, [A]")

    @staticmethod
    def assert_u_res_min_max_value(
        u_res_dfs: List[pd.DataFrame], value_range: Tuple[float, float] = (-40e-3, 40e-3)
    ) -> None:
        """Method asserting whether the voltage of the resistive part of a DFB is within the specified range

        :param u_res_dfs: list of resistive voltage measurements
        :param value_range: a tuple of min and max values
        :return: None
        """
        AssertionBuilder().with_signal(u_res_dfs).has_min_max_value(
            value_min=value_range[0], value_max=value_range[1]
        ).show_plot(ylabel="U_RES, [V]")

    @staticmethod
    def assert_u_res_min_max_slope(
        u_res_dfs: List[pd.DataFrame],
        plateau_start: List[int],
        plateau_end: List[int],
        t_0: int,
        slope_range: Tuple[float, float] = (-2, 2),
    ) -> None:
        """Method asserting whether the U_RES has a slope within the prescribed range during the current plateaus.
        The slope is expressed in volts per second.

        :param u_res_dfs: list of U_RES signals
        :param plateau_start: List of plateau start timestamps in ns
        :param plateau_end: List of plateau end timestamps in ns
        :param t_0: start time of current signal in ns
        :param slope_range: a tuple of min and max slope values
        :return: None
        """
        AssertionBuilder().with_signal(u_res_dfs).with_time_range(
            t_start=(np.array(plateau_start) - t_0) / 1e9, t_end=(np.array(plateau_end) - t_0) / 1e9
        ).has_min_max_slope(slope_min=slope_range[0], slope_max=slope_range[1]).show_plot(ylabel="U_RES, [V]")

    @staticmethod
    def assert_u_hts_min_max_value(
        u_hts_dfs: List[pd.DataFrame], value_range: Tuple[float, float] = (-0.5e-3, 0.5e-3), is_warning: bool = False
    ) -> None:
        """Method asserting whether the voltage of the HTS part of a DFB is within the specified range

        :param u_hts_dfs: list of resistive voltage measurements
        :param value_range: a tuple of min and max values
        :param is_warning: boolean flag deciding whether a warning is displayed (True) or a text printed (False)
        :return: None
        """
        AssertionBuilder().with_signal(u_hts_dfs).has_min_max_value(
            value_min=value_range[0], value_max=value_range[1], is_warning=is_warning
        ).show_plot(ylabel="U_HTS, [V]")

    @staticmethod
    def assert_u_hts_min_max_slope(
        u_hts_dfs: List[pd.DataFrame],
        plateau_start: List[int],
        plateau_end: List[int],
        t_0: int,
        slope_range: Tuple[float, float] = (-0.5, 0.5),
    ) -> None:
        """Method asserting whether the U_HTS has a slope within the prescribed range during the current plateaus.
                The slope is expressed in volts per second.

        :param u_hts_dfs: list of U_HTS signals
        :param plateau_start: List of plateau start timestamps in ns
        :param plateau_end: List of plateau end timestamps in ns
        :param t_0: start time of current signal in ns
        :param slope_range: a tuple of min and max slope values
        :return: None
        """
        AssertionBuilder().with_signal(u_hts_dfs).with_time_range(
            t_start=(np.array(plateau_start) - t_0) / 1e9, t_end=(np.array(plateau_end) - t_0) / 1e9
        ).has_min_max_slope(slope_min=slope_range[0], slope_max=slope_range[1]).show_plot(ylabel="U_HTS, [V]")

    @staticmethod
    def assert_u_res_between_two_references(
        u_res_dfs: List[pd.DataFrame],
        first_ref: pd.DataFrame,
        first_scaling: float,
        second_ref: pd.DataFrame,
        second_scaling: float,
        median_filter=False,
    ) -> None:
        """Method asserting whether all DFB U_RES voltages fall between two references given as two signals with
        appropriate scaling coefficients.

        :param u_res_dfs: U_RES DFB voltages
        :param first_ref: first reference signal
        :param first_scaling: scaling of the first reference signal
        :param second_ref: second reference signal
        :param second_scaling: scaling of the second reference signal
        :param median_filter: optional filter
        :return: None
        """

        def filter_median(hds_dfs, window=3) -> List[pd.DataFrame]:
            """Median filter with default window size of 3

            :param hds_dfs: list of signals to filter
            :param window: window size, 3 by default
            :return: list of signals after applying the median filter with a given window size
            """
            return [hds_df.rolling(window=window, min_periods=1).median() for hds_df in hds_dfs]

        if median_filter:
            u_res_dfs = filter_median(u_res_dfs)
        AssertionBuilder().with_signal(u_res_dfs).is_between_two_references(
            first_ref=first_ref, first_scaling=first_scaling, second_ref=second_ref, second_scaling=second_scaling
        ).show_plot(ylabel="U_RES, [V]", data_sec=first_ref, ylabel_sec="I_MEAS, [A]")

    @staticmethod
    def assert_cv891_min_max_variation(
        cv891_dfs: List[pd.DataFrame],
        variation_min_max: float,
        plateau_start: List[int],
        plateau_end: List[int],
        t_0: int,
    ) -> None:
        """Method asserting whether the variation in opening of a DFB valve is within the specified range during
        current plateaus.

        :param cv891_dfs: list of DFB valve opening measurements
        :param variation_min_max: variation between minimum and maximum
        :param plateau_start: List of plateau start timestamps in ns
        :param plateau_end: List of plateau end timestamps in ns
        :param t_0: start time of current signal in ns
        :return: None
        """
        AssertionBuilder().with_signal(cv891_dfs).with_time_range(
            t_start=(np.array(plateau_start) - t_0) / 1e9, t_end=(np.array(plateau_end) - t_0) / 1e9
        ).has_min_max_variation(variation_min_max=variation_min_max).show_plot(ylabel="CV891.POSST. [%]")

    @staticmethod
    def assert_cv891_min_max_value(cv891_dfs: List[pd.DataFrame], value_range: Tuple[float, float] = (2, 50)) -> None:
        """Method asserting whether the voltage of the HTS part of a DFB is within the specified range

        :param cv891_dfs: list of DFB valve opening measurements
        :param value_range: a tuple of min and max values
        :return: None
        """
        AssertionBuilder().with_signal(cv891_dfs).has_min_max_value(
            value_min=value_range[0], value_max=value_range[1]
        ).show_plot(ylabel="CV891.POSST, [%]")

    @check_dataframe_empty(mode="all", warning="All DataFrames are empty, LEADS voltage plot skipped!")
    def analyze_leads_voltage(
        self,
        u_dfs: List[pd.DataFrame],
        circuit_name: str,
        timestamp_fgc: int,
        *,
        timestamp_qps=np.nan,
        signal: str,
        value_min: float,
        value_max: float,
    ) -> None:
        """Method asserting whether the voltage is within the prescribed range. In addition, the first QPS trigger is
        indicated.

        :param u_dfs: list of DFB voltages to plot
        :param circuit_name: name of a circuit (for plot title)
        :param timestamp_fgc: for calculation of the first QPS timestamp
        :param timestamp_qps: first QPS timestamp
        :param signal: signal name
        :param value_min: minimum value
        :param value_max: maximum value
        :return: None
        """
        axvline_pos = (timestamp_qps - timestamp_fgc) * 1e-9 if not np.isnan(timestamp_qps) else timestamp_qps
        AssertionBuilder().with_signal(u_dfs).has_min_max_value(value_min=value_min, value_max=value_max).show_plot(
            ylabel="{}, [V]".format(signal),
            axvline=axvline_pos,
            title="%s, %s: %s(t)" % (Time.to_string_short(timestamp_fgc), circuit_name, signal),
        ).get_assertion_result()

    @staticmethod
    def plot_leads_voltage_zoom(
        u_hts_dfs: List[pd.DataFrame],
        circuit_name: str,
        timestamp_fgc: int,
        timestamp_qps: int,
        timestamp_ee_odd: int,
        timestamp_ee_even: int,
        *,
        signal="U_HTS",
        value_range=(-0.001, 0.001),
    ) -> None:
        """Method plotting a DFB voltage. In addition, the first QPS trigger, both EE triggers are indicated.

        :param u_hts_dfs: list of DFB voltages
        :param circuit_name: circuit name
        :param timestamp_fgc: FGC PM timestamp for calculation of the first QPS timestamp
        :param timestamp_qps: first QPS PM timestamp
        :param timestamp_ee_odd: EE ODD PM timestamp
        :param timestamp_ee_even: EE EVEN PM timestamp
        :param signal: signal name
        :return: None
        """
        axvline_pos = (timestamp_qps - timestamp_fgc) * 1e-9 if not np.isnan(timestamp_qps) else timestamp_qps
        title = "%s, %s: %s(t)" % (Time.to_string_short(timestamp_fgc), circuit_name, signal)
        ax = (
            PlotBuilder()
            .with_signal(u_hts_dfs, grid=True, title=title, style="x-")
            .with_xlim((-1, 2))
            .with_ylabel(ylabel="%s, [V]" % signal)
            .with_ylim(value_range)
            .plot(show_plot=False)
            .get_axes()[0]
        )

        ax.axvline(axvline_pos, ls="--")
        ax.axvline((timestamp_ee_odd - timestamp_fgc) * 1e-9, ls="--", color="cyan")
        ax.axvline((timestamp_ee_even - timestamp_fgc) * 1e-9, ls="--", color="green")
        plt.show()
