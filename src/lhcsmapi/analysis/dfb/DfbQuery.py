from typing import List, Tuple

import pandas as pd
from pyspark.sql.session import SparkSession

from lhcsmapi.analysis.CircuitQuery import CircuitQuery, execution_count
from lhcsmapi.analysis.decorators import check_nan_timestamp_signals, check_nan_timestamp
from lhcsmapi.api.query_builder import QueryBuilder


class DfbQuery(CircuitQuery):
    """Class for query of distributed feedbox (DFB) system"""

    @execution_count()
    @check_nan_timestamp(return_type=pd.DataFrame())
    def find_timestamp_leads(
        self, t_start: int, system: str = "LEADS", duration: List[Tuple[float, str]] = [(10, "s"), (400, "s")]
    ) -> pd.DataFrame:
        """Method searching PM leads events
        If the timestamp is NaN, then an empty dataframe is returned.

        :param t_start: FGC PM event timestamp (ns precision) to which the query is synchronised
        :param system: name of a leads system to query according to metadata
        :param duration: search duration before and after the start time
        :return: pd.DataFrame with source and timestamp of the PM LEADS events
        """
        return (
            QueryBuilder()
            .with_pm()
            .with_duration(t_start=t_start, duration=duration)
            .with_circuit_type(self.circuit_type)
            .with_metadata(circuit_name=self.circuit_name, system=system)
            .event_query(verbose=self.verbose)
            .get_dataframe()
        )

    @execution_count()
    @check_nan_timestamp_signals(return_type=pd.DataFrame())
    def query_leads(
        self,
        timestamp_fgc: int,
        source_timestamp_leads_df: pd.DataFrame,
        *,
        system: str = "LEADS",
        signal_names: List[str],
        spark: SparkSession,
        duration: List[Tuple[int, str]] = [(30, "s"), (90, "s")],
    ) -> List[pd.DataFrame]:
        """Method querying leads signals from either PM or NXCALS
        If any timestamp is NaN, then a list of empty dataframes is returned.
        Signals are synchronized to input timestamp and index is converted to seconds.

        :param timestamp_fgc: FGC PM event timestamp (ns precision) to which the query is synchronised
        :param source_timestamp_leads_df:
        :param system: name of a leads system to query according to metadata
        :param signal_names: list of signal names to query according to metadata description
        :param spark: spark session variable needed to perform an NXCALS query
        :param duration: search duration before and after the start time
        :return: list of pd.DataFrames (if a signal was not queried, an empty pd.DataFrame is returned and warning
            containing the raw query is returned)
        """
        if source_timestamp_leads_df.empty:
            return self._query_leads_nxcals(duration, signal_names, spark, system, timestamp_fgc)
        else:
            return self._query_leads_pm(signal_names, source_timestamp_leads_df, system, timestamp_fgc)

    def _query_leads_nxcals(
        self,
        duration: List[Tuple[int, str]],
        signal_names: List[str],
        spark: SparkSession,
        system: str,
        timestamp_fgc: int,
    ) -> List[pd.DataFrame]:
        """Method querying NXCALS for leads signals
        Signals are synchronized to input timestamp and index is converted to seconds.

        :param duration: search duration before and after the start time
        :param signal_names: list of signal names to query according to metadata description
        :param spark: spark session variable needed to perform an NXCALS query
        :param system: name of a leads system to query according to metadata
        :param timestamp_fgc: FGC PM event timestamp (ns precision) to which the query is synchronised
        :return: list of pd.DataFrames (if a signal was not queried, an empty pd.DataFrame is returned and warning
            containing the raw query is returned)
        """
        return (
            QueryBuilder()
            .with_nxcals(spark)
            .with_duration(t_start=timestamp_fgc, duration=duration)
            .with_circuit_type(self.circuit_type)
            .with_metadata(circuit_name=self.circuit_name, system=system, signal=signal_names)
            .signal_query(verbose=self.verbose)
            .synchronize_time(timestamp_fgc)
            .convert_index_to_sec()
            .get_dataframes()
        )

    def _query_leads_pm(
        self, signal_names: List[str], source_timestamp_leads_df: pd.DataFrame, system: str, timestamp_fgc: int
    ) -> List[pd.DataFrame]:
        """Method querying PM for leads signals.
        Signals are synchronized to input timestamp and index is converted to seconds.

        :param signal_names: list of signal names to query according to metadata description
        :param source_timestamp_leads_df:
        :param system: name of a leads system to query according to metadata
        :param timestamp_fgc: FGC PM event timestamp (ns precision) to which the query is synchronised
        :return: list of pd.DataFrames (if a signal was not queried, an empty pd.DataFrame is returned and warning
            containing the raw query is returned)
        """
        source_leads = source_timestamp_leads_df.loc[0, "source"]
        timestamp_leads = source_timestamp_leads_df.loc[0, "timestamp"]
        return (
            QueryBuilder()
            .with_pm()
            .with_timestamp(timestamp_leads)
            .with_circuit_type(self.circuit_type)
            .with_metadata(circuit_name=self.circuit_name, system=system, signal=signal_names, source=source_leads)
            .signal_query(verbose=self.verbose)
            .synchronize_time(timestamp_fgc)
            .convert_index_to_sec()
            .get_dataframes()
        )

    def query_dfb_signal_nxcals(self, t_start, t_end, *, system, signal_names, spark) -> List[pd.DataFrame]:
        """Method querying DFB signals with NXCALS

        :param t_start: start time
        :param t_end: end time
        :param system: DFB system
        :param signal_names: list of signal names
        :param spark: spark connector
        :return: list of pd.DataFrame signals
        """
        return (
            QueryBuilder()
            .with_nxcals(spark)
            .with_duration(t_start=t_start, t_end=t_end)
            .with_circuit_type(self.circuit_type)
            .with_metadata(circuit_name=self.circuit_name, system=system, signal=signal_names)
            .signal_query()
            .synchronize_time()
            .convert_index_to_sec()
            .filter_median()
            .get_dataframes()
        )
