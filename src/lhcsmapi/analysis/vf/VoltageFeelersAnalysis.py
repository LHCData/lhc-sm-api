from typing import List

import pandas as pd

from lhcsmapi.Time import Time
from lhcsmapi.analysis.CircuitAnalysis import CircuitAnalysis
from lhcsmapi.analysis.decorators import check_dataframe_empty
from lhcsmapi.pyedsl.PlotBuilder import PlotBuilder


class VoltageFeelersAnalysis(CircuitAnalysis):
    """Base class for voltage feeler (VF) analysis"""

    @check_dataframe_empty(mode="all", warning="All DataFrames are empty, voltage feeler plot skipped!")
    def analyze_voltage_feelers(
        self,
        circuit_name: str,
        timestamp_fgc: int,
        i_meas_df: pd.DataFrame,
        u_dfs: List[pd.DataFrame],
        signal: str,
        system: str,
        xlim=None,
        ylim=None,
    ) -> None:
        """Method analyzing and plotting voltage feeler measurements in RB and RQ circuits.
        The analysis includes:
        - if measurement is outside of [-2000, 2000] V range; this indicates not communicating cards
        - if measurement is within [-1, 1] V range; this indicates disabled cards

        :param circuit_name: circuit name (for title)
        :param timestamp_fgc: FGC PM timestamp (for title)
        :param i_meas_df: main power converter current
        :param u_dfs: list of voltage feeler measurements
        :param signal: signal name
        :param system: system name
        :param xlim: limits of x-axis
        :param ylim: limits of y-axis
        :return:
        """
        # Plot
        title = "%s %s I_MEAS(t) and %s(t)" % (Time.to_string_short(timestamp_fgc), circuit_name, signal)
        PlotBuilder().with_signal(i_meas_df, title=title, grid=True).with_ylabel(ylabel="I_MEAS, [A]").with_signal(
            u_dfs, legend=False
        ).with_ylabel(ylabel="%s, [V]" % signal).with_xlim(xlim).with_ylim(ylim).plot()

        PlotBuilder().with_signal(i_meas_df, title=title, grid=True).with_ylabel(ylabel="I_MEAS, [A]").with_signal(
            u_dfs, legend=False
        ).with_ylabel(ylabel="%s, [V]" % signal).with_xlim((-2, 3)).with_ylim(ylim).plot()

        # Analyze
        not_communicating_cards = []
        disabled_cards = []
        status = "OK"
        for u_df in u_dfs:
            if (float(u_df.min()) < -2000) and (float(u_df.max()) < -2000):
                not_communicating_cards.append(u_df.columns[0])
                status = "NOT_OK"
            if (float(u_df.min()) > -1) and (float(u_df.max()) < 1):
                disabled_cards.append(u_df.columns[0])
                status = "NOT_OK"

        if not_communicating_cards:
            status += "_NOT_COMMUNICATING_CARDS"
        if disabled_cards:
            status += "_DISABLED_CARDS"

        # Print a report
        print("Report on Voltage Feeler cards:")
        print("- there are {} cards.".format(len(u_dfs)))
        print(
            "- there are {} functioning cards.".format(len(u_dfs) - len(not_communicating_cards) - len(disabled_cards))
        )
        print(
            "- there are {} not communicating cards: {}.".format(len(not_communicating_cards), not_communicating_cards)
        )
        print("- there are {} disabled cards: {}.".format(len(disabled_cards), disabled_cards))

        # Store analysis result
        if self.results_table is not None:
            system_signal = "{}_{}".format(system, signal)
            self.results_table[system_signal] = status
