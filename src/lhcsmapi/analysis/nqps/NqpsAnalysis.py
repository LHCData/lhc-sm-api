from typing import List, Callable

import pandas as pd
import numpy as np

from lhcsmapi.Time import Time
from lhcsmapi.analysis.CircuitAnalysis import CircuitAnalysis
from lhcsmapi.analysis.decorators import check_dataframe_empty
from lhcsmapi.pyedsl.PlotBuilder import PlotBuilder


class NqpsAnalysis(CircuitAnalysis):
    """Class with methods for nQPS analysis (diode voltage in particular)"""

    @check_dataframe_empty(mode="all", warning="All DataFrames are empty, nQPS voltage plot skipped!")
    def filter_quenched_magnets(
        self, u_diode_rb_dfs: List[pd.DataFrame], quenched_magnets_df: pd.Series
    ) -> List[pd.DataFrame]:
        """Method filtering only U_DIODE of quenched magnets from a list of all magnets

        :param u_diode_rb_dfs: list of all U_DIODE signals
        :param quenched_magnets_df: list of quenched magnets
        :return: List[pd.DataFrame]
        """

        def make_filter_quenched_magnets(quenched_magnets: np.ndarray) -> Callable[[pd.DataFrame], bool]:
            """Function making a filter for quenched magnets

            :param quenched_magnets: a list of quenched magnets
            :return: a function for name filter
            """

            def name_filter(df: pd.DataFrame):
                return any([magnet in df.columns[0] for magnet in quenched_magnets])

            return name_filter

        filter_quenched_magnets = make_filter_quenched_magnets(quenched_magnets_df.values)

        return list(filter(filter_quenched_magnets, u_diode_rb_dfs))

    @check_dataframe_empty(mode="all", warning="All DataFrames are empty, nQPS voltage plot skipped!")
    def analyze_u_diode_nqps(
        self,
        circuit_name: str,
        timestamp_fgc: int,
        i_meas_df: pd.DataFrame,
        u_dfs: List[pd.DataFrame],
        signal: str,
        system: str,
        legend=False,
        xlim=(-5, 150),
    ) -> None:
        """Method plotting nQPS voltage together with main power converter current

        :param circuit_name: name of a circuit (for title)
        :param timestamp_fgc: FGC PM timestamp (for title)
        :param i_meas_df: main power converter current
        :param u_dfs: list of nQPS voltages to plot
        :param signal: name of a signal to plot (for title)
        :param system: name of a system to plot
        :param legend: a boolean flag to display (if True) a legend; False by default as typically u_dfs is long
        :param xlim: limits of x-axis
        :return: None
        """
        title = "%s %s I_MEAS(t) and %s(t)" % (Time.to_string_short(timestamp_fgc), circuit_name, signal)
        PlotBuilder().with_signal(i_meas_df, title=title, grid=True).with_ylabel(ylabel="I_MEAS, [A]").with_signal(
            u_dfs, legend=legend
        ).with_ylabel(ylabel="%s, [V]" % signal).with_xlim(xlim).plot()
