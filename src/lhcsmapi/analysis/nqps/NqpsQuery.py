import warnings
from typing import List, Tuple

import pandas as pd
from pyspark.sql.session import SparkSession
from tqdm.notebook import tqdm

from lhcsmapi import utils
from lhcsmapi.analysis.CircuitQuery import CircuitQuery, execution_count
from lhcsmapi.analysis.decorators import check_dataframe_empty, check_nan_timestamp_signals
from lhcsmapi.api.query_builder import QueryBuilder
from lhcsmapi.metadata.MappingMetadata import MappingMetadata

_RB_SOURCES_WITHOUT_U_DIODE = ["A8L3", "A8R3", "A8L7", "A8R7"]


class NqpsQuery(CircuitQuery):
    """Class with methods for nQPS event and signal queries"""

    def find_source_timestamp_nqps_pm(
        self, source_qds: str, timestamp_qds: int, duration: List[Tuple[int, str]] = [(600, "s"), (600, "s")]
    ) -> pd.DataFrame:
        """Method searching PM nQPS events.
        If the timestamp is NaN, then an empty dataframe is returned.

        :param source_qds: PM QDS source according to metadata
        :param timestamp_qds: PM QDS timestamp in ns precision
        :param duration: search duration before and after the start time
        :return: pd.DataFrame with source and timestamp of the PM nQPS events
        """
        qps_crate = MappingMetadata.get_crate_name_from_magnet_name(self.circuit_type, source_qds)
        system = "DIODE_{}".format(self.circuit_name.split(".")[0])

        return (
            QueryBuilder()
            .with_pm()
            .with_duration(t_start=timestamp_qds, duration=duration)
            .with_circuit_type(self.circuit_type)
            .with_metadata(circuit_name=self.circuit_name, system=system, source=qps_crate)
            .event_query(verbose=self.verbose)
            .get_dataframe()
        )

    @execution_count()
    def query_nqps_voltage_pm(self, source_timestamp_qds_df: pd.DataFrame) -> List[List[pd.DataFrame]]:
        """Method querying PM for nQPS signals
        Signals are synchronized to the nQPS PM event and index is converted to seconds.

        :param source_timestamp_qds_df: dataframe with PM source and timestamp for QDS events
        :return: List of list of nQPS signals. The outer list has as many elements as nQPS events and the length
            of the inner is equal to the number of nQPS signals.
        """
        u_nqps_dfs = []
        for index, row in tqdm(
            source_timestamp_qds_df.iterrows(),
            total=source_timestamp_qds_df.shape[0],
            desc="Querying PM for nQPS analog signals",
        ):
            timestamp_qds = row["timestamp"]
            source_qds = row["source"]
            qps_crate = MappingMetadata.get_crate_name_from_magnet_name(self.circuit_type, source_qds)
            system = "DIODE_{}".format(self.circuit_name.split(".")[0])
            signal = "U_DIODE_{}".format(self.circuit_name.split(".")[0])
            wildcard = "MB.{}".format(source_qds) if self.circuit_type == "RB" else "MQ.{}".format(source_qds)

            source_timestamp_nqps_df = self.find_source_timestamp_nqps_pm(source_qds, timestamp_qds)
            if not source_timestamp_nqps_df.empty:
                timestamp_nqps = source_timestamp_nqps_df.loc[0, "timestamp"]

                u_diode_rqd_df = (
                    QueryBuilder()
                    .with_pm()
                    .with_timestamp(timestamp_nqps)
                    .with_circuit_type(self.circuit_type)
                    .with_metadata(
                        circuit_name=self.circuit_name,
                        system=system,
                        signal=signal,
                        source=qps_crate,
                        wildcard={"MAGNET": wildcard},
                    )
                    .signal_query()
                    .synchronize_time(timestamp_nqps)
                    .convert_index_to_sec()
                    .get_dataframes()
                )

                u_ref_rqd_df = (
                    QueryBuilder()
                    .with_pm()
                    .with_timestamp(timestamp_nqps)
                    .with_circuit_type(self.circuit_type)
                    .with_metadata(
                        circuit_name=self.circuit_name,
                        system=system,
                        signal="U_REF_N1",
                        source=qps_crate,
                        wildcard={"QPS_CRATE": qps_crate},
                    )
                    .signal_query()
                    .synchronize_time(timestamp_nqps)
                    .convert_index_to_sec()
                    .get_dataframes()
                )
                u_nqps_dfs.append([u_diode_rqd_df, u_ref_rqd_df])
            else:
                warnings.warn("nQPS buffer is empty for %s and %d !" % (source_qds, timestamp_qds))
                u_nqps_dfs.append([pd.DataFrame(), pd.DataFrame()])

        return u_nqps_dfs

    # U_DIODE, U_EARTH
    @execution_count()
    def query_voltage_nxcals(
        self,
        system: str,
        signal: str,
        timestamp_sync: int,
        *,
        spark: SparkSession,
        duration: List[Tuple[int, str]] = [(50, "s"), (150, "s")],
    ) -> List[pd.DataFrame]:
        """Method querying CALS for voltage signals (either U_DIODE or U_EARTH)
        Signals are synchronized to the input timestamp

        :param system: name of a leads system to query according to metadata
        :param signal: name of leads signal to query according to metadata
        :param timestamp_sync: FGC PM event timestamp (ns precision) to which the query is synchronised
        :param spark: NXCALS connector
        :param duration: search duration before and after the start time
        :return: list of pd.DataFrames (if a signal was not queried, an empty pd.DataFrame is returned and warning
            containing the raw query is returned)
        """
        return (
            QueryBuilder()
            .with_nxcals(spark)
            .with_duration(t_start=timestamp_sync, duration=duration)
            .with_circuit_type(self.circuit_type)
            .with_metadata(circuit_name=self.circuit_name, system=system, signal=signal, wildcard={"MAGNET": "*"})
            .multi_signal_query(verbose=self.verbose)
            .synchronize_time(timestamp_sync)
            .convert_index_to_sec()
            .get_dataframes()
        )


class NqpsRqQuery(NqpsQuery):
    """Class with methods for nQPS event and signal queries in RQ circuit"""

    @execution_count()
    def query_voltage_nxcals(
        self,
        system: str,
        signal: str,
        timestamp_fgc: int,
        *,
        spark: SparkSession,
        duration: List[Tuple[int, str]] = [(50, "s"), (150, "s")],
    ) -> List[pd.DataFrame]:
        """Method querying NXCALS for voltage signals (either voltage feelers or diodes).
        If voltage across diodes is queried, then in addition, there is a U_REF signal queried for those diodes
        without U_DIODE signal.

        :param system: name of a system to query according to metadata
        :param signal: name of a signal to query
        :param timestamp_fgc: unix timestamp of an FGC event in a circuit in nanoseconds to which the query is synced
        :param spark: NXCALS connector
        :param duration: search duration before and after the start time
        :return: list of pd.DataFrames (if a signal was not queried, an empty pd.DataFrame is returned and warning
            containing the raw query is returned)
        """
        u_nxcals_dfs = super().query_voltage_nxcals(system, signal, timestamp_fgc, spark=spark, duration=duration)

        if "DIODE" in system:
            rq_diode = MappingMetadata.read_magnet_to_cell_qps_crate(circuit_type="RQ")
            crates = list(
                rq_diode[rq_diode["Crate U_DIODE_RQx"].isna() & (rq_diode["Circuit"] == self.circuit_name)][
                    "Crate U_REF_N1"
                ].values
            )

            u_ref_dfs = (
                QueryBuilder()
                .with_nxcals(spark)
                .with_duration(t_start=int(timestamp_fgc), duration=duration)
                .with_circuit_type(self.circuit_type)
                .with_metadata(
                    circuit_name=self.circuit_name, system=system, signal="U_REF_N1", wildcard={"QPS_CRATE": crates}
                )
                .multi_signal_query()
                .synchronize_time(timestamp_fgc)
                .convert_index_to_sec()
                .get_dataframes()
            )

            return u_nxcals_dfs + u_ref_dfs
        else:
            return u_nxcals_dfs


class NqpsRbQuery(NqpsQuery):
    """Class with methods for nQPS event and signal queries in RB circuit"""

    @check_dataframe_empty(
        mode="any",
        warning="At least one of the timestamps (nQPS or QDS) is missing, query skipped returning "
        "empty pd.DataFrame.",
        default_return=pd.DataFrame(),
    )
    @execution_count()
    def query_voltage_nqps(
        self,
        source_timestamp_nqps_df: pd.DataFrame,
        source_timestamp_qds_df: pd.DataFrame,
        timestamp_fgc: int,
        *,
        spark: SparkSession,
    ):
        """Method querying voltage of nQPS from either PM or NXCALS.
            The signals are synchronized in time and converted from ns to s.

        :param source_timestamp_nqps_df: table with source and timestamp for nQPS
        :param source_timestamp_qds_df: table with source and timestamp for iQPS
        :param spark: NXCALS connector
        :return: list of pd.DataFrames (if a signal was not queried, an empty pd.DataFrame is returned and warning
            containing the raw query is returned)
        """

        # merge source timestamp nqps and qds
        source_timestamp_qds_renamed_df = source_timestamp_qds_df.rename(
            columns={"source": "source_iqps", "timestamp": "timestamp_iqps"}
        )
        source_timestamp_qds_renamed_df["source_nqps"] = source_timestamp_qds_renamed_df["source_iqps"].apply(
            lambda col: MappingMetadata.get_crate_name_from_magnet_name(self.circuit_type, col)
        )
        source_timestamp_nqps_renamed_df = source_timestamp_nqps_df.rename(
            columns={"source": "source_nqps", "timestamp": "timestamp_nqps"}
        )
        source_timestamp_qds_renamed_df = (
            source_timestamp_qds_renamed_df.merge(
                source_timestamp_nqps_renamed_df[["timestamp_nqps", "source_nqps"]], on="source_nqps", how="left"
            )
            .sort_values(by="timestamp_iqps")
            .reset_index(drop=True)
        )

        u_nqps_dfs = []
        for index, row in tqdm(
            source_timestamp_qds_renamed_df.iterrows(),
            total=source_timestamp_qds_renamed_df.shape[0],
            desc="Querying PM/NXCALS U_DIODE_RB, U_REF_N1",
        ):
            source_nqps, timestamp_nqps, timestamp_qds = row[["source_nqps", "timestamp_nqps", "timestamp_iqps"]]

            magnet_names = MappingMetadata.get_magnet_names_from_crate_name(self.circuit_type, source_nqps)
            if 1e-9 * (timestamp_qds - timestamp_fgc) < 2:
                u_diode_dfs, u_ref_n1_dfs = self._query_nqps_pm(
                    magnet_names, source_nqps, timestamp_nqps, signal_names=("U_DIODE_RB", "U_REF_N1")
                )
            else:
                u_diode_dfs, u_ref_n1_dfs = self._query_nqps_nxcals(
                    source_nqps, magnet_names, timestamp_qds, spark=spark, signal_names=("U_DIODE_RB", "U_REF_N1")
                )

            u_nqps_dfs.append(u_diode_dfs + u_ref_n1_dfs)

        return u_nqps_dfs

    @execution_count()
    def query_voltage_nqps_pm(self, source_timestamp_nqps_df: pd.DataFrame):
        """Method querying voltage of nQPS from PM.
            The signals are synchronized in time and converted from ns to s.

        :param source_timestamp_nqps_df: table with source and timestamp for nQPS
        :return: list of pd.DataFrames (if a signal was not queried, an empty pd.DataFrame is returned and warning
            containing the raw query is returned)
        """

        def _query_nqps_pm(row):
            source_nqps, timestamp_nqps = row[["source", "timestamp"]]
            magnet_names = MappingMetadata.get_magnet_names_from_crate_name(self.circuit_type, source_nqps)

            u_diode_dfs, u_ref_n1_dfs = self._query_nqps_pm(
                magnet_names, source_nqps, timestamp_nqps, signal_names=("U_DIODE_RB", "U_REF_N1")
            )
            return u_diode_dfs + u_ref_n1_dfs

        return [
            _query_nqps_pm(row)
            for (_, row) in tqdm(
                source_timestamp_nqps_df.iterrows(),
                total=source_timestamp_nqps_df.shape[0],
                desc="Querying PM U_DIODE_RB, U_REF_N1",
            )
            if row["source"] not in _RB_SOURCES_WITHOUT_U_DIODE
        ]

    @check_nan_timestamp_signals(return_type=pd.DataFrame())
    def _query_nqps_nxcals(
        self,
        source_nqps,
        magnet_names,
        timestamp_qds,
        *,
        spark,
        signal_names=("U_DIODE_RB", "U_REF_N1"),
        duration=[(10, "s"), (350, "s")],
    ) -> Tuple[List[pd.DataFrame], List[pd.DataFrame]]:
        u_diode_dfs = (
            QueryBuilder()
            .with_nxcals(spark)
            .with_duration(t_start=timestamp_qds, duration=duration)
            .with_circuit_type(self.circuit_type)
            .with_metadata(
                circuit_name=self.circuit_name,
                system="DIODE_RB",
                source=source_nqps,
                wildcard={"MAGNET": magnet_names},
                signal=signal_names[0],
            )
            .signal_query()
            .synchronize_time(timestamp_qds)
            .convert_index_to_sec()
            .get_dataframes()
        )
        u_ref_n1_dfs = (
            QueryBuilder()
            .with_nxcals(spark)
            .with_duration(t_start=timestamp_qds, duration=duration)
            .with_circuit_type(self.circuit_type)
            .with_metadata(
                circuit_name=self.circuit_name,
                system="DIODE_RB",
                source=source_nqps,
                wildcard={"QPS_CRATE": source_nqps},
                signal=signal_names[1],
            )
            .signal_query()
            .synchronize_time(timestamp_qds)
            .convert_index_to_sec()
            .get_dataframes()
        )

        return utils.vectorize(u_diode_dfs), utils.vectorize(u_ref_n1_dfs)

    @check_nan_timestamp_signals(return_type=pd.DataFrame())
    def _query_nqps_pm(
        self, magnet_names, source_nqps, timestamp_nqps, signal_names=("U_DIODE_RB", "U_REF_N1")
    ) -> Tuple[List[pd.DataFrame], List[pd.DataFrame]]:
        u_diode_dfs = (
            QueryBuilder()
            .with_pm()
            .with_timestamp(timestamp_nqps)
            .with_circuit_type(self.circuit_type)
            .with_metadata(
                circuit_name=self.circuit_name,
                system="DIODE_RB",
                source=source_nqps,
                wildcard={"MAGNET": magnet_names},
                signal=signal_names[0],
            )
            .signal_query()
            .synchronize_time(timestamp_nqps)
            .convert_index_to_sec()
            .get_dataframes()
        )
        u_ref_n1_dfs = (
            QueryBuilder()
            .with_pm()
            .with_timestamp(timestamp_nqps)
            .with_circuit_type(self.circuit_type)
            .with_metadata(
                circuit_name=self.circuit_name,
                system="DIODE_RB",
                source=source_nqps,
                wildcard={"QPS_CRATE": source_nqps},
                signal=signal_names[1],
            )
            .signal_query()
            .synchronize_time(timestamp_nqps)
            .convert_index_to_sec()
            .get_dataframes()
        )

        return utils.vectorize(u_diode_dfs), utils.vectorize(u_ref_n1_dfs)
