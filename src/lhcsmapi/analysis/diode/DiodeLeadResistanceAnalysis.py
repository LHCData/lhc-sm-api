import warnings
from copy import deepcopy
from typing import Tuple, List

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from lhcsmapi.Time import Time
from lhcsmapi.analysis.CircuitAnalysis import CircuitAnalysis
from lhcsmapi.analysis.expert_input import check_show_next
from lhcsmapi.metadata.MappingMetadata import MappingMetadata
from lhcsmapi.api.processing import SignalProcessing


# Analysis
def calculate_resistance_nxcals(
    dt_wrt_fgc: int,
    circuit_name: str,
    i_circuit_sync_df: pd.DataFrame,
    u_diode_sync_df: pd.DataFrame,
    board_toggling_period: int,
) -> pd.DataFrame:
    """Function calculating diode leads resistance from voltage (U_DIODE) and current (I_MEAS) querried from NXCALS

    :param circuit_name: name of a circuit
    :param i_circuit_sync_df: synchronised circuit current (I_MEAS)
    :param u_diode_sync_df: synchronised diode voltage (U_DIODE)
    :param board_toggling_period: toggling period of nQPS U_DIODE signals in NXCALS, expressed in number of points
    :return:  diode lead resistance
    """

    pd.set_option("mode.chained_assignment", None)  # SIGMON-342 - verified that the warning can be silenced
    U_DIODE = u_diode_sync_df.columns[0]
    # remove duplicate index
    u_diode_sync_df = u_diode_sync_df[~u_diode_sync_df.index.duplicated(keep="first")]

    # Resample circuit current
    t_end = 350 if "RB" in circuit_name else 150
    i_circuit_sync_resampled = (
        SignalProcessing(i_circuit_sync_df).resample(ts=0.1, t0=-50, t_end=t_end).get_dataframes()
    )
    if len(i_circuit_sync_resampled) > len(u_diode_sync_df):
        i_circuit_sync_resampled = i_circuit_sync_resampled.head(len(u_diode_sync_df))

    # Join current and diode voltage into a single dataframe
    u_diode_sync_df["I_MEAS"] = i_circuit_sync_resampled.values

    # Recover resistive and diode minimum voltage
    calculate_minimum_diode_and_current_leads_voltage(u_diode_sync_df, U_DIODE, board_toggling_period)

    # Calculate resistance
    u_diode_sync_df["RES"] = u_diode_sync_df.apply(lambda row: 1e6 * row["U_RES"] / row["I_MEAS"], axis=1, raw=False)

    # Set to 0 resistance values for too low current and with too high diode voltage derivative
    set_to_zero_resistance_outside_of_range(u_diode_sync_df, "I_MEAS")

    # Filter resistance
    filter_resistance(u_diode_sync_df)
    pd.set_option("mode.chained_assignment", "warn")

    return u_diode_sync_df


def set_to_zero_resistance_outside_of_range(u_diode_df, I_MEAS: str) -> None:
    """Function setting to 0 values outside of the range

    :param u_diode_df: diode voltage signal
    :param I_MEAS: name of current signal
    :return: None, update done in-place on the u_diode_df variable
    """
    calc_diff_u_diode = lambda x: x[-1] - x[0]
    u_diode_df["dU_DIODE"] = u_diode_df["U_DIODE_min"].rolling(11).apply(calc_diff_u_diode, raw=True)

    u_diode_df.loc[u_diode_df.index < u_diode_df.index[550], "dU_DIODE"] = 0

    if u_diode_df["dU_DIODE"].max() > 0.05:
        index_cut = u_diode_df.query("dU_DIODE > 0.05").index[0]
    else:
        index_cut = max(u_diode_df.index)

    u_diode_df.loc[u_diode_df[I_MEAS] <= 1000, "RES"] = 0
    u_diode_df.loc[u_diode_df.index > index_cut, "RES"] = 0


def calculate_minimum_diode_and_current_leads_voltage(
    u_diode_df: pd.DataFrame, U_DIODE: str, board_toggling_period: int
) -> None:
    """Function calculating minimum diode and current leads voltage. It takes into account the toggling period of
    diode voltage logging (board A&B toggling).

    :param u_diode_df: diode voltage dataframe
    :param U_DIODE: name of voltage signal
    :param board_toggling_period: toggling period of nQPS U_DIODE signals in NXCALS, expressed in number of points
    :return: None, update done in-place on the u_diode_df variable
    """
    # "50s x 10 Hz + 20" delay to the diode full opening

    u_diode_df["U_DIODE_min"] = (
        u_diode_df.loc[u_diode_df.index, U_DIODE]
        .rolling((2 * board_toggling_period) + 1)
        .apply(lambda x: min((x[0] + x[-1]) / 2, x[board_toggling_period]), raw=True)
        .shift(-board_toggling_period)
    )
    u_diode_df["U_DIODE_min"].iloc[0:535] = 0

    u_diode_df["U_RES"] = (
        u_diode_df.loc[u_diode_df.index >= u_diode_df.index[535], U_DIODE]
        .rolling((2 * board_toggling_period) + 1)
        .apply(lambda x: abs(x[board_toggling_period] - (x[0] + x[-1]) / 2), raw=True)
        .shift(-board_toggling_period)
    )

    u_diode_df["U_RES"] = u_diode_df["U_RES"].clip(lower=0)

    u_diode_df.loc[u_diode_df.index < u_diode_df.index[540], ["U_RES", "U_DIODE_min"]] = 0


def filter_resistance(u_diode_df: pd.DataFrame) -> None:
    """Function applying a median filter with a window of 41

    :param u_diode_df: U_DIODE signal
    :return: None, update done in-place on the u_diode_df variable
    """
    u_diode_df["RES"] = u_diode_df["RES"].rolling(41).median().values
    u_diode_df["RES_filtered"] = u_diode_df["RES"].rolling(window=10).mean()


def extract_res_max_time_current_nxcals(u_diode_df: pd.DataFrame, I_MEAS: str) -> Tuple[float, float, float]:
    """Function extracting maximum diode leads resistance as well as corresponding time and current

    :param u_diode_df: diode voltage signal (it contains columns for current I_MEAS and resistance RES_filtered)
    :param I_MEAS: name of current signal
    :return: a tuple of maximum resistance, time, and current
    """
    res_max = u_diode_df["RES_filtered"].max()
    t_res_max = u_diode_df.idxmax(axis=0, skipna=True)["RES_filtered"]
    i_res_max = u_diode_df.loc[u_diode_df.index == t_res_max, I_MEAS].values[0]
    return res_max, t_res_max, i_res_max


def preprocess_pm_qps_signal(
    time_corr: float, timestamp_unix: int, pm_qps_signal: pd.DataFrame, pm_signal_name: str
) -> pd.DataFrame:
    """

    :param time_corr:
    :param timestamp_unix:
    :param pm_qps_signal:
    :param pm_signal_name:
    :return:
    """
    pm_qps_signal.drop(pm_qps_signal.index[:2], inplace=True)
    pm_qps_signal.drop(pm_qps_signal.index[-2:], inplace=True)
    pm_qps_signal.drop(pm_qps_signal[pm_qps_signal[pm_signal_name] == 0.0].index, inplace=True)
    pm_qps_signal = pm_qps_signal.set_index((pm_qps_signal.index.values - timestamp_unix) / 1e9 + time_corr)
    return pm_qps_signal


def preprocess_pm_pc_signal(
    iab_i_a: pd.DataFrame, iab_i_a_name: str, index_end_diode: int, index_start_diode: int, timestamp_unix: int
) -> pd.DataFrame:
    """Function preprocessing PM power converter current IAB.I_A. The following operations are performed:
    - 0 value is removed
    - time is synchronized to an input timestamp and converted to seconds
    - time between diode voltage start and end is maintained

    :param iab_i_a: power converter current IAB.I_A
    :param iab_i_a_name: name of the power converter current IAB.I_A
    :param index_end_diode: index of the end of diode voltage
    :param index_start_diode: index of the start of diode voltage
    :param timestamp_unix: unix timestamp for signal synchronization
    :return: preprocessed power converter current
    """
    iab_i_a.drop(iab_i_a[iab_i_a[iab_i_a_name] == 0.0].index, inplace=True)
    iab_i_a = iab_i_a.set_index((iab_i_a.index.values - timestamp_unix) / 1e9)

    index_start, index_end = iab_i_a.index.get_indexer([index_start_diode, index_end_diode], method="nearest")
    iab_i_a.drop(iab_i_a.index[index_end:], inplace=True)
    iab_i_a.drop(iab_i_a.index[:index_start], inplace=True)
    return iab_i_a


def resample_pm_signal(df: pd.DataFrame) -> pd.DataFrame:
    """Function resampling a dataframe into 2001 points (assuming the same sampling for all signals)

    :param df: signal to be resampled
    :return: resampled signal
    """
    df_resampled = df.reindex(df.index.union(np.linspace(df.index[0], df.index[-1], 2001))).interpolate("index")
    df_resampled.drop(df.index, axis=0, inplace=True)
    return df_resampled


def calculate_resistence(t_max: float, df: pd.DataFrame) -> None:
    """Function calculating resistance for PM signals based on U_DIODE signals (board A and B) and power converter
    current IAB.I_A

    :param t_max: maximum time
    :param df: dataframe with U_DIODE_A, U_DIODE_B, and IAB.I_A
    :return: None, df is updated in-place
    """
    df["U_RES"] = df["U_DIODE_A"] - df["U_DIODE_B"]
    df.loc[df.index < t_max + 0.05, "U_RES"] = 0
    df["RES"] = df["U_RES"] / df["IAB.I_A"]
    df["RES"] = df["RES"].rolling(window=3, min_periods=1).median()
    df["RES"] = df["RES"] * 1e6
    df["time"] = t_max


def extract_res_max_time_current_pm(df: pd.DataFrame) -> Tuple[float, float, float]:
    """

    :param df:
    :return:
    """
    res_max = int(df["RES"].max())
    t_res_max = df.idxmax(axis=0, skipna=True)["RES"]
    i_res_max = df.reindex([t_res_max], method="nearest", tolerance=0.0001)["IAB.I_A"].values[0]
    return res_max, t_res_max, i_res_max


def calculate_resistance_pm(
    iab_i_a: pd.DataFrame,
    u_diode_a: pd.DataFrame,
    u_diode_b: pd.DataFrame,
    u_ref: pd.DataFrame,
    timestamp_qds: int,
    time_corr=0.0,
) -> pd.DataFrame:
    """Function calculating resistance for diode leads signals queried with PM.

    :param iab_i_a: power converter current
    :param u_diode_a: diode voltage board A
    :param u_diode_b: diode voltage board B
    :param u_ref: nQPS reference voltage
    :param timestamp_qds: QDS pm timestamp
    :param time_corr: optional time correction of QPS signals
    :return: DataFrame with resampled U_DIODE_A, U_DIODE_B, U_REF IAB.I_A, RES_filtered
    """

    timestamp_unix = Time.to_unix_timestamp(timestamp_qds)

    # U_DIODE
    u_diode_a = preprocess_pm_qps_signal(time_corr, timestamp_unix, u_diode_a, u_diode_a.columns[0])
    u_diode_b = preprocess_pm_qps_signal(time_corr, timestamp_unix - 1e6, u_diode_b, u_diode_b.columns[0])

    # U_REF
    u_ref = preprocess_pm_qps_signal(time_corr, timestamp_unix, u_ref, u_ref.columns[0])

    # I_A
    index_start_diode = u_diode_a.index[0]
    index_end_diode = u_diode_a.index[-1]
    iab_i_a = preprocess_pm_pc_signal(iab_i_a, iab_i_a.columns[0], index_end_diode, index_start_diode, timestamp_unix)

    # Resample signals
    u_diode_a_resampled = resample_pm_signal(u_diode_a)
    u_diode_b_resampled = resample_pm_signal(u_diode_b)
    u_ref_resampled = resample_pm_signal(u_ref)
    iab_i_a_resampled = resample_pm_signal(iab_i_a)

    # Concatenate signals
    res_df = pd.DataFrame()
    res_df["U_DIODE_A"] = u_diode_a_resampled[u_diode_a.columns[0]]
    res_df["U_DIODE_B"] = u_diode_b_resampled.values
    res_df["U_REF"] = u_ref_resampled.values
    res_df["IAB.I_A"] = iab_i_a_resampled.values

    # Calculate resistance
    t_max = u_diode_a.idxmax(axis=0, skipna=True)[0]
    calculate_resistence(t_max, res_df)

    return res_df


# Plots
def plot_current_voltage_nxcals(
    circuit_name: str, source_qds: str, df: pd.DataFrame, ax=None, *, title="NXCALS"
) -> None:
    """Function plotting current and voltage from NXCALS

    :param circuit_name: the name of the circuit
    :param source_qds: source of the QDS
    :param df: dataframe with U_DIODE and I_MEAS
    :param ax: axis on which the plot is displayed
    :param title: plot title
    :return: None
    """
    i_meas = df.filter(regex="I_MEAS")
    u_diode = _filter_u_diode(df, circuit_name, source_qds)
    if ax is None:
        ax1 = i_meas.plot(title=title, figsize=(15, 7.5), legend=True, lw=2, color="C0")
    else:
        ax1 = i_meas.plot(ax=ax, title=title, legend=True, lw=2, color="C0")

    ax1.title.set_size(20)
    ax1.set_xlabel("time, [s]", fontsize=20)
    ax1.tick_params(axis="both", which="major", labelsize=15)
    ax1.legend(["I_MEAS"], loc="lower left")
    ax1.set_ylabel("I_MEAS [A]", color="C0", fontsize=20)
    ax1.grid()

    ax2 = ax1.twinx()
    u_diode.plot(ax=ax2, legend=True, lw=2, color="C2")
    ax2.set_ylabel("U_DIODE [V]", color="C2", fontsize=20)
    ax2.tick_params(axis="both", which="major", labelsize=15)

    ax2.set_ylim([0, ax2.get_ylim()[1]])

    if ax is None:
        plt.show()


def _filter_u_diode(dataframe: pd.DataFrame, circuit_name: str, source_qds: str):
    """Subset the dataframe columns to those containing the diode signal for the given magnet.
    The name of the column with the diode signal is '<magnet>:U_DIODE_RB' for 'RB' and
     either '<magnet>:U_DIODE_RQ<F/D>' or 'DQQDS.<neighboring qps_crate>.<circuit>:U_REF_N1' for 'RQ'.
    """
    magnet_name = f"MB.{source_qds}" if circuit_name.startswith("RB") else f"MQ.{source_qds}"
    signal_name = "U_DIODE_RB"
    source_name = magnet_name
    if circuit_name.startswith("RQ"):
        crate_name, signal_name = MappingMetadata.get_crate_and_diode_signal_name_for_rq(circuit_name, magnet_name)
        if signal_name == "U_REF_N1":
            source_name = f"DQQDS.{crate_name}.{circuit_name}"

    return dataframe.filter(regex=f"{source_name}:{signal_name}")


def plot_current_resistance_nxcals(u_diode_df: pd.DataFrame, ax=None, *, title="NXCALS") -> None:
    """Function plotting queried current and calculated resistance. Signals used for the resistance calculation come
     from NXCALS

    :param u_diode_df: dataframe with U_DIODE and I_MEAS
    :param ax: axis on which the plot is displayed
    :param title: plot title
    :return: None
    """
    if ax is None:
        ax1 = u_diode_df.filter(regex="I_MEAS").plot(title=title, figsize=(15, 7.5), legend=None, lw=2, color="C0")
    else:
        ax1 = u_diode_df.filter(regex="I_MEAS").plot(ax=ax, title=title, legend=None, lw=2, color="C0")

    ax1.title.set_size(20)
    ax1.set_xlabel("time, [s]", fontsize=20)
    ax1.tick_params(axis="both", which="major", labelsize=15)
    ax1.legend(["I_MEAS"], loc="lower left")
    ax1.set_ylabel("I_MEAS [A]", color="C0", fontsize=20)
    ax1.grid()

    ax2 = ax1.twinx()
    u_diode_df["RES_filtered"].plot(ax=ax2, legend=True, lw=2, color="C2")
    ax2.set_ylabel("R_DIODE_LEADS(t) (Calculated) [uOhm]", color="C2", fontsize=20)
    ax2.tick_params(axis="both", which="major", labelsize=15)
    ax2.legend(["R_DIODE_LEADS(t)"], loc="upper right")

    x_text = 100
    res_max_cals, t_res_max_cals, i_res_max_cals = extract_res_max_time_current_nxcals(u_diode_df, "I_MEAS")
    plot_text = "r_max = {:.1f} uOhm\ni_r_max = {} A\nt_r_max = {} s".format(
        res_max_cals, int(i_res_max_cals), int(t_res_max_cals)
    )
    ax2.annotate(
        plot_text,
        xytext=(x_text, res_max_cals / 2),
        xy=(t_res_max_cals, res_max_cals),
        arrowprops=dict(facecolor="white", shrink=0.05),
        fontsize=15,
    )
    if ax is None:
        plt.show()


def plot_current_voltage_resistance_pm_nxcals(
    source_qds: str, circuit_name: str, timestamp_qds: int, u_diode_pm: pd.DataFrame, u_diode_nxcals: pd.DataFrame
) -> None:
    """Function plotting current, voltage, and resistance from PM and NXCALS in a single plot with two subplots.

    :param source_qds: name of the QDS source (magnet name)
    :param circuit_name: circuit name
    :param timestamp_qds: QDS PM timestamp
    :param u_diode_pm: DataFrame with columns for all considered signals from PM
    :param u_diode_nxcals: DataFrame with columns for all considered signals from NXCALS
    :return: None
    """
    fig, ax = plt.subplots(2, 2, figsize=(30, 15))
    title = "Circuit: {}, Magnet: {}, Time Stamp: {}, I_MEAS(t), U_DIODE(t), " "and R_DIODE_LEADS(t)".format(
        circuit_name, source_qds, Time.to_string_short(timestamp_qds)
    )
    fig.suptitle(title, fontsize=20)
    # # PM
    plot_current_voltage_pm(source_qds, u_diode_pm, timestamp_qds, ax=ax[0, 0])
    plot_current_resistance_pm(circuit_name, source_qds, u_diode_pm, timestamp_qds, ax=ax[1, 0])
    # # NXCALS
    plot_current_voltage_nxcals(circuit_name, source_qds, u_diode_nxcals, ax=ax[0, 1])
    plot_current_resistance_nxcals(u_diode_nxcals, ax=ax[1, 1])
    plt.show()


def plot_current_voltage_resistance_nxcals(
    source_qds: str, circuit_name: str, timestamp_qds: int, u_diode_nxcals: pd.DataFrame
) -> None:
    """Function plotting current, voltage, and resistance from NXCALS

    :param source_qds: QDS name
    :param u_diode_nxcals: DataFrame with U_DIODE, I_MEAS, and RES
    :return: None
    """
    fig, ax = plt.subplots(2, figsize=(15, 15))
    title = "Circuit: {}, Magnet: {}, Time Stamp: {}, I_MEAS(t), U_DIODE(t), " "and R_DIODE_LEADS(t)".format(
        circuit_name, source_qds, Time.to_string_short(timestamp_qds)
    )
    fig.suptitle(title, fontsize=20)

    plot_current_voltage_nxcals(circuit_name, source_qds, u_diode_nxcals, ax=ax[0])
    plot_current_resistance_nxcals(u_diode_nxcals, ax=ax[1])
    plt.show()


def plot_current_voltage_pm(source_qds: str, res_df: pd.DataFrame, timestamp_nqps: int, ax=None) -> None:
    """Function plotting current and voltage from PM

    :param source_qds: QDS name
    :param res_df: DataFrame with current and voltage columns
    :param timestamp_nqps: nQPS PM timestamp
    :param ax: axis on which the plot is displayed
    :return: None
    """
    if ax is None:
        title = "Magnet: {}, Time Stamp: {}, " "IAB.I_A(t) and U_DIODE(t)".format(
            source_qds, Time.to_string_short(timestamp_nqps)
        )
        ax1 = res_df["IAB.I_A"].plot(figsize=(15, 10), title=title, legend=True, lw=2, color="C0")
    else:
        ax1 = res_df["IAB.I_A"].plot(ax=ax, title="PM", legend=True, lw=2, color="C0")
    ax1.title.set_size(20)
    ax1.set_xlabel("time, [s]", fontsize=20)
    ax1.tick_params(axis="both", which="major", labelsize=15)
    ax1.legend(["IAB.I_A"], loc="lower left")
    ax1.set_ylabel("IAB.I_A [A]", color="C0", fontsize=20)
    ax1.grid()

    ax2 = ax1.twinx()
    res_df["U_DIODE_A"].plot(ax=ax2, lw=2, color="C2")
    res_df["U_DIODE_B"].plot(ax=ax2, lw=2, color="C3")
    res_df["U_REF"].plot(ax=ax2, lw=2, color="C4")
    ax2.set_ylabel("U_DIODE [V]", color="C2", fontsize=20)
    ax2.legend(["U_DIODE_A", "U_DIODE_B", "U_REF"], loc="upper right")
    ax2.tick_params(axis="both", which="major", labelsize=15)

    if ax is None:
        plt.show()


def plot_current_resistance_pm(
    circuit_name: str, source_qds: str, res_df: pd.DataFrame, timestamp_nqps: int, ax=None
) -> None:
    """Function plotting current and resistance for signals queried from PM

    :param circuit_name: circuit name
    :param source_qds: QDS name (for title)
    :param res_df: DataFrame with IAB.I_A and RES columns
    :param timestamp_nqps: nQPS PM timestamps
    :param ax: axis on which the plot is displayed
    :return: None
    """
    if ax is None:
        title = "Magnet: {}, Time Stamp: {}, " "IAB.I_A(t) and R_DIODE_LEADS(t)".format(
            source_qds, Time.to_string_short(timestamp_nqps)
        )
        ax1 = res_df["IAB.I_A"].plot(figsize=(15, 10), title=title, legend=True, grid=True, color="C0")
    else:
        ax1 = res_df["IAB.I_A"].plot(ax=ax, title="PM", legend=True, grid=True, color="C0")
    ax1.title.set_size(20)
    ax1.set_xlabel("time, [s]", fontsize=20)
    ax1.tick_params(axis="both", which="major", labelsize=15)
    ax1.legend(["IAB.I_A"], loc="lower left")
    ax1.set_ylabel("IAB.I_A [A]", color="C0", fontsize=20)

    ax2 = ax1.twinx()
    res_df["RES"].plot(ax=ax2, legend=True, color="C2")
    ax2.legend(["R_DIODE_LEADS"], loc="upper right")
    ax2.tick_params(axis="both", which="major", labelsize=15)
    ax2.set_ylabel("R_DIODE_LEADS(t) (Calculated) [uOhm]", color="C2", fontsize=20)

    x_text = 0.5 if "RB" in circuit_name else 1

    res_max_pm, t_res_max_pm, i_res_max_pm = extract_res_max_time_current_pm(res_df)

    plot_text = "r_max = {:.1f} uOhm\ni_r_max = {} A\nt_r_max = {:.3f} s".format(
        res_max_pm, int(i_res_max_pm), t_res_max_pm
    )
    ax2.annotate(
        plot_text,
        xytext=(t_res_max_pm - x_text, res_max_pm / 2),
        xy=(t_res_max_pm, res_max_pm),
        arrowprops=dict(facecolor="white", shrink=0.05),
        fontsize=15,
    )
    ax2.tick_params(axis="both", which="major", labelsize=15)

    if ax is None:
        plt.show()


class DiodeLeadResistanceAnalysis(CircuitAnalysis):
    """Base class for diode lead resistance analysis"""

    def analyze_diode_leads(
        self,
        source_timestamp_qds_df: pd.DataFrame,
        timestamp_fgc: int,
        i_quench_df: pd.DataFrame,
        circuit_name: str,
        i_meas_u_diode_u_ref_pm_dfs: List[List[pd.DataFrame]],
        i_meas_u_diode_nxcals_dfs: List[List[pd.DataFrame]],
        board_toggling_period: int,
    ) -> None:
        """Method analyzing and plotting diode leads resistance based on signals from PM and NXCALS. Initial part of
        the necessary voltage and current and stored in PM and the remainder is logged in NXCALS (with nQPS board
        toggling). Maximum values of resistances are reported.

        :param source_timestamp_qds_df: source and timestamp QDS PM DataFrame
        :param timestamp_fgc: FGC PM timestamp
        :param i_quench_df: quench current for each magnet (if the current is below 1 kA, resistance is not calculated)
        :param circuit_name: name of the circuit
        :param i_meas_u_diode_u_ref_pm_dfs: list of list with voltage and current signals from PM
        :param i_meas_u_diode_nxcals_dfs: list of list with voltage and current signals from NXCALS
        :param board_toggling_period: toggling period of nQPS U_DIODE signals in NXCALS, expressed in number of points
        :return:
        """

        if source_timestamp_qds_df.empty:
            warnings.warn("Analysis skipped since there's no data in QDS dataframe.")
            return

        index_max = source_timestamp_qds_df.index[-1]
        for index, row in source_timestamp_qds_df.iterrows():
            source_qds, timestamp_qds = row[["source", "timestamp"]]
            i_quench = i_quench_df[index]
            dt_wrt_fgc = source_timestamp_qds_df["timestamp"][index] - timestamp_fgc

            print(
                "{}/{}: Analysing diode lead resistance of {} on {}, {}".format(
                    index, index_max, source_qds, Time.to_string(timestamp_qds), timestamp_qds
                )
            )
            print("dt w.r.t. FGC = {} s".format(dt_wrt_fgc * 1e-9))

            if i_quench < 1000:
                print("The analysis skipped as the quench current is below 1 kA ({} A)".format(i_quench))
                continue

            # NXCALS
            i_meas_diode_df, u_diode_df = deepcopy(i_meas_u_diode_nxcals_dfs[index])

            if len(u_diode_df) < 1000:
                print("The analysis skipped as the diode measurement has only {} data points".format(len(u_diode_df)))
                continue

            try:
                res_diode_nxcals_df = calculate_resistance_nxcals(
                    dt_wrt_fgc, circuit_name, i_meas_diode_df, u_diode_df, board_toggling_period
                )
            except ValueError as err:
                print("The analysis skipped as as we encountered an exception: {}".format(err))
                continue

            # PM
            if 1e-9 * (timestamp_qds - timestamp_fgc) < 2:
                i_a_df, u_diode_a_df, u_diode_b_df, u_ref_df = deepcopy(i_meas_u_diode_u_ref_pm_dfs[index])
                if not i_a_df.empty and not u_diode_a_df.empty and not u_diode_b_df.empty and not u_ref_df.empty:
                    try:
                        res_diode_pm_df = calculate_resistance_pm(
                            i_a_df, u_diode_a_df, u_diode_b_df, u_ref_df, timestamp_qds, time_corr=0.0
                        )
                        res_max_pm, _, i_res_max_pm = extract_res_max_time_current_pm(res_diode_pm_df)
                        plot_current_voltage_resistance_pm_nxcals(
                            source_qds, circuit_name, timestamp_qds, res_diode_pm_df, res_diode_nxcals_df
                        )
                    except:
                        res_max_pm = 0
                        i_res_max_pm = 0
                        warnings.warn(
                            "Computation of diode resistance failed, "
                            "please check the input signals and analysis assumptions.",
                            stacklevel=2,
                        )

                else:
                    res_max_pm = 0
                    i_res_max_pm = 0
                    print("The analysis skipped as it seems that there was no quench.")
            else:
                res_max_pm = 0
                i_res_max_pm = 0
                plot_current_voltage_resistance_nxcals(source_qds, circuit_name, timestamp_qds, res_diode_nxcals_df)

            # Maximum resistance value
            res_max_nxcals, _, i_res_max_nxcals = extract_res_max_time_current_nxcals(res_diode_nxcals_df, "I_MEAS")

            if res_max_nxcals > res_max_pm:
                res_max = res_max_nxcals
                i_res_max = i_res_max_nxcals
            else:
                res_max = res_max_pm
                i_res_max = i_res_max_pm

            if res_max > 50:
                warnings.warn("Warning: diode leads resistance %s is above 50 uOhm!" % res_max, stacklevel=2)
            elif res_max > 150:
                warnings.warn("Alarm: diode leads resistance %s is above 150 uOhm!" % res_max, stacklevel=2)
            else:
                print("Diode lead resistance is %s uOhm." % round(res_max, 2))

            # Update results table
            if "RQ" in circuit_name:
                col_r = "R_DL_max_%s" % circuit_name.split(".")[0]
                col_i = "I_%s at R_DL_max_%s" % (circuit_name.split(".")[0], circuit_name.split(".")[0])
            else:
                col_r = "R_DL_max"
                col_i = "I at R_DL_max"

            self.results_table.loc[index, col_r] = round(res_max, 2)
            self.results_table.loc[index, col_i] = round(i_res_max, 2)

            # Show next
            check_show_next(index, index_max, self.is_automatic)
