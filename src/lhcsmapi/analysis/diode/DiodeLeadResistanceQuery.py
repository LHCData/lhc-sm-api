import warnings
from typing import Union, List, Tuple

import pandas as pd
from pyspark.sql.session import SparkSession
from tqdm.notebook import tqdm

from lhcsmapi.Time import Time
from lhcsmapi.analysis.CircuitQuery import CircuitQuery, execution_count
from lhcsmapi.metadata.MappingMetadata import MappingMetadata
from lhcsmapi.api.query_builder import QueryBuilder


class DiodeLeadResistanceQuery(CircuitQuery):
    """Class containing methods for query of events and signals for diode lead resistance calculation."""

    def __init__(self, circuit_type, circuit_name, max_executions=None, verbose=True):
        super().__init__(circuit_type, circuit_name, max_executions, verbose)

        if isinstance(circuit_name, str):  # TODO change in SIGMON-239
            self._diode_system = f'DIODE_{self.circuit_name.split(".")[0]}'
            self._magnet_template = "MB.{}" if self.circuit_type == "RB" else "MQ.{}"
            self._u_diode_signal = f'U_DIODE_{self.circuit_name.split(".")[0]}'

    @execution_count()
    def find_source_timestamp_nqps(
        self,
        t_start: Union[int, str, float],
        duration: List[Tuple[int, str]] = [(10, "s"), (400, "s")],
        warn_on_missing_pm_buffers: bool = False,
    ) -> pd.DataFrame:
        """Method searching PM nQPS events for diode query
        If the start is NaN, then an empty dataframe is returned.

        :param t_start: start time of a search
        :param duration: search duration before and after the start time
        :param warn_on_missing_pm_buffers: Warn if not all nQPS PM buffers were found
        :return: pd.DataFrame with source and timestamp of the PM nQPS events
        """
        result = (
            QueryBuilder()
            .with_pm()
            .with_duration(t_start=t_start, duration=duration)
            .with_circuit_type(self.circuit_type)
            .with_metadata(circuit_name=self.circuit_name, system=self._diode_system, source="*")
            .event_query(verbose=self.verbose)
            .filter_source(self.circuit_type, self.circuit_name, self._diode_system)
            .sort_values(by=["timestamp", "source"])
            .drop_duplicate_source()
            .get_dataframe()
        )

        if warn_on_missing_pm_buffers:
            self._warn_if_pm_buffers_are_missing(result, t_start, duration)

        return result

    @execution_count()
    def query_current_voltage_diode_leads_pm(
        self, timestamp_fgc: int, source_timestamp_qds_df: pd.DataFrame
    ) -> List[Tuple[pd.DataFrame, pd.DataFrame, pd.DataFrame, pd.DataFrame]]:
        """Method querying PM for current and voltage of a diode

        :param timestamp_fgc: FGC PM event timestamp (ns precision) to which the query is synchronised
        :param source_timestamp_qds_df: source timestamp qds table
        :return: List of tuples of current and voltage pd.DataFrames
        """
        i_meas_u_diode_u_ref_pm_dfs = []
        for index, row in tqdm(
            source_timestamp_qds_df.iterrows(), total=source_timestamp_qds_df.shape[0], desc="Querying U, I Diode PM"
        ):
            source_qds, timestamp_qds = row[["source", "timestamp"]]
            is_nqps_in_pm = 1e-9 * (timestamp_qds - timestamp_fgc) < 2

            i_meas_u_diode_u_ref_pm_df = (pd.DataFrame, pd.DataFrame, pd.DataFrame, pd.DataFrame)

            if is_nqps_in_pm:
                source_timestamp_nqps_df = self._find_source_timestamp_nqps_pm(source_qds, timestamp_qds)
                if not source_timestamp_nqps_df.empty:
                    timestamp_nqps_a = source_timestamp_nqps_df.loc[0, "timestamp"]
                    timestamp_nqps_b = source_timestamp_nqps_df.loc[1, "timestamp"]

                    i_a_df = self._query_i_diode_pm(timestamp_fgc)
                    u_diode_a_df, u_diode_b_df = self._query_u_diode_pm(source_qds, timestamp_nqps_a, timestamp_nqps_b)
                    u_ref_df = self._query_u_diode_ref_pm(source_qds, timestamp_nqps_a)

                    i_meas_u_diode_u_ref_pm_df = (i_a_df, u_diode_a_df, u_diode_b_df, u_ref_df)

            i_meas_u_diode_u_ref_pm_dfs.append(i_meas_u_diode_u_ref_pm_df)

        return i_meas_u_diode_u_ref_pm_dfs

    @execution_count()
    def query_current_voltage_diode_leads_nxcals(
        self,
        source_timestamp_qds_df: pd.DataFrame,
        *,
        spark: SparkSession,
        duration: List[Tuple[int, str]] = [(50, "s"), (150, "s")],
    ) -> List[Tuple[pd.DataFrame, pd.DataFrame]]:
        """Method querying NXCALS for current and voltage of a diode

        :param source_timestamp_qds_df: analysis results table
        :param spark: spark session variable needed to perform an NXCALS query
        :param duration: search duration before and after the QDS timestamp which is taken from source_timestamp_qds_df
        :return: List of tuples of current and voltage pd.DataFrames
        """
        i_meas_u_diode_nxcals_dfs = []
        for index, row in tqdm(
            source_timestamp_qds_df.iterrows(),
            total=source_timestamp_qds_df.shape[0],
            desc="Querying U, I Diode leads NXCALS",
        ):
            source_qds, timestamp_qds = row[["source", "timestamp"]]

            timestamp_qds = Time.to_unix_timestamp(timestamp_qds)

            i_meas_diode_df = self._query_i_diode_nxcals(timestamp_qds, spark=spark, duration=duration)
            u_diode_rqd_df = self._query_u_diode_nxcals(source_qds, timestamp_qds, spark=spark, duration=duration)

            i_meas_u_diode_nxcals_dfs.append((i_meas_diode_df, u_diode_rqd_df))

        return i_meas_u_diode_nxcals_dfs

    def _warn_if_pm_buffers_are_missing(
        self, source_timestamp_nqps_df: pd.DataFrame, t_start: Union[int, str, float], duration: List[Tuple[int, str]]
    ):
        """Displays a warning if any of nQPS PM buffers are missing in the given pd.DataFrame

        :param source_timestamp_nqps_df: query result with nQPS PM buffers in 'source' column
        :param t_start: start time of a search for logging purposes
        :param duration: search duration before and after the start time for logging purposes
        """
        crates = MappingMetadata.get_crates_for_circuit_names(self.circuit_type, self.circuit_name)
        found_sources = source_timestamp_nqps_df["source"].tolist() if "source" in source_timestamp_nqps_df else []
        missing_buffers = [crate for crate in crates if crate not in found_sources]
        if len(missing_buffers) > 0:
            warnings.warn(
                f"Some nQPS PM buffers missing. {missing_buffers} not found for {self.circuit_name} "
                f"within {duration} around {t_start}."
            )

    def _find_source_timestamp_nqps_pm(self, source_qds: str, timestamp_qds: int) -> pd.DataFrame:
        qps_crate = MappingMetadata.get_crate_name_from_magnet_name(self.circuit_type, source_qds)

        return (
            QueryBuilder()
            .with_pm()
            .with_duration(t_start=timestamp_qds, duration=[(600, "s"), (600, "s")])
            .with_circuit_type(self.circuit_type)
            .with_metadata(circuit_name=self.circuit_name, system=self._diode_system, source=qps_crate)
            .event_query(verbose=self.verbose)
            .get_dataframe()
        )

    def _query_i_diode_pm(self, timestamp_fgc):
        return (
            QueryBuilder()
            .with_pm()
            .with_timestamp(timestamp_fgc)
            .with_circuit_type(self.circuit_type)
            .with_metadata(circuit_name=self.circuit_name, system="PC", signal="I_A")
            .signal_query()
            .get_dataframes()
        )

    def _query_u_diode_pm(
        self, source_qds: str, timestamp_qds_a: int, timestamp_qds_b: int
    ) -> Tuple[pd.DataFrame, pd.DataFrame]:
        metadata = self._get_u_query_metadata(source_qds)

        def query(timestamp):
            return (
                QueryBuilder()
                .with_pm()
                .with_timestamp(timestamp)
                .with_circuit_type(self.circuit_type)
                .with_metadata(**metadata)
                .signal_query()
                .get_dataframes()
            )

        return query(timestamp_qds_a), query(timestamp_qds_b)

    def _query_u_diode_ref_pm(self, source_qds: str, timestamp_qds: int) -> pd.DataFrame:
        qps_crate = MappingMetadata.get_crate_name_from_magnet_name(self.circuit_type, source_qds)

        return (
            QueryBuilder()
            .with_pm()
            .with_timestamp(timestamp_qds)
            .with_circuit_type(self.circuit_type)
            .with_metadata(
                circuit_name=self.circuit_name,
                system=self._diode_system,
                signal="U_REF_N1",
                source=qps_crate,
                wildcard={"QPS_CRATE": qps_crate},
            )
            .signal_query()
            .get_dataframes()
        )

    def _query_i_diode_nxcals(self, timestamp_qds: int, *, spark, duration) -> pd.DataFrame:
        return (
            QueryBuilder()
            .with_nxcals(spark)
            .with_duration(t_start=timestamp_qds, duration=duration)
            .with_circuit_type(self.circuit_type)
            .with_metadata(circuit_name=self.circuit_name, system="PC", signal="I_MEAS")
            .signal_query()
            .synchronize_time(timestamp_qds)
            .convert_index_to_sec()
            .get_dataframes()
        )

    def _query_u_diode_nxcals(self, source_qds: str, timestamp_qds: int, *, spark, duration) -> pd.DataFrame:
        metadata = self._get_u_query_metadata(source_qds)
        return (
            QueryBuilder()
            .with_nxcals(spark)
            .with_duration(t_start=timestamp_qds, duration=duration)
            .with_circuit_type(self.circuit_type)
            .with_metadata(**metadata)
            .signal_query()
            .synchronize_time(timestamp_qds)
            .convert_index_to_sec()
            .get_dataframes()
        )

    def _get_u_query_metadata(self, source_qds: str) -> dict:
        magnet = self._magnet_template.format(source_qds)
        crate = MappingMetadata.get_crate_name_from_magnet_name(self.circuit_type, magnet)
        return {
            "circuit_name": self.circuit_name,
            "system": self._diode_system,
            "signal": self._u_diode_signal,
            "source": crate,
            "wildcard": {"MAGNET": magnet},
        }


class DiodeLeadResistanceRqQuery(DiodeLeadResistanceQuery):
    """Queries data for diode lead resistance calculation in RQ circuits."""

    def _get_u_query_metadata(self, source_qds) -> dict:
        """For each RQ circuit there are cases where a QPS crate control QH (quench heater) firing for 2 magnets.
        The side effect is that we are missing U_DIODE_RQx signal in PM and NXCALS. Fortunately they are not entirely
        lost because the missing diode signals are used for comparison in the neighboring crate where they are named
        U_REF_N1 plus some cryptical prefix. See https://its.cern.ch/jira/browse/SIGMON-179"""
        metadata = super()._get_u_query_metadata(source_qds)
        crate, signal = MappingMetadata.get_crate_and_diode_signal_name_for_rq(self.circuit_name, f"MQ.{source_qds}")

        if signal == "U_REF_N1":
            metadata["source"] = crate
            metadata["signal"] = signal
            metadata["wildcard"] = {"QPS_CRATE": crate}

        return metadata
