import pandas as pd
import numpy as np
import plotly.graph_objects as go
import plotly.express as px
from tqdm.notebook import tqdm

from lhcsmapi.Time import Time
from lhcsmapi.metadata.MappingMetadata import MappingMetadata
from lhcsmapi.metadata import signal_metadata


def show_schematic(
    circuit_type: str,
    circuit_name: str,
    results_table: pd.DataFrame,
    source_timestamp_nqps_df: pd.DataFrame,
    source_timestamp_leads_odd_df: pd.DataFrame,
    source_timestamp_leads_even_df: pd.DataFrame,
    timestamp_fgc: int,
    source_ee_odd: str,
    timestamp_ee_odd: int,
    source_ee_even: str,
    timestamp_ee_even: int,
    show_magnet_name=False,
    verbose=True,
) -> None:
    """Function preparing positions for an RB circuit schematic with:
    - power converter
    - energy extraction systems
    - current leads
    - magnets (with an order of quenching)
    - nQPS crates
    along with names and timestamps of PM events.

    :param circuit_type: circuit type
    :param circuit_name: circuit name
    :param results_table: a table with FPA analysis results
    :param source_timestamp_nqps_df: PM source and timestamp DataFrame for nQPS
    :param source_timestamp_leads_odd_df: PM source and timestamp DataFrame for leads at the odd point
    :param source_timestamp_leads_even_df: PM source and timestamp DataFrame for leads at the even point
    :param timestamp_fgc: PM FGC timestamp
    :param source_ee_odd: name of an EE odd PM source
    :param timestamp_ee_odd: timestamp of an EE odd pm event
    :param source_ee_even: name of an EE even PM source
    :param timestamp_ee_even: timestamp of an EE even pm event
    :param show_magnet_name: flag to decide whether to show (True) or not (False) magnet name next to the schematic
    :param verbose: flag to descide whether to pring (True) or not (False) information about the progress of schematic
    drawing
    :return: None
    """
    if verbose:
        print("Calculating magnet positions...")
    magnets = get_magnets_visualisation(circuit_type, circuit_name, results_table)

    if verbose:
        print("Calculating nQPS crate positions...")
    crates = get_nqps_crates_visualsation(circuit_type, circuit_name, source_timestamp_nqps_df, magnets)

    pc_df = pd.DataFrame(
        {
            "name": {0: signal_metadata.get_fgc_names(circuit_name, timestamp_fgc)[0]},
            "timestamp": {0: Time.to_string_short(timestamp_fgc)},
            "type": {0: "PC"},
            "delay": {0: 0},
            "w": {0: 1.0},
            "h": {0: 4.0},
            "y0": {0: -2.0},
        }
    )

    ee_even_df = pd.DataFrame(
        {
            "name": {0: source_ee_even},
            "timestamp": {0: Time.to_string_short(timestamp_ee_even)},
            "type": {0: "EE_EVEN"},
            "delay": {0: (timestamp_ee_even - timestamp_fgc) / 1e9},
            "w": {0: 0.5},
            "h": {0: 1},
            "y0": {0: -2.0},
        }
    )

    ee_odd_df = pd.DataFrame(
        {
            "name": {0: source_ee_odd},
            "timestamp": {0: Time.to_string_short(timestamp_ee_odd)},
            "type": {0: "EE_ODD"},
            "delay": {0: (timestamp_ee_odd - timestamp_fgc) / 1e9},
            "w": {0: 0.75},
            "h": {0: 4.0},
            "y0": {0: -2.0},
        }
    )

    if not source_timestamp_leads_odd_df.empty:
        source_leads_odd = source_timestamp_leads_odd_df.at[0, "source"]
        timestamp_leads_odd = source_timestamp_leads_odd_df.at[0, "timestamp"]

        leads_odd_df = pd.DataFrame(
            {
                "name": {0: source_leads_odd, 1: source_leads_odd},
                "timestamp": {
                    0: Time.to_string_short(timestamp_leads_odd),
                    1: Time.to_string_short(timestamp_leads_odd),
                },
                "type": {0: "LEADS_ODD", 1: "LEADS_ODD"},
                "delay": {
                    0: (timestamp_leads_odd - timestamp_fgc) / 1e9,
                    1: (timestamp_leads_odd - timestamp_fgc) / 1e9,
                },
                "x_center": {0: 0.5, 1: 0.5},
                "y_center": {0: 1.5, 1: -1.5},
            }
        )
    else:
        leads_odd_df = pd.DataFrame()

    if not source_timestamp_leads_even_df.empty:
        source_leads_even = source_timestamp_leads_even_df.at[0, "source"]
        timestamp_leads_even = source_timestamp_leads_even_df.at[0, "timestamp"]

        leads_even_df = pd.DataFrame(
            {
                "name": {0: source_leads_even, 1: source_leads_even},
                "timestamp": {
                    0: Time.to_string_short(timestamp_leads_even),
                    1: Time.to_string_short(timestamp_leads_even),
                },
                "type": {0: "LEADS_EVEN", 1: "LEADS_EVEN"},
                "delay": {
                    0: (timestamp_leads_even - timestamp_fgc) / 1e9,
                    1: (timestamp_leads_even - timestamp_fgc) / 1e9,
                },
                "x_center": {0: 79, 1: 79},
                "y_center": {0: 1.5, 1: -1.5},
            }
        )
    else:
        leads_even_df = pd.DataFrame()

    draw_schematic(
        circuit_name,
        magnets,
        crates,
        pc_df,
        ee_even_df,
        ee_odd_df,
        leads_odd_df,
        leads_even_df,
        show_magnet_name,
        verbose,
    )


def get_magnets_visualisation(circuit_type: str, circuit_name: str, results_table: pd.DataFrame) -> pd.DataFrame:
    """Function calculating posistion of magnets to draw on the schematic

    :param circuit_type: circuit type
    :param circuit_name: circuit name
    :param results_table: a table with FPA analysis results
    :return: DataFrame with positions of magnets in the circuit
    """
    pd.set_option("mode.chained_assignment", None)  # SIGMON-342 - verified that the warning can be silenced
    magnet_names = MappingMetadata.get_magnets_for_circuit_names(circuit_type, circuit_name)
    magnets = pd.DataFrame(columns=["magnet"], data=magnet_names)
    magnets["qps_crate"] = magnets.apply(
        lambda row: MappingMetadata.get_crate_name_from_magnet_name(circuit_type, row["magnet"]), axis=1
    )

    # join with results_table
    if "timestamp_iqps" in results_table.columns:
        sub_results_table = results_table[["Position", "I_Q_M", "timestamp_iqps"]]
        sub_results_table["datetime_iqps"] = results_table["timestamp_iqps"].apply(
            lambda col: Time.to_string_short(col)
        )
    else:
        sub_results_table = results_table[["Position", "I_Q_M"]]
        sub_results_table["datetime_iqps"] = np.nan

    sub_results_table["q_order"] = sub_results_table.index
    sub_results_table["magnet"] = sub_results_table.apply(lambda row: "MB." + row["Position"], axis=1)

    magnets = magnets.merge(sub_results_table, on="magnet", how="outer")

    # magnet rectangle
    magnets["x0"] = magnets.apply(lambda row: 1 + row.name / 2, axis=1)
    magnets["x1"] = magnets.apply(lambda row: 1.25 + row.name / 2, axis=1)
    magnets["y0"] = magnets.apply(lambda row: 1 if row.name % 2 == 0 else -1, axis=1)
    magnets["y1"] = magnets.apply(lambda row: 2 if row.name % 2 == 0 else -2, axis=1)

    # magnet name
    magnets["x_text"] = magnets.apply(lambda row: 1.125 + row.name / 2, axis=1)
    magnets["y_text"] = magnets.apply(lambda row: 2.25 if row.name % 2 == 0 else -2.25, axis=1)

    # magnet centered hover text
    magnets["x_center"] = magnets.apply(lambda row: 1.125 + row.name / 2, axis=1)
    magnets["y_center"] = magnets.apply(lambda row: 1.5 if row.name % 2 == 0 else -1.5, axis=1)

    magnets["is_quenched"] = magnets.apply(lambda row: str(not np.isnan(row["I_Q_M"])), axis=1)
    pd.set_option("mode.chained_assignment", "warn")

    return magnets


def get_nqps_crates_visualsation(
    circuit_type: str, circuit_name: str, source_timestamp_nqps_df: pd.DataFrame, magnets: pd.DataFrame
) -> pd.DataFrame:
    """Function calculating nQPS crates positions for visualization

    :param circuit_type: circuit type
    :param circuit_name: circuit name
    :param source_timestamp_nqps_df: nQPS source and timestamp DataFrame
    :param magnets: DataFrame with magnet positions
    :return: DataFrame with nQPS positions
    """
    crate_names = MappingMetadata.get_crates_for_circuit_names(circuit_type, circuit_name)
    crates = pd.DataFrame(columns=["qps_crate"], data=crate_names)
    crates["x0"] = crates.apply(lambda row: -1 + 3 * row.name / 2, axis=1)
    crates["x1"] = crates.apply(lambda row: 1.25 + 3 * row.name / 2, axis=1)
    crates["y0"] = crates.apply(lambda row: 0.25 if row.name % 2 == 0 else -0.25, axis=1)
    crates["y1"] = crates.apply(lambda row: 0.75 if row.name % 2 == 0 else -0.75, axis=1)
    # Correcting exceptions
    # # Line A
    crates.at[0, "x0"] += 2
    crates.at[52, "x1"] -= 2
    # # Line B
    crates["x0"] = crates.apply(lambda row: row["x0"] - 1 if row.name % 2 == 1 else row["x0"], axis=1)
    crates["x1"] = crates.apply(lambda row: row["x1"] - 1 if row.name % 2 == 1 else row["x1"], axis=1)
    crates.at[1, "x0"] += 2
    crates.at[53, "x1"] -= 2
    # Center for the hover text
    crates["x_center"] = crates.apply(lambda row: (row["x0"] + row["x1"]) / 2, axis=1)
    crates["y_center"] = crates.apply(lambda row: (row["y0"] + row["y1"]) / 2, axis=1)
    crates.head()

    # add timestamp_nqps
    crates = crates.merge(source_timestamp_nqps_df, left_on="qps_crate", right_on="source", how="left")
    crates["timestamp"] = crates["timestamp"].apply(lambda x: Time.to_string_short(x))

    # add quenched qps_crates
    sub_magnets = magnets[magnets["is_quenched"] == "True"][["qps_crate", "is_quenched"]]
    sub_magnets = sub_magnets.drop_duplicates("qps_crate")
    crates = crates.merge(sub_magnets, on="qps_crate", how="outer")
    crates = crates.fillna(value={"is_quenched": "False"})

    return crates


def draw_schematic(
    circuit_name: str,
    magnets: pd.DataFrame,
    crates: pd.DataFrame,
    pc_df: pd.DataFrame,
    ee_even_df: pd.DataFrame,
    ee_odd_df: pd.DataFrame,
    leads_odd_df: pd.DataFrame,
    leads_even_df: pd.DataFrame,
    show_magnet_name=False,
    verbose=True,
) -> None:
    """Function drawing an RB circuit schematic with:
    - power converter
    - energy extraction systems
    - current leads
    - magnets (with an order of quenching)
    - nQPS crates
    along with names and timestamps of PM events.

    :param circuit_name: circuit name
    :param magnets: DataFrame with magnet positions
    :param crates: DataFrame with nQPS positions
    :param pc_df: DataFrame with PC source and timestamp
    :param ee_even_df: DataFrame with EE even source and timestamp
    :param ee_odd_df: DataFrame with EE odd source and timestamp
    :param leads_odd_df: DataFrame with LEADS odd source and timestamp
    :param leads_even_df: DataFrame with LEADS even source and timestamp
    :param show_magnet_name: flag to decide whether to show (True) or not (False) magnet name next to the schematic
    :param verbose: flag to descide whether to pring (True) or not (False) information about the progress of schematic
    drawing
    :return: None
    """
    if verbose:
        print("Initializing plot...")
    magnets_quenched = magnets[magnets["is_quenched"] == "True"]
    magnets_not_quenched = magnets[magnets["is_quenched"] == "False"]

    fig = px.scatter(
        magnets_quenched,
        x="x_center",
        y="y_center",
        hover_name="magnet",
        hover_data=["qps_crate", "I_Q_M", "datetime_iqps"],
        color="is_quenched",
        color_discrete_sequence=px.colors.qualitative.Set1,
    )
    fig2 = px.scatter(
        magnets_not_quenched,
        x="x_center",
        y="y_center",
        hover_name="magnet",
        hover_data=["qps_crate"],
        color="is_quenched",
        color_discrete_sequence=px.colors.qualitative.G10,
    )
    fig.add_trace(fig2.data[0])

    if verbose:
        print("Plotting nQPS crates with quenches...")

    crates_quenched = crates[crates["is_quenched"] == "True"]
    crates_not_quenched = crates[crates["is_quenched"] == "False"]
    fig3 = px.scatter(
        crates_quenched,
        x="x_center",
        y="y_center",
        hover_name="qps_crate",
        hover_data=["qps_crate"],
        color="is_quenched",
        color_discrete_sequence=px.colors.qualitative.Set1,
    )

    fig.add_trace(fig3.data[0])
    if verbose:
        print("Plotting remaining nQPS crates...")

    fig3 = px.scatter(
        crates_not_quenched,
        x="x_center",
        y="y_center",
        hover_name="qps_crate",
        hover_data=["qps_crate"],
        color="is_quenched",
        color_discrete_sequence=["yellow"],
    )

    fig.add_trace(fig3.data[0])

    # Line A
    fig.add_shape(type="line", x0=0, y0=1.5, x1=80, y1=1.5, line=dict(color="RoyalBlue", width=2))

    # Line B
    fig.add_shape(type="line", x0=0, y0=-1.5, x1=80, y1=-1.5, line=dict(color="RoyalBlue", width=2))

    for index, row in tqdm(magnets.iterrows(), total=magnets.shape[0], desc="Drawing magnets"):
        if show_magnet_name:
            fig.add_trace(go.Scatter(x=[row["x_text"]], y=[row["y_text"]], text=[row["magnet"]], mode="text"))

        # Add quench order
        if row["is_quenched"] == "True":
            y = 0 if show_magnet_name else row["y1"] + row["y1"] / abs(row["y1"]) * 0.5
            fig.add_trace(go.Scatter(x=[row["x_center"]], y=[y], text=[str(int(row["q_order"]) + 1)], mode="text"))

        color = "Red" if row["is_quenched"] == "True" else "LightSkyBlue"

        # Add MB rectangle
        fig.add_shape(
            type="rect",
            x0=row["x0"],
            y0=row["y0"],
            x1=row["x1"],
            y1=row["y1"],
            line=dict(color="RoyalBlue", width=4),
            fillcolor=color,
        )
    for index, row in tqdm(crates.iterrows(), total=crates.shape[0], desc="Drawing nQPS crates"):
        # Add QPS Board
        color = "Red" if row["is_quenched"] == "True" else "Gray"
        fig.add_shape(
            type="rect",
            x0=row["x0"],
            y0=row["y0"],
            x1=row["x1"],
            y1=row["y1"],
            line=dict(color="Yellow", width=4),
            fillcolor=color,
        )

    if circuit_name in ["RB.A12", "RB.A34", "RB.A56", "RB.A78"]:
        pc_df["x0"] = 79.25
        ee_even_df["x0"] = 78.25
        ee_odd_df["x0"] = -0.5
        x_axis_range = [65, 82]
    else:
        pc_df["x0"] = -1
        ee_even_df["x0"] = 0.5
        ee_odd_df["x0"] = 78.5
        x_axis_range = [-2, 15]

    for df in [pc_df, ee_even_df, ee_odd_df]:
        df["x1"] = df["x0"] + df["w"]
        df["y1"] = df["y0"] + df["h"]
        df["x_center"] = (df.at[0, "x0"] + df.at[0, "x1"]) / 2
        df["y_center"] = (df.at[0, "y0"] + df.at[0, "y1"]) / 2

        fig.add_shape(
            type="rect",
            x0=float(df.at[0, "x0"]),
            y0=float(df.at[0, "y0"]),
            x1=float(df.at[0, "x1"]),
            y1=float(df.at[0, "y1"]),
            line=dict(color="Gray", width=4),
            fillcolor="Gray",
        )

        fig4 = px.scatter(df, x="x_center", y="y_center", hover_name="name", hover_data=["type", "timestamp", "delay"])

        fig.add_trace(fig4.data[0])

    if not leads_odd_df.empty:
        fig5 = px.scatter(
            leads_odd_df, x="x_center", y="y_center", hover_name="name", hover_data=["type", "timestamp", "delay"]
        )
        fig.add_trace(fig5.data[0])

    if not leads_even_df.empty:
        fig6 = px.scatter(
            leads_even_df, x="x_center", y="y_center", hover_name="name", hover_data=["type", "timestamp", "delay"]
        )
        fig.add_trace(fig6.data[0])

    if verbose:
        print("Rendering figure...")

    # Set axes properties
    fig.update_xaxes(range=x_axis_range, showgrid=False)
    fig.update_yaxes(range=[-3, 3])

    fig.update_layout(xaxis=dict(rangeslider=dict(visible=True)))

    fig.update_shapes(dict(xref="x", yref="y"))
    fig.layout.update(showlegend=False)
    fig.show()
