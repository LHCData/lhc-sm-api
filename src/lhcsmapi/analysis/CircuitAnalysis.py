import warnings
from typing import Union, Dict, List

import numpy as np
import pandas as pd
from IPython.display import display, HTML

from lhcsmapi.Time import Time
from lhcsmapi.analysis.AnalysisResult import AnalysisResult
from lhcsmapi.analysis.warnings import warning_on_one_line

warnings.formatwarning = warning_on_one_line


def append(data_structure: Union[Dict, pd.DataFrame], col: str, values: List[float], suffixes=("",)) -> None:
    """Method appending a value for a column or a list of values for a column with multiple suffixes.

    :param data_structure: a data structure to be updated (either a dictionary or a pd.DataFrame)
    :param col: key/column to be added
    :param values: values for the key/column
    :param suffixes: list of suffixes for each column if values are a list (to append many values)
    :return: None as the operation is done inplace
    """
    suffixes = [""] * len(values) if suffixes[0] == "" else suffixes
    for index, tau in enumerate(values):
        data_structure["%s%s" % (col, suffixes[index])] = tau


class CircuitAnalysis(object):
    """Base class for circuit analysis. It contains methods for displaying timestamp tables (actual and reference)"""

    def __init__(self, circuit_type, results_table=None, is_automatic=True, colormap="default"):
        self.circuit_type = circuit_type
        self.results_table = results_table
        self.is_automatic = is_automatic
        self.colormap = colormap
        self.analysis_result = AnalysisResult()

    def create_timestamp_table(
        self, timestamp_dct: dict, circuit_name: str = "", qps_fgc_max_delay: float = 0.04
    ) -> None:
        """Method creating a reference timestamp table for display in a notebook. It analyzes precedence of timestamps
        and displays warnings in case some rules are violated:
        - the first nQPS timestamp before the first iQPS timestamp (for RB and RQ)
        - FGC as the first timestamp (indicates a PC trip)
        - iQPS/nQPS synchronized to FGC timestamp for more than 20 ms

        :param timestamp_dct: dictionary of timestamps and source&timestamp DataFrames
        :param circuit_name: name of a circuit
        :param qps_fgc_max_delay: maximum time delay between FGC and QPS timestamps in s
        :return: None
        """

        timestamp_dct_filtered = {}
        for key, value in timestamp_dct.items():
            if isinstance(value, pd.DataFrame):
                if not value.empty:
                    timestamp_dct_filtered[key] = value.loc[0, "timestamp"]
                else:
                    timestamp_dct_filtered[key] = 0
            else:
                timestamp_dct_filtered[key] = value if not np.isnan(value) else 0

        timestamp_df = pd.DataFrame(
            {"Source": list(timestamp_dct_filtered.keys()), "Timestamp": list(timestamp_dct_filtered.values())}
        )
        timestamp_df["Date and time"] = timestamp_df["Timestamp"].apply(
            lambda col: Time.to_string_short(col) if col != 0 else 0
        )

        timestamp_df.sort_values(by="Timestamp", inplace=True)
        timestamp_df.reset_index(inplace=True, drop=True)

        # Put zero rows at the end
        zero_rows = timestamp_df.loc[timestamp_df["Timestamp"] == 0]
        non_zero_rows = timestamp_df.loc[timestamp_df["Timestamp"] != 0]
        timestamp_df = pd.concat([non_zero_rows, zero_rows])
        timestamp_df.reset_index(inplace=True, drop=True)

        # Compute time difference w.r.t. the first event
        timestamp_first_event = timestamp_df.loc[0, "Timestamp"]
        timestamp_df["dt w.r.t. origin [ms]"] = timestamp_df["Timestamp"].apply(
            lambda col: (col - timestamp_first_event) * 1e-6 if col != 0 else 0
        )

        # Compute time difference w.r.t. the FGC
        if any(timestamp_df["Source"].str.contains("FGC")):
            timestamp_fgc = timestamp_df.loc[timestamp_df["Source"].str.contains("FGC"), "Timestamp"].values[0]
            timestamp_df["dt w.r.t. FGC [ms]"] = timestamp_df["Timestamp"].apply(
                lambda col: (col - timestamp_fgc) * 1e-6 if col != 0 else 0
            )

        # Compute time difference w.r.t. the PIC
        if any(timestamp_df["Source"].str.contains("PIC")):
            timestamp_fgc = timestamp_df.loc[timestamp_df["Source"].str.contains("PIC"), "Timestamp"].values[0]
            timestamp_df["dt w.r.t. PIC [ms]"] = timestamp_df["Timestamp"].apply(
                lambda col: (col - timestamp_fgc) * 1e-6 if col != 0 else 0
            )

        # append the timestamps to the results table
        if self.results_table is not None:
            for k, v in timestamp_dct_filtered.items():
                if not (("iqps" in k.lower()) or ("nqps" in k.lower())):
                    self.results_table["timestamp_%s" % k.lower()] = v

        display(HTML(timestamp_df.to_html()))

        if "FGC" in timestamp_df.loc[0, "Source"] and len(timestamp_df) > 1:
            warnings.warn("FGC is the first timestamp, potentially a PC trip not a quench!", stacklevel=2)

        if ("nQPS" in timestamp_dct_filtered) and ("iQPS" in timestamp_dct_filtered):
            if timestamp_dct_filtered["nQPS"] < timestamp_dct_filtered["iQPS"]:
                warnings.warn(
                    "nQPS PM timestamp ({}) was created before iQPS one ({})".format(
                        timestamp_dct_filtered["nQPS"], timestamp_dct_filtered["iQPS"]
                    ),
                    stacklevel=2,
                )

        if "nQPS" in timestamp_dct_filtered and any(timestamp_df["Source"].str.contains("FGC")):
            timestamp_fgc = timestamp_df.loc[timestamp_df["Source"].str.contains("FGC"), "Timestamp"].values[0]
            if abs(timestamp_dct_filtered["nQPS"] - timestamp_fgc) > int(1e9 * qps_fgc_max_delay):
                warnings.warn(
                    "nQPS and FGC PM events are not synchronized to +/- %d ms!" % int(1e3 * qps_fgc_max_delay),
                    stacklevel=2,
                )

        if "iQPS" in timestamp_dct_filtered and any(timestamp_df["Source"].str.contains("FGC")):
            timestamp_fgc = timestamp_df.loc[timestamp_df["Source"].str.contains("FGC"), "Timestamp"].values[0]
            if abs(timestamp_dct_filtered["iQPS"] - timestamp_fgc) > int(1e9 * qps_fgc_max_delay):
                warnings.warn(
                    "iQPS and FGC PM events are not synchronized to +/- %d ms!" % int(1e3 * qps_fgc_max_delay),
                    stacklevel=2,
                )

    @staticmethod
    def create_ref_timestamp_table(timestamp_ref_dct) -> pd.DataFrame:
        """Method creating a reference timestamp table for display in a notebook

        :param timestamp_ref_dct: dictionary of timestamps and source&timestamp DataFrames
        :return: pd.DataFrame with reference timestamps
        """
        timestamp_ref_df = pd.DataFrame(
            {"Source": list(timestamp_ref_dct.keys()), "Timestamp": list(timestamp_ref_dct.values())}
        )
        timestamp_ref_df["Date and time"] = timestamp_ref_df["Timestamp"].apply(lambda col: Time.to_string_short(col))
        return timestamp_ref_df

    def update_results_table(self, col_name, index, value) -> None:
        """

        :param col_name:
        :param value:
        :return: None
        """
        if self.results_table is not None:
            self.results_table.loc[index, col_name] = value
