from IPython.display import display, HTML


def provide_container_style_html() -> str:
    return "<style>.container { width:95% !important; }</style>"


def provide_text_cell_render_html() -> str:
    return (
        "<style>"
        "div.text_cell_render {"
        "      padding: 1pt;"
        "    }"
        "    div#notebook p,"
        "    div#notebook,"
        "    div#notebook li,"
        "    p {"
        "      font-size: 9pt;"
        "      line-height: 135%;"
        "      margin: 0;"
        "    }"
        "    .rendered_html h1,"
        "    .rendered_html h1:first-child {"
        "      font-size: 14pt;"
        "      margin: 7pt 0;"
        "    }"
        "        "
        "    "
        "    .rendered_html h2,"
        "    .rendered_html h2:first-child {"
        "      font-size: 12pt;"
        "      margin: 6pt 0;"
        "    }"
        "    .rendered_html h3,"
        "    .rendered_html h3:first-child {"
        "      font-size: 10pt;"
        "      margin: 6pt 0;"
        "    }"
        " </style>"
    )


def apply_report_template():
    display(HTML(provide_container_style_html()))
    display(HTML(provide_text_cell_render_html()))
