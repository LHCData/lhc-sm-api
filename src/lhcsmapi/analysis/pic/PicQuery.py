from typing import List, Tuple
import warnings

import numpy as np
from pyspark.sql.session import SparkSession

from lhcsmapi.analysis.CircuitQuery import CircuitQuery, execution_count
from lhcsmapi.analysis.decorators import check_nan_timestamp
from lhcsmapi.api.query_builder import QueryBuilder


class PicQuery(CircuitQuery):
    """Base class for power interlock controller (PIC) query"""

    @execution_count()
    @check_nan_timestamp(return_type=np.nan)
    def find_timestamp_pic(
        self, timestamp_fgc: int, *, spark: SparkSession, duration: List[Tuple[int, str]] = [(1, "s"), (60, "s")]
    ) -> float:
        """Method searching PIC timestamps around an FGC PM event timestamp.
        If the input timestamp is NaN or the PIC timestamp was not found, then a NaN is returned.

        :param timestamp_fgc: FGC PM event timestamp (ns precision)
        :param spark: spark session variable needed to perform an NXCALS query
        :param duration: search duration before and after the FGC PM timestamp
        :return: float value of PIC timestamp in ns precision
        """
        st_abort_pic_df = (
            QueryBuilder()
            .with_nxcals(spark)
            .with_duration(t_start=timestamp_fgc, duration=duration)
            .with_circuit_type(self.circuit_type)
            .with_metadata(circuit_name=self.circuit_name, system="PIC", signal="ST_ABORT_PIC")
            .signal_query(verbose=self.verbose)
            .get_dataframes()
        )

        return st_abort_pic_df.index[0] if len(st_abort_pic_df) > 0 else np.nan


class PicRbQuery(PicQuery):
    """Class for power interlock controller (PIC) analysis"""

    @execution_count()
    @check_nan_timestamp(return_type=[np.nan, np.nan])
    def find_timestamp_pic(
        self, timestamp_fgc: int, *, spark: SparkSession, duration: List[Tuple[int, str]] = [(1, "s"), (60, "s")]
    ) -> List[int]:
        """Method finding PIC timestamps by querying NXCALS database around an FGC timestamp.
        If the input timestamp is NaN, then a NaN is returned.
        If the PIC timestamp was not found, then a list of NaN is returned.

        :param timestamp_fgc: FGC PM event timestamp (ns precision)
        :param spark: spark session variable needed to perform an NXCALS query
        :param duration: search duration before and after the FGC PM timestamp
        :return: list of float value of PIC timestamp in ns precision
        """
        st_abort_pic_dfs = (
            QueryBuilder()
            .with_nxcals(spark)
            .with_duration(t_start=int(timestamp_fgc), duration=duration)
            .with_circuit_type(self.circuit_type)
            .with_metadata(circuit_name=self.circuit_name, system="PIC", signal="ST_ABORT_PIC")
            .signal_query(verbose=self.verbose)
            .get_dataframes()
        )

        timestamp_pic = []
        for st_abort_pic_df in st_abort_pic_dfs:
            if not st_abort_pic_df.empty:
                timestamp_pic.append(st_abort_pic_df.index[0])
            else:
                warnings.warn("PIC signal %s is empty!" % st_abort_pic_df.columns[0])
                timestamp_pic.append(np.nan)
        return timestamp_pic
