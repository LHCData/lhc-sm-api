import warnings
from typing import List

import numpy as np

from lhcsmapi.Time import Time
from lhcsmapi.analysis.CircuitAnalysis import CircuitAnalysis


class PicAnalysis(CircuitAnalysis):
    """Base class for power interlock controller (PIC) analysis"""

    def analyze_pic(self, timestamp_pic: List[int], min_diff_in_ns: int = 1e6, max_diff_in_ns: int = 5e6) -> None:
        """Compares given PIC timestamps. If the difference is outside the acceptance range, a warning is displayed.

        :param timestamp_pic: list of PIC timestamps
        :param min_diff_in_ns: a min acceptable difference between the PIC timestamps in ns
        :param max_diff_in_ns: a mx acceptable difference between the PIC timestamps in ns
        :return: None
        """
        if any([np.isnan(timestamp_pic_el) for timestamp_pic_el in timestamp_pic]):
            warnings.warn("At least one timestamp is NaN, check of timestamp PIC difference is skipped.", stacklevel=2)
        else:
            dt_pic = abs(timestamp_pic[0] - timestamp_pic[1])
            max_diff_in_ms = int(max_diff_in_ns / 1e6)
            min_diff_in_ms = int(min_diff_in_ns / 1e6)
            if dt_pic >= max_diff_in_ns:
                warnings.warn(
                    f"PIC timestamps ({Time.to_string_short(timestamp_pic[0])}) and "
                    f"({Time.to_string_short(timestamp_pic[1])}) differ by more than "
                    f"{max_diff_in_ms} ms ({dt_pic} ns)"
                )
            elif dt_pic < min_diff_in_ns:
                warnings.warn(
                    f"PIC timestamps ({Time.to_string_short(timestamp_pic[0])}) and "
                    f"({Time.to_string_short(timestamp_pic[1])}) differ by less than "
                    f"{min_diff_in_ms} ms ({dt_pic} ns)"
                )
            else:
                print(
                    f"EVEN and ODD PIC timestamps ({Time.to_string_short(timestamp_pic[0])}) and "
                    f"({Time.to_string_short(timestamp_pic[1])}) are within {min_diff_in_ms}-{max_diff_in_ms}"
                    f" ms away."
                )
