import pandas as pd

from lhcsmapi.analysis.CircuitAnalysis import CircuitAnalysis
from lhcsmapi.analysis.pc.PcAnalysis import PcAnalysis
from lhcsmapi import reference


class R60_80_120ACircuitAnalysis(PcAnalysis, CircuitAnalysis):
    """Class for analysis of 60A, 80-120A circuits"""

    def create_mp3_results_table(self):
        """Method creating an MP3-compatible quench database row for a circuit

        :return: pd.DataFrame with the results of analysis calculation
        """
        columns_mp3 = reference.get_quench_database_columns(circuit_type="60_80_120A", table_type="MP3")
        mp3_table = pd.DataFrame(columns=columns_mp3, index=self.results_table.index)

        mp3_table[columns_mp3] = self.results_table[columns_mp3]

        return mp3_table
