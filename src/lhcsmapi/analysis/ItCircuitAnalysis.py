import warnings
from typing import Dict

import pandas as pd
import numpy as np

from lhcsmapi.Time import Time
from lhcsmapi.analysis.busbar.BusbarResistanceAnalysis import ItBusbarResistanceAnalysis
from lhcsmapi.analysis.dfb.DfbAnalysis import DfbAnalysis
from lhcsmapi.analysis.pc.PcAnalysis import PcAnalysis
from lhcsmapi.analysis.qds.QdsAnalysis import QdsAnalysis
from lhcsmapi.analysis.qh.QuenchHeaterVoltageAnalysis import QuenchHeaterVoltageAnalysis
from lhcsmapi.analysis.warnings import warning_on_one_line
from lhcsmapi.pyedsl.PlotBuilder import PlotBuilder, create_title
from lhcsmapi.analysis.CircuitAnalysis import CircuitAnalysis
from lhcsmapi.analysis.decorators import check_dataframe_empty
from lhcsmapi import reference

warnings.formatwarning = warning_on_one_line


class ItCircuitAnalysis(
    PcAnalysis, QdsAnalysis, QuenchHeaterVoltageAnalysis, ItBusbarResistanceAnalysis, DfbAnalysis, CircuitAnalysis
):
    """Class for analysis of IT (Inner Triplet) quadrupole circuits"""

    # Timestamp table
    def create_timestamp_table(
        self,
        timestamp_dct: Dict[str, int],
        circuit_name: str = "",
        is_qps_exp: bool = True,
        qps_fgc_max_delay: float = 0.04,
    ) -> None:
        """Method creating timestamp table as well as checking the timing between considered timestamps.

        :param timestamp_dct: a dictionary from timestamp name to its value (it is assumed that does not contain NaN)
        :param circuit_name: name of a circuit
        :param is_qps_exp: flag checking whether a QPS is expected or not
        :param qps_fgc_max_delay: maximum time delay between FGC and QPS timestamps in s
        """
        super().create_timestamp_table(timestamp_dct)

        if is_qps_exp and np.isnan(timestamp_dct.get("QDS_A", np.nan)) and np.isnan(timestamp_dct.get("QDS_B", np.nan)):
            warnings.warn(
                "Neither QDS board A nor B are present in the PM buffer. No QH fired, it is not an FPA!", stacklevel=2
            )
        elif np.isnan(timestamp_dct.get("QDS_A", np.nan)):
            warnings.warn("QDS Board A not present in PM buffer!", stacklevel=2)
        elif np.isnan(timestamp_dct.get("QDS_B", np.nan)):
            warnings.warn("QDS Board B not present in PM buffer!", stacklevel=2)
        else:
            if timestamp_dct["QDS_B"] - timestamp_dct["QDS_A"] != int(1e6):
                warnings.warn("QDS PM events (board A and B) are not synchronized to 1 ms!")

        if ("QDS_A" in timestamp_dct) and ("FGC_RQX" in timestamp_dct):
            if abs(timestamp_dct["QDS_A"] - timestamp_dct["FGC_RQX"]) > int(1e9 * qps_fgc_max_delay):
                warnings.warn(
                    "QDS_A and FGC PM events are not synchronized to +/- %d ms!" % int(1e3 * qps_fgc_max_delay)
                )
        else:
            warnings.warn(
                "Either QDS_A or FGC PM event is missing. " "Can not check synchronization of QDS and FGC timestamps!"
            )

    @staticmethod
    @check_dataframe_empty(mode="any", warning="At least one DataFrame is empty, plot of I_MEAS_Q1-Q3 skipped!")
    def plot_magnet_current(
        circuit_name: str,
        timestamp_fgc: int,
        i_meas_q1_df: pd.DataFrame,
        i_meas_q2_df: pd.DataFrame,
        i_meas_q3_df: pd.DataFrame,
    ) -> None:
        """Method plotting current through inner triplet quadrupole magnets Q1-Q3. The magnet current is calculated
        from the main power converter current.

        :param circuit_name: name of the analyzed circuit (for the title)
        :param timestamp_fgc: timestamp of the QDS PM event (for the title)
        :param i_meas_q1_df: current of the first magnet calculated with respective I_MEAS signals
        :param i_meas_q2_df: current of the second magnet calculated with respective I_MEAS signals
        :param i_meas_q3_df: current of the third magnet calculated with respective I_MEAS signals
        """
        title = create_title(circuit_name, timestamp_fgc, ["I_MEAS_Q1", "I_MEAS_Q2", "I_MEAS_Q3"])
        PlotBuilder().with_signal(
            [i_meas_q1_df, i_meas_q2_df, i_meas_q3_df], title=title, figsize=(13, 6.5), grid=True
        ).with_ylabel(ylabel="I, [A]").with_xlim((-1, 2)).plot()

    @staticmethod
    @check_dataframe_empty(mode="any", warning="At least one DataFrame is empty, plot of U_RES_Q1-Q3 skipped!")
    def plot_u_res_q1_q2_q3(
        circuit_name: str,
        timestamp_qds: int,
        u_res_q1_df: pd.DataFrame,
        u_res_q2_df: pd.DataFrame,
        u_res_q3_df: pd.DataFrame,
    ) -> None:
        """Method plotting U_RES voltage from QPS for Q1, Q2, Q3. A yellow-green band for the detection threshold is
        displayed.

        :param circuit_name: name of the analyzed circuit (for the title)
        :param timestamp_qds: timestamp of the QDS PM event (for the title)
        :param u_res_q1_df: U_RES voltage from QPS for Q1 magnet
        :param u_res_q2_df: U_RES voltage from QPS for Q2 magnet
        :param u_res_q3_df: U_RES voltage from QPS for Q3 magnet
        """
        title = create_title(circuit_name, timestamp_qds, ["U_RES_Q1", "U_RES_Q2", "U_RES_Q3"])
        PlotBuilder().with_signal(
            [u_res_q1_df, u_res_q2_df, u_res_q3_df], title=title, figsize=(15, 7), grid=True
        ).with_ylabel(ylabel="U, [V]").with_xlim((-0.1, 0.2)).with_axhspan(
            ymin=-0.1, ymax=+0.1, facecolor="xkcd:yellowgreen"
        ).plot()

    def create_mp3_results_table(self):
        """Method creating an MP3-compatible quench database row for an IT circuit

        :return: pd.DataFrame with the results of analysis calculation
        """
        columns_mp3 = reference.get_quench_database_columns(circuit_type="IT", table_type="MP3")
        mp3_table = pd.DataFrame(columns=columns_mp3, index=self.results_table.index)

        mp3_table[columns_mp3] = self.results_table[columns_mp3]

        # Fill non-matching column names
        mp3_table["Timestamp_PIC"] = self.results_table.apply(
            lambda col: Time.to_string_short(col["timestamp_pic"]), axis=1
        )
        mp3_table["Delta_t(FGC_RQX-PIC)"] = self.results_table.apply(
            lambda col: 1e-6 * (col["timestamp_fgc_rqx"] - col["timestamp_pic"]), axis=1
        )
        mp3_table["Delta_t(QPS-PIC)"] = self.results_table.apply(
            lambda col: 1e-6 * (col["timestamp_qds_a"] - col["timestamp_pic"]), axis=1
        )

        if isinstance(self.results_table.loc[0, "Quench origin"], str):
            mp3_table["dU_QPS/dt"] = self.results_table["dU_RES_dt_%s" % self.results_table.loc[0, "Quench origin"]]

        return mp3_table
