import inspect
import warnings
from functools import wraps
from typing import List, Union, Callable, Optional

import numpy as np
import pandas as pd
from lhcsmapi.analysis.warnings import warning_on_one_line

warnings.formatwarning = warning_on_one_line


def check_dataframe_empty(
    mode="all", arg_names=None, warning="All provided DataFrames are missing, analysis skipped.", default_return=None
):
    def inner_function(func):
        args_indices = _get_indices_for_arg_names(func, arg_names)

        @wraps(func)
        def wrapper(*args, **kw):
            args_to_check = [args[i] for i in args_indices if i < len(args)]

            check = {"all": _are_all_dataframes_empty, "any": _is_any_dataframe_empty}

            def raise_not_supported(_):
                raise ValueError("Mode takes only values in ['all', 'any'], got {}".format(mode))

            if check.get(mode, raise_not_supported)(args_to_check):
                warnings.warn(warning)
                return default_return
            return func(*args, **kw)

        return wrapper

    return inner_function


def _flatten_nested_lists(nested_lists):
    result = []
    for el in nested_lists:
        if isinstance(el, (list, tuple)):
            result.extend(_flatten_nested_lists(el))
        else:
            result.append(el)
    return result


def _are_all_dataframes_empty(arguments: List) -> bool:
    args_scalars = [arg.empty for arg in arguments if isinstance(arg, pd.DataFrame)]
    cond_scalar = all(args_scalars) if args_scalars else False
    args_list = _flatten_nested_lists([arg for arg in arguments if isinstance(arg, list)])
    cond_vector = all([arg.empty for arg in args_list]) if args_list else False
    return cond_scalar or cond_vector


def _is_any_dataframe_empty(arguments: List) -> bool:
    cond_scalar = any([arg.empty for arg in arguments if isinstance(arg, pd.DataFrame)]) if arguments else False
    args_list = _flatten_nested_lists([arg for arg in arguments if isinstance(arg, list)])
    cond_vector = any([arg.empty for arg in args_list if isinstance(arg, pd.DataFrame)])
    return cond_scalar or cond_vector


def check_nan_timestamp(
    return_type=pd.DataFrame, warning="Input timestamp is NaN, query skipped returning default return_type."
):
    def inner_function(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            cond_arg = any([np.isnan(arg) for arg in args if isinstance(arg, float)])
            cond_kwarg = any([np.isnan(kwarg) for kwarg in kwargs.values() if isinstance(kwarg, float)])

            if cond_arg or cond_kwarg:
                warnings.warn("In function %s: %s" % (str(func).split(" ")[1], warning))
                return return_type
            return func(*args, **kwargs)

        return wrapper

    return inner_function


def check_nan_timestamp_signals(
    return_type=pd.DataFrame,
    signal_name="signal_names",
    warning="Input timestamp is NaN, query skipped returning default return_type.",
):
    def inner_function(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            cond_arg = any([np.isnan(arg) for arg in args if isinstance(arg, float)])
            cond_kwarg = any([np.isnan(kwarg) for kwarg in kwargs.values() if isinstance(kwarg, float)])

            if cond_arg or cond_kwarg:
                warnings.warn("In function %s: %s" % (str(func).split(" ")[1], warning))
                signals_count = 1 if isinstance(kwargs[signal_name], str) else len(kwargs[signal_name])
                return return_type if signals_count == 1 else [return_type] * signals_count

            return func(*args, **kwargs)

        return wrapper

    return inner_function


def _get_indices_for_arg_names(function, arg_names) -> Union[range, List[int]]:
    sig = inspect.signature(function)
    if arg_names is None:
        return range(0, len(sig.parameters))
    return [list(sig.parameters.keys()).index(arg_name) for arg_name in arg_names]


def check_arguments_not_none():
    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            none_args = [i for i, arg in enumerate(args) if arg is None]
            if none_args:
                arg_names = ", ".join(func.__code__.co_varnames[i] for i in none_args)
                raise ValueError(f"The following positional arguments cannot be None: {arg_names}")

            none_kwargs = [key for key, val in kwargs.items() if val is None]
            if none_kwargs:
                raise ValueError(f"The following keyword arguments cannot be None: {', '.join(none_kwargs)}")

            return func(*args, **kwargs)

        return wrapper

    return decorator
