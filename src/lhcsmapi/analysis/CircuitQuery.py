from functools import wraps
from typing import List, Union

from IPython.display import display, HTML

import pandas as pd
import numpy as np
import warnings

from lhcsmapi.analysis.decorators import check_nan_timestamp_signals
from lhcsmapi.api.query_builder import QueryBuilder
from lhcsmapi.Time import Time


def execution_count():
    """Decorator counting the number of query function executions and displaying progress in a notebook.

    :return: inner function performing calculation of query methods execution
    """

    def inner_function(func):
        @wraps(func)
        def wrapper(*args, **kw):
            query = list(filter(lambda arg: isinstance(arg, CircuitQuery), args))[0]
            if query.max_executions is not None:
                query.execution_count += 1

                def cstr(s, color="black"):
                    return "<text style=color:{}>{}</text>".format(color, s)

                output_text = "Executing %s query function %s: %d/%d." % (
                    query.circuit_name,
                    func.__name__,
                    query.execution_count,
                    query.max_executions,
                )
                display(HTML(cstr(output_text, "blue")))
            return func(*args, **kw)

        return wrapper

    return inner_function


def get_quench_current(i_meas_df: pd.DataFrame, index: float) -> int:
    """Method returning a quench current for a given index (moment in time)

    :param i_meas_df: main power converter current
    :param index: index for which quench current is returned
    :return: an integer value of a quench current
    """
    value_at_index = i_meas_df.reindex([index], method="nearest", tolerance=0.1).values[0]
    if np.isnan(value_at_index):
        value_at_index = i_meas_df.values[-1]
        warnings.warn(
            "Time of quench {} s is outside of PM FGC I_MEAS signal. Taken last value of current. "
            "For a more accurate value of quench current, please query NXCALS.".format(index)
        )
    return int(value_at_index)


def min_nan(timestamp_a, timestamp_b):
    import numpy as np

    if np.isnan(timestamp_a):
        return timestamp_b
    elif np.isnan(timestamp_b):
        return timestamp_a
    else:
        return min(timestamp_a, timestamp_b)


class CircuitQuery(object):
    """Class containing methods for query of events and signals. Each query is implemented with pyeDSL."""

    def __init__(self, circuit_type, circuit_name, max_executions=None, verbose=True):
        self.circuit_type = circuit_type
        self.circuit_name = circuit_name
        self.verbose = verbose
        self.max_executions = max_executions
        self.execution_count = 0

    # General purpose
    @execution_count()
    @check_nan_timestamp_signals(return_type=pd.DataFrame())
    def query_signal_nxcals(
        self, t_start, t_end=None, duration=None, t0=0, *, system: str, signal_names: Union[str, List[str]], spark
    ) -> Union[List[pd.DataFrame], pd.DataFrame]:
        """Method querying a signal from NXCALS with a given t_start, t_end,duration as well as system and signal names.
        Either t_end or duration has to be present. Signal is synchronized to synchronization time `t0`.

        :param t_start: start time of a query
        :param t_end: end time of a query (optional)
        :param duration: duration w.r.t. t_start (optional)
        :param t0: synchronization time for a signal
        :param system: name of a system to query
        :param signal_names: either a signal name or a list of signal names to query
        :param spark: spark connector
        :return: list of signals queried from NXCALS
        """
        if duration is not None:
            t_start, t_end = Time.get_query_period_in_unix_time(t_start, t_end, duration, "")
        return (
            QueryBuilder()
            .with_nxcals(spark)
            .with_duration(t_start=t_start, t_end=t_end)
            .with_circuit_type(self.circuit_type)
            .with_metadata(circuit_name=self.circuit_name, system=system, signal=signal_names)
            .signal_query()
            .synchronize_time(t0)
            .convert_index_to_sec()
            .get_dataframes()
        )

    @execution_count()
    @check_nan_timestamp_signals(return_type=pd.DataFrame())
    def query_raw_signal_nxcals(
        self, t_start, t_end=None, duration=None, *, system: str, signal_names: Union[str, List[str]], spark
    ) -> Union[List[pd.DataFrame], pd.DataFrame]:
        """Method querying a signal from NXCALS with a given t_start, t_end,duration as well as system and signal names.
        Either t_end or duration has to be present. Signal is synchronized to synchronization time `t0`.

        :param t_start: start time of a query
        :param t_end: end time of a query (optional)
        :param duration: duration w.r.t. t_start (optional)
        :param system: name of a system to query
        :param signal_names: either a signal name or a list of signal names to query
        :param spark: spark connector
        :return: list of signals queried from NXCALS
        """
        t_start, t_end = Time.get_query_period_in_unix_time(t_start, t_end, duration, "")
        return (
            QueryBuilder()
            .with_nxcals(spark)
            .with_duration(t_start=t_start, t_end=t_end)
            .with_circuit_type(self.circuit_type)
            .with_metadata(circuit_name=self.circuit_name, system=system, signal=signal_names)
            .signal_query()
            .get_dataframes()
        )
