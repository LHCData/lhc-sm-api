from importlib.metadata import version

import pandas as pd

from lhcsmapi.Time import Time
from lhcsmapi.analysis.CircuitQuery import CircuitQuery
from lhcsmapi.analysis.busbar.BusbarResistanceQuery import BusbarResistanceQuery
from lhcsmapi.analysis.dfb.DfbQuery import DfbQuery
from lhcsmapi.analysis.pc.PcQuery import PcQuery
from lhcsmapi.analysis.pic.PicQuery import PicQuery
from lhcsmapi.analysis.qds.QdsQuery import QdsIpdQuery
from lhcsmapi.analysis.qh.QuenchHeaterQuery import QuenchHeaterQuery
from lhcsmapi.metadata import signal_metadata
from lhcsmapi import reference


class IpdCircuitQuery(PicQuery, PcQuery, QdsIpdQuery, QuenchHeaterQuery, BusbarResistanceQuery, DfbQuery, CircuitQuery):
    """Class extending the CircuitQuery class with methods for creation of templates of analysis tables in IPD circuits."""

    def __init__(self, circuit_type, circuit_name, max_executions=None, verbose=True):
        super().__init__(circuit_type, circuit_name, max_executions, verbose)

        self.circuit_type = signal_metadata.get_circuit_type_for_circuit_name(circuit_name)

    def create_report_analysis_template(self, timestamp_fgc: int, author: str = "") -> pd.DataFrame:
        """Method creating a report analysis template for IPD circuits

        :param timestamp_fgc: timestamp of an FGC event
        :param author: NICE account name of the analysis author
        :return: pre-filled pd.DataFrame with a template to be completed further with data from an analysis of
            a powering event in IPD circuits
        """
        columns = reference.get_quench_database_columns(circuit_type="IPD", table_type="MP3")

        results_table = pd.DataFrame(columns=columns, index=[0])
        results_table["Circuit Name"] = self.circuit_name
        results_table["Circuit Family"] = self.circuit_name.split(".")[0]
        results_table["Period"] = reference.get_mp3_period(timestamp_fgc)
        results_table["Date (FGC)"] = Time.to_string_short(timestamp_fgc).split(" ")[0]
        results_table["Time (FGC)"] = Time.to_string_short(timestamp_fgc).split(" ")[1]
        results_table["Analysis performed by"] = author
        results_table["lhcsmapi version"] = version("lhcsmapi")

        return results_table
