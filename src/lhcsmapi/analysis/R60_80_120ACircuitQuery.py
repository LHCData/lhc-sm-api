from importlib.metadata import version

import pandas as pd

import lhcsmapi
from lhcsmapi.Time import Time
from lhcsmapi.analysis.CircuitQuery import CircuitQuery
from lhcsmapi.analysis.pc.PcQuery import PcQuery
from lhcsmapi.analysis.pic.PicQuery import PicQuery
from lhcsmapi import reference


class R60_80_120ACircuitQuery(PcQuery, PicQuery, CircuitQuery):
    """Class extending the CircuitQuery class with methods for creation of templates of analysis tables in
    60A, 80-120A circuits.

    """

    def __init__(self, circuit_type, circuit_name, max_executions=None, verbose=True):
        super().__init__(circuit_type, circuit_name, max_executions, verbose)

    def create_report_analysis_template(self, timestamp_fgc: int, author: str = "") -> pd.DataFrame:
        """Method creating a report analysis template for IPD circuits

        :param timestamp_fgc: timestamp of an FGC event
        :param author: NICE account name of the analysis author
        :return: pre-filled pd.DataFrame with a template to be completed further with data from an analysis of
            a powering event in IPD circuits
        """
        columns = reference.get_quench_database_columns(circuit_type="60_80_120A", table_type="MP3")

        results_table = pd.DataFrame(columns=columns, index=[0])
        results_table["Circuit Name"] = self.circuit_name

        circuit_families = [
            "RCBCH",
            "RCBCV",
            "RCOSX3",
            "RCOX3",
            "RCSSX3",
            "RCSX3",
            "RCTX3",
            "RCBYH",
            "RCBYV",
            "RCBH",
            "RCBV",
        ]
        for circuit_family in circuit_families:
            if circuit_family in self.circuit_name:
                results_table["Circuit Family"] = circuit_family

        results_table["Period"] = reference.get_mp3_period(timestamp_fgc)
        results_table["Date (FGC)"] = Time.to_string_short(timestamp_fgc).split(" ")[0]
        results_table["Time (FGC)"] = Time.to_string_short(timestamp_fgc).split(" ")[1]
        results_table["Analysis performed by"] = author
        results_table["lhcsmapi version"] = version("lhcsmapi")

        return results_table
