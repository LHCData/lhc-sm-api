from importlib.metadata import version

import pandas as pd

from lhcsmapi import reference
from lhcsmapi.Time import Time
from lhcsmapi.analysis.CircuitQuery import CircuitQuery, get_quench_current, execution_count
from lhcsmapi.analysis.busbar.BusbarResistanceQuery import BusbarResistanceQuery
from lhcsmapi.analysis.dfb.DfbQuery import DfbQuery
from lhcsmapi.analysis.diode.DiodeLeadResistanceQuery import DiodeLeadResistanceQuery
from lhcsmapi.analysis.ee.EeQuery import Ee13kAQuery
from lhcsmapi.analysis.nqps.NqpsQuery import NqpsRbQuery
from lhcsmapi.analysis.pc.PcQuery import Pc13kAQuery
from lhcsmapi.analysis.pic.PicQuery import PicRbQuery
from lhcsmapi.analysis.qds.QdsQuery import QdsRbQuery
from lhcsmapi.analysis.qh.QuenchHeaterQuery import QuenchHeaterQuery
from lhcsmapi.metadata.MappingMetadata import MappingMetadata


class RbCircuitQuery(
    PicRbQuery,
    Pc13kAQuery,
    Ee13kAQuery,
    NqpsRbQuery,
    QuenchHeaterQuery,
    BusbarResistanceQuery,
    DiodeLeadResistanceQuery,
    QdsRbQuery,
    DfbQuery,
    CircuitQuery,
):
    """Class extending the CircuitQuery class with methods for creation of templates of analysis tables in RB circuits."""

    @execution_count()
    def create_report_analysis_template(
        self,
        source_timestamp_qds_df: pd.DataFrame,
        source_timestamp_nqps_df: pd.DataFrame,
        timestamp_pic: int,
        timestamp_fgc: int,
        i_meas_df: pd.DataFrame,
        author: str,
    ) -> pd.DataFrame:
        """Method creating a report analysis template for main dipole circuits

        :param source_timestamp_qds_df: table with QDS PM source and timestamp
        :param source_timestamp_nqps_df: table with QDS PM source and timestamp
        :param timestamp_pic: unix timestamp of the first PIC event in RQ circuit in nanoseconds
        :param timestamp_fgc: unix timestamp of an FGC event in RB circuit in nanoseconds
        :param i_meas_df: measured power converter current in RB circuit
        :param author: NICE account name of the analysis author
        :return: pre-filled pd.DataFrame with a template to be filled further with data from analysis
            of a powering event in main dipole (RB) circuits
        """

        columns = reference.get_quench_database_columns(circuit_type="RB", table_type="MP3")

        results_table = pd.DataFrame(columns=columns)

        if not source_timestamp_qds_df.empty:
            results_table["Position"] = source_timestamp_qds_df["source"]
            results_table["nQPS crate name"] = results_table["Position"].apply(
                lambda col: MappingMetadata.get_crate_name_from_magnet_name(self.circuit_type, col)
            )
            results_table["Delta_t(iQPS-PIC)"] = 1e-6 * (source_timestamp_qds_df["timestamp"] - timestamp_pic)
            results_table["I_Q_M"] = results_table["Delta_t(iQPS-PIC)"].apply(
                lambda col: get_quench_current(i_meas_df, 1e-3 * col)
            )

            if not source_timestamp_nqps_df.empty:
                results_table["timestamp_iqps"] = (
                    source_timestamp_qds_df["timestamp"] if "timestamp" in source_timestamp_qds_df else None
                )
                source_timestamp_nqps_renamed_df = source_timestamp_nqps_df.rename(
                    columns={"source": "nQPS crate name", "timestamp": "timestamp_nqps"}
                )
                results_table = (
                    results_table.merge(
                        source_timestamp_nqps_renamed_df[["timestamp_nqps", "nQPS crate name"]],
                        on="nQPS crate name",
                        how="left",
                    )
                    .sort_values(by="timestamp_iqps")
                    .reset_index(drop=True)
                )
                results_table["Delta_t(nQPS-PIC)"] = 1e-6 * (results_table["timestamp_nqps"] - timestamp_pic)
        else:
            # generate single-row table if there is no quench
            results_table.loc[0] = pd.Series(dtype="object")

        results_table["Circuit Name"] = self.circuit_name
        results_table["Circuit Family"] = self.circuit_type
        results_table["Period"] = reference.get_mp3_period(timestamp_fgc)
        results_table["Date (FGC)"] = Time.to_string_short(timestamp_fgc).split(" ")[0]
        results_table["Time (FGC)"] = Time.to_string_short(timestamp_fgc).split(" ")[1]
        results_table["Analysis performed by"] = author
        results_table["lhcsmapi version"] = version("lhcsmapi")

        return results_table
