import pandas as pd

from lhcsmapi.Time import Time
from lhcsmapi.analysis.CircuitAnalysis import CircuitAnalysis
from lhcsmapi.analysis.busbar.BusbarResistanceAnalysis import MultipleBusbarResistanceAnalysis
from lhcsmapi.analysis.dfb.DfbAnalysis import DfbAnalysis
from lhcsmapi.analysis.diode.DiodeLeadResistanceAnalysis import DiodeLeadResistanceAnalysis
from lhcsmapi.analysis.ee.Ee13kAAnalysis import Ee13kAAnalysis
from lhcsmapi.analysis.nqps.NqpsAnalysis import NqpsAnalysis
from lhcsmapi.analysis.pc.PcAnalysis import Pc13kAAnalysis
from lhcsmapi.analysis.pic.PicAnalysis import PicAnalysis
from lhcsmapi.analysis.qds.QdsAnalysis import QdsRbAnalysis
from lhcsmapi.analysis.qh.QuenchHeaterVoltageCurrentAnalysis import QuenchHeaterVoltageCurrentAnalysis
from lhcsmapi.analysis.vf.VoltageFeelersAnalysis import VoltageFeelersAnalysis
from lhcsmapi import reference


class RbCircuitAnalysis(
    PicAnalysis,
    Pc13kAAnalysis,
    Ee13kAAnalysis,
    QuenchHeaterVoltageCurrentAnalysis,
    QdsRbAnalysis,
    NqpsAnalysis,
    DiodeLeadResistanceAnalysis,
    MultipleBusbarResistanceAnalysis,
    DfbAnalysis,
    VoltageFeelersAnalysis,
    CircuitAnalysis,
):
    """Class for analysis of main dipole circuits"""

    def create_mp3_results_table(self, timestamp_pic: int, timestamp_fgc: int) -> pd.DataFrame:
        """Method creating an MP3-compatible quench database row for the main quadrupole circuit

        :param timestamp_pic: PIC NXCALS timestamp
        :param timestamp_fgc: FGC PM timestamp
        :return: pd.DataFrame with the results of analysis calculation
        """
        columns_mp3 = reference.get_quench_database_columns(circuit_type="RB", table_type="MP3")
        mp3_table = pd.DataFrame(columns=columns_mp3, index=self.results_table.index)
        mp3_table[columns_mp3] = self.results_table[columns_mp3]

        mp3_table["Timestamp_PIC"] = Time.to_string_short(timestamp_pic)
        mp3_table["Delta_t(FGC-PIC)"] = 1e-6 * (timestamp_fgc - timestamp_pic)
        mp3_table["Delta_t(EE_odd-PIC)"] = self.results_table.apply(
            lambda col: 1e-6 * (col["timestamp_ee_odd"] - timestamp_pic), axis=1
        )
        mp3_table["Delta_t(EE_even-PIC)"] = self.results_table.apply(
            lambda col: 1e-6 * (col["timestamp_ee_even"] - timestamp_pic), axis=1
        )

        return mp3_table
