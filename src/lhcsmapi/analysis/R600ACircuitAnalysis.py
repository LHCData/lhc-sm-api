import warnings
from typing import Dict

import numpy as np
import pandas as pd

from lhcsmapi.Time import Time
from lhcsmapi.analysis.CircuitAnalysis import CircuitAnalysis
from lhcsmapi.analysis.decorators import check_dataframe_empty
from lhcsmapi.analysis.dfb.DfbAnalysis import DfbAnalysis
from lhcsmapi.analysis.pc.PcAnalysis import PcAnalysis
from lhcsmapi.analysis.qds.QdsAnalysis import QdsAnalysis
from lhcsmapi.analysis.warnings import warning_on_one_line
from lhcsmapi.metadata import signal_metadata
from lhcsmapi.pyedsl.PlotBuilder import PlotBuilder, create_title
from lhcsmapi import reference
import lhcsmapi.signal_analysis.features as signal_analysis
import lhcsmapi.analysis.features_helper as utility_features

warnings.formatwarning = warning_on_one_line


def timestamp_check(
    timestamp_dct: Dict[str, int], qds_a_name: str, qds_b_name: str, fgc_ee_name: str, qps_fgc_max_delay: float = 0.04
) -> None:
    """Function checks timestamps of QPS, FGC, and EE for 600A circuits:
    For all circuit families: with/without EE, RCBXH/V, RCD/O the
    - for RCBXH/V and RCD/O circuits a check between QPS A and B boards and QPS board A and FGC is performed
      - the difference between timestamps of QDS board A and B is at maximum equal to 1 ms
      - the difference between timestamps of QDS board A and FGC is at maximum equal to 40 ms

    - in addition for circuits with EE a check between QDS board A and EE timestamps is performed
      - the difference between timestamps of QDS board A and EE is at maximum equal to 40 ms

    :param timestamp_dct: a dictionary from timestamp name to its value (it is assumed that does not contain NaN)
    :param qds_a_name: name of QDS board A timestamp
    :param qds_b_name: name of QDS board B timestamp
    :param fgc_ee_name: name of either FGC or EE timestamp
    :param qps_fgc_max_delay: maximum time delay between FGC and QPS timestamps in ns
    """
    if (qds_a_name not in timestamp_dct) or np.isnan(timestamp_dct[qds_a_name]):
        warnings.warn("QDS Board A not present in PM buffer.", stacklevel=2)
    elif (qds_b_name not in timestamp_dct) or np.isnan(timestamp_dct[qds_b_name]):
        warnings.warn("QDS Board B not present in PM buffer!", stacklevel=2)
    else:
        if timestamp_dct[qds_b_name] - timestamp_dct[qds_a_name] != int(1e6):
            warnings.warn("QDS PM events (board A and B) are not synchronized to 1 ms!")

    fgc_or_ee = "FGC" if "FGC" in fgc_ee_name else "EE"
    if (qds_a_name in timestamp_dct) and (fgc_ee_name in timestamp_dct):
        if abs(timestamp_dct[qds_a_name] - timestamp_dct[fgc_ee_name]) > int(1e9 * qps_fgc_max_delay):
            warnings.warn(
                "QDS_A and %s PM events are not synchronized to +/- %d ms!" % (fgc_or_ee, int(1e3 * qps_fgc_max_delay)),
                stacklevel=2,
            )
    else:
        warnings.warn(
            "Either QDS_A or %s PM event is missing. "
            "Can not check synchronization of QDS and FGC timestamps!" % fgc_or_ee,
            stacklevel=2,
        )


class R600ACircuitAnalysis(PcAnalysis, QdsAnalysis, DfbAnalysis, CircuitAnalysis):
    """Class for analysis of 600A circuits supporting three distinct cases:
    - circuits with/without EE
    - RCBXH/V circuits
    - RCD/O circuits
    """

    # Timestamp table
    def create_timestamp_table(self, timestamp_dct: Dict[str, int], circuit_name: str = "") -> None:
        """Method creating timestamp table as well as checking the timing between considered timestamps.
        Depending on the type of 600A circuit, an appropriate check is performed.

        :param timestamp_dct: a dictionary from timestamp name to its value (it is assumed that does not contain NaN)
        :param circuit_name: name of a circuit
        """
        super().create_timestamp_table(timestamp_dct)
        if "RCD" in circuit_name:
            timestamp_check(timestamp_dct, qds_a_name="QDS_A_RCD", qds_b_name="QDS_B_RCD", fgc_ee_name="FGC_RCD")
            timestamp_check(timestamp_dct, qds_a_name="QDS_A_RCO", qds_b_name="QDS_B_RCO", fgc_ee_name="FGC_RCO")
        elif "RCBX" in circuit_name:
            timestamp_check(timestamp_dct, qds_a_name="QDS_A_RCBXH", qds_b_name="QDS_B_RCBXH", fgc_ee_name="FGC_RCBXH")
            timestamp_check(timestamp_dct, qds_a_name="QDS_A_RCBXV", qds_b_name="QDS_B_RCBXV", fgc_ee_name="FGC_RCBXV")
        else:
            timestamp_check(timestamp_dct, qds_a_name="QDS_A", qds_b_name="QDS_B", fgc_ee_name="FGC")
            timestamp = list(timestamp_dct.values())[0] if timestamp_dct else None
            if "EE" in signal_metadata.get_system_types_per_circuit_name(self.circuit_type, circuit_name, timestamp):
                timestamp_check(timestamp_dct, qds_a_name="QDS_A", qds_b_name="QDS_B", fgc_ee_name="EE")

    # PC
    @staticmethod
    @check_dataframe_empty(mode="any", warning="I_MEAS, I_REF are empty, plot of coupled circuit current skipped!")
    def plot_coupled_circuit_current(
        i_meas_first_df, i_ref_first_df, i_meas_second_df, i_ref_second_df, timestamp_first, timestamp_second, suffixes
    ):
        def rename_column(df, suffix):
            return df.rename(columns={df.columns[0]: df.columns[0] + "_" + suffix})

        i_dfs = [
            rename_column(i_meas_first_df, suffixes[0]),
            rename_column(i_ref_first_df, suffixes[0]),
            rename_column(i_meas_second_df, suffixes[1]),
            rename_column(i_ref_second_df, suffixes[1]),
        ]

        title = "%s - %s; %s - %s" % (
            Time.to_string_short(timestamp_first),
            suffixes[0],
            Time.to_string_short(timestamp_second),
            suffixes[1],
        )

        PlotBuilder().with_signal(
            i_dfs, title=title, grid=True, style=["-", "--", "-", "--"], color=["C0", "C1", "C2", "C3"]
        ).with_ylabel(ylabel="I, [A]").plot()

    def compute_and_print_current_angle_for_combined_powering(self):
        """
        Method calculating and printing out the current angle in combined powering of coupled circuits.
        It is based on already calculated columns in results table. The value of these columns can be updated manually
        by the experts.
        """
        I_Q_H = self.results_table.loc[0, "I_Q_H"]
        if np.isnan(I_Q_H):
            warnings.warn("Horizontal circuit not powered, analysis skipped")
            return

        I_Q_V = self.results_table.loc[0, "I_Q_V"]
        if np.isnan(I_Q_V):
            warnings.warn("Vertical circuit not powered, analysis skipped")
            return

        vector = I_Q_V + I_Q_H * 1j
        angle = np.round(np.mod(np.angle(vector, True), 360))

        self.results_table["phase"] = angle
        print(f"The phase of currents in the combined powering is {angle} deg.")

    # EE
    @check_dataframe_empty(mode="any", warning="I_MEAS, U_DUMP_RES signals are empty, analysis skipped!")
    def analyze_u_dump_res_ee(
        self,
        circuit_name: str,
        timestamp_fgc: int,
        i_meas_df: pd.DataFrame,
        u_dump_res_df: pd.DataFrame,
        col_name: str = "U_EE_max",
    ) -> None:
        """Method plotting and calculating the maximum energy extraction voltage.
        If any input signal is missing, then the analysis is skipped.
        The calculated maximum value is set in the results table.

        :param circuit_name: name of the analyzed circuit (for the title)
        :param timestamp_fgc: timestamp of the FGC PM event (for the title)
        :param i_meas_df: measured power converter current
        :param u_dump_res_df: energy extraction voltage
        :param col_name: name of column in the output Excel file
        """
        # Analyze

        u_dump_res_max = signal_analysis.calculate_features(u_dump_res_df, utility_features.max_abs)

        print("The maximum EE voltage is %s V." % round(u_dump_res_max, 3))

        # Store analysis result
        if self.results_table is not None:
            self.results_table[col_name] = round(u_dump_res_max, 3)

        # Plot
        title = create_title(circuit_name, timestamp_fgc, ["I_MEAS", "U_DUMP_RES"])
        PlotBuilder().with_signal(i_meas_df, title=title, grid=True).with_ylabel(ylabel="I_MEAS, [A]").with_xlim(
            [-1, 2]
        ).with_signal(u_dump_res_df).with_ylabel(ylabel="U_DUMP_RES, [V]").plot()

    # QPS
    @check_dataframe_empty(mode="all", warning="U_RES, U_DIFF, I_DCCT, I_DIDT signals are empty, analysis skipped!")
    def plot_qds(
        self,
        circuit_name: str,
        timestamp_qps: int,
        i_dcct_df: pd.DataFrame,
        i_didt_df: pd.DataFrame,
        u_diff_df: pd.DataFrame,
        u_res_df: pd.DataFrame,
    ) -> None:
        """Method plotting the QDS signals.
        If any input signal is missing, then the analysis is skipped.

        :param circuit_name: name of the analyzed circuit (for the title)
        :param timestamp_sync: timestamp of the FGC PM event (for the title)
        :param i_dcct_df: current of the DCCT sensor
        :param i_didt_df: signal of the current derivative sensor
        :param u_diff_df: differential voltage
        :param u_res_df: resistive voltage
        """
        xlim = [-0.1, 0.1]
        i_dcct_short_df = i_dcct_df[(i_dcct_df.index >= xlim[0]) & (i_dcct_df.index <= xlim[1])]
        i_didt_short_df = i_didt_df[(i_didt_df.index >= xlim[0]) & (i_didt_df.index <= xlim[1])]
        u_diff_short_df = u_diff_df[(u_diff_df.index >= xlim[0]) & (u_diff_df.index <= xlim[1])]

        title = create_title(circuit_name, timestamp_qps, ["U_RES", "U_DIFF"])

        PlotBuilder().with_signal(u_res_df, title=title, figsize=(13, 6.5), color=["C0"], grid=True).with_ylabel(
            ylabel="U_RES, [V]"
        ).with_ylim([-0.25, 0.25]).with_axhspan(ymin=-0.07, ymax=+0.07, facecolor="xkcd:yellowgreen").with_legend(
            loc="upper left"
        ).with_signal(
            u_diff_short_df, color=["C1"]
        ).with_ylabel(
            ylabel="U_DIFF, [V]"
        ).with_xlim(
            [-0.1, 0.1]
        ).with_ylim(
            [
                min(u_diff_short_df.values) * (0.9 if min(u_diff_short_df.values) > 0 else 1.1),
                max(u_diff_short_df.values) * (1.1 if min(u_diff_short_df.values) > 0 else 0.9),
            ]
        ).with_legend(
            loc="lower left"
        ).plot()

        title = create_title(circuit_name, timestamp_qps, ["I_DCCT", "I_DIDT"])

        PlotBuilder().with_signal(i_dcct_short_df, title=title, figsize=(13, 6.5), color=["C0"], grid=True).with_ylabel(
            ylabel="I_DCCT, [A]"
        ).with_ylim(
            [
                min(i_dcct_short_df.values) * (0.9 if min(i_dcct_short_df.values) > 0 else 1.1),
                max(i_dcct_short_df.values) * (1.1 if min(i_dcct_short_df.values) > 0 else 0.9),
            ]
        ).with_legend(
            loc="upper right"
        ).with_signal(
            i_didt_short_df, color=["C1"]
        ).with_ylabel(
            ylabel="I_DIDT, [A/s]"
        ).with_ylim(
            [
                min(i_didt_short_df.values) * (0.9 if min(i_didt_short_df.values) > 0 else 1.1),
                max(i_didt_short_df.values) * (1.1 if min(i_didt_short_df.values) > 0 else 0.9),
            ]
        ).with_legend(
            loc="lower right"
        ).plot()

    @staticmethod
    @check_dataframe_empty(mode="all", warning="U_RES signal is empty, analysis skipped!")
    def plot_u_res_slope(circuit_name, timestamp_qps, u_res_df, u_res_slope_df):
        # Skip first ten points to avoid initial noise
        u_res_slice_df = u_res_df[u_res_df.index > u_res_df.index[10]]

        # Plot
        title = create_title(circuit_name, timestamp_qps, "U_RES")

        PlotBuilder().with_signal(
            [u_res_slice_df, u_res_slope_df], title=title, grid=True, marker=["x", None]
        ).with_ylabel(ylabel="U_RES, [V]").with_xlim((-0.25, 0.25)).with_axhspan(
            ymin=-0.1, ymax=+0.1, facecolor="xkcd:yellowgreen"
        ).plot()

    # MP3 Results Tables
    def create_mp3_results_table(self) -> pd.DataFrame:
        """Method creating an MP3-compatible quench database row for 600A circuits with/without EE

        :return: pd.DataFrame with the results of analysis calculation
        """
        columns_mp3 = reference.get_quench_database_columns(circuit_type="600A", table_type="MP3")
        mp3_table = pd.DataFrame(columns=columns_mp3, index=self.results_table.index)

        mp3_table[columns_mp3] = self.results_table[columns_mp3]

        # Fill non-matching column names
        mp3_table["Timestamp_PIC"] = self.results_table.apply(
            lambda col: Time.to_string_short(col["timestamp_pic"]), axis=1
        )
        mp3_table["Delta_t(FGC-PIC)"] = self.results_table.apply(
            lambda col: 1e-6 * (col["timestamp_fgc"] - col["timestamp_pic"]), axis=1
        )
        mp3_table["Delta_t(QPS-PIC)"] = self.results_table.apply(
            lambda col: 1e-6 * (col["timestamp_qds_a"] - col["timestamp_pic"]), axis=1
        )
        if "timestamp_ee" in self.results_table.columns:
            mp3_table["Delta_t(EE-PIC)"] = self.results_table.apply(
                lambda col: 1e-6 * (col["timestamp_ee"] - col["timestamp_pic"]), axis=1
            )

        return mp3_table

    def create_mp3_results_table_rcdo(self) -> pd.DataFrame:
        """Method creating an MP3-compatible quench database row for RCD/RCO 600A circuits

        :return: pd.DataFrame with the results of analysis calculation
        """
        columns_mp3 = reference.get_quench_database_columns(circuit_type="600A_RCDO", table_type="MP3")
        mp3_table = pd.DataFrame(columns=columns_mp3, index=self.results_table.index)

        mp3_table[columns_mp3] = self.results_table[columns_mp3]

        # Fill non-matching column names
        timestamp_fgc = find_non_zero_min(
            self.results_table.loc[0, "timestamp_fgc_rcd"], self.results_table.loc[0, "timestamp_fgc_rco"]
        )

        timestamp_qds_a = find_non_zero_min(
            self.results_table.loc[0, "timestamp_qds_a_rcd"], self.results_table.loc[0, "timestamp_qds_a_rco"]
        )

        timestamp_pic = find_non_zero_min(
            self.results_table.loc[0, "timestamp_pic_rcd"], self.results_table.loc[0, "timestamp_pic_rco"]
        )

        mp3_table["Timestamp_PIC"] = Time.to_string_short(timestamp_pic)
        mp3_table["Delta_t(FGC-PIC)"] = 1e-6 * (timestamp_fgc - timestamp_pic)
        mp3_table["Delta_t(QPS-PIC)"] = 1e-6 * (timestamp_qds_a - timestamp_pic)
        mp3_table["Delta_t(EE_RCD-PIC)"] = self.results_table.apply(
            lambda col: 1e-6 * (col["timestamp_ee_rcd"] - timestamp_pic), axis=1
        )

        return mp3_table

    def create_mp3_results_table_rcbxhv(self) -> pd.DataFrame:
        """Method creating an MP3-compatible quench database row for RCBXH/RCBXV 600A circuits

        :return: pd.DataFrame with the results of analysis calculation
        """
        columns_mp3 = reference.get_quench_database_columns(circuit_type="600A_RCBXHV", table_type="MP3")
        mp3_table = pd.DataFrame(columns=columns_mp3, index=self.results_table.index)

        mp3_table[columns_mp3] = self.results_table[columns_mp3]

        # Fill non-matching column names
        timestamp_fgc = find_non_zero_min(
            self.results_table.loc[0, "timestamp_fgc_rcbxh"], self.results_table.loc[0, "timestamp_fgc_rcbxv"]
        )

        timestamp_qds_a = find_non_zero_min(
            self.results_table.loc[0, "timestamp_qds_a_rcbxh"], self.results_table.loc[0, "timestamp_qds_a_rcbxv"]
        )

        timestamp_pic = find_non_zero_min(
            self.results_table.loc[0, "timestamp_pic_rcbxh"], self.results_table.loc[0, "timestamp_pic_rcbxv"]
        )

        mp3_table["Timestamp_PIC"] = Time.to_string_short(timestamp_pic)
        mp3_table["Delta_t(FGC-PIC)"] = 1e-6 * (timestamp_fgc - timestamp_pic)
        mp3_table["Delta_t(QPS-PIC)"] = 1e-6 * (timestamp_qds_a - timestamp_pic)

        # Calculate I_radius
        i_q_h = 0 if np.isnan(self.results_table["I_Q_H"].values[0]) else self.results_table["I_Q_H"].values[0]
        i_q_v = 0 if np.isnan(self.results_table["I_Q_V"].values[0]) else self.results_table["I_Q_V"].values[0]
        mp3_table["I_radius"] = (i_q_h**2 + i_q_v**2) ** 0.5

        return mp3_table


def find_non_zero_min(timestamp_a: int, timestamp_b: int) -> int:
    """Function finding a non-zero minimum timestamp. It works as follows:
    - if any of the timestamps is 0, the other is returned; otherwise a minimum value of the two is returned.

    :param timestamp_a: first timestamp
    :param timestamp_b: second timestamp
    :return: non-zero minimum
    """
    if timestamp_a == 0:
        return timestamp_b
    elif timestamp_b == 0:
        return timestamp_a
    else:
        return min(timestamp_a, timestamp_b)
