"""This module contains a set of utility functions which calculate a feature over a pd.DataFrame.
They can also be used with lhcsmapi.signal_analysis.features.calculate_features"""

import pandas as pd
import numpy as np


def first20mean(x: pd.Series) -> pd.Series:
    """Function returning the mean value of the first 20 elements of the input signal

    Args:
        x: input signal

    Returns:
        Mean value of the first 20 elements of the input signal
    """
    return np.mean(x.iloc[0:20])


def last20mean(x: pd.Series) -> pd.Series:
    """Function returning the mean value of the last 20 elements of the input signal

    Args:
        x: input signal

    Returns:
        Mean value of the last 20 elements of the input signal
    """
    return np.mean(x.iloc[-20:-1])


def max_abs(df: pd.DataFrame) -> float:
    """Function calculating the maximum absolute value of the signal.
    Let min_df and max_df be the global minimum and maximum value of the input dataframe.
    Then, the returned value is min_df if |min_df| > max_df, otherwise the reutrned value is max_df.
    As a result, should the absolute value of the minimum current be greater than the maximum value, then the
    minimum value with the negative sign is returned.

    Args:
        df: input signal

    Returns:
        Maximum absolute value of the input signal
    """
    return df.max() if df.max() > abs(df.min()) else df.min()


def first(df: pd.DataFrame) -> pd.Series:
    """Utility function which returns the first row of a single-column dataframe.

    Args:
        df: input signal

    Returns:
        First row of the signal
    """
    return df.iloc[0]
