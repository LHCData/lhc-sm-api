import warnings
from typing import Dict

import pandas as pd
import numpy as np

from lhcsmapi.Time import Time
from lhcsmapi.analysis.CircuitAnalysis import CircuitAnalysis
from lhcsmapi.analysis.busbar.BusbarResistanceAnalysis import SingleBusbarResistanceAnalysis
from lhcsmapi.analysis.decorators import check_dataframe_empty
from lhcsmapi.analysis.dfb.DfbAnalysis import DfbAnalysis
from lhcsmapi.analysis.pc.PcAnalysis import PcAnalysis
from lhcsmapi.analysis.qds.QdsAnalysis import QdsAnalysis
from lhcsmapi.analysis.qh.QuenchHeaterVoltageAnalysis import QuenchHeaterVoltageAnalysis
from lhcsmapi.metadata import signal_metadata
from lhcsmapi.pyedsl.PlotBuilder import PlotBuilder, create_title
from lhcsmapi import reference


class IpqCircuitAnalysis(
    DfbAnalysis, PcAnalysis, QdsAnalysis, QuenchHeaterVoltageAnalysis, SingleBusbarResistanceAnalysis, CircuitAnalysis
):
    """Class for analysis of IPQ (Individually Powered Quadrupole) circuits"""

    def __init__(self, circuit_type, results_table, *, is_automatic=True, colormap="default", circuit_name):
        super().__init__(circuit_type, results_table, is_automatic, colormap)
        self.circuit_type = signal_metadata.get_circuit_type_for_circuit_name(circuit_name)

    # Timestamp table
    def create_timestamp_table(
        self,
        timestamp_dct: Dict[str, int],
        circuit_name: str = "",
        is_qps_exp: bool = True,
        qps_fgc_max_delay: float = 0.04,
    ) -> None:
        """Method creating timestamp table as well as checking the timing between considered timestamps.

        :param timestamp_dct: a dictionary from timestamp name to its value (it is assumed that does not contain NaN)
        :param circuit_name: name of a circuit
        :param is_qps_exp: flag checking whether a QPS is expected or not
        :param qps_fgc_max_delay: maximum time delay between FGC and QPS timestamps in s
        """
        super().create_timestamp_table(timestamp_dct)

        if is_qps_exp and np.isnan(timestamp_dct.get("QDS_A", np.nan)) and np.isnan(timestamp_dct.get("QDS_B", np.nan)):
            warnings.warn("Neither QDS board A nor B are present in the PM buffer.", stacklevel=2)
        elif np.isnan(timestamp_dct.get("QDS_A", np.nan)):
            warnings.warn("QDS Board A not present in PM buffer!", stacklevel=2)
        elif np.isnan(timestamp_dct.get("QDS_B", np.nan)):
            warnings.warn("QDS Board B not present in PM buffer!", stacklevel=2)
        else:
            if timestamp_dct["QDS_B"] - timestamp_dct["QDS_A"] != int(1e6):
                warnings.warn("QDS PM events (board A and B) are not synchronized to 1 ms!")

        if ("QDS_A" in timestamp_dct) and ("FGC_B1" in timestamp_dct):
            if abs(timestamp_dct["QDS_A"] - timestamp_dct["FGC_B1"]) > int(1e9 * qps_fgc_max_delay):
                warnings.warn(
                    "QDS_A and FGC_B1 PM events are not synchronized to +/- %d ms!" % int(1e3 * qps_fgc_max_delay)
                )
        else:
            warnings.warn(
                "Either QDS_A or FGC_B1 PM event is missing. "
                "Can not check synchronization of QDS and FGC_B1 timestamps!"
            )

        if ("QDS_A" in timestamp_dct) and ("FGC_B2" in timestamp_dct):
            if abs(timestamp_dct["QDS_A"] - timestamp_dct["FGC_B2"]) > int(1e9 * qps_fgc_max_delay):
                warnings.warn(
                    "QDS_A and FGC_B2 PM events are not synchronized to +/- %d ms!" % int(1e3 * qps_fgc_max_delay)
                )
        else:
            warnings.warn(
                "Either QDS_A or FGC_B2 PM event is missing. "
                "Can not check synchronization of QDS and FGC_B2 timestamps!"
            )

    def create_mp3_results_table(self):
        """Method creating an MP3-compatible quench database row for an IPQ circuit

        :return: pd.DataFrame with the results of analysis calculation
        """
        columns_mp3 = reference.get_quench_database_columns(circuit_type="IPQ", table_type="MP3")
        mp3_table = pd.DataFrame(columns=columns_mp3, index=self.results_table.index)

        mp3_table[columns_mp3] = self.results_table[columns_mp3]

        # Fill non-matching column names
        mp3_table["Timestamp_PIC"] = self.results_table.apply(
            lambda col: Time.to_string_short(col["timestamp_pic"]), axis=1
        )
        mp3_table["Delta_t(FGC-PIC)"] = self.results_table.apply(
            lambda col: 1e-6 * (min(col["timestamp_fgc_b1"], col["timestamp_fgc_b2"]) - col["timestamp_pic"]), axis=1
        )
        mp3_table["Delta_t(QPS-PIC)"] = self.results_table.apply(
            lambda col: 1e-6 * (col["timestamp_qds_a"] - col["timestamp_pic"]), axis=1
        )

        return mp3_table

    @staticmethod
    @check_dataframe_empty(mode="any", warning="At least one DataFrame is empty, plot of U_RES_B1-B 3 skipped!")
    def plot_u_res_b1_b2(
        circuit_name: str,
        timestamp_qds: int,
        u_res_b1_df: pd.DataFrame,
        u_res_b2_df: pd.DataFrame,
        t_quench_b1: float,
        t_quench_b2: float,
        threshold: float = 0.1,
    ) -> None:
        """Method plotting U_RES voltage from QPS for IPQ B1, B2. A yellow-green band for the detection threshold is
        displayed.

        :param circuit_name: name of the analyzed circuit (for the title)
        :param timestamp_qds: timestamp of the QDS PM event (for the title)
        :param u_res_b1_df: U_RES voltage from QPS for Q1 magnet
        :param u_res_b2_df: U_RES voltage from QPS for Q2 magnet
        :param t_quench_b1: time of quench start in B1
        :param t_quench_b2: time of quench start in B2
        :param threshold: voltage threshold
        """
        xmin = min([t_quench_b1 - threshold, t_quench_b2 - threshold])
        xmax = max([t_quench_b1 + 2 * threshold, t_quench_b2 + 2 * threshold])
        title = create_title(circuit_name, timestamp_qds, ["U_RES_B1", "U_RES_B2"])
        PlotBuilder().with_signal(
            [u_res_b1_df, u_res_b2_df], title=title, figsize=(15, 7), grid=True, marker="."
        ).with_ylabel(ylabel="U, [V]").with_xlim((xmin, xmax)).with_axhspan(
            ymin=-threshold, ymax=+threshold, facecolor="xkcd:yellowgreen"
        ).plot()
