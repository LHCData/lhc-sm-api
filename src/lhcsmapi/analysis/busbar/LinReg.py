from typing import List

import numpy as np


class LinReg(object):
    """Class with methods to perform linear regression with error estimation"""

    def __init__(self, theta=None):
        self.theta = theta
        self.sd_error = None

    def fit(self, x: np.ndarray, y: np.ndarray):
        """Method performing a linear fit to x given y

        :param x: input arguments
        :param y: input values to be fit
        :return: LinReg object with updated theta
        """
        self.theta = np.linalg.inv(x.T @ x) @ x.T @ y
        return self

    def predict(self, x: np.ndarray) -> np.ndarray:
        """Method predicting values for given x and theta parameters stored in the object

        :param x: input vector for which values are predicted
        :return: a vector of predicted values
        """
        return x @ self.theta.reshape(-1, 1)

    @staticmethod
    def add_intercept_to_x(x: np.ndarray) -> np.ndarray:
        """Method adding an intercept term as the first element in the x vector

        :param x: x vector without the intercept term
        :return: x vector with the intercept term
        """
        x = x.reshape(-1, 1)
        n = x.shape[0]
        p = x.shape[1] + 1  # plus one because linear regression adds an intercept term

        x_with_intercept = np.empty(shape=(n, p), dtype=np.float)
        x_with_intercept[:, 0] = 1
        x_with_intercept[:, 1:p] = x

        return x_with_intercept

    @staticmethod
    def estimate_linear_fit_error(x_intercept: np.ndarray, y: np.ndarray, y_hat: np.ndarray) -> List[float]:
        """Method estimating the linear fit error parameters for a given dataset

        :param x_intercept: x vector with the intercept term
        :param y: values of the function
        :param y_hat: values of the fit
        :return: estimation of error in each parameter
        """
        n, p = x_intercept.shape
        residuals = y - y_hat
        residual_sum_of_squares = residuals.T @ residuals
        sigma_squared_hat = residual_sum_of_squares[0, 0] / (n - p)
        var_beta_hat = np.linalg.inv(x_intercept.T @ x_intercept) * sigma_squared_hat

        sd_error = []
        for p_el in range(p):
            sd_error.append(var_beta_hat[p_el, p_el] ** 0.5)

        return sd_error
