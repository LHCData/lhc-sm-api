from __future__ import annotations
import re
import warnings
from copy import deepcopy
from dataclasses import dataclass
from typing import Dict, List, Tuple

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from IPython.display import display
from sklearn.linear_model import LinearRegression

from lhcsmapi.analysis.busbar.LinReg import LinReg
from lhcsmapi.analysis.CircuitAnalysis import CircuitAnalysis
from lhcsmapi.api.processing import FeatureProcessing
from lhcsmapi.api.processing import SignalProcessing
from lhcsmapi.api.query_builder import QueryBuilder
from lhcsmapi.metadata import signal_metadata
from lhcsmapi.metadata.MappingMetadata import MappingMetadata
from lhcsmapi.pyedsl.AssertionBuilder import AssertionBuilder
from lhcsmapi.pyedsl.PlotBuilder import PlotBuilder


def generate_busbar_status(beam_mode_with_pattern_series: pd.DataFrame, pattern: Dict[str, float]) -> pd.DataFrame:
    """Function searching for provided beam mode patterns. It returns a DataFrame with beam mode indices and
    corresponding timestamps.

    :param beam_mode_with_pattern_series: a series containing all beam modes
    :param pattern: a particular beam mode pattern to be found
    :return: A DataFrame with timestamps corresponding to beam modes in the pattern
    """
    # find patterns long-enough
    beam_mode_spfr_ramp_pattern_df, _ = find_indexes_with_exact_pattern_in_series(
        beam_mode_with_pattern_series, pattern=list(pattern.values())
    )
    beam_mode_spfr_ramp_pattern_df["is_sb_long_enough"] = (
        beam_mode_spfr_ramp_pattern_df[14.0] - beam_mode_spfr_ramp_pattern_df[8.0]
    ) > 1800 * 1e9
    beam_mode_spfr_ramp_pattern_long_enough_df = beam_mode_spfr_ramp_pattern_df.query("is_sb_long_enough == True")

    t_setups = beam_mode_spfr_ramp_pattern_long_enough_df[2].values
    t_preramps = beam_mode_spfr_ramp_pattern_long_enough_df[6].values
    t_flattops = beam_mode_spfr_ramp_pattern_long_enough_df[8].values
    t_start_injs = np.array([max(t_s.item(), t_p.item() - int(1800 * 1e9)) for t_s, t_p in zip(t_setups, t_preramps)])
    t_end_injs = np.array([t_preramp.item() for t_preramp in t_preramps])
    t_start_sbs = np.array([t_flattop.item() for t_flattop in t_flattops])
    t_end_sbs = np.array([t_flattop.item() + int(1800 * 1e9) for t_flattop in t_flattops])
    busbar_df = pd.DataFrame(
        {"t_start_injs": t_start_injs, "t_end_injs": t_end_injs, "t_start_sbs": t_start_sbs, "t_end_sbs": t_end_sbs}
    )
    busbar_df.sort_values(by="t_start_injs", inplace=True)

    return busbar_df


def find_indexes_with_exact_pattern_in_series(
    series: pd.DataFrame, pattern: List[float]
) -> Tuple[pd.DataFrame, List[int]]:
    """Method finding indexes in a signal that match the input pattern

    :param series: input series/signal in which a pattern is searched
    :param pattern: pattern to be found
    :return: a tuple with a DataFrame containing rows matching exactly the pattern and indices of rows with the
    requested pattern.
    """
    series_with_last_index_of_pattern = series.rolling(len(pattern)).apply(
        lambda x: all(np.equal(x, pattern)), raw=False
    )
    df_with_pattern = series_with_last_index_of_pattern.fillna(0)
    last_indices_of_pattern = np.where(df_with_pattern)[0]
    all_indices_of_pattern = [range(match - len(pattern) + 1, match + 1) for match in last_indices_of_pattern]

    data = []
    for subs in all_indices_of_pattern:
        data.append(series.iloc[subs].index)

    flattened_indices = [val for indices_of_pattern in all_indices_of_pattern for val in indices_of_pattern]

    return pd.DataFrame(data, columns=pattern), flattened_indices


def get_complete_busbar_df(t_start: int, t_end: int, pattern: Dict[str, float], spark) -> pd.DataFrame:
    """Function querying a list of beam mode indices corresponding to the input pattern.

    :param t_start: start time of a search for a beam mode
    :param t_end: end time of a search for a beam mode
    :param pattern: pattern mapping from string to float
    :param spark: spark connector
    :return: DataFrame with results from NXCALS
    """
    # {'SETUP': 2.0, 'PRERAMP': 6.0, 'FLATTOP': 8.0, 'RAMPDOWN': 14.0}

    # query beam mode
    beam_mode_with_pattern_df = (
        QueryBuilder()
        .with_nxcals(spark)
        .with_duration(t_start=t_start, t_end=t_end)
        .with_query_parameters(nxcals_system="CMW", signal="HX:BMODE")
        .signal_query()
        .filter_values(list(pattern.keys()))
        .map_values(pattern)
        .get_dataframes()
    )

    # generate busbar status
    return generate_busbar_status(beam_mode_with_pattern_df["HX:BMODE"], pattern)


def extract_intersecting_plateaus(starts: List[List[int]], ends: List[List[int]]) -> Tuple[List[int], List[int]]:
    """Finds intersections between plateaus for each signal

    Args:
        starts: list of lists of start points for plateaus - one list for each signal
        ends: list of lists of end points for plateaus - one list for each signal

    Returns:
        a tuple consisting of list of start and list of end points for the instersecting plateaus
    """

    @dataclass
    class Plateau:
        start: int
        end: int

        def intersect(self, other_plateaus: List[Plateau]) -> List[Plateau]:
            return [intersection for other in other_plateaus if (intersection := self._get_intersection(other))]

        def _get_intersection(self, other: Plateau) -> Plateau:
            intersection = Plateau(max(self.start, other.start), min(self.end, other.end))
            if intersection.start >= intersection.end:
                return None
            return intersection

    if len(starts) == 1:
        return starts[0], ends[0]

    plateaus = [[Plateau(s, e) for s, e in zip(start, end)] for start, end in zip(starts, ends)]

    first_plateaus = plateaus.pop(0)

    first_two_intersections = []

    for plateau in first_plateaus:
        intersections = plateau.intersect(plateaus[0])
        first_two_intersections.extend(intersections)

    starts = [[i.start for i in first_two_intersections]] + starts[2:]
    ends = [[i.end for i in first_two_intersections]] + ends[2:]

    return extract_intersecting_plateaus(starts, ends)


def find_start_end_of_the_longest_ramp_up(plateau_start: List[int], plateau_end: List[int]) -> Tuple[int, int]:
    """Function finding the start and end of the longest ramp up

    :param plateau_start: list of current plateau starts (in ns)
    :param plateau_end: list of current plateau ends (in ns)
    :return: start and end of the longest ramp up
    """
    # calculate differences between plateau_start[i+1] and plateau_end[i] for i=0...len(plateau_start)-2
    ramp_up_duration = []
    for i in range(len(plateau_start) - 1):
        ramp_up_duration.append(plateau_start[i + 1] - plateau_end[i])

    # find the index of the maximum ramp up
    index_max_duration = ramp_up_duration.index(max(ramp_up_duration))

    return plateau_end[index_max_duration], plateau_start[index_max_duration + 1]


class BusbarResistanceAnalysis(CircuitAnalysis):
    """Base class for busbar resistance analysis"""

    @staticmethod
    def convert_to_col(res_row_df: pd.DataFrame, signal_name: str) -> pd.DataFrame:
        """Method converting a resistance value as a row into a column

        :param res_row_df: tow with resistance values
        :param signal_name: voltage signal name (e.g., U_RES)
        :return: DataFrame with resistances as a row
        """
        res_row_df = res_row_df.dropna(axis=1)
        res_row_df.index = [signal_name.replace("U", "R")]
        res_row_df = res_row_df.T
        res_row_df.index = [index.split("_")[0] + ":" + signal_name for index in res_row_df.index]
        res_row_df["nxcals_variable_name"] = res_row_df.index
        res_row_df.reset_index(inplace=True, drop=True)

        return res_row_df

    @staticmethod
    def calculate_current_plateau_start_end(
        i_meas_raw_dfs: List[pd.DataFrame], *, i_meas_threshold=50, min_duration_in_sec=1, time_shift_in_sec=(0, 0)
    ) -> Tuple[List[int], List[int]]:
        """Method calculating a list of intersecting plateaus across a list of power converter currents.

        :param i_meas_raw_dfs: list of power converter currents for plateau calculation
        :param i_meas_threshold: minimum power converter current
        :param min_duration_in_sec: minimum plateau duration in seconds
        :param time_shift_in_sec: tuple with the time subtracted from the start (first element) and end (second element)
        of a plateau
        :return: a tuple with a list of start and end times of intersecting plateaus
        """

        # Find plateau start and end based on the current profile
        for i_meas_raw_df in i_meas_raw_dfs:
            if i_meas_raw_df.max().values[0] < 100 and len(i_meas_raw_dfs) > 3:  # For IT it is OK
                print("There is at least one signal with the peak current below 100 A.")
                return [], []

        plateau_starts = []
        plateau_ends = []
        for i_meas_raw_df in i_meas_raw_dfs:
            if i_meas_raw_df.mean().values[0] > i_meas_threshold:
                plateau_start, plateau_end = BusbarResistanceAnalysis.find_plateau_start_and_end(
                    i_meas_raw_df,
                    i_meas_threshold=i_meas_threshold,
                    min_duration_in_sec=min_duration_in_sec,
                    time_shift_in_sec=time_shift_in_sec,
                )
                if plateau_start is None:
                    print("\tThe signal does not contain ramp-up!")
                    return [], []

                plateau_starts.append(plateau_start)
                plateau_ends.append(plateau_end)

        max_len_plateau_starts = max(len(plateau_start) for plateau_start in plateau_starts)
        max_len_plateau_ends = max(len(plateau_end) for plateau_end in plateau_ends)
        if max_len_plateau_starts != max_len_plateau_ends:
            return [], []

        return extract_intersecting_plateaus(plateau_starts, plateau_ends)

    @staticmethod
    def find_plateau_start_and_end(
        i_meas_raw_df: pd.DataFrame, i_meas_threshold=500, min_duration_in_sec=1, time_shift_in_sec=(0, 0)
    ) -> Tuple[List[int], List[int]]:
        """Method finding a tuple of two lists with starts and ends of current plateaus

        :param i_meas_raw_df: dataframe with power converter currents for plateau calculation
        :param i_meas_threshold: minimum power converter current
        :param min_duration_in_sec: minimum plateau duration in seconds
        :param time_shift_in_sec: tuple with the time subtracted from the start (first element) and end (second element)
        of a plateau
        :return: a tuple with a list of start and end times of intersecting plateaus
        """
        if sum(time_shift_in_sec) > min_duration_in_sec:
            raise ValueError(
                "The sum of time shift {} is greater than the minimum duration in sec {} s".format(
                    time_shift_in_sec, min_duration_in_sec
                )
            )

        i_meas_df = deepcopy(i_meas_raw_df)
        I_MEAS = i_meas_df.columns[0]

        i_meas_df_processing = SignalProcessing(i_meas_df).synchronize_time().convert_index_to_sec()

        timestamp = pd.DataFrame(
            data=i_meas_df.index.values,
            columns=["timestamp"],
            index=i_meas_df_processing.get_dataframes().index.values,
            dtype=int,
        )
        i_meas_df = i_meas_df_processing.resample(ts=1).get_dataframes()
        timestamp = SignalProcessing(timestamp).resample(ts=1).get_dataframes()

        i_meas_df["timestamp"] = timestamp["timestamp"].apply(lambda row: int(row))
        # take the current only above current threshold in [A]
        i_meas_df = i_meas_df[i_meas_df[I_MEAS] >= i_meas_threshold].copy()

        # calculate time derivative
        i_meas_df["dI_MEAS_dt"] = (
            SignalProcessing(i_meas_df.loc[:, [I_MEAS]]).calculate_time_derivative("diff", False).get_dataframes()
        )
        i_meas_df = i_meas_df.iloc[1:, :]  # the derivative is undefined for the first point

        # round to the nearest integer
        i_meas_df["round_dI_MEAS_dt"] = i_meas_df.apply(lambda row: np.around(row["dI_MEAS_dt"], decimals=1), axis=1)

        # get sign of the round value
        i_meas_df["sign_dI_MEAS_dt"] = i_meas_df.apply(lambda row: np.sign(row["round_dI_MEAS_dt"]), axis=1)

        # After computing a current derivative and taking its sign there are three potential outcomes:
        # - current increase: +1
        # - current plateau: 0
        # - current decrease: -1
        # The algorithm needs two current states:
        # - current plateau: 0
        # - current change: 1 (current increase: +1 and current decrease: -1)

        i_meas_df["sign_dI_MEAS_dt"] = i_meas_df["sign_dI_MEAS_dt"].abs()

        # The derivative will be 1 if the signal increases or decreases (since we got the `.abs` in the above line)
        # We now need to find the exact places where we have [1, 0], which means that the plateau is starting (from
        # increasing/decreasing to plateau).
        # And [0, 1] which means that the plateau is ending (from plateau to increasing/decreasing).

        SIGNAL_CHANGE = 1
        SIGNAL_PLATEAU = 0
        # Find change of current state
        _, plateau_start_pattern_idx = find_indexes_with_exact_pattern_in_series(
            i_meas_df["sign_dI_MEAS_dt"], [SIGNAL_CHANGE, SIGNAL_PLATEAU]
        )

        _, plateau_end_pattern_idx = find_indexes_with_exact_pattern_in_series(
            i_meas_df["sign_dI_MEAS_dt"], [SIGNAL_PLATEAU, SIGNAL_CHANGE]
        )

        # plateau start, plateau_end
        plateau_start = list(i_meas_df.loc[i_meas_df.index[plateau_start_pattern_idx[1::2]], "timestamp"].values)
        plateau_end = list(i_meas_df.loc[i_meas_df.index[plateau_end_pattern_idx[1::2]], "timestamp"].values)

        if len(plateau_start) < len(plateau_end):
            plateau_start.insert(0, i_meas_df["timestamp"].values[0])

        if plateau_end[0] < plateau_start[0]:
            plateau_start = [i_meas_df["timestamp"].values[0]] + plateau_start

        if len(plateau_start) > len(plateau_end):
            plateau_end.append(i_meas_df["timestamp"].values[-1])

        plateau_start_duration = []
        plateau_end_duration = []

        for plateau_start_el, plateau_end_el in zip(plateau_start, plateau_end):
            if (plateau_end_el - plateau_start_el) / 1e9 > min_duration_in_sec:
                plateau_start_duration.append(plateau_start_el + int(time_shift_in_sec[0] * 1e9))
                plateau_end_duration.append(plateau_end_el - int(time_shift_in_sec[1] * 1e9))

        return plateau_start_duration, plateau_end_duration

    @staticmethod
    def analyze_busbar_magnet_resistance(
        res_df: pd.DataFrame, signal_name: str, value_max: float, title=""
    ) -> pd.DataFrame:
        """Method analyzing busbar or magnet resistances by comparing them to a maximum allowed value

        :param res_df: DataFrame with resistances (column format)
        :param signal_name: name of resistance signal
        :param value_max: maximum allowed value
        :param title: plot title
        :return: a subset of the input DataFrame with resistances outside of the allowed range
        """
        features_outside_range_df = (
            AssertionBuilder()
            .with_feature(res_df)
            .has_min_max_value(feature=signal_name, value_min=0, value_max=value_max)
            .show_plot(xlabel="Busbar, [-]", ylabel=(f"{signal_name} (Calculated), [Ohm]"), title=title)
            .get_features_outside_range()
        )

        if not features_outside_range_df.empty:
            warning_message = (
                "Due to the limited accuracy of the measurement it cannot be excluded that "
                f"one or more joint resistances in the circuit are larger than {(value_max * 1e9):.6f} nOhm."
            )
            warnings.warn(warning_message, stacklevel=1)

        return features_outside_range_df

    @staticmethod
    def analyze_busbar_magnet_voltage(u_res_df: pd.DataFrame, value_max=5e-3, ylabel="", title="") -> pd.DataFrame:
        """Method analyzing busbar or magnet voltages by comparing to an allowed maximum

        :param u_res_df: DataFrame with voltages
        :param value_max: maximum allowed value
        :param ylabel: label for the y- axis
        :param title: plot title
        :return: DataFrame with voltages above the allowed maximum
        """
        return (
            AssertionBuilder()
            .with_feature(u_res_df)
            .has_min_max_value(feature="max_abs", value_min=0, value_max=value_max)
            .show_plot(xlabel="Busbar, [-]", ylabel=ylabel, title=title)
            .get_features_outside_range()
        )

    @staticmethod
    def check_signal_count(u_df: pd.DataFrame, expected_signal_count: int, signal: str) -> None:
        """Method checking the signal count,i.e., if there are all signals logged in the database.

        :param u_df: DataFrame with an signals to count
        :param expected_signal_count: expected count of signals
        :param signal: signal name (for print out)
        :return: None
        """
        non_nan_signal_count = len(u_df.dropna())
        if non_nan_signal_count < expected_signal_count:
            warnings.warn(f"There were missing {signal} signals. Found {non_nan_signal_count} signals, expected {156}.")
        else:
            print(f"All ({expected_signal_count}) {signal} signals were found.")

    @staticmethod
    def plot_i_meas_u_res_current_plateau(
        i_meas_df: pd.DataFrame,
        u_res_df: pd.DataFrame,
        t0: int,
        plateau_start: List[int],
        plateau_end: List[int],
        title="",
    ) -> None:
        """Method plotting current and voltage measurement along with current plateaus. Used for analyzing cases with
        resistances outside of range.

        :param i_meas_df: main power converter current signal
        :param u_res_df: magnet or busbar voltage
        :param t0: start time for plateau synchronization (in ns)
        :param plateau_start: list with starts of current plateaus (in ns)
        :param plateau_end: list with ends of current plateaus (in ns)
        :param title: plot title
        :return: None
        """
        ax = (
            PlotBuilder()
            .with_signal(i_meas_df, title=title, grid=True)
            .with_ylabel(ylabel="I_MEAS, [A]")
            .with_signal(u_res_df)
            .with_ylabel(ylabel="U_RES, [V]")
            .plot(show_plot=False)
            .get_axes()[0]
        )

        for ps, pe in zip(plateau_start, plateau_end):
            ax.axvspan(xmin=(ps - t0) / 1e9, xmax=(pe - t0) / 1e9, facecolor="xkcd:orange")
        plt.show()


class ItBusbarResistanceAnalysis(BusbarResistanceAnalysis):
    """Class with busbar calculation for IT circuit"""

    @staticmethod
    def calculate_resistance(i_meas_feature_df: pd.DataFrame, u_mag_feature_df: pd.DataFrame) -> pd.DataFrame:
        """Method for calculating resistance based on current and voltage features

        :param i_meas_feature_df: current features
        :param u_mag_feature_df: voltage features
        :return: DataFrame with resistances
        """

        i_rqx_df = i_meas_feature_df[i_meas_feature_df["device"].str.contains("RQX")]
        i_rtqx1_df = i_meas_feature_df[i_meas_feature_df["device"].str.contains("RTQX1")]
        i_rtqx2_df = i_meas_feature_df[i_meas_feature_df["device"].str.contains("RTQX2")]

        def get_i_q1() -> np.ndarray:
            return np.array(
                [
                    i_rqx_df[i_rqx_df["class"] == i]["mean"].values[0]
                    + i_rtqx1_df[i_rtqx1_df["class"] == i]["mean"].values[0]
                    for i in [1, 2]
                ]
            )

        def get_i_q2() -> np.ndarray:
            return np.array(
                [
                    i_rqx_df[i_rqx_df["class"] == i]["mean"].values[0]
                    + i_rtqx2_df[i_rtqx2_df["class"] == i]["mean"].values[0]
                    for i in [1, 2]
                ]
            )

        def get_i_q3() -> np.ndarray:
            return np.array([i_rqx_df[i_rqx_df["class"] == i]["mean"].values[0] for i in [1, 2]])

        res = {}
        error = {}
        u_mag_feature_df = u_mag_feature_df[(u_mag_feature_df["class"] > 0) & (u_mag_feature_df["class"] < 3)]
        for magnet_name, i_mean in [("Q1", get_i_q1()), ("Q2", get_i_q2()), ("Q3", get_i_q3())]:
            u_mean = (
                u_mag_feature_df[u_mag_feature_df["nxcals_variable_name"].str.contains(magnet_name)]
                .sort_values(by="class")["mean"]
                .values
            )
            u_std = (
                u_mag_feature_df[u_mag_feature_df["nxcals_variable_name"].str.contains(magnet_name)]
                .sort_values(by="class")["std"]
                .values
            )
            try:
                res[f"R_{magnet_name}"] = abs(LinearRegression().fit(i_mean.reshape(-1, 1), u_mean).coef_[0])
                error[f"R_{magnet_name}"] = (u_std[0] ** 2 + u_std[1] ** 2) ** (0.5) / abs(i_mean[1] - i_mean[0])
            except ValueError as ve:
                warnings.warn(str(ve))
                res[f"R_{magnet_name}"] = float("nan")
                error[f"R_{magnet_name}"] = float("nan")

        return pd.DataFrame({"R_RES": res, "error": error})

    @staticmethod
    def analyze_busbar_magnet_resistance(
        r_res_error_df: pd.DataFrame, signal_name: str, value_max: float, title=""
    ) -> pd.DataFrame:
        """Method analyzing busbar or magnet resistances by comparing them to a maximum allowed value

        :param res_df: DataFrame with resistances (column format)
        :param signal_name: name of resistance signal
        :param value_max: maximum allowed value
        :param title: plot title
        :return: a subset of the input DataFrame with resistances outside of the allowed range
        """
        features_outside_range_df = (
            AssertionBuilder()
            .with_feature(pd.DataFrame(r_res_error_df["R_RES"]))
            .has_min_max_value(feature=signal_name, value_min=0, value_max=value_max)
            .get_features_outside_range()
        )

        if not features_outside_range_df.empty:
            warning_message = (
                "Due to the limited accuracy of the measurement it cannot be excluded that "
                f"one or more joint resistances in the circuit are larger than {value_max:.6f} nOhm."
            )
            warnings.warn(warning_message, stacklevel=1)

        return features_outside_range_df

    @staticmethod
    def dataframe_to_string_resistance(resistance_df: pd.DataFrame):
        """
        Prints out the given resistance dataframe with truncated values expressed in nOhm.
        param resistance_df: resistance dataframe with format "magnet_name(index) resistance error"
        """
        for index, row in resistance_df.iterrows():
            print(f"{index} = {row['R_RES'] * 1e9:.2f} +/- {row['error'] * 1e9:.2f} nOhm")


class SingleBusbarResistanceAnalysis(BusbarResistanceAnalysis):
    """Class with a method for calculating a single busbar based on linear regression and error estimation"""

    @staticmethod
    def calculate_splice_resistance_linreg_with_errors(
        u_res_df: pd.DataFrame, i_meas_df: pd.DataFrame, plateau_start: List[int], plateau_end: List[int]
    ) -> float:
        """Method calculating splice resistance based on voltage and current signals during plateaus. In addition,
        estimation errors are displayed.

        :param u_res_df: voltage signal
        :param i_meas_df: current signal
        :param plateau_start: list of plateau starts
        :param plateau_end: list of plateau ends
        :return: resistance
        """
        # Merge both signals
        voltage_col = u_res_df.columns[0]
        current_col = i_meas_df.columns[0]
        u_i_plateau_df = merge_voltage_current(i_meas_df, u_res_df, plateau_start, plateau_end)

        # Calculate resistance as a linear fit
        x = u_i_plateau_df[current_col].values
        y = u_i_plateau_df[voltage_col].values
        x_intercept = LinReg.add_intercept_to_x(x)
        linreg = LinReg().fit(x_intercept, y)
        y_hat = linreg.predict(x_intercept)
        theta_error = LinReg.estimate_linear_fit_error(x_intercept, y, y_hat)

        u_i_plateau_df["fit"] = u_i_plateau_df[current_col] * linreg.theta[1] + linreg.theta[0]

        ax = u_i_plateau_df.plot.scatter(x=current_col, y=voltage_col, figsize=(15, 7))
        u_i_plateau_df.plot.line(x=current_col, y="fit", ax=ax, grid=True, color="red")
        ax.legend(["fit", voltage_col])
        ax.set_xlabel("I_MEAS, [A]")
        ax.set_ylabel("U_RES, [V]")

        print(
            f"The resistance estimate from the linear regression " f"is {linreg.theta[1]} Ohm +/- {theta_error[1]} Ohm."
        )

        return linreg.theta[1]

    @staticmethod
    def calculate_splice_resistance_linreg(
        u_res_df: pd.DataFrame, i_meas_df: pd.DataFrame, plateau_start: List[int], plateau_end: List[int]
    ) -> float:
        """Method calculating splice resistance based on voltage and current signals during plateaus

        :param u_res_df: voltage signal
        :param i_meas_df: current signal
        :param plateau_start: list of plateau starts
        :param plateau_end: list of plateau ends
        :return: resistance
        """
        # Merge both signals
        voltage_col = u_res_df.columns[0]
        current_col = i_meas_df.columns[0]
        u_i_plateau_df = merge_voltage_current(i_meas_df, u_res_df, plateau_start, plateau_end)

        # Calculate resistance as a linear fit
        linreg = LinearRegression().fit(
            np.array(u_i_plateau_df[current_col]).reshape((-1, 1)), u_i_plateau_df[voltage_col]
        )

        u_i_plateau_df["fit"] = u_i_plateau_df[current_col] * linreg.coef_[0] + linreg.intercept_
        ax = u_i_plateau_df.plot.scatter(x=current_col, y=voltage_col, figsize=(15, 7))
        u_i_plateau_df.plot.line(x=current_col, y="fit", ax=ax, grid=True, color="red")
        ax.legend(["fit", voltage_col])
        ax.set_xlabel("I_MEAS, [A]")
        ax.set_ylabel("U_RES, [V]")

        return linreg.coef_[0]


def merge_voltage_current(i_meas_df, u_res_df, plateau_start, plateau_end):
    idx = u_res_df.index.union(i_meas_df.index)
    u_i_merged = u_res_df.reindex(idx).interpolate("index")
    u_i_merged = u_i_merged.join(i_meas_df.reindex(idx).interpolate("index")).dropna()
    # Take only constant current part
    mask = np.zeros((len(u_i_merged.index), 1))
    for ps, pe in zip(plateau_start, plateau_end):
        mask_index = (u_i_merged.index >= ps) & (u_i_merged.index <= pe)
        mask = mask + mask_index.reshape((len(u_i_merged.index), 1)).astype(int)
    u_i_plateau_df = u_i_merged[mask.squeeze().astype(bool)]

    return u_i_plateau_df


def _get_slope_linear_fit_v_i(sector_to_i_meas_mean_df, busbar_voltage_df):
    if len(sector_to_i_meas_mean_df) == 1:
        return busbar_voltage_df.apply(
            lambda col: abs(col["mean_vector"][0]) / sector_to_i_meas_mean_df[col["sector"]].values[0], axis=1
        )
    else:
        return busbar_voltage_df.apply(
            lambda col: abs(
                LinearRegression()
                .fit(sector_to_i_meas_mean_df[col["sector"]].values.reshape((-1, 1)), col["mean_vector"])
                .coef_[0]
            ),
            axis=1,
        )


class MultipleBusbarResistanceAnalysis(BusbarResistanceAnalysis):
    """Method for calculating multiple busbars (e.g., RB, RQ)"""

    def _convert_current_into_mean_vector(self, i_busbar_feature_df: pd.DataFrame) -> pd.DataFrame:
        grouped_by_device = i_busbar_feature_df.groupby("device")
        out = {
            signal_metadata.get_sector_names_from_component(
                self.circuit_type, "PC", device
            ): grouped_by_device.get_group(device)["mean"].values
            for device in grouped_by_device.groups
        }

        return pd.DataFrame(out)

    def _convert_voltage_into_mean_vector(self, u_res_feature_df: pd.DataFrame) -> pd.DataFrame:
        gb = u_res_feature_df.groupby("class")
        dfs = [gb.get_group(x) for x in gb.groups]

        out = dfs[0]
        columns = set(out.columns) - {"nxcals_variable_name"}
        out = out.rename(columns={col: f"{col}_0" for col in columns})
        out.index.names = [None] * len(out.index.names)
        for index in range(1, len(dfs)):
            dfs[index].index.names = [None] * len(dfs[index].index.names)
            out = out.merge(dfs[index], on="nxcals_variable_name")
            # add index suffix to columns
            out = out.rename(columns={col: f"{col}_{index}" for col in columns})

        out["mean_vector"] = out.filter(regex="mean_").values.tolist()
        return out

    def calculate_resistance_from_lin_fit(
        self, busbar_voltage_df: pd.DataFrame, sector_to_i_meas_mean_df: pd.DataFrame
    ) -> pd.DataFrame:
        """Function calculating resistance from the slope of a linear fit of voltage and current

        :param circuit_type: type of a circuit
        :param busbar_voltage_df: dataframe with mean and std of voltage for linear fit computation
        :param sector_to_i_meas_mean_df: dataframe with mean and std of current for linear fit computation
        :return: a dataframe with a column containing one resistance per busbar
        """
        first_busbar = busbar_voltage_df.loc[0, "nxcals_variable_name"]
        RES_NAME = first_busbar.split(":")[1].replace("U_", "R_")

        if self.circuit_type == "RB":
            busbar_voltage_df["sector"] = busbar_voltage_df.apply(
                lambda row: signal_metadata.get_sector_names_from_component(
                    self.circuit_type, "BUSBAR", row["nxcals_variable_name".split(":")[0]]
                ),
                axis=1,
            )
        elif self.circuit_type == "RQ":

            def get_sector_name(nxcals_variable_name):
                busbar_name = "BUSBAR_RQD" if nxcals_variable_name[3] == "D" else "BUSBAR_RQF"
                sector_index = 0 if nxcals_variable_name[3] == "D" else 1
                return signal_metadata.get_sector_names_from_component(
                    self.circuit_type, busbar_name, nxcals_variable_name.split(":")[0]
                )[sector_index]

            busbar_voltage_df["sector"] = busbar_voltage_df.apply(
                lambda row: get_sector_name(row["nxcals_variable_name"]), axis=1
            )
        else:
            raise ValueError(f"Circuit type {self.circuit_type} not supported!")

        busbar_voltage_df[RES_NAME] = _get_slope_linear_fit_v_i(sector_to_i_meas_mean_df, busbar_voltage_df)

        return busbar_voltage_df

    def _convert_to_row(self, df, index: int):
        """Method converting a column of mean voltage and current into a row. Method only considers two steps of the
        current profile (at injection and at a stable beams).

        :param index: index for the output row
        :return: an updated ResistanceBuilderBusbarVoltageCurrent object
        """
        if "R_RES" in df.columns:
            out_df = df[["nxcals_variable_name", "mean_0", "std_0", "mean_1", "std_1", "R_RES"]]
        elif "R_MAG" in df.columns:
            out_df = df[["nxcals_variable_name", "mean_0", "std_0", "mean_1", "std_1", "R_MAG"]]
        else:
            raise KeyError(f"R_RES or R_MAG not present in DF: {df.columns}")

        out_df = out_df.assign(
            nxcals_variable_name_prefix=out_df["nxcals_variable_name"].str.split(":", n=2, expand=True)[0]
        )
        out_df.index = out_df["nxcals_variable_name_prefix"].values
        out_df.drop(columns=["nxcals_variable_name", "nxcals_variable_name_prefix"], inplace=True)

        out_df = pd.DataFrame(out_df.stack())
        out_df.index = out_df.index.map("_".join)
        out_df = out_df.T
        out_df.reset_index(inplace=True, drop=True)
        out_df.index = pd.Index([index], dtype="int")

        return out_df

    def _fill_missing_columns(self, df: pd.DataFrame, signal_name: str, circuit_name="*", split="_"):
        """Method filling missing columns corresponding to missing voltage measurements. Missing data is filled with
        NaNs.

        :param signal_name: name of a signal to fill with NaNs
        :param circuit_type: circuit type
        :param split: character used to separate busbar name and signal name
        :return: an updated ResistanceBuilderBusbarVoltageCurrent object
        """
        signal_name = signal_name.replace("U", "R")

        busbars_ordered_df = MappingMetadata.read_busbar_to_magnet_mapping(self.circuit_type)

        if circuit_name != "*":
            circuit_name = (
                signal_metadata.get_circuit_names(self.circuit_type) if self.circuit_type == "RB" else circuit_name
            )
            circuit_names = circuit_name if isinstance(circuit_name, list) else [circuit_name]
            is_busbar_in_circuit_name = busbars_ordered_df["Circuit"].isin(circuit_names)
            busbars_ordered_df = busbars_ordered_df[is_busbar_in_circuit_name]

        # Get all column names in df
        list_df_columns = [
            elem.split(split)[0] for elem in list(df.filter(regex=f"{re.escape(signal_name)}$", axis=1).columns)
        ]
        columns_missing = [
            column for column in busbars_ordered_df["Bus Bar Segment Name"].tolist() if column not in list_df_columns
        ]

        missing_cols_df = pd.DataFrame(columns=[f"{col}{split}{signal_name}" for col in reversed(columns_missing)])

        return pd.concat([missing_cols_df, df], axis=1)

    def _sort_columns_busbar_location(self, df, signal_name: str, circuit_name="*", split="_"):
        """Method sorting busbar location for RB and RQ circuits according to their physical location in the
        LHC tunnel.
        :param signal_name: name of a signal to sort
        :param circuit_type: circuit type (RB and RQ supported)
        :param circuit_name: name of a circuit, e.g., RB.A12
        :param split: character on which signal name is split to recover busbar name for sorting
        :return: an updated ResistanceBuilderBusbarVoltageCurrent object
        """
        out = df.copy()
        signal_name = signal_name.replace("U", "R")

        out.drop([col for col in out.columns if col[-1] not in "_%s" % signal_name], axis=1, inplace=True)
        res_df_t = out.T
        res_df_t["nxcals_variable_name"] = res_df_t.index
        res_df_t.reset_index(inplace=True, drop=True)

        res_df_t = (
            FeatureProcessing(res_df_t)
            .sort_busbar_location(circuit_type=self.circuit_type, circuit_name=circuit_name, split=split)
            .get_dataframe()
        )

        res_df_t.set_index(res_df_t["nxcals_variable_name"], inplace=True)
        res_df_t.rename(columns={"nxcals_variable_name": "Index"}, inplace=True)
        res_df_sorted = res_df_t.T
        res_df_sorted.drop(["Index"], inplace=True)
        res_df_sorted.index = pd.Index(res_df_sorted.index, dtype="int")

        return res_df_sorted

    def calculate_resistance(
        self,
        i_busbar_feature_df: pd.DataFrame,
        u_res_feature_df: pd.DataFrame,
        signal_name: str,
        t_start: int,
        circuit_name: str,
    ) -> pd.DataFrame:
        """Method calculating multiple busbars with mean current and voltage per current plateau.

        :param i_meas_feature_df: mean power converter current
        :param u_res_feature_df: mean busbar/magnet voltage
        :param signal_name: signal name
        :param t_start: start time for an output resistance index
        :param circuit_name: name of the circuit to sort resistances according to their tunnel location
        :return: a column DataFrame with resistance values
        """

        def _get_class_not_zero(df):
            return df[df["class"] != 0] if "class" in df else df

        i_busbar_feature_df = _get_class_not_zero(i_busbar_feature_df)
        u_res_feature_df = _get_class_not_zero(u_res_feature_df)

        mean_current = self._convert_current_into_mean_vector(i_busbar_feature_df)
        mean_voltage = self._convert_voltage_into_mean_vector(u_res_feature_df)

        mean_resistance = self.calculate_resistance_from_lin_fit(mean_voltage, mean_current)
        row_df = self._convert_to_row(mean_resistance, t_start)
        filled_df = self._fill_missing_columns(row_df, signal_name, circuit_name)
        sorted_df = self._sort_columns_busbar_location(filled_df, signal_name, circuit_name)

        return sorted_df

    @staticmethod
    def merge_busbar_metadata_with_resistance(
        res_busbar_df, circuit_type, circuit_name, split_char=":", res_col="R_RES"
    ):
        res_busbar_copy_df = deepcopy(res_busbar_df)
        busbars_ordered_df = MappingMetadata.read_busbar_to_magnet_mapping(circuit_type)
        circuit_name = circuit_name if isinstance(circuit_name, list) else [circuit_name]
        is_busbar_in_circuit_name = busbars_ordered_df["Circuit"].isin(circuit_name)
        busbars_ordered_per_circuit_df = busbars_ordered_df[is_busbar_in_circuit_name]

        res_busbar_copy_df["Bus Bar Segment Name"] = res_busbar_copy_df["nxcals_variable_name"].apply(
            lambda col: col.split(split_char)[0]
        )
        busbar_metadata_resistance_df = busbars_ordered_per_circuit_df.merge(
            res_busbar_copy_df, on="Bus Bar Segment Name", right_index=False
        )
        return busbar_metadata_resistance_df[
            ["QPS Crate&Board", "Bus Bar Segment Name", "1st Magnet", "2nd Magnet", "Num of splices", res_col]
        ]

    @staticmethod
    def display_busbar_metadata_resistance_with_threshold(busbar_metadata_resistance_df, threshold, res_col="R_RES"):
        busbar_metadata_resistance_df["is_outlier"] = busbar_metadata_resistance_df[res_col] > threshold
        color = busbar_metadata_resistance_df["is_outlier"].map({True: "background-color: red", False: ""})
        busbar_metadata_resistance_df[res_col] = busbar_metadata_resistance_df[res_col].map("{:.2E}".format)
        busbar_metadata_resistance_df.drop(columns="is_outlier", inplace=True)
        display(busbar_metadata_resistance_df.style.apply(lambda s: color))
