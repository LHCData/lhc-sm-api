from typing import cast, List, Tuple

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pyspark.sql
import pyspark.sql.functions as F
from IPython.display import display
from IPython.display import HTML
from nxcals.api.extraction.data.builders import DataQuery
from pyspark.sql.types import IntegerType

from lhcsmapi.analysis.busbar.BusbarResistanceAnalysis import BusbarResistanceAnalysis
from lhcsmapi.analysis.busbar.BusbarResistanceAnalysis import find_start_end_of_the_longest_ramp_up
from lhcsmapi.analysis.CircuitQuery import CircuitQuery
from lhcsmapi.analysis.CircuitQuery import execution_count
from lhcsmapi.analysis.expert_input import check_show_next
from lhcsmapi.api import processing
from lhcsmapi.api import resolver
from lhcsmapi.api.query_builder import QueryBuilder
from lhcsmapi.Time import Time
from lhcsmapi import utils


class BusbarResistanceQuery(CircuitQuery):
    """Class for query of busbar voltage and current to calculate resistance"""

    @execution_count()
    def query_busbar_features_nxcals(
        self,
        t_start,
        t_end=None,
        duration=None,
        *,
        signal="U_RES",
        features=None,  # the argument is left here in order not to change the notebooks
        spark,
    ) -> pd.DataFrame:
        """Method querying NXCALS to compute features of a busbar signal

        :param t_start: start time of a query
        :param t_end: end time of a query (optional)
        :param duration: duration w.r.t. t_start (optional)
        :param t0: synchronization time for a signal
        :param signal: signal name to query (scalar)
        :param features: list of features to query
        :param spark: spar connector
        :return: DataFrame with columns corresponding to the features of the selected busbar signal
        """
        t_start, t_end = Time.get_query_period_in_unix_time(t_start, t_end, duration, "")
        duration = t_end - t_start
        query_params = resolver.get_params_for_nxcals(
            self.circuit_type,
            self.circuit_name,
            "BUSBAR",
            t_start,
            duration,
            signals=[signal],
            wildcard={"BUSBAR": "*"},
        )

        raw_nxcals_dataset = (
            DataQuery.builder(spark)
            .byVariables()
            .system(query_params.system)
            .startTime(t_start)
            .endTime(t_end)
            .variables(query_params.signals)
            .build()
        )

        features_dataframe = (
            raw_nxcals_dataset.groupBy("nxcals_variable_name")
            .agg(
                F.max("nxcals_value").alias("max"),
                F.min("nxcals_value").alias("min"),
                F.max(F.abs("nxcals_value")).alias("max_abs"),
            )
            .toPandas()
        )

        return (
            processing.FeatureProcessing(features_dataframe)
            .sort_busbar_location(self.circuit_type, self.circuit_name)
            .get_dataframe()
        )

    @execution_count()
    def calculate_current_plateau_start_end(
        self, t_start, t_end, *, i_meas_threshold=50, min_duration_in_sec=1, time_shift_in_sec=(0, 0), spark
    ) -> Tuple[List[int], List[int]]:
        """Method querying and calculating current plateau start and end intervals

        :param t_start: start time for the query
        :param t_end: end tim for the query
        :param i_meas_threshold: minimum current to be considered for plateau calculation
        :param min_duration_in_sec: minimum duration of a plateau
        :param time_shift_in_sec: time shift in seconds from the start and the end of a plateau
        :param spark: spark connector
        :return: tuple of start and end plateau timestamps (each as a list)
        """

        i_meas_raw_df = (
            QueryBuilder()
            .with_nxcals(spark)
            .with_duration(t_start=t_start, t_end=int(t_end))
            .with_circuit_type(self.circuit_type)
            .with_metadata(circuit_name=self.circuit_name, system="PC", signal="I_MEAS")
            .signal_query()
            .get_dataframes()
        )

        return BusbarResistanceAnalysis.calculate_current_plateau_start_end(
            utils.vectorize(i_meas_raw_df),
            i_meas_threshold=i_meas_threshold,
            min_duration_in_sec=min_duration_in_sec,
            time_shift_in_sec=time_shift_in_sec,
        )

    @execution_count()
    def get_busbar_resistances(
        self,
        t_start,
        t_end,
        plateau_start: List[int],
        plateau_end: List[int],
        *,
        signal_name="U_RES",
        system="BUSBAR",
        spark,
    ) -> Tuple[pd.DataFrame, pd.DataFrame]:
        """Method calculating, with the NXCALS cluster, busbar resistances between start and end time as well as
        current plateaus.

        :param t_start: query start time (in unix time)
        :param t_end: query end time (in unix time)
        :param plateau_start: list of starts of plateaus
        :param plateau_end: list of ends of plateaus
        :param signal_name: name of a voltage signal to query
        :param system: system to query
        :param spark: spark connector
        :return: DataFrame with resistances
        """

        plateau_start = np.array(plateau_start)
        plateau_end = np.array(plateau_end)
        ramp_up_start, ramp_up_end = find_start_end_of_the_longest_ramp_up(plateau_start, plateau_end)

        def classify_phase(timestamp):
            """Determines in which plateau `timestamp` lays.

            Returns 0 if `timestamp` is during the longest ramp up phase or the plateau index.
            Otherwise, returns -1.
            """
            mask_plateau = (timestamp >= plateau_start) & (timestamp <= plateau_end)
            index_plateau = np.where(mask_plateau == True)[0]

            if len(index_plateau) > 0:
                return int((index_plateau[0] + 1))

            if (timestamp > ramp_up_start) and (timestamp < ramp_up_end):
                return 0

            return -1

        classify_phase_udf = F.udf(classify_phase, IntegerType())

        i_meas_dataset = self._query_nxcals_for_i_meas(spark, t_start, t_end)

        i_meas_feature_df = _get_i_meas_df(i_meas_dataset, classify_phase_udf)

        i_meas_feature_df = processing.FeatureProcessing(i_meas_feature_df).sort_values(by="class").get_dataframe()

        u_res_dataset = self._query_nxcals_for_signals(spark, system, t_start, t_end, signal_name)

        u_res_feature_df = _get_u_res_feature_df(u_res_dataset, classify_phase_udf)

        u_res_feature_df = (
            processing.FeatureProcessing(u_res_feature_df)
            .sort_busbar_location(self.circuit_type, circuit_name=self.circuit_name)
            .correct_voltage_sign()
            .get_dataframe()
        )

        return u_res_feature_df, i_meas_feature_df

    @staticmethod
    def query_and_plot_outlier_voltage(
        res_outliers_df: pd.DataFrame,
        t_start,
        t_end,
        t_0: int,
        plateau_start: List[int],
        plateau_end: List[int],
        *,
        is_automatic=False,
        spark,
    ) -> None:
        """Method querying and plotting outlier voltages. Outliers are provided from the resistance assertion.

        :param res_outliers_df: DataFrame with outlier resistances
        :param t_start: start time of a query
        :param t_end: end time of a query
        :param t_0: synchronization time for current plateaus
        :param plateau_start: start of current plateaus (for plotting)
        :param plateau_end: end of current plateaus (for plotting)
        :param is_automatic: flag for looping through the outliers
        :param spark: spark connector
        :return: None
        """
        if not res_outliers_df.empty:
            print("Resistance(s) outside of range:")
            display(HTML(res_outliers_df.to_html()))
        else:
            print("All resistances within the range.")

        for index, row in res_outliers_df.iterrows():
            u_df = (
                QueryBuilder()
                .with_nxcals(spark)
                .with_duration(t_start=t_start, t_end=t_end)
                .with_query_parameters(nxcals_system="CMW", signal=row["nxcals_variable_name"])
                .signal_query()
                .synchronize_time()
                .convert_index_to_sec()
                .get_dataframes()
            )

            if not u_df.empty:
                ax = u_df.plot(figsize=(15, 7), grid=True)
                # Add ranges of detected plateaus in orange
                for ps, pe in zip(plateau_start, plateau_end):
                    ax.axvspan(
                        xmin=(ps - Time.to_unix_timestamp(t_0)) / 1e9,
                        xmax=(pe - Time.to_unix_timestamp(t_0)) / 1e9,
                        facecolor="xkcd:goldenrod",
                    )
                ax.set_xlabel("time, [s]", fontsize=15)
                ax.set_ylabel("U, [V]", fontsize=15)
                ax.tick_params(labelsize=15)
                plt.show()

            if check_show_next(index, len(res_outliers_df), is_automatic=is_automatic):
                break

    def _query_nxcals_for_signals(self, spark, system, t_start, t_end, signals) -> pyspark.sql.DataFrame:
        query_params = resolver.get_params_for_nxcals(
            self.circuit_type,
            self.circuit_name,
            system,
            int(t_start),
            int(t_end) - int(t_start),
            signals=signals,
            wildcard={"BUSBAR": "*"},
        )
        return (
            DataQuery.builder(spark)
            .byVariables()
            .system(query_params.system)
            .startTime(t_start)
            .endTime(t_end)
            .variables(query_params.signals)
            .build()
        )

    def _query_nxcals_for_i_meas(self, spark, t_start, t_end) -> pyspark.sql.DataFrame:
        params = resolver.get_params_for_nxcals(
            self.circuit_type, self.circuit_name, "PC", int(t_start), int(t_end) - int(t_start), signals=["I_MEAS"]
        )
        query = (
            DataQuery.builder(spark)
            .byVariables()
            .system(params.system)
            .startTime(t_start)
            .endTime(t_end)
            .variables(params.signals)
        )

        return query.build()


def _get_i_meas_df(dataset: pyspark.sql.DataFrame, udf) -> pd.DataFrame:
    df = (
        dataset.select("nxcals_variable_name", udf("nxcals_timestamp").alias("class"), "nxcals_value")
        .filter(F.col("class") != -1)
        .groupBy("nxcals_variable_name", "class")
        .agg(F.mean("nxcals_value").alias("mean"), F.stddev("nxcals_value").alias("std"))
        .sort("class")
        .toPandas()
    )
    df["nxcals_variable_name"] = df["nxcals_variable_name"].str.split(":").str[0]
    df = df.rename(columns={"nxcals_timestamp": "timestamp", "nxcals_variable_name": "device"})
    return cast(pd.DataFrame, df)


def _get_u_res_feature_df(dataset: pyspark.sql.DataFrame, udf) -> pd.DataFrame:
    df = (
        dataset.select("nxcals_variable_name", udf("nxcals_timestamp").alias("class"), "nxcals_value")
        .filter(F.col("class") != -1)
        .groupBy("nxcals_variable_name", "class")
        .agg(F.mean("nxcals_value").alias("mean"), F.stddev("nxcals_value").alias("std"))
        .toPandas()
    )
    df = df.rename(columns={"nxcals_timestamp": "timestamp"})
    return cast(pd.DataFrame, df)
