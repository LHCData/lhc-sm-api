import warnings
from typing import Union, List, Tuple

import numpy as np
import pandas as pd
from pyspark.sql.session import SparkSession

from lhcsmapi.analysis.CircuitQuery import CircuitQuery, execution_count
from lhcsmapi.analysis.decorators import check_nan_timestamp, check_nan_timestamp_signals
from lhcsmapi.api import query, resolver
from lhcsmapi.metadata import signal_metadata
from lhcsmapi.api.query_builder import QueryBuilder
from lhcsmapi import reference
from lhcsmapi.Time import Time


class PcQuery(CircuitQuery):
    """Base class with methods for PC query in all circuits"""

    @execution_count()
    @check_nan_timestamp(return_type=pd.DataFrame())
    def find_source_timestamp_pc(self, t_start: Union[str, int, float], t_end: Union[str, int, float]) -> pd.DataFrame:
        """Method searching a PM FGC events between given start and end times.
        If either the start or end time is NaN, then an empty dataframe is returned.

        :param t_start: start time of a search
        :param t_end: end time of a search
        :return: pd.DataFrame with source and timestamp of the PM FGC events
        """
        return (
            QueryBuilder()
            .with_pm()
            .with_duration(t_start=t_start, t_end=t_end)
            .with_circuit_type(self.circuit_type)
            .with_metadata(circuit_name=self.circuit_name, system="PC")
            .event_query(verbose=self.verbose)
            .get_dataframe()
        )

    @execution_count()
    @check_nan_timestamp_signals(return_type=pd.DataFrame())
    def query_pc_nxcals(
        self,
        timestamp_fgc: int,
        *,
        signal_names: List[str],
        spark: SparkSession,
        duration: List[Tuple[int, str]] = [(300, "s"), (10, "s")],
    ) -> List[pd.DataFrame]:
        """Method querying NXCALS for iQPS signals

        :param timestamp_fgc: FGC PM event timestamp (ns precision) to which the query is synchronised
        :param signal_names: list of signal names to query according to metadata description
        :param spark: spark session variable needed to perform an NXCALS query
        :param duration: search duration before and after the start time
        :return: list of pd.DataFrames (if a signal was not queried, an empty pd.DataFrame is returned and warning
            containing the raw query is returned)
        """
        return (
            QueryBuilder()
            .with_nxcals(spark)
            .with_duration(t_start=timestamp_fgc, duration=duration)
            .with_circuit_type(self.circuit_type)
            .with_metadata(circuit_name=self.circuit_name, system="PC", signal=signal_names)
            .signal_query(verbose=self.verbose)
            .synchronize_time(timestamp_fgc)
            .convert_index_to_sec()
            .filter_median()
            .get_dataframes()
        )

    @execution_count()
    @check_nan_timestamp_signals(return_type=pd.DataFrame())
    def query_pc_pm(
        self, timestamp_fgc: int, timestamp_sync: int, *, signal_names: List[str]
    ) -> Union[pd.DataFrame, List[pd.DataFrame]]:
        """Method querying PM for power converter signals
        If any timestamp is NaN, then a list of empty dataframes of the size of the signal names is returned.
        The signals are synchronized in time and converted from ns to s.

        :param timestamp_fgc: FGC PM event timestamp (ns precision)
        :param timestamp_sync: timestamp to which the queried signals are synchronized (ns precision)
        :param signal_names: list of signal names to query according to metadata description
        :return: list of pd.DataFrames (if a signal was not queried, an empty pd.DataFrame is returned and warning
            containing the raw query is returned)
        """
        return (
            QueryBuilder()
            .with_pm()
            .with_timestamp(timestamp_fgc)
            .with_circuit_type(self.circuit_type)
            .with_metadata(circuit_name=self.circuit_name, system="PC", signal=signal_names)
            .signal_query(verbose=self.verbose)
            .synchronize_time(timestamp_sync)
            .convert_index_to_sec()
            .get_dataframes()
        )

    @execution_count()
    @check_nan_timestamp(return_type=pd.DataFrame(columns=["EVENTS.ACTION", "EVENTS.SYMBOL"]))
    def query_pc_pm_events(self, timestamp_fgc: int) -> pd.DataFrame:
        """Method querying PM for FGC event signals
        If the timestamp is NaN, then a list of empty dataframes of the size of the signal names is returned.
        Note that the output signal may have duplicated indices and is neither synchronized nor converted to s.

        :param timestamp_fgc: FGC PM event timestamp (ns precision)
        :return: list of pd.DataFrames (if a signal was not queried, an empty pd.DataFrame is returned and warning
            containing the raw query is returned)
        """
        columns = ["EVENTS.ACTION", "EVENTS.SYMBOL"]
        params = resolver.get_params_for_pm_events(self.circuit_type, self.circuit_name, "PC", timestamp_fgc, 1)
        triplet = params.triplets[0]  # we know that it is used for the circuits with a single PC only
        print(
            f"\tQuerying PM event signal(s) EVENTS.SYMBOL, EVENTS.ACTION for system: {triplet.system},"
            f" className: {triplet.class_}, source: {triplet.source} at {Time.to_string_short(timestamp_fgc)}"
        )
        data = query.query_raw_pm_data(triplet.system, triplet.class_, triplet.source, params.timestamp, columns)
        if not data:
            return pd.DataFrame(columns=columns)
        return pd.DataFrame(data=data).set_index("EVENTS.TIMESTAMP")


class Pc13kAQuery(PcQuery):
    """Class with methods for 13 kA PC analysis in RB and RQ"""

    @execution_count()
    def get_fgc_timestamp_ref(self, timestamp) -> int:
        """Method returning a reference timestamp for FGC

        :param timestamp: timestamp for which a reference is searched
        :return: unix timestamp in ns
        """
        return reference.get_power_converter_reference_fpa(self.circuit_type, self.circuit_name, "fgcPm", timestamp)

    @execution_count()
    def get_ee_timestamp_ref(self, timestamp) -> int:
        """Method returning a reference timestamp for EE

        :param timestamp: timestamp for which a reference is searched
        :return: unix timestamp in ns
        """
        return reference.get_power_converter_reference_fpa(self.circuit_type, self.circuit_name, "eePm", timestamp)


class PcItQuery(PcQuery):
    """Class with methods for IT PC analysis"""

    @staticmethod
    def split_source_timestamp_fgc(source_timestamp_fgc_df: pd.DataFrame, pc_name: str) -> Tuple[str, int]:
        """Method splitting source and timestamp for a given source and timestamp dataframe as well as IT power
        converter name.

        :param source_timestamp_fgc_df: a dataframe with FGC PM source and timestamp obtained with QueryBuilder
        :param pc_name: name of an IT circuit power converter for which the source and timestamp is retrieved
        :return: a tuple with FGC PM event source and timestamp
        """
        source_timestamp_fgc_with_pc_name_df = source_timestamp_fgc_df[
            source_timestamp_fgc_df["source"].str.contains(pc_name)
        ]
        if source_timestamp_fgc_with_pc_name_df.empty:
            return "", np.nan
        else:
            source = source_timestamp_fgc_with_pc_name_df["source"].values[0]
            timestamp = source_timestamp_fgc_with_pc_name_df["timestamp"].values[0]
            return source, timestamp

    @execution_count()
    def query_pc_pm_with_source(
        self, timestamp_fgc: int, timestamp_sync: int, source: str, *, signal_names: List[str]
    ) -> List[pd.DataFrame]:
        """Method querying PM for PC signals with a given source and a list of signal names.

        :param timestamp_fgc: timestamp of an FGC event (ns precision)
        :param timestamp_sync: timestamp to which the signals are synchronised, typically FGC timestamp,
            (ns precision)
        :param source: PM FGC source for which the query is executed
        :param signal_names: list of signal names to query according to metadata description
        :return: list of pd.DataFrame with synchronized signals there are as many elements as the signal names
        """

        if np.isnan(timestamp_fgc) or np.isnan(timestamp_sync):
            warnings.warn("timestamp_fgc or timestamp_sync are nan.")
            return [pd.DataFrame()] * len(signal_names)

        # Get metadata
        md_pc_pm = signal_metadata.get_signal_metadata(self.circuit_type, self.circuit_name, "PC", "PM", timestamp_fgc)

        # Get signal names to query - using general-purpose query
        if any([signal_name not in md_pc_pm for signal_name in signal_names]):
            raise ValueError("At least one signal name is not in the metadata, please correct your input!")

        signal_names_pm = [md_pc_pm[signal_name] for signal_name in signal_names]

        # Query and append suffix to columns
        suffix = "_" + source.split(".")[-2]
        return (
            QueryBuilder()
            .with_pm()
            .with_timestamp(timestamp_fgc)
            .with_query_parameters(
                system=md_pc_pm["system"], source=source, className=md_pc_pm["className"], signal=signal_names_pm
            )
            .signal_query(verbose=self.verbose)
            .synchronize_time(timestamp_sync)
            .convert_index_to_sec()
            .append_suffix_to_columns(suffix)
            .get_dataframes()
        )
