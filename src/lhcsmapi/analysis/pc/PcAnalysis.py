import warnings
from typing import List, Optional, Tuple

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from IPython.display import display, HTML
from ipywidgets import Output

import lhcsmapi.analysis.features_helper as utility_features
import lhcsmapi.signal_analysis.features as signal_analysis
import lhcsmapi.signal_analysis.functions as signal_analysis_functions
from lhcsmapi.Time import Time
from lhcsmapi.analysis import comparison
from lhcsmapi.analysis.CircuitAnalysis import CircuitAnalysis
from lhcsmapi.analysis.decorators import check_dataframe_empty, check_nan_timestamp
from lhcsmapi.pyedsl.AssertionBuilder import AssertionBuilder
from lhcsmapi.pyedsl.PlotBuilder import create_title, convert_plot_features_map_to_text, PlotBuilder
from lhcsmapi.api.processing import SignalProcessing
import lhcsmapi.signal_analysis.features as signal_analysis
import lhcsmapi.signal_analysis.functions as signal_analysis_functions
import lhcsmapi.analysis.features_helper as utility_features


class PcAnalysis(CircuitAnalysis):
    """Base class with methods for PC analysis in all circuits"""

    @staticmethod
    @check_dataframe_empty(mode="any", warning="PC signal is empty, plot is skipped!")
    def plot_i_meas(
        i_meas_df: pd.DataFrame,
        xlim: Tuple[float, float] = (np.nan, np.nan),
        ylim: Tuple[float, float] = (np.nan, np.nan),
        title="",
    ) -> None:
        """Method plotting a power converter current for a given x and y limits as well as a title.

        :param i_meas_df: main power converter current
        :param xlim: limits of the x-axis
        :param ylim: limits of the y-axis
        :param title: plot title
        :return: None
        """
        _plot_i_dfs(i_meas_df, title, None, None, xlim, ylim)

    @staticmethod
    @check_dataframe_empty(mode="any", warning="PC signals are empty, plot is skipped!")
    def plot_i_meas_with_current_plateau(i_meas_df, t0, plateau_start, plateau_end, title=""):
        """Method plotting power converter current along with a boxes for constant current part. It is assumed that
        the start and current plateau lists are of the same length.

        :param i_meas_df: power converter current
        :param t0: initial timestamp of a current profile to synchronize current plateau duration
        :param plateau_start: a list of starts of current plateaus
        :param plateau_end: a list of ends of current plateaus
        :param title: plot title
        :return: None
        """
        ax = (
            PlotBuilder()
            .with_signal(i_meas_df, title=title, grid=True)
            .with_ylabel(ylabel="I_MEAS, [A]")
            .plot(show_plot=False)
            .get_axes()[0]
        )

        for ps, pe in zip(plateau_start, plateau_end):
            ax.axvspan(
                xmin=(ps - Time.to_unix_timestamp(t0)) / 1e9,
                xmax=(pe - Time.to_unix_timestamp(t0)) / 1e9,
                facecolor="xkcd:goldenrod",
            )
        plt.show()

    @staticmethod
    @check_dataframe_empty(mode="any", warning="PC signal is empty, assertion is skipped!")
    def assert_v_meas(
        timestamp_ee: int,
        timestamp_pic: int,
        t_after_ee: int,
        v_meas_df: pd.DataFrame,
        value_min: float,
        value_max: float,
        title="",
        xmax=None,
    ) -> None:
        """Method asserting whether a power converter voltage is between minimum and maximum value for a given amount
        of time from a PIC timestamp. Plot of voltage along with an acceptance bar is displayed

        :param timestamp_ee: EE PM timestamp (second EE)
        :param timestamp_pic: PIC NXCALS timestamp
        :param t_after_ee: time after an EE trigger
        :param v_meas_df: power converter voltage
        :param value_min: minimum allowed voltage value
        :param value_max: maximum allowed voltage value
        :param title: plot title
        :param xmax: maximum x axis
        :return:
        """
        t_assertion = (timestamp_ee - timestamp_pic) * 1e-9 + t_after_ee
        v_meas_at_assertion = v_meas_df.reindex([t_assertion], method="nearest", tolerance=0.1).values[0]
        xmax = v_meas_df.index[-1] if xmax is None else xmax
        ax = (
            PlotBuilder()
            .with_signal(v_meas_df, title=title, grid=True)
            .with_ylabel(ylabel="V_MEAS, [V]")
            .with_xlim(lim=(0, xmax))
            .plot(show_plot=False)
            .get_axes()[0]
        )

        y_err = (value_max + value_min) / 2
        error_range = value_max - y_err
        ax.errorbar(t_assertion, y_err, yerr=[[error_range], [error_range]], fmt="o", color="red")
        plt.show()
        print("Analysis result")
        if value_min < v_meas_at_assertion < value_max:
            print(
                f"{v_meas_df.columns[0]} signal value at t = {t_assertion:.6f} s  ({v_meas_at_assertion[0]:.1f} V) "
                f"is within of the acceptance range [{value_min:.1f}, {value_max:.1f}] V."
            )
        else:
            warnings.warn(
                f"{v_meas_df.columns[0]} signal value at t = {t_assertion:.6f} s  ({v_meas_at_assertion[0]:.1f} V) is "
                f"outside of the acceptance range [{value_min:.1f}, {value_max:.1f}] V."
            )

    @check_dataframe_empty(mode="all", warning="PC signals are empty, plot is skipped!")
    def plot_i_meas_pc(
        self,
        circuit_name: str,
        timestamp_fgc: int,
        i_dfs: List[pd.DataFrame],
        xlim: Tuple[float, float] = (np.nan, np.nan),
        ylim: Tuple[float, float] = (np.nan, np.nan),
    ) -> None:
        """Method plotting power converter currents.
        If any input signal is missing, then the analysis is skipped.

        :param circuit_name: name of the analyzed circuit (for the title)
        :param timestamp_fgc: timestamp of the FGC PM event (for the title)
        :param i_dfs: list of power converter currents dataframes
        :param i_ref_dfs: list of reference power converter currents
        :param xlim: limits of the x-axis
        :param ylim: limits of the y-axis
        """
        title = create_title(circuit_name, timestamp_fgc, "I_MEAS, I_A, I_REF")

        def has_data(df: pd.DataFrame):
            if df is None or df.empty:
                warnings.warn("Input contains empty dataframes.")

                return False

            return True

        i_dfs = list(filter(has_data, i_dfs))

        i_meas_dfs, i_a_dfs, i_ref_dfs = _split_i_dataframes(i_dfs)
        color = _get_color(i_meas_dfs, i_a_dfs, i_ref_dfs)
        len_i_meas, len_i_ref, len_i_a = len(i_meas_dfs), len(i_ref_dfs), len(i_a_dfs)
        marker = [None] * len_i_meas + ["x"] * len_i_ref + [None] * len_i_a

        # Global view
        _plot_i_dfs(i_meas_dfs + i_ref_dfs + i_a_dfs, title, color, marker, xlim, ylim)

    @staticmethod
    @check_dataframe_empty(mode="any", warning="PC signals are empty, plot is skipped!")
    def plot_i_meas_pc_with_cursor(
        circuit_name: str, timestamp_fgc: int, i_dfs: List[pd.DataFrame], t_quench: float
    ) -> List[Tuple[float, bool]]:
        """Method plotting power converter currents.
        If any input signal is missing, then the analysis is skipped.

        :param circuit_name: name of the analyzed circuit (for the title)
        :param timestamp_fgc: timestamp of the FGC PM event (for the title)
        :param i_dfs: list of power converter currents dataframes
        :param i_ref_dfs: list of reference power converter currents
        """

        title = create_title(circuit_name, timestamp_fgc, "I_MEAS, I_A, I_REF")

        i_meas_dfs, i_a_dfs, i_ref_dfs = _split_i_dataframes(i_dfs)
        color = _get_color(i_meas_dfs, i_a_dfs, i_ref_dfs)
        len_i_meas, len_i_ref, len_i_a = len(i_meas_dfs), len(i_ref_dfs), len(i_a_dfs)
        marker = ["o"] * len_i_meas + ["x"] * len_i_ref + ["s"] * len_i_a

        fig, ax = plt.subplots(figsize=(13, 6.5))

        for index, df in enumerate(i_meas_dfs + i_ref_dfs + i_a_dfs):
            if not df.empty:
                df.plot(ax=ax, grid=True, title=title, color=color[index], marker=marker[index], legend=True)

        ax.title.set_size(20)
        ax.set_xlabel("t, [s]", fontsize=15)
        ax.set_ylabel("I, [A]", fontsize=15)
        ax.tick_params(labelsize=15)
        ax.set_xlim((t_quench - 0.5, t_quench + 0.5))
        ax.legend(loc="lower left")

        out = Output()
        display(out)

        with out:
            print(
                f"Our algorithm estimated {t_quench:.3f} s as start of the quench. \n"
                f"Press right button on a mouse on the figure above to adjust the quench start.",
                end="\r",
            )

        t_quench_correction = [[t_quench, False]]

        def onclick(event):
            if event.button == 3:
                x = event.xdata
                ylim = ax2.get_ylim()
                xlim = ax2.get_xlim()
                ax2.clear()
                ax2.plot([x, x], [-10, 10], "k--")
                ax2.get_yaxis().set_ticks([])
                ax2.set_xlim(xlim)
                ax2.set_ylim(ylim)

                t_quench_correction.append([x, True])

                with out:
                    print(
                        f"Expert selected {x:.3f} s as start of the quench. "
                        f"This value will be used for further computation.",
                        end="\r",
                    )

        # plot cursor
        ax2 = ax.twinx()
        ax2.plot([t_quench, t_quench], [-10, 10], "k--")
        ax2.set_ylim([0, 1])
        ax2.get_yaxis().set_ticks([])
        fig.canvas.mpl_connect("button_press_event", onclick)
        plt.show()
        plt.draw()

        return t_quench_correction

    @staticmethod
    def choose_quench_time_from_auto_or_manual(t_quench_correction) -> float:
        if t_quench_correction is None:
            t_quench = 0.0
            print(
                f"Automatically computed quench start {t_quench:.3f} s is used for further computation "
                f"(e.g., quench current, MIIts)."
            )
        elif len(t_quench_correction) == 1:
            t_quench = t_quench_correction[0][0]
            print(
                f"Automatically computed quench start {t_quench:.3f} s is used for further computation "
                f"(e.g., quench current, MIIts)."
            )
        else:
            t_quench = t_quench_correction[-1][0]
            print(
                f"Manually adjusted quench start {t_quench:.3f} s is used for further computation "
                f"(e.g., quench current, MIIts)."
            )

        return t_quench

    @staticmethod
    @check_dataframe_empty(mode="any", warning="I_MEAS signals are empty, tau calculation is skipped!")
    def calculate_i_meas_tau(i_meas_dfs: List[pd.DataFrame], duration_decay: Tuple[float, float] = (2, 100)):
        """Method calculating time constant based on power converter I_MEAS current

        :param i_meas_dfs: list of power converter I_MEAS currents
        :param duration_decay: duration of decay calculation
        :return: list of time constant calculated for a set of input currents
        """

        subtracted_min = signal_analysis.subtract_min_value(i_meas_dfs)
        selected_duration = signal_analysis.take_window(subtracted_min, *duration_decay)

        return signal_analysis.calculate_features(selected_duration, signal_analysis_functions.tau_energy).values[0]

    @check_dataframe_empty(mode="any", warning="PC signals are empty, plot is skipped!")
    def plot_i_meas_tau_pc(
        self,
        circuit_name: str,
        timestamp_fgc: int,
        i_dfs: List[pd.DataFrame],
        xlim: Tuple[float, float] = (np.nan, np.nan),
        ylim: Tuple[float, float] = (np.nan, np.nan),
    ) -> None:
        """Method plotting power converter currents.
        If any input signal is missing, then the analysis is skipped.

        :param circuit_name: name of the analyzed circuit (for the title)
        :param timestamp_fgc: timestamp of the FGC PM event (for the title)
        :param i_dfs: list of power converter currents dataframes
        :param i_ref_dfs: list of reference power converter currents
        :param xlim: limits of the x-axis
        :param ylim: limits of the y-axis
        """
        title = create_title(circuit_name, timestamp_fgc, "I_MEAS, I_A, I_REF")

        i_meas_dfs, i_a_dfs, i_ref_dfs = _split_i_dataframes(i_dfs)
        color = _get_color(i_meas_dfs, i_a_dfs, i_ref_dfs)
        len_i_meas, len_i_ref, len_i_a = len(i_meas_dfs), len(i_ref_dfs), len(i_a_dfs)
        marker = [None] * len_i_meas + ["x"] * len_i_ref + [None] * len_i_a

        # Global view
        _plot_i_dfs(i_meas_dfs + i_ref_dfs + i_a_dfs, title, color, marker, xlim, ylim)

    @check_dataframe_empty(mode="any", warning="PC signals are empty, zoomed plot is skipped!")
    def plot_i_meas_pc_zoom(
        self,
        circuit_name: str,
        timestamp_fgc: int,
        t_quench: float,
        i_dfs: List[pd.DataFrame],
        xlim: Tuple[float, float] = (np.nan, np.nan),
        ylim: Tuple[float, float] = (np.nan, np.nan),
    ) -> None:
        """Method plotting power converter currents in zoom view around the start of a quench or
        If any input signal is missing, then the analysis is skipped.

        :param circuit_name: name of the analyzed circuit (for the title)
        :param timestamp_fgc: timestamp of the FGC PM event (for the title)
        :param t_quench: time of quench start (in seconds)
        :param i_dfs: list power converter currents
        :param i_ref_dfs: list power converter currents
        :param xlim: limits of the x-axis
        :param ylim: limits of the y-axis
        """
        title = create_title(circuit_name, timestamp_fgc, "I_MEAS, I_A, I_REF")

        # Zoomed view
        # # in x
        if np.isnan(xlim[0]) and np.isnan(xlim[1]):
            xlim = [t_quench - 0.01, 0.01]

        # # in y
        if np.isnan(ylim[0]) and np.isnan(ylim[1]):
            mid = 0
            for i_df in i_dfs:
                idx = i_df.index.get_indexer([t_quench], "nearest")[0]
                mid = max(mid, i_df[i_df.index == i_df.index[idx]].values[0][0])
            ylim = [mid - 10, mid + 10]

        i_meas_dfs, i_a_dfs, i_ref_dfs = _split_i_dataframes(i_dfs)
        color = _get_color(i_meas_dfs, i_a_dfs, i_ref_dfs)
        len_i_meas, len_i_ref, len_i_a = len(i_meas_dfs), len(i_ref_dfs), len(i_a_dfs)
        marker = [None] * len_i_meas + ["x"] * len_i_ref + [None] * len_i_a

        # Global view
        PlotBuilder().with_signal(
            i_meas_dfs + i_ref_dfs + i_a_dfs, title=title, color=tuple(color), marker=tuple(marker), grid=True
        ).with_ylabel(ylabel="I, [A]").with_axvline(x=t_quench, ls="--").with_xlim(lim=xlim).with_ylim(lim=ylim).plot()

    @check_dataframe_empty(mode="any", warning="At least one DataFrame is empty, PC trigger analysis skipped!")
    def analyze_i_meas_pc_trigger(self, timestamp_fgc: int, events_pc_df: pd.DataFrame) -> None:
        """Method finding and displaying the source of a PC trigger.
        If any input signal is missing, then the analysis is aborted.

        :param timestamp_fgc: FGC event timestamp
        :param events_pc_df: PC events action and symbol
        """
        fault_event_pc_df = events_pc_df[events_pc_df["EVENTS.ACTION"] == "FAULT"]
        if not fault_event_pc_df.empty:
            array = np.asarray(fault_event_pc_df.index)
            idx = (np.abs(array - timestamp_fgc)).argmin()

            print("The reason for PC PM event is:")
            display(HTML(fault_event_pc_df[fault_event_pc_df.index == fault_event_pc_df.index[idx]].to_html()))

    @check_dataframe_empty(mode="all", warning="I_MEAS signal is empty, dI_MEAS/dt analys skipped!")
    def calculate_current_slope(
        self, i_meas_df: pd.DataFrame, col_name: Tuple[str, str] = ("Ramp rate", "Plateau duration")
    ) -> None:
        """Method analyzing the power converter current derivative.
        If the input signal is missing, then the analysis is skipped.
        In case the current derivative prior to an event was zero (less than 0.1 A/s), then
        - the value of the current plateau and its duration are reported in the results table
        otherwise only the value of the derivative (ramp-rate) is reported in the results table

        :param i_meas_df: measured power converter current
        :param col_name: name of a column to set in results table
        """
        # Ramp Rate and Plateau Duration
        i_meas_df_der = i_meas_df[i_meas_df.index < 0]

        last_di_dt, last_di_dt_duration = SignalProcessing(i_meas_df_der).calculate_last_const_derivative()
        print(f"Last di/dt prior to an FPA is {last_di_dt:.6f} A/s.")
        print(f"Duration of the last di/dt prior to an FPA is {last_di_dt_duration:.6f} s (as seen in the PM buffer).")
        # Store analysis result
        if self.results_table is not None:
            if abs(last_di_dt) > 0.1:
                self.results_table[col_name[0]] = last_di_dt
                self.results_table[col_name[1]] = 0
            else:
                self.results_table[col_name[0]] = 0
                self.results_table[col_name[1]] = last_di_dt_duration

    @check_dataframe_empty(mode="any", warning="At least one DataFrame is empty, PC trigger analysis skipped!")
    def calculate_current_miits(self, i_meas_df: pd.DataFrame, t_quench: float, col_name: str = "I_MEAS MIITs") -> None:
        """Method calculating quench load (MIITs) of the power converter current.
        If any input signal is missing, then the analysis is aborted.
        The calculated MIITs is set in the results table.

        :param i_meas_df: measured and filtered power converter current
        :param t_quench: time of quench start (in seconds)
        :param col_name: name of a column to set the output results table
        """
        i_meas_pos_df = i_meas_df[i_meas_df.index >= t_quench]
        miits_i_meas = signal_analysis.calculate_features(i_meas_pos_df, signal_analysis_functions.miits)

        print(
            f"{col_name} is {miits_i_meas} MA^2s from t_1 = {i_meas_pos_df.index[0]:.6f} s (quench detection) "
            f"to t_2 = {i_meas_pos_df.index[-1]:.6f} s (end of PM buffer)."
        )

        # Store analysis result
        if self.results_table is not None:
            self.results_table[col_name] = miits_i_meas

    @check_dataframe_empty(mode="any", warning="At least one DataFrame is empty, PC trigger analysis skipped!")
    def calculate_current_miits_i_meas_i_a(
        self, i_meas_df: pd.DataFrame, i_a_df: pd.DataFrame, t_quench: float, col_name: str = "I_MEAS MIITs"
    ) -> None:
        """Method calculating quench load (MIITs) of the power converter current.
        If any input signal is missing, then the analysis is aborted.
        The calculated MIITs is set in the results table.

        :param i_meas_df: measured and filtered power converter current
        :param i_a_df: measured power converter current with high logging frequency and no filtering
        :param t_quench: time of quench start (in seconds)
        :param col_name: name of a column to set the output results table
        """
        i_meas_abs_pos_df = abs(i_meas_df[i_meas_df.index >= t_quench])
        i_a_abs_pos_df = abs(i_a_df[i_a_df.index >= t_quench])

        if i_a_df.values[-1] <= 0.05 * i_a_df.values[0]:
            i_df = i_a_abs_pos_df
        else:
            i_df = i_meas_abs_pos_df

        miits_i_meas = signal_analysis.calculate_features(i_df, signal_analysis_functions.miits)
        print(
            f"{col_name} is {miits_i_meas} MA^2s from t_1 = {i_df.index[0]:.6f} s (quench detection) "
            f"to t_2 = {i_df.index[-1]:.6f} s (end of PM buffer)."
        )

        # Store analysis result
        if self.results_table is not None:
            self.results_table[col_name] = miits_i_meas

    @check_dataframe_empty(mode="any", warning="I_EARTH signal is empty, analysis skipped!")
    def calculate_max_i_earth_pc(self, i_earth_df: pd.DataFrame, col_name: str = "Earth Current") -> None:
        """Method calculating the absolute maximum value of earth current in mA.
        If the input signal is missing, then the calculation is skipped.
        The calculated absolute maximum value is set in the results table.

        :param i_earth_df: current of the earth circuit in the power converter
        :param col_name: name of a column to set in results table
        """

        i_earth_median = 1000 * i_earth_df.dropna().median().values[0]
        i_earth_unbiased = 1000 * i_earth_df - i_earth_median

        i_earth_max_unbiased = signal_analysis.calculate_features(i_earth_unbiased, utility_features.max_abs)
        print(f"The maximum unbiased abs(I_EARTH) is {i_earth_max_unbiased:.2f} mA.")

        # Store analysis result
        if self.results_table is not None:
            self.results_table[col_name] = round(i_earth_max_unbiased, 6)

    @check_dataframe_empty(mode="any", warning="I_EARTH signal is empty, analysis skipped!")
    def plot_i_earth_pc(self, circuit_name: str, timestamp_fgc: int, i_earth_df: pd.DataFrame) -> None:
        """Method plotting the earth current in mA.
        If the input signal is missing, then the plot is skipped.

        :param circuit_name: name of the analyzed circuit (for the title)
        :param timestamp_fgc: timestamp of the FGC PM event (for the title)
        :param i_earth_df: current of the earth circuit in the power converter
        """
        title = create_title(circuit_name, timestamp_fgc, "I_EARTH")
        PlotBuilder().with_signal(1000 * i_earth_df, title=title, grid=True).with_ylabel(ylabel="IEARTH, [mA]").plot()

    @check_dataframe_empty(mode="any", warning="I_EARTH_PCNT signal is empty, analysis skipped!")
    def plot_i_earth_pcnt_pc(self, circuit_name: str, timestamp_fgc: int, i_earth_pcnt_df: pd.DataFrame) -> None:
        """Method plotting the percentage earth current.
        If the input signal is missing, then the plot is skipped.

        :param circuit_name: name of the analyzed circuit (for the title)
        :param timestamp_fgc: timestamp of the FGC PM event (for the title)
        :param i_earth_pcnt_df: current of the earth circuit in the power converter
        """
        title = create_title(circuit_name, timestamp_fgc, "I_EARTH_PCNT")
        PlotBuilder().with_signal(i_earth_pcnt_df, title=title, grid=True).with_ylabel(
            ylabel="I_EARTH_PCNT, [%]"
        ).plot()

    @check_dataframe_empty(mode="any", warning="At least one DataFrame is empty, I_MEAS quench analysis skipped!")
    @check_nan_timestamp(return_type=None, warning="t_quench is nan, analysis skipped!")
    def calculate_quench_current(
        self, i_meas_df: pd.DataFrame, t_quench: float, col_name: str = "Quench Current"
    ) -> None:
        """Method calculating the quench current, i.e., the current at the moment when I_A and I_REF start to
        deviate from one another.
        If any input signal is missing, then the analysis is skipped.
        The calculated quench current is set in the results table.

        :param i_meas_df: measured power converter current
        :param t_quench: time of quench start (in seconds)
        :param col_name: name of a column to set in results table
        """
        i_meas_pos_df = i_meas_df[i_meas_df.index >= t_quench]
        quench_current = round(i_meas_pos_df.iloc[0].values[0], 1)
        print(f"{col_name} is {quench_current} A.")

        # Store analysis result
        if self.results_table is not None:
            self.results_table[col_name] = quench_current

    @staticmethod
    @check_dataframe_empty(mode="any", warning="I_A_REF, I_A signals are empty, t_quench calculation skipped!")
    def estimate_quench_start_from_i_ref_i_a(i_ref_df: pd.DataFrame, i_a_df: pd.DataFrame) -> float:
        """Function calculating the time at which the reference and actual power converter currents start to diverge.
        The algorithm firstly takes an absolute value of both signals and re-interpolates them to the same sampling
        time.
        Then, the maximum difference for time range [-1, t=i_max] s is obtained.
        The time of deviation is equal to the first index for which the difference exceeds the maximum one before an
        event.

        :param i_ref_df: reference power converter current
        :param i_a_df: actual power converter current (non-filtered, high sampling rate)

        :return: time at which I_A and I_REF signals start to diverge
        """
        i_ref_column_name = i_ref_df.columns[0]
        i_a_column_name = i_a_df.columns[0]

        # take absolute value of each signal
        i_ref_abs_df = abs(i_ref_df)
        i_a_abs_df = abs(i_a_df)
        # re-interpolate both signals
        signal_comparison_df = join_signal_with_ref(i_a_abs_df, i_ref_abs_df)

        # find maximum difference between the time of maximum I_REF value and 0.1 second before/after
        t_i_ref_max = i_ref_abs_df[(i_ref_abs_df.index > -0.1) & (i_ref_abs_df.index < 0.1)].idxmax().values[0]
        diff_df = abs(signal_comparison_df[i_ref_column_name] - signal_comparison_df[i_a_column_name])
        max_diff = diff_df[(diff_df.index > (t_i_ref_max - 1)) & (diff_df.index < t_i_ref_max)].max()

        # find time of start of deviation from maximum difference between maximum value of I_REF and 0 (FGC timestamp)
        mask = (diff_df.index > t_i_ref_max) & (diff_df.index < 0) & (diff_df.values > max_diff)
        if diff_df[mask].empty:
            return 0.0
        return diff_df[mask].idxmin()

    @staticmethod
    def estimate_quench_start_from_i_ref(i_ref_df: pd.DataFrame) -> float:
        """Method finding the start of a quench as one time stamp before the drop of I_REF to 0 A around t = 0 s;
        +/- 2 s.

        :param i_ref_df: reference current of the power converter
        :return: one time stamp before the drop of I_REF to 0 A
        """
        t_i_ref_min = i_ref_df[(i_ref_df.index > -2) & (i_ref_df.index < 2)].idxmin().values[0]
        return i_ref_df[i_ref_df.index < t_i_ref_min].index[-1]

    @staticmethod
    @check_dataframe_empty(mode="any", warning="I_A_REF, I_A signals are empty, t_quench calculation skipped!")
    def detect_quench_start_from_i_ref_i_a(
        i_a_df: pd.DataFrame,
        i_ref_df: pd.DataFrame,
        fgc_source: str = None,
        moving_average: bool = True,
        ma_factor: int = 10,
        stable_part_indices: Tuple[float, float] = (-1, -0.5),
        std_factor: float = 3,
    ) -> Optional[float]:
        """Detects start of the quench from I_REF and I_A signals. Starting from the FGC timestamp (index 0),
        the quench start is detected as the last entry in a dataframe consisting of only the points laying in
        the statistical range.
        Args:
            i_a_df: an 'I_A' signal
            i_ref_df: an 'I_REF' signal
            fgc_source: a name of the source of the FGC
            moving_average: if True the signals will be smoothed before processing further
            ma_factor: size of the rolling window (number of points) for calculating moving average
            stable_part_indices: a tuple of the indices representing a stable part of signal
                                 (before quench which is usually at 0) which is used to calculate a mean and std
                                 of a signal
            std_factor: determines a range around the mean of the current calculated on a stable part
                                 for quench candidates search
        Returns:
            an estimate time of quench start (value of the index)

        """
        i_a_signal_name = i_a_df.columns[0]
        i_ref_signal_name = i_ref_df.columns[0]
        signal_comparison_df = join_signal_with_ref(i_a_df, i_ref_df)

        if moving_average:
            signal_comparison_df[i_a_signal_name] = signal_comparison_df[i_a_signal_name].rolling(ma_factor).mean()
            signal_comparison_df = signal_comparison_df.dropna()

        # Calculate I_ERR
        signal_comparison_df["I_ERR"] = signal_comparison_df[i_ref_signal_name] - signal_comparison_df[i_a_signal_name]
        last_point_idx = 0  # index at which we are sure that the quench is ongoing

        # Calculate statistical measurements
        signal_comparison_stable_df = signal_comparison_df.loc[stable_part_indices[0] : stable_part_indices[1]]
        i_err_avg = signal_comparison_stable_df["I_ERR"].mean()
        i_err_std = signal_comparison_stable_df["I_ERR"].std()

        # Find "last point" and points afterwards in range [I_ERR_avg-3*I_ERR_std,I_ERR_avg+3*I_ERR_std]
        searching_range = signal_comparison_df.loc[stable_part_indices[1] : last_point_idx]
        upper_bound = i_err_avg - std_factor * i_err_std
        lower_bound = i_err_avg + std_factor * i_err_std
        i_err_range_df = searching_range[searching_range.I_ERR.between(upper_bound, lower_bound, inclusive="both")]

        # Possible quench start
        i_err_range_df = i_err_range_df.reset_index()
        if i_err_range_df.empty:
            return None
        idx_quench_start = i_err_range_df.iloc[-1, 0]  # Criteria: First value within given range (from back)

        # Correction of moving average result
        if moving_average:
            # correction is based on a true window size (in s) divided by 2
            correction = (ma_factor * 0.001) / 2
            idx_quench_start = idx_quench_start - correction

        return idx_quench_start


class Pc13kAAnalysis(PcAnalysis):
    """Class with methods for 13 kA PC analysis in RB and RQ"""

    @check_dataframe_empty(mode="any", warning="PC signals are empty, I_MEAS plot is skipped!")
    def analyze_i_meas_pc(
        self,
        circuit_name: str,
        timestamp_fgc: int,
        timestamp_fgc_ref: int,
        timestamp_pic: int,
        i_meas_df: pd.DataFrame,
        i_meas_ref_df: pd.DataFrame,
        duration_decay=(2, 100),
    ) -> None:
        """Method analyzing and plotting main power converter current and compares to its reference.
        It computes characteristic time constant for a given time window. The actual characteristic decay time has to
        fall within +/- 20 % of the reference characteristic decay time.

        :param circuit_name: circuit name (either RB or RQ - 13 kA power converter)
        :param timestamp_fgc: FGC PM timestamp
        :param timestamp_fgc_ref: reference FGC PM timestamp
        :param timestamp_pic: PIC NXCALS timestamp
        :param i_meas_df: main power converter current
        :param i_meas_ref_df: reference power converter current
        :param duration_decay: time window over which the characteristic decay time is calculated
        :return: None
        """

        # Plot
        title = (
            f"{Time.to_string_short(timestamp_fgc)} {circuit_name}: I_MEAS(t) and I_MEAS(t)/dI_MEAS(t) "
            f"(Reference)\n"
        )

        tau_i_meas_df, tau_i_meas_ref_df = (
            SignalProcessing([i_meas_df, i_meas_ref_df]).calculate_char_time_profile().get_dataframes()
        )
        # Plot
        ylim = (50, 110) if self.circuit_type == "RB" else (0, 35)
        PlotBuilder().with_signal(
            [i_meas_df, i_meas_ref_df], title=title, grid=True, figsize=(13, 6.5), style=["-", "--"]
        ).with_ylabel(ylabel="I_MEAS, [A]").with_legend(labels=["I_MEAS", "I_MEAS (Reference)"]).with_signal(
            [tau_i_meas_df, tau_i_meas_ref_df], style=["-", "--"]
        ).with_ylabel(
            ylabel="-I_MEAS/dI_MEAS/dt [s]"
        ).with_legend(
            labels=["-I_MEAS/dI_MEAS_dt", "-I_MEAS/dI_MEAS_dt (Reference)"]
        ).with_ylim(
            ylim
        ).with_xlim(
            (-50, np.nan)
        ).plot()

        def get_rounded_max_i_meas(i_meas_dataframe):
            i_meas_max = i_meas_dataframe.max().values[0]
            return round(i_meas_max) if not np.isnan(i_meas_max) else np.nan

        # Display analysis results
        print("Analysis results")
        print("----------------")
        plot_features = {
            "Circuit name": [circuit_name, ""],
            "PIC Abort": [Time.to_string_short(timestamp_pic), ""],
            "PC Off": [Time.to_string_short(timestamp_fgc), ""],
            "Reference": [Time.to_string_short(timestamp_fgc_ref), ""],
            "max(I_MEAS)": [get_rounded_max_i_meas(i_meas_df), "A"],
            "max(I_MEAS, Reference)": [get_rounded_max_i_meas(i_meas_ref_df), "A"],
        }
        print(convert_plot_features_map_to_text(plot_features))

        # Calculate Features
        if i_meas_df.max().values[0] > 760 and (timestamp_pic - timestamp_fgc) < 40 * 1e9:
            self.calculate_display_current_characteristic_decay_time(i_meas_df, i_meas_ref_df, duration_decay)

    def calculate_display_current_characteristic_decay_time(
        self, i_meas_df: pd.DataFrame, i_meas_ref_df: pd.DataFrame, duration_decay=(2, 100)
    ) -> None:
        """Method computing characteristic time constant for a given time window. The actual characteristic decay time
        has to fall within +/- 20 % of the reference characteristic decay time.

        :param i_meas_df: main power converter current
        :param i_meas_ref_df: reference power converter current
        :param duration_decay: time window over which the characteristic decay time is calculated
        :return: None
        """

        i_meas_df, i_meas_ref_df = signal_analysis.take_window([i_meas_df, i_meas_ref_df], *duration_decay)
        tau_i_meas, tau_i_meas_ref = signal_analysis.calculate_features(
            [i_meas_df, i_meas_ref_df], signal_analysis_functions.tau_energy
        ).values[0]
        # Compare features to the reference
        pc_act_df = pd.DataFrame(
            {"tau_i_meas": round(tau_i_meas) if not np.isnan(tau_i_meas) else np.nan}, index=["act"]
        )
        tau_i_meas_comp_df = comparison.compare_features_to_reference(pc_act_df, self.circuit_type, "PC")
        pc_ref_df = pd.DataFrame(
            {"tau_i_meas": round(tau_i_meas_ref) if not np.isnan(tau_i_meas_ref) else np.nan}, index=["act"]
        )
        tau_i_meas_comp_ref_df = comparison.compare_features_to_reference(pc_ref_df, self.circuit_type, "PC")
        tau_i_meas_comp_ref_df.index = ["tau_i_meas_ref"]
        tau_i_meas_comp_df = pd.concat((tau_i_meas_comp_df, tau_i_meas_comp_ref_df), axis=0)
        comparison.check_feature_comparison(tau_i_meas_comp_df)

    @staticmethod
    @check_dataframe_empty(mode="any", warning="PC signals are empty, I_MEAS plot is skipped!")
    def plot_i_meas_smoothness(i_meas_df: pd.DataFrame, title="", value_min=-10, value_max=10, window=10) -> None:
        """Method calculating and displaying power converter current smoothness (second time derivative).
        The derivative is calculated as a rolling finite difference with a given window length.

        :param i_meas_df: main power converter current
        :param title: plot title
        :param value_min: minimum value for the current smoothness
        :param value_max: maximum value for the current smoothness
        :param window: size of a window for rolling derivative calculation
        :return: None
        """
        dt = i_meas_df.index[window - 1] - i_meas_df.index[0]
        i_meas_discharge_df = i_meas_df[i_meas_df.index >= 1]
        i_meas_discharge_dt_df = i_meas_discharge_df.rolling(window).apply(
            lambda x: (x[window - 1] - x[0]) / dt, raw=True
        )
        i_meas_discharge_dt_dt_df = i_meas_discharge_dt_df.rolling(window).apply(
            lambda x: (x[window - 1] - x[0]) / dt, raw=True
        )
        signal_name = i_meas_discharge_df.columns[0]
        i_meas_discharge_dt_dt_df.rename(columns={signal_name: f"d{signal_name}/dt^2"}, inplace=True)

        AssertionBuilder().with_signal([i_meas_discharge_dt_dt_df]).has_min_max_value(
            value_min=value_min, value_max=value_max
        ).show_plot(title=title, ylabel="dI_MEAS/dt^2, [A/s^2]", data_sec=i_meas_discharge_df, ylabel_sec="I_MEAS, [A]")

    @check_dataframe_empty(mode="any", warning="At least one DataFrame is empty, I_EARTH analysis skipped!")
    def analyze_i_earth_pc(
        self,
        circuit_name: str,
        timestamp_fgc: int,
        i_a_df: pd.DataFrame,
        i_earth_df: pd.DataFrame,
        i_earth_ref_df: pd.DataFrame,
        xlim=(-0.1, 0.3),
    ) -> None:
        """Method plotting main power converter current and earth current. Earth current plotted with its reference.

        :param circuit_name: RQ circuit name
        :param timestamp_fgc: RQ FGC timestamp (for title)
        :param i_a_df: main power converter current (not-filtered, high sampling-rate)
        :param i_earth_df: earth current
        :param i_earth_ref_df: reference earth current
        :param xlim: limits for x-axis
        :return: None
        """

        title = f"{Time.to_string_short(timestamp_fgc)} {circuit_name}: I_A and I_EARTH (Reference)"

        PlotBuilder().with_signal(i_a_df, title=title, grid=True).with_ylabel(ylabel="I_A, [A]").with_signal(
            [1000 * i_earth_df, 1000 * i_earth_ref_df], style=["-", "--"]
        ).with_ylabel(ylabel="I_EARTH, [mA]").with_legend(labels=["I_EARTH", "I_EARTH (Reference)"]).with_xlim(
            xlim
        ).plot()

    @staticmethod
    @check_dataframe_empty(mode="any", warning="At least one DataFrame is empty, I_EARTH_PCNT analysis skipped!")
    def analyze_i_earth_pcnt_pc(
        circuit_type: str,
        circuit_name: str,
        timestamp_fgc: int,
        i_meas_df: pd.DataFrame,
        i_meas_ref_df: pd.DataFrame,
        i_earth_pcnt_df: pd.DataFrame,
        i_earth_pcnt_ref_df: pd.DataFrame,
        xlim=(5, 120),
        start_point=3,
        offset=3,
        scaling=None,
    ) -> None:
        """
        Method plotting main power converter current and percentage earth current. Earth current is shown with its
        reference.
        Reference percentage earth current is scaled according to the main currents. A comment related to
        the percentage earth current is updated.
        In addition, similarity between the percentage earth currents is calculated.

        :param circuit_type: circuit type
        :param circuit_name: circuit name
        :param timestamp_fgc: FGC timestamp (for title)
        :param i_meas_df: main power converter current (for scaling)
        :param i_meas_ref_df: reference  main power converter current (for scaling)
        :param i_earth_pcnt_df: percentage earth current
        :param i_earth_pcnt_ref_df: reference percentage earth current
        :param xlim: the x limits of the plot
        :param start_point: the start_point of the analysis when comparing signals.
        :param offset: the valid range offset between reference and signals.
        :param scaling: if not provided 'i_meas_df.max() / i_meas_ref_df.max()' will be used
        :return: None
        """

        title = f"{Time.to_string_short(timestamp_fgc)} {circuit_name}: I_MEAS and I_EARTH_PCNT (Reference)"
        if scaling is None:
            scaling = (i_meas_df.max() / i_meas_ref_df.max()).values[0]
        PlotBuilder().with_signal(i_meas_df, title=title, grid=True).with_ylabel(ylabel="I_MEAS, [A]").with_signal(
            [
                i_earth_pcnt_df[i_earth_pcnt_df.index > start_point],
                i_earth_pcnt_ref_df[i_earth_pcnt_ref_df.index > start_point] * scaling,
            ],
            yerr=[None, offset],
        ).with_ylabel(ylabel="I_EARTH_PCNT, [%]").with_legend(
            labels=["I_EARTH_PCNT", "I_EARTH_PCNT (Reference)"]
        ).with_xlim(
            xlim
        ).plot()

        AssertionBuilder().with_signal(
            i_earth_pcnt_df[i_earth_pcnt_ref_df.index > start_point]
        ).is_between_two_references(
            first_ref=i_earth_pcnt_ref_df[i_earth_pcnt_ref_df.index > start_point],
            first_scaling=scaling,
            first_offset=offset,
            second_ref=i_earth_pcnt_ref_df[i_earth_pcnt_ref_df.index > start_point],
            second_scaling=scaling,
            second_offset=offset,
            is_plot=False,
        ).get_assertion_result()


class Pc13kARqAnalysis(Pc13kAAnalysis):
    """
    Class with methods for RQ 13 kA PC analysis
    """

    @staticmethod
    @check_dataframe_empty(mode="any", warning="At least one DataFrame is empty, I_EARTH_PCNT analysis skipped!")
    def analyze_i_earth_pcnt_pc(
        circuit_name: str,
        timestamp_fgc: int,
        i_meas_df: pd.DataFrame,
        i_meas_ref_df: pd.DataFrame,
        i_earth_pcnt_df: pd.DataFrame,
        i_earth_pcnt_ref_df: pd.DataFrame,
        xlim=(5, 120),
        start_point=3,
        offset=3,
    ):
        super(Pc13kARqAnalysis, Pc13kARqAnalysis).analyze_i_earth_pcnt_pc(
            "RQ",
            circuit_name,
            timestamp_fgc,
            i_meas_df,
            i_meas_ref_df,
            i_earth_pcnt_df,
            i_earth_pcnt_ref_df,
            xlim,
            start_point,
            offset,
        )


def _split_i_dataframes(i_dfs):
    i_meas_dfs = [i_df for i_df in i_dfs if "I_MEAS" in i_df.columns[0]]
    i_a_dfs = [i_df for i_df in i_dfs if "I_A" in i_df.columns[0]]
    i_ref_dfs = [i_df for i_df in i_dfs if "I_REF" in i_df.columns[0]]
    return i_meas_dfs, i_a_dfs, i_ref_dfs


def _get_color(i_meas_dfs, i_a_dfs, i_ref_dfs):
    len_i_meas, len_i_ref, len_i_a = len(i_meas_dfs), len(i_ref_dfs), len(i_a_dfs)
    color_i_meas = [f"C{i}" for i in range(len_i_meas)]
    color_i_ref = [f"C{i}" for i in range(len_i_ref)]
    color_i_a = [f"C{i}" for i in range(max(len_i_meas, len_i_ref), max(len_i_meas, len_i_ref) + len_i_a)]
    return color_i_meas + color_i_ref + color_i_a


def _plot_i_dfs(dfs, title, color, marker, xlim, ylim):
    PlotBuilder().with_signal(dfs, title=title, color=color, marker=marker, grid=True).with_ylabel(
        ylabel="I, [A]"
    ).with_xlim(lim=xlim).with_ylim(lim=ylim).plot()


def join_signal_with_ref(signal: pd.DataFrame, ref_signal: pd.DataFrame):
    idx = ref_signal.index.union(signal.index)
    signal_comparison_df = ref_signal.reindex(idx).interpolate("index")
    return signal_comparison_df.join(signal).dropna()
