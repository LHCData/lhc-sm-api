import json

from lhcsmapi.Time import Time

FAILED = "FAILED"
PASSED = "PASSED"


class AnalysisResult:
    """Class for keeping an analysis result and analysis status"""

    def __init__(self) -> None:
        self.results = []

    def add_qh_analysis(self, timestamp, magnet, ref_timestamp, status: bool):
        result = AnalysisResult.Result(timestamp, magnet, ref_timestamp, status)
        self.results.append(result)

    def toJSON(self):
        return json.dumps(self.results, default=lambda o: o.__dict__)

    def get_status(self) -> str:
        for result in self.results:
            if result.status == FAILED:
                return FAILED
        return PASSED

    class Result:
        def __init__(self, timestamp, magnet, ref_timestamp, status) -> None:
            self.timestamp = Time.to_string_short(timestamp)
            self.magnet = magnet
            self.ref_timestamp = Time.to_string_short(ref_timestamp)
            self.status = PASSED if status else FAILED
