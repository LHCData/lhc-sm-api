"""Methods to compare data features with the reference"""

from typing import Optional
import warnings

import numpy as np
import pandas as pd
from IPython.display import display

from lhcsmapi import reference
from lhcsmapi.analysis.warnings import warning_on_one_line
from lhcsmapi.utils import dataframe

warnings.formatwarning = warning_on_one_line


def compare_features_to_reference(
    features_df: pd.DataFrame,
    circuit_type: str,
    system: str,
    *,
    wildcard: Optional[str] = None,
    timestamp: Optional[int] = None,
    nominal_voltage: int = 900,
    precision: int = 6,
) -> pd.DataFrame:
    """Method comparing input features with reference features given as a [min, max] range of values for:

    - Quench Heaters
    - Power Converter
    - Energy Extraction

    Method considers only a subset of reference features matching the input ones.

    :param features_df: input features to be compared to the references
    :param circuit_type:
    :param system: system name
    :param wildcard: wildcard in order to replace reference feature name
    :param timestamp: timestamp in order to include appropriate reference features (EE settings for RB were changed
    during the operation)
    :param nominal_voltage: nominal QH voltage (10, 300, 900); 900 by default
    :param precision: floating point precision to which values are compared

    :return: True if features are within min/max range from reference features, otherwise False
    """
    if system == "QH":
        reference_df = reference.get_quench_heaters_reference_features(circuit_type, nominal_voltage).T
    elif system == "PC":
        reference_df = reference.get_power_converter_reference_features(circuit_type).T
    elif system == "EE":
        reference_df = reference.get_energy_extraction_reference_features(circuit_type, timestamp).T
    else:
        raise KeyError("System {} is not supported!".format(system))

    # update wildcards in reference features
    if wildcard is not None:
        key, value = list(wildcard.items())[0]
        reference_df.columns = [col.replace("%{}%".format(key), wildcard[key]) for col in reference_df.columns]

    features_df.index = ["act"]
    common_columns = list(set(reference_df.columns.values).intersection(set(features_df.columns.values)))
    comparison_df = pd.concat([reference_df[common_columns], features_df[common_columns]], sort=True)
    comparison_df_t = comparison_df.T

    def _round(floating):
        return round(floating, precision)

    comparison_df_t["result"] = comparison_df_t.apply(
        lambda row: (_round(row["act"]) >= _round(row["min"])) and (_round(row["act"]) <= _round(row["max"])), axis=1
    )

    return comparison_df_t


def compare_difference_of_features_to_reference(
    features_df: pd.DataFrame,
    reference_df: pd.DataFrame,
    circuit_type: str,
    system: str,
    wildcard: Optional[str] = None,
    nominal_voltage: int = 900,
    precision: int = 6,
) -> pd.DataFrame:
    """Method comparing the difference of the input features and the reference ones to an acceptance range

    :param features_df: input features
    :param reference_df: reference features
    :param circuit_type: circuit type
    :param system: system name
    :param wildcard: wildcard in order to replace reference feature name
    :param nominal_voltage: nominal QH voltage (10, 300, 900); 900 by default
    :param precision: floating point precision to which values are compared

    :return: a DataFrame with comparison of features
    """
    if system == "QH":
        differences_df = reference.get_quench_heaters_reference_differences(circuit_type, nominal_voltage).T
    else:
        raise KeyError("System {} is not supported!".format(system))

    if wildcard is not None:
        key, value = list(wildcard.items())[0]
        differences_df.columns = [col.replace("%{}%".format(key), value) for col in differences_df.columns]

    features_df.index = ["act"]
    reference_df.index = ["ref"]

    comparison_df = pd.concat(
        [
            dataframe.get_columns(reference_df, features_df.columns),
            features_df,
            dataframe.get_columns(differences_df, features_df.columns),
        ],
        sort=False,
    )

    def compare(row):
        if np.isnan(row["diff"]):
            return np.nan
        return round(abs(row["act"] - row["ref"]), precision) <= row["diff"]

    comparison_df = comparison_df.T
    comparison_df["result"] = comparison_df.apply(compare, axis=1)

    return comparison_df


def check_feature_comparison(comparison_df: pd.DataFrame):
    """Method checking if the comparison dataframe contains all results within a prescribed range.
    In case the results fall outside of the prescribed range (some rows in the results column are False), then
    these rows are colored in red.

    :param comparison_df: dataframe containing a result of a comparison in a result column

    :return: a DataFrame with comparison of features
    """
    color = comparison_df["result"].map({False: "background-color: red", True: ""})
    display(comparison_df.style.apply(lambda s: color))
    return comparison_df["result"].all()
