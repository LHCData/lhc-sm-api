from typing import Union, Optional


def warning_on_one_line(
    message: Union[Warning, str], category: type[Warning], filename: str, lineno: int, line: Optional[str] = None
) -> str:
    """Function formatting warning message into a one line containing the message only

    :param message: warning message
    :param category: warning category
    :param filename: filename in which a warning was raised
    :param lineno: line at which the warning was raised
    :param file: optional
    :param line: optional
    :return: formatted warning message
    """
    return "%s\n" % message
