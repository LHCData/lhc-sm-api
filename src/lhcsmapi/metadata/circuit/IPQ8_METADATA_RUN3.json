{
  "DESCRIPTION": "Individually Powered Quads (IPQ) in the insertion regions. Each beam is on separate circuit which is required by the optics.",
  "TIME-VALIDITY": {
    "COMMENT": "This version of IPQ8_METADATA with eight quench heaters is valid for Run3",
    "YEAR-START": 2019,
    "YEAR-END": 2024
  },
  "META_VARIABLES": [
    "%CIRCUIT%",
    "%CIRCUIT2%",
    "%CIP%",
    "%SECTORID%",
    "%PC%",
    "%DFB%",
    "%DFB_PREFIX%",
    "%CRYOSTAT2%"
  ],
  "META_TABLES": [
    "magnet/IPQ_MagnetHistory.csv",
    "magnet/IPQ_LayoutDetails.csv"
  ],
  "COMMENT": "Each circuit has a dictionary key which describes specific characteristics",
  "CIRCUITS": {
    "RQ4.L2": {
      "%CIRCUIT%": "RQ4.L2",
      "CIP": {
        "TYPE": "CIP",
        "%CIP%": "CIP.UA23.ML2"
      },
      "CRYO": {
        "TYPE": "CRYO_4.5K",
        "%SECTORID%": "12"
      },
      "PIC": {
        "TYPE": "PIC"
      },
      "PC": {
        "TYPE": "PC",
        "%PC%": ["RPHH.UA23.RQ4.L2B1", "RPHH.UA23.RQ4.L2B2"]
      },
      "QDS": {
        "TYPE": "QDS"
      },
      "QH": {
        "TYPE": "QH"
      },
      "LEADS_B1": {
        "TYPE": "LEADS_B1",
        "%DFB%": "DFLCS.4L2",
        "ORDER": [
          "LD1",
          "LD2"
        ],
        "%DFB_PREFIX%": [
          "DMAC03_04L2_",
          "DMAC04_04L2_"
        ]
      },
      "LEADS_B2": {
        "TYPE": "LEADS_B2",
        "%DFB%": "DFLCS.4L2",
        "ORDER": [
          "LD2",
          "LD3"
        ],
        "%DFB_PREFIX%": [
          "DMAC04_04L2_",
          "DMAC05_04L2_"
        ]
      }
    },
    "RQ4.L8": {
      "%CIRCUIT%": "RQ4.L8",
      "CIP": {
        "TYPE": "CIP",
        "%CIP%": "CIP.UA83.ML8"
      },
      "CRYO": {
        "TYPE": "CRYO_4.5K",
        "%SECTORID%": "78"
      },
      "PIC": {
        "TYPE": "PIC"
      },
      "PC": {
        "TYPE": "PC",
        "%PC%": ["RPHH.UA83.RQ4.L8B1", "RPHH.UA83.RQ4.L8B2"]
      },
      "QDS": {
        "TYPE": "QDS"
      },
      "QH": {
        "TYPE": "QH"
      },
      "LEADS_B1": {
        "TYPE": "LEADS_B1",
        "%DFB%": "DFLCS.4L8",
        "ORDER": [
          "LD1",
          "LD2"
        ],
        "%DFB_PREFIX%": [
          "DMAC03_04L8_",
          "DMAC04_04L8_"
        ]
      },
      "LEADS_B2": {
        "TYPE": "LEADS_B2",
        "%DFB%": "DFLCS.4L8",
        "ORDER": [
          "LD2",
          "LD3"
        ],
        "%DFB_PREFIX%": [
          "DMAC04_04L8_",
          "DMAC05_04L8_"
        ]
      }
    },
    "RQ4.R2": {
      "%CIRCUIT%": "RQ4.R2",
      "CIP": {
        "TYPE": "CIP",
        "%CIP%": "CIP.UA27.MR2"
      },
      "CRYO": {
        "TYPE": "CRYO_4.5K",
        "%SECTORID%": "23"
      },
      "PIC": {
        "TYPE": "PIC"
      },
      "PC": {
        "TYPE": "PC",
        "%PC%": ["RPHH.UA27.RQ4.R2B1", "RPHH.UA27.RQ4.R2B2"]
      },
      "QDS": {
        "TYPE": "QDS"
      },
      "QH": {
        "TYPE": "QH"
      },
      "LEADS_B1": {
        "TYPE": "LEADS_B1",
        "%DFB%": "DFLCS.4R2",
        "ORDER": [
          "LD1",
          "LD2"
        ],
        "%DFB_PREFIX%": [
          "DMBC01_04R2_",
          "DMBC02_04R2_"
        ]
      },
      "LEADS_B2": {
        "TYPE": "LEADS_B2",
        "%DFB%": "DFLCS.4R2",
        "ORDER": [
          "LD2",
          "LD3"
        ],
        "%DFB_PREFIX%": [
          "DMBC02_04R2_",
          "DMBC03_04R2_"
        ]
      }
    },
    "RQ4.R8": {
      "%CIRCUIT%": "RQ4.R8",
      "CIP": {
        "TYPE": "CIP",
        "%CIP%": "CIP.UA87.MR8"
      },
      "CRYO": {
        "TYPE": "CRYO_4.5",
        "%SECTORID%": "81"
      },
      "PIC": {
        "TYPE": "PIC"
      },
      "PC": {
        "TYPE": "PC",
        "%PC%": ["RPHH.UA87.RQ4.R8B1", "RPHH.UA87.RQ4.R8B2"]
      },
      "QDS": {
        "TYPE": "QDS"
      },
      "QH": {
        "TYPE": "QH"
      },
      "LEADS_B1": {
        "TYPE": "LEADS_B1",
        "%DFB%": "DFLCS.4R8",
        "ORDER": [
          "LD1",
          "LD2"
        ],
        "%DFB_PREFIX%": [
          "DMBC01_04R8_",
          "DMBC02_04R8_"
        ]
      },
      "LEADS_B2": {
        "TYPE": "LEADS_B2",
        "%DFB%": "DFLCS.4R8",
        "ORDER": [
          "LD2",
          "LD3"
        ],
        "%DFB_PREFIX%": [
          "DMBC02_04R8_",
          "DMBC03_04R8_"
        ]
      }
    },
    "RQ5.L2": {
      "%CIRCUIT%": "RQ5.L2",
      "CIP": {
        "TYPE": "CIP",
        "%CIP%": "CIP.UA23.ML2"
      },
      "CRYO": {
        "TYPE": "CRYO_4.5",
        "%SECTORID%": "12"
      },
      "PIC": {
        "TYPE": "PIC"
      },
      "PC": {
        "TYPE": "PC",
        "%PC%": ["RPHH.UA23.RQ5.L2B1", "RPHH.UA23.RQ5.L2B2"]
      },
      "QDS": {
        "TYPE": "QDS"
      },
      "QH": {
        "TYPE": "QH"
      },
      "LEADS_B1": {
        "TYPE": "LEADS_B1",
        "%DFB%": "DFLCS.5L2",
        "ORDER": [
          "LD1",
          "LD2"
        ],
        "%DFB_PREFIX%": [
          "DMCC01_05L2_",
          "DMCC02_05L2_"
        ]
      },
      "LEADS_B2": {
        "TYPE": "LEADS_B2",
        "%DFB%": "DFLCS.5L2",
        "ORDER": [
          "LD2",
          "LD3"
        ],
        "%DFB_PREFIX%": [
          "DMCC02_05L2_",
          "DMCC03_05L2_"
        ]
      }
    },
    "RQ5.R8": {
      "%CIRCUIT%": "RQ5.R8",
      "CIP": {
        "TYPE": "CIP",
        "%CIP%": "CIP.UA87.MR8"
      },
      "CRYO": {
        "TYPE": "CRYO_4.5",
        "%SECTORID%": "81"
      },
      "PIC": {
        "TYPE": "PIC"
      },
      "PC": {
        "TYPE": "PC",
        "%PC%": ["RPHH.UA87.RQ5.R8B1", "RPHH.UA87.RQ5.R8B2"]
      },
      "QDS": {
        "TYPE": "QDS"
      },
      "QH": {
        "TYPE": "QH"
      },
      "LEADS_B1": {
        "TYPE": "LEADS_B1",
        "%DFB%": "DFLCS.5R8",
        "ORDER": [
          "LD1",
          "LD2"
        ],
        "%DFB_PREFIX%": [
          "DMIC01_05R8_",
          "DMIC02_05R8_"
        ]
      },
      "LEADS_B2": {
        "TYPE": "LEADS_B2",
        "%DFB%": "DFLCS.5R8",
        "ORDER": [
          "LD2",
          "LD3"
        ],
        "%DFB_PREFIX%": [
          "DMIC02_05R8_",
          "DMIC03_05R8_"
        ]
      }
    }
  },
  "CIP": {
    "Comment": "%CIP% example -> CIP.UL14.LL1",
    "PM": {
    },
    "NXCALS": {
      "system": "WINCCOA",
      "ST_CRYO_MAINTAIN": "%CIP%:ST_CRYO_MAINTAIN",
      "ST_CRYO_START": "%CIP%:ST_CRYO_START"
    }
  },
   "CRYO_4.5K": {
    "Example": "%SECTORID% -> 12, %CRYOSTAT2% -> LQYCH_04R1",
    "PM": {
	},
    "NXCALS": {
      "system": "WINCCOA",
      "PTMIN_LINC": "LSS%SECTORID%_LINC_PTMIN.POSST",
      "PTMIN_LIND": "LSS%SECTORID%_LIND_PTMIN.POSST",
	  "PTMAX_LINC": "LSS%SECTORID%_LINC_PTMAX.POSST",
	  "PTMAX_LIND": "LSS%SECTORID%_LIND_PTMAX.POSST",
	  "PTAVG_LINC": "LSS%SECTORID%_LINC_PTAVG.POSST",
	  "PTAVG_LIND": "LSS%SECTORID%_LIND_PTAVG.POSST",
	  "TTMIN": "LSS%SECTORID%_MAGS_TTMIN.POSST",
	  "TTMAX": "LSS%SECTORID%_MAGS_TTMAX.POSST",
	  "TTAVG": "LSS%SECTORID%_MAGS_TTAVG.POSST",
	  "TT8xx": "%CRYOSTAT2%_TT830A.TEMPERATURECALC"
    }
  },
  "PIC": {
    "Comment": "%CIRCUIT% example -> RQ4.L1",
    "NXCALS": {
      "WINCCOA":{
      "CMD_PWR_PERM_PIC": "%CIRCUIT%:CMD_PWR_PERM_PIC",
      "CMD_PWR_PERM_B1_PIC": "%CIRCUIT%:CMD_PWR_PERM_B1_PIC",
      "CMD_PWR_PERM_B2_PIC": "%CIRCUIT%:CMD_PWR_PERM_B2_PIC",
      "ST_ABORT_PIC": "%CIRCUIT%:ST_ABORT_PIC",
      "CMD_ABORT_PIC": "%CIRCUIT%:CMD_ABORT_PIC",
      "ST_FAILURE_PIC": "%CIRCUIT%:ST_FAILURE_PIC"
    }}
  },
  "PC": {
    "Comment": "%PC% example -> RPHH.RR13.RQ4.L1B1",
    "PM": {
      "system": "FGC",
      "className": "lhc_self_pmd",
      "source": "%PC%",
      "I_REF": "ILOOP.I_REF",
      "I_MEAS": "STATUS.I_MEAS",
      "V_REF": "STATUS.V_REF",
      "V_MEAS": "STATUS.V_MEAS",
      "I_EARTH": "IEARTH.I_EARTH",
      "I_EARTH_PCNT": "STATUS.I_EARTH_PCNT",
      "I_A": "IAB.I_A",
      "I_B": "IAB.I_B",
      "ST_FAULTS": "STATUS.ST_FAULTS",
      "ST_UNLATCHED": "STATUS.ST_UNLATCHED"
    },
    "NXCALS": {
      "CMW": {
        "I_MEAS": "%PC%:I_MEAS",
        "I_EARTH_MA": "%PC%:I_EARTH_MA",
        "I_REF": "%PC%:I_REF",
        "I_ERR_MA": "%PC%:I_ERR_MA",
        "V_MEAS": "%PC%:V_MEAS"
      }
    }
  },
  "QDS": {
    "Comment": "%CIRCUIT% example -> RQ4.L1",
    "PM": {
      "system": "QPS",
      "className": "DQAMGNRQC",
      "source": "%CIRCUIT%",
      "U_RES_B1": "circ.%CIRCUIT%:U_RES_B1",
      "U_RES_B2": "circ.%CIRCUIT%:U_RES_B2",
      "U_INDUCT_B1": "circ.%CIRCUIT%:U_INDUCT_B1",
      "U_INDUCT_B2": "circ.%CIRCUIT%:U_INDUCT_B2",
      "U_1_B1": "circ.%CIRCUIT%:U_1_B1",
      "U_1_B2": "circ.%CIRCUIT%:U_1_B2",
      "U_2_B1": "circ.%CIRCUIT%:U_2_B1",
      "U_2_B2": "circ.%CIRCUIT%:U_2_B2",
      "ST_CIRCUIT_OK_QPS": "stat.%CIRCUIT%:ST_CIRCUIT_OK_QPS",
      "ST_CIRCUIT_OK_QPS_B1": "stat.%CIRCUIT%:ST_CIRCUIT_OK_QPS_B1",
      "ST_CIRCUIT_OK_QPS_B2": "stat.%CIRCUIT%:ST_CIRCUIT_OK_QPS_B2",
      "ST_PWR_PERM_B1": "stat.%CIRCUIT%:ST_PWR_PERM_B1",
      "ST_PWR_PERM_B2": "stat.%CIRCUIT%:ST_PWR_PERM_B2"
    },
    "NXCALS": {
      "CMW": {
      "U_RES_B1": "%CIRCUIT%:U_RES_B1",
      "U_RES_B2": "%CIRCUIT%:U_RES_B2",
      "U_INDUCT_B1": "%CIRCUIT%:U_INDUCT_B1",
      "U_INDUCT_B2": "%CIRCUIT%:U_INDUCT_B2",
      "U_1_B1": "%CIRCUIT%:U_1_B1",
      "U_1_B2": "%CIRCUIT%:U_1_B2",
      "U_2_B1": "%CIRCUIT%:U_2_B1",
      "U_2_B2": "%CIRCUIT%:U_2_B2",
      "ST_CIRCUIT_OK_QPS": "%CIRCUIT%:ST_CIRCUIT_OK_QPS",
      "ST_CIRCUIT_OK_QPS_B1": "%CIRCUIT%:ST_CIRCUIT_OK_QPS_B1",
      "ST_CIRCUIT_OK_QPS_B2": "%CIRCUIT%:ST_CIRCUIT_OK_QPS_B2",
      "ST_PWR_PERM_B1": "%CIRCUIT%:ST_PWR_PERM_B1",
      "ST_PWR_PERM_B2": "%CIRCUIT%:ST_PWR_PERM_B2"
    }}
  },
  "QH": {
    "Comment": "%CIRCUIT% example -> RQ5.L2",
    "PM": {
      "system": "QPS",
      "className": "DQAMGNRQC",
      "source": "%CIRCUIT%",
      "U_HDS": [
        "heat.%CIRCUIT%:U_HDS_1_B1",
        "heat.%CIRCUIT%:U_HDS_2_B1",
        "heat.%CIRCUIT%:U_HDS_3_B1",
        "heat.%CIRCUIT%:U_HDS_4_B1",
        "heat.%CIRCUIT%:U_HDS_1_B2",
        "heat.%CIRCUIT%:U_HDS_2_B2",
        "heat.%CIRCUIT%:U_HDS_3_B2",
        "heat.%CIRCUIT%:U_HDS_4_B2"
      ]
    },
    "NXCALS": {
      "CMW": {
      "U_HDS": [
        "%CIRCUIT%:U_HDS_1_B1",
        "%CIRCUIT%:U_HDS_2_B1",
        "%CIRCUIT%:U_HDS_3_B1",
        "%CIRCUIT%:U_HDS_4_B1",
        "%CIRCUIT%:U_HDS_1_B2",
        "%CIRCUIT%:U_HDS_2_B2",
        "%CIRCUIT%:U_HDS_3_B2",
        "%CIRCUIT%:U_HDS_4_B2"
      ]
    }}
  },
  "LEADS_B1": {
    "Comment": "%CIRCUIT% example -> RQ4.L1",
    "PM": {
      "system": "QPS",
      "className": "DQAMGNRQC",
      "source": "%CIRCUIT%",
      "U_RES": [
        "stat.%DFB%.%CIRCUIT%.LD1:U_RES",
        "stat.%DFB%.%CIRCUIT%.LD2:U_RES"
      ],
      "U_HTS": [
        "stat.%DFB%.%CIRCUIT%.LD1:U_HTS",
        "stat.%DFB%.%CIRCUIT%.LD2:U_HTS"
      ],
      "ST_LEAD_OK": [
        "stat.%DFB%.%CIRCUIT%.LD1:ST_LEAD_OK",
        "stat.%DFB%.%CIRCUIT%.LD2:ST_LEAD_OK"
      ],
      "ST_PWR_PERM": [
        "stat.%DFB%.%CIRCUIT%.LD1:ST_PWR_PERM",
        "stat.%DFB%.%CIRCUIT%.LD2:ST_PWR_PERM"
      ]
    },
    "NXCALS": {
      "CMW": {
	  "U_RES": [
        "%DFB%.%CIRCUIT%.LD1:U_RES",
        "%DFB%.%CIRCUIT%.LD2:U_RES"
      ],
      "U_HTS": [
        "%DFB%.%CIRCUIT%.LD1:U_HTS",
        "%DFB%.%CIRCUIT%.LD2:U_HTS"
      ],
      "ST_LEAD_OK": [
        "%DFB%.%CIRCUIT%.LD1:ST_LEAD_OK",
        "%DFB%.%CIRCUIT%.LD2:ST_LEAD_OK"
      ],
      "ST_PWR_PERM": [
        "%DFB%.%CIRCUIT%.LD1:ST_PWR_PERM",
        "%DFB%.%CIRCUIT%.LD2:ST_PWR_PERM"
      ]},
      "WINCCOA": {
        "TT893": "%DFB_PREFIX%TT893.TEMPERATURECALC",
      "TT891A": "%DFB_PREFIX%TT891A.TEMPERATURECALC",
      "CV891": "%DFB_PREFIX%CV891.POSST"
      }
    }
  },
  "LEADS_B2": {
    "Comment": "%CIRCUIT% example -> RQ4.L1",
    "PM": {
      "system": "QPS",
      "className": "DQAMGNRQC",
      "source": "%CIRCUIT%",
      "U_RES": [
        "stat.%DFB%.%CIRCUIT%.LD2:U_RES",
        "stat.%DFB%.%CIRCUIT%.LD3:U_RES"
      ],
      "U_HTS": [
        "stat.%DFB%.%CIRCUIT%.LD2:U_HTS",
        "stat.%DFB%.%CIRCUIT%.LD3:U_HTS"
      ],
      "ST_LEAD_OK": [
        "stat.%DFB%.%CIRCUIT%.LD2:ST_LEAD_OK",
        "stat.%DFB%.%CIRCUIT%.LD3:ST_LEAD_OK"
      ],
      "ST_PWR_PERM": [
        "stat.%DFB%.%CIRCUIT%.LD2:ST_PWR_PERM",
        "stat.%DFB%.%CIRCUIT%.LD3:ST_PWR_PERM"
      ]
    },
    "NXCALS": {
      "CMW": {
	  "U_RES": [
        "%DFB%.%CIRCUIT%.LD2:U_RES",
        "%DFB%.%CIRCUIT%.LD3:U_RES"
      ],
      "U_HTS": [
        "%DFB%.%CIRCUIT%.LD2:U_HTS",
        "%DFB%.%CIRCUIT%.LD3:U_HTS"
      ],
      "ST_LEAD_OK": [
        "%DFB%.%CIRCUIT%.LD2:ST_LEAD_OK",
        "%DFB%.%CIRCUIT%.LD3:ST_LEAD_OK"
      ],
      "ST_PWR_PERM": [
        "%DFB%.%CIRCUIT%.LD2:ST_PWR_PERM",
        "%DFB%.%CIRCUIT%.LD3:ST_PWR_PERM"
      ]},
      "WINCCOA": {
         "TT893": "%DFB_PREFIX%TT893.TEMPERATURECALC",
      "TT891A": "%DFB_PREFIX%TT891A.TEMPERATURECALC",
      "CV891": "%DFB_PREFIX%CV891.POSST"
      }
    }
  },
  "MAGNET": {
    "MAGNET_PREFIX": [
      "MQY."
    ],
    "MAGNET_NAME_REG_EXP": "[R]{1}[Q]{1}\\d+[.][LR]{1}[1-8]{1}"
  }
}