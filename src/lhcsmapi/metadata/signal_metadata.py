"""Module provides access to signal names and corresponding metadata.
The circuits are divided into main families:

- RB: main dipole circuits
- RQ: main quadrupole circuits
- IPQ: individually powered quadrupoles (IPQ2, IPQ4, IPQ8 - the division depends on the QPS)
- IPD: individually powered dipoles (IPD2_B1B2, IPD2 - the division depends on the QPS)
- 600A: circuits with 600A power converters
- 80-120A: circuits with rated current from 80-120 A
- 60A: circuits with rated current of 60A
"""

from __future__ import annotations

import copy
import csv
import enum
import json
import os
import pathlib
import re
import warnings
from typing import Any, Literal

import pandas as pd

from lhcsmapi import utils
from lhcsmapi.metadata.MappingMetadata import MappingMetadata
from lhcsmapi.Time import Time, TimeAlias

_RUN_3_START = 1577833200000000000  # 2020-01-01 00:00:00 GMT+01:00
_RUN_3_QPS_UPGRADE_START = 1677583560000000000  # 2023-02-28 12:26:00

_V1 = ""
_V2 = "_RUN3"
_V3 = "_RUN3_QPS_UPGRADE"

_CIRCUITS_TO_PATH_TEMPLATES = {
    "RB": "circuit/RB_METADATA{}.json",
    "RQ": "circuit/RQ_METADATA{}.json",
    "IT": "circuit/IT_METADATA{}.json",
    "IPD2_B1B2": "circuit/IPD2_B1B2_METADATA{}.json",
    "IPD2": "circuit/IPD2_METADATA{}.json",
    "IPQ2": "circuit/IPQ2_METADATA{}.json",
    "IPQ4": "circuit/IPQ4_METADATA{}.json",
    "IPQ8": "circuit/IPQ8_METADATA{}.json",
    "600A": "circuit/600A_METADATA{}.json",
    "60A": "circuit/60A_METADATA{}.json",
    "80-120A": "circuit/80-120A_METADATA{}.json",
}


class CircuitType(str, enum.Enum):
    A60 = "60A"
    A600 = "600A"
    A80_120 = "80-120A"
    IPD2 = "IPD2"
    IPD2_B1B2 = "IPD2_B1B2"
    IPQ2 = "IPQ2"
    IPQ4 = "IPQ4"
    IPQ8 = "IPQ8"
    IT = "IT"
    RB = "RB"
    RQ = "RQ"

    def __str__(self) -> str:
        return str.__str__(self)


class GenericCircuitType(str, enum.Enum):
    A60 = "60A"
    A600 = "600A"
    A80_120 = "80-120A"
    IPD = "IPD"
    IPQ = "IPQ"
    IT = "IT"
    RB = "RB"
    RQ = "RQ"

    def __str__(self) -> str:
        return str.__str__(self)


_cache: dict[str, dict[str, dict | str]] = {}


def _get_metadata(circuit_type: str, timestamp: TimeAlias | None = None) -> dict[str, dict | str]:
    """Loads metadata for provided circuit_type valid at the given point in time.
    If no timestamp is provided, the most recent data is returned.

    Args:
       circuit_type: a detailed circuit type (e.g. one of the IPQ2, IPQ4, IPQ8 for the IPQ family)
       timestamp: point in time the returned metadata will refer to (now by default)

    Returns:
       A dictionary containing:
        - the mapping from circuit names to systems (and corresponding replacements for wildcards),
        - the mapping from systems to signal names available in PM and NXCALS (and corresponding parameters needed to
        construct a valid query). Note that both parameters and signals may contain wildcards.
    """
    if circuit_type.upper() not in _CIRCUITS_TO_PATH_TEMPLATES:
        raise KeyError(f"{circuit_type} is not mapped to a json file!")

    metadata_file_suffix = _get_metadata_version(circuit_type, timestamp)
    file_name = _CIRCUITS_TO_PATH_TEMPLATES[circuit_type.upper()].format(metadata_file_suffix)

    if file_name not in _cache:
        _load_to_cache(file_name)

    return _cache[file_name]


def _get_metadata_version(circuit_type: str, timestamp: TimeAlias | None) -> str:
    """Returns the metadata version based on the provided arguments.
    Metadata of all circuits but 'IPD2' have 2 possible versions:
        - _V1 valid before _RUN_3_START
        - _V2 valid after _RUN_3_START (inclusive)
    Metadata of 'IPD2' have additional version _V3 valid from _RUN_3_QPS_UPGRADE_START
    (see: https://its.cern.ch/jira/browse/SIGMON-477):
    """
    timestamp = Time.to_unix_timestamp(timestamp) if timestamp else None
    if timestamp and timestamp < _RUN_3_START:
        return _V1
    if circuit_type.upper() == "IPD2" and (not timestamp or timestamp > _RUN_3_QPS_UPGRADE_START):
        return _V3
    return _V2


def _load_to_cache(file_name):
    full_path = os.path.join(os.path.dirname(__file__), file_name)
    try:
        with open(full_path, encoding="utf-8") as json_file:
            _cache[file_name] = json.load(json_file)
    except FileNotFoundError as error:
        raise FileNotFoundError(f"Could not load {full_path}") from error


def _get_metadata_for_circuit_name(
    circuit_type: str, circuit_name: str, timestamp: TimeAlias | None = None
) -> dict[str, dict | str]:
    metadata = _get_metadata(circuit_type, timestamp)
    if not any(isinstance(value, str) for value in metadata.values()):
        raise TypeError(f"Metadata for {circuit_type} is not a dictionary")
    return _narrow_metadata_to_circuit(circuit_type, circuit_name, metadata)


def _narrow_metadata_to_circuit(
    circuit_type: str, circuit_name: str, metadata: dict[str, dict | str]
) -> dict[str, dict | str]:
    if not isinstance(metadata["CIRCUITS"], dict):
        raise TypeError(f"Metadata for {circuit_type} is not a dictionary")
    metadata = metadata["CIRCUITS"]
    if circuit_name not in metadata:
        raise ValueError(f"Circuit name {circuit_name} not present in metadata for {circuit_type}.")
    if not isinstance(metadata[circuit_name], dict):
        raise TypeError(f"Metadata for {circuit_name} is not a dictionary")
    metadata_circuit_name = metadata[circuit_name]
    if not isinstance(metadata_circuit_name, dict):
        raise TypeError(f"Metadata for {circuit_name} is not a dictionary")
    return metadata_circuit_name


def get_circuit_types() -> list[str]:
    """
    Returns:
        A list of circuit types to which a metadata dictionary exists.
    """
    return list(_CIRCUITS_TO_PATH_TEMPLATES.keys())


def get_circuit_names(circuit_type: str | list[str] | None = None, timestamp: TimeAlias | None = None) -> list[str]:
    """
    Args:
        circuit_type: detailed circuit type(s)
        timestamp: point in time the returned metadata will refer to (now by default)

    Returns:
         A list of circuit names for given circuit type(s) valid at the given point in time.
    """
    if circuit_type is None:
        circuit_type = get_circuit_types()

    circuit_types = utils.vectorize(circuit_type)
    circuit_names: list[str] = []
    for circuit_type_ in circuit_types:
        metadata = _get_metadata(circuit_type_, timestamp)["CIRCUITS"]
        if not isinstance(metadata, dict):
            raise TypeError(f"Metadata for {circuit_type_} is not a dictionary")
        circuit_names.extend(metadata.keys())
    return circuit_names


def get_pc_names(
    circuit_type: str | list[str], circuit_name: str | list[str] | None = None, timestamp: TimeAlias | None = None
) -> list[str]:
    """DEPRECATED: use get_fgc_names instead"""

    def circuit_is_wanted(circuit):
        return circuit_name is None or circuit in utils.vectorize(circuit_name)

    circuit_types = utils.vectorize(circuit_type)
    pc_names = []
    for circuit_type_ in circuit_types:
        metadata = _get_metadata(circuit_type_, timestamp)
        if not isinstance(metadata["CIRCUITS"], dict):
            raise TypeError(f"Metadata for {circuit_type_} is not a dictionary")
        else:
            for circuit_name_, circuit_metadata in metadata["CIRCUITS"].items():
                if circuit_is_wanted(circuit_name_):
                    pc_name = circuit_metadata["PC"]["%PC%"]
                    pc_names.extend(utils.vectorize(pc_name))
    return pc_names


def get_fgc_names(circuit_name: str, timestamp: TimeAlias) -> list[str]:
    """Returns the names of the FGCs for a given circuit name and timestamp

    Args:
        circuit_name: circuit name
        timestamp: timestamp in ns since epoch

    Returns:
        List of FGC names for input circuit type(s) narrowed to the provided circuit names(s) valid
        at the given point in time.

    Raises:
        ValueError: If the circuit name is not present in the metadata

    Examples:
        >>> from lhcsmapi.metadata import signal_metadata
        >>> circuit_name = "RQ5.L1"
        >>> timestamp = 1679052360000000000
        >>> signal_metadata.get_fgc_names(circuit_name, timestamp)
        ['RPHSB.RR13.RQ5.L1B1', 'RPHSB.RR13.RQ5.L1B2']
    """
    try:
        circuit_type = get_circuit_type_for_circuit_name(circuit_name)
    except KeyError as e:
        raise ValueError(f"Circuit name {circuit_name} not present in the metadata.") from e
    return get_pc_names(circuit_type, circuit_name, timestamp)


def get_ee_names(circuit_name: str, timestamp: TimeAlias) -> list[str]:
    """Returns the names of the EE systems for a given circuit name and timestamp

    Args:
        circuit_name: circuit name
        timestamp: timestamp

    Returns:
        List of EE names for input circuit type(s) narrowed to the provided circuit names(s) valid
        at the given point in time.

    Raises:
        ValueError: If the circuit name is not present in the metadata
        TypeError: If the metadata found is not a dictionary

    Examples:
        >>> from lhcsmapi.metadata import signal_metadata
        >>> signal_metadata.get_ee_names("RQD.A12", 1679052360000000000) # RQ
        ['UA23.RQD.A12']
        >>> signal_metadata.get_ee_names("RB.A12", 1679052360000000000) # RB
        ['RR17.RB.A12', 'UA23.RB.A12']
        >>> signal_metadata.get_ee_names("RCD.A12B1", 1679052360000000000) # 600A with EE
        ['UA23.RCD.A12B1']
        >>> signal_metadata.get_ee_names("RD1.L2", 1679052360000000000) # IPD
        []
    """
    if not has_ee(circuit_name):
        return []

    try:
        circuit_type = get_circuit_type_for_circuit_name(circuit_name)
    except KeyError as e:
        raise ValueError(f"Circuit name {circuit_name} not present in the metadata.") from e

    metadata = _get_metadata(circuit_type, timestamp)
    if not isinstance(metadata["CIRCUITS"], dict):
        raise TypeError(f"Metadata for {circuit_type} is not a dictionary")

    if circuit_name not in metadata["CIRCUITS"]:
        raise ValueError(f"Circuit name {circuit_name} not present in metadata for {circuit_type}.")

    circuit_metadata = metadata["CIRCUITS"][circuit_name]
    if circuit_type == CircuitType.RB:
        return [circuit_metadata["EE_ODD"]["%EE_ODD%"], circuit_metadata["EE_EVEN"]["%EE_EVEN%"]]
    if circuit_type in (CircuitType.RQ, CircuitType.A600):
        return [circuit_metadata["EE"]["%EE%"]]
    raise NotImplementedError(
        f"Even though {circuit_name} is supposed to have EE, the get_ee_names method does not support this circuit."
    )


def __get_dfb_sections(circuit_name: str, timestamp: TimeAlias) -> list[dict[str, Any]]:
    if not has_dfb(circuit_name):
        raise ValueError(f"Circuit {circuit_name} does not have a DFB.")
    try:
        circuit_type = get_circuit_type_for_circuit_name(circuit_name)
    except KeyError as e:
        raise ValueError(f"Circuit name {circuit_name} not present in the metadata.") from e

    metadata = _get_metadata(circuit_type, timestamp)
    if not isinstance(metadata["CIRCUITS"], dict):
        raise TypeError(f"Metadata for {circuit_type} is not a dictionary")

    if circuit_name not in metadata["CIRCUITS"]:
        raise ValueError(f"Circuit name {circuit_name} not present in metadata for {circuit_type}.")

    circuit_metadata = metadata["CIRCUITS"][circuit_name]

    if circuit_type == CircuitType.A600:
        if "LEADS_IT" in circuit_metadata and "LEADS" in circuit_metadata:
            raise NotImplementedError(
                f"600A circuit {circuit_name} both has a LEADS and a LEADS_IT section. "
                "The implementation of get_dfb_names should be reconsidered."
            )
        if "LEADS_IT" in circuit_metadata:
            return [circuit_metadata["LEADS_IT"]]
        return [circuit_metadata["LEADS"]]

    if circuit_type in (CircuitType.IPQ2, CircuitType.IPQ4, CircuitType.IPQ8):
        return [circuit_metadata["LEADS_B1"], circuit_metadata["LEADS_B2"]]

    if circuit_type in (CircuitType.RQ, CircuitType.IT, CircuitType.IPD2, CircuitType.IPD2_B1B2):
        return [circuit_metadata["LEADS"]]

    if circuit_type == CircuitType.RB:
        return [circuit_metadata["LEADS_ODD"], circuit_metadata["LEADS_EVEN"]]

    raise NotImplementedError(f"Method does not support circuit type {circuit_type}.")


def get_dfb_names(circuit_name: str, timestamp: TimeAlias) -> list[str]:
    """Returns the name of the DFB for a given circuit name and timestamp

    Args:
        circuit_name: circuit name
        timestamp: timestamp

    Returns:
        DFB name for input circuit type(s) narrowed to the provided circuit names(s) valid
        at the given point in time.

    Raises:
        ValueError: If the circuit name is not present in the metadata
        TypeError: If the metadata found is not a dictionary

    Examples:
        >>> from lhcsmapi.metadata import signal_metadata
        >>> signal_metadata.get_dfb_names("RQ5.L1", 1679052360000000000)
        ['DFLCS.RR13.RQ5.L1']
    """
    leads_sections = __get_dfb_sections(circuit_name, timestamp)
    if get_circuit_type_for_circuit_name(circuit_name) in (
        CircuitType.A600,
        CircuitType.IPD2,
        CircuitType.IPD2_B1B2,
        CircuitType.IPQ2,
        CircuitType.IPQ4,
        CircuitType.IPQ8,
        CircuitType.IT,
    ):
        return list(dict.fromkeys(f"{lead_section['%DFB%']}.{circuit_name}" for lead_section in leads_sections))

    return [lead_section["%DFB%"] for lead_section in leads_sections]


def get_dfb_prefixes(circuit_name: str, dfb: str, timestamp: TimeAlias) -> list[str]:
    """Returns the DFB prefixes ("%DFB_PREFIX%") for a given dfb and timestamp

    Args:
        circuit_name: circuit name
        dfb: dfb
        timestamp: timestamp

    Returns:
        DFB prefixes for a given dfb at the given point in time.

    Raises:
        ValueError: If the circuit name is not present in the metadata
        TypeError: If the metadata found is not a dictionary

    Examples:
        >>> from lhcsmapi.metadata import signal_metadata
        >>> circuit_name = "ROF.A56B2"
        >>> timestamp = 1679052360000000000
        >>> [dfb] = signal_metadata.get_dfb_names(circuit_name, timestamp)
        >>> signal_metadata.get_dfb_prefixes(circuit_name, dfb, timestamp)
        ['DAJB27_07R5_', 'DAJB28_07R5_']
    """
    leads_sections = __get_dfb_sections(circuit_name, timestamp)

    leads_data_for_selected_dfb = [lead_data for lead_data in leads_sections if dfb.startswith(lead_data["%DFB%"])]
    if not leads_data_for_selected_dfb:
        raise ValueError(f"DFB {dfb} not present in metadata for {circuit_name}.")

    return sorted(
        set(dfb_prefixes for lead_data in leads_data_for_selected_dfb for dfb_prefixes in lead_data["%DFB_PREFIX%"])
    )


def get_system_types_per_circuit_name(
    circuit_type: str, circuit_name: str, timestamp: TimeAlias | None = None
) -> list[str]:
    """
    Args:
        circuit_type: detailed circuit type
        circuit_name: a circuit name of a given circuit_type
        timestamp: point in time the returned metadata will refer to (now by default)

    Returns:
        A list of systems for an input circuit name valid at the given point in time.
    """
    circuit_metadata = _get_metadata_for_circuit_name(circuit_type, circuit_name, timestamp)

    types = set()
    for value in circuit_metadata.values():
        if isinstance(value, dict):
            types.add(value["TYPE"])
        elif isinstance(value, list):
            for element in value:
                types.add(element["TYPE"])
    return list(types)


def get_sector_names_from_component(
    circuit_type: str, system: str, component: str, timestamp: TimeAlias | None = None
) -> str:
    """Reads sector name from a component.
    Useful in case of PM queries where a search for events within
    a given time interval may return events with systems not in the analysed sector.

    Args:
        circuit_type: circuit type
        system: name of system
        component: component name for which the sector name is retrieved
        timestamp: point in time the returned metadata will refer to (now by default)

    Returns:
        Sector name
    """
    metadata = _get_metadata(circuit_type, timestamp)
    if system in ["MAGNET", "BUSBAR", "BUSBAR_RQD", "BUSBAR_RQF"]:
        if not isinstance(metadata[system], dict):
            raise TypeError(f"Metadata for {system} is not a dictionary")
        metadata_system = metadata[system]
        if not isinstance(metadata_system, dict):
            raise TypeError(f"Metadata for {system} is not a dictionary")
        reg_exp = metadata_system["REL_IP_POS_REG_EXP"]
        if not isinstance(metadata["REL_IP_POS_TO_CIRCUIT_NAME"], dict):
            raise TypeError("Metadata for REL_IP_POS_TO_CIRCUIT_NAME is not a dictionary")
        metadata = metadata["REL_IP_POS_TO_CIRCUIT_NAME"]
        return _get_sector_name_from_magnet_name(metadata, reg_exp, component)
    if system == "PC":
        system_metadata = metadata[system]
        if not isinstance(system_metadata, dict):
            raise TypeError(f"Metadata for {system} is not a dictionary")
        reg_exp = system_metadata["REL_IP_POS_REG_EXP"]
        return re.findall(reg_exp, component)[0]
    raise KeyError(f"System {system} does not support this feature")


def _get_sector_name_from_magnet_name(metadata, reg_exp, magnet_name):
    found_reg_exp = re.findall(reg_exp, magnet_name)
    if len(found_reg_exp) != 1:
        warnings.warn(f"Key {magnet_name} does not contain proper MB magnet signal")
        return ""
    key = found_reg_exp[0]
    if key not in metadata:
        warnings.warn(f"Key {key} does not exist")
        return ""
    return metadata[key]


def get_signal_metadata(
    circuit_type: str, circuit_name: str, system: str, database: str, timestamp: TimeAlias | None = None
) -> dict[str, str | list[str] | dict]:
    """Retrieves signal metadata for the given arguments.

    Args:
        circuit_type: circuit type
        circuit_name: circuit name corresponding to circuit type
        system: name of a system
        database: name of a database (PM, NXCALS)
        timestamp: point in time the returned metadata will refer to (now by default)

    Returns:
        Signal metadata
    """
    metadata = _get_metadata(circuit_type, timestamp)

    if system not in metadata:
        raise KeyError(f"System type {system} not present in {metadata.keys()}.")

    if database not in metadata[system]:
        raise KeyError(f"Database signal {database} not present in metadata keys.")

    circuit_metadata = _narrow_metadata_to_circuit(circuit_type, circuit_name, metadata)
    metadata_system = metadata[system]
    if not isinstance(metadata_system, dict):
        raise TypeError(f"Metadata for {system} is not a dictionary")
    db_metadata = copy.deepcopy(metadata_system[database])

    if database == "PM":
        _update_source_wildcard_for_pm(system, db_metadata, circuit_metadata)
    elif database == "NXCALS":
        _update_source_wildcard_for_nxcals(system, db_metadata, circuit_metadata)
    return db_metadata


def _update_source_wildcard_for_pm(system, db_metadata, circuit_metadata):
    source = db_metadata["source"]
    if source in circuit_metadata:
        db_metadata["source"] = circuit_metadata[source]
    elif source in circuit_metadata[system]:
        db_metadata["source"] = circuit_metadata[system][source]


def _update_source_wildcard_for_nxcals(system, db_metadata, circuit_metadata):
    if "CMW" in db_metadata and "device" in db_metadata["CMW"]:
        device = db_metadata["CMW"]["device"]
        if device in circuit_metadata:
            db_metadata["CMW"]["device"] = circuit_metadata[device]
        elif device in circuit_metadata[system]:
            db_metadata["CMW"]["device"] = circuit_metadata[system][device]


def get_circuit_type_for_circuit_name(circuit_name: str) -> str:
    """
    Args:
        circuit_name: circuit name

    Returns:
        A circuit type for a given circuit name

    Raises:
        KeyError: If circuit name not found
    """
    for circuit_type in get_circuit_types():
        if circuit_name in get_circuit_names(circuit_type) or circuit_name in get_circuit_names(
            circuit_type, timestamp=_RUN_3_START - 1
        ):
            return circuit_type

    raise KeyError(f"Circuit name {circuit_name} does not map to internal metadata.")


def get_generic_circuit_type_for_circuit_name(circuit_name: str) -> GenericCircuitType:
    """
    Returns the generic circuit type for a given circuit name, and raises an error if the circuit name is not found.
    """
    if circuit_name not in get_circuit_names():
        raise ValueError(f"Circuit name {circuit_name} not present in the metadata.")

    circuit_type_mapping = {
        is_60a: GenericCircuitType.A60,
        is_80_120a: GenericCircuitType.A80_120,
        is_600a: GenericCircuitType.A600,
        is_inner_triplet: GenericCircuitType.IT,
        is_ipd: GenericCircuitType.IPD,
        is_ipq: GenericCircuitType.IPQ,
        is_main_dipole: GenericCircuitType.RB,
        is_main_quadrupole: GenericCircuitType.RQ,
    }

    for check_func, circuit_type in circuit_type_mapping.items():
        if check_func(circuit_name):
            return circuit_type

    raise NotImplementedError(f"Circuit name '{circuit_name}' does not map to a generic circuit type.")


def get_family_name_for_600A(circuit_name: str) -> str:
    """Parses name of the 600A circuit to get its family name:
    - RCD-RCO for RCD, RCO circuits
    - RCBX for RCBXH, RCBXV circuits
    - RSD for RSD circuit (circuit name is followed by a number before the dot)
    - RSF for RSF circuit (circuit name is followed by a number before the dot)
    - prefix before the dot for: RQTL11, RQTL10, RQTL8, RQTL7, RQT13, RQT12, RQS, RQSX3, RU, RSS, RQTL9, RQTD, RQTF,
    RQS, RQ6, ROD, ROF, RCS

    Args:
        circuit_name: a name of the 600A circuit

    Returns:
        A family name
    """
    if "RCD" in circuit_name or "RCO" in circuit_name:
        return "RCD-RCO"
    for family in ["RCBX", "RSD", "RSF"]:
        if family in circuit_name:
            return family
    return circuit_name.split(".")[0]


def get_family_name_for_80_120A(circuit_name: str) -> str:
    """Parses name of the 80-120A circuit to get its family name:
    - Family name is the prefix before the separation dot:
    e.g., RCOSX3, RCOX3, RCSSX3, RCSX3, RCTX3 -> RCOSX3, RCOX3, RCSSX3, RCSX3, RCTX3
    - otherwise it contains letters from the prefix before the separation dot:
    e.g., RCBCH5-10, etc. -> RCBCH

    Args:
        circuit_name: a name of the 80-120A circuit

    Returns:
        A family name
    """
    prefix = circuit_name.split(".")[0]
    if prefix in ["RCOSX3", "RCOX3", "RCSSX3", "RCSX3", "RCTX3"]:
        return prefix
    prefix_alpha = [prefix_el for prefix_el in prefix if prefix_el.isalpha()]
    return "".join(prefix_alpha)


def get_wildcard_for_circuit_system_db_signal(
    circuit_type: str, circuit_name: str, system: str, database: str, signal: str
) -> tuple[str, list[str]]:
    """Retrieves a wildcard and its possible values for given arguments.
    Eg. For 'BUSBAR' system, returns %BUSBAR%' and a list of busbars corresponding with a `circuit_name`

    Args:
        circuit_type: circuit type
        circuit_name: circuit name corresponding to circuit type
        system: name of a system
        database: name of a database (PM, NXCALS)
        signal: name of a signal

    Returns:
     A tuple of a wildcard name and a list of possible values
    """

    if system in ["QH", "QDS"] and circuit_type == "RB":
        return _get_wildcard_for_quench(circuit_type, circuit_name, database)
    if "VF" in system and circuit_type in ["RB", "RQ"]:
        wildcard = MappingMetadata.get_vf_for_circuit_names(circuit_type, circuit_name)
        return "%MAGNET%", wildcard
    if "BUSBAR" in system:
        wildcard = MappingMetadata.get_busbars_for_circuit_name(circuit_type, circuit_name)
        return "%BUSBAR%", wildcard
    if "DIODE" in system and circuit_type in ["RQ", "RB"]:
        return _get_wildcard_for_diode(circuit_type, circuit_name, signal)
    if system == "CRYO":
        layout = MappingMetadata.read_layout_details(circuit_type)
        cryostat = layout[layout["Circuit"] == circuit_name]["Cryostat2"].values
        return "%CRYOSTAT2%", list(cryostat)
    return "", [""]


def _get_wildcard_for_quench(circuit_type, circuit_name, database):
    if database == "PM":
        wildcard = MappingMetadata.get_cells_for_circuit_names(circuit_type, circuit_name)
        return "%CELL%", wildcard
    if database == "NXCALS":
        wildcard = MappingMetadata.get_magnets_for_circuit_names(circuit_type, circuit_name)
        return "%MAGNET%", wildcard
    raise KeyError(f"Database type {database} not present in ['PM', 'NXCALS'].")


def _get_wildcard_for_diode(circuit_type, circuit_name, signal):
    if circuit_type == "RB":
        if signal == "U_REF_N1":
            wildcard = MappingMetadata.get_crates_for_circuit_names(circuit_type, circuit_name)
            return "%CRATE%", wildcard
        wildcard = MappingMetadata.get_magnets_for_circuit_names(circuit_type, circuit_name)
        return "%MAGNET%", wildcard
    if circuit_type == "RQ":
        if signal == "U_REF_N1":
            wildcard = MappingMetadata.get_crates_for_circuit_names(circuit_type, circuit_name)
            return "%CRATE%", wildcard
        wildcard = MappingMetadata.get_magnets_for_circuit_names(circuit_type, circuit_name)
        # remove wildcards for which U_DIODE_RQx is NaN
        magnets_with_nan = _get_rq_magnet_names_without_u_diode(circuit_name)
        wildcard = [wc for wc in wildcard if wc not in magnets_with_nan]
        return "%MAGNET%", wildcard
    return "", [""]


def _get_rq_magnet_names_without_u_diode(circuit_name: str) -> list[str]:
    """Lists RQ magnet names for which a U_DIODE signal is not present.

    Args:
        circuit_name: RQ circuit name

    Returns:
     A list of RQ magnet names without a diode.
    """

    magnet_to_qps_crate_df = MappingMetadata.read_magnet_to_cell_qps_crate("RQ")
    mask_magnets_with_nan = (magnet_to_qps_crate_df["Circuit"] == circuit_name) & (
        magnet_to_qps_crate_df["Crate U_DIODE_RQx"].isna()
    )
    return magnet_to_qps_crate_df[mask_magnets_with_nan]["Magnet"].values.tolist()


def get_signal_name(
    circuit_type: str,
    circuit_name: str | list[str],
    system: str,
    database: str,
    signal: str | list[str],
    timestamp: TimeAlias | None = None,
) -> str | list[str]:
    """Lists the signal names for the given arguments.
    Updates the wildcards with the metadata corresponding to the circuit name.

    Args:
        circuit_type: circuit type
        circuit_name: circuit name corresponding to circuit type
        system: name of a system
        database: name of a database (PM, NXCALS)
        signal: name of a signal
        timestamp: point in time the returned metadata will refer to (now by default)

    Returns:
        A list of signal names (with updated wildcards)
    """

    circuit_names = utils.vectorize(circuit_name)
    signals = utils.vectorize(signal)

    signal_names: list[str] = []
    for circuit_name_ in circuit_names:
        for signal_ in signals:
            var_names = _update_wildcards_in_signal_name_per_circuit_name(
                circuit_type, circuit_name_, database, system, signal_, timestamp
            )

            signal_names.extend(utils.vectorize(var_names))

    return signal_names[0] if len(signal_names) == 1 else signal_names


def get_nxcals_system_name(
    circuit_type: str, system: str | list[str], signal: str | list[str], timestamp: TimeAlias | None = None
) -> str | list[str]:
    """Retrieves an NXCALS system (CMW or WINCCOA) matching given arguments.
    If more than one system and more than one signal provided, all the combinations will be checked.

    Args:
        circuit_type: circuit type
        system: system names(s)
        signal: signal names(s)
        timestamp: point in time the returned metadata will refer to (now by default)

    Returns:
        NXCALS system name (CMW or WINCCOA)

    Raises:
        KeyError: If signal do not found for the provided system and circuit type.
                  If signals come from different NXCALS systems
    """

    def get_nxcals_system(sigmon_sys, variable):
        for nxcals_sys in circuit_metadata[sigmon_sys]["NXCALS"]:
            if variable in circuit_metadata[sigmon_sys]["NXCALS"][nxcals_sys]:
                return nxcals_sys
        raise KeyError(f"Signal '{variable}' not present in {sigmon_sys}.")

    found_systems = set()
    circuit_metadata = _get_metadata(circuit_type, timestamp)
    systems = utils.vectorize(system)
    signals = utils.vectorize(signal)
    for sigmon_system in systems:
        for variable_name in signals:
            found_systems.add(get_nxcals_system(sigmon_system, variable_name))
        if len(found_systems) > 1:
            raise KeyError(f"Can't query for all signals {signals} because they come from different NXCALS systems")
    return found_systems.pop()


def get_circuit_interlock_type(circuit_name: str) -> str:
    """Returns the interlock type for a specific circuit name"""

    with open(pathlib.Path(__file__).parent / "circuit" / "circuit_types_location_acctesting.csv") as f:
        for row in csv.DictReader(f):
            if row["CIRCUIT_NAME"] == circuit_name:
                return row["INTERLOCK_TYPE"]
    raise ValueError(f"Circuit name {circuit_name} not present in the metadata.")


def get_circuit_location(circuit_name: str) -> str:
    """Returns the location of the circuit."""

    with open(pathlib.Path(__file__).parent / "circuit" / "circuit_types_location_acctesting.csv") as f:
        for row in csv.DictReader(f):
            if row["CIRCUIT_NAME"] == circuit_name:
                return row["LOCATION"]
    raise ValueError(f"Circuit name {circuit_name} not present in the metadata.")


def get_circuit_subsector(circuit_name: str) -> str:
    """Returns the subsector of the circuit."""

    with open(pathlib.Path(__file__).parent / "circuit" / "circuit_types_location_acctesting.csv") as f:
        for row in csv.DictReader(f):
            if row["CIRCUIT_NAME"] == circuit_name:
                return row["SUBSECTOR"]
    raise ValueError(f"Circuit name {circuit_name} not present in the metadata.")


def has_dfb(circuit_name: str) -> bool:
    """Check if a circuit has a dfb.

    Args:
        circuit_name: circuit name

    Returns:
        True if the circuit has a dfb, False otherwise

    Raises:
        ValueError: If the circuit name is not present in the metadata
    """
    return get_generic_circuit_type_for_circuit_name(circuit_name) not in (
        GenericCircuitType.A60,
        GenericCircuitType.A80_120,
    )


def has_ee(circuit_name: str) -> bool:
    """Check if a circuit has an energy extraction system.

    Args:
        circuit_name: circuit name

    Returns:
        True if the circuit has an energy extraction system, False otherwise

    Raises:
        ValueError: If the circuit name is not present in the metadata
    """
    circuit_type = get_generic_circuit_type_for_circuit_name(circuit_name)

    if circuit_type in (GenericCircuitType.RB, GenericCircuitType.RQ):
        return True
    if circuit_type == GenericCircuitType.A600:
        with open(pathlib.Path(__file__).parent / "circuit" / "circuit_types_location_acctesting.csv") as f:
            for row in csv.DictReader(f):
                if row["CIRCUIT_NAME"] == circuit_name:
                    return row["CIRCUIT_TYPE"] == "600A EE"
        raise ValueError(f"Circuit name {circuit_name} not present in the metadata.")
    return False


def has_crowbar(circuit_name: str) -> bool:
    """Check if a circuit has a crowbar

    Args:
        circuit_name: circuit name

    Returns:
        True if the circuit has a crowbar system, False otherwise

    Raises:
        ValueError: If the circuit name is not present in the metadata
    """
    with open(pathlib.Path(__file__).parent / "circuit" / "circuit_types_location_acctesting.csv") as f:
        for row in csv.DictReader(f):
            if row["CIRCUIT_NAME"] == circuit_name:
                if not row["CIRCUIT_TYPE"].startswith("600A"):
                    raise NotImplementedError("The has_crowbar method currently only supports 600a circuits.")
                return row["CIRCUIT_TYPE"].endswith("crowbar")
    raise ValueError(f"Circuit name {circuit_name} not present in the metadata.")


def get_fgc_pm_class_name(circuit_name: str, timestamp: TimeAlias, origin: Literal["self", "ext"]) -> str:
    """Returns the class name to query FGCs from PM

    Args:
        circuit_type: circuit type
        timestamp: FGC query timestamp
        origin: "self" (for standard FGC data dumps) or "ext" (for external FGC data dumps)

    Returns:
        FGC class name

    Raises:
        ValueError: If the origin is not "self" or "ext" or if the circuit name is not present in the metadata

    Examples:
        >>> from lhcsmapi.metadata import signal_metadata
        >>> timestamp = 1498780800000000000 # 2017-07-01 00:00:00
        >>> signal_metadata.get_fgc_pm_class_name("RCBH11.L1B2", timestamp, "self")
        'lhc_self_pmd'
        >>> signal_metadata.get_fgc_pm_class_name("RCBH11.L1B2", timestamp, "ext")
        'lhc_ext_pmd'
        >>> signal_metadata.get_fgc_pm_class_name("RQ5.L1", timestamp, "self")
        '51_self_pmd'
        >>> signal_metadata.get_fgc_pm_class_name("RQ5.L1", timestamp, "ext")
        '51_ext_pmd'
    """
    if origin not in {"self", "ext"}:
        raise ValueError(f"The origin of the FGC dump could be only 'self' or 'ext' - provided {origin}.")
    timestamp = Time.to_unix_timestamp(timestamp)
    renaming_date_of_fgc_pm_class_names = _get_renaming_date_of_fgc_pm_class_names(circuit_name)

    return f"lhc_{origin}_pmd" if timestamp >= renaming_date_of_fgc_pm_class_names else f"51_{origin}_pmd"


def get_fgc_with_earth_measurement(circuit_name: str, timestamp: int) -> str:
    """Returns the name of the fgc with earth measurement for a specific IPQ circuit
    See https://edms.cern.ch/document/2359699/1

    Args:
        circuit_type: circuit type
        circuit_name: circuit name
        timestamp: timestamp event

    Returns:
        The name of the fgc

    Raises:
        ValueError: If the circuit type is not supported

    Examples:
        >>> from lhcsmapi.metadata import signal_metadata
        >>> circuit_name = "RQ5.L5"
        >>> timestamp = 1679052360000000000
        >>> signal_metadata.get_fgc_with_earth_measurement(circuit_name, timestamp)
        'RPHSB.RR53.RQ5.L5B1'
    """
    if not is_ipq(circuit_name):
        raise ValueError(f"Circuit {circuit_name} is not an IPQ.")

    timestamp = Time.to_unix_timestamp(timestamp)
    with open(pathlib.Path(__file__).parent / "earth_current" / "IPQ_EarthCurrent.csv") as f:
        for row in csv.DictReader(f):
            if row["Circuit name"] == circuit_name:
                fgc_names = get_fgc_names(circuit_name, timestamp)
                for fgc_name in fgc_names:
                    if fgc_name.endswith(row["Power converter with earth measurement"]):
                        return fgc_name
                raise NotImplementedError(
                    f"Circuit name {circuit_name} is supposed to have a fgc with earth measurement that "
                    f"finishes with {row['Power converter with earth measurement']} but it is not present. "
                    f"Fgc names for this circuit are {fgc_names}."
                )
    raise NotImplementedError(f"Circuit name {circuit_name} is supported not present in the metadata.")


def _get_renaming_date_of_fgc_pm_class_names(circuit_name: str) -> int:
    """Returns the migration date of the FGC class names in PM

    Args:
        circuit_name: circuit name

    Returns:
        FGC class name migration date in ns since epoch

    Raises:
        ValueError: If the circuit name is not present in the metadata.
    """
    circuit_type = get_generic_circuit_type_for_circuit_name(circuit_name)

    if circuit_type == GenericCircuitType.A60:
        return 1483228800000000000  # 2017-01-01 00:00:00
    else:
        return 1514764800000000000  # 2018-01-01 00:00:00


def is_ipq(circuit_name: str) -> bool:
    """Check if a circuit is an IPQ circuit
    See https://confluence.cern.ch/display/MPEEP/DYPG+IPQ+racks+and+magnets

    Args:
        circuit_name: circuit name

    Returns:
        True if the circuit is an IPQ circuit, False otherwise
    """
    return get_circuit_type_for_circuit_name(circuit_name) in [CircuitType.IPQ2, CircuitType.IPQ4, CircuitType.IPQ8]


def is_ipd(circuit_name: str) -> bool:
    """Check if a circuit is an IPD circuit
    See https://confluence.cern.ch/display/MPEEP/DYPG+IPD+racks+and+magnets

    Args:
        circuit_name: circuit name

    Returns:
        True if the circuit is an IPD circuit, False otherwise
    """
    return get_circuit_type_for_circuit_name(circuit_name) in [CircuitType.IPD2, CircuitType.IPD2_B1B2]


def is_main_dipole(circuit_name: str) -> bool:
    """Check if a circuit is a main dipole circuit

    Args:
        circuit_name: circuit name

    Returns:
        True if the circuit is a main dipole circuit, False otherwise
    """
    return get_circuit_type_for_circuit_name(circuit_name) == CircuitType.RB


def is_main_quadrupole(circuit_name: str) -> bool:
    """Check if a circuit is a main quadrupole circuit

    Args:
        circuit_name: circuit name

    Returns:
        True if the circuit is a main quadrupole circuit, False otherwise
    """
    return get_circuit_type_for_circuit_name(circuit_name) == CircuitType.RQ


def is_inner_triplet(circuit_name: str) -> bool:
    """Check if a circuit is an inner triplet circuit

    Args:
        circuit_name: circuit name

    Returns:
        True if the circuit is an inner triplet circuit, False otherwise
    """
    return get_circuit_type_for_circuit_name(circuit_name) == CircuitType.IT


def is_60a(circuit_name: str) -> bool:
    """Check if a circuit is a 60A circuit

    Args:
        circuit_name: circuit name

    Returns:
        True if the circuit is a 60A circuit, False otherwise
    """
    return get_circuit_type_for_circuit_name(circuit_name) == CircuitType.A60


def is_80_120a(circuit_name: str) -> bool:
    """Check if a circuit is an 80-120A circuit

    Args:
        circuit_name: circuit name

    Returns:
        True if the circuit is an 80-120A circuit, False otherwise
    """
    return get_circuit_type_for_circuit_name(circuit_name) == CircuitType.A80_120


def is_600a(circuit_name: str) -> bool:
    """Check if a circuit is a 600A circuit

    Args:
        circuit_name: circuit name

    Returns:
        True if the circuit is a 600A circuit, False otherwise
    """
    return get_circuit_type_for_circuit_name(circuit_name) == CircuitType.A600


def is_undulator(circuit_name: str) -> bool:
    """Check if a circuit is an undulator
    See https://edms.cern.ch/document/493200/2

    Args:
        circuit_name: circuit name

    Returns:
        True if the circuit is an undulator, False otherwise
    """
    return circuit_name in ["RU.L4", "RU.R4"]


def get_number_of_heaters(circuit_name: str) -> int:
    """Returns the number of heaters
    See:
        IPQs: https://confluence.cern.ch/display/MPEEP/DYPG+IPQ+racks+and+magnets
        IPDs: https://confluence.cern.ch/display/MPEEP/DYPG+IPD+racks+and+magnets

    Args:
        circuit_name: circuit name

    Returns:
        Number of heaters
    """
    circuit_type = get_circuit_type_for_circuit_name(circuit_name)

    mapping = {
        CircuitType.IPD2.value: 2,
        CircuitType.IPD2_B1B2.value: 2,
        CircuitType.IPQ2.value: 2,
        CircuitType.IPQ4.value: 4,
        CircuitType.IPQ8.value: 8,
    }

    if circuit_type not in mapping:
        raise NotImplementedError(
            f"This method does not support the circuit type {circuit_type}. "
            "It should only be relevant for IPQ and IPD circuits."
        )

    return mapping[circuit_type]


def get_qps_pm_class_name_for_ipq(circuit_name: str) -> str:
    """Returns the DQAMGNRQx class name depending on the number of heaters
    See https://confluence.cern.ch/display/MPEEP/DQAMGNRQx

    Args:
        circuit_type: circuit type

    Returns:
        IPQ QDS pm class name

    Raises:
        ValueError: If the circuit type is not IPQ
    """
    if not is_ipq(circuit_name):
        raise ValueError(f"{circuit_name} is not an IPQ circuit.")

    number_of_heaters = get_number_of_heaters(circuit_name)
    suffix_mapping = {2: "A", 4: "B", 8: "C"}
    if number_of_heaters not in suffix_mapping:
        raise NotImplementedError(f"The number of heaters {number_of_heaters} is not supported.")

    return f"DQAMGNRQ{suffix_mapping[number_of_heaters]}"


def get_qps_pm_class_name_for_ipd(circuit_name: str, timestamp: int) -> str:
    """Returns the IPD class name depending on the number of heaters
    See https://confluence.cern.ch/display/MPEEP/DQAMGNRQx and https://confluence.cern.ch/display/MPEEP/DQAMGNRD

    Args:
        circuit_type: circuit type
        timestamp: timestamp of the query in ns since epoch

    Returns:
        IPD QPS pm class name

    Raises:
        ValueError: If the circuit type is not IPD
    """
    if not is_ipd(circuit_name):
        raise ValueError(f"{circuit_name} is not an IPD circuit.")

    circuit_type = get_circuit_type_for_circuit_name(circuit_name)
    return "DQAMGNRD" if circuit_type == CircuitType.IPD2 and timestamp <= _RUN_3_QPS_UPGRADE_START else "DQAMGNRQA"


def get_hwc_summary(path: str | pathlib.Path | None = None) -> pd.DataFrame:
    """Returns the HWC summary, a summary of the hardware commissioning history
    results of the hardware commissioning tests.

    Args:
        path: optional path to override the default HWC summary file

    Examples:
        >>> from lhcsmapi.metadata import signal_metadata
        >>> df = signal_metadata.get_hwc_summary()
        >>> print(df[["systemName", "testName", "executionStartTime", "executedTestStatus"]].head().to_markdown())
        |    | systemName   | testName   | executionStartTime            | executedTestStatus   |
        |---:|:-------------|:-----------|:------------------------------|:---------------------|
        |  0 | RQ5.R4       | PCL        | 2007-12-06 15:23:19.283000000 | SUCCESSFUL           |
        |  1 | RQ5.R4       | PCC.4      | 2007-12-06 15:38:14.241000000 | SIGNING_PENDING      |
        |  2 | RD4.R4       | PCL        | 2007-12-06 15:53:54.849000000 | SUCCESSFUL           |
        |  3 | RD4.R4       | PCC.3      | 2007-12-06 16:11:03.081000000 | FAILED               |
        |  4 | RCBYH5.R4B2  | PCL        | 2007-12-06 16:23:06.627000000 | SUCCESSFUL           |
    """
    if path is None:
        path = pathlib.Path(__file__).parent / "acctesting" / "hwc_summary.csv"

    return pd.read_csv(path, low_memory=False).sort_values("executionStartTime").reset_index(drop=True)


def get_cells_for_circuit(circuit_name: str) -> set[str]:
    """Returns the cell names for a given RB or RQ circuit name."""
    circuit_type = get_generic_circuit_type_for_circuit_name(circuit_name)
    if circuit_type not in (GenericCircuitType.RB, GenericCircuitType.RQ):
        raise ValueError(f"{circuit_name} is not a main dipole or main quadrupole circuit.")

    df = pd.read_csv(pathlib.Path(__file__).parent / "magnet" / f"{circuit_type}_MagnetCellQpscrateSector.csv")
    return set(df[df["Circuit"] == circuit_name]["Cell"].tolist())


def _update_wildcards_in_signal_name_per_circuit_name(
    circuit_type: str, circuit_name: str, database: str, system: str, signal: str, timestamp
) -> str | list[str]:
    circuit_signal_type_db_metadata = get_signal_metadata(circuit_type, circuit_name, system, database, timestamp)

    if database == "NXCALS":
        for system_type in circuit_signal_type_db_metadata:
            if signal in circuit_signal_type_db_metadata[system_type]:
                circuit_meta = circuit_signal_type_db_metadata[system_type]
                if isinstance(circuit_meta, dict):
                    signal = circuit_meta[signal]
                    break
                else:
                    raise TypeError(f"Metadata for {system_type} is not a dictionary")
    else:
        circuit_metadata = circuit_signal_type_db_metadata[signal]

        if isinstance(circuit_metadata, list) and all(isinstance(vn, str) for vn in circuit_metadata):
            return [
                str(_update_wildcards_in_signal_name(circuit_type, circuit_name, system, vn, timestamp))
                for vn in circuit_metadata
            ]
        elif isinstance(circuit_metadata, str):
            return str(
                _update_wildcards_in_signal_name(circuit_type, circuit_name, system, circuit_metadata, timestamp)
            )

    if isinstance(signal, list):
        return [_update_wildcards_in_signal_name(circuit_type, circuit_name, system, vn, timestamp) for vn in signal]
    return _update_wildcards_in_signal_name(circuit_type, circuit_name, system, signal, timestamp)


def _update_wildcards_in_signal_name(
    circuit_type: str, circuit_name: str, system: str, signal: str, timestamp
) -> str | list[str]:
    # get all wildcards to be replaced
    circuit_name_metadata = _get_metadata_for_circuit_name(circuit_type, circuit_name, timestamp)
    # for circuit_name
    for key, value in circuit_name_metadata.items():
        if "%" in key:
            if not isinstance(value, str):
                raise TypeError(f"Value {value} from metadata is not a string")
            signal = signal.replace(key, value)
    # for system
    system_metadata = circuit_name_metadata[system]
    if not isinstance(system_metadata, dict):
        raise TypeError(f"Metadata for {system} is not a dictionary")
    for key, value in system_metadata.items():
        if key != "TYPE" and key in signal:
            if isinstance(value, list):
                return [signal.replace(key, val) for val in value]
            if not isinstance(value, str):
                raise TypeError(f"Value {value} from metadata is not a string")
            return signal.replace(key, value)
    return signal
