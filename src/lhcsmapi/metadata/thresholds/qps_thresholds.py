import os
from dataclasses import dataclass
from enum import Enum
from typing import List

import pandas as pd

from lhcsmapi.Time import Time

FAR_IN_THE_FUTURE_TIMESTAMP = 2524608001000000000  # 2050-01-01
_thresholds = None


def _read_qps_thresholds_from_csv() -> pd.DataFrame:
    metadata_dir = os.path.dirname(__file__)
    full_path = os.path.join(metadata_dir, "qps_thresholds.csv")
    return pd.read_csv(full_path)


@dataclass
class QpsThreshold:
    """Class for keeping qps thresholds

    Attributes:
        board: type of the board
        comment: additional information
        upper_threshold: upper quench detection threshold in mV
        lower_threshold: lower quench detection threshold in mV
        evaluation_time: evaluation time in ms
    """

    board: str
    comment: str
    upper_threshold: int
    lower_threshold: int
    evaluation_time: int


class QpsSource(Enum):
    MAGNET = "magnet"
    LEAD = "lead"
    BUSBAR = "bus-bar"


def get_thresholds_for_magnet(source: str, timestamp: any) -> List[QpsThreshold]:
    """Method returning a list of QpsThreshold for given source and timestamp for a magnet quench

    Args:
        source: source of quench
        timestamp: timestamp as string, datetime or nanoseconds

    Returns:
        List of QpsThresholds which contain board type, comment, lower and upper threshold and evaluation time
    """

    df = _get_filtered_qps_thresholds(source, QpsSource.MAGNET, timestamp)
    return _get_qps_thresholds_from_df(df)


def get_thresholds_for_lead(source: str, timestamp: any) -> List[QpsThreshold]:
    """Method returning a list of QpsThreshold for given source and timestamp for a lead quench

    Args:
        source: source of quench
        timestamp: timestamp as string, datetime or nanoseconds

    Returns:
        List of QpsThresholds which contain board type, comment, lower and upper threshold and evaluation time
    """

    df = _get_filtered_qps_thresholds(source, QpsSource.LEAD, timestamp)
    return _get_qps_thresholds_from_df(df)


def get_thresholds_for_busbar(source: str, timestamp: any) -> List[QpsThreshold]:
    """Method returning a list of QpsThreshold for given source and timestamp for a busbar quench

    Args:
        source: source of quench
        timestamp: timestamp as string, datetime or nanoseconds

    Returns:
        List of QpsThresholds which contain board type, comment, lower and upper threshold and evaluation time
    """

    df = _get_filtered_qps_thresholds(source, QpsSource.BUSBAR, timestamp)
    df = _filter_by_timestamp(df, timestamp)
    return _get_qps_thresholds_from_df(df)


def _get_filtered_qps_thresholds(source: str, quench_source: QpsThreshold, timestamp: any) -> pd.DataFrame:
    global _thresholds
    if _thresholds is None:
        _thresholds = _read_qps_thresholds_from_csv()

    df = _thresholds[(_thresholds["source"] == source) & (_thresholds["quench source"] == quench_source.value)]
    return _filter_by_timestamp(df, timestamp)


def _get_qps_thresholds_from_df(df: pd.DataFrame) -> List[QpsThreshold]:
    result = []
    for _, row in df.iterrows():
        threshold = QpsThreshold(
            row["board type"],
            row["comment"],
            row["upper threshold (mV)"],
            row["lower threshold (mV)"],
            row["evaluation time (ms)"],
        )
        result.append(threshold)
    return result


def _filter_by_timestamp(df: pd.DataFrame, timestamp) -> pd.DataFrame:
    if ((df["start"] != 0) | (df["end"] != FAR_IN_THE_FUTURE_TIMESTAMP)).any():
        timestamp = Time.to_unix_timestamp(timestamp)
        return df[(df["start"].astype(int) <= timestamp) & (df["end"].astype(int) > timestamp)]
    return df
