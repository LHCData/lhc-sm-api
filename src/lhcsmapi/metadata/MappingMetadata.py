import os
from functools import reduce
from typing import List, Union, Tuple

import pandas as pd
import numpy as np

selected_path = os.path.dirname(__file__)


class MappingMetadata(object):
    """MappingMetadata class provides methods to access tables with mapping from circuits to components:

    - blm
    - busbar
    - magnet
    - qps_crate

    """

    # read methods
    @staticmethod
    def read_circuit_name_to_sector() -> pd.DataFrame:
        """Method reading table with columns [Circuit type, Circuit name, Sector name]. The table provides a mapping
        from circuit name to sector name.

        :return: pd.DataFrame with mapping from circuit name to sector name
        """
        full_path = os.path.join(selected_path, "sector/Circuit_To_Sector_Names.csv")
        return pd.read_csv(full_path, index_col=0)

    @staticmethod
    def read_magnet_to_cell_qps_crate(circuit_type: str) -> pd.DataFrame:
        """Method reading table with magnet mapping of magnet name, qps crate, voltage feeler, sector, reference

        :param circuit_type: circuit type

        :return: pd.DataFrame with mapping from magnet to cell_or_circuit, qps crate, and sector
        """
        if circuit_type == "RQ":
            full_path = os.path.join(selected_path, "magnet/RQ_MagnetCellQpscrateSector.csv")
        else:
            raise KeyError("Circuit type {} is not supported!".format(circuit_type))
        return pd.read_csv(full_path, index_col=0)

    @staticmethod
    def read_crate_to_magnet_and_vf_mapping(circuit_type: str) -> pd.DataFrame:
        """Method reading table with qps crate mapping of magnet name to qps crate, voltage feeler, sector, reference.
        Method returns only magnets which have TD is present in the table entries (trigger lines).
        https://wikis.cern.ch/display/MPEEP/DQLPUS+crates+and+magnets

        :param circuit_type: circuit type

        :return: pd.DataFrame with mapping from magnet to cell_or_circuit, qps crate, and sector
        """

        if circuit_type == "RB":
            full_path = os.path.join(selected_path, "qps_crate/RB_CrateToMagnetAndVoltageFeelerMap.csv")
        elif circuit_type == "RQ":
            full_path = os.path.join(selected_path, "qps_crate/RQ_CrateToVoltageFeelerMap.csv")
        else:
            raise KeyError("Circuit type {} is not supported!".format(circuit_type))
        return pd.read_csv(full_path)

    @staticmethod
    def get_magnet_to_crate_and_vf_mapping(circuit_type) -> pd.DataFrame:
        """Method reading table with qps crate mapping of qps crate to magnet name, voltage feeler, sector, reference.
        Method returns only magnets which have TD is present in the table entries (trigger lines).
        https://wikis.cern.ch/display/MPEEP/DQLPUS+crates+and+magnets

        :param circuit_type: circuit type

        :return: pd.DataFrame with mapping from qps crate to cell_or_circuit, qps crate, and sector
        """
        if circuit_type == "RB":
            full_path = os.path.join(selected_path, "qps_crate/RB_MagnetToCrateAndVoltageFeelerMap.csv")
        elif circuit_type == "RQ":
            full_path = os.path.join(selected_path, "qps_crate/RQ_MagnetToCrateMap.csv")
        else:
            raise KeyError("Circuit type {} is not supported!".format(circuit_type))
        return pd.read_csv(full_path)

    @staticmethod
    def read_layout_details(circuit_type: str) -> pd.DataFrame:
        """Method reading table with magnet details for a given circuit type. The magnet details include:

        - circuit name
        - magnet name
        - magnet position
        - aperture of beam 1
        - diode type
        - corrector
        - EE place
        - cryostat

        :param circuit_type: circuit type

        :return: pd.DataFrame with circuit layout details
        """
        if circuit_type in ["RB", "RQ"]:
            full_path = os.path.join(selected_path, "magnet/%s_LayoutDetails.csv" % circuit_type)
        else:
            raise KeyError("Circuit type {} is not supported!".format(circuit_type))
        return pd.read_csv(full_path)

    @staticmethod
    def read_magnet_name_to_magnet_id_mapping(circuit_type: str) -> pd.DataFrame:
        """Method reading a table with:
        - magnet name
        - magnet position
        - time when it was replaced:

           - Initial
           - Incident (2008)
           - Run1
           - LS1
           - S12 warm-up
           - Run2 part2
           - LS2
           - Run3

        :param circuit_type: circuit type

        :return: pd.DataFrame with mapping from magnet name to magnet it
        """
        if circuit_type in ["RB", "RQ"]:
            full_path = os.path.join(selected_path, "magnet/%s_MagnetHistory.csv" % circuit_type)
        else:
            raise KeyError("Circuit type {} is not supported!".format(circuit_type))
        return pd.read_csv(full_path)

    @staticmethod
    def read_magnet_cell_crate_circuit_name_mapping(circuit_type: str) -> pd.DataFrame:
        """Method reading a mapping from magnet name to cell_or_circuit, qps_crate, and circuit name

        :param circuit_type: circuit type

        :return: pd.Dataframe with mapping from magnet name to cell_or_circuit, qps crate, circuit name
        """
        full_path = os.path.join(selected_path, "magnet/{}_MagnetCellQpscrateSector.csv".format(circuit_type))
        return pd.read_csv(full_path)

    @staticmethod
    def read_busbar_to_magnet_mapping(circuit_type: str) -> pd.DataFrame:
        """Method reading a mapping from busbar to magnet, qps crate, qps board, circuit name

        :param circuit_type: circuit type

        :return: pd.DataFrame with mapping from busbar to magnet, qps crate, qps board, circuit name
        """
        if circuit_type in ["RB", "RQ"]:
            full_path = os.path.join(selected_path, "busbar/nQPS_%s_busBarProtectionDetails.csv" % circuit_type)
        else:
            raise KeyError("Circuit type {} is not supported!".format(circuit_type))
        return pd.read_csv(full_path, index_col=0)

    @staticmethod
    def get_blm_table() -> pd.DataFrame:
        """Method reading a BLM table allowing to construct signal names for NXCALS database

        :return: pd.DataFrame with BLM details for NXCALS
        """
        full_path = os.path.join(selected_path, "blm/BLM_NXCALS.csv")
        return pd.read_csv(full_path, index_col=0).reset_index(drop=True)

    @staticmethod
    def get_rq_aperture(circuit_name: str, magnet_name: str) -> str:
        """Method returning aperture (EXT or INT) of an RQ circuit for a given circuit and magnet name

        :param circuit_name: circuit type
        :param magnet_name: magnet name

        :return: str with aperture of an RQ circuit for a given circuit and magnet name
        """
        rq_layout_details_df = MappingMetadata.read_layout_details("RQ")
        return rq_layout_details_df[
            (rq_layout_details_df["Circuit"] == circuit_name) & (rq_layout_details_df["Magnet"] == magnet_name)
        ]["Aperture"].values[0]

    @staticmethod
    def get_magnets_for_circuit_names(circuit_type: str, circuit_names: Union[str, List[str]]) -> List[str]:
        """Method returning a list of magnet names corresponding to an input circuit type and circuit name(s)

        :param circuit_type:
        :param circuit_names:

        :return: List[str] of magnet names corresponding to an input circuit type and circuit name(s)
        """
        circuit_names = circuit_names if isinstance(circuit_names, list) else [circuit_names]  # to a decorator!

        if circuit_type in ["RB", "RQ"]:
            magnet_to_circuit_name = MappingMetadata.read_magnet_cell_crate_circuit_name_mapping(circuit_type)
            are_magnets_matching_circuit_name = magnet_to_circuit_name["Circuit"].isin(circuit_names)
            magnets_matching_circuit_name = magnet_to_circuit_name[are_magnets_matching_circuit_name]
            return list(magnets_matching_circuit_name["Magnet"].values)
        else:
            raise KeyError("Circuit type {} is not supported!".format(circuit_type))

    @staticmethod
    def get_cells_for_circuit_names(circuit_type: str, circuit_names: Union[str, List[str]]) -> List[str]:
        """Method returning a list of magnet cells corresponding to an input circuit type and circuit name(s)

        :param circuit_type:
        :param circuit_names:

        :return: List[str] of magnet cells corresponding to an input circuit type and circuit name(s)
        """
        circuit_names = circuit_names if isinstance(circuit_names, list) else [circuit_names]  # to a decorator!

        if circuit_type in ["RB", "RQ"]:
            magnet_to_circuit_name = MappingMetadata.read_magnet_cell_crate_circuit_name_mapping(circuit_type)
            are_magnets_matching_circuit_name = magnet_to_circuit_name["Circuit"].isin(circuit_names)
            magnets_matching_circuit_name = magnet_to_circuit_name[are_magnets_matching_circuit_name]
            return list(magnets_matching_circuit_name["Cell"].values)
        else:
            raise KeyError("Circuit type {} is not supported!".format(circuit_type))

    @staticmethod
    def get_crate_name_from_magnet_name(circuit_type: str, magnet: Union[str, List[str]]) -> Union[str, List[str]]:
        """Method returning nQPS crate name for a given circuit type and magnet name

        :param circuit_type: circuit type
        :param magnet: magnet name

        :return: str or List[str] of nQPS crates by which magnets are supervised
        """
        magnets_crates = MappingMetadata.get_magnet_to_crate_and_vf_mapping(circuit_type)
        magnets_crates.fillna(value="", inplace=True)
        if circuit_type == "RB":
            mask = reduce(
                np.logical_or, [magnets_crates["Magnet{}".format(i)].str.contains(magnet) for i in range(1, 5)]
            )
            magnet_crates_matching = magnets_crates.loc[mask]
        elif circuit_type == "RQ":
            # primary crate if matching u_diode signal
            mask = magnets_crates.apply(lambda row: magnet in row["U_DIODE_RQx"], axis=1)
            magnet_crates_matching = magnets_crates.loc[mask]
            if len(magnet_crates_matching) == 0:
                # secondary crate if matching u_ref signal (special feature of RQ circuits, not needed for RB)
                mask = magnets_crates.apply(lambda row: magnet in row["U_REF_N1"], axis=1)
                magnet_crates_matching = magnets_crates.loc[mask]
        else:
            raise KeyError("Circuit type: {} is not supported".format(circuit_type))

        if len(magnet_crates_matching) == 0:
            raise ValueError("Magnet: {} does not map to a crate".format(magnet))
        return magnet_crates_matching["Crate"].values[0]

    @staticmethod
    def get_crates_for_circuit_names(circuit_type: str, circuit_names: Union[str, List[str]]) -> List[str]:
        """Method returning a list of nQPS crates corresponding to an input circuit type and circuit name(s)

        :param circuit_type:
        :param circuit_names:

        :return: List[str] of nQPS crates corresponding to an input circuit type and circuit name(s)
        """
        circuit_names = circuit_names if isinstance(circuit_names, list) else [circuit_names]

        if circuit_type in ["RB", "RQ"]:
            crate_to_magnet = MappingMetadata.read_crate_to_magnet_and_vf_mapping(circuit_type)
            is_crate_matching_sector = crate_to_magnet["Circuit"].isin(circuit_names)
            crates_matching_sector = crate_to_magnet[is_crate_matching_sector]
            return list(crates_matching_sector["Crate"].values)
        else:
            raise KeyError("Circuit type {} is not supported!".format(circuit_type))

    @staticmethod
    def get_vf_for_circuit_names(circuit_type: str, circuit_names: Union[str, List[str]]) -> List[str]:
        """Method returning a list of voltage feelers corresponding to an input circuit type and circuit name(s)

        :param circuit_type:
        :param circuit_names:

        :return: List[str] of voltage feelers corresponding to an input circuit type and circuit name(s)
        """
        circuit_names = circuit_names if isinstance(circuit_names, list) else [circuit_names]  # to a decorator!

        if circuit_type == "RB":
            crate_to_magnet = MappingMetadata.read_crate_to_magnet_and_vf_mapping(circuit_type)
            is_crate_matching_sector = crate_to_magnet["Circuit"].isin(circuit_names)
            crates_matching_sector = crate_to_magnet[is_crate_matching_sector]
            return list(crates_matching_sector["U_EARTH_RB"].dropna().values)
        elif circuit_type == "RQ":
            crate_to_magnet = MappingMetadata.read_crate_to_magnet_and_vf_mapping(circuit_type)
            is_crate_matching_sector = crate_to_magnet["Circuit"].isin(circuit_names)
            crates_matching_sector = crate_to_magnet[is_crate_matching_sector]
            return list(crates_matching_sector["U_EARTH_RQx"].dropna().values)
        else:
            raise KeyError("Circuit type {} is not supported!".format(circuit_type))

    @staticmethod
    def get_busbars_for_circuit_name(circuit_type: str, circuit_names: Union[str, List[str]]) -> List[str]:
        """Method returning a list of busbars corresponding to an input circuit type and circuit name(s)

        :param circuit_type:
        :param circuit_names:

        :return: List[str] of busbars corresponding to an input circuit type and circuit name(s)
        """

        circuit_names = circuit_names if isinstance(circuit_names, list) else [circuit_names]  # to a decorator!

        bb_df = MappingMetadata.read_busbar_to_magnet_mapping(circuit_type)
        is_busbar_matching_circuit_name = bb_df["Circuit"].isin(circuit_names)
        busbar_matching_circuit_name = bb_df[is_busbar_matching_circuit_name]["Bus Bar Segment Name"].values
        return list(busbar_matching_circuit_name)

    @staticmethod
    def get_crate_name_from_busbar_names(circuit_type: str, busbar_names: Union[str, List[str]]) -> List[str]:
        """Method returning an nQPS crate name for given busbar(s).

        :param circuit_type: circuit type
        :param busbar_names: either a single busbar or a list of busbar names

        :return: either a single or a list of nQPS crate(s)
        """
        busbar_names = busbar_names if isinstance(busbar_names, list) else [busbar_names]

        bb_df = MappingMetadata.read_busbar_to_magnet_mapping(circuit_type)
        is_busbar_matching_circuit_name = bb_df["Bus Bar Segment Name"].isin(busbar_names)
        busbar_matching_circuit_name = bb_df[is_busbar_matching_circuit_name]["QPS Crate"].values
        return list(busbar_matching_circuit_name)

    @staticmethod
    def get_magnet_names_from_crate_name(circuit_type: str, crate_name: str) -> List[str]:
        """Method returning a list of magnets corresponding to an nQPS crate

        :param circuit_type: circuit type
        :param crate_name: nQPS crate name

        :return: a list of magnets corresponding to an nQPS crate
        """
        magnet_to_crate_and_vf = MappingMetadata.get_magnet_to_crate_and_vf_mapping(circuit_type)

        magnets = (
            magnet_to_crate_and_vf[magnet_to_crate_and_vf["Crate"] == crate_name].filter(regex=r"Magnet\d{1}").values[0]
        )
        # type is checked as the table may contain NaN
        return [x for x in magnets if isinstance(x, str) and ("MB." in x)]

    @staticmethod
    def get_magnet_id_from_magnet_name(circuit_type: str, magnet: Union[str, List[str]]) -> List[str]:
        """Method returning a magnet ID for a given matching name and a timestamp.

        :param circuit_type: circuit type
        :param magnet: magnet name

        :return: magnet ID matching a circuit type and magnet name for a given operational period
        """
        magnet = magnet if isinstance(magnet, list) else [magnet]

        magnet_name_to_magnet_id = MappingMetadata.read_magnet_name_to_magnet_id_mapping(circuit_type)
        are_magnet_names_matching = magnet_name_to_magnet_id["Magnet"].isin(magnet)
        magnet_name_to_magnet_id_matching = magnet_name_to_magnet_id[are_magnet_names_matching]

        if len(magnet_name_to_magnet_id_matching) != len(magnet):
            raise ValueError("Didn't find matching magnet ids for all magnets")

        if len(magnet_name_to_magnet_id_matching) == 1:
            return magnet_name_to_magnet_id_matching["Run2 part2"].values[0]
        else:
            return magnet_name_to_magnet_id_matching["Run2 part2"].values

    @staticmethod
    def get_electrical_position(circuit_type: str, magnet: str) -> int:
        """Method returning an electrical position in a string of magnets for a given circuit type and magnet name.

        :param circuit_type: circuit type
        :param magnet: magnet name

        :return: int with an index of electrical position (1-based)
        """
        layout_details = MappingMetadata.read_layout_details(circuit_type)
        return layout_details[layout_details["Magnet"] == magnet]["#Electric_circuit"].values[0]

    @staticmethod
    def get_qps_schematic_naming(circuit_type: str, circuit_name: str) -> str:
        """Method returning a QPS schematic name for an IPQ circuit.

        :param circuit_type: circuit type
        :param circuit_name: circuit name
        :return: QPS schematic name
        """
        if "IPQ" in circuit_type:
            full_path = os.path.join(selected_path, "magnet/IPQ_SchematicNaming.csv")
        elif "IPD" in circuit_type:
            full_path = os.path.join(selected_path, "magnet/IPD_SchematicNaming.csv")
        else:
            raise KeyError("Circuit type {} is not supported!".format(circuit_type))

        schematic_naming = pd.read_csv(full_path)

        return schematic_naming[schematic_naming["circuit_name"] == circuit_name]["schematic_name"].values[0]

    @staticmethod
    def get_sector_name_for_circuit_name(circuit_name: str) -> str:
        sectors_df = MappingMetadata.read_circuit_name_to_sector()
        return sectors_df[sectors_df["Circuit name"] == circuit_name]["Sector name"].values[0]

    @staticmethod
    def get_subsector_name_for_circuit_name(circuit_name: str) -> str:
        sectors_df = MappingMetadata.read_circuit_name_to_sector()
        return sectors_df[sectors_df["Circuit name"] == circuit_name]["Safety subsector name"].values[0]

    @staticmethod
    def get_crate_and_diode_signal_name_for_rq(circuit_name: str, magnet_name: str) -> Tuple[str, str]:
        """Returns the name of the source crate and the corresponding diode signal name for the circuits of the RQ type.
        The diode signal name for the RQ is one of "U_DIODE_RQD", "U_DIODE_RQF" and "U_REF_N1".

        :param circuit_name: the name of the circuit
        :param magnet_name: the name of the magnet
        :return: the crate and the diode signal name
        """
        rq_diode = MappingMetadata.read_magnet_to_cell_qps_crate("RQ")
        rq_diode = rq_diode[(rq_diode["Circuit"] == circuit_name) & (rq_diode["Magnet"] == magnet_name)]
        crate_name = rq_diode["Crate U_DIODE_RQx"].values[0]
        signal_name = f'U_DIODE_{circuit_name.split(".")[0]}'

        if not isinstance(crate_name, str) and np.isnan(crate_name):
            crate_name = rq_diode["Crate U_REF_N1"].values[0]
            signal_name = "U_REF_N1"

        return crate_name, signal_name
