from typing import Collection, Any

import pandas as pd


def get_columns(df: pd.DataFrame, columns: Collection[Any]) -> pd.DataFrame:
    """Selects columns from the dataFrame, for missing columns an empty column is returned."""
    return pd.DataFrame(df, columns=columns)[columns]
