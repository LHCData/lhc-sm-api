from typing import Union, List, Any


def vectorize(element: Union[List[Any], Any]) -> List[Any]:
    """Unless the provided element is a list, wraps it up in a list."""
    return element if isinstance(element, list) else [element]


def unpack_single_element(elements: List[Any]) -> Union[List[Any], Any]:
    """If the list contains only one element returns only the element"""
    if len(elements) == 1:
        return elements[0]
    return elements
