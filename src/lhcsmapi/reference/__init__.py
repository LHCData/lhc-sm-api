"""Methods to access tables with reference values of the following components:

- EE
- Current Leads
- MP3
- Power Converter
- QH
- Query Table
"""

from __future__ import annotations

import pkgutil
import warnings
from enum import Enum
from io import StringIO
from typing import Callable

import pandas as pd
from dateutil import parser

from lhcsmapi.metadata.MappingMetadata import MappingMetadata
from lhcsmapi.Time import Time, TimeAlias

_UNKNOWN_MP3_PERIOD = "Unknown Period"
_FIRST_HWC_START = "2007-12-06 00:00:00"


# The following dates are taken from AccTesting, see https://its.cern.ch/jira/browse/SIGMON-651
_LS1_END = "2014-07-01 00:00:00"
_LS2_END = "2020-11-01 00:00:00"
_ADDITIONAL_POST_LS2_THERMOCYCLE = "2021-08-15 00:00:00"


def get_quench_database_columns(circuit_type: str, table_type: str = "MP3") -> list[str]:
    """Method returning columns of an MP3 quench database Excel file

    :param circuit_type: circuit type
    :param table_type: table type (either MP3 or LHCSM)

    :return: pd.DataFrame with names of columns for an MP3 quench database Excel file
    """

    return _read_csv(f"mp3/{circuit_type}_{table_type}_Quench_Database_Columns.csv", index_col=0).columns


def get_quench_heater_reference_discharge(circuit_type: str, cell_or_circuit: str, timestamp: int) -> int:
    """Method returning the closest timestamp corresponding to a reference quench heater discharge

    :param circuit_type:
    :param cell_or_circuit: name of a magnet cell
    :param timestamp: timestamp of a QH discharge for which a reference is searched

    :return: int in a nanosecond precision unix timestamp of a reference QH timestamp
    """

    if circuit_type in ["RB", "RQ"]:
        col_name = "Cell"
    elif circuit_type in ["IT", "IPD2", "IPD2_B1B2", "IPQ2", "IPQ4", "IPQ8"]:
        col_name = "Circuit"
    else:
        raise ValueError(f"Circuit type {circuit_type} not supported!")

    qh_ref_discharges_df = _read_csv(f"qh/{circuit_type}_Reference_Discharges.csv")
    qh_ref_matching_df = qh_ref_discharges_df[qh_ref_discharges_df[col_name] == cell_or_circuit]

    return _get_latest_ref(qh_ref_matching_df, timestamp, "QH discharge")


def get_quench_heaters_reference_features(circuit_type: str, nominal_voltage: int = 900) -> pd.DataFrame:
    """Method reading a pandas DataFrame with reference values of QH features
    For all circuits:

    - initial voltage
    - final voltage

    :param circuit_type: circuit type
    :param nominal_voltage: nominal QH voltage (300, 900); 900 by default

    :return: pd.DataFrame with reference QH features
    """
    reference_features_df = _read_csv("qh/Reference_Features.csv", index_col=0)

    return reference_features_df[
        (reference_features_df["circuit_type"] == circuit_type)
        & (reference_features_df["nominal_voltage"] == nominal_voltage)
    ][["min", "max"]]


def get_quench_heaters_reference_differences(circuit_type: str, nominal_voltage: int = 900) -> pd.DataFrame:
    """Method reading a pandas DataFrame with allowed differences of QH features from features calculated with
    the reference discharges.
    For all circuits (with QH voltage measurement):

    - voltage time constant

    For circuits with QH current measurement:

    - current time constant
    - initial resistance value

    :param circuit_type: circuit type
    :param nominal_voltage: nominal QH voltage (300, 900); 900 by default

    :return: pd.DataFrame with allowed differences of QH features from features calculated with the reference
    discharges.
    """
    reference_diff_df = _read_csv("qh/Reference_Differences.csv", index_col=0)
    return pd.DataFrame(
        reference_diff_df[
            (reference_diff_df["circuit_type"] == circuit_type)
            & (reference_diff_df["nominal_voltage"] == nominal_voltage)
        ]["diff"]
    )


def get_energy_extraction_reference_features(
    circuit_type: str, timestamp_fgc: str = "2021-01-01 00:00:00.000+01:00"
) -> pd.DataFrame:
    """Method returning a pandas DataFrame with min/max reference EE features
    RB:
    - R_1
    - R_2
    - t_delay_1
    - t_delay_2
    - tau_u_dump_res_1
    - tau_u_dump_res_2

    RQ:
    - R
    - t_delay
    - tau_u_dump_res

    :param circuit_type: circuit type
    :param timestamp_fgc: FGC timestamp needed to select an appropriate EE reference features

    :return: pd.DataFrame with reference EE features
    """

    if circuit_type == "RQ":
        return _read_csv("ee/RQ_Reference_Features.csv", index_col=0)
    elif circuit_type == "RB":
        date_change = parser.parse("2018-01-01 00:00:00.000+01:00")
        timestamp_fgc_datetime = Time.to_datetime(timestamp_fgc)

        if timestamp_fgc_datetime > date_change:
            return _read_csv("ee/RB_Reference_Features_2018_01_01.csv", index_col=0)
        else:
            return _read_csv("ee/RB_Reference_Features_2014_01_01_2018_01_01.csv", index_col=0)
    else:
        raise KeyError(f"Circuit type {circuit_type} not supported!")


def get_power_converter_reference_features(circuit_type: str) -> pd.DataFrame:
    """Method returning a pandas DataFrame with min/max reference features of a Power Converter:
    - tau_i_meas

    :param circuit_type: circuit type

    :return: pd.DataFrame with reference Power Converter features
    """
    if circuit_type not in ["RB", "RQ"]:
        raise ValueError(f"Reference features for {circuit_type} circuit type are not defined.")

    return _read_csv(f"pc/{circuit_type}_Reference_Features.csv", index_col=0)


def get_power_converter_reference_fpa(circuit_type: str, circuit_name: str, col: str, timestamp: int) -> int:
    """Method returning a timestamp of a reference FGC/EE timestamp for a PM event

    :param circuit_type: circuit type
    :param circuit_name: circuit name
    :param col: column: RB: fgcPm, eeEvenPm, eeOddPm, RQ: fgcPm, eePm
    :param timestamp: timestamp for which a reference is searched

    :return: int in a nanosecond precision unix timestamp of a reference QH FGC/EE timestamp
    """
    if circuit_type == "RB":
        ref_fpa_df = _read_csv("pc/RB_Reference_PNO_b2.csv")
    elif circuit_type == "RQ":
        ref_fpa_df = _read_csv("pc/RQ_Reference_PNO_b3.csv")
    else:
        raise ValueError(f"Circuit type {circuit_type} not supported!")

    ref_fpa_df = ref_fpa_df[(ref_fpa_df["circuit"] == circuit_name)]
    ref_fpa_df["Timestamp"] = ref_fpa_df[col].apply(Time.to_unix_timestamp)
    return _get_latest_ref(ref_fpa_df, timestamp, col)


def get_mp3_period(date_time) -> str:
    """Method returning an MP3 period name based on date and time

    :param date_time: date time for which MP3 period name is requested
    :return: method returns a string with MP3 period name
    """
    date_time = Time.to_string_short(date_time)
    periods: pd.DataFrame = _read_csv("mp3/HWC_Operation_YETS_Periods.csv")
    candidates = periods[(date_time <= periods["date_end"]) & (date_time > periods["date_start"])]["Period"].values
    return candidates[0] if len(candidates) == 1 else _UNKNOWN_MP3_PERIOD


def get_rq_circuit_parameters() -> pd.DataFrame:
    """Method returning RQ circuit parameters

    :return: a pd.DataFrame with RQ circuit parameters
    """
    return _read_csv("circuit/RQ_parameters.csv")


def _read_csv(file: str, index_col=None) -> pd.DataFrame:
    data = pkgutil.get_data("lhcsmapi.resources.reference", file).decode("utf-8")
    return pd.read_csv(StringIO(data), index_col=index_col)


def _get_latest_ref(df: pd.DataFrame, timestamp: int, name: str) -> int:
    df = df.sort_values(by="Timestamp")

    if df["Timestamp"].values[0] >= timestamp:
        warnings.warn(f"The given is older than timestamp reference {name}!")
        return df["Timestamp"].values[0]
    else:
        # find indices of rows with timestamp before the given timestamp
        # take the last found row
        return df[df["Timestamp"] < timestamp]["Timestamp"].values[-1]


def _get_last(relevant_tests: pd.DataFrame, t_start: str, *args) -> pd.Series | None:
    sorted_tests = relevant_tests[(relevant_tests["executionStartTime"] < t_start)].sort_values("executionStartTime")
    if sorted_tests.empty:
        return None
    return sorted_tests.iloc[-1]


def _get_first_after(point_in_time: str, relevant_tests: pd.DataFrame, t_start: str):
    tests_after_ls = relevant_tests[
        (relevant_tests["executionStartTime"] > point_in_time) & (relevant_tests["executionStartTime"] < t_start)
    ].sort_values(by="executionStartTime")
    if tests_after_ls.empty:
        return None
    return tests_after_ls.iloc[0]


def _get_first_after_ls(relevant_tests: pd.DataFrame, t_start: str, *args) -> pd.Series | None:
    ls_end = _get_end_of_previous_ls(t_start)
    return _get_first_after(ls_end, relevant_tests, t_start)


def _get_first_after_thermocycle(relevant_tests: pd.DataFrame, t_start: str, circuit_name: str) -> pd.Series | None:
    if (
        MappingMetadata.get_sector_name_for_circuit_name(circuit_name) in ["S23", "S78"]
        and t_start >= _ADDITIONAL_POST_LS2_THERMOCYCLE
        and _get_end_of_previous_ls(t_start) <= _ADDITIONAL_POST_LS2_THERMOCYCLE
    ):
        return _get_first_after(_ADDITIONAL_POST_LS2_THERMOCYCLE, relevant_tests, t_start)

    return _get_first_after_ls(relevant_tests, t_start)


class ReferenceTestSelectionStrategy(Enum):

    LAST = "LAST"
    FIRST_AFTER_LS = "FIRST_AFTER_LS"
    FIRST_AFTER_THERMOCYCLE = "FIRST_AFTER_THERMOCYCLE"


_STRATEGIES: dict[ReferenceTestSelectionStrategy, Callable[[pd.DataFrame, str, str], pd.Series | None]] = {
    ReferenceTestSelectionStrategy.LAST: _get_last,
    ReferenceTestSelectionStrategy.FIRST_AFTER_LS: _get_first_after_ls,
    ReferenceTestSelectionStrategy.FIRST_AFTER_THERMOCYCLE: _get_first_after_thermocycle,
}


def get_reference_test(
    hwc_summary: pd.DataFrame,
    circuit_name: str,
    hwc_test: str,
    t_start: TimeAlias,
    strategy: ReferenceTestSelectionStrategy = ReferenceTestSelectionStrategy.LAST,
) -> tuple[str, str] | tuple[None, None]:
    """Returns the reference tests according to the provided policy
    Args:
        hwc_summary_path: path to the file with the summary of the past hwc tests
        circuit_name: name of the circuit
        hwc_test: name of the HWC test
        t_start: start time of the test
        strategy: reference test selection strategy
    """
    relevant_tests_df = hwc_summary[
        (hwc_summary["systemName"].str.contains(circuit_name))
        & (hwc_summary["testName"].str.contains(hwc_test))
        & (hwc_summary["executedTestStatus"] == "SUCCESSFUL")
    ]
    t_start = Time.to_string_short(t_start)

    reference_test = _STRATEGIES[strategy](relevant_tests_df, t_start, circuit_name)
    if reference_test is None:
        raise KeyError(
            f"No reference test according to the strategy {strategy} "
            f"for given circuit name {circuit_name}, hwc test {hwc_test} and start time {t_start}"
        )

    return reference_test["executionStartTime"], reference_test["executionEndTime"]


def _get_end_of_previous_ls(t_start) -> str:
    """Returns a string with the end date of the most recent LS prior the give timestamp."""
    if t_start > _LS2_END:
        return _LS2_END
    if t_start > _LS1_END:
        return _LS1_END
    return _FIRST_HWC_START
