from typing import List

from lhcsmapi.api.query_builder import QueryBuilder
from lhcsmapi.gui import pc
from lhcsmapi.gui.pc import find
from lhcsmapi.metadata import signal_metadata


class R60AFgcPmFindButtonBaseModule(find.FgcPmFindButtonBaseModule):
    """A class with methods to search for 60A FGC PM events to fill-in a search field in the FGC PM search module."""

    def _find_fgc_pm_events_with_qps(self, start_time, end_time, circuit_name):
        circuit_names_all = signal_metadata.get_circuit_names("60A")
        circuit_names = list(filter(lambda x: circuit_name in x, circuit_names_all))
        return query_pm_convert_pc_to_circuit_name(start_time, end_time, circuit_names)


def query_pm_convert_pc_to_circuit_name(start_time, end_time, circuit_names: List[str]) -> List[pc.Fgc]:
    """Method querying FGC PM source and timestamp tuple and taking only those corresponding to the given circuits

    :param start_time: start time for a query
    :param end_time: end time for a query
    :param circuit_names: list of circuit names for which
    :return: a list of PM source and timestamp tuples corresponding to circuit names
    """
    # Query PM for the specified period of time
    source_timestamp_df = (
        QueryBuilder()
        .with_pm()
        .with_duration(t_start=start_time, t_end=end_time)
        .with_query_parameters(system="FGC", className="lhc_self_pmd", source="*")
        .event_query()
        .keep_sources_for_circuit_names("60A", circuit_names, "PC", start_time)
        .get_dataframe()
    )

    # Convert PC names to circuit names
    fgc_df = find.convert_source_timestamp_to_circuit_name_timestamp(source_timestamp_df)
    return [pc.Fgc(el[0], el[1]) for el in fgc_df]
