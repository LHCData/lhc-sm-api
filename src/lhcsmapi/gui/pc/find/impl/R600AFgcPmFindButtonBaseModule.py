from typing import List, Tuple

from lhcsmapi.api.query_builder import QueryBuilder
from lhcsmapi.gui import pc
from lhcsmapi.gui.pc import find
from lhcsmapi.metadata import signal_metadata


class R600AFgcPmFindButtonBaseModule(find.FgcPmFindButtonBaseModule):
    """A class with methods to search for 600A FGC PM events to fill-in a search field in the FGC PM search module."""

    def _find_fgc_pm_events_with_qps(self, start_time, end_time, circuit_name: str) -> List[pc.Fgc]:
        circuit_names_all = signal_metadata.get_circuit_names("600A")
        circuit_names = filter(lambda x: circuit_name in x, circuit_names_all)
        return query_pm_convert_pc_to_circuit_name(start_time, end_time, list(circuit_names))


class R600ARcdoFgcPmFindButtonBaseModule(find.FgcPmFindButtonBaseModule):
    """A class with methods to search for RCD-RCO FGC PM events to fill-in a search field in the FGC PM search module."""

    def _find_fgc_pm_events_with_qps(self, start_time, end_time, circuit_name: str) -> List[pc.RcdoFgc]:
        def empty_coupled(fgc, old_prefix, new_prefix):
            return pc.Fgc(fgc.circuit_name.replace(old_prefix, new_prefix), float("nan"))

        def build_rcdo_fgc(t):
            if isinstance(t, tuple):
                rcd, rco = t if "RCD" in t[0].circuit_name else (t[1], t[0])
            else:
                rcd = t if "RCD" in t.circuit_name else empty_coupled(t, "RCO", "RCD")
                rco = t if "RCO" in t.circuit_name else empty_coupled(t, "RCD", "RCO")
            return pc.RcdoFgc(rcd, rco)

        circuit_names = [circuit_name.replace("RC", "RCD"), circuit_name.replace("RC", "RCO")]
        output = query_pm_convert_pc_to_circuit_name(start_time, end_time, circuit_names)
        combined = combine_coupled_fgc_pm_entries(output, circuit_prefixes=["RCD", "RCO"])
        return [build_rcdo_fgc(pm) for pm in combined]


class R600ARcbxhvFgcPmFindButtonBaseModule(find.FgcPmFindButtonBaseModule):
    """A class with methods to search for RCBX FGC PM events to fill-in a search field in the FGC PM search module."""

    def _find_fgc_pm_events_with_qps(self, start_time, end_time, circuit_name: str) -> List[pc.RcbxhvFgc]:
        def empty_coupled(fgc, old_prefix, new_prefix):
            return pc.Fgc(fgc.circuit_name.replace(old_prefix, new_prefix), float("nan"))

        def build_rcbxhv_fgc(t):
            if isinstance(t, tuple):
                rcbxh, rcbxv = t if "RCBXH" in t[0].circuit_name else (t[1], t[0])
            else:
                rcbxh = t if "RCBXH" in t.circuit_name else empty_coupled(t, "RCBXV", "RCBXH")
                rcbxv = t if "RCBXV" in t.circuit_name else empty_coupled(t, "RCBXH", "RCBXV")
            return pc.RcbxhvFgc(rcbxh, rcbxv)

        circuit_names = [circuit_name.replace("RCBX", "RCBXH"), circuit_name.replace("RCBX", "RCBXV")]
        output = query_pm_convert_pc_to_circuit_name(start_time, end_time, circuit_names)
        combined = combine_coupled_fgc_pm_entries(output, circuit_prefixes=["RCBXH", "RCBXV"])
        return [build_rcbxhv_fgc(pm) for pm in combined]


def combine_coupled_fgc_pm_entries(
    fgc_pm_entries: List[pc.Fgc], circuit_prefixes: List[str]
) -> List[Tuple[pc.Fgc, ...]]:
    """Function combining coupled FGC pm entries into a single list of tuples

    :param fgc_pm_entries: a list of FGC PM source and timestamp tuples
    :param circuit_prefixes: a list of circuit prefixes
    :return: a list of source and timestamp tuples
    """
    out_pm_entries = []
    for fgc_pm_entry in fgc_pm_entries:
        circuit_name = fgc_pm_entry.circuit_name
        timestamp = fgc_pm_entry.timestamp
        # find coupled circuit name
        if circuit_prefixes[0] in circuit_name:
            coupled_circuit_name = circuit_name.replace(circuit_prefixes[0], circuit_prefixes[1])
        else:
            coupled_circuit_name = circuit_name.replace(circuit_prefixes[1], circuit_prefixes[0])

        # find coupled circuit name
        indices_coupled = [i for i, fgc in enumerate(fgc_pm_entries) if fgc.circuit_name == coupled_circuit_name]

        # check if timestamp is within 2 s
        found_index = -1
        for index_coupled in indices_coupled:
            if abs(timestamp - fgc_pm_entries[index_coupled].timestamp) <= 2e9:
                found_index = index_coupled

        if found_index == -1:
            out_pm_entries.append(fgc_pm_entry)
        else:
            out_pm_entries.append((fgc_pm_entry, fgc_pm_entries[found_index]))
            fgc_pm_entries.pop(found_index)

    return out_pm_entries


def query_pm_convert_pc_to_circuit_name(start_time, end_time, circuit_names: List[str]) -> List[pc.Fgc]:
    """Method querying FGC PM source and timestamp tuple and taking only those corresponding to the given circuits

    :param start_time: start time for a query
    :param end_time: end time for a query
    :param circuit_names: list of circuit names for which
    :return: a list of PM source and timestamp tuples corresponding to circuit names
    """
    # Query PM for the specified period of time

    metadata = signal_metadata.get_signal_metadata("600A", circuit_names[0], "PC", "PM", start_time)
    source_timestamp_df = (
        QueryBuilder()
        .with_pm()
        .with_duration(t_start=start_time, t_end=end_time)
        .with_query_parameters(system="FGC", className=metadata["className"], source="*")
        .event_query()
        .keep_sources_for_circuit_names("600A", circuit_names, "PC", start_time)
        .get_dataframe()
    )

    # Convert PC names to circuit names
    circuit_name_timestamp = find.convert_source_timestamp_to_circuit_name_timestamp(source_timestamp_df)
    return [pc.Fgc(el[0], el[1]) for el in circuit_name_timestamp]
