from typing import List

from lhcsmapi.api.query_builder import QueryBuilder
from lhcsmapi.gui import pc
from lhcsmapi.gui.pc import find
from lhcsmapi.metadata import signal_metadata


class IpdFgcPmFindButtonBaseModule(find.FgcPmFindButtonBaseModule):
    """A class with methods to search for IPD FGC PM events to fill-in a search field in the FGC PM search module."""

    def _find_fgc_pm_events_with_qps(self, start_time, end_time, circuit_name) -> List[pc.FgcBase]:
        return query_pm_convert_pc_to_circuit_name(start_time, end_time, circuit_name)


def query_pm_convert_pc_to_circuit_name(start_time, end_time, circuit_name: str) -> List[pc.Fgc]:
    """Method querying FGC PM source and timestamp tuple and taking only those corresponding to the given circuits

    :param start_time: start time for a query
    :param end_time: end time for a query
    :param circuit_name: circuit name
    :return: a list of PM source and timestamp tuples corresponding to circuit names
    """
    circuit_type = signal_metadata.get_circuit_type_for_circuit_name(circuit_name)

    # Query PM for the specified period of tim
    source_timestamp_df = (
        QueryBuilder()
        .with_pm()
        .with_duration(t_start=start_time, t_end=end_time)
        .with_circuit_type(circuit_type)
        .with_metadata(circuit_name=circuit_name, system="PC", source="*")
        .event_query()
        .drop_duplicates("timestamp")
        .get_dataframe()
    )

    if source_timestamp_df.empty:
        return []

    # Use circuit name instead of power converter name
    return [pc.Fgc(circuit_name, t) for t in source_timestamp_df["timestamp"].values]
