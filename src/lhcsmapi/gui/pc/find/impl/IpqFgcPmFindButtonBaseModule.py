from typing import List

from lhcsmapi.api.query_builder import QueryBuilder
from lhcsmapi.gui import pc
from lhcsmapi.gui.pc import find
from lhcsmapi.metadata import signal_metadata


class IpqFgcPmFindButtonBaseModule(find.FgcPmFindButtonBaseModule):
    """A class with methods to search for IPQ FGC PM events to fill-in a search field in the FGC PM search module."""

    def _find_fgc_pm_events_with_qps(self, start_time, end_time, circuit_name: str) -> List[pc.Fgc]:
        return query_pm_convert_pc_to_circuit_name(start_time, end_time, circuit_name)


def query_pm_convert_pc_to_circuit_name(start_time, end_time, circuit_name: str) -> List[pc.Fgc]:
    """Method querying FGC PM source and timestamp tuple and taking only those corresponding to the given circuits

    :param start_time: start time for a query
    :param end_time: end time for a query
    :param circuit_name: circuit name
    :return: a list of PM source and timestamp tuples corresponding to circuit names
    """
    # It is assumed that all IPQ circuits have the same FGC PC className
    metadata = signal_metadata.get_signal_metadata("IPQ2", "RQ5.R5", "PC", "PM", start_time)
    source_timestamp_df = (
        QueryBuilder()
        .with_pm()
        .with_duration(t_start=start_time, t_end=end_time)
        .with_query_parameters(system="FGC", className=metadata["className"], source="*")
        .event_query()
        .get_dataframe()
    )

    if source_timestamp_df.empty:
        return []

    pc_names = signal_metadata.get_pc_names(["IPQ2", "IPQ4", "IPQ8"], None, start_time)
    source_timestamp_df = source_timestamp_df[source_timestamp_df["source"].isin(pc_names)]
    source_timestamp_df = source_timestamp_df[source_timestamp_df["source"].str.contains(circuit_name)]

    source_timestamp_df["source_without_beam"] = source_timestamp_df["source"].apply(lambda source: source[:-2])
    source_timestamp_df = source_timestamp_df.drop_duplicates(["timestamp", "source_without_beam"])

    def create_fgc(row):
        pc_name_split = row["source"].split(".")
        full_circuit_name = f"{pc_name_split[-2]}.{pc_name_split[-1][0:2]}"
        return pc.Fgc(full_circuit_name, row["timestamp"])

    return source_timestamp_df.apply(create_fgc, axis=1).tolist()
