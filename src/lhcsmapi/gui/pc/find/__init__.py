from lhcsmapi.gui.pc.find.base import convert_source_timestamp_to_circuit_name_timestamp
from lhcsmapi.gui.pc.find.base import (
    FgcPmFindButtonBaseModule,
    RbFgcPmFindButtonBaseModule,
    RqFgcPmFindButtonBaseModule,
)
from lhcsmapi.gui.pc.find.impl.IpdFgcPmFindButtonBaseModule import IpdFgcPmFindButtonBaseModule
from lhcsmapi.gui.pc.find.impl.IpqFgcPmFindButtonBaseModule import IpqFgcPmFindButtonBaseModule
from lhcsmapi.gui.pc.find.impl.ItFgcPmFindButtonBaseModule import ItFgcPmFindButtonBaseModule
from lhcsmapi.gui.pc.find.impl.R600AFgcPmFindButtonBaseModule import (
    R600AFgcPmFindButtonBaseModule,
    R600ARcbxhvFgcPmFindButtonBaseModule,
    R600ARcdoFgcPmFindButtonBaseModule,
)
from lhcsmapi.gui.pc.find.impl.R60AFgcPmFindButtonBaseModule import R60AFgcPmFindButtonBaseModule
from lhcsmapi.gui.pc.find.impl.R80_120AFgcPmFindButtonBaseModule import R80_120AFgcPmFindButtonBaseModule


def get_module_for_circuit_type(circuit_type: str):
    """Method creating a FgcPmFindButtonBaseModule object for a FGC PM query of a given circuit type

    :param circuit_type: circuit type to initiate a FgcPmFindButtonBaseModule object
    :return: a FgcPmFindButtonBaseModule object for a given circuit type
    """
    if circuit_type == "RB":
        return RbFgcPmFindButtonBaseModule(circuit_type)
    elif circuit_type == "RQ":
        return RqFgcPmFindButtonBaseModule(circuit_type)
    elif circuit_type == "IT":
        return ItFgcPmFindButtonBaseModule(circuit_type)
    elif circuit_type == "IPQ":
        return IpqFgcPmFindButtonBaseModule(circuit_type)
    elif circuit_type == "IPD":
        return IpdFgcPmFindButtonBaseModule(circuit_type)
    elif circuit_type == "600A":
        return R600AFgcPmFindButtonBaseModule(circuit_type)
    elif circuit_type == "600A_RCDO":
        return R600ARcdoFgcPmFindButtonBaseModule(circuit_type)
    elif circuit_type == "600A_RCBXHV":
        return R600ARcbxhvFgcPmFindButtonBaseModule(circuit_type)
    elif circuit_type == "80-120A":
        return R80_120AFgcPmFindButtonBaseModule(circuit_type)
    elif circuit_type == "60A":
        return R60AFgcPmFindButtonBaseModule(circuit_type)
    else:
        raise ValueError(f"Circuit type {circuit_type} not supported!")
