import functools
from abc import ABC, abstractmethod
from typing import List

from ipywidgets import Button, IntProgress, Label, Layout

from lhcsmapi.gui import pc
from lhcsmapi.gui.BaseModule import BaseModule
from lhcsmapi.metadata import signal_metadata
from lhcsmapi.pyedsl.dbsignal.post_mortem.PmDbRequest import PmDbRequest
from lhcsmapi.Time import Time


def convert_source_timestamp_to_circuit_name_timestamp(source_timestamp_df):
    output = []
    for _, row in source_timestamp_df.iterrows():
        pc_name = row["source"]
        pc_name_split = pc_name.split(".")
        circuit_name = f"{pc_name_split[-2]}.{pc_name_split[-1]}"
        output.append((circuit_name, row["timestamp"]))
    return output


class FgcPmFindButtonBaseModule(BaseModule, ABC):
    def __init__(self, circuit_type):
        super().__init__()
        self.circuit_type = circuit_type

        # Widget
        self.find_btn = Button(
            description="Find FGC PM entries",
            tooltip="Click to find FGC PM entries between indicated dates.",
            button_style="",  # 'success', 'info', 'warning', 'danger' or ''
        )
        self.find_btn.on_click(functools.partial(FgcPmFindButtonBaseModule.handle_find_button_event, self=self))

        # # Text field
        self.lbl_search = Label(value="Click button to search for FGC PM entries within the specified period of time")

        # # Progress bar
        self.progress_search = IntProgress(
            value=0,
            min=0,
            step=1,
            bar_style="success",
            orientation="horizontal",
            layout=Layout(width="20%", visibility="hidden"),
        )

        self.widget = [self.find_btn, self.lbl_search, self.progress_search]

    def handle_find_button_event(b, self):
        if self.mediator is not None:
            self.mediator.notify(self, pc.FgcBrowserAction.FIND_BUTTON_CLICKED)

    def find_fgc_pm_events_with_qps(self, start_time, end_time, circuit_name) -> List[pc.FgcBase]:
        """Method finding FGC PM entries with QPS for a given time range and circuit name

        :param start_time: start time for the query
        :param end_time: end time for the query
        :param circuit_name: circuit name
        :return: a list of FGC source and timestamp tuples
        """
        # IPD + IPQ + IT + 60A
        self.lbl_search.value = "Querying PM for FGC events"
        self.progress_search.layout.visibility = "visible"
        self.progress_search.max = 1
        self.progress_search.description = ""

        output = self._find_fgc_pm_events_with_qps(start_time, end_time, circuit_name)

        self.lbl_search.value = "Query completed"
        self.progress_search.layout.visibility = "visible"
        self.progress_search.value = 1
        self.progress_search.description = ""

        return output

    @abstractmethod
    def _find_fgc_pm_events_with_qps(self, start_time, end_time, circuit_name) -> List[pc.Fgc]:
        pass


class FgcPmFindButtonBaseModuleWithProgressBar(FgcPmFindButtonBaseModule, ABC):
    @abstractmethod
    def find_fgc_pm_events_with_qps(self, start_time, end_time, circuit_name) -> List[pc.Fgc]:
        pass

    def _find_fgc_pm_events_with_qps(self, start_time, end_time, circuit_name) -> List[pc.Fgc]:
        metadata_fgc = signal_metadata.get_signal_metadata(
            self.circuit_type, circuit_name, "PC", "PM", Time.to_unix_timestamp(start_time)
        )

        quotient, remainder = Time.modulo_day_in_unix_timestamp(start_time, end_time)
        event_duration_nsec = int(24 * 60 * 60 * 1e9)
        source_timestamp_fgc = []

        self.progress_search.layout.visibility = "visible"
        self.progress_search.max = quotient
        self.progress_search.description = f"Days: 0/{quotient}"

        for day in range(quotient):
            start_time_temp = Time.to_unix_timestamp(start_time) + int(day * event_duration_nsec)

            # Update GUI
            self.lbl_search.value = f"Querying PM for FGC entries on {Time.to_string(start_time_temp)}"

            source_timestamp_fgc += PmDbRequest.find_events(
                metadata_fgc["source"],
                metadata_fgc["system"],
                metadata_fgc["className"],
                t_start=start_time_temp,
                duration=[(event_duration_nsec, "ns")],
            )
            self.progress_search.description = f"Days: {day + 1}/{quotient}"
            self.progress_search.value = day + 1

        if remainder > 0:
            start_time_temp = Time.to_unix_timestamp(end_time) - remainder
            self.lbl_search.value = f"Querying PM for FGC entries on {Time.to_string(start_time_temp)}"
            source_timestamp_fgc += PmDbRequest.find_events(
                metadata_fgc["source"],
                metadata_fgc["system"],
                metadata_fgc["className"],
                t_start=start_time_temp,
                duration=[(remainder, "ns")],
            )

        return [pc.Fgc(circuit_name, el[1]) for el in source_timestamp_fgc]


class RbFgcPmFindButtonBaseModule(FgcPmFindButtonBaseModuleWithProgressBar):
    def find_fgc_pm_events_with_qps(self, start_time, end_time, circuit_name) -> List[pc.Fgc]:
        return self._find_fgc_pm_events_with_qps(start_time, end_time, circuit_name)


class RqFgcPmFindButtonBaseModule(FgcPmFindButtonBaseModule):
    """A class with methods to search for RQ FGC PM events to fill-in a search field in the FGC PM search module."""

    def _find_fgc_pm_events_with_qps(self, start_time, end_time, circuit_name) -> List[pc.Fgc]:
        metadata_fgc_rqd = signal_metadata.get_signal_metadata("RQ", circuit_name, "PC", "PM", start_time)

        quotient, remainder = Time.modulo_day_in_unix_timestamp(start_time, end_time)
        event_duration_nsec = int(24 * 60 * 60 * 1e9)
        source_timestamp_fgc = []

        self.progress_search.layout.visibility = "visible"
        self.progress_search.max = quotient
        self.progress_search.description = f"Days: 0/{quotient}"

        for day in range(quotient):
            start_time_temp = Time.to_unix_timestamp(start_time) + int(day * event_duration_nsec)

            # Update GUI
            self.lbl_search.value = f"Querying PM for FGC entries on {Time.to_string(start_time_temp)}"

            source_timestamp_fgc += PmDbRequest.find_events(
                metadata_fgc_rqd["source"],
                metadata_fgc_rqd["system"],
                metadata_fgc_rqd["className"],
                t_start=start_time_temp,
                duration=[(event_duration_nsec, "ns")],
            )
            self.progress_search.description = f"Days: {day + 1}/{quotient}"
            self.progress_search.value = day + 1

        if remainder > 0:
            start_time_temp = Time.to_unix_timestamp(end_time) - remainder
            self.lbl_search.value = f"Querying PM for FGC entries on {Time.to_string(start_time_temp)}"
            source_timestamp_fgc += PmDbRequest.find_events(
                metadata_fgc_rqd["source"],
                metadata_fgc_rqd["system"],
                metadata_fgc_rqd["className"],
                t_start=start_time_temp,
                duration=[(remainder, "ns")],
            )

        return [pc.Fgc(circuit_name, el[1]) for el in source_timestamp_fgc]

    def find_fgc_pm_events_with_qps(self, start_time, end_time, circuit_name: str) -> List[pc.RqFgc]:
        """Method finding FGC PM entries with QPS for a given time range and circuit name

        :param start_time: start time for the query
        :param end_time: end time for the query
        :param circuit_name: circuit name
        :return: a list of FGC source and timestamp tuples
        """
        # convert RQ.AXX into [RQD.AXX, RQF.AXX]
        circuit_names = [circuit_name.replace("RQ", rq_circuit_name) for rq_circuit_name in ["RQD", "RQF"]]

        source_timestamp_fgc = self._find_fgc_pm_events_with_qps(start_time, end_time, circuit_names[0])
        return self._keep_fgc_rqd_with_rqf("RQ", circuit_names, source_timestamp_fgc)

    def _keep_fgc_rqd_with_rqf(
        self, circuit_type: str, circuit_names: List[str], source_timestamp_fgc: List[pc.Fgc]
    ) -> List[pc.RqFgc]:
        """Method keeping FGC PM events with RQF

        :param circuit_type: circuit type
        :param circuit_names: List of RQ circuit names (RQD.AXY, RQF.AXY)
        :param source_timestamp_fgc: a list of tuples with source and timestamp
        :return: a list of tuples with source and timestamp that has an QPS PM event around an FGC as well as a
        corresponding RQF FGC timestamp
        """
        # Check if an RQD FGC timestamp has
        # # an RQF FGC timestamp
        source_timestamp_fgc_with_rqf = []

        for index, elem in enumerate(source_timestamp_fgc):
            metadata_fgc_rqf = signal_metadata.get_signal_metadata(
                circuit_type, circuit_names[1], "PC", "PM", elem.timestamp
            )
            # Update GUI
            self.lbl_search.value = (
                f"Checking if there are QPS PM around an FGC timestamp {Time.to_string(elem.timestamp)}"
            )
            self.progress_search.description = f"FGC: {index + 1}/{len(source_timestamp_fgc)}"
            self.progress_search.value = index + 1

            # Check if there is a corresponding RQF timestamp
            source_timestamp_fgc_rqf = PmDbRequest.find_events(
                metadata_fgc_rqf["source"],
                metadata_fgc_rqf["system"],
                metadata_fgc_rqf["className"],
                t_start=elem.timestamp,
                duration=[(1, "s"), (1, "s")],
            )
            if source_timestamp_fgc_rqf:
                source_timestamp_fgc_with_rqf.append(
                    pc.RqFgc(
                        pc.Fgc(circuit_names[0], elem.timestamp),
                        pc.Fgc(circuit_names[1], source_timestamp_fgc_rqf[0][1]),
                    )
                )

        self.lbl_search.value = "Query completed."

        return source_timestamp_fgc_with_rqf
