import functools
from typing import List

from ipywidgets import Dropdown

from lhcsmapi.gui import pc
from lhcsmapi.gui.BaseModule import BaseModule
from lhcsmapi.metadata import signal_metadata


class FgcPmCircuitNameDropdownBaseModule(BaseModule):
    def __init__(self, circuit_type):
        super().__init__()

        self.circuit_type = circuit_type

        # Widget
        circuit_names = get_circuit_names_or_prefixes_to_display(circuit_type)
        description = "Circuit name prefix:" if circuit_type in ["600A", "IPQ", "80-120A", "60A"] else "Circuit name:"

        self.select_circuit_name_or_prefix_dropdown = Dropdown(
            options=circuit_names, description=description, disabled=False
        )
        # On select value change, update output
        self.select_circuit_name_or_prefix_dropdown.observe(
            functools.partial(FgcPmCircuitNameDropdownBaseModule.handle_circuit_name_dropdown_change, self=self)
        )

        self.widget = self.select_circuit_name_or_prefix_dropdown

    @staticmethod
    def handle_circuit_name_dropdown_change(_, self):
        self.mediator.notify(self, pc.FgcBrowserAction.CIRCUIT_NAME_DROPDOWN_CHANGE)

    def get_circuit_name_or_family(self):
        return self.select_circuit_name_or_prefix_dropdown.value


def get_circuit_names_or_prefixes_to_display(circuit_type: str):
    """Function returning a list of circuit names or types if the list of names is very long
    Depending on the circuit type the list is modified accordingly:
    - independent circuits: RB, IT, IPD - full list of names
    - coupled circuits: RQ, 600A_RCBXHV, 600A_RCDO - list of reduced names to avoid duplications,
    e.g., RQ.A12 instead of RQD.A12 and RQF.A12
    - multiple circuits: 600A - family types in order to avoid very long lists,
    e.g. RU instead of RU.L4

    :param circuit_type: circuit type such as RB, RQ, 600A, IT, IPD, IPQ
    :return: list of circuit names or types
    """
    if circuit_type in ["IPD"]:
        return signal_metadata.get_circuit_names(["IPD2", "IPD2_B1B2"])
    elif circuit_type in ["RB", "IT"]:
        return signal_metadata.get_circuit_names(circuit_type)
    elif circuit_type == "60A":
        return _get_prefixes(signal_metadata.get_circuit_names("60A"))
    elif circuit_type == "80-120A":
        names = signal_metadata.get_circuit_names("80-120A")
        return _get_prefixes([signal_metadata.get_family_name_for_80_120A(name) for name in names])
    elif circuit_type == "IPQ":
        return _get_prefixes(signal_metadata.get_circuit_names(["IPQ2", "IPQ4", "IPQ8"]))
    elif circuit_type == "RQ":
        names = signal_metadata.get_circuit_names("RQ")
        return _filter_and_replace(names, "RQD", "RQ")
    elif circuit_type == "600A":
        names = signal_metadata.get_circuit_names("600A")
        return _get_prefixes([name for name in names if not any(p in name for p in ["RCBX", "RCD", "RCO"])])
    elif circuit_type == "600A_RCBXHV":
        names = signal_metadata.get_circuit_names("600A")
        return _filter_and_replace(names, "RCBXH", "RCBX")
    elif circuit_type == "600A_RCDO":
        names = signal_metadata.get_circuit_names("600A")
        return _filter_and_replace(names, "RCD", "RC")
    else:
        raise KeyError(f"Circuit type {circuit_type} is not supported")


def _get_prefixes(circuit_names):
    return sorted(list({name.split(".")[0] for name in circuit_names}))


def _filter_and_replace(circuit_names: List[str], old: str, new: str) -> List[str]:
    return sorted(list({name.replace(old, new) for name in circuit_names if old in name}))
