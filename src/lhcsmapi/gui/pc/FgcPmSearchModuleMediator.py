from ipywidgets import HBox, VBox
from IPython.display import display, clear_output

from lhcsmapi.gui.AnalysisModeDoneByModule import AnalysisModeDoneByModule
from lhcsmapi.gui.pc.FgcPmCircuitNameDropdownBaseModule import FgcPmCircuitNameDropdownBaseModule
from lhcsmapi.gui.pc import select, find
from lhcsmapi.gui.ModuleMediator import ModuleMediator
from lhcsmapi.gui import pc
from lhcsmapi.gui.DateTimeBaseModule import DateTimeBaseModule


class FgcPmSearchModuleMediator(ModuleMediator):
    def __init__(self, date_time_module: DateTimeBaseModule, circuit_type="RB"):
        self.circuit_type = circuit_type

        # First row - circuit name or family select + start and end time selection
        self.fgc_circuit_name_family_select = FgcPmCircuitNameDropdownBaseModule(circuit_type)
        self.fgc_circuit_name_family_select.mediator = self

        self.date_time_module = date_time_module
        self.date_time_module.mediator = self

        first_row = HBox(children=[self.fgc_circuit_name_family_select.widget, self.date_time_module.widget])

        # Second row - analysis mode button + done by text
        self.analysis_mode_done_by_module = AnalysisModeDoneByModule()

        second_row = HBox(children=self.analysis_mode_done_by_module.widget)

        # Third row - find button + progress text display
        self.fgc_pm_find_button = find.get_module_for_circuit_type(circuit_type)
        self.fgc_pm_find_button.mediator = self

        third_row = HBox(children=self.fgc_pm_find_button.widget)

        # Fourth row - FGC PM event Select
        self.fgc_pm_event_select = select.get_module_for_circuit_type(circuit_type)
        self.fgc_pm_event_select.mediator = self

        fourth_row = self.fgc_pm_event_select.widget

        # Final GUI
        self.widget = VBox(children=[first_row, second_row, third_row, fourth_row])

        display(self.widget)

    def notify(self, sender: object, event: str) -> None:
        clear_output(wait=True)
        display(self.widget)

        start_date_time = self.date_time_module.get_start_date_time()
        end_date_time = self.date_time_module.get_end_date_time()
        circuit_name_or_family = self.get_circuit_name_or_family()

        if DateTimeBaseModule.check_date_time(start_date_time, end_date_time):
            if event == pc.FgcBrowserAction.FIND_BUTTON_CLICKED:
                self._query_fgc_pm_events_update_widget(circuit_name_or_family, end_date_time, start_date_time)
            elif event in [pc.FgcBrowserAction.EVENT_SELECTION_CHANGE, pc.FgcBrowserAction.TIME_RANGE_CHANGE]:
                print(f'The analysis is executed in {"Automatic" if self.is_automatic_mode() else "Manual"} mode.')
                self.fgc_pm_event_select.update_event_selection()
            elif event == pc.FgcBrowserAction.CIRCUIT_NAME_DROPDOWN_CHANGE:
                self._update_circuit_name_selection()
            else:
                raise KeyError(f"Action {event} not supported!")

    def _query_fgc_pm_events_update_widget(self, circuit_name, end_date_time, start_date_time):
        self.fgc_pm_event_select.fgc_pm_events = []
        self.fgc_pm_event_select.fgc_pm_event_sel.options = []
        fgc_pm_events = self.fgc_pm_find_button.find_fgc_pm_events_with_qps(
            start_date_time, end_date_time, circuit_name
        )

        if fgc_pm_events:
            self.fgc_pm_event_select.fgc_pm_events = fgc_pm_events
            options = self.fgc_pm_event_select.convert_fgc_pm_events_to_options(self.fgc_pm_event_select.fgc_pm_events)
            self.fgc_pm_event_select.fgc_pm_event_sel.options = options
        else:
            print(f"No FGC PM events for {circuit_name} from {start_date_time} to {end_date_time}")

    def _update_circuit_name_selection(self):
        self.fgc_pm_event_select.fgc_pm_events = []
        self.fgc_pm_event_select.fgc_pm_event_sel.options = []

    def get_circuit_type(self):
        return self.circuit_type

    def get_circuit_name_or_family(self):
        return self.fgc_circuit_name_family_select.get_circuit_name_or_family()

    def is_automatic_mode(self):
        return self.analysis_mode_done_by_module.is_automatic_mode()

    def get_author(self):
        return self.analysis_mode_done_by_module.get_author()

    def get_fgc_timestamp(self):
        return self.fgc_pm_event_select.get_selected_timestamp()

    def get_fgc_circuit(self):
        return self.fgc_pm_event_select.get_selected_circuit_name()
