"""Contains base module classes for searching of FGC PM events."""

import functools
import math
from typing import List, Tuple

import numpy as np
import pandas as pd
from IPython.display import display, Image
from ipywidgets import HBox, HTML, Label, Layout, Select, Text, VBox

from lhcsmapi.api import query, resolver
from lhcsmapi.gui import pc
from lhcsmapi.gui.BaseModule import BaseModule
from lhcsmapi.gui.pc import Fgc, FgcCompound
from lhcsmapi.metadata import signal_metadata
from lhcsmapi.metadata.MappingMetadata import MappingMetadata
from lhcsmapi.Time import Time


def get_module_for_circuit_type(circuit_type: str) -> "FgcPmEventSelectBaseModule":
    """Method creating a FgcPmEventSelectBaseModule object for a FGC PM query of a given circuit type

    :param circuit_type: circuit type to initiate a FgcPmEventSelectBaseModule object
    :return: a FgcPmEventSelectBaseModule object for a given circuit type
    """

    if circuit_type == "IPD":
        return IpdFgcPmEventSelectBaseModule(circuit_type, _get_short_description)
    elif circuit_type == "IPQ":
        return IpqFgcPmEventSelectBaseModule(circuit_type, _get_long_description)
    elif circuit_type in ["RQ", "600A_RCDO", "600A_RCBXHV"]:
        return FgcPmEventSelectBaseModule(circuit_type, _get_long_description_compound)
    elif circuit_type in ["600A", "80-120A", "60A"]:
        return FgcPmEventSelectBaseModule(circuit_type, _get_long_description)
    elif circuit_type in ["RB", "IT"]:
        return FgcPmEventSelectBaseModule(circuit_type, _get_short_description)
    else:
        raise ValueError(f"Circuit type {circuit_type} not supported!")


class FgcPmEventSelectBaseModule(BaseModule):
    """Base class for search of FGC PM events by respective notebook browsers"""

    def __init__(self, circuit_type, get_description):
        super().__init__()

        self.circuit_type = circuit_type
        self._get_description = get_description
        self.fgc_pm_events = []

        self.fgc_pm_event_sel = Select(options=[], rows=10, disabled=False, layout=Layout(width="50%"))

        # On select value change, update output
        self.fgc_pm_event_sel.observe(
            functools.partial(FgcPmEventSelectBaseModule.handle_event_selection_change, self=self), names=["value"]
        )

        first_row = HBox(children=[Label(value="FGC PM Events:"), self.fgc_pm_event_sel])

        # Fifth row - search for
        self.time_range = Text(placeholder="Time range for search of FGC", description="", value="5")
        self.time_range.observe(functools.partial(FgcPmEventSelectBaseModule.handle_time_range_change, self=self))
        self.time_range_label = Label(value="")
        second_row = HBox(
            children=[
                Label(value="Time range for search of FGC PM events around selected event [s]"),
                self.time_range,
                self.time_range_label,
            ]
        )

        self.widget = VBox(children=[first_row, second_row])

    @staticmethod
    def handle_event_selection_change(_, self):
        if self.mediator is not None:
            self.mediator.notify(self, pc.FgcBrowserAction.EVENT_SELECTION_CHANGE)

    @staticmethod
    def handle_time_range_change(_, self):
        if self.mediator is not None:
            self.mediator.notify(self, pc.FgcBrowserAction.TIME_RANGE_CHANGE)

    def update_event_selection(self):
        if self.fgc_pm_events:
            # Fix an occasional issue with widget updates
            fgc_pm_event = self._get_fgc_pm_event()
            pm_event_text = self._get_description(fgc_pm_event)
            circuit_name, timestamp = _get_first_with_timestamp_not_nan(
                fgc_pm_event.get_all_circuits(), fgc_pm_event.get_all_timestamps()
            )

            if self.circuit_type == "600A":
                print_ee_info_600A(circuit_name, self.circuit_type, timestamp)

            self.display_qps_circuit_schematic(circuit_name)

            print(f"Selected FGC PM event {pm_event_text}.")

            # Displaying FGC PM timestamps from the same subsector
            print(f"CIRCUIT NAME {circuit_name}")
            subsector = MappingMetadata.get_subsector_name_for_circuit_name(circuit_name)
            print(f"FGC PM events in circuits of subsector: {subsector}")

            self.time_range_label = "Searching for other PM events in the subsector, please wait..."
            time_range = float(self.time_range.value)
            source_timestamp_df = self.query_self_fgc_pm_events(timestamp, [(time_range, "s"), (time_range, "s")])
            display_fgc_pm_events_from_the_same_subsector(subsector, source_timestamp_df, timestamp)
            self.time_range_label = "Search for PM events is completed."

    def display_qps_circuit_schematic(self, circuit_name: str):
        pass

    def convert_fgc_pm_events_to_options(self, fgc_pm_entries: List[pc.FgcBase]) -> List[str]:
        """Method converting a list of source and timestamp tuples into a list of strings to be displayed

        :param fgc_pm_entries: a list of FGC PM source and timestamp tuples
        :return: list of strings with FGC PM source and timestamp
        """

        return [self._get_description(fgc_pm_entry) for fgc_pm_entry in fgc_pm_entries]

    def get_selected_circuit_name(self):
        circuits = self._get_fgc_pm_event().get_all_circuits()
        return tuple(circuits) if len(circuits) != 1 else circuits[0]

    def get_selected_timestamp(self):
        timestamps = self._get_fgc_pm_event().get_all_timestamps()
        return tuple(timestamps) if len(timestamps) != 1 else timestamps[0]

    def _get_fgc_pm_event(self):
        if not self.fgc_pm_events:
            raise ValueError(
                "The list of FGC PM Entries should contain at least one element. "
                "Please adjust circuit name as well as start and end date."
            )

        index = 0 if self.fgc_pm_event_sel.index is None else self.fgc_pm_event_sel.index
        return self.fgc_pm_events[index]

    def query_self_fgc_pm_events(self, timestamp_fgc: int, duration: List[Tuple[float, str]]) -> pd.DataFrame:
        start_time, end_time = Time.get_query_period_in_unix_time(timestamp_fgc, None, duration, "")
        duration_ = end_time - start_time
        circuit_type = self._get_meta_circuit_type()
        params = resolver.get_params_for_pm_events(circuit_type, "*", "PC", start_time, duration_)
        class_ = params.triplets[0].class_  # either '51_self_pmd' or 'lhc_self_pmd'
        return query.query_pm_data_headers("FGC", class_, "*", start_time, duration_)

    def _get_meta_circuit_type(self):
        if self.circuit_type.startswith("600A"):
            return "600A"
        if self.circuit_type in ["IPQ", "IPD"]:
            return signal_metadata.get_circuit_type_for_circuit_name(self.get_selected_circuit_name())
        return self.circuit_type


def print_ee_info_600A(circuit_family: str, circuit_type: str, timestamp_query: int) -> None:
    circuit_name = list(filter(lambda col: circuit_family in col, signal_metadata.get_circuit_names("600A")))[0]

    if "EE" in signal_metadata.get_system_types_per_circuit_name(circuit_type, circuit_name, timestamp_query):
        print(f"Circuit family {circuit_family} has an EE system.")
    else:
        print(f"Circuit family {circuit_family} does not have an EE system.")


def display_fgc_pm_events_from_the_same_subsector(
    subsector: str, source_timestamp_df: pd.DataFrame, timestamp_fgc: int
) -> None:
    if not source_timestamp_df.empty:
        sectors_df = MappingMetadata.read_circuit_name_to_sector()
        sectors_df = sectors_df[sectors_df["Safety subsector name"] == subsector]
        sectors_df["PC name"] = sectors_df.apply(
            lambda row: signal_metadata.get_fgc_names(row["Circuit name"], timestamp_fgc), axis=1
        )
        sectors_df = sectors_df.explode("PC name", ignore_index=True)

        sectors_df = sectors_df[sectors_df["PC name"].isin(source_timestamp_df["source"])]

        sectors_df = sectors_df.merge(source_timestamp_df, left_on=["PC name"], right_on=["source"]).drop(
            columns=["source", "Circuit type Layout"], axis=1
        )

        sectors_df["dt [ms]"] = (sectors_df["timestamp"] - timestamp_fgc) / 1e6
        sectors_df["timestamp"] = sectors_df["timestamp"].apply(Time.to_string_short)

        sectors_df.rename(columns={"Circuit type SIGMON": "Circuit type"}, inplace=True)
        sectors_df.sort_values(by="dt [ms]", inplace=True)
        sectors_df.reset_index(drop=True, inplace=True)

        display(HTML(sectors_df.to_html()))


class IpqFgcPmEventSelectBaseModule(FgcPmEventSelectBaseModule):
    _repo_url = "https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/raw/master"
    _circuit_class_to_url = {
        "2MQM": f"{_repo_url}/figures/ipq/2MQM_QPS.png",
        "2x2MQM": f"{_repo_url}/figures/ipq/2x2MQM_QPS.png",
        "2MQML": f"{_repo_url}/figures/ipq/2MQML_QPS.png",
        "2MQM+2MQML": f"{_repo_url}/figures/ipq/2MQM_2MQML_QPS.png",
        "2MQM+2MQMC": f"{_repo_url}/figures/ipq/2MQM_2MQMC_QPS.png",
        "2MQY": f"{_repo_url}/figures/ipq/2MQY_QPS.png",
        "2x2MQY": f"{_repo_url}/figures/ipq/2x2MQY_QPS.png",
    }

    def display_qps_circuit_schematic(self, circuit_name: str) -> None:
        circuit_class = MappingMetadata.get_qps_schematic_naming("IPQ", circuit_name)
        display(Image(url=self._circuit_class_to_url[circuit_class]))


class IpdFgcPmEventSelectBaseModule(FgcPmEventSelectBaseModule):
    _repo_url = "https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/raw/master"
    _circuit_class_to_url = {
        "MBX": f"{_repo_url}/figures/ipd/IPD_MBX_D1.png",
        "MBRC": f"{_repo_url}/figures/ipd/IPD_MBRC_D2_MBRB_D4.png",
        "MBRS": f"{_repo_url}/figures/ipd/IPD_MBRS_D3.png",
        "MBRB": f"{_repo_url}/figures/ipd/IPD_MBRC_D2_MBRB_D4.png",
    }

    def display_qps_circuit_schematic(self, circuit_name: str) -> None:
        circuit_class = MappingMetadata.get_qps_schematic_naming("IPD", circuit_name)
        display(Image(url=self._circuit_class_to_url[circuit_class]))


def _get_short_description(fgc: Fgc) -> str:
    return f"{Time.to_string(fgc.timestamp)}"


def _get_long_description(fgc: Fgc) -> str:
    return f"{fgc.circuit_name}: {Time.to_string(fgc.timestamp)}"


def _get_long_description_compound(fgc: FgcCompound) -> str:
    fgcs = [fgc for fgc in fgc.get_all_fgcs() if not math.isnan(fgc.timestamp)]
    return "; ".join([_get_long_description(pm) for pm in fgcs])


def _get_first_with_timestamp_not_nan(circuits, timestamps):
    return next(tuple_ for tuple_ in zip(circuits, timestamps) if not math.isnan(tuple_[1]))
