from abc import ABC, abstractmethod
from dataclasses import dataclass
from enum import Enum
from typing import List, Tuple, Union


class FgcBase(ABC):
    """Base class for the FGC event"""

    @abstractmethod
    def get_all_circuits(self) -> List[str]:
        pass

    @abstractmethod
    def get_all_timestamps(self) -> List[int]:
        pass


@dataclass
class Fgc(FgcBase):
    """Represents an FGC event associated with a single circuit."""

    circuit_name: str
    timestamp: int

    def get_all_circuits(self) -> List[str]:
        return [self.circuit_name]

    def get_all_timestamps(self) -> List[int]:
        return [self.timestamp]


class FgcCompound(FgcBase, ABC):
    """Represents an FGC event associated with many correlated circuits."""

    def get_all_circuits(self) -> List[str]:
        return [pm.circuit_name for pm in self.get_all_fgcs()]

    def get_all_timestamps(self) -> List[int]:
        return [pm.timestamp for pm in self.get_all_fgcs()]

    @abstractmethod
    def get_all_fgcs(self) -> List[Fgc]:
        pass


@dataclass
class RqFgc(FgcCompound):
    """RQ specific FGC event."""

    rqd: Fgc
    rqf: Fgc

    def get_all_fgcs(self) -> List[Fgc]:
        return [self.rqd, self.rqf]


@dataclass
class RcdoFgc(FgcCompound):
    """RCDO specific FGC event."""

    rcd: Fgc
    rco: Fgc

    def get_all_fgcs(self) -> List[Fgc]:
        return [self.rcd, self.rco]


@dataclass
class RcbxhvFgc(FgcCompound):
    """RCBXHV specific FGC event."""

    rcbxh: Fgc
    rcbxv: Fgc

    def get_all_fgcs(self) -> List[Fgc]:
        return [self.rcbxh, self.rcbxv]


class FgcBrowserAction(Enum):
    FIND_BUTTON_CLICKED = 1
    EVENT_SELECTION_CHANGE = 2
    TIME_RANGE_CHANGE = 3
    CIRCUIT_NAME_DROPDOWN_CHANGE = 4
