from ipywidgets import Box

from lhcsmapi.gui.ModuleMediator import ModuleMediator


class BaseModule(object):
    """
    The Base Module provides the basic functionality of storing a mediator's
    instance inside component objects.
    """

    def __init__(self, mediator: ModuleMediator = None, widget: Box = None) -> None:
        self._mediator = mediator
        self._widget = widget

    @property
    def mediator(self):
        return self._mediator

    @mediator.setter
    def mediator(self, mediator: ModuleMediator) -> None:
        self._mediator = mediator

    @property
    def widget(self) -> Box:
        return self._widget

    @widget.setter
    def widget(self, widget: Box) -> None:
        self._widget = widget
