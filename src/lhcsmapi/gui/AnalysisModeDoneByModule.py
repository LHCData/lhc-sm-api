import getpass

from ipywidgets import ToggleButtons, Text, Box


class AnalysisModeDoneByModule(object):
    def __init__(self):
        # Widget
        tooltips = [
            "Select automatic for automatic execution, comment, and acceptance.",
            "Select manual to execute, comment, and accept each analysis manually.",
        ]
        self.analysis_mode_tgl_btn = ToggleButtons(
            options=["Manual", "Automatic"], description="Analysis:", disabled=False, button_style="", tooltips=tooltips
        )

        self.done_by_txt = Text(placeholder="Your name", description="Done by:", value=getpass.getuser(), disabled=True)

        self.widget = [self.analysis_mode_tgl_btn, self.done_by_txt]

    def is_automatic_mode(self):
        return self.analysis_mode_tgl_btn.value == "Automatic"

    def get_author(self):
        return self.done_by_txt.value
