import functools
import getpass
import warnings

import pandas as pd
from IPython.display import clear_output, display
from ipywidgets import Dropdown, HBox, Label, Layout, Select, VBox

from lhcsmapi import reference
from lhcsmapi.gui.AnalysisModeDoneByModule import AnalysisModeDoneByModule
from lhcsmapi.gui.ModuleMediator import ModuleMediator
from lhcsmapi.gui.pc import select
from lhcsmapi.gui.pc.FgcPmCircuitNameDropdownBaseModule import get_circuit_names_or_prefixes_to_display
from lhcsmapi.metadata import signal_metadata


class HwcSearchModuleMediator(ModuleMediator):
    def __init__(self, circuit_type, hwc_test, hwc_summary_path):
        self._circuit_type = circuit_type
        self.hwc_test = hwc_test
        columns = ["systemName", "campaign", "testName", "executionStartTime", "executionEndTime", "executedTestStatus"]
        # the reason for `low_memory=False` is explained in  https://its.cern.ch/jira/browse/SIGMON-232
        self.hwc_summary = pd.read_csv(hwc_summary_path, low_memory=False)[columns].sort_values("executionStartTime")
        self.hwc_summary_circuit = self.hwc_summary
        # First row
        circuit_names = (
            get_circuit_names_or_prefixes_to_display(circuit_type)
            if circuit_type != "RQ"
            else signal_metadata.get_circuit_names("RQ")
        )
        description = "Circuit type:" if self._circuit_type == "600A" else "Circuit name"
        self.select_circuit_name_dropdown = Dropdown(options=circuit_names, description=description, disabled=False)

        self.select_circuit_name_dropdown.observe(
            functools.partial(HwcSearchModuleMediator.update_hwc_table, self=self), names=["value"]
        )

        first_row = HBox(children=[self.select_circuit_name_dropdown])

        # Second row
        self.analysis_mode_done_by_module = AnalysisModeDoneByModule()

        second_row = HBox(children=self.analysis_mode_done_by_module.widget)

        # Third row
        self.hwc_sel = Select(options=[], rows=15, disabled=False, layout=Layout(width="90%"))

        # On select value change, update output
        self.hwc_sel.observe(
            functools.partial(HwcSearchModuleMediator.update_schematic_text_output, self=self), names=["value"]
        )

        third_row = HBox(children=[Label(value="HWC tests:"), self.hwc_sel])

        # Final GUI
        self.widget = VBox(children=[first_row, second_row, third_row])

        # Display and Initialize
        display(self.widget)
        HwcSearchModuleMediator.update_hwc_table(None, self)
        HwcSearchModuleMediator.update_schematic_text_output(None, self)

    @staticmethod
    def update_hwc_table(value, self):
        self.hwc_summary_circuit = self.find_hwc_tests_for_selected_circuit()

        # find max string length
        if not self.hwc_summary_circuit.empty:
            max_str_lens = self.find_longest_string_per_column()
            options = self.create_options_list_from_hwc_summary_for_circuit(max_str_lens)
            self.hwc_sel.options = options

    def create_options_list_from_hwc_summary_for_circuit(self, max_str_lens):
        options = []
        for index, row in self.hwc_summary_circuit.iterrows():
            aligned_values = [
                (max_str_len - len(value)) * "  " + value for max_str_len, value in zip(max_str_lens, row.values)
            ]
            options.append(" | ".join(aligned_values))
        return options

    def find_longest_string_per_column(self):
        max_str_lens = []
        for col in self.hwc_summary_circuit.columns:
            max_str_lens.append(len(max(self.hwc_summary_circuit[col].values, key=len)))
        return max_str_lens

    def find_hwc_tests_for_selected_circuit(self):
        hwc_summary_circuit = self.hwc_summary[
            (self.hwc_summary["systemName"].str.contains(self.select_circuit_name_dropdown.value))
            & (self.hwc_summary["testName"].str.contains(self.hwc_test))
            & (self.hwc_summary["executedTestStatus"] != "RUNNING")
        ]
        hwc_summary_circuit.sort_values(by="executionStartTime", inplace=True)
        hwc_summary_circuit.reset_index(drop=True, inplace=True)

        return hwc_summary_circuit

    @staticmethod
    def update_schematic_text_output(value, self):
        clear_output(wait=True)
        display(self.widget)

        if self.hwc_sel.index is None:
            self.hwc_sel.index = 0
            clear_output(wait=True)
            display(self.widget)

        if self._circuit_type == "IPQ":
            select.get_module_for_circuit_type("IPQ").display_qps_circuit_schematic(self.get_circuit_name())
        elif self._circuit_type == "IPD":
            select.get_module_for_circuit_type("IPD").display_qps_circuit_schematic(self.get_circuit_name())

        print("Selected circuit: %s" % (self.get_circuit_name()))
        print("Selected HWC test start time: %s and end time: %s." % (self.get_start_time(), self.get_end_time()))
        print("The analysis is performed by %s." % getpass.getuser())
        print("The analysis is executed in %s mode." % ("Automatic" if self.is_automatic_mode() else "Manual"))

    def get_start_time(self):
        if not self.hwc_summary_circuit.empty:
            return self.hwc_summary_circuit.loc[self.hwc_sel.index, "executionStartTime"]
        else:
            return ""

    def get_ref_start_time(self):
        selection_mask = (self.hwc_summary_circuit.index < self.hwc_sel.index) & (
            self.hwc_summary_circuit["executedTestStatus"] == "SUCCESSFUL"
        )

        if not self.hwc_summary_circuit[selection_mask].empty:
            return self.hwc_summary_circuit[selection_mask].iloc[-1]["executionStartTime"]
        else:
            warnings.warn("There is no reference HWC test for the selected one!")
            return ""

    def get_end_time(self):
        if not self.hwc_summary_circuit.empty:
            return self.hwc_summary_circuit.loc[self.hwc_sel.index, "executionEndTime"]
        else:
            return ""

    def get_ref_end_time(self):
        selection_mask = (self.hwc_summary_circuit.index < self.hwc_sel.index) & (
            self.hwc_summary_circuit["executedTestStatus"] == "SUCCESSFUL"
        )

        if not self.hwc_summary_circuit[selection_mask].empty:
            return self.hwc_summary_circuit[selection_mask].iloc[-1]["executionEndTime"]
        else:
            warnings.warn("There is no reference HWC test for the selected one!")
            return ""

    def get_circuit_name(self):
        if not self.hwc_summary_circuit.empty:
            return self.hwc_summary_circuit.loc[self.hwc_sel.index, "systemName"]
        else:
            return ""

    def get_campaign(self):
        if not self.hwc_summary_circuit.empty:
            return self.hwc_summary_circuit.loc[self.hwc_sel.index, "campaign"]
        else:
            return ""

    def is_automatic_mode(self):
        return self.analysis_mode_done_by_module.is_automatic_mode()

    def get_author(self):
        return self.analysis_mode_done_by_module.get_author()

    @staticmethod
    def get_user():
        return getpass.getuser()

    @staticmethod
    def get_last_ref_start_end_time(hwc_summary_path, circuit_name, hwc_test, t_start):
        """Deprecated: Use reference.get_reference_test instead."""
        hwc_summary = signal_metadata.get_hwc_summary(hwc_summary_path)
        return reference.get_reference_test(hwc_summary, circuit_name, hwc_test, t_start)
