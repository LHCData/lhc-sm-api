import warnings

from ipywidgets import Box, Text, Label
import functools

from lhcsmapi.Time import Time
from lhcsmapi.gui.BaseModule import BaseModule
from lhcsmapi.gui.BrowserAction import BrowserAction


class DateTimeBaseModule(BaseModule):
    def __init__(self, start_date_time="2010-01-01 00:00:00+01:00", end_date_time="2018-12-13 00:00:00+01:00"):
        placeholder = "YYYY-MM-DD HH:MM:SS+01:00"
        self.txt_start = Text(value=start_date_time, placeholder=placeholder, description="Start:")
        self.txt_start_value = start_date_time
        self.txt_start.on_submit(functools.partial(DateTimeBaseModule.update_txt_start, self=self))

        self.txt_end = Text(value=end_date_time, placeholder=placeholder, description="End:")
        self.txt_end_value = end_date_time
        self.txt_end.on_submit(functools.partial(DateTimeBaseModule.update_txt_end, self=self))

        self.lbl_status = Label(value="")
        self.widget = Box(children=[self.txt_start, self.txt_end, self.lbl_status])

    @staticmethod
    def check_date_time(start_date_time, end_date_time):
        try:
            start_date_time_unix = Time.to_unix_timestamp(start_date_time)
            end_date_time_unix = Time.to_unix_timestamp(end_date_time)
        except ValueError:
            warnings.warn("Provided dates are invalid.")
            return False
        if start_date_time_unix < end_date_time_unix:
            return True
        else:
            warnings.warn("Start time is not later than the end time.")
            return False

    @staticmethod
    def update_txt_start(value, self):
        if DateTimeBaseModule.check_date_time(self.get_start_date_time(), self.get_end_date_time()):
            self.lbl_status.value = ""
            self.txt_start_value = self.txt_start.value
            self.mediator.notify(self, BrowserAction.DATE_CHANGED)
        else:
            self.lbl_status.value = "The start date is wrong, please type it again"
            # set start time to previous value
            self.txt_start.value = self.txt_start_value

    @staticmethod
    def update_txt_end(value, self):
        if DateTimeBaseModule.check_date_time(self.get_start_date_time(), self.get_end_date_time()):
            self.lbl_status.value = ""
            self.txt_end_value = self.txt_end.value
            if self.mediator is not None:
                self.mediator.notify(self, BrowserAction.DATE_CHANGED)
        else:
            self.lbl_status.value = "The end date is wrong, please type it again"
            # set end time to previous value
            self.txt_end.value = self.txt_end_value

    def get_start_date_time(self):
        return self.txt_start.value

    def get_end_date_time(self):
        return self.txt_end.value
