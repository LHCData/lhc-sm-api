from enum import Enum


class BrowserAction(Enum):
    QUERY_BUTTON_CLICKED = 1
    SAVE_BUTTON_CLICKED = 2
    DATE_CHANGED = 3
