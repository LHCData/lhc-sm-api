from abc import ABC


# from https://refactoring.guru/design-patterns/mediator/python/example
class ModuleMediator(ABC):
    """
    The Module Mediator interface declares a method used by components to notify the
    mediator about various events. The Mediator may react to these events and
    pass the execution to other components.
    """

    def notify(self, sender: object, event: str) -> None:
        pass
