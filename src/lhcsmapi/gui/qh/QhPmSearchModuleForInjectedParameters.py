from IPython.display import display, HTML

from lhcsmapi.analysis.qh.QuenchHeaterQuery import QuenchHeaterQuery
from lhcsmapi.metadata import signal_metadata


class QhPmSearchModuleForInjectedParameters:
    """Class for query of quench heater events and signals (both voltage and current) provided circuit name,
    discharge level and event dates

    """

    def __init__(self, circuit_name, discharge_level, start_time, end_time, is_automatic):
        self._circuit_name = circuit_name
        self._discharge_level = discharge_level
        self._is_automatic = is_automatic
        circuit_sub_type = signal_metadata.get_circuit_type_for_circuit_name(circuit_name)
        self.source_timestamp_df = QuenchHeaterQuery(
            circuit_sub_type, circuit_name
        ).find_source_timestamp_qh_with_pattern("*", start_time, end_time)
        display(HTML(self.source_timestamp_df.to_html()))

    def get_circuit_name(self):
        return self._circuit_name

    def get_discharge_level(self):
        return self._discharge_level

    def is_automatic_mode(self):
        return self._is_automatic
