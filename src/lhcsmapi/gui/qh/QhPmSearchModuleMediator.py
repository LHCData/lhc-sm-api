import getpass
import functools
import warnings

from ipywidgets import Text, VBox, ToggleButtons, HBox, Button, Label, Dropdown
from IPython.display import display, HTML, clear_output

from lhcsmapi.Time import Time
from lhcsmapi.gui.ModuleMediator import ModuleMediator
from lhcsmapi.gui.DateTimeBaseModule import DateTimeBaseModule
from lhcsmapi.metadata import signal_metadata
from lhcsmapi.metadata.MappingMetadata import MappingMetadata
from lhcsmapi.analysis.qh.QuenchHeaterQuery import QuenchHeaterQuery


def get_circuit_names_or_types(circuit_type: str):
    """Method returning a list of circuit names

    :param circuit_type: circuit type such as RB, RQ, IT, IPD, IPQ
    :return: list of circuit names or types
    """
    if circuit_type in ["RB", "IT", "RQ"]:
        return signal_metadata.get_circuit_names(circuit_type)
    elif circuit_type == "IPD":
        return signal_metadata.get_circuit_names(["IPD2", "IPD2_B1B2"])
    elif circuit_type == "IPQ":
        return sorted(signal_metadata.get_circuit_names(["IPQ2", "IPQ4", "IPQ8"]))
    else:
        raise KeyError(f"Circuit type {circuit_type} is not supported")


class QhPmSearchModuleMediator(ModuleMediator):
    def __init__(self, date_time_module: DateTimeBaseModule, circuit_type="RB"):
        self._circuit_type = circuit_type
        # First row
        circuit_names = get_circuit_names_or_types(circuit_type)
        self.select_circuit_name_dropdown = Dropdown(options=circuit_names, description="Circuit name", disabled=False)

        self._date_time_module = date_time_module
        self._date_time_module.mediator = self

        first_row = HBox(children=[self.select_circuit_name_dropdown, self._date_time_module.widget])

        # Second row
        is_pattern_disabled = False if self._circuit_type in ["RB", "RQ"] else True
        self.pattern_txt = Text(description="Pattern:", value="*", disabled=is_pattern_disabled)

        self.analysis_mode_tgl_btn = ToggleButtons(
            options=["Automatic", "Manual"],
            description="Analysis:",
            disabled=False,
            button_style="",
            tooltips=[
                "Select automatic for automatic execution, comment, and acceptance.",
                "Select manual to execute, comment, and accept each analysis manually.",
            ],
        )

        self.done_by_txt = Text(placeholder="Your name", description="Done by:", value=getpass.getuser(), disabled=True)

        second_row = HBox(children=[self.pattern_txt, self.analysis_mode_tgl_btn, self.done_by_txt])

        # Third row
        self.discharge_level_dropdown = Dropdown(description="U_nominal [V]:", options=["900", "300"], value="900")

        third_row = HBox(children=[self.discharge_level_dropdown])

        # Fourth row
        # # Search button
        self.find_btn = Button(
            description="Find QH PM entries",
            tooltip="Click to find QH PM entries between indicated dates.",
            button_style="",
        )

        self.find_btn.on_click(functools.partial(QhPmSearchModuleMediator.handle_find_button_event, self=self))

        # # Text field
        self.lbl_search = Label(value="Click button to search for QH PM entries within the specified period of time")

        fourth_row = HBox(children=[self.find_btn, self.lbl_search])

        # Final GUI
        self.widget = VBox(children=[first_row, second_row, third_row, fourth_row])

        display(self.widget)

    def handle_find_button_event(b, self) -> None:
        clear_output(wait=True)
        display(self.widget)
        start_date_time = self._date_time_module.get_start_date_time()
        end_date_time = self._date_time_module.get_end_date_time()

        if DateTimeBaseModule.check_date_time(start_date_time, end_date_time):
            pattern = self.pattern_txt.value
            circuit_name = self.get_circuit_name()

            circuit_type = signal_metadata.get_circuit_type_for_circuit_name(circuit_name)

            self.lbl_search.value = "Searching for QH PM events. Please wait."
            self.source_timestamp_df = QuenchHeaterQuery(
                circuit_type, circuit_name
            ).find_source_timestamp_qh_with_pattern(pattern, start_date_time, end_date_time)

            if not self.source_timestamp_df.empty:
                self.source_timestamp_df.sort_values(by="source", inplace=True)
                self.source_timestamp_df.reset_index(drop=True, inplace=True)

            self.lbl_search.value = "Found %d QH PM events." % len(self.source_timestamp_df)

            self.display_found_qh_source_timestamp(circuit_name, start_date_time, end_date_time)

    def display_found_qh_source_timestamp(self, circuit_name, start_date_time, end_date_time):
        if self.source_timestamp_df.empty:
            print("No events found!")
        else:
            if self._circuit_type in ["RB", "RQ"]:
                # Find the list of missing sources
                cells_in_circuit = MappingMetadata.get_cells_for_circuit_names(self._circuit_type, circuit_name)
                missing_sources = list(set(cells_in_circuit) - set(self.source_timestamp_df["source"].values))
                print("List of missing sources for circuit %s:" % circuit_name)
                print(missing_sources)
                print()

            # List of found sources
            self.source_timestamp_df["datetime"] = self.source_timestamp_df["timestamp"].apply(
                lambda row: Time.to_string(row)
            )

            print(
                "List of found sources and timestamps (%s - %s) for circuit %s: "
                % (start_date_time, end_date_time, circuit_name)
            )
            display(HTML(self.source_timestamp_df.to_html()))

    def get_circuit_type(self):
        return self._circuit_type

    def get_circuit_name(self):
        return self.select_circuit_name_dropdown.value

    def is_automatic_mode(self):
        return self.analysis_mode_tgl_btn.value == "Automatic"

    def get_author(self):
        return self.done_by_txt.value

    def get_discharge_level(self):
        return int(self.discharge_level_dropdown.value)
