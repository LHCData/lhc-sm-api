from __future__ import annotations

import math
import time
import warnings
from datetime import date, datetime, timedelta
from typing import Union

import mpmath
import pandas as pd
import tzlocal

TimeAlias = Union[int, str, datetime, pd.Timestamp]


class Time:
    """Time class encapsulates provides a set of methods for conversion between various time formats.
    In addition it provides a generator for days between given periods"""

    __units = {"ns": 1, "us": 1e3, "ms": 1e6, "s": 1e9}

    def __init__(self, date_time, unit=None, tz=None):
        """Constructor of the Time conversion object.

        :param date_time: input date and time in various formats (string, int, float, datetime, pd.Timestamp)
        :param unit: unit of the input
        :param tz: time zone of the input
        """
        if (unit is not None) and (unit not in self.__units.keys()):
            raise ValueError("cannot cast unit %s" % unit)

        if isinstance(date_time, (int, float)) and unit in Time.__units.keys():
            date_time = Time.__convert_real_numbers_to_unix_time(date_time, Time.__units[unit])
            unit = "ns"

        if tz:
            self.__pd_timestamp = pd.Timestamp(date_time, tz=tz, unit=unit)
        else:
            self.__pd_timestamp = pd.Timestamp(date_time, tz=tzlocal.get_localzone(), unit=unit)

    # region properties
    @property
    def unix_timestamp(self):
        return self.__pd_timestamp.value

    @property
    def pandas_timestamp(self):
        return self.__pd_timestamp

    @property
    def string_timestamp(self):
        return self.__pd_timestamp.__str__()

    @property
    def datetime_timestamp(self):
        return self.__pd_timestamp.to_pydatetime()

    # endregion

    # region conversion methods
    @staticmethod
    def to_unix_timestamp(date_time: TimeAlias, unit: str = "ns", tz=None) -> int:
        """
        **Method converts input date and time variable into a unix timestamp integer type**

        Method converts time from four types (integer, string, datetime, pandas.Timestamp) into
        unix timestamp integer.
        The method relies on pandas.timestamp for convertion.

        :param date_time: input date and time
        :param unit: unit of the input: s, ms, us, ns (optional, ns by default)
        :param tz: time zone (optional, local by default)
        :return: time expressed as a unix time stamp

        - Example::

              >>> from lhcsmapi.Time import Time
              >>> string_date = "2018-05-01 00:00:00"
              >>> Time.to_unix_timestamp(string_date, unit="ns")
              1525125600000000000

              >>> from lhcsmapi.Time import Time
              >>> from datetime import datetime
              >>> date_time = datetime(2018, 5, 1, 0, 0, 0, 0)
              >>> Time.to_unix_timestamp(date_time, unit="ns")
              1525125600000000000

              >>> from lhcsmapi.Time import Time
              >>> timestamp = 1525125600000000000
              >>> Time.to_unix_timestamp(timestamp, unit="ns")
              1525125600000000000

              >>> from lhcsmapi.Time import Time
              >>> import pandas as pd
              >>> pd_timestamp = pd.Timestamp(2018, 5, 1, 0)
              >>> Time.to_unix_timestamp(pd_timestamp, unit="ns")
              1525125600000000000

        - Expected Fail Response::

              >>> from lhcsmapi.Time import Time
              >>> string_date = "2018-05-01 00:00:00"
              >>> Time.to_unix_timestamp(string_date, unit="n") # doctest: +SKIP
              Traceback (most recent call last):
              ...
              ValueError: cannot cast unit n
        """
        time_converter = Time.__validate_conversion(date_time, "unix", unit, tz)
        return time_converter.unix_timestamp

    @staticmethod
    def to_pandas_timestamp(date_time: TimeAlias, unit: str = "ns", tz=None) -> pd.Timestamp:
        """
        **Method converts input date and time time variable into a pandas.Timestamp type**

        Method converts convert date and time from four types (integer, string, datetime, pandas.Timestamp) into
        pandas.Timestamp.
        The method relies on pandas.timestamp for convertion.

        :param date_time: input date and time
        :param unit: unit of the input: s, ms, us, ns (optional, ns by default)
        :param tz: time zone (optional, local by default)
        :return: time expressed as a unix time stamp

        - Example::

              >>> from lhcsmapi.Time import Time
              >>> string_date = "2019-07-10 10:23:27.013"
              >>> Time.to_pandas_timestamp(string_date, unit="ns")
              Timestamp('2019-07-10 10:23:27.013000+0200', tz='Europe/Berlin')

              >>> from lhcsmapi.Time import Time
              >>> from datetime import datetime
              >>> date_time = datetime(2019, 7, 10, 10, 23, 27, 13)
              >>> Time.to_pandas_timestamp(date_time, unit="ns")
              Timestamp('2019-07-10 10:23:27.000013+0200', tz='Europe/Berlin')

              >>> from lhcsmapi.Time import Time
              >>> timestamp = 1562747007013000000
              >>> Time.to_pandas_timestamp(timestamp, unit="ns")
              Timestamp('2019-07-10 10:23:27.013000+0200', tz='Europe/Berlin')

              >>> from lhcsmapi.Time import Time
              >>> import pandas as pd
              >>> pd_timestamp = pd.Timestamp(2019, 7, 10, 10, 23, 27, 13000)
              >>> Time.to_pandas_timestamp(pd_timestamp, unit="ns")
              Timestamp('2019-07-10 10:23:27.013000+0200', tz='Europe/Berlin')

        - Expected Fail Response::

              >>> from lhcsmapi.Time import Time
              >>> string_date = "2019-07-10 10:23:27.013"
              >>> Time.to_pandas_timestamp(string_date, unit="n") # doctest: +SKIP
              Traceback (most recent call last):
              ...
              ValueError: cannot cast unit n
        """
        time_converter = Time.__validate_conversion(date_time, "pandas", unit, tz)
        return time_converter.pandas_timestamp

    @staticmethod
    def to_string(date_time: TimeAlias, unit: str = "ns", tz=None) -> str:
        """
        **Method converts input date and time variable into a string type**

        Method converts time from four types (integer, string, datetime, pandas.Timestamp) into string.
        The method relies on pandas.timestamp for convertion.

        :param date_time: input date and time
        :param unit: unit of the input: s, ms, us, ns (optional, ns by default)
        :param tz: time zone (optional, local by default)
        :return: time expressed as a unix time stamp

        - Example::

              >>> from lhcsmapi.Time import Time
              >>> string_date = "2017-07-02 01:23:27"
              >>> Time.to_string(string_date, unit = "ns")
              '2017-07-02 01:23:27+02:00'

              >>> from lhcsmapi.Time import Time
              >>> from datetime import datetime
              >>> date_time = datetime(2017, 7, 2, 1, 23, 27, 0)
              >>> Time.to_string(date_time, unit = "ns")
              '2017-07-02 01:23:27+02:00'

              >>> from lhcsmapi.Time import Time
              >>> timestamp = 1498951407000000000
              >>> Time.to_string(timestamp, unit = "ns")
              '2017-07-02 01:23:27+02:00'

              >>> from lhcsmapi.Time import Time
              >>> import pandas as pd
              >>> pd_timestamp = pd.Timestamp(2017, 7, 2, 1, 23, 27)
              >>> Time.to_string(pd_timestamp, unit = "ns")
              '2017-07-02 01:23:27+02:00'

        - Expected Fail Response::

              >>> from lhcsmapi.Time import Time
              >>> string_date = "2017-07-02 01:23:27"
              >>> Time.to_string(string_date, unit="n") # doctest: +SKIP
              Traceback (most recent call last):
              ...
              ValueError: cannot cast unit n
        """
        if isinstance(date_time, float) and math.isnan(date_time):
            return ""

        time_converter = Time.__validate_conversion(date_time, "string formatted", unit, tz)
        return time_converter.string_timestamp

    @staticmethod
    def to_string_short(date_time: TimeAlias, unit: str = "ns", tz=None, n_dec_digits: int = 3) -> str:
        """
        **Method converts input date and time variable into a string type without the time zone and limited
        number of sub-second digits.**

        Method converts time from four types (integer, string, datetime, pandas.Timestamp) into string.
        The method relies on pandas.timestamp for convertion.

        :param date_time: input date and time
        :param unit: unit of the input: s, ms, us, ns (optional, ns by default)
        :param tz: time zone (optional, local by default)
        :return: time expressed as a unix time stamp

        - Example::

              >>> from lhcsmapi.Time import Time
              >>> string_date = "2017-07-02 01:23:27"
              >>> Time.to_string_short(string_date, unit = "ns")
              '2017-07-02 01:23:27'

              >>> from lhcsmapi.Time import Time
              >>> from datetime import datetime
              >>> date_time = datetime(2017, 7, 2, 1, 23, 27, 0)
              >>> Time.to_string_short(date_time, unit = "ns")
              '2017-07-02 01:23:27'

              >>> from lhcsmapi.Time import Time
              >>> timestamp = 1498951407000000000
              >>> Time.to_string_short(timestamp, unit = "ns")
              '2017-07-02 01:23:27'

              >>> from lhcsmapi.Time import Time
              >>> import pandas as pd
              >>> pd_timestamp = pd.Timestamp(2017, 7, 2, 1, 23, 27)
              >>> Time.to_string_short(pd_timestamp, unit = "ns")
              '2017-07-02 01:23:27'

        - Expected Fail Response::

              >>> from lhcsmapi.Time import Time
              >>> string_date = "2017-07-02 01:23:27"
              >>> Time.to_string_short(string_date, unit="n") # doctest: +SKIP
              Traceback (most recent call last):
              ...
              ValueError: cannot cast unit n
        """
        if isinstance(date_time, float) and math.isnan(date_time):
            return ""

        time_converter = Time.__validate_conversion(date_time, "string formatted", unit, tz)
        time_without_tz = time_converter.string_timestamp.split("+")[0]

        # Cut trailing zeros up to the number of digits
        if "." in time_without_tz:
            time_without_tz = time_without_tz.rstrip("0")
            n_dec_digits_act = len(time_without_tz.split(".")[-1])
            if n_dec_digits_act < n_dec_digits:
                time_without_tz += "0" * (n_dec_digits - n_dec_digits_act)
            return time_without_tz
        else:
            return time_without_tz

    @staticmethod
    def to_datetime(date_time: TimeAlias, unit: str = "ns", tz=None) -> datetime:
        """
        **Method converts input time variable into a datetime type**

        Method converts time from four types (integer, string, datetime, pandas.Timestamp) into datetime.
        The method relies on pandas.timestamp for convertion.

        :param date_time: input date and time
        :param unit: unit of the input: s, ms, us, ns (optional, ns by default)
        :param tz: time zone (optional, local by default)
        :return: time expressed as a unix time stamp

        - Example::

              >>> from lhcsmapi.Time import Time
              >>> string_date = "2019-01-02 23:59:11"
              >>> Time.to_datetime(string_date, unit = "ns")
              datetime.datetime(2019, 1, 2, 23, 59, 11, tzinfo=<DstTzInfo 'Europe/Berlin' CET+1:00:00 STD>)

              >>> from lhcsmapi.Time import Time
              >>> from datetime import datetime
              >>> date_time = datetime(2019, 1, 2, 23, 59, 11, 0)
              >>> Time.to_datetime(string_date, unit="ns")
              datetime.datetime(2019, 1, 2, 23, 59, 11, tzinfo=<DstTzInfo 'Europe/Berlin' CET+1:00:00 STD>)

              >>> from lhcsmapi.Time import Time
              >>> timestamp = 1546469951000000000
              >>> Time.to_datetime(timestamp, unit="ns")
              datetime.datetime(2019, 1, 2, 23, 59, 11, tzinfo=<DstTzInfo 'Europe/Berlin' CET+1:00:00 STD>)

              >>> from lhcsmapi.Time import Time
              >>> import pandas as pd
              >>> pd_timestamp = pd.Timestamp(2019, 1, 2, 23, 59, 11, 0)
              >>> Time.to_datetime(pd_timestamp, unit="ns")
              datetime.datetime(2019, 1, 2, 23, 59, 11, tzinfo=<DstTzInfo 'Europe/Berlin' CET+1:00:00 STD>)

        - Expected Fail Response::

              >>> from lhcsmapi.Time import Time
              >>> string_date = "2019-01-02 23:59:11"
              >>> Time.to_datetime(string_date, unit="n") # doctest: +SKIP
              Traceback (most recent call last):
              ...
              ValueError: cannot cast unit n
        """
        time_converter = Time.__validate_conversion(date_time, "datetime", unit, tz)
        return time_converter.datetime_timestamp

    @staticmethod
    def to_unix_timestamp_in_sec(date_time: TimeAlias, unit: str = "ns", tz=None, warn: bool = False):
        """
        **Method converts input time variable into a float type with us precision**

        Method returns input converted to unix time in seconds with us precision.

        :param date_time: input unix timestamp in nanoseconds
        :param unit: unit of the input: s, ms, us, ns (optional, ns by default)
        :param durationData: time zone (optional, local by default)
        :param tz: time zone (optional, local by default)
        :return: time expressed as a unix time stamp

        - Example::
            >>> from lhcsmapi.Time import Time
            >>> string_date = "2018-05-01 00:00:00"
            >>> Time.to_unix_timestamp_in_sec(string_date, unit = "ns")
            1525125600.0

            >>> from lhcsmapi.Time import Time
            >>> from datetime import datetime
            >>> date_time = datetime(2018, 5, 1, 0, 0, 0, 0)
            >>> Time.to_unix_timestamp_in_sec(date_time, unit = "ns")
            1525125600.0

            >>> from lhcsmapi.Time import Time
            >>> timestamp = 1525125600000000000
            >>> Time.to_unix_timestamp_in_sec(timestamp, unit = "ns")
            1525125600.0

            >>> from lhcsmapi.Time import Time
            >>> import pandas as pd
            >>> pd_timestamp = pd.Timestamp(2018, 5, 1, 0)
            >>> Time.to_unix_timestamp_in_sec(pd_timestamp, unit = "ns")
            1525125600.0


        - Expected Fail Response::
            >>> from lhcsmapi.Time import Time
            >>> string_date = "2018-05-01 00:00:00"
            >>> Time.to_unix_timestamp_in_sec(string_date, unit = "n") # doctest: +SKIP
            Traceback (most recent call last):
            ...
            ValueError: cannot cast unit n
        """

        unix_time = Time.to_unix_timestamp(date_time, unit, tz)
        return Time.__convert_unix_time_in_nanoseconds_to_seconds(unix_time, warn)

    # endregion
    @staticmethod
    def get_query_period_in_unix_time(
        start_time_date: int | str | float | None = None,
        end_time_date: int | str | float | None = None,
        duration_date: int | str | float | None = None,
        tz=None,
    ):
        """
        **Calculates start and end time from start time and either end time or duration**

        Method calculates t_start and t_end or duration values based on the arguments and then returns a tuple
        with these converted to unix time. Three operation modes are possible:

        - if start_time_date is not None:

        (t_start, t_end) = (start_time_date, end_time_date)

        - elif len(duration_date) is 1:

        (t_start, t_end) = (start_time_date, startTimeData+end_time_date[0])

        - elif len(duration_date) is 2:

        (t_start, t_end) = (start_time_date-duration_date[0], start_time_date+duration_date[1])

        :param start_time_date: input date and time
        :param end_time_date: unit of the input (None if durationData defined)
        :param duration_date: time zone (None if endTimeData defined)
        :param tz: time zone (optional, local by default)
        :return: time expressed as a unix time stamp

        - Example::

              >>> from lhcsmapi.Time import Time
              >>> start_timestamp = 1426220459491000000
              >>> end_timestamp =   1426220479491000000
              >>> Time.get_query_period_in_unix_time(start_timestamp, end_timestamp, None, tz='Europe/Berlin')
              (1426220459491000000, 1426220479491000000)

              >>> from lhcsmapi.Time import Time
              >>> start_timestamp = 1426220459491000000
              >>> duration =  [(20, 's')]
              >>> Time.get_query_period_in_unix_time(start_timestamp, None, duration, tz='Europe/Berlin')
              (1426220459491000000, 1426220479491000000)

              >>> from lhcsmapi.Time import Time
              >>> start_timestamp = 1426220469491000000
              >>> duration =  [(10, 's'), (10, 's')]
              >>> Time.get_query_period_in_unix_time(start_timestamp, None, duration, tz='Europe/Berlin')
              (1426220459491000000, 1426220479491000000)

        - Expected Fail Response::

              >>> from lhcsmapi.Time import Time
              >>> start_timestamp = 1426220469491000000
              >>> Time.get_query_period_in_unix_time(start_timestamp, None, None, tz='Europe/Berlin') # doctest: +SKIP
              Traceback (most recent call last):
              ...
              ValueError: end_time_date and duration_date can not be both None at the same time!
        """

        # check input correctness
        if (end_time_date is None) and (duration_date is None):
            raise ValueError("end_time_date and duration_date can not be both None at the same time!")

        # convert t_start to unix time:
        start_time, unit = Time.__validate_time_tuple(start_time_date)
        if start_time:
            start_time = Time.to_unix_timestamp(start_time, unit, tz)

        # extact t_end if provided if so convert to unix time:
        end_time, unit = Time.__validate_time_tuple(end_time_date)
        if end_time:
            end_time = Time.to_unix_timestamp(end_time, unit, tz)

        elif duration_date:
            if isinstance(duration_date, tuple):
                duration_date = [duration_date]

            # duration is list with single entry or tuple , calculate t_end from duration:
            if isinstance(duration_date, list) and len(duration_date) == 1:
                duration, unit = Time.__validate_time_tuple(duration_date[0])
                end_time = start_time + Time.to_unix_timestamp(duration, unit, tz)

            # duration is list with two entries (tuple) , calculate t_start and t_end from duration:
            elif isinstance(duration_date, list) and len(duration_date) == 2:
                dt1, unit1 = Time.__validate_time_tuple(duration_date[0])
                dt2, unit2 = Time.__validate_time_tuple(duration_date[1])

                end_time = start_time + Time.to_unix_timestamp(dt2, unit2, tz)
                start_time -= Time.to_unix_timestamp(dt1, unit1, tz)

        return start_time, end_time

    @staticmethod
    def daterange(start_date: date, end_date: date):
        """
        **Method creates a generator of consecutive days between two dates**

        Method allowing users to create a generator of days from a given
        start_date, with an increment of a day, to end_date - 1 day

        :param start_date: starting date for the generator
        :param end_date: end date
        :return: generator providing a day from start_date to end_date - 1 day

        - Example::

              >>> from lhcsmapi.Time import Time
              >>> from datetime import date
              >>> start_date = date(2014, 1, 1)
              >>> end_date = date(2014, 1, 3)
              >>> for single_date in Time.daterange(start_date, end_date):
              ...  print(single_date)
              2014-01-01
              2014-01-02

        - Expected Fail Response::

              >>> from datetime import date
              >>> start_date = date(2014, 1, 5)
              >>> end_date = date(2014, 1, 3)
              >>> for single_date in Time.daterange(start_date, end_date): print(single_date)


        """
        for n in range(int((end_date - start_date).days)):
            yield start_date + timedelta(n)

    @staticmethod
    def modulo_day_in_unix_timestamp(start_time, end_time) -> tuple[int, int]:
        """Method calculates quotient and remainder of time difference between start_time and end_time.

        :param start_time: start time for the calculation
        :param end_time: end time for the calculation
        :return: tuple with quotient and remainder of time difference division into days
        """
        start_time_unix = Time.to_unix_timestamp(start_time)
        end_time_unix = Time.to_unix_timestamp(end_time)

        if start_time_unix > end_time_unix:
            raise AttributeError("t_start {} is greater than the t_end {}".format(start_time, end_time))

        diff_time_unix = end_time_unix - start_time_unix
        day_in_nanos = int(24 * 60 * 60 * 1e9)

        quotient = diff_time_unix // day_in_nanos
        remainder = diff_time_unix % day_in_nanos

        return quotient, remainder

    # region private helper methods
    @staticmethod
    def __validate_time_tuple(time_data):
        """Method returns a tuple that can be safely unpacked regardless of whether time_data is a  tuple: (time, unit)
        or a single: time object

        :param time_data: a tuple with time data
        """

        time = None
        unit = None

        if isinstance(time_data, tuple):
            if len(time_data) > 0:
                time = time_data[0]

            if len(time_data) > 1:
                unit = time_data[1]
        else:
            time = time_data

        return time, unit

    @staticmethod
    def __validate_conversion(date_time, conversion_type, unit="ns", tz=None) -> "Time":
        """Method validates if time conversion can be performed"""
        try:
            return Time(date_time, unit, tz)
        except TypeError:
            raise TypeError("Cannot convert input {} to {} timestamp".format(type(date_time), conversion_type))

    @staticmethod
    def __convert_real_numbers_to_unix_time(date_time, ratio) -> int:
        """Method converts input of int or float to unix time based on ratio"""
        with mpmath.mp.workdps(20):
            input_full = mpmath.mpf(str(date_time))

            ratio = mpmath.mpf(ratio)
            unix_time = input_full * ratio
            return int(unix_time)

    @staticmethod
    def __convert_unix_time_in_nanoseconds_to_seconds(unix_time, warn=False) -> float:
        """Method converts unix time in nanoseconds to unix time in seconds with us precision"""

        ns_remainder = unix_time % 1000

        if warn and ns_remainder > 0:
            warnings.warn("Discarding nonzero {} nanoseconds in conversion".format(ns_remainder), stacklevel=2)

        ns = unix_time - ns_remainder
        unix_time_seconds = mpmath.mpf(ns) / mpmath.mpf(1e9)

        return float(unix_time_seconds)

    # endregion

    @staticmethod
    def get_analysis_start_time(time_format="%Y-%m-%d-%Hh%M") -> str:
        """Method returns start time of an analysis formatted according to the format string. Used in notebooks for
        file name creation.

        :return: current time at the moment of function execution formatted according to the format string
        """
        return datetime.now().strftime(time_format)

    @staticmethod
    def sleep(sleep_duration: int) -> None:
        """Method sleeps for a given amount of seconds

        :param sleep_duration: number of seconds to wait until proceeding to the next statement (if any)
        """
        time.sleep(sleep_duration)

    @staticmethod
    def now() -> datetime:
        """Method returns current time

        :return: datetime object with the current time
        """
        return datetime.now()
