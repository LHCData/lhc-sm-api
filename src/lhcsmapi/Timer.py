import time


class Timer(object):
    """Timer class measures time needed to execute operations in a given context.

    - Example::

          >>> from lhcsmapi.Timer import Timer
          >>> with Timer(): # doctest: +SKIP
          ...    for i in range(1000000):
          ...      i = i + 1
          Elapsed: 0.17786049842834473 s.

    In case the measured time be needed for further use, one can use time module.

    - Example::
          >>> import time
          >>> t = time.time()
          >>> for i in range(1000000):
          ...   i = i + 1
          >>> time.time() - t # doctest: +SKIP
          0.1149590015411377
    """

    def __enter__(self):
        """Magic method registering the time at the moment of entering the context manager

        :return: None returned, context manager object updated
        """
        self.tstart = time.time()

    def __exit__(self, type, value, traceback):
        """Magic method displaying the time of context execution at the moment of exiting the context manager

        :param type: not used
        :param value: not used
        :param traceback: not used

        :return: None returned, context manager object updated
        """
        print("Elapsed: {:.3f} s.".format(time.time() - self.tstart))
