from typing import Union, List

import pandas as pd

from lhcsmapi.Time import Time


class SignalIndexConversion(object):
    """SignalIndexConversion class contains static methods for processing dataframe indices"""

    @staticmethod
    def convert_indices(
        data_structure: Union[pd.Series, pd.DataFrame], conversion_function, unit="ns", tz=None
    ) -> Union[pd.Series, pd.DataFrame]:
        """Method applying a conversion function to the index of the signal's DataFrame

        :param data_structure: input data structure (either a Series or a DataFrame) with an index to convert
        :param conversion_function: converstion function (e.g., to seconds)
        :param unit: input index unit
        :param tz: time zone for conversion
        :return: output data structure with converted index
        """

        if isinstance(data_structure, (pd.DataFrame, pd.Series)):
            if not data_structure.empty:
                index = data_structure.index.tolist()
                new_index = [conversion_function(t, unit, tz) for t in index]
                data_structure.set_index([new_index], inplace=True)
            return data_structure
        else:
            raise TypeError(
                "Cannot convert the indices of input {}.\n"
                "The data structure must be a pandas DataFrame or Series.".format(type(data_structure))
            )

    @staticmethod
    def convert_indices_to_unix_timestamp(
        data_structure: Union[pd.Series, pd.DataFrame], unit="ns", tz=None
    ) -> Union[pd.Series, pd.DataFrame]:
        """Method converting the indices of a pandas DataFrame to unix time (ns) and returns the DataFrame

        :param data_structure: input data structure (either a Series or a DataFrame) with an index to convert
        :param unit: input index unit
        :param tz: time zone for conversion
        :return: output data structure with index converted to unix timestamp
        """
        return SignalIndexConversion.convert_indices(data_structure, Time.to_unix_timestamp, unit, tz)

    @staticmethod
    def convert_indices_to_sec(
        data_structure: Union[pd.Series, pd.DataFrame], unit="ns", tz=None
    ) -> Union[pd.Series, pd.DataFrame]:
        """Method converting the indices of a pandas DataFrame to unix time (s) and returns the DataFrame

        :param data_structure: input data structure (either a Series or a DataFrame) with an index to convert
        :param unit: input index unit
        :param tz: time zone for conversion
        :return: output data structure with index converted to unix timestamp
        """

        return SignalIndexConversion.convert_indices(data_structure, Time.to_unix_timestamp_in_sec, unit, tz)

    @staticmethod
    def convert_indices_to_timestamps(
        data_structure: Union[pd.Series, pd.DataFrame], unit="ns", tz=None
    ) -> Union[pd.Series, pd.DataFrame]:
        """Method converting the indices of a pandas DataFrame to pandas Timestamps and returns the DataFrame

        :param data_structure: input data structure (either a Series or a DataFrame) with an index to convert
        :param unit: input index unit
        :param tz: time zone for conversion
        :return: output data structure with index converted to unix timestamp
        """

        return SignalIndexConversion.convert_indices(data_structure, Time.to_pandas_timestamp, unit, tz)

    @staticmethod
    def synchronize_df(df: Union[pd.Series, pd.DataFrame], t0=None) -> Union[pd.Series, pd.DataFrame]:
        """Method synchronizes the indices of a pandas DataFrame to given t0

        :param df: either a DataFrame or a Series
        :param t0: time stamp index is synchronized to (if 0, the first index is taken)
        :return:
        """

        t0 = df.index[0] if t0 is None else t0

        new_index = [idx - t0 for idx in df.index]
        df.set_index([new_index], inplace=True)
        return df

    @staticmethod
    def synchronize_dfs(dfs: List[pd.DataFrame]) -> List[pd.DataFrame]:
        return [SignalIndexConversion.synchronize_df(df, df.index[0]) for df in dfs]
