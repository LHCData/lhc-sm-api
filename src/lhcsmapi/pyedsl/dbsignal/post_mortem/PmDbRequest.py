from typing import List, Optional, Tuple

import requests
import json
import warnings
import pandas as pd
import numpy as np
import functools
from lhcsmapi.Time import Time


class PmDbRequest(object):
    """PmDbRequest class encapsulates PM database queries in order to simplify the query creation as well as browsing of
    PM events for a given period.
    """

    _request_types = ["pmevent", "pmdata", "pmdata/signal", "analysisResult"]
    _keyword_to_value_type = {
        "system": str,
        "className": str,
        "source": str,
        "signal": str,
        "fromTimestampInNanos": int,
        "timestampInNanos": int,
        "durationInNanos": int,
    }

    @staticmethod
    def generate_request(request_type: str, is_header: bool = False, pm_rest_api_path: Optional[str] = None, **kwargs):
        """**Generates a PostMortem request**

        Method generates a HTTP REST Post Mortem request query for three request types, with/without PM header,
        and for a given timestamp or a timestamp and duration.

        :param request_type: type of PM request (supported: pmdata, pmdata/signal, pmevent)
        :param is_header: flag indicating whether (if True) or not (if False) PM header should be requested
        :param kwargs: dictionary of (keyword: argument) pairs which parametrize a db API query
        :return: string with a PM HTTP request

        - Example::

            >>> from lhcsmapi.pyedsl.dbsignal.post_mortem.PmDbRequest import PmDbRequest
            >>> source = "*"
            >>> system = "QPS"
            >>> className = "DQAMCNMB_PMHSU"
            >>> selectedDayTimeStamp = 1426201200000000000
            >>> eventDurationSeconds = 86400000000000
            >>> PmDbRequest.generate_request("pmdata", False, source=source, system=system, className=className, fromTimestampInNanos=selectedDayTimeStamp, duration=eventDurationSeconds) # doctest: +SKIP
            http://pm-rest.cern.ch/v2/pmdata/within/duration?source=*&system=QPS&className=DQAMCNMB_PMHSU&fromTimestampInNanos=1426201200000000000&durationInNanos=86400000000000

        - Expected Fail Response::

            >>> from lhcsmapi.pyedsl.dbsignal.post_mortem.PmDbRequest import PmDbRequest
            >>> source = "*"
            >>> system = "QPS"
            >>> className = "DQAMCNMB_PMHSU"
            >>> selectedDayTimeStamp = 1426201200000000000
            >>> eventDurationSeconds = 86400000000000
            >>> PmDbRequest.generate_request("pmdata", False, source=source, system=system, className=className, fromTimestampInNanos=selectedDayTimeStamp, duration=eventDurationSeconds) # doctest: +SKIP
            Traceback (most recent call last):
            ...
            ValueError: Some keywords: {'duration'} not in dict_keys(['system', 'className', 'source', 'signal', 'fromTimestampInNanos', 'timestampInNanos', 'durationInNanos'])!

        """
        if pm_rest_api_path is None:
            pm_rest_api_path = "http://pm-rest.cern.ch/v2/"

        # check if keys are present
        key_difference = kwargs.keys() - PmDbRequest._keyword_to_value_type.keys()
        if len(key_difference) > 0:
            raise ValueError(f"Some keywords: {key_difference} not in {PmDbRequest._keyword_to_value_type.keys()}!")

        # check if value types are correct

        # for time setting check if it is either timestampInNanos and not (fromTimestampInNanos or durationInNanos)
        if "timestampInNanos" in kwargs and any(type in ["fromTimestampInNanos", "durationInNanos"] for type in kwargs):
            raise ValueError(
                "If timestampInNanos is present, then both fromTimestampInNanos and "
                "durationInNanos have to be absent and vice versa!"
            )

        # concatenate keys and values for a query
        keywords_and_argument_concat = "&".join([f"{key}={value}" for (key, value) in kwargs.items()])

        if not any([request_type in PmDbRequest._request_types]):
            raise ValueError(f"PM request type {request_type} not supported.")

        request = f"{pm_rest_api_path}{request_type}"

        if is_header:
            request = request + "/header"

        if "durationInNanos" in kwargs:
            return f"{request}/within/duration?{keywords_and_argument_concat}"
        else:
            return f"{request}?{keywords_and_argument_concat}"

    @staticmethod
    @functools.lru_cache(maxsize=16, typed=False)
    def get_response(
        request_type: str,
        is_header: bool = False,
        display_warning: bool = False,
        pm_rest_api_path: Optional[str] = None,
        **kwargs,
    ):
        """**Method generates a PM request**

        Method returns content of an REST HTTP Post Mortem request query for three request types, with/without PM header,
        and for a given timestamp or a timestamp and duration.

        :param request_type: type of PM request (supported: pmdata, pmdata/signal, pmevent)
        :param is_header: flag indicating whether (if True) or not (if False) PM header should be requested
        :param kwargs: dictionary of (keyword: argument) pairs which parametrize a db API query
        :return: string with a PM HTTP request

        - Example::

            >>> from lhcsmapi.pyedsl.dbsignal.post_mortem.PmDbRequest import PmDbRequest
            >>> source = "*"
            >>> system = "QPS"
            >>> className = "DQAMCNMB_PMHSU"
            >>> selectedDayTimeStamp = 1426201200000000000
            >>> eventDurationSeconds = 86400000000000
            >>> PmDbRequest.get_response("pmdata", False, source=source, system=system, className=className, fromTimestampInNanos=selectedDayTimeStamp, durationInNanos=eventDurationSeconds) # doctest: +SKIP
            {'content': [{'valueDescriptor': {'attributes': {'QUENCHTIME': {'description': {'signal': 'description',
            ...

        - Expected Fail Response::

            >>> from lhcsmapi.pyedsl.dbsignal.post_mortem.PmDbRequest import PmDbRequest
            >>> source = "*"
            >>> system = "QPS"
            >>> className = "DQAMCNMB_PMHSU"
            >>> selectedDayTimeStamp = 1426201200000000000
            >>> eventDurationSeconds = 86400000000000
            >>> PmDbRequest.get_response("pmdata", False, source=source, system=system, className=className, fromTimestampInNanos=selectedDayTimeStamp, duration=eventDurationSeconds) # doctest: +SKIP
            Traceback (most recent call last):
            ...
            ValueError: Some keywords: {'duration'} not in dict_keys(['system', 'className', 'source', 'signal', 'fromTimestampInNanos', 'timestampInNanos', 'durationInNanos'])!

            >>> from lhcsmapi.pyedsl.dbsignal.post_mortem.PmDbRequest import PmDbRequest
            >>> source = "*"
            >>> system = "QPS"
            >>> className = "DQAMCNMB_PMHSU"
            >>> selectedDayTimeStamp = 1426201200000000000
            >>> eventDurationSeconds = 86400
            >>> PmDbRequest.get_response("pmdata", False, source=source, system=system, className=className, fromTimestampInNanos=selectedDayTimeStamp, durationInNanos=eventDurationSeconds) # doctest: +SKIP
            Traceback (most recent call last):
            ...
            UserWarning: Querying Post Mortem failed using the following query: http://pm-rest.cern.ch/v2/pmdata/within/duration?source=*&system=QPS&className=DQAMCNMB_PMHSU&fromTimestampInNanos=1426201200000000001&durationInNanos=86400

        """

        request = PmDbRequest.generate_request(request_type, is_header, pm_rest_api_path, **kwargs)
        response = requests.get(request)

        try:
            if response.status_code == 204:
                if display_warning:
                    warning = f"Post Mortem returned no data for the following query: {request}"
                    warnings.warn(warning)
                return ""
            return json.loads(response.text)
        except json.decoder.JSONDecodeError:
            warning = f"Querying Post Mortem failed using the following query: {request}"
            if display_warning:
                warnings.warn(warning)
            return ""

    @staticmethod
    def find_events(
        source: str,
        system: str,
        className: str,
        t_start: int,
        t_end: Optional[int] = None,
        duration: Optional[int] = None,
        pm_rest_api_path: Optional[str] = None,
        display_warning: bool = False,
    ):
        """**Finds PM events for a given source, system, className as well as start and end time**

        Method returns content of an REST HTTP Post Mortem request query for three request types, with/without PM header,
        and for a given timestamp or a timestamp and duration. Three operation modes are possible:

        - if endTimeData is not None:

        (t_start, t_end) = (startTimeData, endTimeData)

        - elif len(durationData) is 1:

        (t_start, t_end) = (startTimeData, startTimeData+durationData[0])

        - elif len(durationData) is 2:

        (t_start, t_end) = (startTimeData-durationData[0], startTimeData+durationData[1])

        :param source: PM event source
        :param system: PM event system
        :param className: PM event class signal
        :param t_start: input date and time
        :param t_end: unit of the input (None if duration defined)
        :param duration: time zone (None if t_end defined)
        :return: list of (source, timestamp) pairs

        - Example::

            >>> from lhcsmapi.pyedsl.dbsignal.post_mortem.PmDbRequest import PmDbRequest
            >>> source = "*"
            >>> system = "QPS"
            >>> className = "DQAMCNMB_PMHSU"
            >>> selectedDayTimeStamp = 1426201200000000000
            >>> eventDurationSeconds = 24*60*60

            >>> PmDbRequest.find_events(source, system, className, eventTime=selectedDayTimeStamp, duration=[(eventDurationSeconds, 's')]) # doctest: +SKIP
            [('B20L5', 1426220469491000000),
            ...

        - Expected Fail Response::

            >>> from lhcsmapi.pyedsl.dbsignal.post_mortem.PmDbRequest import PmDbRequest
            >>> source = "*"
            >>> system = "QPS"
            >>> className = "DQAMCNMB_PMHSU"
            >>> selectedDayTimeStamp = 1426201200000000000
            >>> eventDurationSeconds = 1
            >>> PmDbRequest.find_events(source, system, className, eventTime=selectedDayTimeStamp, duration=[(eventDurationSeconds, 's')]) # doctest: +SKIP
            Querying Post Mortem failed using the following query:
                            http://pm-rest.cern.ch/v2/pmdata/header/within/duration?system=QPS&className=DQAMCNMB_PMHSU&source=*&fromTimestampInNanos=1426201200000000000&durationInNanos=1000000000 returned no data so an empty string is being returned

        """

        from_timestamp_in_nanos, end_time_in_nanos = Time.get_query_period_in_unix_time(t_start, t_end, duration)
        max_duration = int(24 * 60 * 60 * 1e9)
        duration_in_nanos = end_time_in_nanos - from_timestamp_in_nanos
        duration_in_nanos = min(duration_in_nanos, max_duration)

        # Fetch PM event data
        pm_headers = PmDbRequest.get_response(
            "pmdata",
            True,
            display_warning,
            pm_rest_api_path,
            system=system,
            className=className,
            source=source,
            fromTimestampInNanos=from_timestamp_in_nanos,
            durationInNanos=duration_in_nanos,
        )

        source_key = "source"
        timestamp_key = "timestamp"

        event_list = []
        for pm_header in pm_headers:
            if "DT" not in pm_header[source_key]:
                event_list.append((pm_header[source_key], pm_header[timestamp_key]))

            # if 'DT' in 'source', then rubbish data injected into official PM data

        return event_list

    @staticmethod
    def find_events_between_dates(t_start, t_end, className: str, source: str, system: str) -> List[Tuple[str, int]]:
        """Method finding events between two dates. It is assumed that t_start is greater than t_end. The difference
        between start and end can be more than a day.

        :param t_start: start time for the search
        :param t_end: end time for the search
        :param className: PM class name
        :param source: PM source name
        :param system: PM system name
        :return: list with tuples, each tuple contains a source name and a corresponding timestamp
        """
        quotient, remainder = Time.modulo_day_in_unix_timestamp(t_start, t_end)
        event_duration_nano_seconds = int(24 * 60 * 60 * 1e9)
        event_source_and_timestamp = []
        for day in range(quotient):
            start_time_temp = Time.to_unix_timestamp(t_start) + int(day * event_duration_nano_seconds)
            event_source_and_timestamp += PmDbRequest.find_events(
                source, system, className, t_start=start_time_temp, duration=[(event_duration_nano_seconds, "ns")]
            )
        if remainder > 0:
            start_time_temp = Time.to_unix_timestamp(t_end) - remainder
            event_source_and_timestamp += PmDbRequest.find_events(
                source, system, className, t_start=start_time_temp, duration=[(remainder, "ns")]
            )
        return event_source_and_timestamp

    @staticmethod
    def query_context(t_start, t_end, system: str, className: str, source: str, parameters: List[str]) -> pd.DataFrame:
        """Method querying BLM context variables from PM. The difference between t_end and t_start should be less than
        24 hours.

        :param t_start: query start
        :param t_end: query end
        :param system: BLM PM system
        :param className: BLM PM className
        :param source: BLM PM source
        :param parameters: a list of parameters to extract from a PM dump
        :return: a DataFrame with parameters obtained with a PM query
        """
        duration = t_end - t_start
        response = PmDbRequest.get_response(
            "pmdata",
            False,
            True,
            system=system,
            className=className,
            source=source,
            fromTimestampInNanos=t_start,
            durationInNanos=duration,
        )
        parameter_to_value = {}

        if response == "":
            parameter_to_value = {parameter: np.nan for parameter in parameters}
            return pd.DataFrame(parameter_to_value, index=[0])

        if system in ["BLM", "LDBS"]:
            for entry in response["content"][0]["namesAndValues"]:
                for parameter in parameters:
                    if entry["name"] == parameter:
                        parameter_to_value[parameter] = entry["value"]
        elif system == "LBDS":
            for entry in response["content"][0]["namesAndValues"]:
                for parameter in parameters:
                    if entry["name"] == parameter:
                        parameter_to_value[parameter] = max(entry["value"])
            # check for signal validity
            for entry in response["content"][0]["namesAndValues"]:
                if (entry["name"] == "dataValid") and (entry["value"] == "False"):
                    parameter_to_value = {parameter: 0.0 for parameter in parameters}
        elif system == "LHC":
            parameter_list = []
            for entry in response["content"][0]["namesAndValues"]:
                if entry["name"] == "registerNames":
                    parameter_list = entry["value"]
            for entry in response["content"][0]["namesAndValues"]:
                if entry["name"] == "registerValues":
                    for parameter in parameters:
                        index = parameter_list.index(parameter)
                        parameter_to_value[parameter] = entry["value"][index]

        timestamp = response["content"][0]["rawDataEntry"]["pmEventStamp"]

        if all(not isinstance(value, (list, tuple)) for value in parameter_to_value.values()):
            return pd.DataFrame(parameter_to_value, index=[timestamp])
        else:
            df = pd.DataFrame(parameter_to_value)
            df["timestamp"] = timestamp
            return df

    @staticmethod
    def get_blm_signal(t_start, t_end, system: str, className: str, source: str, signal: str) -> pd.DataFrame:
        """Method querying a signal from PM for all BLMs in the bucket.
        It is assumed that t_end is after t_start and the difference between both is less than 24 hours.

        :param t_start: start time of the query
        :param t_end: end time of the query
        :param system: BLM PM system name
        :param className: BLM PM className name
        :param source: BLM PM source name
        :param signal: BLM PM signal name
        :return: a dataframe with the selected signal for all BLMs in the PM bucket
        """
        duration = t_end - t_start

        response = PmDbRequest.get_response(
            "pmdata",
            False,
            True,
            system=system,
            className=className,
            source=source,
            fromTimestampInNanos=t_start,
            durationInNanos=duration,
        )
        blm_lst = list(
            PmDbRequest.query_context(
                t_start=t_start, t_end=t_end, system=system, className=className, source=source, parameters=["blmNames"]
            )["blmNames"].values
        )
        blm_to_signal = {}
        for entry in response["content"][0]["namesAndValues"]:
            if entry["name"] == signal:
                for blm_lst_el in blm_lst:
                    index = blm_lst.index(blm_lst_el)
                    blm_to_signal[blm_lst_el] = entry["value"][index]

        return pd.DataFrame(blm_to_signal)

    @staticmethod
    def get_bpm_header(t_start, t_end, system: str, className: str, source: str, parameter: str) -> str:
        """Method returning a parameter form BPM PM header

        :param t_start: start of a query
        :param t_end: end of a query
        :param system: BPM PM system name
        :param className: BPM PM className
        :param source: BPM PM source
        :param parameter: BPM PM header parameter
        :return: BPM PM header parameter value
        """
        duration = t_end - t_start

        response = PmDbRequest.get_response(
            "analysisResult",
            True,
            True,
            system=system,
            className=className,
            source=source,
            fromTimestampInNanos=t_start,
            durationInNanos=duration,
        )
        return response[0][parameter]

    @staticmethod
    def get_bpm_signal(t_start, t_end, system: str, className: str, source: str, parameter: str) -> pd.DataFrame:
        """Method querying a signal from PM for all BLMs in the bucket.
        It is assumed that t_end is after t_start and the difference between both is less than 24 hours.

        :param t_start: start of a query
        :param t_end: end of a query
        :param system: BPM PM system name
        :param className: BPM PM className
        :param source: BPM PM source
        :param parameter: BPM PM header parameter value
        :return: a DataFrame with a BPM signal
        """
        duration = t_end - t_start

        response = PmDbRequest.get_response(
            "analysisResult",
            False,
            True,
            system=system,
            className=className,
            source=source,
            fromTimestampInNanos=t_start,
            durationInNanos=duration,
        )
        param = None
        for entry in response["content"][0]["namesAndValuesTypes"]:
            if entry["name"] == parameter:
                param = np.asarray(entry["value"])

        return pd.DataFrame(param)
