import math
import warnings
from copy import deepcopy
from typing import List, Tuple

import matplotlib.pyplot as plt
import matplotlib.patches as patches
from sklearn.linear_model import LinearRegression
import numpy as np
import pandas as pd
from IPython.display import display, HTML

from lhcsmapi.analysis.warnings import warning_on_one_line

warnings.formatwarning = warning_on_one_line


class Assertion(object):
    """Fundamental class built with AssertionBuilder.
    Its instances contain information about signals and optionally start and end time to consider.

    """

    def set_dfs(self, dfs: List[pd.DataFrame]):
        """Method setting a list of dataframes for assertion

        :param dfs: list of pd.Dataframes
        """
        self.dfs = dfs

    def set_time_range(self, t_start: float, t_end: float):
        """Method setting time range for assertion

        :param t_start: start of signal interval for assertion
        :param t_end: end of signal interval for assertion
        """
        self.t_start = t_start
        self.t_end = t_end


class AssertionBuilder(object):
    """Builder class for the Assertion objects.
    It works either with signals or with features.

    """

    def __init__(self):
        self.assertion = Assertion()

    def with_signal(self, dfs):
        """Method initializing the Assertion class with a list of signals

        :param dfs: list of signals to assert
        :return: an AssertionBuilderSignal object with set list of signals
        """
        self.assertion.set_dfs(dfs)

        return AssertionBuilderSignal(self.assertion)

    def with_feature(self, dfs):
        """Method initializing the Assertion class with a list of feature DataFrames

        :param dfs: list of features to assert
        :return: an AssertionBuilderFeatures object with set list of features
        """
        self.assertion.set_dfs(dfs)

        return AssertionBuilderFeature(self.assertion)


def plot_with_value_range(dfs: List[pd.DataFrame], value_min: float, value_max: float) -> Tuple[plt.Figure, plt.Axes]:
    """Function plotting a list of signals between minimum and maximum values

    :param dfs: list of dataframes to plot
    :param value_min: minimum value
    :param value_max: maximum value
    :return: tuple containing a reference to a figure and axes
    """
    fig, ax = plt.subplots(figsize=(13, 6.5))

    for df in dfs:
        if not df.empty:
            df.plot(ax=ax, grid=True)

    ax.legend(loc="lower left")
    ax.tick_params(labelsize=15)
    ax.set_xlabel("time, [s]", fontsize=15)

    ax.axhspan(ymin=value_min, ymax=value_max, facecolor="xkcd:yellowgreen", alpha=0.5)

    y_lim_min = 0.9 * value_min if value_min > 0 else 1.1 * value_min
    y_lim_max = 1.1 * value_max if value_max > 0 else 0.9 * value_max

    ax.set_ylim((y_lim_min, y_lim_max))

    return fig, ax


class AssertionBuilderFeature(object):
    """Class with Assertion object initialized with a feature DataFrame
    It provides a method for checking if a feature is between a minimum and maximum value.
    Additional assertions can be added as needed.
    """

    def __init__(self, assertion):
        self.assertion = assertion

    def has_min_max_value(self, feature: pd.DataFrame, value_min: float, value_max: float):
        """Method checking if a selected feature has value between min and max.

        :param feature: a feature to assert
        :param value_min: minimum value
        :param value_max: maximum value
        :return: an AssertionBuilderFeaturePlot object allowing for further plot customization.
        """
        features_outside_range = self.assertion.dfs[
            (self.assertion.dfs[feature] < value_min) | (self.assertion.dfs[feature] > value_max)
        ]
        if len(features_outside_range) > 0:
            warnings.warn("The following features are outside of range.", stacklevel=2)
            display(HTML(features_outside_range.to_html()))

        fig, ax = plt.subplots(figsize=(13, 6.5))
        self.assertion.dfs[feature].plot(figsize=(13, 6.5), kind="bar", color="red", grid=True, ax=ax)
        ax.tick_params(labelsize=15)
        ax.axhspan(ymin=value_min, ymax=value_max, facecolor="xkcd:yellowgreen", alpha=0.5)

        return AssertionBuilderFeaturePlot(fig, ax, features_outside_range)


def plot_between_two_references(dfs: List[pd.DataFrame], envelope_df: pd.DataFrame) -> Tuple[plt.Figure, plt.Axes]:
    """Function plotting a list of signals with an envelope representing the allowed range of values.

    :param dfs: list of signals to plot
    :param envelope_df: a dataframe with two columns: envelope_neg and envelope_pos to represent an envelope
    :return: tuple containing a reference to a figure and axes
    """
    fig, ax = plt.subplots(figsize=(13, 6.5))

    for df in dfs:
        df.plot(ax=ax, grid=True)

    ax.legend(loc="lower left")
    ax.set_xlabel("time, [s]", fontsize=15)
    ax.tick_params(labelsize=15)

    ax.fill_between(
        envelope_df.index, envelope_df["envelope_pos"], envelope_df["envelope_neg"], color="xkcd:yellowgreen", alpha=0.5
    )

    return fig, ax


class AssertionBuilderSignal(object):
    """Class with Assertion object initialized with a feature DataFrame
    It provides methods to define time range for an assertion and/or perform an assertion:
    - check if a value is between min and max
    - compare a signal to a reference
    - check if a signal is within an envelope of two references
    Additional assertions can be added as needed.
    """

    def __init__(self, assertion):
        self.assertion = assertion

    def with_time_range(self, t_start: float, t_end: float):
        """Method setting a time range for an assertion

        :param t_start: start of signal interval for assertion
        :param t_end: end of signal interval for assertion

        :return: an AssertionBuilderSignalTimeRange object with time definition
        """
        self.assertion.set_time_range(t_start, t_end)

        return AssertionBuilderSignalTimeRange(self.assertion)

    def for_time_greater_than(self, t_greater):
        """Method setting a minimum time for an assertion

        :param t_greater: start of signal interval for assertion

        :return: an AssertionBuilderSignalTimeRange object with time definition
        """
        self.assertion.set_time_range(t_greater, None)

        return AssertionBuilderSignalTimeRange(self.assertion)

    def has_min_max_value(self, value_min: float, value_max: float, is_warning: bool = True):
        """Method checking if a signal has value between min and max.

        :param value_min: minimum value
        :param value_max: maximum value
        :return: an AssertionBuilderSignalPlot object allowing for further plot customization.
        """
        is_signal_correct = True
        for df in self.assertion.dfs:
            col = df.columns[0]
            df_above_thres = df[(df[col] < value_min) | (df[col] > value_max)]
            if len(df_above_thres) > 0:
                message = "{} outside of the [{}, {}] threshold".format(col, value_min, value_max)
                if is_warning:
                    warnings.warn(message, stacklevel=2)
                else:
                    print(message)
                is_signal_correct = False

        # Plot
        fig, ax = plot_with_value_range(self.assertion.dfs, value_min, value_max)
        return AssertionBuilderSignalPlot(fig, ax, is_signal_correct)

    def compare_to_reference(self, signal_ref_dfs: List[pd.DataFrame], abs_margin: float, scaling=1):
        """Method comparing a list of signals to a list of references subject to scaling.
        The comparison is carried out point by point. If an absolute difference is above a margin, a warning is printed.

        :param signal_ref_dfs: list of reference signals. It is assumed that the length is the same as
        the list of signals for an assertion.
        :param abs_margin: absolute margin from an asserted signal and its reference
        :param scaling: scaling for the reference signal (1 by default)
        :return: an AssertionBuilderSignalPlot object
        """
        fig, ax = plt.subplots(figsize=(13, 6.5))
        is_signal_correct = True
        for signal_df, signal_ref_df in zip(self.assertion.dfs, signal_ref_dfs):
            if not signal_df.empty and not signal_ref_df.empty:
                col = signal_ref_df.columns[0]
                col_ref = "{}_REF".format(col)
                col_min = "{}_MIN".format(col)
                col_max = "{}_MAX".format(col)
                signal_ref_df = signal_ref_df * scaling
                signal_ref_df = signal_ref_df.rename(columns={col: col_ref})
                idx = signal_ref_df.index.union(signal_df.index)
                signal_comparison_df = signal_ref_df.reindex(idx).interpolate("index")
                signal_comparison_df = signal_comparison_df.join(signal_df).dropna()

                signal_comparison_df[col_min] = signal_comparison_df[col_ref] - abs_margin
                signal_comparison_df[col_max] = signal_comparison_df[col_ref] + abs_margin

                # Compare two curves
                signal_comparison_df["Comparison"] = signal_comparison_df.apply(
                    lambda row: (row[col] >= row[col_min]) and (row[col] <= row[col_max]), axis=1
                )

                if any(signal_comparison_df["Comparison"] == False):
                    warnings.warn("Signal {0} outside of range [-{1}, {1}]!".format(col, abs_margin), stacklevel=2)
                    is_signal_correct = False

                # Plot
                signal_df.plot(ax=ax)
                signal_ref_df.plot(ax=ax, grid=True, style="--")

                ax.fill_between(
                    signal_comparison_df.index,
                    signal_comparison_df[col_min],
                    signal_comparison_df[col_max],
                    color="xkcd:yellowgreen",
                    alpha=0.5,
                )
                ax.tick_params(labelsize=15)
                ax.set_xlabel("time, [s]", fontsize=15)

        return AssertionBuilderSignalPlot(fig, ax, is_signal_correct)

    def is_between_two_references(
        self,
        first_ref: pd.DataFrame,
        second_ref: pd.DataFrame,
        first_scaling=1,
        second_scaling=1,
        first_offset=0,
        second_offset=0,
        corr_thr=5e-3,
        is_plot=True,
    ):
        """Method checking if the list of signal is between two references. References may be subject to
        an additional offset and scaling. Method assumes that first_ref and second_ref have the same index.

        :param first_ref: first reference signal
        :param second_ref: second reference signal
        :param first_scaling: scaling for the first reference signal
        :param second_scaling: scaling for the second reference signal
        :param first_offset: offset for the first reference signal
        :param second_offset: offset for the second reference signal
        :param corr_thr: correction threshold to avoid comparing low values subject to low signal-to-noise ratio
        :param is_plot: flag to control plot display: if True, then a plot is displayed, otherwise plot is hidden
        :return: an AssertionBuilderSignalPlot object
        """
        # assumes that first_ref and second_ref have the same index
        envelope_df = deepcopy(first_ref)
        envelope_df["envelope_pos"] = first_ref * first_scaling + first_offset
        envelope_df["envelope_neg"] = second_ref * second_scaling - second_offset
        envelope_df = envelope_df.drop(columns=first_ref.columns)

        is_signal_correct = True
        self.assertion.dfs = self.assertion.dfs if isinstance(self.assertion.dfs, list) else [self.assertion.dfs]
        for u_res_df in self.assertion.dfs:
            col = u_res_df.columns[0]
            print("Analysing {}".format(col))
            idx = envelope_df.index.union(u_res_df.index)
            envelope_df_analysis = envelope_df.reindex(idx).interpolate("index")
            envelope_df_analysis = envelope_df_analysis.join(u_res_df).dropna()
            envelope_df_analysis["comparison"] = envelope_df_analysis.apply(
                lambda row: (row[col] >= row["envelope_neg"]) and (row[col] <= row["envelope_pos"]), axis=1
            )
            # Correction for low values
            envelope_df_analysis.loc[abs(envelope_df_analysis[col]) < corr_thr, "comparison"] = True
            if len(envelope_df_analysis[envelope_df_analysis["comparison"] == False]) > 0:
                warnings.warn(
                    "{} outside of the lower: {}*{}, upper: {}*{} reference".format(
                        col, first_ref.columns[0], first_scaling, second_ref.columns[0], second_scaling
                    ),
                    stacklevel=2,
                )
                is_signal_correct = False
        if is_plot:
            fig, ax = plot_between_two_references(self.assertion.dfs, envelope_df)
        else:
            fig, ax = None, None
        return AssertionBuilderSignalPlot(fig, ax, is_signal_correct)


def plot_with_time_range(
    dfs: List[pd.DataFrame], t_start: List[float], t_end: List[float]
) -> Tuple[plt.Figure, plt.Axes]:
    """Function plotting a list of signals with a box for start nad end time

    :param dfs: list of signals to plot
    :param t_start: start of signal interval for assertion
    :param t_end: end of signal interval for assertion
    :return: tuple containing a reference to a figure and axes
    """
    fig, ax = plt.subplots(figsize=(13, 6.5))

    for df in dfs:
        df.plot(ax=ax, grid=True)

    ax.legend(loc="lower left")
    ax.tick_params(labelsize=15)
    ax.set_xlabel("time, [s]", fontsize=15)

    for t_s, t_e in zip(t_start, t_end):
        ax.axvspan(xmin=t_s, xmax=t_e, facecolor="xkcd:goldenrod")

    return fig, ax


class AssertionBuilderSignalTimeRange(object):
    """Class providing assertion methods for a signal with time range definition.
    Available methods are:
    - has_min_max_variation
    - has_min_max_slope
    - has_min_max_value

    """

    def __init__(self, assertion):
        self.assertion = assertion

    def has_min_max_variation(self, variation_min_max: float):
        """Method checking if a difference between a min and max value is below provided threshold.

        :param variation_min_max: threshold for difference between a min and max value
        :return: an AssertionBuilderSignalPlot object allowing for further plot customization.
        """
        t_start = self.assertion.t_start
        t_end = self.assertion.t_end

        # Assertion
        is_signal_correct = True
        for df in self.assertion.dfs:
            col = df.columns[0]
            for t_s, t_e in zip(t_start, t_end):
                slice_df = df[(df.index > t_s) & (df.index < t_e)]
                variation = slice_df.max() - slice_df.min()
                if variation.values[0] > variation_min_max:
                    warnings.warn(
                        "The variation of {} ({:.1f} %) exceeds {} % for constant current from {} to {} s".format(
                            col, variation_min_max, variation.values[0], t_s, t_e
                        ),
                        stacklevel=2,
                    )
                    is_signal_correct = False

        # Plot
        fig, ax = plot_with_time_range(self.assertion.dfs, t_start, t_end)
        return AssertionBuilderSignalPlot(fig, ax, is_signal_correct)

    def has_min_max_slope(self, slope_min: float, slope_max: float, scaling: float = 3600 * 1000, unit: str = "mV/h"):
        """Method checking if a signal has slope between min and max. Method assumes that signals
        being asserted are expressed in base SI unit (by default V) and time unit is second.

        :param slope_min: minimum value
        :param slope_max: maximum value
        :param scaling: scaling coefficient to convert signal first derivative from X/s to mX/h
        (by default V/s to mV/h)
        :param unit: unit of the min/max slope
        :return: an AssertionBuilderSignalPlot object allowing for further plot customization.
        """
        t_start = self.assertion.t_start
        t_end = self.assertion.t_end

        # Assertion
        is_signal_correct = True
        for df in self.assertion.dfs:
            col = df.columns[0]
            for t_s, t_e in zip(t_start, t_end):
                slice_df = df[(df.index > t_s) & (df.index < t_e)]
                slope = (
                    LinearRegression().fit(np.array(slice_df.index).reshape((-1, 1)), slice_df[col]).coef_[0] * scaling
                )
                if (slope < slope_min) or (slope > slope_max):
                    warnings.warn(
                        "The drift of {} is {:.3f} {} for constant current from {} to {} s".format(
                            col, slope, unit, t_s, t_e
                        ),
                        stacklevel=2,
                    )
                    is_signal_correct = False

        # Plot
        fig, ax = plot_with_time_range(self.assertion.dfs, t_start, t_end)
        return AssertionBuilderSignalPlot(fig, ax, is_signal_correct)

    def has_min_max_value(self, value_min: float = 0, value_max: float = math.inf):
        """Method checking if a signal has value between min and max.

        :param value_min: minimum value
        :param value_max: maximum value
        :return: an AssertionBuilderSignalPlot object allowing for further plot customization.
        """
        t_start = self.assertion.t_start if isinstance(self.assertion.t_start, list) else [self.assertion.t_start]
        t_end = self.assertion.t_end if isinstance(self.assertion.t_end, list) else [self.assertion.t_end]

        # Assertion
        is_signal_correct = True
        for df in self.assertion.dfs:
            col = df.columns[0]
            for t_s, t_e in zip(t_start, t_end):
                if t_e is not None:
                    slice_df = df[(df.index > t_s) & (df.index < t_e)]
                else:
                    slice_df = df[(df.index > t_s)]
                df_above_thres = slice_df[(slice_df[col] < value_min) | (slice_df[col] > value_max)]
                if len(df_above_thres) > 0:
                    warnings.warn(
                        "{} outside of the [{}, {}] threshold".format(col, value_min, value_max), stacklevel=2
                    )
                    is_signal_correct = False

        fig, ax = plot_with_time_range_min_max_value(self.assertion.dfs, t_start, t_end, value_min, value_max)
        return AssertionBuilderSignalPlot(fig, ax, is_signal_correct)


def plot_with_time_range_min_max_value(
    dfs: List[pd.DataFrame], t_start: List[float], t_end: List[float], value_min: float, value_max: float
) -> Tuple[plt.Figure, plt.Axes]:
    """Function plotting signals with a box representing min/max value as well as t_start/t_end

    :param dfs: list of signals
    :param t_start: start time
    :param t_end: end time
    :param value_min: minimum value
    :param value_max: maximum value
    :return: tuple containing a reference to a figure and axes
    """
    fig, ax = plt.subplots(figsize=(13, 6.5))

    for df in dfs:
        df.plot(ax=ax, grid=True)

    ax.legend(loc="lower left")
    ax.tick_params(labelsize=15)
    ax.set_xlabel("time, [s]", fontsize=15)
    for t_s, t_e in zip(t_start, t_end):
        t_e = dfs[-1].index[-1] if t_e is None else t_e
        rect = patches.Rectangle((t_s, value_min), t_e - t_s, value_max - value_min, facecolor="xkcd:goldenrod")
        # Add the patch to the Axes
        ax.add_patch(rect)

    return fig, ax


class AssertionBuilderSignalPlot(object):
    """Class with signal assertion result for further plot customization"""

    def __init__(self, fig, ax, is_signal_correct):
        self.is_signal_correct = is_signal_correct
        self.ax = ax
        self.fig = fig

    def show_plot(self, **kwargs):
        """Method showing plot and providing keyword-argument dictionary for plot customizations:
        - vertical lines
        - y label
        - title
        - y lim
        - secondary y axis with signal, label, drawing style

        :param kwargs: dictionary with plot customizations
        :return: an AssertionBuilderSignalPlotResult object with assertion result
        """
        if "axvline" in kwargs:
            self.ax.axvline(kwargs["axvline"], ls="--")

        if "ylabel" in kwargs:
            self.ax.set_ylabel(kwargs["ylabel"], fontsize=15)

        if "title" in kwargs:
            self.fig.suptitle(kwargs["title"], fontsize=20)

        if "xlim" in kwargs:
            self.ax.set_xlim(kwargs["xlim"])

        if "ylim" in kwargs:
            self.ax.set_ylim(kwargs["ylim"])

        if "data_sec" in kwargs:
            ax2 = self.ax.twinx()
            ax2.tick_params(labelsize=15)
            data_sec = kwargs["data_sec"]
            data_sec = data_sec if isinstance(data_sec, list) else [data_sec]

            if any([not data_sec_el.empty for data_sec_el in data_sec]):
                for data_sec_el in data_sec:
                    if not data_sec_el.empty:
                        if "drawstyle_sec" in kwargs:
                            data_sec_el.plot(ax=ax2, drawstyle=kwargs["drawstyle_sec"], color="C2")
                        else:
                            data_sec_el.plot(ax=ax2)

                if "ylabel_sec" in kwargs:
                    ax2.set_ylabel(kwargs["ylabel_sec"], fontsize=15, color="C3")
                ax2.legend(loc="upper right")

        plt.show()

        return AssertionBuilderSignalPlotResult(self.is_signal_correct)

    def get_assertion_result(self):
        return self.is_signal_correct


class AssertionBuilderSignalPlotResult(object):
    """Class stores signal assertion result after plotting for a retrieval"""

    def __init__(self, is_signal_correct: bool):
        self.is_signal_correct = is_signal_correct

    def get_assertion_result(self) -> bool:
        return self.is_signal_correct


class AssertionBuilderFeaturePlot(object):
    """Class with feature assertion result for further plot customization"""

    def __init__(self, fig, ax, features_outside_range):
        self.fig = fig
        self.ax = ax
        self.features_outside_range = features_outside_range

    def show_plot(self, **kwargs):
        """Method showing plot and providing keyword-argument dictionary for plot customizations:
        - vertical lines
        - y label
        - title
        - y lim
        - secondary y axis with signal, label, drawing style

        :param kwargs: dictionary with plot customizations
        :return: an AssertionBuilderFeaturePlotResult object with assertion result
        """
        if "ylabel" in kwargs:
            self.ax.set_ylabel(kwargs["ylabel"], fontsize=15)

        if "title" in kwargs:
            self.fig.suptitle(kwargs["title"], fontsize=20)

        if "xlabel" in kwargs:
            self.ax.set_xlabel(kwargs["xlabel"], fontsize=15)

        if "xlim" in kwargs:
            self.ax.set_xlim(kwargs["xlim"])

        plt.show()

        return AssertionBuilderFeaturePlotResult(self.features_outside_range)

    def get_features_outside_range(self):
        return self.features_outside_range


class AssertionBuilderFeaturePlotResult(object):
    """Class stores feature assertion result after plotting for a retrieval"""

    def __init__(self, features_outside_range: pd.DataFrame):
        self.features_outside_range = features_outside_range

    def get_features_outside_range(self) -> pd.DataFrame:
        """Method returning features outside of the allowed range

        :return: pd.DataFrame with features outside of the allowed range
        """
        return self.features_outside_range
