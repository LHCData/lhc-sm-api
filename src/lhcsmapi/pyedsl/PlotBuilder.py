from datetime import datetime
from typing import Optional, Union, Dict

import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import warnings

from lhcsmapi.Time import Time


def convert_plot_features_map_to_text(features_map: Dict[str, float]) -> str:
    """Function converting a feature dictionary into a string displayed next to a plot

    :param features_map: dictionary from feature to value
    :return: formatted string
    """
    max_key_len = max(map(len, features_map))
    return "\n\n".join(
        [
            "{}:{} {} {}".format(key, (max_key_len - len(key)) * " ", value[0], value[1])
            for key, value in features_map.items()
        ]
    )


def create_hwc_plot_title(hwc_test: str, t_start: int, t_end: int, signal: str) -> str:
    """Function generating a HWC plot title

    :param hwc_test: name of a HWC test
    :param t_start: start time of a HWC test
    :param t_end: end time of a HWC test
    :param signal: name of a signal in the plot
    :return: formatted string with the plot title
    """
    return "HWC test {} start time: {}, end time: {}, {}".format(
        hwc_test, Time.to_string_short(t_start), Time.to_string_short(t_end), signal
    )


def create_hwc_plot_title_with_circuit_name(
    circuit_name: str, hwc_test: str, t_start: Union[str, int], t_end: Union[str, int], signal: str
) -> str:
    """Function generating a HWC plot title with circuit name

    :param circuit_name: circuit name
    :param hwc_test: name of a HWC test
    :param t_start: start time of a HWC test
    :param t_end: end time of a HWC test
    :param signal: name of a signal in the plot
    :return: formatted string with the plot title
    """
    return "%s, %s: %s - %s, %s" % (
        circuit_name,
        hwc_test,
        Time.to_string_short(t_start),
        Time.to_string_short(t_end),
        signal,
    )


def create_title(
    circuit_name: Union[str, list[str]], timestamp: Union[int, str, datetime], signals: Union[str, list[str]]
):
    """Method creates a plot title based on circuit name, timestamp (if not nan) and list of signals.
    In case the timestamp is NaN, then it is not present in the title.

    :param circuit_name: name of a circuit
    :param timestamp: timestamp of a relevant event for which the plot is generated
    :param signals: either a single signal name or a list of signal names

    :return: a string composed of (timestamp) + circuit_name + signals

    - Example (timestamp is not NaN)
        >>> from lhcsmapi.pyedsl.PlotBuilder import create_title
        >>> circuit_name = 'ROD.A56B1'
        >>> timestamp = 1524371479600000000
        >>> signals = ['I_MEAS', 'I_REF', 'I_A']
        >>> create_title(circuit_name, timestamp, signals)
        '2018-04-22 06:31:19.600, ROD.A56B1: I_MEAS(t), I_REF(t), I_A(t)'

    - Example (timestamp is NaN)
        >>> from lhcsmapi.pyedsl.PlotBuilder import create_title
        >>> circuit_name = 'ROD.A56B1'
        >>> timestamp = np.nan
        >>> signals = ['I_MEAS', 'I_REF', 'I_A']
        >>> create_title(circuit_name, timestamp, signals)
        'ROD.A56B1: I_MEAS(t), I_REF(t), I_A(t)'
    """
    signals = signals if isinstance(signals, list) else [signals]
    signals = [signal + "(t)" for signal in signals]
    title = "%s: %s" % (circuit_name, ", ".join(signals))
    return title if np.isnan(timestamp) else "%s, %s" % (Time.to_string_short(timestamp), title)


class Plot(object):
    """Class storing parameters representing a plot with its signal.
    The class is initialized with default parameters and based on user settings customizes the plot.
    In addition, if an input dataframe is empty, then a plot is not created while the axes are set as specified.
    This class uses matplotlib library for plotting. This can be replaced by another library, e.g., plotly.

    """

    def __init__(self):
        self.dfs = None
        self.ax = None

        self.title = ""
        self.title_fontsize = 20

        self.xlabel = {"xlabel": "time, [s]", "fontsize": 15, "color": "black"}
        self.xlim = None
        self.is_twinx = True
        self.ylabel = {"ylabel": "", "fontsize": 15}
        self.ylim = None
        self.ylabel_coords = None
        self.legend = {"loc": "lower left"}
        self.axis = {"grid": False, "color": ["C0", "C1"], "style": "-", "yerr": None, "legend": True, "marker": None}
        self.tick_params = {"labelsize": 15}
        self.annotate = {"s": None, "xy": None, "xytext": None, "fontsize": 15, "family": "monospace"}
        self.axvline = {"x": None, "ls": "--"}
        self.axhspan = {}
        self.axvspan = {}
        self.spines = {}

    def plot(self) -> None:
        """Method creates a plot for an input signal(s) provided that at least one is not empty.
        If all input signals are empty, then a warning is displayed and the method execution is terminated.
        Otherwise a plot is created. In both cases all appropriate axis settings are performed in order to
        communicate that there is a missing signal, while all the settings are correct.

        :return: None
        """
        if len(self.dfs) <= len(self.axis["color"]):
            for index, df in enumerate(self.dfs):
                style = self.axis["style"][index] if isinstance(self.axis["style"], list) else self.axis["style"]
                color = (
                    self.axis["color"][index] if isinstance(self.axis["color"], (list, tuple)) else self.axis["color"]
                )
                yerr = self.axis["yerr"][index] if isinstance(self.axis["yerr"], list) else self.axis["yerr"]
                marker = (
                    self.axis["marker"][index]
                    if isinstance(self.axis["marker"], (list, tuple))
                    else self.axis["marker"]
                )
                if not df.empty:
                    df.plot(
                        ax=self.ax,
                        grid=self.axis["grid"],
                        title=self.title,
                        style=style,
                        color=color,
                        yerr=yerr,
                        marker=marker,
                    )
        else:
            for df in self.dfs:
                if not df.empty:
                    df.plot(
                        ax=self.ax,
                        grid=self.axis["grid"],
                        title=self.title,
                        style=self.axis["style"],
                        legend=self.axis["legend"],
                    )

        # set title
        self.ax.title.set_size(self.title_fontsize)

        # set xlabel
        self.ax.set_xlabel(**self.xlabel)
        # set xlim
        if self.xlim is not None:
            if not np.isnan(self.xlim[0]) and not np.isnan(self.xlim[1]):
                self.ax.set_xlim(self.xlim)

            if not np.isnan(self.xlim[0]) and np.isnan(self.xlim[1]):
                self.ax.set_xlim((self.xlim[0], self.ax.get_xlim()[1]))

            if np.isnan(self.xlim[0]) and not np.isnan(self.xlim[1]):
                self.ax.set_xlim((self.ax.get_xlim()[0], self.xlim[1]))

        # set ylabel
        self.ax.set_ylabel(**self.ylabel)
        # set ylim
        if self.ylim is not None:
            if not np.isnan(self.ylim[0]) and not np.isnan(self.ylim[1]):
                self.ax.set_ylim(self.ylim)

            if not np.isnan(self.ylim[0]) and np.isnan(self.ylim[1]):
                self.ax.set_ylim((self.ylim[0], self.ax.get_ylim()[1]))

            if np.isnan(self.ylim[0]) and not np.isnan(self.ylim[1]):
                self.ax.set_ylim((self.ax.get_ylim()[0], self.ylim[1]))
        # set ylabel_coords
        if self.ylabel_coords is not None:
            self.ax.yaxis.set_label_coords(*self.ylabel_coords)
        # set tick_params
        self.ax.tick_params(**self.tick_params)
        # set axvline
        if self.axvline["x"] is not None:
            self.ax.axvline(self.axvline["x"], ls=self.axvline["ls"])
        # set legend
        if self.is_any_df_not_empty() and self.axis["legend"]:
            self.ax.legend(**self.legend)
        # set spines
        if "axes" in self.spines and "position" in self.spines:
            self.ax.spines[self.spines["axes"]].set_position(("axes", self.spines["position"]))
        # set axhspan
        if self.axhspan:
            self.ax.axhspan(**self.axhspan)
        # set axvspan
        if self.axvspan:
            self.ax.axvspan(**self.axvspan)
        # set annotate
        if self.annotate["s"] is not None:
            self.ax.annotate(**self.annotate)

    def is_any_df_not_empty(self):
        """Method checks if there is at least one df to plot

        :return: True if there is at least one non-empty df, otherwise False
        """
        return any([df.empty is False for df in self.dfs])


class PlotBuilder(object):
    """Class allowing for creation of a plot with multiple y-axis.
    Each plot can be customized by setting appropriate parameters with setter methods (with_*()).
    Each setter method returns an instance of the PlotBuilder object allowing for chaining the operations (Fluent
    interface). This way the plot creation process is abstracted enabling use of different libraries for plotting.
    In addition, it is possible to change the default settings for all plots, e.g. for color-blind users.

    """

    def __init__(self):
        self._plots = []
        self._axes = [None]
        self.figsize = (13, 6.5)
        self.fig = None

    def with_signal(
        self,
        dfs: pd.DataFrame,
        figsize: tuple[float, float] = (13, 6.5),
        title: str = "",
        color: Optional[tuple[str, str]] = None,
        grid: bool = False,
        title_fontsize: int = 20,
        is_twinx=True,
        style="-",
        yerr=None,
        legend=True,
        marker=None,
    ):
        """Method initializes a PlotBuilder instance with signal and figure settings.
        The figsize can be only set at the first execution of this method.


        :param dfs: DataFrame with signal (either single df or a list of dfs)
        :param figsize: figure size in x and y
        :param title: figure title
        :param color: plot color
        :param title_fontsize: title font size in points

        :return: initialized PlotBuilder object
        """
        if not self._plots:
            self._plots.append(Plot())
            self._plots[-1].ylabel["color"] = color[0] if color is not None else "C0"
            self._plots[-1].axis["color"] = color if color is not None else ["C0", "C1"]
            self.figsize = figsize
        else:
            self._plots.append(Plot())
            self._plots[-1].ylabel["color"] = color[0] if color is not None else "C2"
            self._plots[-1].legend["loc"] = "upper right"
            self._plots[-1].axis["color"] = color if color is not None else ["C2", "C3"]

        self._plots[-1].axis["grid"] = grid
        self._plots[-1].axis["style"] = style
        self._plots[-1].axis["yerr"] = yerr
        self._plots[-1].axis["legend"] = legend
        self._plots[-1].axis["marker"] = marker
        self._plots[-1].dfs = [dfs] if isinstance(dfs, pd.DataFrame) else dfs
        self._plots[-1].is_twinx = is_twinx
        self._plots[-1].title = title
        self._plots[-1].title_fontsize = title_fontsize

        return self

    def with_tick_params(self, **kwargs):
        """Method sets tick_params parameters for the current axis while preserving the default ones

        :param kwargs: list of pairs of keyword and argument for parameters of tick_params
        :return: PlotBuilder object to continue customizing a plot
        """
        self._plots[-1].tick_params.update(kwargs)
        return self

    def with_ylabel(self, **kwargs):
        """Method sets ylabel parameters for the current axis while preserving the default ones

        :param kwargs: list of pairs of keyword and argument for parameters of ylabel
        :return: PlotBuilder object to continue customizing a plot
        """
        self._plots[-1].ylabel.update(kwargs)
        return self

    def with_ylim(self, lim):
        """Method sets y-axis limits for the current axis

        :param lim: two-element list with min and max limits for the y axis
        :return: PlotBuilder object to continue customizing a plot
        """
        self._plots[-1].ylim = lim
        return self

    def with_ylabel_coords(self, position):
        """Method sets ylabel coords for the current axis

        :param position: two-element list with x and y coordinates for the y axis
        :return: PlotBuilder object to continue customizing a plot
        """
        self._plots[-1].ylabel_coords = position
        return self

    def with_xlabel(self, **kwargs):
        """Method sets xlabel parameters for the current axis

        :param kwargs: list of pairs of keyword and argument for parameters of xlabel
        :return: PlotBuilder object to continue customizing a plot
        """
        self._plots[-1].xlabel.update(kwargs)
        return self

    def with_xlim(self, lim):
        """Method sets x-axis limits for the current axis

        :param lim: two-element list with min and max limits for the x axis
        :return: PlotBuilder object to continue customizing a plot
        """
        self._plots[-1].xlim = lim
        return self

    def with_legend(self, **kwargs):
        """Method sets legend parameters for the current axis

        :param kwargs: list of pairs of keyword and argument for parameters of legend
        :return: PlotBuilder object to continue customizing a plot
        """
        self._plots[-1].legend.update(kwargs)
        return self

    def with_annotate(self, **kwargs):
        """Method sets annotate parameters for the current axis while preserving the default ones

        :param kwargs: list of pairs of keyword and argument for parameters of annotate
        :return: PlotBuilder object to continue customizing a plot
        """
        self._plots[-1].annotate.update(kwargs)
        return self

    def with_axvline(self, **kwargs):
        """Method sets axvline parameters for the current axis while preserving the default ones

        :param kwargs: list of pairs of keyword and argument for parameters of axvline
        :return: PlotBuilder object to continue customizing a plot
        """
        self._plots[-1].axvline.update(kwargs)
        return self

    def with_axhspan(self, **kwargs):
        """Method sets axhspan parameters for the current axis

        :param kwargs: list of pairs of keyword and argument for parameters of axhspan
        :return: PlotBuilder object to continue customizing a plot
        """
        self._plots[-1].axhspan.update(kwargs)
        return self

    def with_axvspan(self, **kwargs):
        """Method sets axvspan parameters for the current axis

        :param kwargs: list of pairs of keyword and argument for parameters of axvspan
        :return: PlotBuilder object to continue customizing a plot
        """
        self._plots[-1].axvspan.update(kwargs)
        return self

    def with_spines(self, **kwargs):
        """Method sets spines parameters for the current axis while preserving the default ones

        :param kwargs: list of pairs of keyword and argument for parameters of spines
        :return: PlotBuilder object to continue customizing a plot
        """
        self._plots[-1].spines.update(kwargs)
        return self

    def plot(self, yscale="linear", show_plot=True):
        """Method plotting all signals assigned to the list of plots.
        Firstly, the method creates a figure if all plots are non-empty; otherwise a warning is displayed and
        the method execution is terminated.
        - if there is at least one non-empty DataFrame, then create a figure and the primary axis.
        - the primary axis is assigned to the first element of the list of axis.

        Secondly, the method assigns axes to each plot in the list of plots.
        - for the first plot, it is the primary axis
        - for the second plot, it is a twinx of the primary axis - secondary axis

        Thirdly, the method executes plots for each plot on the assigned axes.

        :return: None - method terminates a chain of execution of PlotBuilder functions
        """
        # Create figure
        if any([plot.is_any_df_not_empty() for plot in self._plots]):
            self.fig, self._axes[0] = plt.subplots(figsize=self.figsize)
        else:
            warnings.warn("All DataFrames are empty, no plots generated")
            return None

        # Assign axes
        for index, plot in enumerate(self._plots):
            if index == 0:
                plot.ax = self._axes[0]
            else:
                if plot.is_twinx:
                    self._axes.append(self._axes[0].twinx())
                    plot.ax = self._axes[index]
                else:
                    plot.ax = self._axes[0]

        # Plot
        for plot in self._plots:
            plot.plot()
        plt.yscale(yscale)
        if show_plot:
            plt.show()
        else:
            return self

    def get_axes(self):
        return self._axes
