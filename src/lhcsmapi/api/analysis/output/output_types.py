"""This module contains classes used to represent the output of an `lhcsmapi.api.analysis.Analysis` class."""

from dataclasses import dataclass

import matplotlib.figure


class Output:
    """Base class for all the output classes."""

    pass


@dataclass(frozen=True)
class HTMLOutput(Output):
    html: str


@dataclass(frozen=True)
class FigureOutput(Output):
    figure: matplotlib.figure.Figure


@dataclass(frozen=True)
class TextOutput(Output):
    text: str
