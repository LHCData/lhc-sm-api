import logging


class Handler(logging.Handler):
    def __init__(self, logs):
        self.logs = logs
        super().__init__()

    def emit(self, record):
        self.format(record)
        self.logs.append(record)
