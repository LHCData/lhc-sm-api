from __future__ import annotations

import logging
from abc import ABC, abstractmethod
from typing import Any

from lhcsmapi.api.analysis import logging_handler


class AnalysisError(Exception):
    pass


class Analysis(ABC):
    """Abstract class for the concrete analysis classes."""

    def __init__(self, identifier: str, logger: logging.Logger | None = None):
        self._context: dict[Any, Any] = {}
        self._identifier = identifier
        if logger:
            self._logger = logger
        else:
            self._logger = logging.getLogger(identifier)
        self._logs: list[logging.LogRecord] = []
        self._data_queried = False

        logger_handler = logging_handler.Handler(self._logs)
        self._logger.addHandler(logger_handler)

    def get_identifier(self) -> str:
        """Returns the identifier of the analysis"""

        return self._identifier

    def get_filtered_logs(self, *tags) -> str:
        """
        Returns a string of filtered logs based on the provided tags.

        Args:
            *tags: Variable length argument list of tags to filter the logs. Each tag is a string.

        Returns:
            A string of log messages that match any of the provided tags.

        """
        return "\n".join(
            log.getMessage() for log in self._logs if hasattr(log, "tags") and any(tag in log.tags for tag in tags)
        )

    def get_logs(self) -> list[logging.LogRecord]:
        """Returns the list of logs from the analysis."""

        return self._logs

    def set_spark(self, spark):
        """Sets the spark instance inside the class."""

        self._spark = spark

    @abstractmethod
    def query(self):
        """Query method to be implemented by the concrete class."""

        self._data_queried = True

    @abstractmethod
    def analyze(self):
        """Analysis method to be implemented by the concrete class.

        If the query method has not been executed before calling this method, it will be
        automatically executed."""

        if not self._data_queried:
            self.query()

    def register_result(self, identifier: str, data: Any):
        """Registers data inside the context of the class.

        Args:
            identifier: string identifying the data being passed
            data: object to store inside the context
        """

        self._context[identifier] = data

    def get_result(self, identifier: str) -> Any | None:
        """Returns data from the context of the class.

        Args:
            identifier: string identifying the data that has to be retrieved
        """

        return self._context.get(identifier, None)

    @abstractmethod
    def get_analysis_output(self) -> bool:
        """Returns whether the analysis passed or failed."""

        raise NotImplementedError("get_analysis_output method should be defined in the concrete class")


class AnalysisThrows(Analysis):
    """Dummy concrete class that just throws an Exception."""

    def analyze(self):
        raise AnalysisError("Analysis aborted because data is not present")


class AnalysisManager:
    """Class which manages the execution of a complete analysis."""

    def __init__(self) -> None:
        self._registered_analysis: dict[str, Analysis] = {}

    def register_analysis(self, analysis_object: Analysis):
        """Registers an analysis object inside the Analysis Manager.

        Args:
            analysis_object: analysis instance to register
        """

        self._registered_analysis[analysis_object.get_identifier()] = analysis_object

    def get_analysis(self, identifier: str) -> Analysis | None:
        """Returns a registered analysis.

        Args:
            identifier: string identifying the analysis instance
        """

        return self._registered_analysis.get(identifier, None)

    def set_spark(self, spark):
        """Calls the set_spark method on all the registered analysis objects."""

        for analysis_object in self._registered_analysis.values():
            analysis_object.set_spark(spark)

    def query_all(self):
        """Calls the query method on all the registered analysis objects."""

        for analysis_object in self._registered_analysis.values():
            analysis_object.query()

    def analyze_all(self):
        """Calls the analyze method on all the registered analysis objects."""

        for analysis_object in self._registered_analysis.values():
            analysis_object.analyze()

    def get_logs(self) -> dict[str, list[logging.LogRecord]]:
        """Returns a dictionary containing the list of logs for each of the registered
        analysis object.

        Returns:
            A dictionary with analysis identifiers as keys and list of log records as values.
        """

        return {
            identifier: analysis_object.get_logs() for identifier, analysis_object in self._registered_analysis.items()
        }

    def get_filtered_logs(self, *tags) -> dict[str, str]:
        """Returns a dictionary containing the string of filtered logs for each of the registered
        analysis objects.

        This method retrieves the filtered logs for each registered analysis object based on the provided tags.
        The logs are returned as a dictionary, where the keys are the analysis identifiers and the values are
        strings of log messages.

        Args:
            *tags: Variable-length argument list of tags used for filtering the logs.

        Returns:
            A dictionary with analysis identifiers as keys and a string of log messages as values.

        """
        return {
            identifier: analysis_object.get_filtered_logs(*tags)
            for identifier, analysis_object in self._registered_analysis.items()
        }

    def get_result(self) -> bool:
        # Uses the values in self._context to process a boolean
        # Maybe we should let the user implement this?
        # So we don't assume that the logic is only boolean-valued and the
        # result is the AND.
        # The user could specify a function (and pass to the AnalysisManager?) that reads
        # from the context and depending on the data returns a True or False

        return all(a.get_analysis_output() for a in self._registered_analysis.values())

    def get_registered_analyses_identifiers(self) -> list[str]:
        """Returns the list of identifiers of the registered analysis objects."""

        return list(self._registered_analysis.keys())
