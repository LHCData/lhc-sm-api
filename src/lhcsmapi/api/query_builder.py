from __future__ import annotations

from pyspark.sql.session import SparkSession

from lhcsmapi import utils
from lhcsmapi.api import processing, query, resolver
from lhcsmapi.api.resolver import NxcalsQueryParams
from lhcsmapi.metadata import signal_metadata
from lhcsmapi.Time import Time
from lhcsmapi.utils import arguments


class QueryBuilder:
    def with_pm(self) -> _PmQueryBuilder:
        """Method setting PM as the database for a query

        Returns:
            an instance of QueryBuilderWithDb with selected db
        """
        return _PmQueryBuilder()

    def with_nxcals(self, dbconnector: SparkSession) -> _NxcalsTimestampAndDurationQueryBuilder:
        """Method setting NXCALS as the database for a query with a spark connector

        Args:
            dbconnector: spark connector

        Returns:
            an instance of QueryBuilderWithDb with selected db
        """
        return _NxcalsTimestampAndDurationQueryBuilder(dbconnector)


class _NxcalsTimestampAndDurationQueryBuilder:
    def __init__(self, spark):
        self._spark = spark

    def with_timestamp(self, timestamp):
        return _NxcalsCircuitTypeAndQueryParameterQueryBuilder(self._spark, timestamp, timestamp)

    def with_duration(self, **duration):
        """Method setting time definition based on input keyword-argument

        Args:
            duration: a keyword-argument time definition (t_start, t_end) or (t_start, duration)

        Returns:
            an instance of QueryBuilderWithDbTimeDef
        """
        t_start, t_end = _get_time_definition(duration)

        return _NxcalsCircuitTypeAndQueryParameterQueryBuilder(self._spark, t_start, t_end)


class _NxcalsCircuitTypeAndQueryParameterQueryBuilder:
    def __init__(self, spark, t_start, t_end):
        self._spark = spark
        self._t_start = t_start
        self._t_end = t_end

    def with_circuit_type(self, circuit_type: str):
        """Method setting circuit type for further circuit-oriented metadata definition

        :param circuit_type: name of a circuit type
        :return: a QueryBuilderWithDbTimeDefCircuitType object
        """
        if circuit_type not in signal_metadata.get_circuit_types():
            raise ValueError(f"Circuit type {circuit_type} not supported!")

        return _NxcalsMetadataQueryBuilder(self._spark, self._t_start, self._t_end, circuit_type.upper())

    def with_query_parameters(self, **kwargs):
        """Method setting custom query parameters.
        User is responsible for validity check as no checks are performed.

        :param kwargs: a list of keyword-arguments for a database query
        :return: a QueryBuilderWithDbTimeDefCircuitType object
        """
        signals = arguments.vectorize(kwargs["signal"])
        query_params = resolver.VariableQueryParams(
            kwargs["nxcals_system"], self._t_start, self._t_end - self._t_start, signals
        )
        return _NxcalsSignalQueryBuilder(self._spark, query_params)


class _NxcalsMetadataQueryBuilder:
    def __init__(self, spark, t_start, t_end, circuit_type):
        self._spark = spark
        self._t_start = t_start
        self._t_end = t_end
        self._circuit_type = circuit_type

    def with_metadata(self, **kwargs):
        """Method setting metadata for a query in a convenient way based on:
        - circuit name
        - system
        - signal (for signal and feature query)
        - wildcard (for signal, feature, and event query)

        :param kwargs: keyword arguments for selection of metadata based on wildcards
        :return: an updated QueryBuilderWithDbTimeDefMetadata object
        """
        circuit_name = kwargs["circuit_name"]
        system = kwargs["system"]
        duration = self._t_end - self._t_start
        args = _get_kwargs_for_resolver(kwargs)
        systems = utils.vectorize(system)
        query_params = None
        for s in systems:
            params = resolver.get_params_for_nxcals(
                self._circuit_type, circuit_name, s, self._t_start, duration, **args
            )
            query_params = query_params.merge(params) if query_params else params

        return _NxcalsSignalQueryBuilder(self._spark, query_params)


class _NxcalsSignalQueryBuilder:
    def __init__(self, spark, query_params: NxcalsQueryParams):
        self._spark = spark
        self._query_params = query_params

    def signal_query(self, **kwargs):
        # backwards compatible log (we should be using logging instead)
        if "verbose" in kwargs and kwargs["verbose"]:
            three_dots = "..." if len(self._query_params.signals) > 5 else ""
            signals = ", ".join(self._query_params.signals[:5])
            t_start = Time.to_string_short(self._query_params.timestamp)
            t_end = Time.to_string_short(self._query_params.timestamp + self._query_params.duration)
            print(f"\tQuerying NXCALS signal(s) {signals}{three_dots} from {t_start} to {t_end}")

        result = query.query_nxcals_by_variables(self._spark, self._query_params)
        return processing.SignalProcessing(result)

    def multi_signal_query(self, **kwargs):
        return self.signal_query(**kwargs)


class _PmQueryBuilder:
    def with_timestamp(self, timestamp):
        return _PmSignalCircuitTypeAndQueryParameterQueryBuilder(timestamp)

    def with_duration(self, **duration):
        """Method setting time definition based on input keyword-argument

        :param duration: a keyword-argument time definition (t_start, t_end) or (t_start, duration)
        :return: an instance of QueryBuilderWithDbTimeDef
        """
        t_start, t_end = _get_time_definition(duration)
        return _PmEventCircuitTypeAndQueryParameterQueryBuilder(t_start, t_end)


class _PmEventCircuitTypeAndQueryParameterQueryBuilder:
    def __init__(self, t_start, t_end):
        self._t_start = t_start
        self._t_end = t_end

    def with_circuit_type(self, circuit_type: str):
        """Method setting circuit type for further circuit-oriented metadata definition

        :param circuit_type: name of a circuit type
        :return: a QueryBuilderWithDbTimeDefCircuitType object
        """
        if circuit_type not in signal_metadata.get_circuit_types():
            raise ValueError(f"Circuit type {circuit_type} not supported!")

        return _PmEventMetadataQueryBuilder(self._t_start, self._t_end, circuit_type.upper())

    def with_query_parameters(self, **kwargs):
        """Method setting custom query parameters.
        User is responsible for validity check as no checks are performed.

        :param kwargs: a list of keyword-arguments for a database query
        :return: a QueryBuilderWithDbTimeDefCircuitType object
        """
        triplets = _get_triplets(kwargs)
        query_params = resolver.PmEventQueryParams(self._t_start, self._t_end - self._t_start, triplets)
        return _PmEventQueryBuilder(query_params)


class _PmEventMetadataQueryBuilder:
    def __init__(self, t_start, t_end, circuit_type):
        self._t_start = t_start
        self._t_end = t_end
        self._circuit_type = circuit_type

    def with_metadata(self, **kwargs):
        """Method setting metadata for a query in a convenient way based on:
        - circuit name
        - system
        - signal (for signal and feature query)
        - wildcard (for signal, feature, and event query)

        :param kwargs: keyword arguments for selection of metadata based on wildcards
        :return: an updated QueryBuilderWithDbTimeDefMetadata object
        """
        circuit_name = kwargs["circuit_name"]
        system = kwargs["system"]
        duration = self._t_end - self._t_start
        args = _get_kwargs_for_resolver(kwargs)
        _update_with_source(args, kwargs, self._circuit_type, circuit_name, system, self._t_start)
        query_params = None
        for s in arguments.vectorize(system):
            q = resolver.get_params_for_pm_events(self._circuit_type, circuit_name, s, self._t_start, duration, **args)
            query_params = query_params.merge(q) if query_params else q

        if "source" in kwargs and kwargs["source"] != "*":
            query_params.triplets = [triplet for triplet in query_params.triplets if triplet.source == kwargs["source"]]

        return _PmEventQueryBuilder(query_params)


class _PmEventQueryBuilder:
    def __init__(self, query_params: resolver.PmEventQueryParams):
        self._query_params = query_params

    def event_query(self, **kwargs):
        # backwards compatible log (we should be using logging instead)
        if "verbose" in kwargs and kwargs["verbose"]:
            three_dots = "..." if len(self._query_params.triplets) > 5 else ""
            systems = ", ".join(list({t.system for t in self._query_params.triplets})[:5])
            classes = ", ".join(list({t.class_ for t in self._query_params.triplets})[:5])
            sources = ", ".join(list({t.source for t in self._query_params.triplets})[:5])
            t_start = Time.to_string_short(self._query_params.timestamp)
            t_end = Time.to_string_short(self._query_params.timestamp + self._query_params.duration)
            print(
                f"\tQuerying PM event timestamps for system: {systems}{three_dots}, className: {classes}{three_dots},"
                f" source: {sources}{three_dots} from {t_start} to {t_end}"
            )
        result = query.query_pm_events_with_resolved_params(self._query_params)
        return processing.EventProcessing(result)


class _PmSignalCircuitTypeAndQueryParameterQueryBuilder:
    def __init__(self, timestamp):
        self._timestamp = timestamp

    def with_circuit_type(self, circuit_type: str):
        """Method setting circuit type for further circuit-oriented metadata definition

        :param circuit_type: name of a circuit type
        :return: a QueryBuilderWithDbTimeDefCircuitType object
        """
        if circuit_type not in signal_metadata.get_circuit_types():
            raise ValueError(f"Circuit type {circuit_type} not supported!")

        return _PmSignalMetadataQueryBuilder(self._timestamp, circuit_type.upper())

    def with_query_parameters(self, **kwargs):
        """Method setting custom query parameters.
        User is responsible for validity check as no checks are performed.

        :param kwargs: a list of keyword-arguments for a database query
        :return: a QueryBuilderWithDbTimeDefCircuitType object
        """
        triplets = _get_triplets(kwargs)
        query_params = resolver.PmSignalQueryParams(
            self._timestamp, [resolver.PmSignals(triplet, kwargs["signal"]) for triplet in triplets]
        )
        return _PmSignalQueryBuilder(query_params)


class _PmSignalMetadataQueryBuilder:
    def __init__(self, timestamp, circuit_type):
        self._timestamp = timestamp
        self._circuit_type = circuit_type

    def with_metadata(self, **kwargs):
        """Method setting metadata for a query in a convenient way based on:
        - circuit name
        - system
        - signal (for signal and feature query)
        - wildcard (for signal, feature, and event query)

        :param kwargs: keyword arguments for selection of metadata based on wildcards
        :return: an updated QueryBuilderWithDbTimeDefMetadata object
        """
        circuit_name = kwargs["circuit_name"]
        systems = arguments.vectorize(kwargs["system"])

        query_params = None
        for system in systems:
            args = _get_kwargs_for_resolver(kwargs)
            _update_with_source(args, kwargs, self._circuit_type, circuit_name, system, self._timestamp)
            p = resolver.get_params_for_pm_signals(self._circuit_type, circuit_name, system, self._timestamp, **args)
            query_params = query_params.merge(p) if query_params else p

        if "source" in kwargs and kwargs["source"] != "*":
            query_params.signals = [s for s in query_params.signals if s.triplet.source == kwargs["source"]]

        return _PmSignalQueryBuilder(query_params)


class _PmSignalQueryBuilder:
    def __init__(self, query_params: resolver.PmSignalQueryParams):
        self._query_params = query_params

    def signal_query(self, **kwargs):
        # backwards compatible log (we should be using logging instead)
        if "verbose" in kwargs and kwargs["verbose"]:
            three_dots = "..." if len(self._query_params.signals) > 5 else ""
            timestamp = Time.to_string_short(self._query_params.timestamp)
            signals = ", ".join(self._query_params.signals[0].signals[:5])
            systems = ", ".join(list({t.triplet.system for t in self._query_params.signals})[:5])
            classes = ", ".join(list({t.triplet.class_ for t in self._query_params.signals})[:5])
            sources = ", ".join(list({t.triplet.source for t in self._query_params.signals})[:5])
            print(
                f"\tQuerying PM event signal(s) {signals}{three_dots} for system: {systems}{three_dots},"
                f" className: {classes}{three_dots}, source: {sources}{three_dots} at {timestamp}"
            )

        result = query.query_pm_signals_with_resolved_params(self._query_params)
        return processing.SignalProcessing(result)

    def multi_signal_query(self, **kwargs):
        return self.signal_query(**kwargs)


def _get_time_definition(time_def_dct: dict[str, int | str | float]) -> tuple[int, int]:
    """Function validating time definition given in three ways:
    - t_start, t_end
    - t_start, time range definition (time after t_start)
    - t_start, time range definition (time before and after t_start)

    If t_start not found, the timestamp is used

    :param time_def_dct: a dictionary with time definition
    :return: a tuple of (t_start, t_end) based on the time definition
    """

    t_start = time_def_dct.get("t_start")
    if "t_start" not in time_def_dct:
        t_start = time_def_dct.get("timestamp")
    t_end = time_def_dct.get("t_end")
    duration = time_def_dct.get("duration")
    return Time.get_query_period_in_unix_time(t_start, t_end, duration, "")


def _get_triplets(kwargs: dict) -> list[resolver.PmTriplet]:
    if not isinstance(kwargs["system"], list):
        return [resolver.PmTriplet(kwargs["system"], kwargs["className"], kwargs["source"])]

    triplets = []
    for i in range(len(kwargs["system"])):
        triplets.append(resolver.PmTriplet(kwargs["system"][i], kwargs["className"][i], kwargs["source"][i]))
    return triplets


def _get_kwargs_for_resolver(kwargs):
    """Translates QueryBuilder's keyword arguments into the resolver's kwargs."""
    resolvers_kwargs = {}
    if "wildcard" in kwargs:
        resolvers_kwargs["wildcard"] = kwargs["wildcard"]
    if "signal" in kwargs:
        resolvers_kwargs["signals"] = kwargs["signal"]
    return resolvers_kwargs


def _update_with_source(resolvers, kwargs, circuit_type, circuit_name, system, timestamp):
    """Updates resolver's kwargs with the wildcard for source."""
    if "source" in kwargs:
        meta = signal_metadata.get_signal_metadata(circuit_type, circuit_name, system, "PM", timestamp)
        if "%" in meta["source"]:
            key = meta["source"].replace("%", "")
            if "wildcard" in resolvers:
                resolvers["wildcard"][key] = kwargs["source"]
            else:
                resolvers["wildcard"] = {key: kwargs["source"]}
