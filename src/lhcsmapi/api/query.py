from __future__ import annotations

import json
import logging
from collections.abc import Sequence
from datetime import datetime
from functools import wraps
from typing import Any, cast, overload

import pandas as pd
import requests
from nxcals.api.extraction.data import builders
from pyspark.sql.dataframe import DataFrame as SparkDataFrame
from pyspark.sql.session import SparkSession

from lhcsmapi.analysis.decorators import check_arguments_not_none
from lhcsmapi.api import resolver
from lhcsmapi.metadata import signal_metadata
from lhcsmapi.Time import Time, TimeAlias
from lhcsmapi.utils import unpack_single_element, vectorize

CMW = "CMW"
WINCCOA = "WINCCOA"
_ONE_DAY_NS = 86400000000000
_ONE_YEAR_NS = 365 * _ONE_DAY_NS
_PM_ENDPOINT = "http://pm-rest.cern.ch/v2/"
_RD1_CIRCUITS = ["RD1.L2", "RD1.L8", "RD1.R2", "RD1.R8"]

logger = logging.getLogger()


def _rd1_very_dirty_fix():
    """Decorator which replaces new RD1 signal names with the old ones.
    It's a temporary patch, see https://its.cern.ch/jira/browse/SIGMON-477.
    """

    def is_fix_needed(query_params):
        return (
            isinstance(query_params, (resolver.VariableQueryParams, resolver.PmSignalQueryParams))
        ) and Time.to_unix_timestamp(query_params.timestamp) > signal_metadata._RUN_3_QPS_UPGRADE_START

    def inner_function(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            query_results = vectorize(func(*args, **kwargs))
            query_params = args[-1]
            if is_fix_needed(query_params):
                _rd1_rename_signals(query_results)
            return unpack_single_element(query_results)

        return wrapper

    return inner_function


def _rd1_rename_signals(dataframes):
    for df in dataframes:
        df.columns = [
            c.replace("_B1", "").replace("1_B2", "2") if any(circuit in c for circuit in _RD1_CIRCUITS) else c
            for c in df.columns
        ]


@check_arguments_not_none()
@_rd1_very_dirty_fix()
def query_nxcals_by_variables(
    spark: SparkSession,
    query_parameters: resolver.VariableQueryParams,
    include_latest_data_point_prior_to_start: bool = False,
    max_latest_data_point_search_period: int = _ONE_YEAR_NS,  # inherited from CALS
) -> pd.DataFrame | list[pd.DataFrame]:
    """Queries NXCALS by variables and returning pandas dataframes (one for each variable)
    Deprecated: Use query_cmw_by_variables / query_winccoa_by_variables instead

    Args:
        spark: spark instance
        query_parameters: instance of VariableQueryParams which defines values, which NXCALS will be queried with

    Returns:
        pd.DataFrame for each variable with values queried from NXCALS

    Examples:
        >>> from nxcals.spark_session_builder import get_or_create
        >>> spark = get_or_create() # doctest: +SKIP
        >>> from lhcsmapi.api import query
        >>> circuit_type = "RB"
        >>> duration = (60, "s")
        >>> circuit_name = "RB.A12"
        >>> timestamp = 1544112297720000000
        >>> system = "PIC"
        >>> signal_names = "ST_ABORT_PIC"
        >>> nxcals_query_params = resolver.get_params_for_nxcals(
        ...     circuit_type, circuit_name, system, timestamp, duration, signals=signal_names
        ... )
        >>> df = query.query_nxcals_by_variables(spark, nxcals_query_params) # doctest: +SKIP
        >>> print(df[0].to_markdown()) # doctest: +SKIP
        |           timestamp |   RB.A12.ODD:ST_ABORT_PIC |
        |--------------------:|--------------------------:|
        | 1544112338481000000 |                         1 |
        | 1544112338482000000 |                         0 |
    """
    return _query_nxcals_by_variables(
        spark,
        query_parameters.system,
        query_parameters.timestamp,
        query_parameters.duration,
        query_parameters.signals,
        include_latest_data_point_prior_to_start,
        max_latest_data_point_search_period,
    )


@check_arguments_not_none()
def query_pm_events_with_resolved_params(query_parameters: resolver.PmEventQueryParams) -> pd.DataFrame:
    """Queries Post Mortem events
    Deprecated: Use query_pm_data_headers instead

    Args:
        query_parameters: instance of PmEventQueryParams with values to query PM with

    Returns:
        Pandas dataframe with event data retrieved from PM

    Examples:
        >>> from lhcsmapi.api import query
        >>> t_start = 1521124480636000000
        >>> t_end = 1521126606060000000
        >>> duration = t_end - t_start
        >>> circuit_name = "RQ10.R2"
        >>> circuit_type = "IPQ"
        >>> system = "PC"
        >>> pm_query_parameters = resolver.get_params_for_pm_events(
        ...     circuit_type, circuit_name, system, t_start, duration
        ... )
        >>> df = query.query_pm_events_with_resolved_params(pm_query_parameters)
        >>> print(df.to_markdown())
        |    | source               |           timestamp |
        |---:|:---------------------|--------------------:|
        |  0 | RPHGA.UA27.RQ10.R2B1 | 1521124585040000000 |
        |  1 | RPHGA.UA27.RQ10.R2B2 | 1521124585040000000 |
    """
    dfs = []

    for triplet in query_parameters.triplets:
        pm_data = query_pm_data_headers(
            triplet.system, triplet.class_, triplet.source, query_parameters.timestamp, query_parameters.duration
        )
        dfs.append(pm_data)

    return pd.concat(dfs).reset_index(drop=True)


@check_arguments_not_none()
@_rd1_very_dirty_fix()
def query_pm_signals_with_resolved_params(
    query_parameters: resolver.PmSignalQueryParams,
) -> pd.DataFrame | list[pd.DataFrame]:
    """Queries Post Mortem signals
    Deprecated: Use query_pm_data_signals instead

    Args:
        query_parameters: instance of PmSignalQueryParams with values to query PM with

    Returns:
        Pandas dataframe for each queried signal with values retrieved from PM

    Examples:
        Without wildcards:
        >>> from lhcsmapi.api import query, resolver
        >>> timestamp = 1544112297720000000
        >>> signal_names = ["I_MEAS"]
        >>> circuit_name = "RB.A12"
        >>> circuit_type = "RB"
        >>> system = "PC"
        >>> pm_query_parameters = resolver.get_params_for_pm_signals(
        ...     circuit_type, circuit_name, system, timestamp, signals=signal_names
        ... )
        >>> df = query.query_pm_signals_with_resolved_params(pm_query_parameters)
        >>> print(df.head().to_markdown())
        |           timestamp |   STATUS.I_MEAS |
        |--------------------:|----------------:|
        | 1544111937260000000 |         97.0859 |
        | 1544111937280000000 |         97.0813 |
        | 1544111937300000000 |         97.0816 |
        | 1544111937320000000 |         97.0823 |
        | 1544111937340000000 |         97.081  |

        With wildcards:
        >>> from lhcsmapi.api import query, resolver
        >>> pm_query_params = resolver.get_params_for_pm_signals(
        ...     "RQ", "RQD.A12", "QDS", 1544622149598000000, signals=["U_1_EXT"], wildcard={"CELL": "16L2"}
        ... )
        >>> df = query.query_pm_signals_with_resolved_params(pm_query_params)
        >>> print(df.head().to_markdown())
        |           timestamp |   16L2:U_1_EXT |
        |--------------------:|---------------:|
        | 1544622148400000000 |      -1.21551  |
        | 1544622148402000000 |      -0.510755 |
        | 1544622148404000000 |      -0.510755 |
        | 1544622148406000000 |      -0.510755 |
        | 1544622148408000000 |      -0.510755 |
    """

    results = []
    for signal in query_parameters.signals:
        pm_data = query_pm_data_signals(
            signal.triplet.system,
            signal.triplet.class_,
            signal.triplet.source,
            signal.signals,
            query_parameters.timestamp,
        )
        if isinstance(pm_data, list):
            results.append([signal.to_frame() for signal in pm_data])
        else:
            results.append(pm_data.to_frame())
    return unpack_single_element(results)


def _query_nxcals_by_variables_with_series(
    spark: SparkSession,
    system: str,
    start_time: TimeAlias,
    duration: int | tuple[int, str],
    variables: str | Sequence[str],
    include_latest_data_point_prior_to_start: bool,
    max_latest_data_point_search_period: int,
) -> pd.Series | list[pd.Series]:
    res = _query_nxcals_by_variables(
        spark,
        system,
        start_time,
        duration,
        variables,
        include_latest_data_point_prior_to_start,
        max_latest_data_point_search_period,
    )
    if isinstance(variables, list):
        return [res.iloc[:, 0] for res in res] if isinstance(res, list) else [res.iloc[:, 0]]

    if isinstance(res, list):
        if len(res) > 1:
            raise ValueError(
                f"Querying NXCALS returned more than one result for the following parameters: "
                f"system: {system}, start_time: {start_time}, duration: {duration}, variables: {variables}"
            )
        return res[0].iloc[:, 0]
    return res.iloc[:, 0]


@overload
def query_cmw_by_variables(
    spark: SparkSession,
    start_time: TimeAlias,
    duration: int | tuple[int, str],
    variables: str,
    include_latest_data_point_prior_to_start: bool = ...,
    max_latest_data_point_search_period: int = ...,
) -> pd.Series: ...


@overload
def query_cmw_by_variables(
    spark: SparkSession,
    start_time: TimeAlias,
    duration: int | tuple[int, str],
    variables: Sequence[str],
    include_latest_data_point_prior_to_start: bool = ...,
    max_latest_data_point_search_period: int = ...,
) -> list[pd.Series]: ...


@check_arguments_not_none()
def query_cmw_by_variables(
    spark: SparkSession,
    start_time: TimeAlias,
    duration: int | tuple[int, str],
    variables: str | Sequence[str],
    include_latest_data_point_prior_to_start: bool = False,
    max_latest_data_point_search_period: int = _ONE_YEAR_NS,  # inherited from CALS
) -> pd.Series | list[pd.Series]:
    """Method querying NXCALS by variables in CMW system and returning one pandas dataframe per variable

    Args:
        spark: spark instance
        start_time: start time (default in local time) as int, string or datetime
        duration: duration in nanoseconds or Tuple of number and unit i.e. 250000000000 or (250, 's')
        variables: variable name or list of variable names

    Returns:
        pd.Series for each variable with values queried from NXCALS,
        indexed by timestamp (in nanoseconds since epoch)

    Examples:
        >>> from nxcals.spark_session_builder import get_or_create
        >>> spark = get_or_create() # doctest: +SKIP
        >>> from lhcsmapi.api import query
        >>> t_start = "2022-01-09 13:00:00.000"
        >>> duration = 86400000000000
        >>> variables = "TSTLHC.BPM.SR2.B2_DOROS:ORBIT_MODE"
        >>> nxcals_data = query.query_cmw_by_variables(spark, t_start, duration, variables) # doctest: +SKIP
        >>> print(nxcals_data.head().to_markdown()) # doctest: +SKIP
        |           timestamp |   TSTLHC.BPM.SR2.B2_DOROS:ORBIT_MODE |
        |--------------------:|-------------------------------------:|
        | 1641730008042249000 |                                    1 |
        | 1641730009042040000 |                                    8 |
        | 1641730355047104000 |                                    1 |
        | 1641730356042149000 |                                    8 |
        | 1641730966042287000 |                                    1 |
    """
    return _query_nxcals_by_variables_with_series(
        spark,
        CMW,
        start_time,
        duration,
        variables,
        include_latest_data_point_prior_to_start,
        max_latest_data_point_search_period,
    )


@overload
def query_winccoa_by_variables(
    spark: SparkSession,
    start_time: TimeAlias,
    duration: int | tuple[int, str],
    variables: str,
    include_latest_data_point_prior_to_start: bool = ...,
    max_latest_data_point_search_period: int = ...,
) -> pd.Series: ...


@overload
def query_winccoa_by_variables(
    spark: SparkSession,
    start_time: TimeAlias,
    duration: int | tuple[int, str],
    variables: list[str],
    include_latest_data_point_prior_to_start: bool = ...,
    max_latest_data_point_search_period: int = ...,
) -> list[pd.Series]: ...


@check_arguments_not_none()
def query_winccoa_by_variables(
    spark: SparkSession,
    start_time: TimeAlias,
    duration: int | tuple[int, str],
    variables: str | Sequence[str],
    include_latest_data_point_prior_to_start: bool = False,
    max_latest_data_point_search_period: int = _ONE_YEAR_NS,  # inherited from CALS
) -> pd.Series | list[pd.Series]:
    """Method querying NXCALS by variables in WINCCOA system and returning one pandas dataframe per variable

    Args:
        spark: spark instance
        start_time: start time (default in local time) as int, string or datetime
        duration: duration in nanoseconds or Tuple of number and unit i.e. 250000000000 or (250, 's')
        variables: variable name or list of variable names
        include_latest_data_point_prior_to_start: if True, the latest preceding value will be included in the result

    Returns:
        pd.Series for each variable with values queried from NXCALS,
        indexed by timestamp (in nanoseconds since epoch)

    Examples:
        >>> from nxcals.spark_session_builder import get_or_create
        >>> spark = get_or_create() # doctest: +SKIP
        >>> from lhcsmapi.api import query
        >>> t_start = 1544112297720000000
        >>> duration = 60000000000
        >>> variables = "RB.A12.ODD:ST_ABORT_PIC"
        >>> nxcals_data = query.query_winccoa_by_variables(spark, t_start, duration, variables) # doctest: +SKIP
        >>> print(nxcals_data.to_markdown()) # doctest: +SKIP
        |           timestamp |   RB.A12.ODD:ST_ABORT_PIC |
        |--------------------:|--------------------------:|
        | 1544112338481000000 |                         1 |
        | 1544112338482000000 |                         0 |
    """
    return _query_nxcals_by_variables_with_series(
        spark,
        WINCCOA,
        start_time,
        duration,
        variables,
        include_latest_data_point_prior_to_start,
        max_latest_data_point_search_period,
    )


@check_arguments_not_none()
def query_cmw_by_entities(
    spark: SparkSession,
    start_time: TimeAlias,
    duration: int | tuple[int, str],
    key_values: tuple[str, str] | list[tuple[str, str]],
    signal: str,
) -> pd.DataFrame | list[pd.DataFrame]:
    """Method querying NXCALS by entities in CMW system and returning one pandas dataframes
    for each device-property pair

    Args:
        spark: spark instance
        start_time: start time (default in local time) as int, string or datetime
        duration: duration in nanoseconds or Tuple of number and unit i.e. 250000000000 or (250, 's')
        key_values: a Tuple of device and property or a list of Tuples
        signal: signal name

    Returns:
        pd.DataFrame for each Tuple of device and property, indexed by timestamp (in nanoseconds since epoch)

    Examples:
        >>> from nxcals.spark_session_builder import get_or_create
        >>> spark = get_or_create()  # doctest: +SKIP
        >>> from lhcsmapi.api import query
        >>> t_start = 1692810137000000000
        >>> duration = 86400000000000
        >>> device = "BE.SMH15L1"
        >>> property = "MEAS.PULSE"
        >>> key_values = (device, property)
        >>> signal = "VALUE"
        >>> nxcals_data = query.query_cmw_by_entities(spark, t_start, duration, key_values, signal) # doctest: +SKIP
        >>> print(nxcals_data.head().to_markdown()) # doctest: +SKIP
        |           timestamp |   VALUE |
        |--------------------:|--------:|
        | 1692810138877000000 | 8626.54 |
        | 1692810140077000000 | 6601.29 |
        | 1692810141277000000 | 8628.94 |
        | 1692810142477000000 | 8627.85 |
        | 1692810143677000000 | 6602.49 |
    """
    start_time = Time.to_unix_timestamp(start_time)
    if isinstance(duration, tuple):
        duration = Time.to_unix_timestamp(duration[0], duration[1])
    nxcals_query = (
        builders.DevicePropertyDataQuery.builder(spark).system(CMW).startTime(start_time).duration(int(duration))
    )
    logger.debug(
        f"Querying NXCALS for {key_values} with signal {signal}, duration {duration} and start time {start_time}"
    )
    key_values = vectorize(key_values)
    for key_value_pair in key_values:
        nxcals_query = nxcals_query.entity().device(key_value_pair[0]).property(key_value_pair[1])
    nxcals_data = nxcals_query.build()

    if nxcals_data.count() < 1:
        logger.warning(
            f"Querying NXCALS returned no results for the following parameters: system: CMW, start_time:"
            f" {start_time}, duration: {duration}, devices and properties: {key_values}"
        )
        return pd.DataFrame({signal: pd.Series(dtype="str")})

    return transform_signal_dataset_to_pandas(nxcals_data, key_values, signal)


def query_raw_pm_data(
    system: str, class_name: str, source: str, timestamp: TimeAlias, signal_names: str | list[str] | None = None
) -> dict:
    """Deprecated, use query_pm_data and query_pm_data_signals instead"""
    query_params = {
        "system": system,
        "className": class_name,
        "source": source,
        "timestampInNanos": Time.to_unix_timestamp(timestamp),
    }
    if signal_names:
        response = _query_pm(query_params, _build_pm_signal_query, signal_names)
    else:
        response = _query_pm(query_params, _build_pm_data_query)
    if not response:
        return {}
    content = response["content"][0]
    return {entity["name"]: entity["value"] for entity in content["namesAndValues"]}


def query_pm_data(system: str, class_name: str, source: str, timestamp: TimeAlias | None) -> dict[str, Any]:
    """Method querying PM data (http://pm-rest.cern.ch/#api-PmData-Get_PmData)

    Args:
        system: PM system
        class_name: PM class name
        source: PM source
        timestamp: timestamp as string, datetime or nanoseconds. If None, no query is performed
                   and an empty df is returned for every requested signal

    Returns:
        Dictionary with values retrieved from PM

    Examples:
        Query PM data for LBDS:
        >>> import pprint
        >>> from lhcsmapi.api import query
        >>> res = query.query_pm_data("LBDS", "TSU", "LBDS.865.MKCTS.B", 1430431203968406625)
        >>> pprint.pprint(res)
        {'acqStamp': 0,
         'acquisitionStamp': 1430431203968406625,
         'armingWatchDogError': True,
         'asyncDumpDone': True,
         'bagkDelayStatus': False,
         'bagkTriggerDelay': 2.47,
         'beamLossDetected_Cpld1': True,
         'beamLossDetected_Cpld2': True,
         'becFastDetected_Cpld1': False,
         'becFastDetected_Cpld2': False,
         'becSlowDetected_Cpld1': True,
         'becSlowDetected_Cpld2': True,
         'bisFastDetected_Cpld1': True,
         'bisFastDetected_Cpld2': True,
         'bisFreq': 9.375,
         'bisFreq_Cpld1': 8.375,
         'bisFreq_Cpld2': 8.375,
         'bisSlowDetected_Cpld1': True,
         'bisSlowDetected_Cpld2': True,
         'brfCmp1': True,
         'brfCmp2': True,
         'brfDelayFault': False,
         'brfDetected': True,
         'brfSyncFault': False,
         'clientDumpRequested': False,
         'clientsOk_Cpld1': False,
         'clientsOk_Cpld2': False,
         'delayedPllFreqCmp1': True,
         'delayedPllFreqCmp2': True,
         'delayedPllOutputDetected': True,
         'dumpDone': True,
         'dumpRequestStamp': 1430431203968331975,
         'dumpRequestStamp2': 1430431203968331925,
         'dumpTriggerDelay': 36.0,
         'injectAndDumpDetected_Cpld1': True,
         'injectAndDumpDetected_Cpld2': True,
         'internalFault': False,
         'plcArming': True,
         'pllComputedCtrlWord': 282,
         'pllCtrlState': 2,
         'pllDphiDtTime': 0.0,
         'pllInCatchState': True,
         'pllInPullInRange': False,
         'pllInPullOutRange': False,
         'pllLockEnabled': True,
         'pllLocked': True,
         'pllLocked1': True,
         'pllLocked2': True,
         'pllNextNcoFrequency': 11.236081,
         'pllNextNcoPeriod': 88.999,
         'pllOutputDetected': True,
         'pllPdState': 1,
         'pllPdTime': 0.0,
         'pllPhiErrorTime': 13.08,
         'pllPresentCtrlWord': 282,
         'pllPresentNcoFrequency': 11.236081,
         'pllPresentNcoPeriod': 88.999,
         'pllPreviousCtrlWord': 282,
         'pllPreviousPhiErrorTime': 13.08,
         'remoteCtrl': True,
         'reset_Cpld1': False,
         'reset_Cpld2': False,
         'scssDetected_Cpld1': True,
         'scssDetected_Cpld2': True,
         'secondSyncFault': False,
         'secondTsuDelayedPllOutputDetected': True,
         'syncDumpEnabled': True,
         'syncDumpRequested': False,
         'syncFault': False,
         'triggerDelayStatus': True,
         'triggerWatchDogError': True,
         'tsuInternalDumpDetected_Cpld1': True,
         'tsuInternalDumpDetected_Cpld2': True,
         'tsuReady': True,
         'tsuState': 1024}

        Query PM data, no results:
        >>> from lhcsmapi.api import query
        >>> query.query_pm_data("LBDS", "TSU", "LBDS.865.MKCTS.B", 1)
        {}
    """
    if timestamp is None:
        logger.warning("Timestamp is None, therefore no query will be performed.")
        return {}

    query_params = {
        "system": system,
        "className": class_name,
        "source": source,
        "timestampInNanos": Time.to_unix_timestamp(timestamp),
    }
    response = _query_pm(query_params, _build_pm_data_query)
    if not response:
        return {}
    content = response["content"][0]
    return {entity["name"]: entity["value"] for entity in content["namesAndValues"]}


@overload
def query_pm_data_signals(
    system: str, class_name: str, source: str, signal_names: str, timestamp: TimeAlias | None
) -> pd.Series: ...


@overload
def query_pm_data_signals(
    system: str, class_name: str, source: str, signal_names: list[str], timestamp: TimeAlias | None
) -> list[pd.Series]: ...


def query_pm_data_signals(
    system: str,
    class_name: str,
    source: str,
    signal_names: str | list[str],
    timestamp: TimeAlias | None,
    apply_correction: bool = True,
) -> pd.Series | list[pd.Series]:
    """Method querying PM data signals (http://pm-rest.cern.ch/#api-PmData-Get_PmData_signal)

    Args:
        system: PM system
        class_name: PM class name
        source: PM source
        signals: list of signals to query
        timestamp: timestamp as string, datetime or nanoseconds. If None, no query is performed
                   and an empty df is returned for every requested signal
        apply_correction: if True, the out of order indices will be removed from the response

    Returns:
        Pandas series for each queried signal with values retrieved from PM

    Examples:
        To query PM signals for FGC:
        >>> from lhcsmapi.api import query
        >>> from lhcsmapi.metadata import signal_metadata
        >>> pm_system = "FGC"
        >>> pm_source = "RPTE.UA23.RB.A12"
        >>> timestamp = 1544112297720000000
        >>> signal = "IAB.I_A"
        >>> circuit_name = "RB.A12"
        >>> pm_class = signal_metadata.get_fgc_pm_class_name(circuit_name, timestamp, "self")
        >>> pm_data = query.query_pm_data_signals(pm_system, pm_class, pm_source, signal, timestamp)
        >>> print(pm_data.head().to_markdown())
        |           timestamp |   IAB.I_A |
        |--------------------:|----------:|
        | 1544112293616000000 |   92.4034 |
        | 1544112293617000000 |   92.3832 |
        | 1544112293618000000 |   92.3904 |
        | 1544112293619000000 |   92.3976 |
        | 1544112293620000000 |   92.3819 |

        To query PM signals for QPS:
        >>> from lhcsmapi.api import query
        >>> pm_data = query_pm_data_signals(
        ...     "QPS", "DQAMCNMB_PMHSU", "B20L5", "I_HDS_3", 1426220469491000000
        ... )
        >>> print(pm_data.head().to_markdown())
        |           timestamp |   I_HDS_3 |
        |--------------------:|----------:|
        | 1426220469491020832 | 0.0915552 |
        | 1426220469491026040 | 0.0915552 |
        | 1426220469491031248 | 0.0915552 |
        | 1426220469491036456 | 0.0915552 |
        | 1426220469491041664 | 0.0915552 |

        To query earth current signal for IPQ FGC:
        >>> from lhcsmapi.api import query
        >>> from lhcsmapi.metadata import signal_metadata
        >>> circuit_name = "RQ5.R8"
        >>> signal = "STATUS.I_EARTH_PCNT"
        >>> timestamp_fgc_b1 = 1620416081940000000
        >>> timestamp_fgc_b2 = 1620416081940000000
        >>> pm_class = signal_metadata.get_fgc_pm_class_name(circuit_name, timestamp, "self")
        >>> pm_source = signal_metadata.get_fgc_with_earth_measurement(circuit_name, timestamp)
        >>> timestamp = timestamp_fgc_b1 if pm_source.endswith("B1") else timestamp_fgc_b2
        >>> pm_data = query.query_pm_data_signals("FGC", pm_class, pm_source, signal, timestamp)
        >>> print(pm_data.head().to_markdown())
        |           timestamp |   STATUS.I_EARTH_PCNT |
        |--------------------:|----------------------:|
        | 1620415729420000000 |                  1.77 |
        | 1620415729440000000 |                  0.43 |
        | 1620415729460000000 |                 -2.02 |
        | 1620415729480000000 |                  1.77 |
        | 1620415729500000000 |                  0.79 |

    """

    vectorized_signals_names = vectorize(signal_names)
    if timestamp is None:
        logger.warning("Timestamp is None, therefore no signals will be queried.")
        signals = [
            pd.Series(name=signal_name).rename_axis(index="timestamp") for signal_name in vectorized_signals_names
        ]
    else:
        query_params = {
            "system": system,
            "className": class_name,
            "source": source,
            "timestampInNanos": Time.to_unix_timestamp(timestamp),
        }
        response = _query_pm(query_params, _build_pm_signal_query, vectorized_signals_names)
        signals = [_get_time_value(response, signal_name, apply_correction) for signal_name in vectorized_signals_names]

    return signals if isinstance(signal_names, list) else signals[0]


@check_arguments_not_none()
def query_pm_data_headers(
    system: str, class_name: str, source: str, timestamp: TimeAlias, duration: int | tuple[int, str]
) -> pd.DataFrame:
    """Queries PM data headers (http://pm-rest.cern.ch/#api-PmData-Get_PmData_header_within_duration)

    Args:
        system: PM system
        class_name: PM class name
        source: PM source
        timestamp: timestamp as string, datetime or nanoseconds
        duration: duration in nanoseconds or Tuple of number and unit i.e. 250000000000 or (250, 's')

    Returns:
        Pandas dataframe with event data retrieved from PM sorted by timestamp and source

    Examples:
        To query PM events for FGC:
        >>> from lhcsmapi.api import query
        >>> from lhcsmapi.metadata import signal_metadata
        >>> t_start = "2017-04-21 19:26:16.760"
        >>> duration = 86400000000000
        >>> circuit_name = "RQD.A12"
        >>> fgc_name = signal_metadata.get_fgc_names(circuit_name, t_start)[0]
        >>> pm_class = signal_metadata.get_fgc_pm_class_name(circuit_name, t_start, "self")
        >>> pm_data = query.query_pm_data_headers("FGC", pm_class, fgc_name, t_start, duration)
        >>> print(pm_data.to_markdown())
        |    | source            |           timestamp |
        |---:|:------------------|--------------------:|
        |  0 | RPHE.UA23.RQD.A12 | 1492801062340000000 |
        |  1 | RPHE.UA23.RQD.A12 | 1492802776760000000 |
        |  2 | RPHE.UA23.RQD.A12 | 1492881133700000000 |

        To query PM events for QPS:
        >>> from lhcsmapi.api import query
        >>> from lhcsmapi.metadata import signal_metadata
        >>> circuit_name = "RQ6.R2"
        >>> class_name = signal_metadata.get_qps_pm_class_name_for_ipq(circuit_name)
        >>> pm_data = query.query_pm_data_headers(
        ...     "QPS", class_name, circuit_name, "2021-05-05 17:04:42.258", 443963000000
        ... )
        >>> print(pm_data.to_markdown())
        |    | source   |           timestamp |
        |---:|:---------|--------------------:|
        |  0 | RQ6.R2   | 1620227369029000000 |
        |  1 | RQ6.R2   | 1620227369030000000 |
    """
    result = pd.DataFrame(columns=["source", "timestamp"]).astype({"source": str, "timestamp": int})

    def fetch_events(timestamp_in_nanos, duration_in_nanos):
        nonlocal result
        query_params = {
            "system": system,
            "className": class_name,
            "source": source,
            "fromTimestampInNanos": timestamp_in_nanos,
            "durationInNanos": duration_in_nanos,
        }
        response = _query_pm(query_params, _build_pm_event_query)
        if response:
            events = _get_events_from_response(response)
            result = pd.concat([result, events])

    timestamp = Time.to_unix_timestamp(timestamp)
    if isinstance(duration, tuple):
        duration = Time.to_unix_timestamp(duration[0], duration[1])

    quotient, remainder = Time.modulo_day_in_unix_timestamp(timestamp, timestamp + duration)
    for day in range(quotient):
        start_time_temp = timestamp + int(day * _ONE_DAY_NS)
        fetch_events(start_time_temp, _ONE_DAY_NS)

    if remainder > 0:
        start_time_temp = timestamp + _ONE_DAY_NS * quotient
        fetch_events(start_time_temp, remainder)
    return result.sort_values(by=["timestamp", "source"]).reset_index(drop=True)


def _query_nxcals_by_variables(
    spark: SparkSession,
    system: str,
    start_time: TimeAlias,
    duration: int | tuple[int, str],
    variables: str | Sequence[str],
    include_latest_data_point_prior_to_start: bool,
    max_latest_data_point_search_period: int,
) -> pd.DataFrame | list[pd.DataFrame]:
    start_time = Time.to_unix_timestamp(start_time)
    if isinstance(duration, tuple):
        duration = Time.to_unix_timestamp(duration[0], duration[1])
    variables = vectorize(variables)
    logger.debug(
        f"Querying NXCALS for {variables} with system {system}, duration {duration} and start time {start_time}"
    )
    nxcals_data: SparkDataFrame = (
        builders.DataQuery.builder(spark)
        .byVariables()
        .system(system)
        .startTime(start_time)
        .duration(int(duration))
        .variables(variables)
        .build()
    )

    transformed_nxcals = (
        build_empty_variable_dataset(variables)
        if nxcals_data.rdd.isEmpty()
        else transform_variables_dataset_to_pandas(nxcals_data, variables)
    )

    DEFAULT_INITIAL_WINDOW_DURATION = 1_000_000_000  # 1 second
    if include_latest_data_point_prior_to_start:
        results: dict[str, pd.DataFrame] = {}
        for variable in variables:
            p_start, p_end = (
                start_time - min(DEFAULT_INITIAL_WINDOW_DURATION, max_latest_data_point_search_period),
                start_time,
            )
            while p_start < p_end:
                logger.debug(f"Querying NXCALS with p_start={p_start}, p_end={p_end} for variable {variable}.")
                prev_data: SparkDataFrame = (
                    builders.DataQuery.builder(spark)
                    .byVariables()
                    .system(system)
                    .startTime(p_start)
                    .duration(p_end - p_start)
                    .variables([variable])
                    .build()
                )
                if not prev_data.rdd.isEmpty():
                    logger.debug("Data found.")
                    break
                logger.debug("No data found, doubling the time window.")
                p_start, p_end = p_start - 2 * (p_end - p_start), p_start
                if p_start <= start_time - max_latest_data_point_search_period:
                    p_start = start_time - max_latest_data_point_search_period

            if p_start < p_end:
                transformed_prev_data: pd.DataFrame = transform_variables_dataset_to_pandas(prev_data, variable)
                results[variable] = transformed_prev_data.tail(1)
            else:
                logger.warning(
                    f"Querying NXCALS returned no results while trying to query the latest data point prior to start: "
                    f"system: {system}, variable: {variable}, start_time: {start_time}, "
                    f"max_latest_data_point_search_period: {max_latest_data_point_search_period:,}"
                )

        vectorized_transformed_nxcals: list[pd.DataFrame] = vectorize(transformed_nxcals)
        for i, df in enumerate(vectorized_transformed_nxcals):
            variable_name = df.columns[0]
            if variable_name in results and not results[variable_name].empty:
                df = pd.concat([results[variable_name], df]) if not df.empty else results[variable_name]
            vectorized_transformed_nxcals[i] = df.rename(index={0: variable_name}).rename_axis("timestamp", axis=0)

        transformed_nxcals = unpack_single_element(vectorized_transformed_nxcals)

    for df in vectorize(transformed_nxcals):
        if df.empty:
            logger.warning(
                f"Querying NXCALS returned no results for the variable {df.columns[0]}: "
                f"system: {system}, start_time: {start_time}, duration: {duration}, "
                f"include_latest_data_point_prior_to_start: {include_latest_data_point_prior_to_start}, "
                f"max_latest_data_point_search_period: {max_latest_data_point_search_period:,}"
            )

    return transformed_nxcals


def build_empty_variable_dataset(variables: list[str]):
    variable_dfs = []
    for variable in variables:
        variable_dfs.append(pd.DataFrame(columns={variable: pd.Series(dtype="str")}))
    return unpack_single_element(variable_dfs)


@check_arguments_not_none()
def transform_variables_dataset_to_pandas(
    dataset: SparkDataFrame, variables: str | list[str]
) -> pd.DataFrame | list[pd.DataFrame]:
    """Transforms spark dataset queried from NXCALS to pandas dataframes (one for each variable)

    Args:
        dataset: spark dataset got from NXCALS
        variables: variable name or list of variable names

    Returns:
        pd.DataFrame for each variable

    Examples:
        >>> from nxcals.spark_session_builder import get_or_create
        >>> from nxcals.api.extraction.data.builders import DataQuery
        >>> from lhcsmapi.api import query
        >>> spark = get_or_create() # doctest: +SKIP
        >>> variables = ["RB.A12.ODD:ST_ABORT_PIC"]
        >>> dataset = (
        ...     DataQuery.builder(spark)
        ...     .byVariables()
        ...     .system("WINCCOA")
        ...     .startTime(1544112297720000000)
        ...     .duration(60000000000)
        ...     .variables(variables)
        ...     .build()
        ... ) # doctest: +SKIP
        >>> print(query.transform_variables_dataset_to_pandas(dataset, variables).to_markdown()) # doctest: +SKIP
        |           timestamp |   RB.A12.ODD:ST_ABORT_PIC |
        |--------------------:|--------------------------:|
        | 1544112338481000000 |                         1 |
        | 1544112338482000000 |                         0 |
    """
    variable_dfs = []
    variables = vectorize(variables)

    if dataset.count() < 1:
        logger.warning("The dataset is empty")
        return build_empty_variable_dataset(variables)

    variable_dict = _get_dictionary_from_dataset(dataset, "nxcals_timestamp", "nxcals_variable_name", "nxcals_value")

    for variable in variables:
        variable_df = _get_modified_df_from_dictionary(variable_dict, variable, "nxcals_variable_name")
        if not variable_df.empty:
            variable_df.rename(columns={"nxcals_value": variable}, inplace=True)
        else:
            variable_df = pd.DataFrame(columns={variable: pd.Series(dtype="str")}).rename_axis("timestamp", axis=0)
        variable_dfs.append(variable_df)

    return unpack_single_element(variable_dfs)


@check_arguments_not_none()
def transform_signal_dataset_to_pandas(
    dataset: SparkDataFrame, key_values: tuple[str, str] | list[tuple[str, str]], signal: str
) -> pd.DataFrame | list[pd.DataFrame]:
    """Transforms spark dataset queried from NXCALS to pandas dataframes (one for each device property pair)

    Args:
        dataset: spark dataset got from NXCALS
        key_values: variable name or list of variable names
        signal: signal name

    Returns:
        pd.DataFrame for each device property pair

    Examples:
        >>> from nxcals.spark_session_builder import get_or_create
        >>> from nxcals.api.extraction.data.builders import DataQuery
        >>> from lhcsmapi.api import query
        >>> spark = get_or_create() # doctest: +SKIP
        >>> device = "BE.SMH15L1"
        >>> property = "MEAS.PULSE"
        >>> dataset = (
        ...     DataQuery.builder(spark)
        ...     .byEntities()
        ...     .system("CMW")
        ...     .startTime(1692810137000000000)
        ...     .duration(86400000000000)
        ...     .entity()
        ...     .keyValue("device", device)
        ...     .keyValue("property", property)
        ...     .build()
        ... ) # doctest: +SKIP
        >>> df = query.transform_signal_dataset_to_pandas(dataset, (device, property), "VALUE") # doctest: +SKIP
        >>> print(df.head().to_markdown()) # doctest: +SKIP
        |           timestamp |   VALUE |
        |--------------------:|--------:|
        | 1692810138877000000 | 8626.54 |
        | 1692810140077000000 | 6601.29 |
        | 1692810141277000000 | 8628.94 |
        | 1692810142477000000 | 8627.85 |
        | 1692810143677000000 | 6602.49 |
    """
    if dataset.count() < 1:
        logger.warning("The dataset is empty")
        return pd.DataFrame(columns={signal: pd.Series(dtype="str")})

    key_values = vectorize(key_values)
    multisignal_dict = _get_dictionary_from_dataset(dataset, "acqStamp", "device", signal)
    signal_dfs = []

    for key_value in key_values:
        signal_df = _get_modified_df_from_dictionary(multisignal_dict, key_value[0], "device")
        if signal_df.empty:
            signal_df = pd.DataFrame(columns={signal: pd.Series(dtype="str")}).rename_axis("timestamp", axis=0)
        signal_dfs.append(signal_df)

    return unpack_single_element(signal_dfs)


def query_hwc_powering_test_parameters(
    circuit_name: str, start_time: TimeAlias, duration: int | tuple[int, str]
) -> dict[str, dict[str, dict[str, Any]]]:
    """Queries HWC powering test parameters from mpe system

    Args:
        circuit_name: circuit name
        start_time: start time
        duration: duration, defaults to nanoseconds but can be expressed as tuple of number and unit i.e. (250, 's')

    Raises:
        KeyError: if circuit name is not found in the metadata
        ValueError: if duration is negative
        HTTPError: if query fails

    Returns:
        Dictionary with the following structure:
        {'power_converter_name': {'parameter_name': {'value': value, 'unit': unit}, ...}, ...}

    Examples:
        >>> import pprint
        >>> from lhcsmapi.api import query
        >>> pprint.pprint(query.query_hwc_powering_test_parameters("RD4.R4", 1678971236639000000, 1856722000000))
        {'RPHF.UA47.RD4.R4': {'ACC_PNO': {'unit': 'A/s²', 'value': 2.0},
                              'DIDT_PNO': {'unit': 'A/s', 'value': 18.147},
                              'I_5TEV': {'unit': 'A', 'value': 3200.0},
                              'I_DELTA': {'unit': 'A', 'value': 50.0},
                              'I_EARTH_MAX': {'unit': 'A', 'value': 0.0075},
                              'I_EARTH_PCC_MAX': {'unit': 'A', 'value': 0.01},
                              'I_ERR_MAX': {'unit': 'A', 'value': 0.07},
                              'I_ERR_PCC_MAX': {'unit': 'A', 'value': 0.04},
                              'I_HARDWARE': {'unit': 'A', 'value': 6850.0},
                              'I_INJECTION': {'unit': 'A', 'value': 350.0},
                              'I_INTERM_1': {'unit': 'A', 'value': 1000.0},
                              'I_INTERM_2': {'unit': 'A', 'value': 2000.0},
                              'I_INTERM_3': {'unit': 'A', 'value': 3500.0},
                              'I_MEAS_MAX': {'unit': 'A', 'value': 5.0},
                              'I_MIN_OP': {'unit': 'A', 'value': 200.0},
                              'I_PCC': {'unit': 'A', 'value': 250.0},
                              'I_PHASE_1': {'unit': 'A', 'value': 1000.0},
                              'I_PNO': {'unit': 'A', 'value': 6000.0},
                              'I_PNO_TRAINING': {'unit': 'A', 'value': 6050.0},
                              'L_TOT': {'unit': 'H', 'value': 0.052},
                              'R_TOT_MEASURED': {'unit': 'Ω', 'value': 0.000581},
                              'TIME_PCC': {'unit': 's', 'value': 30.0},
                              'TIME_PLI': {'unit': 's', 'value': 240.0},
                              'TIME_PNO': {'unit': 's', 'value': 300.0},
                              'TIME_PNO_LEADS': {'unit': 's', 'value': 3600.0},
                              'TIME_ZERO': {'unit': 's', 'value': 60.0}}}
    """
    res: dict[str, dict[str, dict[str, Any]]] = {}

    start_time = Time.to_unix_timestamp(start_time)
    if isinstance(duration, tuple):
        duration = Time.to_unix_timestamp(duration[0], duration[1])

    if duration < 0:
        raise ValueError(f"Duration cannot be negative, got {duration}")

    old_pc_names = signal_metadata.get_fgc_names(circuit_name, start_time)
    new_pc_names = signal_metadata.get_fgc_names(circuit_name, datetime.now())
    for old_pc_name, new_pc_name in zip(old_pc_names, new_pc_names):
        query = (
            f"http://mpe-systems-pro:60245/v1/systemInformation/powerConverters/{new_pc_name}/"
            f"params?startTimeInNanos={start_time}&endTimeInNanos={start_time + duration}"
        )
        logger.debug(f"Querying HWC powering test parameters using the following query: {query}")
        response = requests.get(query)
        if response.status_code == 200:
            d = json.loads(response.text)
            if d:
                res[old_pc_name] = d
            else:
                logger.warning(
                    f"Querying HWC powering test parameters returned no data for the the pc {new_pc_name} "
                    f"(query: {query})"
                )
        else:
            logger.warning(
                f"Querying HWC powering test parameters failed using the following query: {query}, "
                f"status code: {response.status_code}, response: {response.text}"
            )
    return res


def _get_modified_df_from_dictionary(
    dictionary: dict[str, pd.DataFrame], value: str, column_to_drop: str
) -> pd.DataFrame:
    result_df = dictionary.get(value, pd.DataFrame()).rename_axis("timestamp", axis=0)
    if result_df.empty:
        return result_df
    result_df.sort_index(inplace=True)
    result_df.drop(columns=column_to_drop, inplace=True)
    return result_df


def _get_dictionary_from_dataset(
    dataset: SparkDataFrame, timestamp_column: str, grouping_column: str, value_column: str
) -> dict[str, pd.DataFrame]:
    result_df = cast(
        pd.DataFrame, dataset.select([timestamp_column, value_column, grouping_column]).dropna().toPandas()
    )
    result_df.set_index(result_df[timestamp_column], inplace=True)
    result_df.drop(columns=timestamp_column, inplace=True)
    result_df.dropna(inplace=True)
    result_df.sort_index(inplace=True)
    return dict(tuple(result_df.groupby(grouping_column)))


def _build_pm_event_query(query_params) -> str:
    pm_rest_api_path = _PM_ENDPOINT + "/pmdata/header/within/duration"
    keywords_and_argument_concat = "&".join([f"{key}={value}" for (key, value) in query_params.items()])
    return f"{pm_rest_api_path}?{keywords_and_argument_concat}"


def _get_events_from_response(response: dict) -> pd.DataFrame:
    events: dict = {"source": [], "timestamp": []}
    for pm_header in response:
        # filters out the QPS testbed devices (with 'DT' as source)
        if pm_header["source"] != "DT":
            events["source"].append(pm_header["source"])
            events["timestamp"].append(pm_header["timestamp"])
        # the 'new' PM Rest API does not drop the duplicates present in the 'legacy' data
        # https://its.cern.ch/jira/browse/SIGMON-486
    return pd.DataFrame(data=events).astype({"source": str, "timestamp": int}).drop_duplicates()


def _query_pm(query_params, build_query, *args) -> dict:
    pm_query = build_query(query_params, *args)
    logger.debug(f"Querying Post Mortem using the following query: {pm_query}")
    response = requests.get(pm_query)
    try:
        if response.status_code == 200:
            return json.loads(response.text)
        if response.status_code == 204:
            warning = f"Post Mortem returned no {query_params['system']} data for the following query: {pm_query}"
        else:
            warning = f"Querying Post Mortem failed using the following query: {pm_query}"
    except json.decoder.JSONDecodeError:
        warning = f"Querying Post Mortem failed using the following query: {pm_query}"
    logger.warning(warning)
    return {}


def _build_pm_signal_query(query_params, signals) -> str:
    pm_rest_api_path = _PM_ENDPOINT + "pmdata/signal"
    keywords_and_argument_concat = "&".join([f"{key}={value}" for (key, value) in query_params.items()])
    for signal in signals:
        keywords_and_argument_concat += "&signal=" + signal
    return f"{pm_rest_api_path}?{keywords_and_argument_concat}"


def _build_pm_data_query(query_params) -> str:
    pm_rest_api_path = _PM_ENDPOINT + "pmdata"
    keywords_and_argument_concat = "&".join([f"{key}={value}" for (key, value) in query_params.items()])
    return f"{pm_rest_api_path}?{keywords_and_argument_concat}"


def _remove_out_of_order_indices(index: list[int]) -> list[int]:
    # Remove the out of order indices: SIGMON-837
    # This replicates the correction done in PMEA. Sometimes, there exist out-of-order indices in the PM data response,
    # which were removed there.
    result = []
    last_valid_value = float("-inf")

    for i, t in enumerate(index):
        if t >= last_valid_value:
            result.append(i)
            last_valid_value = t

    return result


def _get_time_value(response: dict, signal_name: str, apply_correction: bool) -> pd.Series:
    """Sets time (in ns) and value from the content received with a PM signal query."""
    if not response:
        return pd.Series(name=signal_name).rename_axis(index="timestamp")

    content = response["content"][0]

    full_signal_name = None
    for element in content["namesAndValues"]:
        if signal_name in element["name"]:
            full_signal_name, value = element["name"], element["value"]
            break

    if full_signal_name is None:
        logger.warning(f"Signal {signal_name} not found in the response.")
        return pd.Series(name=signal_name).rename_axis(index="timestamp")

    index = None
    signal_prefix = full_signal_name.split(".")[0]
    for entity in content["namesAndValues"]:
        if entity["name"] in (f"{signal_prefix}.TIMESTAMP", f"{signal_prefix}.AQNTIME"):
            index = entity["value"]
            break

    if index is None:
        # Index has to be generated from t_start and interval
        start_time, interval = None, None
        for entity in content["namesAndValues"]:
            if entity["name"] == f"{signal_prefix}:START_TIME_NSEC":
                start_time = entity["value"]
            elif entity["name"] == f"{signal_prefix}:INTERVAL_NSEC":
                interval = entity["value"]

        if start_time is None or interval is None:
            logger.warning("Time not calculated.")
            return pd.Series(name=signal_name).rename_axis(index="timestamp")

        index = list(range(start_time, start_time + interval * len(value), interval))

    # Remove the out of order indices
    if apply_correction:
        filtered_indices = _remove_out_of_order_indices(index)
        index = [index[i] for i in filtered_indices]
        value = [value[i] for i in filtered_indices]

    signal = pd.Series(value, index, name=signal_name).rename_axis(index="timestamp").sort_index()

    return signal.loc[signal.index != 0 & signal.index.duplicated(keep=False)]
