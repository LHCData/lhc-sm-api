"""Contains logic to resolve the higher level arguments into the real query parameters based on the metadata."""

from __future__ import annotations

import re
from abc import ABC, abstractmethod
from collections.abc import Iterable
from dataclasses import dataclass
from typing import Generic, TypeVar

from lhcsmapi import utils
from lhcsmapi.metadata import signal_metadata
from lhcsmapi.Time import Time, TimeAlias


class ParamsNotCompatibleException(Exception):
    """Indicates that the QueryParams are not compatible (can't be queried together)"""


@dataclass
class PmTriplet:
    """Denotes Pm's triplet"""

    system: str
    class_: str
    source: str


@dataclass
class PmSignals:
    """Denotes Pm's triplet with signals"""

    triplet: PmTriplet
    signals: list[str]


T = TypeVar("T", bound="QueryParams")


class QueryParams(ABC):
    """Abstract class for the query parameters"""

    def merge(self, other: QueryParams) -> QueryParams:
        """Merges compatible parameters

        Args:
            other: parameters to merge

        Returns:
            self

        Raises:
            ParamsNotCompatibleException: if the parameters are not compatible
        """
        self._assert_compatible(other)
        self._merge(other)
        return self

    @abstractmethod
    def _assert_compatible(self, other: T):
        """Checks if 2 QueryParams can be merged together. If not throws ParamsNotCompatibleException"""

    @abstractmethod
    def _merge(self, other):
        """Merges 2 QueryParams into one"""


@dataclass
class NxcalsQueryParams(QueryParams, ABC):
    """Base class for the parameters needed to query Nxcals"""

    system: str
    timestamp: int
    duration: int
    signals: list[str]

    def _assert_compatible(self, other: T):
        if not isinstance(other, NxcalsQueryParams):
            raise ParamsNotCompatibleException(
                f"Incompatible types: expected NxcalsQueryParams, got {type(other).__name__}"
            )

        else:
            if self.system != other.system or self.timestamp != other.timestamp or self.duration != other.duration:
                raise ParamsNotCompatibleException(
                    f"{self.__class__.__name__} objects must have equal system, timestamp and duration to be merge-able"
                )


@dataclass
class VariableQueryParams(NxcalsQueryParams):
    """Class encapsulating a set of parameters needed to query Nxcals by variable"""

    def _merge(self, other: NxcalsQueryParams):
        self.signals.extend([signal for signal in other.signals if signal not in self.signals])


@dataclass
class PmQueryParams(QueryParams, ABC):
    """Base class for the parameters needed to query Post Mortem"""

    timestamp: int

    def _assert_compatible(self, other: T):
        if not isinstance(other, PmQueryParams):
            raise ParamsNotCompatibleException(
                f"Incompatible types: expected PmQueryParams, got {type(other).__name__}"
            )

        if self.timestamp != other.timestamp:
            raise ParamsNotCompatibleException(
                f"{self.__class__.__name__} objects must have equal timestamp to be merge-able"
            )


@dataclass
class PmSignalQueryParams(PmQueryParams):
    """Class encapsulating a set of parameters needed to query Post Mortem for signals"""

    signals: list[PmSignals]

    def _merge(self, other: PmSignalQueryParams):
        for other_params in other.signals:
            with_same_triplet = [params for params in self.signals if params.triplet == other_params.triplet]
            if with_same_triplet:
                with_same_triplet[0].signals.extend(
                    [signal for signal in other_params.signals if signal not in with_same_triplet[0].signals]
                )
            else:
                self.signals.append(other_params)


@dataclass
class PmEventQueryParams(PmQueryParams):
    """Class encapsulating a set of parameters needed to query Post Mortem for events (headers of the dumps)"""

    duration: int
    triplets: list[PmTriplet]

    def _assert_compatible(self, other: T):
        if not isinstance(other, PmEventQueryParams):
            raise ParamsNotCompatibleException(
                f"Incompatible types: expected PmEventQueryParams, got {type(other).__name__}"
            )

        super()._assert_compatible(other)
        if self.duration != other.duration:
            raise ParamsNotCompatibleException("PmEventQueryParams objects must have equal duration to be merge-able")

    def _merge(self, other: PmEventQueryParams):
        for triplet in other.triplets:
            if triplet not in self.triplets:
                self.triplets.append(triplet)


def get_params_for_nxcals(
    circuit_type: str,
    circuit_name: str | Iterable[str],
    system: str,
    timestamp: TimeAlias,
    duration: int | tuple[int, str],
    **kwargs,
) -> NxcalsQueryParams:
    """Translates given arguments into the Nxcals query parameters.

    Args:
        circuit_type: a type of the circuit.
        circuit_name: circuit names of a type circuit_type,
            if '*' all circuits will be queried
        system: a name of the sigmon system
        timestamp: a start time (date-like strings in a local timezone by default)
        duration: duration in nanoseconds or Tuple of number and unit i.e. 250000000000 or (250, 's')

    Keyword Args:
        signals (Union[str, Iterable[str]]): signal names to query (keys in the sigmon's metadata json)
        wildcard (dict): wildcard dict

    Returns:
        An NxcalsQueryParams (VariableQueryParams)
    """
    kwargs["duration"] = duration
    return _resolve(circuit_type, circuit_name, system, timestamp, _NxcalsMetadataResolver, **kwargs)


def get_params_for_pm_signals(
    circuit_type: str, circuit_name: str | Iterable[str], system: str, timestamp: TimeAlias, **kwargs
) -> PmSignalQueryParams:
    """Translates given arguments into the Post Mortem 'signal' query parameters.

    Args:
        circuit_type: a type of the circuit.
        circuit_name: circuit names of a type circuit_type, if '*' all circuits will be queried
        system: a name of the sigmon system
        timestamp: a start time (date-like strings in a local timezone by default)

    Keyword Args:
        signals (Union[str, Iterable[str]]): signal names to query (keys in the sigmon's metadata json)
        wildcard (dict): wildcard dict

    Returns:
        A PmSignalQueryParams
    """
    return _resolve(circuit_type, circuit_name, system, timestamp, _PmSignalMetadataResolver, **kwargs)


def get_params_for_pm_events(
    circuit_type: str,
    circuit_name: str | Iterable[str],
    system: str,
    timestamp: TimeAlias,
    duration: int | tuple[int, str],
    **kwargs,
) -> PmEventQueryParams:
    """Translates given arguments into the Post Mortem 'event' query parameters.

    Args:
        circuit_type: a type of the circuit.
        circuit_name: circuit names of a type circuit_type,
            if '*' all circuits will be queried
        system: a name of the sigmon system
        timestamp: a start time (date-like strings in a local timezone by default)
        duration: duration in nanoseconds or Tuple of number and unit i.e. 250000000000 or (250, 's')

    Keyword Args:
        signals (Union[str, Iterable[str]]): signal names to query (keys in the sigmon's metadata json)
        wildcard (dict): wildcard dict

    Returns:
        A PmEventQueryParams
    """
    kwargs["duration"] = duration
    return _resolve(circuit_type, circuit_name, system, timestamp, _PmEventMetadataResolver, **kwargs)


def _resolve(
    circuit_type: str,
    circuit_name: str | Iterable[str],
    system: str,
    timestamp: TimeAlias,
    resolver_class: type[_MetadataResolver[T]],
    **kwargs,
) -> T:
    timestamp = Time.to_unix_timestamp(timestamp)
    if circuit_name == "*":
        circuit_name = signal_metadata.get_circuit_names(circuit_type, timestamp)
    circuit_names = utils.vectorize(circuit_name)

    if "duration" in kwargs and isinstance(kwargs["duration"], tuple):
        kwargs["duration"] = Time.to_unix_timestamp(kwargs["duration"][0], kwargs["duration"][1])

    timestamp = Time.to_unix_timestamp(timestamp)

    resolvers = [resolver_class(timestamp, system, circuit_name_, **kwargs) for circuit_name_ in circuit_names]
    query_params = None
    for resolver in resolvers:
        resolved = resolver.resolve(kwargs.get("wildcard"))
        if query_params is None:
            query_params = resolved
        else:
            query_params.merge(resolved)
    if not query_params:
        raise ValueError("No query parameters were resolved")
    return query_params


class _MetadataResolver(Generic[T], ABC):
    """Base class for the metadata resolution."""

    def __init__(self, timestamp: int, system: str, circuit_name: str, **kwargs):
        self.timestamp = timestamp
        self.system = system
        self.circuit_type = signal_metadata.get_circuit_type_for_circuit_name(circuit_name)
        self.circuit_name = circuit_name
        self._db = ""

    def resolve(self, wildcards=None) -> T:
        """Translates provided arguments into the real query parameters"""

        metadata = signal_metadata.get_signal_metadata(
            self.circuit_type, self.circuit_name, self.system, self._db, self.timestamp
        )
        return self._resolve(metadata, wildcards)

    @abstractmethod
    def _resolve(self, metadata, wildcards) -> T:
        pass


class _SignalMetadataResolver(_MetadataResolver[T], ABC):
    """Base class for the metadata resolution for the signal queries."""

    def __init__(self, timestamp: int, system: str, circuit_name: str, **kwargs):
        super().__init__(timestamp, system, circuit_name)
        self.signals = kwargs["signals"]
        self.wildcards = kwargs.get("wildcard", {})

    def _resolve_signals(self) -> list[str]:
        """Translates provided signal names into the real ones."""
        signals_with_wildcard = signal_metadata.get_signal_name(
            self.circuit_type, self.circuit_name, self.system, self._db, self.signals, self.timestamp
        )
        signals_with_wildcard = utils.vectorize(signals_with_wildcard)
        resolved_signals = self._replace_wildcard_in_signals(signals_with_wildcard, self.wildcards)
        return resolved_signals

    def _replace_wildcard_in_signals(self, signals_with_wildcard: list[str], wildcards: dict[str, str]) -> list[str]:
        resolved_signals = []
        for signal in signals_with_wildcard:
            if "%" in signal:
                wildcard = re.findall(r"(?<=%)(.*?)(?=%)", signal)[0]

                if wildcard in wildcards:
                    wildcard_value = wildcards[wildcard]
                    if wildcard_value == "*":
                        wildcard_values = signal_metadata.get_wildcard_for_circuit_system_db_signal(
                            self.circuit_type, self.circuit_name, self.system, "nxcals", signal
                        )[1]
                    else:
                        wildcard_values = utils.vectorize(wildcard_value)

                    resolved_signals.extend([signal.replace(f"%{wildcard}%", value_) for value_ in wildcard_values])
                else:
                    raise KeyError(
                        f"In order to fully define a signal name ({signal}), please provide a wildcard as "
                        f"with_metadata(..., wildcard={{'{wildcard}': 'value'}})"
                    )
            else:
                resolved_signals.append(signal)
        return resolved_signals


class _PmSignalMetadataResolver(_SignalMetadataResolver[PmSignalQueryParams]):
    """Class to resolve the metadata for the PM signal queries."""

    def __init__(self, timestamp: int, system: str, circuit_name: str, **kwargs):
        super().__init__(timestamp, system, circuit_name, **kwargs)
        self._db = "PM"

    def _resolve(self, metadata, wildcards) -> PmSignalQueryParams:
        sources = utils.vectorize(metadata["source"])
        pm_signals = [
            PmSignals(PmTriplet(metadata["system"], metadata["className"], resolved_source), self._resolve_signals())
            for source in sources
            for resolved_source in _replace_wildcard(source, wildcards)
        ]
        return PmSignalQueryParams(self.timestamp, pm_signals)


class _PmEventMetadataResolver(_MetadataResolver[PmEventQueryParams]):
    """Class to resolve the metadata for the 'PM event' (pmData header) queries."""

    def __init__(self, timestamp: int, system: str, circuit_name: str, **kwargs):
        super().__init__(timestamp, system, circuit_name)
        self.duration = kwargs["duration"]
        self._db = "PM"

    def _resolve(self, metadata, wildcards) -> PmEventQueryParams:
        sources = utils.vectorize(metadata["source"])
        triplets = [
            PmTriplet(metadata["system"], metadata["className"], resolved_source)
            for source in sources
            for resolved_source in _replace_wildcard(source, wildcards)
        ]
        return PmEventQueryParams(self.timestamp, self.duration, triplets)


class _NxcalsMetadataResolver(_SignalMetadataResolver[NxcalsQueryParams]):
    """Class to resolve the metadata for the Nxcals queries."""

    def __init__(self, timestamp: int, system: str, circuit_name: str, **kwargs):
        super().__init__(timestamp, system, circuit_name, **kwargs)
        self._db = "NXCALS"
        self.duration = kwargs["duration"]

    def _resolve(self, metadata, wildcards) -> NxcalsQueryParams:
        nxcals_system = signal_metadata.get_nxcals_system_name(
            self.circuit_type, self.system, self.signals, self.timestamp
        )
        if not isinstance(nxcals_system, str):
            raise TypeError(f"Expected a string, got {type(nxcals_system)}")
        metadata = metadata[nxcals_system]
        return VariableQueryParams(nxcals_system, self.timestamp, self.duration, self._resolve_signals())


def _replace_wildcard(string: str, wildcards: dict[str, str]) -> list[str]:
    if not wildcards:
        wildcards = {}

    found = re.findall(r"(?<=%)(.*?)(?=%)", string)
    if not found:
        return [string]

    results = [string]
    for wildcard in found:
        wildcard_values = utils.vectorize(wildcards.get(wildcard, []))
        if wildcard_values:
            results = [r.replace(f"%{wildcard}%", value_) for value_ in wildcard_values for r in results]

    return results
