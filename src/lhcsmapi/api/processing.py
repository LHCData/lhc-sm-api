"""This module provides a SignalProcessing class which is used to transform one or more signals.
A signal is represented by a pandas DataFrame with a single column.
"""

from __future__ import annotations

import warnings
from collections.abc import Sequence
from typing import Callable, Generic, TypeVar

import numpy as np
import pandas as pd
from scipy.ndimage import median_filter

from lhcsmapi.metadata import signal_metadata
from lhcsmapi.metadata.MappingMetadata import MappingMetadata
from lhcsmapi.pyedsl.dbsignal.SignalIndexConversion import SignalIndexConversion


def sum_dataframes(df1: pd.DataFrame, df2: pd.DataFrame, col_name: str) -> pd.DataFrame:
    """Function summing the elements of two dataframes and from the result creating a new DataFrame with a given column.
    It is assumed that both DataFrames have only one column.

    Args:
        df1: first dataframe to add
        df2: second dataframe to add
        col_name: column name for the result

    Returns:
        A dataframe with an element-by-element sum of the two input dataframes,
        interpolated index and a given column name
    """
    _validate_dataframes([df1, df2])

    idx = df1.index.union(df2.index)
    idx_df1 = df1.reindex(idx).interpolate("index")
    idx_df2 = df2.reindex(idx).interpolate("index")

    idx_df1.fillna(0, inplace=True)
    idx_df2.fillna(0, inplace=True)

    return pd.DataFrame(index=idx_df1.index, data=idx_df1.values + idx_df2.values, columns=[col_name])


def _calculate_time_derivative(
    df: pd.DataFrame, method="gradient", with_median_filter=True, kernel_size=51
) -> pd.DataFrame:
    column_name = df.columns[0]

    if method == "diff":
        index = np.array(df.index)
        signal = np.array(df[column_name])

        output = np.diff(signal) / np.diff(index)
        index = df.index[1:]  # np.diff skips the first element, since the first element of the derivative
        # is calculated as x[1] - x[0]
    elif method == "gradient":
        output = np.gradient(df[column_name], df.index)
        index = df.index
    else:
        raise ValueError("Method must be either 'gradient' or 'diff'.")

    if with_median_filter:
        return pd.DataFrame(
            median_filter(output, kernel_size, mode="constant"), index=index, columns=[f"d{column_name}"]
        )

    return pd.DataFrame(output, index=index, columns=[f"d{column_name}"])


T = TypeVar("T", pd.DataFrame, list[pd.DataFrame])


class SignalProcessing(Generic[T]):
    """Class provides a set of frequently used methods for signal processing. Each method always returns a reference
    to an object which allows for chaining of method calls and reduce the need for using local variables.

    Examples:
        >>> import pandas as pd
        >>> i_meas_b1 = pd.DataFrame({"STATUS.I_MEAS": {1426220509480000000: 6953.1220}})
        >>> i_meas_b2 = pd.DataFrame({"STATUS.I_MEAS": {1426220509480000000: 6953.1220}})
        >>> i_meas_b1, i_meas_b2 = (
        ...     SignalProcessing([i_meas_b1, i_meas_b2])
        ...     .synchronize_time(1426220509480000000)
        ...     .convert_index_to_sec()
        ...     .rename_columns(["I_MEAS_B1", "I_MEAS_B2"])
        ...     .get_dataframes()
        ... )
        >>> list(i_meas_b1.columns)
        ['I_MEAS_B1']
    """

    def __init__(self, dataframes: T):
        self._dataframes = [df.copy() for df in dataframes] if isinstance(dataframes, list) else [dataframes.copy()]
        _validate_dataframes(self._dataframes)

    def synchronize_time(self, t0: int | None = None) -> SignalProcessing[T]:
        """Method synchronizing time to an input initial timestamp. Method assumes that the time unit of indices and
        t0 are the same (e.g., ns and ns).

        Args:
            t0: initial timestamp for synchronization

        Returns:
            an updated SignalProcessing object with all signals synchronized to the initial timestamp
        """

        self._dataframes = [
            SignalIndexConversion.synchronize_df(df, t0) if not df.empty else df for df in self._dataframes
        ]
        return self

    def convert_index_to_sec(self) -> SignalProcessing[T]:
        """Method converting indices from nanoseconds to seconds

        Returns:
            an updated SignalProcessing object with all signals having indices expressed in seconds
        """

        self._dataframes = [
            SignalIndexConversion.convert_indices_to_sec(df) if not df.empty else df for df in self._dataframes
        ]
        return self

    def rename_columns(self, column_names: list[str]) -> SignalProcessing[T]:
        """Method renaming the first columns in the dataframes

        Args:
            column_names: list of new column names

        Returns:
            an updated SignalProcessing object with new column names

        Raises:
            ValueError: if the number of column names is different than the number of dataframes
        """
        if len(column_names) != len(self._dataframes):
            raise ValueError("Number of column names should be equal to the number of dataframes")

        self._dataframes = [
            df.rename(columns={df.columns[0]: column_names[i]}) for i, df in enumerate(self._dataframes)
        ]
        return self

    def filter_median(self, window: int = 3) -> SignalProcessing[T]:
        """Method applying a median filter of a given window size to each signal

        Args:
            window: median filter window size

        Returns:
            an updated SignalProcessing object with each signal subject to median filtering
        """

        if window > 1:
            self._dataframes = [
                df.rolling(window=window, min_periods=1).median() if not df.empty else df for df in self._dataframes
            ]
        return self

    def remove_values_for_time_less_than_or_equal_to(self, time_threshold: int) -> SignalProcessing[T]:
        """Method removing values for rows less or equal to the time threshold.

        Args:
            time_threshold: time threshold for row removal (rows greater than the threshold are maintained)

        Returns:
            A SignalProcessing object with updated rows
        """

        self._dataframes = [df[df.index > time_threshold] if not df.empty else df for df in self._dataframes]
        return self

    def remove_initial_offset(self, time_threshold: int) -> SignalProcessing[T]:
        """Method removing an initial offset from the signal. The offset is calculated as an average value from the
        beginning of a signal until the input time_threshold (excluded)

        Args:
            time_threshold: time threshold value for offset calculation

        Returns:
            an updated SignalProcessing object with offset value subtracted from each signal
        """

        self._dataframes = [
            df.subtract(df[df.index < time_threshold].mean()) if not df.empty else df for df in self._dataframes
        ]
        return self

    def filter_values(self, values: float) -> SignalProcessing[T]:
        """Method filtering values, i.e., provided list of values is maintained in the first column of each dataframe

        Args:
            values: list of values to maintain

        Returns:
                an updated SignalProcessing object with dataframes containing in the first column
                only the provided values
        """

        self._dataframes = [df[df[df.columns[0]].isin(values)] for df in self._dataframes]
        return self

    def map_values(self, mapping: dict) -> SignalProcessing[T]:
        """Method mapping values in the first column of each dataframe based on the provided mapping dictionary

        Args:
            mapping: a dictionary mapping values between existing and new ones

        Returns:
            an updated SignalProcessing object with dataframes containing mapped values in the first column
        """

        self._dataframes = [
            pd.DataFrame(df.apply(lambda col, df=df: mapping[col[df.columns[0]]], axis=1), columns=df.columns)  # type: ignore
            for df in self._dataframes
            if not df.empty
        ]
        return self

    def append_suffix_to_columns(self, suffix: str) -> SignalProcessing[T]:
        """Method appending a suffix to all columns. No extra character is added.

        Args:
            suffix: suffix string

        Returns:
            an updated SignalProcessing object with a suffix added to all columns in all dataframes
        """

        def append_suffix(df, suffix):
            df.columns = [col + suffix for col in df.columns]
            return df

        self._dataframes = [
            append_suffix(df, suffix) if not df.empty else df for df in self._dataframes if not df.empty
        ]
        return self

    def overwrite_sampling_time(self, t_sampling: float, t_end: float) -> SignalProcessing[T]:
        """Method overwriting an existing index with a new one given sampling time and new index duration.

        Args:
            t_sampling: sampling time for a new index
            t_end: new end time for a new index

        Returns:
            an updated SignalProcessing object with new indices for all dataframes
        """

        def overwrite_sampling_time(df, t_sampling_, t_end_):
            new_index = np.arange(0, len(df), t_end_) * t_sampling_
            df.set_index(new_index, inplace=True)
            return df

        self._dataframes = [overwrite_sampling_time(df, t_sampling, t_end) for df in self._dataframes]
        return self

    def drop_first_n_points(self, n: int) -> SignalProcessing[T]:
        """Method removing first n points from a signal

        Args:
            n: number of first n points to drop

        Returns:
            an updated SignalProcessing object with signals after removing first n rows
        """

        self._dataframes = [df.drop(df.head(n).index) for df in self._dataframes if not df.empty]

        return self

    def drop_last_n_points(self, n: int) -> SignalProcessing[T]:
        """Method removing last n points from a signal

        Args:
            n: number of last n points to drop

        Returns:
            an updated SignalProcessing object with signals after removing last n rows
        """

        self._dataframes = [df.drop(df.tail(n).index) for df in self._dataframes if not df.empty]

        return self

    def get_dataframes(self) -> T:
        """Method returning pandas dataframes

        Returns:
            Pandas dataframe or list of them if there is more than one
        """

        if len(self._dataframes) == 1:
            return self._dataframes[0]
        return self._dataframes

    def calculate_char_time_profile(self, timestamp_event: int = 0) -> SignalProcessing[T]:
        """Function calculating a time evolution characteristic decay time of a current profile

        Args:
            timestamp_event: timestamp of the event in the signal. Every data point before the event will filtered out

        Returns:
            An updated SignalProcessing object
        """

        def _calculate_char_time_profile(df):
            column_name = df.columns[0]
            derivative_column_name = f"d{column_name}"

            signal_derivative = _calculate_time_derivative(df)

            # Calculate time constant as a ratio of a signal by its derivative
            char_time_name = f"{column_name}/d{column_name}"
            kernel_size = 3
            ratio = -df[column_name] / median_filter(
                signal_derivative[derivative_column_name], kernel_size, mode="constant"
            )
            result = pd.DataFrame(data=ratio.values, columns=[char_time_name], index=df.index)
            result.loc[result.index < timestamp_event, [char_time_name]] = np.nan

            return result

        self._dataframes = [_calculate_char_time_profile(df) for df in self._dataframes]

        return self

    def calculate_time_derivative(
        self, method: str = "gradient", with_median_filter: bool = True, kernel_size: int = 51
    ) -> SignalProcessing[T]:
        """Function calculating time derivatives for signal contained in the passed DataFrame object.
        The returned signal name contains prefix d w.r.t. the input signal name, e.g. I_MEAS -> dI_MEAS.

        Args:
            method: selects the method to calculate the derivative.
                gradient: uses the numpy.gradient function to calculate the gradients.
                (The gradient is computed using central differences in the interior and first differences
                at the boundaries. The returned derivative hence has the same shape as the input array)
                diff: uses pandas.DataFrame.diff function to calculate the gradients (The returned derivative hence has
                one value less then the input array)
            with_median_filter: selects whether we should apply a median filter to the output
            kernel_size: kernel size for the mean filter if applied

        Returns:
            an updated SignalProcessing object with derivatives of the signals
        """

        self._dataframes = [
            _calculate_time_derivative(df, method, with_median_filter, kernel_size) for df in self._dataframes
        ]

        return self

    def calculate_last_const_derivative(
        self, const_derivative_duration: int = 100, timestamp_event: int = 0
    ) -> list[tuple[float, float]]:
        """Function calculating the value and duration of the last period (prior to an event) in which the derivative
        of the signal was constant.

        Args:
            const_derivative_duration: threshold value for the duration of the period. If the period lasts for a period
                                       which is smaller of this parameter, it will not be taken into account
            timestamp_event: timestamp of the event in the signal. The algorithm will be executed on the part of the
                             signal prior to the event

        Returns:
            A tuple of two floats. Respectively the value and the duration of the resulting period
        """

        def _calculate_last_const_derivative(df):
            # We select df.index < 0 because we want to find the last const of di/dt prior to the event
            # we assume that the signal is normalized in such a way that the event occurs at t=0
            d_df = _calculate_time_derivative(df[df.index < timestamp_event])

            d_df = d_df.round(1)  # We round to remove noise

            dd_df = _calculate_time_derivative(d_df, kernel_size=1)

            # This df has the same values in rows where the second derivative is constant
            same_values_series = (dd_df.diff(1) != 0).cumsum()

            # Compute the counts of each value
            value_counts = same_values_series.value_counts()

            # Find values that occur more frequently than the constant derivative duration
            values_over_threshold = value_counts.loc[value_counts > const_derivative_duration].index

            df_last_const = d_df.loc[same_values_series[same_values_series.columns[0]] == max(values_over_threshold)[0]]

            return df_last_const.values[0][0], df_last_const.index.max() - df_last_const.index.min()

        output = [_calculate_last_const_derivative(df) for df in self._dataframes]

        return output if len(output) > 1 else output[0]

    def resample(self, ts: int = 0, t0: int | None = None, t_end: int | None = None) -> SignalProcessing[T]:
        """Function resampling a series by interpolating values to get continuous sample frequency

        Args:
            ts: sampling time (if zero, the sampling time is deduced from the first to indices)
            t0: start time for the resampling (if None, then it is deduced as the first index)
            t_end: end time for the resampling (if None, then it is deduced as the last index)

        Returns:
            an updated SignalProcessing object with resampled signals
        """

        def _get_time_point_distribution(ts_: float, t0_: float, t_end_: float) -> np.ndarray:
            t = t_end_ - t0_  # Time period
            n = int(t / ts_)  # Number of sample points
            return np.linspace(t0_, t + t0_, n)

        def _resample(df, ts_, t0_, t_end_):
            # takes sample time of first sample if not given
            if ts_ == 0:
                ts_ = df.index[1] - df.index[0]  # Sample distance

            t0_ = df.index[0] if t0_ is None else t0_
            t_end_ = df.index[-1] if t_end_ is None else t_end_

            index_list = _get_time_point_distribution(ts_, t0_, t_end_)

            # interpolate data in order to have the same sample time everywhere
            interpolated_series = df.reindex(df.index.union(index_list)).interpolate("index", limit_direction="forward")

            # delete those points which are not in in the continuous interval
            del_index = [x for x in df.index if x not in index_list]
            return interpolated_series.drop(del_index, axis=0)

        self._dataframes = [_resample(df, ts, t0, t_end) if not df.empty else df for df in self._dataframes]

        return self

    def decode_states(self, column_name: str | int, states: list[str]) -> SignalProcessing[T]:
        """Converts a single column state into a multiple boolean state columns - a separate one for each state provided

        Args:
            column_name: a name of the column with the states to decode, or an index of the column
            states: a list with the states to find

        Returns:
            an updated SignalProcessing object with a new column for each state which is named as {column_name}:{state}

        Examples:
            >>> from nxcals.spark_session_builder import get_or_create
            >>> from lhcsmapi.api import query
            >>> from lhcsmapi.api import processing
            >>> spark = get_or_create() # doctest: +SKIP
            >>> t_start = 1625169378035000000
            >>> t_end = 1625171037645000000
            >>> pc_states = query.query_cmw_by_variables(
            ...     spark, t_start, t_end - t_start, f"RPHGA.UJ67.RQ10.R6B1:STATE"
            ... ) # doctest: +SKIP
            >>> print(pc_states.to_markdown()) # doctest: +SKIP
            |           timestamp | RPHGA.UJ67.RQ10.R6B1:STATE   |
            |--------------------:|:-----------------------------|
            | 1625169389080000000 | OFF                          |
            | 1625169389580000000 | STARTING                     |
            | 1625169391580000000 | TO_STANDBY                   |
            | 1625169417080000000 | ON_STANDBY                   |
            | 1625169477580000000 | IDLE                         |
            | 1625169478080000000 | ARMED                        |
            | 1625169479080000000 | RUNNING                      |
            | 1625169496580000000 | IDLE                         |
            | 1625169797580000000 | ARMED                        |
            | 1625169798580000000 | RUNNING                      |
            | 1625169971080000000 | IDLE                         |
            | 1625170655580000000 | SLOW_ABORT                   |
            | 1625170873580000000 | FLT_OFF                      |
            >>> resolved_pc_states = (
            ...     processing.SignalProcessing(pc_states).decode_states(0, ["SLOW_ABORT", "FLT_OFF"]).get_dataframes()
            ... )  # doctest: +SKIP
            >>> print(resolved_pc_states.to_markdown()) # doctest: +SKIP
            |           timestamp | RPHGA.UJ67.RQ10.R6B1:STATE   | SLOW_ABORT   | FLT_OFF   |
            |--------------------:|:-----------------------------|:-------------|:----------|
            | 1625169389080000000 | OFF                          | False        | False     |
            | 1625169389580000000 | STARTING                     | False        | False     |
            | 1625169391580000000 | TO_STANDBY                   | False        | False     |
            | 1625169417080000000 | ON_STANDBY                   | False        | False     |
            | 1625169477580000000 | IDLE                         | False        | False     |
            | 1625169478080000000 | ARMED                        | False        | False     |
            | 1625169479080000000 | RUNNING                      | False        | False     |
            | 1625169496580000000 | IDLE                         | False        | False     |
            | 1625169797580000000 | ARMED                        | False        | False     |
            | 1625169798580000000 | RUNNING                      | False        | False     |
            | 1625169971080000000 | IDLE                         | False        | False     |
            | 1625170655580000000 | SLOW_ABORT                   | True         | False     |
            | 1625170873580000000 | FLT_OFF                      | False        | True      |
        """

        def _decode_state(df: pd.DataFrame) -> pd.DataFrame:
            for state in states:
                df[state] = (
                    df.apply(lambda row, state=state: state in row[column_name], axis=1) if not df.empty else None  # type: ignore
                )
            return df

        self._dataframes = [_decode_state(df) for df in self._dataframes]
        return self


class EventProcessing:
    """Class provides a set of frequently used methods for event processing. Each method always returns a reference
    to an object which allows for chaining of method calls and reduce the need for using local variables.
    """

    def __init__(self, dataframe: pd.DataFrame) -> None:
        self._dataframe = dataframe.copy()

    def filter_source(self, circuit_type: str, circuit_name: str, system: str) -> EventProcessing:
        """Filters out QH, QDS and DIODE events that do not come from a given circuit.

        Args:
            circuit_type: circuit type
            circuit_name: name of a circuit with sources to keep
            system: name of a system to consider

        Returns:
            an updated EventProcessing object with events belonging to a given circuit name and system
        """

        if self._dataframe.empty:
            return self

        if system in ["QH", "QDS"]:
            belongs_to_circuit = _belongs_to_circuit_for_qh_or_qds(circuit_type, circuit_name)
        elif system in ["DIODE_RB", "DIODE_RQD", "DIODE_RQF"]:
            belongs_to_circuit = _belongs_to_circuit_for_diode(circuit_type, circuit_name)
        else:
            return self

        self._dataframe = self._dataframe[self._dataframe.apply(belongs_to_circuit, axis=1)]

        return self

    def drop_duplicates(self, column: str) -> EventProcessing:
        """Method dropping rows that contain duplicate values in a given column.

        Args:
            column: column name

        Returns:
            an updated EventProcessing object without duplicates in a given column
        """

        if self._dataframe.empty:
            return self

        self._dataframe = self._dataframe.drop_duplicates(column).sort_values(by=["timestamp"])
        return self

    def keep_sources_for_circuit_names(
        self, circuit_type: str, circuit_names: list[str], system: str, timestamp_query
    ) -> EventProcessing:
        """Method keeping sources that correspond to the list of circuit names. It is useful when a given circuit
        family is queried, e.g., for search of FGC PM timestamps in 600A, 80-120A, 60A.

        Args:
            circuit_type: circuit type
            circuit_names: list of circuit names for which FGC PM sources should be maintained
            system: system name, typically PC
            timestamp_query: timestamp query to select an appropriate metadata

        Returns:
            an updated EventProcessing object with sources corresponding to the circuit names
        """

        if self._dataframe.empty:
            return self

        pc_names = [
            signal_metadata.get_signal_metadata(circuit_type, circuit_name, system, "PM", timestamp_query)["source"]
            for circuit_name in circuit_names
        ]
        mask = self._dataframe["source"].isin(pc_names)
        self._dataframe = self._dataframe[mask]

        return self

    def drop_duplicate_source(self) -> EventProcessing:
        """Method dropping rows that contain duplicate sources, e.g., when events come from redundant boards.

        Returns:
            an updated EventProcessing object without duplicate sources
        """

        if self._dataframe.empty:
            return self

        self._dataframe = self._dataframe.drop_duplicates(subset=["source"]).sort_values(by=["timestamp"])
        return self

    def sort_values(self, by: str | Sequence[str]) -> EventProcessing:
        """Method sorting values by selected column name. In addition, the index is reset.

        Args:
            by: column name by which the sorting is performed

        Returns:
            an updated EventProcessing object with sorted DataFrame by selected column
        """

        if self._dataframe.empty:
            return self

        self._dataframe.sort_values(by=by, inplace=True)
        return self

    def get_dataframe(self) -> pd.DataFrame:
        """Method returning the processed pandas dataframe

        Returns:
            Processed pandas dataframe
        """

        self._dataframe.reset_index(drop=True, inplace=True)
        return self._dataframe


def _belongs_to_circuit_for_qh_or_qds(circuit_type: str, circuit_name: str) -> Callable:
    circuit_types_with_circuit_name_as_source = ["IT", "IPQ", "IPD"]
    circuit_types_with_magnet_cell_as_source = ["RQ", "RB"]

    if circuit_type in circuit_types_with_circuit_name_as_source:
        return lambda row: row.source == circuit_name
    if circuit_type in circuit_types_with_magnet_cell_as_source:
        return lambda row: any(
            row.source == cell for cell in MappingMetadata.get_cells_for_circuit_names(circuit_type, circuit_name)
        )

    return lambda _: True


def _belongs_to_circuit_for_diode(circuit_type: str, circuit_name: str) -> Callable:
    supported_circuits = ["RQ", "RB"]

    if circuit_type in supported_circuits:
        return lambda row: any(
            row.source == crate for crate in MappingMetadata.get_crates_for_circuit_names(circuit_type, circuit_name)
        )

    return lambda _: True


def _validate_dataframes(dataframes):
    for df in dataframes:
        if len(df.columns) != 1:
            raise ValueError("Dataframe should have only one column")


class FeatureProcessing:
    """Class for processing of results of feature queries"""

    def __init__(self, dataframe: pd.DataFrame):
        self._dataframe = dataframe

    def sort_busbar_location(self, circuit_type: str, circuit_name: str, split: str = ":") -> FeatureProcessing:
        """Method sorting busbar location for RB and RQ circuits according to their physical location in the
        LHC tunnel.

        Args:
            circuit_type: circuit type (RB and RQ supported)
            circuit_name: name of a circuit, e.g., RB.A12
            split: character on which signal name is split to recover busbar name for sorting

        Returns:
            an updated FeatureProcessing object
        """
        if circuit_type not in ["RB", "RQ"]:
            return self

        if "nxcals_variable_name" in self._dataframe.columns:
            circuit_names = signal_metadata.get_circuit_names(circuit_type) if circuit_type == "RB" else [circuit_name]

            self._dataframe["name_prefix"] = self._dataframe.apply(
                lambda row: row["nxcals_variable_name"].split(split)[0], axis=1
            )
            busbars_ordered_df = MappingMetadata.read_busbar_to_magnet_mapping(circuit_type)
            is_busbar_in_circuit_name = busbars_ordered_df["Circuit"].isin(circuit_names)
            busbars_ordered_per_circuit_df = busbars_ordered_df[is_busbar_in_circuit_name]
            self._dataframe["name_prefix"] = pd.Categorical(
                self._dataframe["name_prefix"],
                categories=busbars_ordered_per_circuit_df["Bus Bar Segment Name"],
                ordered=True,
            )
            self._dataframe.sort_values(by="name_prefix", inplace=True)
            self._dataframe.drop(columns="name_prefix", inplace=True)
            self._dataframe.reset_index(inplace=True, drop=True)
        else:
            warnings.warn(
                f"This operation is only supported to feature DataFrame with nxcals_variable_name column, "
                f"got {self._dataframe.columns}"
            )

        return self

    def correct_voltage_sign(
        self,
        variable_name: str = "nxcals_variable_name",
        class_compare: str | float = 0,
        correction_col: str = "mean",
        polarity: int = -1,
    ) -> FeatureProcessing:
        """Method providing alteration of polarity to groups of data, by comparing a selected columns
        value, from a selected class to a chosen expected polarity. E.g. Comparing the value of the 'mean' column for
        a Class representing inductive voltage at ramp, to a polarity of -1, will result in swapped polarity for all
        related classes (including selected class) if statement yields true

        Examples:
            - Create default check and correction: Checks class == 0, column that is being checked and
            corrected is 'mean', and we are checking for negative polarity.
            feature_query takes in function 'translate_udf' that creates the 'class'-column needed to complete
            correction of voltage sign

            >>> df = (
            ...     QueryBuilder()
            ...     .with_nxcals(spark)
            ...     .with_duration(t_start=t_start_injs, t_end=t_end_sbs)
            ...     .with_circuit_type(circuit_type)
            ...     .with_metadata(
            ...         circuit_name=["RB.A12", "RB.A45"],
            ...         system="BUSBAR",
            ...         signal="U_MAG",
            ...         wildcard={"BUSBAR": ["DCBB.8L2.R", "DCBA.15R4.R"]},
            ...     )
            ...     .feature_query(["mean"], function=translate_udf)
            ...     .correct_voltage_sign()
            ...     .df
            ... )  # doctest: +SKIP
            >>> print(df) # doctest: +SKIP
            nxcals_variable_name 	class 	mean
            DCBA.15R4.R:U_MAG 	2 	-0.001142
            DCBA.15R4.R:U_MAG 	0 	0.632161
            DCBA.15R4.R:U_MAG 	1 	-0.001239
            DCBB.8L2.R:U_MAG 	1 	0.001861
            DCBB.8L2.R:U_MAG 	0 	0.636315
            DCBB.8L2.R:U_MAG 	2 	0.001983


        Assumes:
            - feature_query includes function to classify, and add, classes to dataframe
            - For each variable_name there is only one of each class type
            - Data from selected correction_col contains numbers value
            - There are groups of data with same variable name

        Args:
            variable_name: Name of column used to group data points together for value comparison.
            class_compare: Class checked for polarity, action performed if polarity chosen equals True.
            correction_col: Chosen value for polarity change.
            polarity: Check for selected polarity, can be set either to 1 or -1

        Returns:
            self._dataframe with altered polarity to selected column
        """
        if (correction_col not in self._dataframe) or ("class" not in self._dataframe):
            warnings.warn(
                f"This operation is only supported to feature DataFrame with class column, and the selected "
                f"{correction_col} column. Got {self._dataframe.columns}"
            )

        def correction(df, class_cmpr, cor_col, pol, num_of_rows=3):
            if len(df) < num_of_rows:
                return df

            class_change = df["class"].unique()
            idx = df.index.unique()

            if (
                (class_cmpr in class_change)
                & (~df[cor_col].isnull().values.any())
                & (np.sign(df[df["class"] == class_cmpr][cor_col].iloc[0]) == pol)
            ):
                for i in range(len(class_change)):
                    df = df.assign(
                        mean=df[cor_col].replace(
                            {df[cor_col][idx[int(class_change[i])]]: df[cor_col][idx[int(class_change[i])]] * -1}
                        )
                    )
            return df

        self._dataframe = self._dataframe.groupby(variable_name, group_keys=False).apply(
            lambda row: correction(row, class_compare, correction_col, polarity)
        )
        return self

    def sort_values(self, by: str) -> FeatureProcessing:
        """Method sorting values by selected column name. In addition the index is reset.

        Args:
            by: column by which the sorting is performed

        Returns:
            an updated FeatureProcessing object with sorted DataFrame by selected column
        """
        self._dataframe.sort_values(by=by, inplace=True)
        self._dataframe.reset_index(inplace=True, drop=True)
        return self

    def get_dataframe(self) -> pd.DataFrame:
        """Method returning the processed pandas dataframe

        Returns:
            Processed pandas dataframe
        """

        return self._dataframe
