"""This module contains the function calculate_features which helps calculating a
set of features for a set of dataframes."""

from typing import List, Union

import pandas as pd


class WrongColumnNumberException(Exception):
    """Raised when the number of columns of a given pandas datafram is not the expected one"""


def take_window(
    dfs: Union[pd.DataFrame, List[pd.DataFrame]], t_start: float, t_end: float
) -> Union[pd.DataFrame, List[pd.DataFrame]]:
    """Function that selects a given time period from the dataframe.

    Args:
        dfs: A single dataframe or a list of dataframes
        t_start: The lower bound of the selected period
        t_end: The upper bound of the selected period

    Returns:
        Depending on the input, a single dataframe or a list of dataframes with the selected time period.
    """

    def select_window(input_df):
        return input_df[(input_df.index > t_start) & (input_df.index < t_end)]

    if isinstance(dfs, list) and all(isinstance(df, pd.DataFrame) for df in dfs):
        return [select_window(df) for df in dfs]
    if isinstance(dfs, pd.DataFrame):
        return select_window(dfs)

    raise TypeError("Input is neither a pandas dataframe nor a list of pandas dataframes")


def subtract_min_value(dfs: Union[pd.DataFrame, List[pd.DataFrame]]) -> Union[pd.DataFrame, List[pd.DataFrame]]:
    """Function that subracts the value of the min value from a dataframe.

    Args:
        dfs: A single dataframe or a list of dataframes

    Returns:
        Depeding on the input, a single dataframe or a list of dataframes with the min value subtracted.
    """

    if isinstance(dfs, list) and all(isinstance(df, pd.DataFrame) for df in dfs):
        return [df - df.min() for df in dfs]
    if isinstance(dfs, pd.DataFrame):
        return dfs - dfs.min()

    raise TypeError("Input is neither a pandas dataframe nor a list of pandas dataframes")


def _validate_dataframes(dfs):
    if isinstance(dfs, list) and not all(isinstance(df, pd.DataFrame) for df in dfs):
        raise TypeError("Input dfs is a list but it contains elements that are not pd.DataFrame")

    if isinstance(dfs, list) and any(df.shape[1] != 1 for df in dfs):
        raise WrongColumnNumberException("Every dataframe should have 1 column")


def calculate_features(
    dfs: Union[pd.DataFrame, List[pd.DataFrame]], features, new_index=0, concat_dfs=True
) -> Union[float, pd.DataFrame, List[pd.DataFrame]]:
    """Function that calculates feature(s) over a single (or a list of) single-column pandas dataframe.

    If a single dataframe and a single function are given as input, the resulting float is directly returned.
    Otherwise, the features will be calculated using the pd.DataFrame.aggregate function over all the dataframes.
    If concat_dfs==True, the output dataframes will be concatenated using pd.concat function
    and the resulting dataframe would be:

    eg. 2 dataframes and 2 features
    index     | column1:feature1 | column1:feature2 | column2:feature1 | column2:feature2 |
    new_index |                  |                  |                  |                  |

    If concat_dfs==False a list of dataframes would be returned:

    eg. 2 dataframes and 2 features
    index     | column1:feature1 | column1:feature2 |
    new_index |                  |                  |

    index     | column2:feature1 | column2:feature2 |
    new_index |                  |                  |

    The name of the feature in the column name of the output is given by the __name__ attribute of the function.
    Given this behaviour, when passing lambdas as features, their columns would contain the string <lambda>.

    Args:
        dfs: A single dataframe or a list of dataframes on which the features are calculated.
        features: A single function or a list of functions to apply to the input.
        new_index: The index of the single row of the output dataframe
        concat_dfs: A boolean that indicates whether the output dataframes are to be returned after concatenating
                           or as a list.

    Returns:
        If concat_dfs==True a list of pd.DataFrame, otherwise a single pd.DataFrame.

    Examples:
        >>> import numpy as np
        >>> from lhcsmapi.signal_analysis import features, functions as signal_features_functions
        >>> df = pd.DataFrame(
        ...     {
        ...         "STATUS.I_MEAS": {
        ...             0.0: 11599.961,
        ...             0.02: 11599.955,
        ...             0.04: 11599.912,
        ...             0.06: 11599.891,
        ...             0.08: 11599.746,
        ...             0.1: 11598.213,
        ...             0.12: 11596.855,
        ...             0.14: 11595.49,
        ...             0.16: 11594.128,
        ...             0.18: 11592.792,
        ...             0.2: 11591.598,
        ...             0.22: 11590.396,
        ...             0.24: 11589.204,
        ...             0.26: 11588.013,
        ...             0.28: 11586.828,
        ...             0.3: 11585.6455,
        ...             0.32: 11584.47,
        ...         }
        ...     }
        ... )
        >>> print(features.calculate_features([df, df], np.mean).to_markdown())
        |    |   STATUS.I_MEAS:mean |   STATUS.I_MEAS:mean |
        |---:|---------------------:|---------------------:|
        |  0 |              11593.7 |              11593.7 |
        >>> features.calculate_features(df, np.mean)
        11593.711617647059
        >>> print(
        ...     features.calculate_features(
        ...         df, [np.mean, signal_features_functions.miits, lambda df: df.max()]
        ...     ).to_markdown()
        ... )
        |    |   STATUS.I_MEAS:mean |   STATUS.I_MEAS:miits |   STATUS.I_MEAS:<lambda> |
        |---:|---------------------:|----------------------:|-------------------------:|
        |  0 |              11593.7 |                43.013 |                    11600 |
    """

    _validate_dataframes(dfs)

    if isinstance(dfs, pd.DataFrame) and not isinstance(features, list):
        # We directly resturn the scalar (we have 1 dataframe and 1 feature to apply)
        if dfs.shape[1] != 1:
            raise WrongColumnNumberException("Input dataframe should have 1 column")

        return dfs.apply(features).iloc[0]

    if not isinstance(dfs, list):
        dfs = [dfs]

    # If we pass a single function to pd.DataFrame.aggregate we get as a result a pd.Series, which is not compatibile
    # with our code (eg. it doesn't have columns). So when we have a single feature we need to pass a list
    # with a single element doing so, pd.DataFrame.aggregate will return a pd.DataFrame
    if not isinstance(features, list):
        features = [features]

    output_dfs = []

    for df in dfs:
        if not df.empty:
            features_df = df.aggregate(features).T

            # features_df.index is now composed by the old column names
            features_column_names = {col: f"{features_df.index[0]}:{col}" for col in features_df.columns}

            features_df = features_df.rename(columns=features_column_names)
            features_df.index = [new_index]
        else:
            # the dataframe is empty but it has a column, we return a dataframe composed
            # similar to how it would look if we would apply the functions but without data

            features_names = [feature.__name__ if not isinstance(feature, str) else feature for feature in features]
            features_column_names = [f"{df.columns[0]}:{feature_name}" for feature_name in features_names]
            features_df = pd.DataFrame(columns=features_column_names, index=[new_index])

        output_dfs.append(features_df)

    return pd.concat(output_dfs, axis=1) if concat_dfs else output_dfs
