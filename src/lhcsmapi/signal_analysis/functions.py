"""This module contains a set of features calculation function which take a single-column dataframe in input
and return a float. They can also be used as parameters for lhcsmapi.signal_analysis.features.calculate_features"""

import pandas as pd
import numpy as np
import scipy.optimize


def tau_charge(df: pd.DataFrame) -> float:
    """Function returning the time constant of an exponential decay computed with the charge approach

    Args:
        df: input signal

    Returns:
        Time constant of an exponential decay computed with the charge approach
    """
    return calculate_integral_of_exp_decay(df, exponent=1)


def tau_energy(df: pd.DataFrame) -> float:
    """Function returning the time constant of an exponential decay computed with the energy approach

    Args:
        df: input signal

    Returns:
        Time constant of an exponential decay computed with the energy approach
    """
    return calculate_integral_of_exp_decay(df, exponent=2)


def calculate_integral_of_exp_decay(exp_decay_in_sec: pd.DataFrame, exponent: int) -> float:
    """Function calculating a characteristic time constant

    Args:
        exp_decay_in_sec: input dataframe with index in seconds
        exponent: exponent for integral calculation (either 1 or 2)

    Returns:
        Integral of an exponential decay
    """
    # suppresses "divide by zero encountered in divide" warnings
    # might happen if exp_decay_in_sec_pow.iloc[0] == exp_decay_in_sec_pow.iloc[-1]
    # or if the signal is constant
    exp_decay_in_sec_pow = exp_decay_in_sec.pow(exponent)
    with np.errstate(divide="ignore"):
        integral = (
            exponent
            * np.trapz(exp_decay_in_sec_pow, x=exp_decay_in_sec_pow.index, axis=0)
            / (exp_decay_in_sec_pow.iloc[0] - exp_decay_in_sec_pow.iloc[-1])
        )
    return integral


def tau_lin_reg(df: pd.DataFrame) -> float:
    """Function returning the time constant of an exponential decay computed with linear fit to a logarithm of an
    exponential decay.

    Args:
        df: input signal

    Returns:
        Characteristic time constant
    """
    right_exp = lambda p, x: -p[1] * x + p[0]
    err = lambda p, x, y: (right_exp(p, x) - y)
    time = df.index
    rate = np.log(df.values)
    v0 = [0.0, time.min()]
    out = scipy.optimize.leastsq(err, v0, args=(time, rate))
    v = out[0]  # fit parameters out []
    time_constant = 1 / v[1]
    return time_constant


def tau_exp_fit(df: pd.DataFrame) -> float:
    """Function returning the time constant of an exponential decay computed with an exponential function fit.

    Args:
        df: input signal

    Returns:
        Characteristic time constant
    """
    right_exp = lambda p, x: p[0] * np.exp(-p[2] * (x - p[1]))
    err = lambda p, x, y: (right_exp(p, x) - y)
    time = df.index
    rate = df.values
    v0 = [0.0, 0.0, 0.0]
    out = scipy.optimize.leastsq(err, v0, args=(time, rate))
    v = out[0]  # fit parameters out []
    time_constant = 1 / v[2]
    return time_constant


def miits(i_meas_df: pd.DataFrame, no_digits=3) -> float:
    """Method computing MIITs - time integral of a square of current divided by 1e6 in order to reduce the exponent

    Args:
        i_meas_df: input dataframe with current
        no_digits: number of digits after the decimal point for rounding

    Returns:
        Value of MIITs for the input current dataframe
    """
    integral_result = np.trapz(i_meas_df.values.reshape(len(i_meas_df.index)) ** 2, i_meas_df.index) * 1e-6
    no_digits = no_digits if integral_result > 1 else 5
    return round(integral_result, no_digits)
