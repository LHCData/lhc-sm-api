from __future__ import annotations

from typing import Any

import pandas as pd


def get_regions_under_threshold(
    ser: pd.Series, threshold: float, min_duration: float | None = None
) -> list[tuple[Any, Any]]:
    """Algorithm to find regions where the values that are close to 0 within the given threshold.
    Note: this function can be used to find plateaus in a signal by using the first derivative as input.

    Args:
        ser: the series to analyze
        threshold: the absolute threshold under which the regions are considered
        min_duration: the minimum duration of a region (to exclude noise related regions for example)

    Returns:
        A list of tuples containing the start and end index of the regions
    """
    if ser.empty:
        return []

    regions: list[tuple[Any, Any]] = []
    start = None
    abs_ser = ser.abs()
    for i in range(len(abs_ser)):
        if abs_ser.iloc[i] < threshold:
            if start is None:
                start = abs_ser.index[i]
        else:
            if start is not None:
                regions.append((start, abs_ser.index[i - 1]))
                start = None

    if start is not None:
        regions.append((start, abs_ser.index[-1]))

    # filter out regions that are too short
    if min_duration is not None:
        regions = [(start, end) for start, end in regions if end - start >= min_duration]

    return regions
