# Getting started

???+ note
    If you want to access this API from SWAN, please consult the [sigmon-swan-deployer README](https://gitlab.cern.ch/mpe-ms/sigmon/sigmon-swan-deployer) for further instructions.

## NXCALS Access

### Request NXCALS access

To perform a query of signals from NXCALS, you are required to obtain a dedicated access right.

Therefore, if you want to query NXCALS with the API, please request the access following the procedure described in <http://nxcals-docs.web.cern.ch/current/user-guide/data-access/nxcals-access-request/>. You will need to request access to the WinCCOA and CMW systems in the PRO environment.

### Connect to Kerberos

Once the access is granted, you need an active a Kerberos ticket. Kerberos is an authentication protocol that allows you to prove your identity to the NXCALS server.

You can test if Kerberos is already installed by using the following command:

```console
klist
```

If nothing is returned, proceed with the installation of Kerberos.

- For Debian systems (including Ubuntu):

    ```console
    sudo apt-get install krb5-user
    ```

- For Red Hat systems (including Alma, RHEL and Fedora):

    ```console
    sudo yum install krb5-workstation krb5-libs krb5-auth-dialog
    ```

If you have had to install Kerberos, or if Kerberos was already installed on your personal machine, you probably need to set the configuration file for it. To do this, please refer to the documentation at <https://linux.web.cern.ch/docs/kerberos-access/#client-configuration-kerberos>.

Once this is done, you can create your Kerberos ticket using kinit:

```console
kinit <your-cern-id>
```

## Create a virtual environment with `venv`

You can create an isolated virtual local environment in a directory using Python's `venv` module. Let's do this in the cloned repository (where the `pyproject.toml` is):

```console
python -m venv env
```

Activate the new environment with:

```console
source ./env/bin/activate
```

If it shows the `pip` binary at `env/bin/pip` then it worked. 🎉

Make sure you have the latest pip version on your local environment to avoid errors on the next steps:

```console
python -m pip install --upgrade pip
```

!!! tip
    Every time you install a new package with `pip` under that environment, activate the environment again.
    This makes sure that if you use a terminal program installed by that package, you use the one from your local environment and not any other that could be installed globally.

## Install package and dependencies using pip

After activating the environment as described above, we need to add the ACC-PY package repository to able to install CERN specific dependencies like `nxcals`.

If you don't have 2FA activated for your CERN account, you can execute the following command:

```console
pip install git+https://gitlab.cern.ch/acc-co/devops/python/acc-py-pip-config.git
```

Otherwise, you will need to create a SSH key to communicate with GitLab. You can find information on how to do so [here](https://docs.gitlab.com/ee/user/ssh.html). Once this is done, you can use the following command:

```console
pip install git+ssh://git@gitlab.cern.ch:7999/acc-co/devops/python/acc-py-pip-config.git
```

Once this step is done, you should be able to install the lhc-sm-api package and all its dependencies.

```console
pip install lhcsmapi
```

Congrats! The package should now be installed. You should now be able to run the following command:

```console
python -c "import lhcsmapi; print('Installation successful')"
```
