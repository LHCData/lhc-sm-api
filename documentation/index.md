<h1 style="color: #1e65b4;text-align: center; font-size: 72px;"> <img src="assets/logo.png" width="51" alt="logo"> <strong>Sigmon API</strong></h1>

The Signal Monitoring API, or Sigmon API for short, is a project aiming at developing tools to help experts analyse quenches and results from tests during Hardware Commissioning campaigns as well as events occurring during operation of superconducting circuits.

The goal of this documentation is to provide a clear way on how to use the different available methods of `lhc-sm-api`.
