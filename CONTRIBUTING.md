# Contributing

## From GITLAB 
https://gitlab.cern.ch/LHCData/lhc-sm-api

### Create a new branch

1. Create a new branch from the existing `dev` branch. https://gitlab.cern.ch/LHCData/lhc-sm-api/-/branches/new

![gitlab-new-branch](resources/contributing/new-branch.jpg)

3. Select the new branch, browse and open the file you need to edit (you can use the `EDIT` button or `Web IDE`).

![gitlab-select-file](resources/contributing/select-file-in-branch.jpg)

4. Edit the file then click on the `Commit` button.
- Add a `Commit Message`. /!\ Mention the JIRA SIGMON task number (eg: SIGMON-241) at the beginning of your message.
- Select `Commit to my-new-branch`
- In case you want to open a merge request, Tick the box `Start a new merge request`

![gitlab-commit-web-ide](resources/contributing/commit-web-ide.jpg)

5. Create a new merge request. 
- Source branch: select the `my-new-branch`.
- Target branch: select `dev` branch.
- Fill the `Description` box
- Click `Assign to me`.
- Select reviewers from the SIGMON software team.
- Review the changes and click `Create Merge Request` button at the bottom.

![gitlab-merge-request](resources/contributing/new-merge-request.jpg)

You can access the list of merge requests in the dedicated tab: https://gitlab.cern.ch/LHCData/lhc-sm-api/-/merge_requests


6. Notify the reviewers that the merge request is open. Please contact `lhc-signal-monitoring@cern.ch`.
If needed, you can review the changes from the branch and add comments to the merge request.

### The Continuous Integration (CI) pipeline
The continuous integration (CI) pipeline is used to apply run all tests against your branch, to build the documentation and to deploy the API venv on EOS.
The continuous integration pipeline is automatically triggered against your new branch on every commit.
You can access the list of pipelines : https://gitlab.cern.ch/LHCData/lhc-sm-api/-/pipelines

__IMPORTANT__ If the pipeline fails, the API `venv` will __NOT__ be deployed and will __NOT__ be accessible on SWAN.

#### Manual triggering of the CI pipeline
If you need to restart the pipeline, you can trigger a new pipeline against your branch : https://gitlab.cern.ch/LHCData/lhc-sm-api/-/pipelines/new
- Select `my-new-branch`
- Click `Run Pipeline`

![gitlab-merge-request](resources/contributing/manual_pipeline.png)

### DEPLOY your branch

#### Protect your branch
In order to be deployed on EOS from the CI pipeline, your branch needs to be protected.
1. Open the `Protected branches` tab from the `Setting->Repository` options panel. https://gitlab.cern.ch/LHCData/lhc-sm-api/-/settings/repository
2. Select the correct configuration.
- `Branch` --> your-branch
- `Allowed to merge` --> Developers + Maintainers
- `Allowed to push` --> Developers + Maintainers
- `Allowed to force push` --> uncheck
- `Require approval from code owners` --> check

![gitlab-protected-branch](resources/contributing/protected-branch.jpg)

3. Click `Protect` to protect your branch. 

__IMPORTANT__ this step must be done before the pipeline is triggered, otherwise the pipeline must be restarted.

#### LOAD your branch in SWAN
1. To load the new branch environment you must restart the SWAN configuration with the option Environment script: `/eos/project/l/lhcsm/public/my-new-branch.sh`
1. __IMPORTANT__ this will not create a new working folder. You need to clone the `https://gitlab.cern.ch/LHCData/lhc-sm-hwc` into you SWAN project.
1. __IMPORTANT__ Once the branch is deleted or merged it is not possible to load it anymore.

### DEPLOY to DEV 

1. __When all reviewers have approved the changes in the merge request__, you can click the `Merge` button.

2. The `DEV` [pipeline](https://gitlab.cern.ch/LHCData/lhc-sm-api/-/pipelines) has been triggered. 
  The pipeline is completed when all steps are green. Otherwise, please contact `lhc-signal-monitoring@cern.ch`
_IMPORTANT__ This pipeline needs to run all the notebook tests, so it takes longer (~ 3 hours) than the usual branch pipeline. 

3. To load the `DEV` environment you can restart the SWAN configuration with the option Environment script: `/eos/project/l/lhcsm/public/dev.sh`

4. On this `DEV` environment experts (you?) can validate that all the changes you introduced are correct.

### DEPLOY TO MASTER
1. __IMPORTANT__ -> Increment the __version number__ in the [__init.py__](https://gitlab.cern.ch/LHCData/lhc-sm-api/-/blob/dev/lhcsmapi/__init__.py#L2) file.
2. Open a merge request from source branch `dev` to target branch `master`.
3. Once the merge request has been reviewed and approved then click on the `Merge` button of the merge request page.

### RELEASE TO PRO
1. When changes are validated, you must __TAG__ the `master` branch with the previously given **_version_ _number_**: https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/tags/new
1. The `PRO` [pipeline](https://gitlab.cern.ch/LHCData/lhc-sm-api/-/pipelines) is now triggered. 
  The pipeline is completed when all steps are green. Otherwise, please contact `lhc-signal-monitoring@cern.ch`
1. To load the `PRO` environment you can restart the SWAN configuration with the option Environment script: `/eos/project/l/lhcsm/public/packages_notebooks.sh`

## Code formatting

In this repository, the `pep8` code style is used.
To autoformat the code using pep8 styleguides, a combination of `yapf` and git-hooks is used. Basically, everytime you commit, the `yapf` command is being executed on the repo, applying pep8 style to `.py` files. To enable this behaviour, you need to install the `pre-commit` python package and install the precommit git hook:

```sh
$ pip install pre-commit
$ pre-commit install
```

Everytime you commit, your files will be formatted automatically by yapf.

### Note

Please note that due to how `pre-commit` works, when `yapf` formats your files, it will obviously edit them. `pre-commit` is designed such that if a git hook modifies a file while committing, that commit will fail. This is an expected behaviour and what you should do is to stage the new changes and commit again. This time yapf will not modify your files, since they are already formatted, and the commit will succeed. 
