## Playground
Contains various scripts and notebooks created to validate SIGMON tasks or debug issues.  
The files are grouped into sub-folders by their area of concern.
> :warning:️ Although it is worth looking at, the code here is not regularly updated; therefore, it may require some adjustments before use.