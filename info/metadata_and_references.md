# Metadata and references used in LHC-SM-API

### Authors: Per Hagen (2021)

## What is the purpose of the various simple .csv files

The main idea is to have parameters used by the various analysis stored in a few files, which hopefully make the project more manageable

The first idea was to use a database to store the configuration data as we feared processing .csv files would significantly impact performance.
In practice having .csv files embedded inside the Gitlab project has advantages
1. Easy to edit without database skills
2. Versioning inside a GitLab branch. Testing and review can be done on a copy without disturbing production version. Versioning is less obvious when using a global database (synch code and parameters)

## Data sources

Since we are basically processing signals produced in LHC, the metadata, references and criteria for accepting tests come mainly from other sources.  

The signal sources are:
* **PM FGC** : event data from electronics controlling PC power converters : FGC = function generator / controller 
* **PM QPS** : event data from electronics protecting and monitoring SC (superconducting) magnets : QPS = Quench Protection System
* Logging data from equipment (FGC, QPS, beam operation, ...): **CALS** until 2018. It was decommissioned in 2020. Historical data has been migrated. **NXCALS** from 2021.

There are two URLs to the PM data which takes an URL query string as input and returns PM events in **JSON** format (REST API concept). These are used by the Python code. In addition, there is a web browser GUI:
* http://pm-rest.cern.ch/v2/
* http://post-mortem-paas.web.cern.ch/ for web GUI

**Documentation for QPS** 
* https://wikis.cern.ch/display/MPEEP/Electronics+for+Protection+Home home of QPS
* https://wikis.cern.ch/display/MPEEP/DQLPU+type+S nQPS drawings for RB and RQ circuits
* https://wikis.cern.ch/display/MPEEP/Protection+Crates for the various circuit protection crates

**Documentation for FGC (PC)**
* https://accwww.cern.ch/proj-fgc/gendoc/def/SiteMap.htm FGC settings
* http://sy-dep-epc-lpc.web.cern.ch/converters.stm PC info

**Equipment databases** 
* https://cern.ch/service-acc-naming naming conventions for physical accelerator equipment 
* https://mtf.cern.ch MTF equipment database (like LHC SC magnets, but not LHC RF and some other subsystems)
* https://cern.ch/norma-db Equipment database for normal conducting magnets (LHC and other accelerators)
* http://edms.cern.ch/ Huge collection of engineering and scientific working papers. Meant to document implementations and changes to infrastructure. Used by MTF to store files. Decent free text search. Linked to from numerous internal web sites  
* https://edms-service.web.cern.ch/cdd/ CERN drawing directory. Mechanical drawings needed to produce various parts like magnets

**Layout database**
* https://layout.cern.ch/ aggregate abstraction of the accelerators based upon concept of functional positions which are mapped to physical objects as function of time. Logical equipment assemblies. Electrical circuits for magnets with warm cables and SC busbars. MAD-X optical sequences. 
 
**More about CERN accelerators, particle physics and physical layout**
* https://cds.cern.ch Scientific papers (and some other stuff) meant to be readable (by specialists) world-wide 
* https://cds.cern.ch/record/782076?ln=en LHC design report volume 1,2,3 (2004) which is a good starting point
* https://cds.cern.ch/record/1976692?ln=en LHC Injectors Upgrade, Technical Design Report as of 2015
* https://cds.cern.ch/record/2284929?ln=en LHC HiLumi upgrade as of 2017
* https://gis.cern.ch/ Geographic locations and names of accelerator facilities (surface buildings, underground alcoves)
* https://cern.ch/maps CERN and accelerator facilities on the surface in the local French/Swiss region
* https://cern.ch/mp3 Web site for the MP3 working group. Q&A of LHC SC magnets during HWC and operation. Recommendations for future operation.
 
**HWC - LHC Hardware Commissioning - SC magnets tests by MP3 working group (AccTesting related)**
* https://twiki.cern.ch/twiki/bin/view/MP3/HWCProceduresInfo test references to official EDMS documents
* https://twiki.cern.ch/twiki/bin/view/MP3/Analysis Internal test info to complement official EDMS documents
 
## Metadata

**Beam mode**

`lhcsmapi/metadata/beam_mode/beam_mode.csv`

```
Mode;Name;Description
1;NOMODE;No mode, data is not available, not set
```
This is the operational mode of the LHC accelerator. A complete working cycle of the LHC during operation would be something in this logical order _PRE-CYCLE, INJECTION, RAMP-UP, SQUEEZE, ADJUST, COLLISIONS FOR PHYSICS, DUMP-BEAM, RAMP-DOWN_. **Warning:** The keywords in .csv are slightly different     

The signal is called **HX:BMODE** in **NXCALS**, system **CMW** and is used for analysis during operation and HWC

**BLM - Beam Loss Monitors**

`lhcsmapi/metadata/blm/BLM_NXCALS.csv`

```
,Subscription Id,Device Name,PPM,Cycle Bound,Class Name,Accelerator,Property Name,Selector,Enabled,Timestamp Type,Field Name,Variable Name,Variable Unit,Variable Description,User filter,UF: Fixed rate,UF: Rounding type,UF: Rounding compare,Responsible,Status Time,Last Check Time,Status,Error Message
,143020,BLMMI.31L2.B2I14_V,FALSE,FALSE,BLM_MONITOR_V,LHC,Acquisition,,TRUE,ACQUISITION,LOSS_RS01,BLMMI.31L2.B2I14,Gy/s,Loss measurement for BLMMI.31L2.B2I14 with 40us integration time,FALSE,,,,Kamil Henryk Krol,2019-03-01T08:43:40.662+0000,2020-04-14T09:46:57.011+0000,VALIDATION_IN_ERROR,Failed to connect to server 'LHC_CONCENTRATION_BLM_BLMS_MEAS' : failed to connect to 'tcp://cs-ccr-concent.cern.ch:4198'
```

Configuration file used for data acquisition of the LHC BLMs. Radiation measured by beam loss monitors during beam presence.
The file name suggests data is located in **NXCALS**. Looking at the file contents the code using this data seems to be talking to the device abstraction in LHC (subscription service) rather than NXCALS. The usage and knowledge of this info most certainly would be in the section of Daniel WOLLMANN

**RB and RQ busbar details**

We measure the resistance in the busbars of the **RB** and **RQ** circuits. These are the most powerful circuits in LHC seen from a energy point of view. 13 kA. Many magnets in series. Several 1000 m of 2 K busbar segments per circuit.
The busbars are the SC cables which connect the magnets together. The smallest unit is called a segment and typically connects from one magnet to another. Busbar segments are electrically connected together with a splice. The splice is a result of soldering and has a small resistance.
The end points of the 2 K SC circuit is the DFB (Distribution Feed Box). 
Excessive R causes extra heat load into the cryogenics system. But low R is a sign that the splice is healthy to carry 13 kA. If the splice R is too high, a cryostat with a main magnet inside might have to be replaced.   

`lhcsmapi/metadata/busbar/nQPS_RB_busBarProtectionDetails.csv`

```
,QPS Crate&Board,Bus Bar Segment Name,1st Magnet,2nd Magnet,Num of splices,Dcum1,Dcum2,QPS Crate,QPS Board,Circuit,#Segment
0,B8L2_0,DCBB.8L2.R,DFLAS.7L2.5,MB.A8L2,4,3081.6104,3047.2854,B8L2,0,RB.A12,1
1,B8L2_1,DCBB.9L2.R,MB.A8L2,MB.A9L2,3,3047.2854,3008.2204,B8L2,1,RB.A12,2
```

`lhcsmapi/metadata/busbar/nQPS_RQ_busBarProtectionDetails.csv`

```
,QPS Crate&Board,Bus Bar Segment Name,1st Magnet,2nd Magnet,Num of splices,Dcum1,Dcum2,QPS Crate,QPS Board,Circuit,#Segment
0,B10L2_3,DCQDD.7L2.L,MQ.11L2.B2,DFLAS.7L2.2,16,2892.5107,3080.5904,B10L2,3,RQD.A12,1
1,B10L2_4,DCQFD.7L2.R,MQ.11L2.B1,DFLAS.7L2.4,16,2892.5107,3081.2704,B10L2,4,RQF.A12,1
2,B12L2_3,DCQDB.A12L2.L,MQ.13L2.B2,MQ.11L2.B2,8,2785.6107,2892.5107,B12L2,3,RQD.A12,2
```
Each row describes one busbar. Table sorted by increasing physical position in tunnel. Some of the columns are used for being able to query PM and NXCALS. See example.

* _Unnamed column:_ Row number 0 ... N-1. Internal ordering criterion
* _Crate&Board:_ QPS crate concatenated with underscore and board
* _Bus Bar Segment Name:_ Available from layout database, electrical connections
* _1st Magnet:_  and _2nd Magnet:_ The busbar connects 1st and 2nd physical object together (DFB or magnet). Available from layout database
* _Num of splices:_ The number of individual splices in the segment connected in series (divide busbar R with num of splices to get average splice R)  
* _Dcum1_ and _Dcum2:_ Cumulative positions of 1st and 2nd physical object. See layout database for more information. 
* _QPS Crate_ and _QPS Board:_ Derived from _Crate&Board_
* _Circuit:_ Name of magnet circuit
* _#Segment:_ Segment number in the circuit. The order magnets and segments are electrically connected (different from physical position).

I believe the files have been contributed to the project by Zinur and previously used in the LabView tools made by him.

```
    Example : Using RB_METADATA_RUN3.json to build a query
    
    "PM": {
      "system": "QPS",
      "className": "DQAMGNSRB_PMREL",
      "source": "%QPS_CRATE%",
      "U_MAG": "%BUSBAR%:U_MAG",
      "U_RES": "%BUSBAR%:U_RES",
      "ST_BUSBAR_OK": "%BUSBAR%:ST_BUSBAR_OK",
      "ST_PWR_PERM": "%BUSBAR%:ST_PWR_PERM"
    }, "NXCALS": {
      "system": "CMW",
      "U_MAG": "%BUSBAR%:U_MAG",
      "U_RES": "%BUSBAR%:U_RES"
    },

```
**Circuit abstraction and mapping to signals**

The superconducting magnets are divided into several classes. The classification is based upon the protection mechanisms and the nominal current and not the optical function they provide for the beams.

* 60, 80, 120 A: Low current magnets hosted inside cryostats of other main magnets. Self-protecting without QPS equipment so only FGC to power
* 600 A: Intermediate current level. Also hosted inside cryostats of other main magnets. QPS boards to detect magnet quench. Some circuits has several magnets in series and the stored energy requires energy extraction (EE)   
* IPD, IPQ: Dipole and quadrupole magnets in the insertions section. 2 to 5 kA. Protected by QPS and quench heaters (QH)
* IT : Innter triplet (triplet = 3 magnets in series for final focus). 13 kA circuit. Protected by QPS and QH.
* RB, RQ : Main dipole and quadrupoles. Protected by bypass diode, QPS, QH and EE. Extra nQPS for symmetric quenches, monitoring bypass diodes, voltage feelers

The internal naming scheme is <circuit type>_METADATA.json. It has been developed and tested for the _LHC Run2_. For _Run 3_ we found it natural to use new files by adding the suffix _RUN3. Probably we should have renamed the original ones to _RUN2_ but we did not. In _Run2_ logging data is stored in _CALS_. In _Run3_ we have _NXCALS_. Ideally we want to be able use data from the beginning of LHC (2007) to diagnose certain problems. It is hard to predict what one will need in the future. Migrating old data through software generations costs storage space and effort.

Let us start with the simplest circuit type which essentially is a magnet connected to a power converter (PC).

```
lhcsmapi/metadata/circuit/60A_METADATA_RUN3.json
lhcsmapi/metadata/circuit/80-120A_METADATA_RUN3.json

{
  "DESCRIPTION": "Orbit correctors for Horizontal and Vertical planes in the arcs.",
  "TIME-VALIDITY": {},
  "META_VARIABLES": [],
  "META_TABLES": [],
  "COMMENT": "Each circuit has a dictionary key which describes specific characteristics",
  "CIRCUITS": {}
  "CIP": {}
  "CRYO_1.9K": {}
  "CRYO_4.5K": {}
  "PC": {}
  "MAGNET": {}
}

```
* The file is just one big JSON container. So in the _METADATA_ files we are no longer representing data using tables (as a conventional database). The meaning of the various dictionary objects must be known by developer. We will try to explain details in the following. 

```
  "TIME-VALIDITY": {
    "COMMENT": "This version of 60A_METADATA is valid for Run3",
    "YEAR-START": 2019,
    "YEAR-END": 2024
  },
```

* The time validity of the data makes provides the versioning information. It is needed as all the systems we monitor evolve over time. The end year for Run3 might not be 2024. Time will tell. 

```
  "META_VARIABLES": [
    "%CIRCUIT%",
    "%CIP%",
    "%SECTORID%",
    "%PC%"
  ],
  "META_TABLES": [
    "magnet/60A-120A_LayoutDetails.csv",
    "magnet/60A-120A_MagnetHistory.csv"
  ],
```
* _META_VARIABLES_ enumerates the so-called wildcard variables used in this .json file. The wildcard variable is a placeholder for an actual value. Since it can occur anywhere inside a complex signal name, we use a % mark on each side of the variable
* _META_TABLES_ are the files which contain the actual variables used for wildcard substitution. But there is no info how to use the info inside the file! Using the meta tables all signal names can be generated.
```
  "CIRCUITS": {
    "RCBH11.L1B2": {},
    "RCBH11.L2B1": {},
    ...
  }
```
* _CIRCUITS_ enumerates all the circuits of this kind. It is like a container of circuit objects. 
* _RCBH11.L1B2_ is a circuit name at the same time a dict object. Inside the object we find the specific info 
```

    "RCBH11.L2B1": {
      "%CIRCUIT%": "RCBH11.L2B1",
      "CRYO": {
        "TYPE": "CRYO_1.9K",
        "%SECTORID%": "12"
      },
      "PC": {
        "TYPE": "PC",
        "%PC%": "RPLA.12L2.RCBH11.L2B1"
      }
    },
```
* "%CIRCUIT%": "RCBH11.L2B1". This declaration assigns a value to the variable %CIRCUIT%. I think of it like an instantiation of an object. Since the outer dict object also has the circuit name, this info is redundant.
* The CRYO and PC objects inside the circuit tells us that there is another object of type CRYO_1.9K, and a PC object. They should be instantiated with the variables %SECTORID% = "12" and %PC% = "RPLA.12L2.RCBH11.L2B1" respectively.   
* PC naming scheme: Example RPLA.12L2.RCBH11.L2B1 as listed in EPC database and layout: _RPLA_ is PC type. _12L2_ is the functional location of the PC. _RCBH11.L2B1_ is the circuit name from layout  
```
 "PC": {
    "Example": "%PC% -> RPLB.RR13.RCBCH10.L1B1",
    "PM": {
      "system": "FGC",
      "className": "lhc_self_pmd",
      "source": "%PC%",
      "I_REF": "STATUS.I_REF",
      "I_MEAS": "STATUS.I_MEAS",
      "V_REF": "STATUS.V_REF",
      "V_MEAS": "STATUS.V_MEAS",
      "I_EARTH": "IEARTH.I_EARTH",
      "I_EARTH_PCNT": "STATUS.I_EARTH_PCNT",
      "I_A": "IAB.I_A",
      "I_B": "IAB.I_B",
      "ACTION": "EVENTS.ACTION",
      "PROPERTY": "EVENTS.PROPERTY",
      "SYMBOL": "EVENTS.SYMBOL"
    },
    "NXCALS": {
      "system": "CMW",
      "device": "%PC%",
      "property": "SUB",
      "I_REF": "I_REF",
      "I_MEAS": "I_MEAS",
      "V_REF": "V_REF",
      "V_MEAS": "V_MEAS",
      "I_EARTH": "I_EARTH_MA",
      "I_EARTH_PCNT": "I_EARTH_PCNT"
    }
  }
```
* The _PC_ dict is at the bottom of the hierarchy and refers to signals and not to other objects
* The _PC_ dict contains one key for _PM_ and one for _NXCALS_
* There are general parameters needed for lookup. _system, className, source_ for _PM_. _system, device, property_ for _NXCALS_
* The same signal is often present in both _PM_ and _NXCALS_ because it is being continously measured. Example: _I_MEAS_
* The user of the metadata uses the same signal name _I_MEAS_ for _PM_ and _NXCALS_ even if some translation needed (_PM: STATUS.I_MEAS_)  

```
  "CRYO_1.9K": {
    "Example": "%SECTOR2% -> 81, %CRYOSTAT2% -> LQNCB_10L1, %TSENSOR% -> TT821",
    "PM": {
    },
    "NXCALS": {
      "system": "WINCCOA",
      "PTMIN_LINC": "ARC%SECTOR2%_LINC_PTMIN.POSST",
      "PTMIN_LIND": "ARC%SECTOR2%_LIND_PTMIN.POSST",
      "PTMAX_LINC": "ARC%SECTOR2%_LINC_PTMAX.POSST",
      "PTMAX_LIND": "ARC%SECTOR2%_LIND_PTMAX.POSST",
      "PTAVG_LINC": "ARC%SECTOR2%_LINC_PTAVG.POSST",
      "PTAVG_LIND": "ARC%SECTOR2%_LIND_PTAVG.POSST",
      "TTMIN": "ARC%SECTOR2%_MAGS_TTMIN.POSST",
      "TTMAX": "ARC%SECTOR2%_MAGS_TTMAX.POSST",
      "TTAVG": "ARC%SECTOR2%_MAGS_TTAVG.POSST",
      "TT8xx": "%CRYOSTAT2%_%TSENSOR%.TEMPERATURECALC"
    }
  }
```
* The _CRYO_1.9K dict is at the bottom of the hierarchy and refers to signals and not to other objects
* This cryogenics dict is to my knowledge not used so far in the project
* 1.9K is for the circuits operating at 1.9 K. Some circuits in the LSS operate at 4.5 K. But all 60 A magnets are in the arc where temperature is 1.9 K
* Variables starting with _PT_ is for the min and max pressure in arc. _TT_ are for the arc temperature
* _TT88x_ is for the individual cryostat temperature by using wildcards
```
  "CIP": {
    "Example": "%CIP% -> CIP.UL14.AL1",
    "PM": {
    },
    "NXCALS": {
      "system": "WINCCOA",
      "ST_CRYO_MAINTAIN": "%CIP%:ST_CRYO_MAINTAIN",
      "ST_CRYO_START": "%CIP%:ST_CRYO_START"
    }
  },
```
* The _CIP_ object is for the cryogenics powering permit (cryogenics interlock powering)
* _CRYO_START_ Cryogenic conditions for start of powering (correct operational temperature just reached)
* _CRYO_MAINTAIN_ Cryogenic conditions OK for powering (stable operational temperature after a few minutes with _CRYO_START_)
* This cryogenics dict is to my knowledge not used so far in the project, but sometimes we have _FPA_ due to failures in the cryo so this signal can be used to find the FPA reason
```
  "MAGNET": {
    "MAGNET_PREFIX": [
      "MCBH.",
      "MCBV."
    ]
```
* The _MAGNET_ dict contains some translation rules for processing. _MAGNET_PREFIX_ are the magnet types, plus a dot, described by this .json file
```
lhcsmapi/metadata/circuit/600A_METADATA_RUN3.json
```
```
  "PIC": {
    "Example": "%CIRCUIT% -> RCBXH1.L1",
    "PM": {
    },
    "NXCALS": {
      "system": "WINCCOA",
      "CMD_PWR_PERM_PIC": "%CIRCUIT%:CMD_PWR_PERM_PIC",
      "ST_ABORT_PIC": "%CIRCUIT%:ST_ABORT_PIC",
      "ST_FAILURE_PIC": "%CIRCUIT%:ST_FAILURE_PIC"
    }
  }
```
* The _PIC_ dict contains the signals for the powering interlock control. These signals are continuously monitored and logged in NXCALS.
  * _CMD_ABORT_PIC:_ FPA requested by PIC. Not used in our metadata
  * _CMD_PWR_PERM_PIC:_ Power permit command from PIC controller 
  * _ST_ABORT_PIC:_ QPS requests FPA. This is the signal we look for most of the time in HWC
  * _ST_FAILURE_PIC:_ FGC detects powering failure
```
  "QDS": {
    "Example": "%CIRCUIT% -> RCBXH1.L1",
    "PM": {
      "system": "QPS",
      "className": "DQAMGNA",
      "source": "%CIRCUIT%",
      "U_RES": "circ.%CIRCUIT%:U_RES",
      "U_DIFF": "circ.%CIRCUIT%:U_DIFF",
      "I_DCCT": "circ.%CIRCUIT%:I_DCCT",
      "I_DIDT": "circ.%CIRCUIT%:I_DIDT",
      "ST_CIRCUIT_OK_QPS": "stat.%CIRCUIT%:ST_CIRCUIT_OK_QPS",
      "ST_PWR_PERM": "stat.%CIRCUIT%:ST_PWR_PERM"
    },
    "NXCALS": {
      "system": "CMW",
      "U_RES": "%CIRCUIT%:U_RES",
      "U_DIFF": "%CIRCUIT%:U_DIFF",
      "I_DCCT": "%CIRCUIT%:I_DCCT",
      "I_DIDT": "%CIRCUIT%:I_DIDT",
      "ST_CIRCUIT_OK_QPS": "%CIRCUIT%:ST_CIRCUIT_OK_QPS",
      "ST_PWR_PERM": "%CIRCUIT%:ST_PWR_PERM"
    }
  }
```
* The _QDS_ dict contains the signals for the Quench Detection System for the magnet(s) in the circuit
  * _U_RES:_ Calculated resistive voltage across the magnet(s) assuming nominal inductance and the measured dI/dt
  * _U_DIFF:_ Voltage across the two leads on the circuit (refer to schematic for measurement points)
  * _I_DCCT:_ Current in circuit measured by QDS 
  * _I_DIDT:_ dI/dt in circuit measured by QDS
  * _ST_CIRCUIT_OK_QPS:_ Circuit health as seen by QPS: 1 = OK, 0 = Not OK
  * _ST_PWR_PERM:_ Power permit for the circuit
```
"LEADS": {
    "Example": "%DFB% -> DFLBS.3L1, %CIRCUIT% example -> RCBXH1.L1",
    "PM": {
      "system": "QPS",
      "className": "DQAMGNA",
      "source": "%CIRCUIT%",
      "U_RES": [
        "stat.%DFB%.%CIRCUIT%.LD1:U_RES",
        "stat.%DFB%.%CIRCUIT%.LD2:U_RES"
      ],
      "U_HTS": [
        "stat.%DFB%.%CIRCUIT%.LD1:U_HTS",
        "stat.%DFB%.%CIRCUIT%.LD2:U_HTS"
      ],
      "ST_LEAD_OK": [
        "stat.%DFB%.%CIRCUIT%.LD1:ST_LEAD_OK",
        "stat.%DFB%.%CIRCUIT%.LD2:ST_LEAD_OK"
      ],
      "ST_PWR_PERM": [
        "stat.%DFB%.%CIRCUIT%.LD1:ST_PWR_PERM",
        "stat.%DFB%.%CIRCUIT%.LD2:ST_PWR_PERM"
      ]
    }
```
* The _LEADS_ dict contains the signals for the QPS protection of the leads (LD1, LD2)
  * _U_RES:_ Voltage across the normal-conducting leads 
  * _U_HTS:_ Voltage across the HTS super-conducting leads 
```
  "EE": {
    "Example": "%EE% -> UA23.RCD.A12B1",
    "PM": {
      "system": "QPS",
      "className": "DQAMSN600",
      "source": "%EE%",
      "U_CAP_A": "circ.DQEMC.%EE%:U_CAP_A",
      "U_CAP_B": "circ.DQEMC.%EE%:U_CAP_B",
      "U_CAP_Z": "circ.DQEMC.%EE%:U_CAP_Z",
      "U_DUMP_RES": "circ.DQEMC.%EE%:U_DUMP_RES",
      "ST_SW_A_OPEN": "circ.DQEMC.%EE%:ST_SW_A_OPEN",
      "ST_SW_B_OPEN": "circ.DQEMC.%EE%:ST_SW_B_OPEN",
      "ST_SW_Z_OPEN": "circ.DQEMC.%EE%:ST_SW_Z_OPEN",
      "ST_TEMP_RES_HIGH": "circ.DQEMC.%EE%:ST_TEMP_RES_HIGH",
      "ST_SNUBB_FAIL": "circ.DQEMC.%EE%:ST_SNUBB_FAIL",
      "ST_SW_OPNFLR_0": "circ.DQEMC.%EE%:ST_SW_OPNFLR_0",
      "ST_SW_OPNFLR_1": "circ.DQEMC.%EE%:ST_SW_OPNFLR_1",
      "ST_SW_OP_WNG_0": "circ.DQEMC.%EE%:ST_SW_OP_WNG_0",
      "ST_SW_OP_WNG_1": "circ.DQEMC.%EE%:ST_SW_OP_WNG_1",
      "ST_CLOS_FAIL_0": "circ.DQEMC.%EE%:ST_CLOS_FAIL_0",
      "ST_CLOS_FAIL_1": "circ.DQEMC.%EE%:ST_CLOS_FAIL_1",
      "ST_SPUR_OPEN_0": "circ.DQEMC.%EE%:ST_SPUR_OPEN_0",
      "ST_SPUR_OPEN_1": "circ.DQEMC.%EE%:ST_SPUR_OPEN_1",
      "ST_MAINS_FAIL": "circ.DQEMC.%EE%:ST_MAINS_FAIL",
      "ST_REMOTE": "circ.DQEMC.%EE%:ST_REMOTE",
      "ST_TST_SOF_MODE_0": "circ.DQEMC.%EE%:ST_TST_SOF_MODE_0",
      "ST_TST_SOF_MODE_1": "circ.DQEMC.%EE%:ST_TST_SOF_MODE_1",
      "ST_FPA_REC_0": "circ.DQEMC.%EE%:ST_FPA_REC_0",
      "ST_FPA_REC_1": "circ.DQEMC.%EE%:ST_FPA_REC_1",
      "ST_VOLT_RES_HIGH": "circ.DQEMC.%EE%:ST_VOLT_RES_HIGH",
      "ST_UCAP_A_LOW": "circ.DQEMC.%EE%:ST_UCAP_A_LOW",
      "ST_UCAP_B_LOW": "circ.DQEMC.%EE%:ST_UCAP_B_LOW",
      "ST_UCAP_Z_LOW": "circ.DQEMC.%EE%:ST_UCAP_Z_LOW",
      "ST_SYST_ST_CLOSED_0": "circ.DQEMC.%EE%:ST_SYST_ST_CLOSED_0",
      "ST_SYST_ST_CLOSED_1": "circ.DQEMC.%EE%:ST_SYST_ST_CLOSED_1",
      "ST_SYST_FAIL_0": "circ.DQEMC.%EE%:ST_SYST_FAIL_0",
      "ST_SYST_FAIL_1": "circ.DQEMC.%EE%:ST_SYST_FAIL_1",
      "ST_SUM_FAULT_FAIL_0": "circ.DQEMC.%EE%:ST_SUM_FAULT_FAIL_0",
      "ST_SUM_FAULT_FAIL_1": "circ.DQEMC.%EE%:ST_SUM_FAULT_FAIL_1",
      "ST_TEMP_EQRES_HIGH": "circ.DQEMC.%EE%:ST_TEMP_EQRES_HIGH"
    }
  }
```
* The _EE_ dict contains the signals for the Energy Extraction system
  * _U_CAP_A, B, Z:_ Voltage across the capacitors in the switches
  * _U_DUMP_RES:_ Voltage across the dump R
  * _ST_TEMP_RES_HIGH:_ Temperature high. EE must cool down before we can allow powering again
  * _ST...:_ Numerous micro switches and timing signals

```
lhcsmapi/metadata/circuit/IPD2_METADATA_RUN3.json
lhcsmapi/metadata/circuit/IPD2_B1B2_METADATA_RUN3.json
```
* The IPD circuits are described in two separate .json files because the signal names differ slightly, and are stored in two different QPS classes. _IPD2_ describes the SC D1 with 2 quench heaters. It is using the same QPS class as the IPQ with 2 quench heaters. _IPD2_B1B2_ describe the D2, D3 and D4 magnets. 
```
  "QH": {
    "Comment": "%CIRCUIT% example -> RD1.L1",
    "PM": {
      "system": "QPS",
      "className": "DQAMGNRD",
      "source": "%CIRCUIT%",
      "U_HDS": [
        "heat.%CIRCUIT%:U_HDS_1",
        "heat.%CIRCUIT%:U_HDS_2"
      ]
    },
    "NXCALS": {
      "system": "CMW",
      "U_HDS": [
        "%CIRCUIT%:U_HDS_1",
        "%CIRCUIT%:U_HDS_2"
      ]
    }
  }

  "QH": {
    "Comment": "%CIRCUIT% example -> RD2.L1",
    "PM": {
      "system": "QPS",
      "className": "DQAMGNRQA",
      "source": "%CIRCUIT%",
      "U_HDS": [
        "heat.%CIRCUIT%:U_HDS_1_B1",
        "heat.%CIRCUIT%:U_HDS_1_B2"
      ]
    },
    "NXCALS": {
      "system": "CMW",
      "U_HDS": [
        "%CIRCUIT%:U_HDS_1_B1",
        "%CIRCUIT%:U_HDS_1_B2"
      ]
    }
  }
```
```
  "LEADS": {
    "Comment": "%DFB% example -> DFLCS.3L2, %CIRCUIT% example -> RD1.L2",
    "PM": {
      "system": "QPS",
      "className": "DQAMGNRD",
      "source": "%CIRCUIT%",
      "U_RES": [
        "stat.%DFB%.%CIRCUIT%.LD1:U_RES",
        "stat.%DFB%.%CIRCUIT%.LD2:U_RES"
      ],
      "U_HTS": [
        "stat.%DFB%.%CIRCUIT%.LD1:U_HTS",
        "stat.%DFB%.%CIRCUIT%.LD2:U_HTS"
      ]
    },
    "NXCALS": {
      "system": "CMW",
      "U_RES": [
        "%DFB%.%CIRCUIT%.LD1:U_RES",
        "%DFB%.%CIRCUIT%.LD2:U_RES"
      ],
      "U_HTS": [
        "%DFB%.%CIRCUIT%.LD1:U_HTS",
        "%DFB%.%CIRCUIT%.LD2:U_HTS"
      ]
    }
  },
  "LEADS_NXCALS_WINCCOA": {
    "Comment": "%DFB% example -> DFLCS.3L2, %CIRCUIT% example -> RD2.L1",
    "NXCALS": {
      "system": "WINCCOA",
      "TT893": "%DFB_PREFIX%TT893.TEMPERATURECALC",
      "TT891A": "%DFB_PREFIX%TT891A.TEMPERATURECALC",
      "CV891": "%DFB_PREFIX%CV891.POSST"
    }
  }
```
* The _LEADS_ dict contains the signals for monitoring the resistive and HTS leads. The resistive leads carry the current close to the PC, and thereafter through the HTS (High temperature SC) leads before reaching the DFB with the low temperature SC busbars and magnets.
* _LD1, LD2 ...:_  lead1, lead2 ...
* _U_RES:_ Resistive voltage across the lead
* _U_HTS:_ Resistive voltage across the HTS lead
* The extra dict object _LEADS_NXCALS_WINCCOA_ has been introduced since the signals for some temperature sensors reside in a different part of _NXCALS_: _WINCCOA_ 
* _TT893_ is the temperature (C) in the resistive leads connected to the PC
* _TT891A_ is the temperature (C) in the HTS intermediate leads 
* _CV891_ is the opening in (%) of the cryogenics valve regulating the HTS temperature 
```
lhcsmapi/metadata/circuit/IPQ2_METADATA_RUN3.json
lhcsmapi/metadata/circuit/IPQ4_METADATA_RUN3.json
lhcsmapi/metadata/circuit/IPQ8_METADATA_RUN3.json
```
* The _IPQ2, IPQ4_ and _IPQ8_ files differ in the number of QH (quench heaters) and therefore the QPS class where PM data is stored. 
```
  "QH": {
    "Comment": "%CIRCUIT% example -> RQ5.L1",
    "PM": {
      "system": "QPS",
      "className": "DQAMGNRQA",
      "source": "%CIRCUIT%",
      "U_HDS": [
        "heat.%CIRCUIT%:U_HDS_1_B1",
        "heat.%CIRCUIT%:U_HDS_1_B2"
      ]
    },
    "NXCALS": {
      "system": "CMW",
      "U_HDS": [
        "%CIRCUIT%:U_HDS_1_B1",
        "%CIRCUIT%:U_HDS_1_B2"
      ]
    }
  }
```
* The _IPQ2_ file contains the circuits with one magnet from the _MQM_ family. The family consists of 3 magnet types which only differ in length: _MQMC_ - short, _MQM_ - normal, _MQML_ - long. The cryostats with _MQM_ magnet family contain one or two main _MQM_ magnets. Essentially this allows different variants which only differ in magnetic length as needed by the optics.   
* Each _MQM_ magnet has 2 QH.     

```
  "QH": {
    "Comment": "%CIRCUIT% example -> RQ4.L1",
    "PM": {
      "system": "QPS",
      "className": "DQAMGNRQB",
      "source": "%CIRCUIT%",
      "U_HDS": [
        "heat.%CIRCUIT%:U_HDS_1_B1",
        "heat.%CIRCUIT%:U_HDS_2_B1",
        "heat.%CIRCUIT%:U_HDS_1_B2",
        "heat.%CIRCUIT%:U_HDS_2_B2"
      ]
    },
    "NXCALS": {
      "system": "CMW",
      "U_HDS": [
        "%CIRCUIT%:U_HDS_1_B1",
        "%CIRCUIT%:U_HDS_2_B1",
        "%CIRCUIT%:U_HDS_1_B2",
        "%CIRCUIT%:U_HDS_2_B2"
      ]
    }
  }
```
* The _IPQ4_ file contains the circuits with two magnets from the _MQM_ family.    
* Each of the _MQM_ magnets has 2 QH so 2 magnets in the same cryostat requires 4 QH      
* In addition, we have a few _MQY_ magnets which has 4 QH in a single magnet due to the optimised design with 4 layers of SC cables       
```
  "QH": {
    "Comment": "%CIRCUIT% example -> RQ5.L2",
    "PM": {
      "system": "QPS",
      "className": "DQAMGNRQC",
      "source": "%CIRCUIT%",
      "U_HDS": [
        "heat.%CIRCUIT%:U_HDS_1_B1",
        "heat.%CIRCUIT%:U_HDS_2_B1",
        "heat.%CIRCUIT%:U_HDS_3_B1",
        "heat.%CIRCUIT%:U_HDS_4_B1",
        "heat.%CIRCUIT%:U_HDS_1_B2",
        "heat.%CIRCUIT%:U_HDS_2_B2",
        "heat.%CIRCUIT%:U_HDS_3_B2",
        "heat.%CIRCUIT%:U_HDS_4_B2"
      ]
    },
    "NXCALS": {
      "system":"CMW",
      "U_HDS": [
        "%CIRCUIT%:U_HDS_1_B1",
        "%CIRCUIT%:U_HDS_2_B1",
        "%CIRCUIT%:U_HDS_3_B1",
        "%CIRCUIT%:U_HDS_4_B1",
        "%CIRCUIT%:U_HDS_1_B2",
        "%CIRCUIT%:U_HDS_2_B2",
        "%CIRCUIT%:U_HDS_3_B2",
        "%CIRCUIT%:U_HDS_4_B2"
      ]
    }
  }
```
* The _IPQ8_ file contains the circuits with two _MQY_ magnets connected in series
* The _MQY_ operate at 4.5 K where the _MQM_ magnets would be less efficient
```
lhcsmapi/metadata/circuit/IT_METADATA_RUN3.json
```
* The _IT_ file contains the inner triplet circuits used for colliding the beams in all of the 4 main experiments in LHC (Atlas in IP1, Alice in IP2, CMS in IP5, LHCb in IP8)
```
  "CIRCUITS": {
    "RQX.L1": {
      "%CIRCUIT%": "RQX.L1",
      "%SECTORID%": "81",
      "CIP": {},
      "CRYO": {},
      "PIC": {},
      "PC": {
        "TYPE": "PC",
        "%PC%": [
          "RPHFC.UL14.RQX.L1",
          "RPMBC.UL14.RTQX1.L1",
          "RPHGC.UL14.RTQX2.L1"
        ]
      },
      "QDS": {},
      "QH": {},
      "LEADS": {
        "TYPE": "LEADS",
        "%DFB%": "DFLX.3L1",
        "ORDER": [
          "LD1",
          "LD2",
          "LD3",
          "LD4"
        ]
```
* Each _IT_ circuit consists of 3 optical elements (Q1, Q2, Q3). Each one in a separate cryostat. Q1 is closest to the IP. All 3 has the same optical strength. Q1 and Q3 are _MQXA_ magnets. Q2 consist of 2 _MQXB_ magnets in series, although it appears as one logical quadrupole as seen by optics. 
* The _PC_ dict contains 3 PCs. The current from _RQX_ flows through all the main magnets. The _MQXB_ magnets have a different design and requires a lot more current to provide the same magnetic strength. The _RTQX2_ is a second power supply for the _MQXB_ magnets. Connected in parallel. The name suggests it is a "trim" PC which it is certainly not! It carries several kA of current. Finally, the _Q1_ has a small 600 A trim power supply _RTQX1_ connected in parallel.
* The _LEADS_ dict contains 4 leads. Two extra leads are needed for the extra power supplies.
```
  "QDS": {
    "Example": "%CIRCUIT% -> RQX.L1",
    "PM": {
      "system": "QPS",
      "className": "DQAMGNC",
      "source": "%CIRCUIT%",
      "U_1_Q1": "circ.%CIRCUIT%:U_1_Q1",
      "U_2_Q1": "circ.%CIRCUIT%:U_2_Q1",
      "U_1_Q2": "circ.%CIRCUIT%:U_1_Q2",
      "U_2_Q2": "circ.%CIRCUIT%:U_2_Q2",
      "U_1_Q3": "circ.%CIRCUIT%:U_1_Q3",
      "U_2_Q3": "circ.%CIRCUIT%:U_2_Q3",
      "U_RES_Q1": "circ.%CIRCUIT%:U_RES_Q1",
      "U_RES_Q2": "circ.%CIRCUIT%:U_RES_Q2",
      "U_RES_Q3": "circ.%CIRCUIT%:U_RES_Q3",
      "ST_CIRCUIT_OK_QPS": "stat.%CIRCUIT%:ST_CIRCUIT_OK_QPS",
      "ST_PWR_PERM": "stat.%CIRCUIT%:ST_PWR_PERM"
    },
    "NXCALS": {
    same kind of signals
    }
```
* The _QDS_ dict is divided into separate signal for Q1, Q2 and Q3.
* _U_1_Qx:_ Voltage across first two poles
* _U_2_Qx:_ Voltage across last two poles. The difference between the U_1 and U_2 allows for quench detection
* _U_RES_Qx:_ Calculated resistive voltage across the magnet(s) assuming nominal inductance and the measured dI/dt
```
  "QH": {
    "Example": "%CIRCUIT% -> RQX.L1",
    "PM": {
      "system": "QPS",
      "className": "DQAMGNC",
      "source": "%CIRCUIT%",
	  "U_HDS": [
        "heat.%CIRCUIT%:U_HDS_1_Q1",
        "heat.%CIRCUIT%:U_HDS_2_Q1",
        "heat.%CIRCUIT%:U_HDS_1_Q2",
        "heat.%CIRCUIT%:U_HDS_2_Q2",
        "heat.%CIRCUIT%:U_HDS_3_Q2",
        "heat.%CIRCUIT%:U_HDS_4_Q2",
        "heat.%CIRCUIT%:U_HDS_1_Q3",
        "heat.%CIRCUIT%:U_HDS_2_Q3"
      ]
    },
    "NXCALS": {
    same kind of signals
    }
  }
```
* The _QH_ dict is divided into separate signal for Q1, Q2 and Q3.
* The optical _Q2_ consists of 2 magnets each with 2 QH so _U_HDS_1_Q2_ to _U_HDS_4_Q2_
```
lhcsmapi/metadata/circuit/RB_METADATA_RUN3.json
```
```
  "RB.A12": {
    "%CIRCUIT%": "RB.A12",
    "CIP": {
      "TYPE": "CIP",
      "%CIP%": [
        "CIP.UL16.AR1",
        "CIP.UA23.AL2"
      ]
    },
```
* The _CIP_ dict shows that the circuit is divided into two cryogenic sections, _Rx_ and _Lx+1_. The signals are like for the other type of circuits, _CRYO_START_ and _CRYO_MAINTAIN_ when powering is allowed
* The _CRYO_ dict is similar to the other circuits
```
    "PIC": {
      "TYPE": "PIC",
      "%PIC%": [
        "RB.A12.ODD",
        "RB.A12.EVEN"
      ]
    }
```
* The _PIC_ dict shows that the circuit is divided into two powering interlock sections, _ODD_ and _EVEN_. The signals are like for the other type of circuits, by adding _ODD_ and _EVEN_ to the name. The main reason for having 2 _PIC_ subsectors is to control the opening of the _ODD_ and _EVEN_ _EE_ switches at two different time intervals.  
* There is one _PC_ for the _ODD_ and _EVEN_ subsectors with 154 dipoles in series.    
```
```
 
_TO BE DONE_

**Earth current**

```
lhcsmapi/metadata/earth_current/IPQ_EarthCurrent.csv

Circuit name,Power converter with earth measurement
RQ4.L1,B1
```
In the IPQ circuits the earth current is measured by one out of two circuits (B1 or B2). This table is used for the lookup. Source: https://edms.cern.ch/ui/file/2359699/LATEST/R2E-LHC-4-6-8kA-08V_LHC-MQM-Converter-Arrangement.pdf 

**Magnet layout and history**

```
lhcsmapi/metadata/magnet/60A-120A_LayoutDetails.csv

Circuit,Magnet,Name,Position,Beam,Aperture,Toperation,Tsensor,Cryostat,Cryostat2,Inactive
RCBCH10.L1B1,,MCBCH.10L1.B1,26278.0442,B1,INT,1.9,TT821,LQNCB.10L1,LQNCB_10L1,
```
* _Circuit:_ Name of magnet circuit from layout database
* _Magnet:_ Unused column (for this magnet class)
* _Name:_ Functional name from layout database 
* _Position:_ Distance cumulated from the LHC layout database (general ordering criterion)    
* _Beam:_ _B1_ = Beam 1 (clockwise seen from sky), _B2_ = Beam 2 (anticlockwise seen from sky), _ONE_ magnet with only one aperture for both beams
* _Aperture:_ The aperture of the _Beam_. Each beam travels 4 sectors in _INT_ (internal = inner) aperture, and 4 sectors in _EXT_ (external = outer) aperture. The beams change aperture (cross over) in the IP region of the 4 experiments
* _Toperation:_ The nominal operational temperature
* _Tsensor:_ The type of the temperature sensor needed for wildcard signal name
* _Cryostat:_ The cryostat name from the layout database
* _Cryostat2:_ Derived cryostat name needed for wildcard signal name
* _Inactive:_ Unused column originally meant to flag if circuit is actively used or has technical problems (not used before repaired in next long shutdown)

```
  "CRYO_1.9K": {
    "Example": "%SECTOR2% -> 81, %CRYOSTAT2% -> LQNCB_10L1, %TSENSOR% -> TT821",
    "PM": {
    },
    "NXCALS": {
      "system": "WINCCOA",
      "TT8xx": "%CRYOSTAT2%_%TSENSOR%.TEMPERATURECALC"
    }
  }
```

```
lhcsmapi/metadata/magnet/600A_LayoutDetails.csv

Circuit,Magnet,Name,Position,Beam,Aperture,Toperation,Tsensor,Cryostat,Cryostat2,Inactive
RCBXH1.L1,,MCBXH.1L1,26629.0412,"B1,B2",ONE,1.9,TT821A,LQXAB.1L1,LQXAB_01L1,
```

```
lhcsmapi/metadata/magnet/IPD_LayoutDetails.csv

Circuit,Circuit2,Magnet,Name,Position,Beam,Aperture,QPS_class,Toperation,Tsensor,Cryostat,Cryostat2
RD2.R1,RD2.R1,MBRC.4R1,MBRC.4R1.B1,157.9,B1,EXT,DQAMGNRQA,4.5,TT831,LBRCD.4R1,LBRCD_04R1
```
* _Circuit2:_ Derived name from _Circuit_. Identical to _Circuit_ Not needed for _IPD_. Only needed for _IPQ_ 
* _QPS_class:_ Name QPS class in PM (_DQAMGNRQA_ mostly used for IPQs or _DQAMGNRD_). The two classes has slightly different magnet architecture and signal names. This column is no longer needed since the metadata for _IPD_ is split into two files  

```
lhcsmapi/metadata/magnet/IPQ_LayoutDetails.csv

Circuit,Circuit2,Magnet,Name,Position,Beam,Aperture,QPS_class,Toperation,Tsensor,Cryostat,Cryostat2
RQ4.R1B1,RQ4.R1,MQY.4R1,MQY.4R1.B1,169.553,B1,EXT,DQAMGNRQB,4.5,TT830A,LQYCH.4R1,LQYCH_04R1
```
* _Circuit2:_ Derived name from _Circuit_. _Circuit_ is always the circuit as seen from the PC point of view. _Circuit2_ is the circuit seen from a _QPS_ point of view (protection). 2 apertures are protected with one QPS class
* _QPS_class:_ Name QPS class in PM.  (_DQAMGNRQA_, _DQAMGNRQB_, _DQAMGNRQC_ depending upon number of QH in cryostat). This column is no longer needed since the metadata for _IPQ_ is split into three files 

```
lhcsmapi/metadata/magnet/IPQ_SchematicNaming.csv

circuit_name,schematic_name
RQ7.L4,2MQM
```
* _Circuit:_ Name of magnet circuit from layout database
* _Schematic_name:_ Name of circuit schematics to display in Notebook 

```
lhcsmapi/metadata/magnet/IT_LayoutDetails.csv

Circuit,Magnet,Position,OpticsId,PC,PC2,Cryostat,Cryostat2,Cryostat3
RQX.R1,MQXA.1R1,26.15,Q1,RPHFC.UL16.RQX.R1,RPMBC.UL16.RTQX1.R1,LQXAA.1R1,LQXAA_01R1,
```

* _Circuit:_ Name of magnet circuit from layout database. Circuit has 2 more PCs. One because there are two magnet families with quite different current. One trim PC for fine adjustment.
* _OpticsId:_ _Q1_ (closest to IP), _Q2_ (middle), _Q3_ (away from IP). Inner triplet magnets serving as one optical function (final focus). Quench protection is for Q1, Q2, Q3 individually   
* _Cryostat:_ Name of cryostat from layout database
* _Cryostat2_, _Cryostat3:_ Derived cryostat names needed for wildcard signal name

```
lhcsmapi/metadata/magnet/RB_LayoutDetails.csv

Circuit,Magnet,Position,B1_aperture,Diode_type,Correctors,EE place,#Electric_EE,#Electric_circuit,Cryostat,Cryostat2
RB.A12,MB.A8R1,276.734,EXT,R,A,EVEN,1,78,LBARE.8R1,LBARE_08R1
```
* _B1_aperture:_ _EXT_ means _B1_ in external aperture. _INT_ means internal aperture 
* _Diode_type:_ Functional position requires diode orientation _L_ (left) or _R_ (right). Given by current direction due to beam location. All MBs in sector has same diode type. 
* _Correctors:_ Functional position requires corrector type _A_ or _B_. _A_ means correctors _MCS_, _MCD_ and _MCO_. _B_ means _MCS_ only
* _EE place:_ Which branch of the circuit the magnet is part of. The current flows from the plus pole through magnet 1, 3, 5 .. 153 which is called the _ODD_ part. It ends in the _ODD EE_. The current then return through even magnets 2, 4, 6 ... 154 which is the _EVEN_ part with a second _EE_ and a connection to the neagtive pole on the PC.    
* _#Electric_EE:_ Electric order within the _EE_ ( 1 ... 77)
* _#Electric_circuit:_ Electric order within the circuit (1 ... 154)

```
lhcsmapi/metadata/magnet/RQ_LayoutDetails.csv

Circuit,Magnet,Position,Beam,Aperture,#Electric_circuit,Cryostat,Cryostat2
RQD.A12,MQ.11R1,435.5557,B2,INT,24,LQTCE.11R1,LQTCE_11R1
lhcsmapi/metadata/magnet/RQ_LayoutDetails.csv
```

```
lhcsmapi/metadata/magnet/60A-120A_MagnetHistory.csv

Magnet,Position,Period->,Initial,Incident,Run1,LS1,Run2,LS2,Run3
MCBCH.10L1.B1,26278.0442,,TE000056,,TE000056,,TE000056,,TE000056

lhcsmapi/metadata/magnet/600A_MagnetHistory.csv
lhcsmapi/metadata/magnet/IPD_MagnetHistory.csv
lhcsmapi/metadata/magnet/IPQ_MagnetHistory.csv
lhcsmapi/metadata/magnet/IPQ_MagnetHistory.csv
lhcsmapi/metadata/magnet/RB_MagnetHistory.csv
lhcsmapi/metadata/magnet/RQ_MagnetHistory.csv

```
* _Magnet:_ Functional magnet from layout database
* _Position:_ Distance cumulated from layout database  
* _Period:_ Period is based upon an abstraction. _Initial_ means 1st HWC in 2008. _Incident_ is the big accident in 2008 followed by repair until autumn 2009. _RunX_ is the operational period of LHC beams. _LSX_ is the long shutdown to fix and upgrade the machine. If a magnet was changed during the maintenance periods, 1 is put into this column. The manufacturer code is used from MTF. The exceptions being (for practical reasons) the RB where we use the 4 digits short magnet names and the RQ where we use 3 digits SSS assembly numbers. Everything derived from MTF. 

The history files are foreseen to help to track magnets in the tunnel if we wanted to so inside Notebooks. It has not been used in any code so far according to my knowledge. The Excel quench files and the MP3 website are so far used by MP3 for manual tracking. They often refer to documents in EDMS or MTF (non-conformity) 

```
lhcsmapi/metadata/magnet/OperationPeriods.csv

,name,start_date,end_date
Initial,2008-01-01 00:00:00.000+01:00,2009-04-30 00:00:00.000+01:00
```
* _Name:_ Period as for the history files. This file is most likely not used and has been replaced with a more accurate description thanks to Arjan VERWEIJ: lhcsmapi/resources/reference/mp3/HWC_Operation_YETS_Periods.csv

**nQPS crate - diode and earth signals**

`lhcsmapi/metadata/qps_crate/RB_CrateToMagnetAndVoltageFeelerMap.csv`

```
Circuit,Crate,Magnet1,Magnet2,Magnet3,Magnet4,U_EARTH_RB,#VF_circuit
RB.A12,B8R1,MB.A8R1,,,U_REF_N1,MB.A8R1,28
```
Each row describes one nQPS crate. Each crate can contain max 4 diode signals and 1 earth signal. Sometimes there are fewer signals depending on the crate location. Table sorted by increasing physical crate position in tunnel.

* _Circuit:_ Name of magnet circuit
* _Crate:_ QPS crate
* _Magnet1 ... Magnet4:_ Name of the magnets which provide the _U_DIODE_RB_ signal in the crate. Typically 3 nearby magnets and _U_REF_N1_ from an adjacent crate. Diode signals are used for comparison in the nQPS algorithm to detect symmetric quench. That is: quench in both magnet apertures. Since we have two boards (A/B) which are connected to slightly different measurement points, the diode lead R can also be calculated from these signals 
* _U_EARTH_RB:_ Name of magnet which provides the _U_EARTH_RB_ signal. This signal is called voltage feeler (VF). It measures the voltage between a point on the magnet and ground. 1 VF per crate. 54 crates per circuit so 54 VF for 154 magnets.   
* _#VF_circuit:_ The order VFs are electrically connected (different from physical position in tunnel).

I believe this file has been contributed to the project by Zinur and previously used in the LabView tools made by him.

An alternative way to provide this information would be to have one _Magnet_ column rather than 4 and repeat the crate as many times as needed

The metadata contains other similar tables

```
  Example : Using RB_METADATA_RUN3.json to build a query


  "DIODE_RB": {
    "Comment": "%QPS_CRATE% example -> B31R1, %MAGNET% example -> MB.A31R1, MB.B30R1, MB.A31R1",
    "PM": {
      "system": "QPS",
      "className": "DQAMGNSRB_PMREL",
      "source": "%QPS_CRATE%",
      "U_DIODE_RB": "%MAGNET%:U_DIODE_RB",
      "U_REF_N1": "DQQDS.%QPS_CRATE%.%CIRCUIT%:U_REF_N1",
    },
    "NXCALS": {
      "system": "CMW",
      "U_DIODE_RB": "%MAGNET%:U_DIODE_RB",
      "U_REF_N1": "DQQDS.%QPS_CRATE%.%CIRCUIT%:U_REF_N1",
    }
  },
  "VF": {
    "Comment": "%QPS_CRATE% example -> B8L2, %MAGNET% example -> MB.A8R1",
    "PM": {
      "system": "QPS",
      "className": "DQAMGNSRB_PMREL",
      "source": "%QPS_CRATE%",
      "U_EARTH_RB": "%MAGNET%:U_EARTH_RB"
    },
    "NXCALS": {
      "system": "CMW",
      "U_EARTH_RB": "%MAGNET%:U_EARTH_RB"
    }
  }

```

```
lhcsmapi/metadata/qps_crate/RB_MagnetToCrateAndVoltageFeelerMap.csv
```

```
Circuit,Crate,Magnet1,Magnet2,Magnet3,Magnet4,U_EARTH_RB,#VF_circuit
RB.A12,B8R1,,MB.A8R1,,,A8R1,28
```

This table seems to be identical to _RB_CrateToMagnetAndVoltageFeelerMap.csv_

```
lhcsmapi/metadata/qps_crate/RB_CrateToDiodeMap.csv
```

```
Circuit,Crate,U_DIODE_RB,#Diode_circuit
RB.A12,B8L2,MB.A8L2,1
```
Each row describes one magnet. Table sorted by increasing physical position in tunnel.  

* _Circuit:_ Name of magnet circuit
* _Crate:_ QPS crate
* _U_DIODE_RB:_ Name of magnet which provides the _U_DIODE_RB_ signal. This signal measures the voltage across the bypass diode. 1 diode per magnet. 154 magnets per circuit. 2 boards (A/B) measures the same signal, but connected slightly different such that the diode lead voltage drop is the difference between the two     
* _#Diode_circuit:_ The order magnets are electrically connected (different from physical position in tunnel).

```
lhcsmapi/metadata/magnet/RB_MagnetCellQpscrateSector.csv
```

```
,Magnet,Cell,QPS Crate,Circuit
0,MB.A8R1,A8R1,B8R1,RB.A12
```
Each row describes one magnet. Table sorted by physical position. Since there 154 rows per circuit, we can assume this is a table for _U_DIODE_RB_ signals! Table sorted by increasing physical position in tunnel.  

```
lhcsmapi/metadata/magnet/RQ_MagnetCellQpscrateSector.csv
```

```
,Circuit,Magnet,Cell,#Diode_ circuit,Crate U_DIODE_RQx,Crate U_REF_N1,Crate U_EARTH_RQx
0,RQD.A12,MQ.12L2,12L2,1,B13L2,,B11L2
```
Each row describes one magnet. Table sorted by physical position. There is a mix of circuits with 47 or 51 magnets because the cleaning sections use MQs far into the LSS. 

* _Unnamed column:_ Row number 0 ... N-1. Internal ordering criterion
* _Circuit:_ Name of magnet circuit. Two families of RQ circuits according to optical function. RQD for defocusing and RQF for focusing magnets.  
* _Magnet:_ Magnet name as described in layout database.
* _Cell:_ Cell number followed by L (left) or R (right) of IP n. See layout database for functional naming. IP is "interaction point" 1 to 8. 4 for experiments. 1 RF. 1 beam dump. 2 for beam cleaning. Cell can easily be derived from magnet name for the MQs because there is always one MQ per cell.   
* _#Diode_circuit:_ The order diodes are electrically connected (different from physical position in tunnel).      
* _Crate U_DIODE_RQx:_ The QPS crate where the signal _U_DIODE_RQD_ or _U_DIODE_RQF_ is stored. See examples.
* _Crate U_REF_N1:_ The QPS crate where the signal _U_REF_N1_ is equivalent to the _U_DIODE_RQx_ signal. This happens in the middle of the arc where one crate controls 2 magnets and the _U_DIODE_RQx_ signal is missing for one of them.
* _Crate U_EARTH_RQx:_ The QPS crate where the signal _U_EARTH_RQD_ or _U_EARTH_RQF_ is stored. See examples.

```
  Examples : Using RQ_METADATA_RUN3.json to build a query

  "DIODE_RQD": {
    "Comment": "%QPS_CRATE% example -> B13L2, %MAGNET% example -> MQ.12L2",
    "PM": {
      "system": "QPS",
      "className": "DQAMGNSRQD_PMREL",
      "source": "%QPS_CRATE%",
      "U_DIODE_RQD": "%MAGNET%:U_DIODE_RQD",
      "U_REF_N1": "DQQDS.%QPS_CRATE%.%CIRCUIT%:U_REF_N1",
    },
    "NXCALS": {
      "system": "CMW",
      "U_DIODE_RQD": "%MAGNET%:U_DIODE_RQD",
      "U_REF_N1": "DQQDS.%QPS_CRATE%.%CIRCUIT%:U_REF_N1",
    }
  },
  "DIODE_RQF": {
    "Comment": "%QPS_CRATE% example -> B13L2, %MAGNET% example -> MQ.12L2",
    "PM": {
      "system": "QPS",
      "className": "DQAMGNSRQF_PMREL",
      "source": "%QPS_CRATE%",
      "U_DIODE_RQF": "%MAGNET%:U_DIODE_RQF",
      "U_REF_N1": "DQQDS.%QPS_CRATE%.%CIRCUIT%:U_REF_N1",
    },
    "NXCALS": {
      "system": "CMW",
      "U_DIODE_RQF": "%MAGNET%:U_DIODE_RQF",
      "U_REF_N1": "DQQDS.%QPS_CRATE%.%CIRCUIT%:U_REF_N1",
    }
  }
```

```
lhcsmapi/metadata/qps_crate/RQ_CrateToDiodeMap.csv
```
 
```
Circuit,Crate,U_DIODE_RQx,U_REF_N1,#Diode_circuit
RQD.A12,B11L2,,,
RQD.A12,B13L2,MQ.12L2,MQ.14L2,1
```
Each row describes one crate. Table sorted by physical position of crate.

* _Circuit:_ Name of magnet circuit  
* _Crate:_ QPS crate
* _U_DIODE_RQx:_ Name of magnet which the diode signal belongs to
* _U_REF_N1:_ Name of magnet which the reference diode signal belongs to
* _#Diode_circuit:_ The order diodes are electrically connected (different from physical position in tunnel)      

```
lhcsmapi/metadata/qps_crate/RQ_CrateToVoltageFeelerMap.csv
```

```
Circuit,Crate,U_EARTH_RQx,#VF_circuit
RQD.A12,B11L2,MQ.12L2,1
```

Each row describes one crate. Table sorted by physical position of crate.

* _Circuit:_ Name of magnet circuit  
* _Crate:_ QPS crate
* _U_EARTH_RQx:_ Name of magnet which the earth / voltage feeler (VF) signal belongs to
* _#VF_circuit:_ The order VFs are electrically connected (different from physical position in tunnel)      

```
lhcsmapi/metadata/qps_crate/RQ_MagnetToCrateMap.csv
```

```
Crate,Circuit,#Diode_ circuit,U_DIODE_RQx,U_REF_N1
B11L2,RQD.A12,,,
B13L2,RQD.A12,1,MQ.12L2,MQ.14L2
```
Each row describes one crate. Table sorted by physical position of crate. Table is similar to _RQ_CrateToDiodeMap.csv_

**Sector to circuit names**
```
lhcsmapi/metadata/sector/Circuit_To_Sector_Names.csv

,Circuit type SIGMON,Circuit name,Sector name,Circuit type Layout,Safety subsector name
0,RB,RB.A12,S12,MAINDIPOLE,A12
```
This file represents the grouping of the various SC circuits in LHC. Most of this information is used by LHC-OP and is part of the LHC functional layout.
This grouping is needed to know which circuits are powered together in the HWC PGC tests (4 separate Notebooks).
 
* _Unnamed column:_ Row number 0 ... N-1. Internal ordering criterion
* _Circuit type SIGMON:_ Logical grouping needed by the logic in the _PGC Notebooks_. Mainly related to which _QPS class_ to look for PM data.
* _Circuit name:_ Circuit name as given by LHC layout database
* _Sector name:_ Circuit location grouped by the 8 sectors (S12, S23, ... S81). Each sector contains an arc (1/8 of a circle) plus straights parts around the IPs (LSS - long straight section)   
* _Circuit type Layout:_ Logical circuit grouping driven by PC current and EE criteria for the various 600 A subtypes (EE, NOEE = No EE, CR = Crowbar for shunting the PC).  
* _Safety subsector name:_ Logical circuit grouping wrt safety for personnel working in the tunnel. Some of the naming scheme is not so obvious so I will not attempt interpretation.  

The cryogenics view of LHC is again different from this table. At least the naming used. Maybe not the grouping.

## References

_TO BE DONE_

**Circuit RQ parameters**

_TO BE DONE_

**EE RB and RQ reference features**

_TO BE DONE_

**MP3 periods and quench files**

_TO BE DONE_

**PC RB and RQ reference features**

_TO BE DONE_

**HWC Summary - outside Gitlab**

_TO BE DONE_

**QH features**

_TO BE DONE_