RELEASE NOTES
=============

Version: 1.6.7
- reference's timestamp to be strictly lower: [SIGMON-485](https://its.cern.ch/jira/browse/SIGMON-485)
- reference discharges updated for RB, RQ and IPD_2: [SIGMON-479](https://its.cern.ch/jira/browse/SIGMON-479), [SIGMON-473](https://its.cern.ch/jira/browse/SIGMON-473), [SIGMON-484](https://its.cern.ch/jira/browse/SIGMON-484)
- pandas 2.0 adaptations: [SIGMON-483](https://its.cern.ch/jira/browse/SIGMON-483)
- fixed selection box crashing on nan timestamp: [SIGMON-481](https://its.cern.ch/jira/browse/SIGMON-481)
- gradient-based derivative calculation fixed: [SIGMON-480](https://its.cern.ch/jira/browse/SIGMON-480)
- support RD1s after the upgrade of QPS: [SIGMON-477](https://its.cern.ch/jira/browse/SIGMON-477)
- `pm-api-pro` not used anymore: [SIGMON-486](https://its.cern.ch/jira/browse/SIGMON-486)
- the data from the QPS testbed devices is filtered out based on the source == 'DT' (not source containing 'DT') 


Version: 1.6.6
- HWC_Operation_YETS_Periods updated (bugfix)


Version: 1.6.5
- `ILOOP.I_REF` used instead of `STATUS.I_REF`, RB sources without the `U_DIODE` signal filtered out by the library: [SIGMON-467](https://its.cern.ch/jira/browse/SIGMON-467)
- new YETS added
- the metadata corrected for `V_MEAS` signal


Version: 1.6.4
- added the default implementation of `AnalysisManager.get_result`: [SIGMON-458](https://its.cern.ch/jira/browse/SIGMON-458)
- introduced an optional `u_res_min` argument for the Qds' quench detection: [SIGMON-461](https://its.cern.ch/jira/browse/SIGMON-461)
- adjusted the deployment to EOS so it works with `102b NXCALS PRO` software stack: [SIGMON-466](https://its.cern.ch/jira/browse/SIGMON-466)


Version: 1.6.3
- new references (past successful tests) updated: [SIGMON-419](https://its.cern.ch/jira/browse/SIGMON-419)
- variable query used for the PC analysis: [SIGMON-441](https://its.cern.ch/jira/browse/SIGMON-441)
- scaling/offset bug fixed in the `analyze_ee_temp`: [SIGMON-448](https://its.cern.ch/jira/browse/SIGMON-448)
- do not expect any order in `HWC_Summary.csv` file: [SIGMON-463](https://its.cern.ch/jira/browse/SIGMON-463)
- depend on `scikit-learn` not `sklearn` (deprecation fix)


Version: 1.6.2
- the default PM REST API hostname set to the real one, not '': [SIGMON-388](https://its.cern.ch/jira/browse/SIGMON-388)
- warnings about the np.float misuse removed: [SIGMON-388](https://its.cern.ch/jira/browse/SIGMON-388)
- duplicated FGCs from IPQs dropped properly (B1 B2): [SIGMON-408](https://its.cern.ch/jira/browse/SIGMON-408)
- openjdk:8 used for sonar: [SIGMON-411](https://its.cern.ch/jira/browse/SIGMON-411)
- event's timestamp dtype set to int in query: [SIGMON-397](https://its.cern.ch/jira/browse/SIGMON-397)
- QHDA final voltage thresholds updated: [SIGMON-427](https://its.cern.ch/jira/browse/SIGMON-427)
- local development documentation prepared: [SIGMON-396](https://its.cern.ch/jira/browse/SIGMON-396)
- quench detection algorithm for IPQs/IPDs written (based on I_A and I_REF signals): [SIGMON-409](https://its.cern.ch/jira/browse/SIGMON-409)
- slope calculation corrected: [SIGMON-428](https://its.cern.ch/jira/browse/SIGMON-428) 
- tutorial notebook about the QPS parameters created


Version: 1.6.1
- pm dump selection tool fixed for IPQs and IPDs


Version: 1.6.0
- queries that uses `ParametersResolver` implemented: [SIGMON-316](https://its.cern.ch/jira/browse/SIGMON-316)
- removed support for Nxcals Spark 2 on SWAN [SIGMON-283](https://its.cern.ch/jira/browse/SIGMON-283)
- tests for the QueryParams domain objects [SIGMON-319](https://its.cern.ch/jira/browse/SIGMON-319)
- do not assert sigmon version in a test [SIGMON-317](https://its.cern.ch/jira/browse/SIGMON-317)
- `SignalTransformationBuilder` removed [SIGMON-291](https://its.cern.ch/jira/browse/SIGMON-291)
- `ResistanceBuilder` removed [SIGMON-290](https://its.cern.ch/jira/browse/SIGMON-290)
- tutorial notebook that presents the new 'signal analysis' API written [SIGMON-321](https://its.cern.ch/jira/browse/SIGMON-321)
- `FeatureQuery` removed [SIGMON-295](https://its.cern.ch/jira/browse/SIGMON-295)
- query API examplery notebook written [SIGMON-320](https://its.cern.ch/jira/browse/SIGMON-320)
- support for CRYO system added [SIGMON-336](https://its.cern.ch/jira/browse/SIGMON-336)
- `AnalysisManager` created [SIGMON-329](https://its.cern.ch/jira/browse/SIGMON-329)
- new QHDA references [SIGMON-339](https://its.cern.ch/jira/browse/SIGMON-339)
- differences between query API documented [SIGMON-341](https://its.cern.ch/jira/browse/SIGMON-341)
- IPQ search tool fix [SIGMON-351](https://its.cern.ch/jira/browse/SIGMON-351)
- plateau detection algorithm improved [SIGMON-322](https://its.cern.ch/jira/browse/SIGMON-322)
- backwards compatible `QueryBuilder` written [SIGMON-299](https://its.cern.ch/jira/browse/SIGMON-299)
- extract a separate `lhcsmapi-qh` package [SIGMON-326](https://its.cern.ch/jira/browse/SIGMON-326)
- installation manual updated [SIGMON-359](https://its.cern.ch/jira/browse/SIGMON-359)
- bugfix for comparisons [SIGMON-337](https://its.cern.ch/jira/browse/SIGMON-337)
- analysis output classes added [SIGMON-355](https://its.cern.ch/jira/browse/SIGMON-355)
- test coverage increased [SIGMON-261](https://its.cern.ch/jira/browse/SIGMON-261)
- AN_RB_FPA corrections: events of class `ext` filtered out, only corresponding subsector queried for events, plots fixed [SIGMON-352](https://its.cern.ch/jira/browse/SIGMON-352)
- 100 first points removed from signal used for a quench detection [SIGMON-335](https://its.cern.ch/jira/browse/SIGMON-335)
- PNO.b2 / FPA notebooks won't fail on empty new QPS [SIGMON-318](https://its.cern.ch/jira/browse/SIGMON-318)
- deployment to EOS of the analysis packages done [SIGMON-364](https://its.cern.ch/jira/browse/SIGMON-364)
- a searching box in RCBXHV and RCDO FPA notebooks fixed
- deploy `lhcsmapi` to AccPy (instead of PyPi) [SIGMON-325](https://its.cern.ch/jira/browse/SIGMON-325)
- deployment to EOS of the `lhc-sm-analysis` fixed [SIGMON-369](https://its.cern.ch/jira/browse/SIGMON-369)
- `lhcsmapi.gui.pc` refactored [SIGMON-356](https://its.cern.ch/jira/browse/SIGMON-356)
- `lhcsmapi` deployed to AccPy (instead of PyPi) [SIGMON-325](https://its.cern.ch/jira/browse/SIGMON-325)


Version: 1.5.21
- `cern` prefix removed from the nxcals imports: [SIGMON-307](https://its.cern.ch/jira/browse/SIGMON-307)
- separate class to resolve signal metadata: [SIGMON-297](https://its.cern.ch/jira/browse/SIGMON-297)
- QPS thresholds added: [SIGMON-48](https://its.cern.ch/jira/browse/SIGMON-48) 
- list of the HWC_Operation_YETS updated
- signal_metadata package refactored (previously SignalMetadata): [SIGMON-309](https://its.cern.ch/jira/browse/SIGMON-309)
- use python 3.8 on GitLab: [SIGMON-282](https://its.cern.ch/jira/browse/SIGMON-282) 


Version: 1.5.20
- package data (metadata files) correctly included in the `lhcsmapi`: [SIGMON-312](https://its.cern.ch/jira/browse/SIGMON-312)
- new, lightweight api to query Nxcals: [SIGMON-294](https://its.cern.ch/jira/browse/SIGMON-294)
- new, lightweight api to query Post Mortem: [SIGMON-296](https://its.cern.ch/jira/browse/SIGMON-296)
- FeatureBuilder refactored: [SIGMON-298](https://its.cern.ch/jira/browse/SIGMON-298)
- SignalProcessing refactored: [SIGMON-289](https://its.cern.ch/jira/browse/SIGMON-289)


Version: 1.5.19
- Pm source filtering for QH, QDS and DIODE improved: [SIGMON-293](https://its.cern.ch/jira/browse/SIGMON-173)
- Result table logic unified for RBs and RQs (single-row table generated if no quench): [SIGMON-248](https://its.cern.ch/jira/browse/SIGMON-248)
- Optional median filter in RbCircuitAnalysis#assert_u_res_between_two_references: [SIGMON-258](https://its.cern.ch/jira/browse/SIGMON-258)
- RCD circuit family renamed to RCO-RCD: [SIGMON-277](https://its.cern.ch/jira/browse/SIGMON-277)
- Metadata structure for Nxcals systems changed: [SIGMON-256](https://its.cern.ch/jira/browse/SIGMON-256)
- Update of the RB csv files for nQPS PM: [SIGMON-230](https://its.cern.ch/jira/browse/SIGMON-230)
- Update of a few magnet tables after HWC 2021: [SIGMON-259](https://its.cern.ch/jira/browse/SIGMON-259)
- HWC_Summary.csv updated: [SIGMON-232](https://its.cern.ch/jira/browse/SIGMON-232)
- Calls to deprecated Nxcals' methods removed: [SIGMON-273](https://its.cern.ch/jira/browse/SIGMON-273)
- PM signal names corrected in the metadata for 600A
- R_MAG signal used for the magnet resistance analysis: [SIGMON-267](https://its.cern.ch/jira/browse/SIGMON-267)
- The filtering changed in DiodeLeadResistanceAnalysis#plot_current_voltage_nxcals so the U_REF_N1 signal can be used: [SIGMON-251](https://its.cern.ch/jira/browse/SIGMON-251) 
- Support queries for multiple signals in QueryBuilder: [SIGMON-245](https://its.cern.ch/jira/browse/SIGMON-245)
- Implementation of the logic to store and interpret the result of the exemplary notebook (for qualification purposes): [SIGMON-217](https://its.cern.ch/jira/browse/SIGMON-217)  
- CONTRIBUTING.md updated: [SIGMON-241](https://its.cern.ch/jira/browse/SIGMON-241)  
- YAPF (python formatter) configured for the project: [SIGMON-236](https://its.cern.ch/jira/browse/SIGMON-236) 
- New GitLab Runner prepared and deployed: [SIGMON-231](https://its.cern.ch/jira/browse/SIGMON-231)
- The documentation migrated from MkDocs Sites to OKD4: [SIGMON-204](https://its.cern.ch/jira/browse/SIGMON-204)


Version: 1.5.18
- Verify whether QPS LEADS data as of HWC 2021 is accessible from CMW in NXCALS: [SIGMON-86](https://its.cern.ch/jira/browse/SIGMON-86)
- Notebooks session in parallel: [SIGMON-114](https://its.cern.ch/jira/browse/SIGMON-114)
- Automatic delete of the venv branches: [SIGMON-140](https://its.cern.ch/jira/browse/SIGMON-140)
- Update Earth Current criteria in RB PNO.b2: [SIGMON-171](https://its.cern.ch/jira/browse/SIGMON-171)
- Use matplotlib inline with papermill: [SIGMON-173](https://its.cern.ch/jira/browse/SIGMON-173)
- HWC_RQ_QHDA: add a comparison to the reference: [SIGMON-175](https://its.cern.ch/jira/browse/SIGMON-175)
- Errors in notebooks_exec are not propagated: [SIGMON-181](https://its.cern.ch/jira/browse/SIGMON-181)
- Notebooks failure in CI: [SIGGMON-183](https://its.cern.ch/jira/browse/SIGMON-183)
- Update LHCData owner[SIGMON-185](https://its.cern.ch/jira/browse/SIGMON-185)
- Cleanup after SIGMON-133 [SIGMON-158](https://its.cern.ch/jira/browse/SIGMON-158)
- Make RB_Analysis::analyze_qds work with the corrupted buffers [SIGMON-162](https://its.cern.ch/jira/browse/SIGMON-162)
- Exception in PCAnalysis [SIGMON-156](https://its.cern.ch/jira/browse/SIGMON-156)
- Failing CI tests [SIGMON-165](https://its.cern.ch/jira/browse/SIGMON-156)
- Add I_EARTH_PCNT in full duration in RB and RQ [SIGMON-82](https://its.cern.ch/jira/browse/SIGMON-82)
- Adapt all notebooks to the CI [SIGMON-141](https://its.cern.ch/jira/browse/SIGMON-141)
- FgcPmSearchModuleMediator throws KeyError [SIGMON-153](https://its.cern.ch/jira/browse/SIGMON-153)
- Index out of range for QH B11R6 (AN_RB_FPA PRO) [SIGMON-166](https://its.cern.ch/jira/browse/SIGMON-166)
- Wrong pm_rest_api_path default value in get_response() [SIGMON-170](https://its.cern.ch/jira/browse/SIGMON-170)
- notebooks_exec job failing in gitlab ci [SIGMON-180](https://its.cern.ch/jira/browse/SIGMON-180)
- IPQ query fails [SIGMON-152](https://its.cern.ch/jira/browse/SIGMON-152)
- Make RB_Analysis::analyze_qds work with the corrupted buffers [SIGMON-162](https://its.cern.ch/jira/browse/SIGMON-162)
- Correct RB metadata for Run2 [SIGMON-172](https://its.cern.ch/jira/browse/SIGMON-172)
- Improve warnings displayed while querying PM [SIGMON-191](https://its.cern.ch/jira/browse/SIGMON-191)
- Incorrect implementation of reading RQ U_DIODE_RQx signals [SIGMON-179](https://its.cern.ch/jira/browse/SIGMON-179)
- QhPmSearchModuleMediator throws TypeError [SIGMON-155](https://its.cern.ch/jira/browse/SIGMON-155)
- New version of the RB FPA [SIGMON-132](https://its.cern.ch/jira/browse/SIGMON-132)
- Don't crash on missing data [SIGMON-145](https://its.cern.ch/jira/browse/SIGMON-145)
- Incorrect dates cause error during query for events [SIGMON-194](https://its.cern.ch/jira/browse/SIGMON-194)
- Update AN_RB_PLI1.d2. [SIGMON-225](https://its.cern.ch/jira/browse/SIGMON-225)
- Drop duplicates after sorting [SIGMON-223](https://its.cern.ch/jira/browse/SIGMON-223)
- update RB_FPA_SNAP notebook [SIGMON-207](https://its.cern.ch/jira/browse/SIGMON-207)
