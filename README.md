# LHC Signal Monitoring API Package

The library for easily querying and processing signals and metadata to analyse quenches and results from tests during Hardware Commissioning campaigns.

The user and installation guide is available at <https://sigmon.docs.cern.ch/>
